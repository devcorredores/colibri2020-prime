module.exports = {

    usrs: [],
    dbs: [
      {
        nombre: "SOPORTE-DESGRAVAMEN",
        valor: "mssql://sa:123456@MMOSCOSO/pnet_seg",
        descripcion:"Conexión LOCALHOST a la base de datos PNET_SEG"
      },
      {
        nombre: "SOPORTE-DESGRAVAMEN_2",
        valor : "mssql://user:password@host.database.windows.net/PNet_Seg?encrypt=true",
        descripcion:"Conexión AZURE CLOUD a la base de datos PNET_SEG"
      },
      {
        nombre: "CONEXION-GARANTIAS_1",
        valor : "mssql://sa:123456@DESKTOP-AA8G8RJ/corredoresdb",
        descripcion:"Conexión LOCALHOST - GARANTIAS"
      },
      {
        nombre: "CONEXION-GARANTIAS",
        valor : "mssql://sa:123456@localhost/corredoresdb",
        descripcion:"Conexión LOCALHOST - GARANTIAS"
      },
      {
        nombre: "CONEXION-GARANTIAS_3",
        valor : "mssql://user:password@host.database.windows.net/corredoresdb?encrypt=true",
        descripcion:"Conexión AZURE CLOUD - GARANTIAS"
      }
    ],
    emails: [],
    wssoap: [
      {
        // nombre: "WS-SOAP-ECOFUTURO_1",
        // wsdl:"https://17X.1X.1X.9X:40448/SeguroServices/Seguros.asmx?wsdl",
        nombre: "WS-SOAP-ECOFUTURO",
        wsdl:"https://192.168.204.168:6667/ECOServices/ECOService.asmx?wsdl",
        descripcion:"Servicio web soap de Banco Ecofuturo - PRODUCCION"
      },
      {
        // nombre: "WS-SOAP-ECOFUTURO",
        nombre: "WS-SOAP-ECOFUTURO_1",
        wsdl:"http://171.3.0.10:8080/EcoSuda/Ecoservice.asmx?wsdl",
        descripcion:"Servicio web soap de Banco Ecofuturo - DESARROLLO"
      },
      {
        nombre: "WS-SOAP-ECOFUTURO-CREDITICIOS",
        wsdl:"https://172.16.15.98:40448/SeguroServices/Seguros.asmx?wsdl",
        descripcion:"Servicio web soap de Banco Ecofuturo Crediticios - DESARROLLO"
      },
      {
        nombre: "WS-SOAP-ECOFUTURO-AUTH",
        wsdl: "https://172.16.15.98:7000/ECOServicesCobranza/ECOCobranza.asmx?wsdl",
        descripcion: "Servicio web soap Autenticacion del Banco Ecofuturo"
      }
    ],
    params: []
    
  };
  
  
