
const axios = require('axios');

//prod
const url_api = 'http://104.42.198.35:3002/api-eco-seguros';

//desa
//const url_api = 'http://192.168.4.62:3000/api-eco-seguros';

let request_soap = {
  request_body:{solicitud:0},
  operation:"getSolicitud"
};


const getSolicitud = (nro_solicitud) => {
  const url = `${url_api}`;
  request_soap.request_body.solicitud = nro_solicitud;
  request_soap.operation = "getSolicitud";
  return new Promise((resolve, reject) => {
    axios.post(url, request_soap)
    .then((response) => {
      resolve(response.data)
    }, (error) => {
      reject(error);
    });

  });
};

const getSolicitudPrimeraEtapa = (nro_solicitud) => {
  const url = `${url_api}`;
  request_soap.request_body.solicitud = nro_solicitud;
  request_soap.operation = "getSolicitudPrimeraEtapa";
  return new Promise((resolve, reject) => {
    axios.post(url, request_soap)
    .then((response) => {
      resolve(response.data)
    }, (error) => {
      reject(error);
    });

  });
};

const getSolicitudSegundaEtapa = (nro_solicitud) => {
  const url = `${url_api}`;
  request_soap.request_body.solicitud = nro_solicitud;
  request_soap.operation = "getSolicitudSegundaEtapa";
  return new Promise((resolve, reject) => {
    axios.post(url, request_soap)
    .then((response) => {
      resolve(response.data)
    }, (error) => {
      reject(error);
    });

  });
};

const getSolicitudTerceraEtapa = (nro_solicitud) => {
  const url = `${url_api}`;
  request_soap.request_body.solicitud = nro_solicitud;
  request_soap.operation = "getSolicitudTerceraEtapa";
  return new Promise((resolve, reject) => {
    axios.post(url, request_soap)
    .then((response) => {
      resolve(response.data)
    }, (error) => {
      reject(error);
    });

  });
};


module.exports.getSolicitud = getSolicitud;
module.exports.getSolicitudPrimeraEtapa = getSolicitudPrimeraEtapa;
module.exports.getSolicitudSegundaEtapa = getSolicitudSegundaEtapa;
module.exports.getSolicitudTerceraEtapa = getSolicitudTerceraEtapa;
