var appRoot = require('app-root-path');
var winston = require('winston');
const { transports, format } = winston;

require('winston-daily-rotate-file');

const print = format.printf((info) => {
    const log = `${info.level}: ${info.message}`;

    return info.stack
        ? `${log}\n${info.stack}`
        : log;
});

var transportDailyError = new winston.transports.DailyRotateFile({
    level: 'error',
    filename: `${appRoot}/logs/app-%DATE%-error.log`,
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: false,
    maxSize: '20m',
    maxFiles: '14d'
});

var transportDaily = new winston.transports.DailyRotateFile({
    level: 'info',
    filename: `${appRoot}/logs/app-%DATE%-info.log`,
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: false,
    maxSize: '20m',
    maxFiles: '14d'
});

var transportEmail = new (winston.transports.File)({
    name: 'email-log',
    filename: `${appRoot}/logs/app-email.log`,
    level: 'info',
    timestamp: true,
    colorize: true,
    handleExceptions: true,
    humanReadableUnhandledException: true,
    prettyPrint: true,
    json: true,
    maxsize: 5242880
});

var transportCatch = new (winston.transports.File)({
    name: 'error-log',
    filename: `${appRoot}/logs/app-error.log`,
    level: 'info',
    timestamp: true,
    colorize: true,
    handleExceptions: true,
    humanReadableUnhandledException: true,
    prettyPrint: true,
    json: true,
    maxsize: 5242880
});

var transportFtp = new (winston.transports.File)({
    name: 'ftp-log',
    filename: `${appRoot}/logs/app-ftp.log`,
    level: 'info',
    timestamp: true,
    colorize: true,
    handleExceptions: true,
    humanReadableUnhandledException: true,
    prettyPrint: true,
    json: true,
    maxsize: 5242880
});

var transportSolicitudes = new (winston.transports.File)({
    name: 'solicitud-log',
    filename: `${appRoot}/logs/app-solicitud.log`,
    level: 'info',
    timestamp: true,
    colorize: true,
    handleExceptions: true,
    humanReadableUnhandledException: true,
    prettyPrint: true,
    json: true,
    maxsize: 5242880
});

var transportPlanPago = new (winston.transports.File)({
    name: 'plan-pago-log',
    filename: `${appRoot}/logs/app-plan-pago.log`,
    level: 'info',
    timestamp: true,
    colorize: true,
    handleExceptions: true,
    humanReadableUnhandledException: true,
    prettyPrint: true,
    json: true,
    maxsize: 5242880
});

var transportLogin = new (winston.transports.File)({
    name: 'login-log',
    filename: `${appRoot}/logs/app-login.log`,
    level: 'info',
    timestamp: true,
    colorize: true,
    handleExceptions: true,
    humanReadableUnhandledException: true,
    prettyPrint: true,
    json: true,
    maxsize: 5242880
});

var options = {
    console: {
        level: 'info',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

var optionsError = {
    console: {
        level: 'error',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

const logger = winston.createLogger({
    level: 'info',
    format: format.combine(
        format.errors({ stack: true }),
        print,
    ),
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        // new winston.transports.File(options.file),
        transportDaily,
        new winston.transports.Console(options.console),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

const loggerError = winston.createLogger({
    level: 'error',
    format: format.combine(
        format.errors({ stack: true }),
        print,
    ),
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        // new winston.transports.File(options.file),
        transportDailyError,
        new winston.transports.Console(optionsError.console),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

const loggerEmail = winston.createLogger({
    level: 'info',
    format: format.combine(
      format.splat(),
      format.simple()
    ),
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        // new winston.transports.File(options.file),
        transportEmail,
        new winston.transports.Console(options.console),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

const loggerCatch = winston.createLogger({
    level: 'info',
    format: format.combine(
      format.splat(),
      format.simple()
    ),
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        // new winston.transports.File(options.file),
        transportCatch,
        new winston.transports.Console(options.console),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

const loggerFtp = winston.createLogger({
    level: 'info',
    format: format.combine(
      format.splat(),
      format.simple()
    ),
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        // new winston.transports.File(options.file),
        transportFtp,
        new winston.transports.Console(options.console),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

const loggerSolicitud = winston.createLogger({
    level: 'info',
    format: format.combine(
      format.splat(),
      format.simple()
    ),
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        // new winston.transports.File(options.file),
        transportSolicitudes,
        new winston.transports.Console(options.console),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

const loggerPlanPago = winston.createLogger({
    level: 'info',
    format: format.combine(
      format.splat(),
      format.simple()
    ),
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        // new winston.transports.File(options.file),
        transportPlanPago,
        new winston.transports.Console(options.console),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

const loggerLogin = winston.createLogger({
    level: 'info',
    format: format.combine(
      format.splat(),
      format.simple()
    ),
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        // new winston.transports.File(options.file),
        transportLogin,
        new winston.transports.Console(options.console),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple(),
    }));
}

logger.stream = {
    write: function(message, encoding) {
        logger.info(message);
    },
};

module.exports.logger = logger;
module.exports.loggerFtp = loggerFtp;
module.exports.loggerError = loggerError;
module.exports.loggerEmail = loggerEmail;
module.exports.loggerCatch = loggerCatch;
module.exports.loggerLogin = loggerLogin;
module.exports.loggerPlanPago = loggerPlanPago;
module.exports.loggerSolicitud = loggerSolicitud;
