getDateYYYYmmDD = () => {
  var d = new Date(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
};

getHoraActual = ()=>{

  const  d = new Date;   
  const hformat = [d.getHours(),d.getMinutes(),d.getSeconds()].join(':');

  return hformat;

}


dateFull = ()=>{
  const d = new Date();
  let dd = ''+d.getDate();
  let mm = ''+d.getMonth() + 1;
  const yyyy = d.getFullYear();
  const hh = d.getHours();
  const min = d.getMinutes();
  if (dd.length < 2)  dd = '0'+dd;
  if (mm.length < 2) mm = '0'+mm;
  return `${yyyy}-${mm}-${dd} ${hh}:${min}`;
}

horaActual = ()=>{
  const d = new Date();
  const hh = d.getHours();
  const min = d.getMinutes();
  return `${hh}:${min}`;
}

fechaActual = ()=>{
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  return [dd,mm,yyyy].join('-');
};

  fechaActualYYYmmDD = () =>{
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');

  }

formatDateDDmmYYYY = (date) => {
  var today = date;
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  return [dd,mm,yyyy].join('-');
}

encriptar = (param1, param2) => {
    const crypto = require('crypto')
    const hmac = crypto.createHmac('sha1', param1).update(param2).digest('hex')
    return hmac
};

fechaServidor = ()=>{
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  const yyyy = today.getFullYear();
  const hours = today.getHours();
  const minutes = today.getMinutes();

  return `${dd}/${mm}/${yyyy} ${hours}:${minutes}`;
}


//module.exports.formatDateYYYYmmDD = formatDateYYYYmmDD;
module.exports.formatDateDDmmYYYY = formatDateDDmmYYYY;
module.exports.fechaActual = fechaActual;
module.exports.encriptar = encriptar;
module.exports.dateFull = dateFull;
module.exports.horaActual = horaActual;
module.exports.fechaServidor = fechaServidor;
module.exports.getDateYYYYmmDD = getDateYYYYmmDD;
module.exports.getHoraActual = getHoraActual;
