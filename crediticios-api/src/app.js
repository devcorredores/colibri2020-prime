const express = require('express');
const app = require("express")();
const http = require("http").Server(app);
const https = require('https'); 
const path = require('path');
const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');
const compression = require('compression');
const util = require('./modules/util');
const env = require("./config/env");

const fs = require('fs');

const https_options = {
	key: fs.readFileSync(path.join(__dirname, 'sslcert/seguroscolibri.key'), 'utf8'),
	cert: fs.readFileSync(path.join(__dirname, 'sslcert/seguroscolibri.crt'), 'utf8'),
	// ca: [
	//   fs.readFileSync(path.join(__dirname, 'sslcert/ce_publickey.crt'), 'utf8'),
	//   fs.readFileSync(path.join(__dirname, 'sslcert/ce_bunble.crt'), 'utf8')
	// ]
};

app.set('port', env.PORT || 4007);
app.set('ports', env.PORTSSL || 4006);

// middlewares
app.use(express.urlencoded({ extended: false }));
app.use(morgan('combined'));
app.use(cors());
app.use(helmet());
//app.use(helmet.frameguard({ action: 'deny' }));
app.use(compression());
app.use(express.json());
//createLog();

app.use("/api-corredores-ecofuturo/file", require("./routes/file-ruta"));
app.use("/api-corredores-ecofuturo/archivo", require("./routes/archivo-ruta"));

// static files
app.use(express.static(path.join(__dirname, 'public')));

// starting server http
http.listen(app.get('port'),()=>{
   console.log(` ${new Date()} : Server http escuchando en el puerto ${app.get('port')}`);
});

// starting server https
const httpsServer = https.createServer(https_options,app);
httpsServer.listen(app.get('ports'), () => {
  console.log(` ${new Date()} :  Server https escuchando en el puerto ${app.get('ports')}`);
});


