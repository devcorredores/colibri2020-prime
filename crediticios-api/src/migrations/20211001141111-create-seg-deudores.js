'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Seg_Deudores', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Deu_Id: {
        type: Sequelize.INTEGER
      },
      Deu_IdSol: {
        type: Sequelize.INTEGER
      },
      Deu_NIvel: {
        type: Sequelize.INTEGER
      },
      Deu_Nombre: {
        type: Sequelize.STRING
      },
      Deu_Paterno: {
        type: Sequelize.STRING
      },
      Deu_Materno: {
        type: Sequelize.STRING
      },
      Deu_Casada: {
        type: Sequelize.STRING
      },
      Deu_DirDom: {
        type: Sequelize.STRING
      },
      Deu_DirOfi: {
        type: Sequelize.STRING
      },
      Deu_TelDom: {
        type: Sequelize.STRING
      },
      Deu_TelOfi: {
        type: Sequelize.STRING
      },
      Deu_TelCel: {
        type: Sequelize.STRING
      },
      Deu_EstCiv: {
        type: Sequelize.STRING
      },
      Deu_Sexo: {
        type: Sequelize.STRING
      },
      Deu_Actividad: {
        type: Sequelize.STRING
      },
      Deu_DetACtiv: {
        type: Sequelize.STRING
      },
      Deu_FecNac: {
        type: Sequelize.STRING
      },
      Deu_PaisNac: {
        type: Sequelize.STRING
      },
      Deu_CiudadNac: {
        type: Sequelize.STRING
      },
      Deu_TipoDoc: {
        type: Sequelize.STRING
      },
      Deu_NumDoc: {
        type: Sequelize.STRING
      },
      Deu_ExtDoc: {
        type: Sequelize.STRING
      },
      Deu_CompDoc: {
        type: Sequelize.STRING
      },
      Deu_CodCli: {
        type: Sequelize.STRING
      },
      Deu_FecRegCli: {
        type: Sequelize.STRING
      },
      Deu_Mano: {
        type: Sequelize.STRING
      },
      Deu_Peso: {
        type: Sequelize.STRING
      },
      Deu_Talla: {
        type: Sequelize.STRING
      },
      Deu_MontoActAcum: {
        type: Sequelize.STRING
      },
      Deu_Incluido: {
        type: Sequelize.STRING
      },
      Deu_MontoActAcumVerif: {
        type: Sequelize.STRING
      },
      Deu_Verificado: {
        type: Sequelize.STRING
      },
      Deu_Edad: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Seg_Deudores');
  }
};