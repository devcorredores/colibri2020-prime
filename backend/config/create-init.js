const env = require('./env');
const Sequelize = require('sequelize')
const sequelize = new Sequelize(env.database, env.username, env.password, {
    host: env.host,
    dialect: env.dialect,
    operatorsAliases: false,
    native: true,
    logging: true,
    define: {
        freezeTableName: true
    },
    pool: {
        max: env.max,
        min: env.pool.min,
        acquire: env.pool.acquire,
        idle: env.pool.idle
    }
});

sequelize.authenticate()
    .then(() => {
        console.log('Conectado')
    })
    .catch(err => {
        console.log('No se conecto')
    });

// CREACION DE TABLAS

const diccionario = require('../modelos/diccionario')(sequelize,Sequelize);
//diccionario.sync({ force: true }).then(() => { console.log('Creada la tabla diccionario'); return true;});

const parametro = require('../modelos/parametro')(sequelize,Sequelize);
//persona.sync({ force: true }).then(() => { console.log('Creada la tabla parametro'); return true;});

const persona = require('../modelos/persona')(sequelize,Sequelize);
//persona.sync({ force: true }).then(() => { console.log('Creada la tabla persona'); return true;});

const usuario = require('../modelos/usuario')(sequelize,Sequelize);
//usuario.sync({ force: true }).then(() => { console.log('Creada la tabla usuario'); return true;});

const rol = require('../modelos/rol')(sequelize,Sequelize);
//rol.sync({ force: true }).then(() => { console.log('Creada la tabla rol'); return true;});

const usuario_x_rol = require('../modelos/usuario_x_rol')(sequelize,Sequelize);
//usuario_x_rol.sync({ force: true }).then(() => { console.log('Creada la tabla usuario_x_rol'); return true;});

const perfil = require('../modelos/perfil')(sequelize,Sequelize);
//perfil.sync({ force: true }).then(() => { console.log('Creada la tabla perfil'); return true;});

const rol_x_perfil = require('../modelos/rol_x_perfil')(sequelize,Sequelize);
//rol_x_perfil.sync({ force: true }).then(() => { console.log('Creada la tabla rol_x_perfil'); return true;});

const modulo = require('../modelos/modulo')(sequelize,Sequelize);
//modulo.sync({ force: true }).then(() => { console.log('Creada la tabla modulo'); return true;});

const perfil_x_modulo = require('../modelos/perfil_x_modulo')(sequelize,Sequelize);
//perfil_x_modulo.sync({ force: true }).then(() => { console.log('Creada la tabla perfil_x_modulo'); return true;});

const vista = require('../modelos/vista')(sequelize,Sequelize);
//vista.sync({ force: true }).then(() => { console.log('Creada la tabla vista'); return true;});

const perfil_x_vista = require('../modelos/perfil_x_vista')(sequelize,Sequelize);
//perfil_x_vista.sync({ force: true }).then(() => { console.log('Creada la tabla perfil_x_vista'); return true;});

const componente = require('../modelos/componente')(sequelize,Sequelize);
//componente.sync({ force: true }).then(() => { console.log('Creada la tabla componente'); return true;});

const perfil_x_componente = require('../modelos/perfil_x_componente')(sequelize,Sequelize);
//perfil_x_componente.sync({ force: true }).then(() => { console.log('Creada la tabla perfil_x_componente'); return true;});

const persona_juridica = require('../modelos/persona_juridica')(sequelize,Sequelize);
//persona_juridica.sync({ force: true }).then(() => { console.log('Creada la tabla persona_juridica'); return true;});

const entidad = require('../modelos/entidad')(sequelize,Sequelize);
//entidad.sync({ force: true }).then(() => { console.log('Creada la tabla entidad'); return true;});

const poliza = require('../modelos/poliza')(sequelize,Sequelize);
//poliza.sync({ force: true }).then(() => { console.log('Creada la tabla poliza'); return true;});

const objeto = require('../modelos/objeto')(sequelize,Sequelize);
//objeto.sync({ force: true }).then(() => { console.log('Creada la tabla objeto'); return true;});

const atributo = require('../modelos/atributo')(sequelize,Sequelize);
//tributo.sync({ force: true }).then(() => { console.log('Creada la tabla atributo'); return true;});

const objeto_x_atributo = require('../modelos/objeto_x_atributo')(sequelize,Sequelize);
//objeto_x_atributo.sync({ force: true }).then(() => { console.log('Creada la tabla objeto_x_atributo'); return true;});

const instancia_poliza = require('../modelos/instancia_poliza')(sequelize,Sequelize);
//instancia_poliza.sync({ force: true }).then(() => { console.log('Creada la tabla instancia_poliza'); return true;});

const atributo_instancia_poliza = require('../modelos/atributo_instancia_poliza')(sequelize,Sequelize);
//atributo_instancia_poliza.sync({ force: true }).then(() => { console.log('Creada la tabla atributo_instancia_poliza'); return true;});

const asegurado = require('../modelos/asegurado')(sequelize,Sequelize);
//asegurado.sync({ force: true }).then(() => { console.log('Creada la tabla asegurado'); return true;});

const tomador = require('../modelos/tomador')(sequelize,Sequelize);
//tomador.sync({ force: true }).then(() => { console.log('Creada la tabla tomador'); return true;});

const intermediario = require('../modelos/intermediario')(sequelize,Sequelize);
//intermediario.sync({ force: true }).then(() => { console.log('Creada la tabla intermediario'); return true;});

//const beneficiario = require('../modelos/beneficiario')(sequelize,Sequelize);
//beneficiario.sync({ force: true }).then(() => { console.log('Creada la tabla beneficiario'); return true;});

const documento = require('../modelos/documento')(sequelize,Sequelize);
//documento.sync({ force: true }).then(() => { console.log('Creada la tabla documento'); return true;});

const instancia_documento = require('../modelos/instancia_documento')(sequelize,Sequelize);
//instancia_documento.sync({ force: true }).then(() => { console.log('Creada la tabla atributo_instancia_poliza'); return true;});

const atributo_instancia_documento = require('../modelos/atributo_instancia_documento')(sequelize,Sequelize);
//atributo_instancia_documento.sync({ force: true }).then(() => { console.log('Creada la tabla atributo_instancia_poliza'); return true;});

//const datos_adicionales_persona = require('../modelos/datos_adicionales_persona')(sequelize,Sequelize);
//datos_adicionales_persona.sync({ force: true }).then(() => { console.log('Creada la tabla datos_adicionales_persona'); return true;});

const anexo_poliza = require('../modelos/anexo_poliza')(sequelize,Sequelize);
//anexo_poliza.sync({ force: true }).then(() => { console.log('Creada la tabla anexo'); return true;});

const clase_asegurado = require('../modelos/clase_asegurado')(sequelize,Sequelize);
//clase_asegurado.sync({ force: true }).then(() => { console.log('Creada la tabla anexo'); return true;});

const anexo_asegurado = require('../modelos/anexo_asegurado')(sequelize,Sequelize);
//anexo_asegurado.sync({ force: true }).then(() => { console.log('Creada la tabla anexo'); return true;});

const instancia_anexo_asegurado = require('../modelos/instancia_anexo_asegurado')(sequelize,Sequelize);
//instancia_anexo_asegurado.sync({ force: true }).then(() => { console.log('Creada la tabla instancia_anexo'); return true;});

const atributo_instancia_anexo_asegurado = require('../modelos/atributo_instancia_anexo_asegurado')(sequelize,Sequelize);
//atributo_instancia_anexo_asegurado.sync({ force: true }).then(() => { console.log('Creada la tabla atributo_instancia_anexo'); return true;});

const dato_anexo_asegurado = require('../modelos/dato_anexo_asegurado')(sequelize,Sequelize);
//dato_anexo_asegurado.sync({ force: true }).then(() => { console.log('Creada la tabla dato_anexo'); return true;});

const reporte_query = require('../modelos/reporte_query')(sequelize,Sequelize);
//reporte_query.sync({ force: true }).then(() => { console.log('Creada la tabla reporte_query'); return true;});

const reporte_query_parametro = require('../modelos/reporte_query_parametro')(sequelize,Sequelize);
//reporte_query_parametro.sync({ force: true }).then(() => { console.log('Creada la tabla reporte_query_parametro'); return true;});

const planPago = require('../modelos/planPago')(sequelize,Sequelize);
//planPago.sync({ force: true }).then(() => { console.log('Creada la tabla planPago'); return true;});

const plan_pago_detalle = require('../modelos/plan_pago_detalle')(sequelize,Sequelize);
//plan_pago_detalle.sync({ force: true }).then(() => { console.log('Creada la tabla plan_pago_detalle'); return true;});

const archivo = require('../modelos/archivo')(sequelize,Sequelize);
//archivo.sync({ force: true }).then(() => { console.log('Creada la tabla archivo'); return true;});

const instancia_archivo = require('../modelos/instancia_archivo')(sequelize,Sequelize);
//instancia_archivo.sync({ force: true }).then(() => { console.log('Creada la tabla instancia_archivo'); return true;});

module.exports = sequelize;








