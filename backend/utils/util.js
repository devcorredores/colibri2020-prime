class Util {

	constructor() {
		this.statusCode = null;
		this.type = null;
		this.data = null;
		this.message = null;
		this.recordsTotal = null;
		this.recordsFiltered = null;
		this.recordsOffset = null;
	}

	static minTwoDigits(n) {
		return (n < 10 ? '0' : '') + n;
	}

	setSuccess(statusCode, message, data, total = 0, filtered = 0, offset = 0, columns = null, searchPanes = null) {
		this.statusCode = statusCode;
		this.message = message;
		this.recordsTotal = parseInt(total);
		this.recordsFiltered = parseInt(filtered);
		this.recordsOffset = parseInt(offset);
		this.searchPanes = searchPanes;
		//this.searchPanes.options = this.setSearchPanes(data, columns);
		this.data = data;
		this.type = 'success';
	}

	setError(statusCode, message) {
		this.statusCode = statusCode;
		this.message = message;
		this.type = 'error';
	}

	setWarning(statusCode, message) {
		this.statusCode = statusCode;
		this.message = message;
		this.type = 'warning';
	}

	send(res) {
		let result;
		if (this.recordsTotal && this.recordsTotal !== undefined && this.recordsFiltered !== undefined && this.recordsOffset !== undefined && this.searchPanes && Object.keys(this.searchPanes).length) {
			result = {
				status: this.type,
				message: this.message,
				recordsTotal: this.recordsTotal,
				recordsFiltered: this.recordsFiltered,
				recordsOffset: this.recordsOffset,
				searchPanes: this.searchPanes ? this.searchPanes : null,
				data: this.data,
			};
		} else if (this.recordsTotal && this.recordsTotal !== undefined && this.recordsFiltered !== undefined && this.recordsOffset !== undefined ) {
			result = {
				status: this.type,
				message: this.message,
				recordsTotal: this.recordsTotal,
				recordsFiltered: this.recordsFiltered,
				recordsOffset: this.recordsOffset,
				data: this.data,
			};
		} else if (this.recordsTotal && this.recordsTotal !== undefined && this.recordsFiltered !== undefined ) {
			result = {
				status: this.type,
				message: this.message,
				recordsTotal: this.recordsTotal,
				recordsFiltered: this.recordsFiltered,
				data: this.data,
			};
		} else if (this.recordsTotal && this.recordsTotal !== undefined) {
			result = {
				status: this.type,
				message: this.message,
				recordsTotal: this.recordsTotal,
				data: this.data,
			};
		} else {
			result = {
				status: this.type,
				message: this.message,
				data: this.data,
			};
		}

		if (this.type === 'success') {
			return res.status(this.statusCode).json(result);
		}
		return res.status(this.statusCode).json({
			status: this.type,
			message: this.message,
		});
	}


	isChar(str) {
		if (typeof str == 'string') {
			return true;
		}
		return false;
	}

	isVarchar(str) {
		if (typeof str == 'string') {
			return true;
		}
		return false;
	}

	isText(str) {
		if (typeof str == 'string') {
			return true;
		}
		return false;
	}
	isString(str) {
		if (typeof str == 'string') {
			return true;
		}
		return false;
	}

	isNumber(n) {
		return !isNaN(parseFloat(n)) && !isNaN(n - 0)
	}

	isDate(str) {
		if (typeof str == 'object') {
			return true;
		}
		return false;
	}

	isDatetime(str) {
		if (typeof str == 'object') {
			return true;
		}
		return false;
	}

	isTinyint(str) {
		if (typeof str == 'number') {
			return true;
		}
		return false;
	}

	isBigint(str) {
		if (typeof str == 'number') {
			return true;
		}
		return false;
	}

	isFloat(str) {
		if (typeof str == 'number') {
			return true;
		}
		return false;
	}

	isBlob(str) {
		if (typeof str == 'string') {
			return true;
		}
		return false;
	}

	isBoolean(str) {
		if (typeof str == 'boolean') {
			return true;
		}
		return false;
	}

	Char(str) {
		if (typeof str == 'string') {
			return str;
		}
		return null;
	}

	Varchar(str) {
		if (typeof str == 'string') {
			return str;
		}
		return null;
	}

	Text(str) {
		if (typeof str == 'string') {
			return str;
		}
		return null;
	}

	String(str) {
		if (typeof str == 'string') {
			return str;
		}
		return null;
	}

	Date(str) {
		if (typeof str == 'object') {
			return str;
		}
		return null;
	}

	Datetime(str) {
		if (typeof str == 'object') {
			return str;
		}
		return null;
	}

	Tinyint(str) {
		if (typeof str == 'number') {
			return str;
		}
		return null;
	}
	Bigint(str) {
		if (typeof str == 'number') {
			return str;
		}
		return null;
	}

	Float(str) {
		if (typeof str == 'number') {
			return str;
		}
		return null;
	}

	Blob(str) {
		if (typeof str == 'string') {
			return str;
		}
		return null;
	}

	Boolean(str) {
		if (typeof str == 'boolean') {
			return str;
		}
		return null;
	}

	asArray(obj, props = {}) {
		try {
			let array = [];
			if(obj) {
				if (obj.length) {
					return obj;
				}
				if(typeof obj == 'object') {
					let keys = Object.keys(obj);
					let values = Object.values(obj);
					keys.forEach((key,index) => {
						if(Object.keys(props).length) {
							if(props.takeColName){
								if(props.as){
									values[index][props.as] = key;
								} else {
									values[index].columnName = key;
								}
							}
						} else {
							values[index].columnName = key;
						}
						if(typeof values[index] == 'object') {
							array.push(values[index]);
						}
					})
				}
				if(array.length) {
					return array;
				}
			}
			return [];
		} catch (e) {
			console.log(e);
		}
	}

	PrimaryKeyTypeIsString(dataType) {
		if (dataType.hasStringAs('char' || dataType.includes('char'))) {
			return true;
		} else if (dataType.hasStringAs('varchar') || dataType.includes('varchar') || dataType.includes('VARCHAR')) {
			return true;
		} else if (dataType.hasStringAs('text' || dataType.includes('text') || dataType.includes('TEXT'))) {
			return true;
		} else if (dataType.hasStringAs('string' || dataType.includes('string') || dataType.includes('STRING'))) {
			return true;
		}
		return false;
	}

	PrimaryKeyTypeIsInteger(dataType) {
		if (dataType.hasStringAs('int' || dataType.includes('int') || dataType.includes('INT'))) {
			return true;
		} else if (dataType.hasStringAs('bigint' || dataType.includes('bigint') || dataType.includes('BIGINT'))) {
			return true;
		} else if (dataType.hasStringAs('tinyint' || dataType.includes('tinyint') || dataType.includes('TINYINT'))) {
			return true;
		} else if (dataType.hasStringAs('integer' || dataType.includes('integer') || dataType.includes('INTEGER'))) {
			return true;
		}
		return false;
	}

	getOrderByColumnDirection(attributes,body) {
		let column, dir;
		if (body) {
			if (attributes) {
				let fields = Object.keys(attributes);
				if (fields && fields.length) {
					let orderIndex, orderName;
					for(let i = 0 ; i < fields.length ; i++) {
						let field = fields[i];
						if (body[`order[${i}][column]`]) {
							orderIndex = body[`order[${i}][column]`];
							orderName = body[`columns[${orderIndex}][data]`];
							if (orderName) {
								if (body[`order[${i}][dir]`]) {
									dir = body[`order[${i}][dir]`];
								}
								if (orderName && orderName.indexOf('.') >= 0) {
									column = orderName.split('.')[1];
								} else {
									column = orderName;
								}
								break;
							}
						}
					}
				}
			}
		}

		return [column, dir];
	}

	getSearchableFields(rawAttributes, body, query, dtColumns,searchPanes = null, dtSearches=null) {
		try {
			let fields, rawNames, objWhere;
			let { where } = query;
			let { root } = query;
			if (body) {
				if (rawAttributes) {
					fields = Object.keys(rawAttributes);
					if (fields && fields.length) {
						// get Ready
						let columns = [];
						let search = body['search[value]'];
						objWhere = where ? this.isJson(where) ? where : JSON.parse(where) : {};

						if (search) {
							objWhere.$or = [];
							for (let i = 0 ; i < fields.length; i++ ) {
								if (body[`columns[${i}][data]`]) {
									let columnData,columnSearchable,columnSearchValue;
									columnData = body[`columns[${i}][data]`];
									columnSearchable = body[`columns[${i}][searchable]`];
									columnSearchValue = body[`columns[${i}][search][value]`];
									if (columnData && columnData.indexOf('.') >= 0) {
										columnData = columnData.split('.')[1];
									}
									columns.push({data:columnData, searchable:columnSearchable, searchValue:columnSearchValue});
								}
							}
						}

						if (dtSearches && dtSearches.length) {
							objWhere.$and = [];
							for (let j = 0 ; j < dtSearches.length ; j++) {
								let searchPane = dtSearches[j];
								if (fields.find(param => param == searchPane.field)) {
									let [optNumber,optDate] = this.setDataValueTypes(searchPane.value);
									objWhere.$and[j] = {};
									objWhere.$and[j][searchPane.field] = {};
									objWhere.$and[j][searchPane.field].$like = optDate && optDate.getDate() ? optDate : optNumber ? optNumber : `%${searchPane.value}%`;
								}
							}
						}

						for (let i = 0 ; i < columns.length ; i++) {
							let column = columns[i];
							if (column.searchable && fields.find(param => param == column.data)) {
								objWhere.$or[i] = {};
								objWhere.$or[i][column.data] = {};
								objWhere.$or[i][column.data].$like = `%${search}%`;
							}
						}
					}
				}
			}
			return [objWhere];
		} catch (e) {
			console.log(e)
		}
	}

	isJson(json) {
		try {
			JSON.parse(json);
		} catch (e) {
			return true;
		}
		return false;
	}

	fixQuery(query) {
		return null;
	}

	fixWhere(where) {
		return null;
	}

	setFilterFields(objLeads) {
		let objFilterFields = [];
		if (objLeads.length) {
			for (let i = 0 ; i < objLeads.length ; i++) {
				let objLead = objLeads[i];
				let leadFields = Object.keys(objLead.dataValues);
				if (leadFields.length) {
					for (let j = 0 ; j < leadFields.length ; j++) {
						let leadField = leadFields[j];
					}
				}
				objFilterFields.push()
			}
		}
		return objLeads;
	}

	setRoot(objs, root) {
		let rootObjs = {};
		if (root) {
			if (objs && root && objs.rows) {
				rootObjs['rows'] = [];
				rootObjs['count'] = objs.count;
				for (let i = 0 ; i < objs.rows.length ; i++ ) {
					let row = objs.rows[i];
					let obj = {};
					obj[root] = row;
					rootObjs['rows'].push(obj);
				}
			}
			return rootObjs
		}
		return objs;
	}

	// setSearchPanes(aData, aColumns) {
	// 	try {
	// 		let searchPanes = {};
	// 		if (aData && aData.length) {
	// 			for (let i = 0 ; i < aData.length ; i++) {
	// 				let data = aData[i];
	// 				let keys = Object.keys(data);
	// 				let key = keys && keys.length ? keys[0] : null;
	// 				let dataFields = data && data.dataValues ? Object.keys(data.dataValues) : key && data[key] ? Object.keys(data[key].dataValues) : data;
	// 				if (dataFields && dataFields.length) {
	// 					for (let j = 0 ; j < dataFields.length ; j++) {
	// 						let dataField = dataFields[j];
	// 						for (let k = 0 ; k < aColumns.length ; k++) {
	// 							let column = aColumns[k];
	// 							if (column.data == dataField) {
	// 								searchPanes[`${key}.${dataField}`] = [];
	// 								searchPanes[`${key}.${dataField}`].push({
	// 									label:data && data.dataValues ? data.dataValues[dataField] : key && data && data[key] && data[key].dataValues ? data[key].dataValues[dataField] : null,
	// 									value:data && data.dataValues ? data.dataValues[dataField] : key && data && data[key] && data[key].dataValues ? data[key].dataValues[dataField] : null,
	// 									total:1,
	// 									count:1
	// 								});
	// 							}
	// 						}
	// 					}
	// 				}
	// 			}
	// 		}
	// 		return searchPanes;
	// 	} catch (e) {
	// 		console.log(e);
	// 	}
	// }

	setDataValueTypes(dtValue) {
		let optDate, optNumber;
		switch (typeof dtValue) {
			case 'number':
				optNumber = dtValue;
				break;
			case 'object':
				optDate = new Date(dtValue);
				if (!optDate.getDate()){
					optDate = null;
				}
				break;
			case 'string':
				let date = new Date(dtValue);
				if (date.getDate()) {
					optDate = date;
				} else {
					optDate = null;
				}
				break;
		}
		return [optNumber,optDate]
	}

	getDatatableColumns(rawAttributes, body) {
		try {
			let columns = [], fields;
			if (body) {
				if (rawAttributes) {
					fields = Object.keys(rawAttributes);
					if (fields && fields.length) {
						// get Ready
						for (let i = 0 ; i < fields.length; i++ ) {
							if (body[`columns[${i}][data]`]) {
								let columnData,columnSearchable,columnSearchValue;
								columnData = body[`columns[${i}][data]`];
								columnSearchable = body[`columns[${i}][searchable]`];
								columnSearchValue = body[`columns[${i}][search][value]`];
								if (columnData && columnData.indexOf('.') >= 0) {
									columnData = columnData.split('.')[1];
								}
								columns.push({data:columnData, searchable:columnSearchable, searchValue:columnSearchValue});
							}
						}
					}
				}
			}
			return [columns];
		} catch (e) {
			console.log(e)
		}
	}

	static formatNumber(num,decimals = 2) {
		let stringFloat = num + "";
		let arraySplitFloat = stringFloat.split(",");
		let decimalsValue = "0";
		if (arraySplitFloat.length > 1) {
			decimalsValue = arraySplitFloat[1].slice(0, decimals);
		}
		let integerValue = arraySplitFloat[0];
		let arrayFullStringValue = [integerValue, decimalsValue];
		let FullStringValue = arrayFullStringValue.join(".");
		let floatFullValue = parseFloat(FullStringValue);
		let formatFloatFullValue = new Intl.NumberFormat('es-ES', { minimumFractionDigits: decimals }).format(floatFullValue);
		return formatFloatFullValue;
	}

	static intersectArrays(arr1, arr2) {
		arr1 = Array.isArray(first) ? first : first.split(' ');
		arr2 = Array.isArray(second) ? second : second.split(' ');
		const res = [];
		const { length: len1 } = arr1;
		const { length: len2 } = arr2;
		const smaller = (len1 < len2 ? arr1 : arr2).slice();
		const bigger = (len1 >= len2 ? arr1 : arr2).slice();
		for(let i = 0; i < smaller.length; i++) {
			if(bigger.indexOf(smaller[i]) !== -1) {
				res.push(smaller[i]);
				bigger.splice(bigger.indexOf(smaller[i]), 1, undefined);
			}
		}
		return res;
	}

	static compareStrings(first, second) {
		first = this.removeSpecialChars(first);
		second = this.removeSpecialChars(second);
		let aFirst = first.split(' ').filter(param => param);
		let aSecond = second.split(' ').filter(param => param);
		let common = this.intersectArrays(aFirst,aSecond)
		console.log(common);
		return common.length ? common : null;
	}

	static removeSpecialChars(str) {
		var lower = str.toLowerCase();
		var upper = str.toUpperCase();
		var res = "";
		for(var i=0; i<lower.length; ++i) {
			if(lower[i] != upper[i] || lower[i].trim() === ''|| lower[i].trim() === ' ')
				res += str[i];
		}
		return res;
	}

}

module.exports = Util;
