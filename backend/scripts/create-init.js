const env = require('../config/env');
const Sequelize = require('sequelize')
const sequelize = new Sequelize(env.database, env.username, env.password, {
    host: env.host,
    dialect: env.dialect,
    operatorsAliases: false,
    native: true,
    logging: true,
    define: {
        freezeTableName: true
    },
    pool: {
        max: env.max,
        min: env.pool.min,
        acquire: env.pool.acquire,
        idle: env.pool.idle
    }
});

sequelize.authenticate()
    .then(() => {
        console.log('Conectado')
    })
    .catch(err => {
        console.log('No se conecto')
    });

// TABLA CLIENTE
// const usuarios = require('../app/modelos/usuario')(sequelize, Sequelize);
// //usuarios.sync({ force: true }).then(() => { console.log('Creada la tabla usuarios'); return true;});

// const datos_obs = require('../modelos/datos-obs')(sequelize, Sequelize);
// //datos_obs.sync({ force: true }).then(() => { console.log('Creada la tabla'); return true;});

// const log_message = require('../modelos/log-message')(sequelize, Sequelize);
// //log_message.sync({ force: true }).then(() => { console.log('Creada la tabla log_message'); return true;});

// const datos_solicitud = require('../modelos/datos_solicitud')(sequelize, Sequelize);
// //datos_solicitud.sync({ force: true }).then(() => { console.log('Creada la tabla datos_solicitud'); return true;});

// const datos_deudor = require('../modelos/datos_deudor')(sequelize, Sequelize);
// //datos_deudor.sync({ force: true }).then(() => { console.log('Creada la tabla deudor'); return true;});

// const datos_codeudor = require('../modelos/datos_codeudor')(sequelize, Sequelize);
// //datos_codeudor.sync({ force: true }).then(() => { console.log('Creada la tabla 3'); return true;});

// const operacion_propuesta = require('../modelos/operacion_propuesta')(sequelize, Sequelize);
// //operacion_propuesta.sync({ force: true }).then(() => { console.log('Creada la tabla operaciones'); return true;});

// const garantia_propuesta = require('../modelos/garantia_propuesta')(sequelize, Sequelize);
// //garantia_propuesta.sync({ force: true }).then(() => { console.log('Creada la tabla garantias'); return true;});

// const estados_solicitud = require('../modelos/estados_solicitud')(sequelize, Sequelize);
// //estados_solicitud.sync({ force: true }).then(() => { console.log('Creada la tabla estados_solicitud'); return true;});

// const datos_mensajes = require('../modelos/datos_mensajes')(sequelize, Sequelize);
// //datos_mensajes.sync({ force: true }).then(() => { console.log('Creada la tabla datos_mensajes'); return true;});

// const validacion_datos = require('../modelos/validacion_datos')(sequelize, Sequelize);
// //validacion_datos.sync({ force: true }).then(() => { console.log('Creada la tabla validacion_datos'); return true;});

// const parametros_campos = require('../modelos/parametros_campos')(sequelize, Sequelize);
// // parametros_campos.sync({ force: true }).then(() => { console.log('Creada la tabla parametros_campos'); return true;});

// const parametros = require('../modelos/parametro')(sequelize, Sequelize);
// // parametros.sync({ force: true }).then(() => { console.log('Creada la tabla parametros'); return true;});

// const datos_observados = require('../modelos/datos_observados')(sequelize,Sequelize);
// // datos_observados.sync({ force: true }).then(() => { console.log('Creada la tabla datos_observados'); return true;});

const cargoxx = require('../modelos/usuario')(sequelize,Sequelize);
cargoxx.sync({ force: true }).then    (() => { console.log('Creada la tabla usuario'); return true;});


//const archivos_adjuntos = require('../modelos/archivos-adjuntos')(sequelize,Sequelize);
// archivos_adjuntos.sync({ force: true }).then(() => { console.log('Creada la tabla archivos adjuntos'); return true;});

module.exports = sequelize;








