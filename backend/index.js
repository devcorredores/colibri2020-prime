var express = require("express");
var session = require("express-session");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var morgan = require("morgan");
var app = express();
var passport = require("passport");
var modelos = require("./modelos");
var winston = require("./modulos/winston");
var os = require("os");
var hostname = os.hostname();

// const logger = winston.logger;
const nets = os.networkInterfaces();
const loggerError = winston.loggerError;
const path = require("path");
const fs = require("fs");
const http = require("http");
const env = require("./config/env");
const initCron = require("./modulos/cron.js");
const ipfilter = require("express-ipfilter").IpFilter;
const results = Object.create(null); // Or just '{}', an empty object

for (const name of Object.keys(nets)) {
  for (const net of nets[name]) {
    // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
    // 'IPv4' is in Node <= 17, from 18 it's a number 4 or 6
    const familyV4Value = typeof net.family === 'string' ? 'IPv4' : 4
    if (net.family === familyV4Value && !net.internal) {
      if (!results[name]) {
        results[name] = [];
      }
      results[name].push(net.address);
    }
  }
}


app.use(bodyParser.json({ limit: "10mb" }));
app.use(express.static(path.join(__dirname, "public")));
app.set("port", env.PORT || 7001);
app.set("ports", env.PORTSSL || 7000);
app.set("hostname", hostname);

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, X-AUTHENTICATION, X-IP, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  next();
});
// app.use(morgan("combined", { stream: logger.stream })); // log every request to the console
app.use(morgan("combined", { stream: loggerError.stream.write })); // log every request to the console

app.use(cookieParser()); // read cookies (needed for auth)
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(session({ secret: "anything", resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json());

initCron(app);
// routes ======================================================================

//require('./rutas/usuario-ns-ruta')(app, passport); // load our routes and pass in our app and fully configured passport

app.use("/api-corredores-ecofuturo/usuario", require("./rutas/usuario-ruta"));
app.use("/api-corredores-ecofuturo/soap-ui", require("./rutas/soapui-ruta"));
app.use("/api-corredores-ecofuturo/persona", require("./rutas/persona-ruta"));
app.use("/api-corredores-ecofuturo/beneficiario", require("./rutas/beneficiario-ruta"));
app.use("/api-corredores-ecofuturo/parametro", require("./rutas/parametro-ruta"));
app.use("/api-corredores-ecofuturo/rol", require("./rutas/rol-ruta"));
app.use("/api-corredores-ecofuturo/autenticacion", require("./rutas/autenticacion-ruta"));
app.use("/api-corredores-ecofuturo/sendmail", require("./rutas/send-mail.ruta"));
app.use("/api-corredores-ecofuturo/admin-permisos", require("./rutas/admin-permisos-ruta"));
app.use("/api-corredores-ecofuturo/consumo-servicios", require("./rutas/soapui-ruta"));
app.use("/api-corredores-ecofuturo/atributo", require("./rutas/atributo-ruta"));
app.use("/api-corredores-ecofuturo/instancia_poliza", require("./rutas/instancia_poliza-ruta"));
app.use("/api-corredores-ecofuturo/instancia_documento", require("./rutas/instancia_documento-ruta"));
app.use("/api-corredores-ecofuturo/instancia_poliza_transicion", require("./rutas/instancia_poliza_transicion-ruta"));
app.use("/api-corredores-ecofuturo/documento", require("./rutas/documento-ruta"));
app.use("/api-corredores-ecofuturo/poliza", require("./rutas/poliza-ruta"));
app.use("/api-corredores-ecofuturo/reporte", require("./rutas/reportes-ruta"));
app.use("/api-corredores-ecofuturo/objeto_atributo", require("./rutas/objeto_atributo-ruta"));
app.use("/api-corredores-ecofuturo/anexo", require("./rutas/anexo-ruta"));
app.use("/api-corredores-ecofuturo/instancia_anexo", require("./rutas/instancia_anexo-ruta"));
app.use("/api-corredores-ecofuturo/instancia_anexo_asegurado", require("./rutas/instancia_anexo_asegurado"));
app.use("/api-corredores-ecofuturo/tmp_CI_CODAGENDA", require("./rutas/tmp_CI_CODAGENDA-ruta"));
app.use("/api-corredores-ecofuturo/tmp_reportes", require("./rutas/tmp_reportes-ruta"));
app.use("/api-corredores-ecofuturo/tmp_cliente_cuenta", require("./rutas/tmp_cliente_cuenta-ruta"));
app.use("/api-corredores-ecofuturo/reporte_query", require("./rutas/reporte_query_ruta"));
app.use("/api-corredores-ecofuturo/plan_pago", require("./rutas/plan_pago-ruta"));
app.use("/api-corredores-ecofuturo/tmp_envio", require("./rutas/tmp_envio-ruta"));
app.use("/api-corredores-ecofuturo/vigencia", require("./rutas/vigencia-ruta"));
app.use("/api-corredores-ecofuturo/file", require("./rutas/file-ruta"));
app.use("/api-corredores-ecofuturo/archivo", require("./rutas/archivo-ruta"));


// Blacklist the following IPs
//const ips = ["200.87.174.130"];

// Create the server
//app.use(ipfilter(ips));

// starting server https
if (hostname == 'CorredoresEco') {
  const https = require("https");
  const https_options = {
    key: fs.readFileSync(path.join(__dirname, "sslcert/seguroscolibri.key"), "utf8"),
    cert: fs.readFileSync(path.join(__dirname, "sslcert/seguroscolibri.crt"),"utf8"),
  };
  const httpsServer = https.createServer(https_options, app);
  httpsServer.listen(app.get("ports"), () => {
    console.log(` ${new Date()} : SERVER NODEJS-EXPRESS Local v.09.01.20 [https] escuchando en el puerto ${app.get("ports")}`);
  });
} else {
  const httpServer = http.createServer({}, app);
  httpServer.listen(app.get("port"), () => {
    console.log(` ${new Date()} : SERVER NODEJS-EXPRESS Local v.09.01.20 [http] escuchando en el puerto ${app.get("port")}`);
  });
}



// var util = require("util");
// var log_file = fs.createWriteStream(__dirname + "/logs/app-debug.log", {
//   flags: "w",
// });
// var log_stdout = process.stdout;

// console.log = function (d) {
//   log_file.write(util.format(d) + "\n");
//   log_stdout.write(util.format(d) + "\n");
// };
