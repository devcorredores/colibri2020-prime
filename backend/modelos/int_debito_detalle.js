module.exports = (sequelize, Sequelize) => {
    const int_debito_detalle = sequelize.define('int_debito_detalle', {
        fuente:Sequelize.STRING,
        Num_Carga:Sequelize.STRING,
        Cod_Agenda:Sequelize.STRING,
        Cuenta:Sequelize.STRING,
        Monto:Sequelize.STRING,
        Moneda:Sequelize.STRING,
        Cod_Prod:Sequelize.STRING,
        No_Pol:Sequelize.STRING,
        NTra:Sequelize.STRING,
        Estado:Sequelize.STRING,
        Motivo:Sequelize.STRING,
        Saldo:Sequelize.STRING,
    },{
        timestamps: false
    });
    return int_debito_detalle;
}
