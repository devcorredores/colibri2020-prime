module.exports = (sequelize, Sequelize) => {

    const instancia_poliza_transicion = sequelize.define('instancia_poliza_transicion', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_instancia_poliza: {
            type: Sequelize.BIGINT,
            references: {
               model: 'instancia_poliza', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        par_estado_id: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
        par_observacion_id: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         observacion:Sequelize.STRING,
         adicionado_por:Sequelize.STRING,
         modificado_por:Sequelize.STRING
    });
    return instancia_poliza_transicion;
}
