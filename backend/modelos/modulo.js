module.exports = (sequelize, Sequelize) => {
    const modulo = sequelize.define('modulo', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_modulo_super: {
            type: Sequelize.BIGINT,
            references: {
               model: 'modulo', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
        codigo: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        abreviacion: Sequelize.STRING,
        icon: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return modulo;
}

