// const crypto = require('crypto');

module.exports = (sequelize, Sequelize) => {
    const Parametro = sequelize.define('parametro', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        parametro_cod: Sequelize.STRING(50),
        parametro_descripcion: Sequelize.STRING(150),
        parametro_abreviacion: Sequelize.STRING(15),
        diccionario_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'diccionario',
                key: 'id'
            },
        },
        id_padre: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        orden: Sequelize.BIGINT
    });
    return Parametro;
}
