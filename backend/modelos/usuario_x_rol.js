module.exports = (sequelize, Sequelize) => {
    const usuario_x_rol = sequelize.define('usuario_x_rol', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_usuario: {
            type: Sequelize.BIGINT,
            references: {
               model: 'usuario', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
        id_rol: {
            type: Sequelize.BIGINT,
            references: {
               model: 'rol', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return usuario_x_rol;
}
