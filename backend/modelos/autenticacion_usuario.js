// const crypto = require('crypto');

module.exports = (sequelize, Sequelize) => {
    const autenticacion_usuario = sequelize.define('autenticacion_usuario', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        usuario_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'usuario', 
                key: 'id'
            }
        },
        par_autenticacion_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', 
                key: 'id', 
            }
        },
        atributo1:Sequelize.STRING,
    });
    return autenticacion_usuario;
}



