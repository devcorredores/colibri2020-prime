module.exports = (sequelize, Sequelize) => {
    const tmp_CI_CUENTA = sequelize.define('tmp_CI_CUENTA', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        cod_agenda:Sequelize.STRING,
        manejo:Sequelize.INTEGER,
        nrocuenta:Sequelize.STRING,
        moneda:Sequelize.INTEGER,
        moneda_abbr:Sequelize.STRING,
    });
    return tmp_CI_CUENTA;
}
