module.exports = (sequelize, Sequelize) => {
    const persona_juridica = sequelize.define('persona_juridica', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        codigo: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        numid_fiscal: Sequelize.STRING,
        direccion: Sequelize.STRING,
        telefono: Sequelize.STRING,
        celular: Sequelize.STRING,
        adicionada_por: Sequelize.STRING,
        modificada_por:Sequelize.STRING
    });
    return persona_juridica;
}
