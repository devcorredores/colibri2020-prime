module.exports = (sequelize, Sequelize) => {
    const atributo_instancia_anexo_asegurado = sequelize.define('atributo_instancia_anexo_asegurado', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_instancia_anexo_asegurado: {
            type: Sequelize.BIGINT,
            references: {
                model: 'instancia_anexo_asegurado', 
                key: 'id'
            }
        },
        id_objeto_x_atributo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'objeto_x_atributo', 
                key: 'id'
            }
        },
        valor:Sequelize.STRING,
        tipo_error:Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return atributo_instancia_anexo_asegurado;
}
