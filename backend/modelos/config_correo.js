module.exports = (sequelize, Sequelize) => {
    const config_correo = sequelize.define('config_correo', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        para:Sequelize.STRING,
        asunto:Sequelize.STRING,
        texto:Sequelize.STRING,
        texto_vacio:Sequelize.STRING,
        html:Sequelize.BOOLEAN,
        con_copia:Sequelize.STRING,
        con_copia_oculta: Sequelize.STRING,
        nombre_archivo_adjunto: Sequelize.STRING,
        nombre_remitente: Sequelize.STRING,
        correo_remitente: Sequelize.STRING,
        contraseña_remitente: Sequelize.STRING,
        codigo:Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
        port: Sequelize.STRING,
        host: Sequelize.STRING,
    });
    return config_correo;
}
