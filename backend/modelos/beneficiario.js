module.exports = (sequelize, Sequelize) => {

    const beneficiario = sequelize.define('beneficiario', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_asegurado: {
            type: Sequelize.BIGINT,
            references: {
               model: 'asegurado', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_beneficiario: {
            type: Sequelize.BIGINT,
            references: {
               model: 'entidad', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         id_parentesco: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         tipo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         estado: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         condicion: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         porcentaje:Sequelize.STRING,
         fecha_declaracion:Sequelize.DATE,
         descripcion_otro:Sequelize.STRING,
         adicionado_por:Sequelize.STRING,
         modificado_por:Sequelize.STRING,
    });
    return beneficiario;
}
