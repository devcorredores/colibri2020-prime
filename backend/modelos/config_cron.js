module.exports = (sequelize, Sequelize) => {
    const config_cron = sequelize.define('config_cron', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        codigo:Sequelize.STRING,
        nombre:Sequelize.STRING,
        expresion:Sequelize.STRING,
        estado:Sequelize.BOOLEAN,
        descripcion:Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
    });
    return config_cron;
}
