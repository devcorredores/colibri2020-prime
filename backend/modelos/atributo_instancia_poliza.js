module.exports = (sequelize, Sequelize) => {

    const atributo_instancia_poliza = sequelize.define('atributo_instancia_poliza', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_instancia_poliza: {
            type: Sequelize.BIGINT,
            references: {
               model: 'instancia_poliza', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_objeto_x_atributo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'objeto_x_atributo', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         valor:Sequelize.STRING,
         tipo_error:Sequelize.STRING,
         adicionado_por:Sequelize.STRING,
         modificado_por:Sequelize.STRING
    });
    return atributo_instancia_poliza;
}
