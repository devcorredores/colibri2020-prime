// const Sequelize = require('sequelize');
// const env = require('../config/env');
var dbconfig = require("../../config/env2");
const Sequelize = require("sequelize");

var sequelize = new Sequelize(
  dbconfig.database,
  dbconfig.username,
  dbconfig.password,
  {
    host: dbconfig.host,
    dialect: dbconfig.dialect,
    dialectOptions: {
      encrypt: true,
      requestTimeout: 9000000,
      packetSize: 102768,
    },
    operatorsAliases: true,
    native: true,
    logging: true,
    define: {
      freezeTableName: true,
    },
    pool: {
      max: dbconfig.pool.max,
      min: dbconfig.pool.min,
      idleTimeoutMillis: 90000,
      acquire: dbconfig.pool.acquire,
      idle: dbconfig.pool.idle,
    },
    options: {
      encrypt: true,
      enableArithAbort: true,
    },
    timezone: "00:00",
  }
);

const db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.seg_deudores = require("./seg_deudores")(sequelize, Sequelize);
db.seg_solicitudes = require("./seg_solicitudes")(sequelize, Sequelize);
db.seg_estados = require("./seg_estados")(sequelize, Sequelize);
db.seg_adicionales = require("./seg_adicionales")(sequelize, Sequelize);
db.seg_aprobaciones = require("./seg_aprobaciones")(sequelize, Sequelize);
db.seg_beneficiarios = require("./seg_beneficiarios")(sequelize, Sequelize);

// Relations

db.seg_deudores.belongsTo(db.seg_solicitudes, {foreignKey: "Deu_IdSol", targetKey: "Sol_IdSol", as: "sol_deudor"});
db.seg_solicitudes.hasMany(db.seg_deudores, {foreignKey: "Deu_IdSol", sourceKey: "Sol_IdSol", as: "sol_deudor"});

db.seg_deudores.belongsTo(db.seg_solicitudes, {foreignKey: "Deu_IdSol", targetKey: "Sol_IdSol", as: "deu_solicitud"});
db.seg_solicitudes.hasMany(db.seg_deudores, {foreignKey: "Deu_IdSol", sourceKey: "Sol_IdSol", as: "deu_solicitud"});

// db.seg_solicitudes.belongsTo(db.seg_deudores, {foreignKey: "Sol_IdSol", targetKey: "Deu_IdSol", as: "deu_solicitud"});
// db.seg_deudores.hasMany(db.seg_solicitudes, {foreignKey: "Sol_IdSol", targetKey: "Deu_IdSol", as: "deu_solicitud"});

db.seg_adicionales.belongsTo(db.seg_solicitudes, {foreignKey: "Adic_IdSol", targetKey: "Sol_IdSol", as: "sol_adicional"});
db.seg_solicitudes.hasMany(db.seg_adicionales, {foreignKey: "Adic_IdSol", sourceKey: "Sol_IdSol", as: "sol_adicional"});

db.seg_adicionales.belongsTo(db.seg_deudores, {foreignKey: "Adic_IdDeu", targetKey: "Deu_Id", as: "deu_adicional"});
db.seg_deudores.hasMany(db.seg_adicionales, {foreignKey: "Adic_IdDeu", sourceKey: "Deu_Id", as: "deu_adicional"});

db.seg_beneficiarios.belongsTo(db.seg_deudores, {foreignKey: "Ben_IdDeu", targetKey: "Deu_Id", as: "deu_beneficiarios"});
db.seg_deudores.hasMany(db.seg_beneficiarios, {foreignKey: "Ben_IdDeu", sourceKey: "Deu_Id", as: "deu_beneficiarios"});

db.seg_aprobaciones.belongsTo(db.seg_solicitudes, {foreignKey: "Apr_IdSol", targetKey: "Sol_IdSol", as: "sol_aprobacion"});
db.seg_solicitudes.hasMany(db.seg_aprobaciones, {foreignKey: "Apr_IdSol", sourceKey: "Sol_IdSol", as: "sol_aprobacion"});

db.seg_aprobaciones.belongsTo(db.seg_deudores, {foreignKey: "Apr_IdDeu", targetKey: "Deu_Id", as: "deu_aprobacion"});
db.seg_deudores.hasMany(db.seg_aprobaciones, {foreignKey: "Apr_IdDeu", sourceKey: "Deu_Id", as: "deu_aprobacion"});

//archivo

module.exports = db;
