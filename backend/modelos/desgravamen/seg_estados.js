module.exports = (sequelize, Sequelize) => {
  const seg_estados = sequelize.define('seg_estados', {
    Est_Codigo:{
      primaryKey: true,
      type: Sequelize.STRING,
      autoIncrement: true
    },
    Est_Descripcion: Sequelize.STRING,
    Est_Cerrado: Sequelize.STRING,
    Est_Grupo: Sequelize.STRING,
    Est_Visible: Sequelize.STRING,
    Est_Orden: Sequelize.INTEGER,
  },{
    tableName:'Seg_Estados',
    timestamps:false
  });
  return seg_estados;
}
