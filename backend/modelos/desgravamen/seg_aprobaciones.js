module.exports = (sequelize, Sequelize) => {
  const Seg_Aprobaciones = sequelize.define('seg_aprobaciones', {
    Apr_IdSol:{
      primaryKey: true,
      type: Sequelize.INTEGER,
      autoIncrement: true
    },
    Apr_IdDeu: Sequelize.INTEGER,
    Apr_Aprobado: Sequelize.STRING,
    Apr_ExtraPrima: Sequelize.STRING,
    Apr_Condiciones: Sequelize.STRING
  },{
    tableName:'Seg_Aprobaciones',
    timestamps:false
  });
  return Seg_Aprobaciones;
}
