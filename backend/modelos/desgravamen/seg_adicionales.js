module.exports = (sequelize, Sequelize) => {
  const Seg_Adicionales = sequelize.define('seg_adicionales', {
    Adic_Id:{
      primaryKey: true,
      type: Sequelize.INTEGER,
      autoIncrement: true
    },
    Adic_IdSol: Sequelize.INTEGER,
    Adic_IdDeu: Sequelize.STRING,
    Adic_Pregunta: Sequelize.STRING,
    Adic_Texto: Sequelize.STRING,
    Adic_Opciones: Sequelize.STRING,
    Adic_Respuesta: Sequelize.STRING,
    Adic_Comment: Sequelize.STRING
  },{
    tableName:'Seg_Adicionales',
    timestamps:false
  });
  return Seg_Adicionales;
}
