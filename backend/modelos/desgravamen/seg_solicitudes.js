module.exports = (sequelize, Sequelize) => {
  const Seg_Solicitudes = sequelize.define('seg_solicitudes', {
    Sol_IdSol: {
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    Sol_Eeff: Sequelize.STRING,
    Sol_NumSol: Sequelize.STRING,
    Sol_FechaSol: Sequelize.DATEONLY,
    Sol_TipoCred: Sequelize.STRING,
    Sol_TipoAsfi: Sequelize.STRING,
    Sol_TipoCli: Sequelize.STRING,
    Sol_Sucursal: Sequelize.STRING,
    Sol_Agencia: Sequelize.STRING,
    Sol_Ciudad: Sequelize.STRING,
    Sol_Oficial: Sequelize.STRING,
    Sol_Digitador: Sequelize.STRING,
    Sol_MontoSol: Sequelize.DECIMAL(18,4),
    Sol_MonedaSol: Sequelize.STRING,
    Sol_PlazoSol: Sequelize.STRING,
    Sol_FrecPagoSol: Sequelize.STRING,
    Sol_CantObl: Sequelize.STRING,
    Sol_TipoOpeUni: Sequelize.STRING,
    Sol_TipoOpeAdic: Sequelize.STRING,
    Sol_TipoOpeLC: Sequelize.STRING,
    Sol_NumLC: Sequelize.STRING,
    Sol_MonedaLC: Sequelize.STRING,
    Sol_FechaLC: Sequelize.STRING,
    Sol_MontoActAcum: Sequelize.STRING,
    Sol_TipRelASFI: Sequelize.STRING,
    Sol_TipoSeg: Sequelize.STRING,
    Sol_Aseg: Sequelize.STRING,
    Sol_PrimaSeg: Sequelize.STRING,
    Sol_SDH: Sequelize.STRING,
    Sol_TasaSeg: Sequelize.STRING,
    Sol_EstadoSol: Sequelize.STRING,
    Sol_Poliza: Sequelize.STRING,
    Sol_ExtraPrima: Sequelize.DECIMAL(18,4),
    Sol_MontoSolSus: Sequelize.STRING,
    Ope_TipoGtia: Sequelize.STRING,
    Ope_Destino: Sequelize.STRING,
    Ope_TasaSeg: Sequelize.STRING,
    Ope_ImporteSeg: Sequelize.STRING,
    Sol_CodOficial: Sequelize.STRING,
    Sol_LC: Sequelize.STRING,
    Sol_TipoParam: Sequelize.STRING,
    operacion_estado: Sequelize.STRING
  },{
    tableName:'Seg_Solicitudes',
    timestamps:false
  });
  return Seg_Solicitudes;
}
