module.exports = (sequelize, Sequelize) => {
  const Seg_Deudores = sequelize.define('seg_deudores', {
    Ben_Id:{
      primaryKey: true,
      type: Sequelize.INTEGER,
      autoIncrement: true
    },
    Ben_IdSol: Sequelize.INTEGER,
    Ben_IdDeu: Sequelize.INTEGER,
    Ben_Nombre: Sequelize.STRING,
    Ben_Paterno: Sequelize.STRING,
    Ben_Materno: Sequelize.STRING,
    Ben_Casada: Sequelize.STRING,
    Ben_Telefono: Sequelize.STRING,
    Ben_TipoDoc: Sequelize.STRING,
    Ben_NumDoc: Sequelize.STRING,
    Ben_ExtDoc: Sequelize.STRING,
    Ben_CompDoc: Sequelize.STRING,
    Ben_Relacion: Sequelize.STRING,
    Ben_Porcentaje: Sequelize.STRING,
    Ben_Sexo: Sequelize.STRING,
    Ben_OtraRel: Sequelize.STRING,
    Ben_Tipo: Sequelize.STRING
  },{
    tableName:'Seg_Beneficiarios',
    timestamps:false
  });
  return Seg_Deudores;
}
