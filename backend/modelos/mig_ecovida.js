module.exports = (sequelize, Sequelize) => {
    const mig_ecovida = sequelize.define('mig_ecovida', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        nro_certificado:Sequelize.STRING,
        tipo_operacion:Sequelize.STRING,
        producto:Sequelize.STRING,
        cliente_ecofuturo:Sequelize.STRING,
        asegurado:Sequelize.STRING,
        estado_civil:Sequelize.STRING,
        ci:Sequelize.STRING,
        fecha_nac:Sequelize.STRING,
        genero:Sequelize.STRING,
        dir_domicilio:Sequelize.STRING,
        tel_domicilio:Sequelize.STRING,
        tel_movil:Sequelize.STRING,
        prima:Sequelize.INTEGER,
        modalidad_pago:Sequelize.STRING,
        nombre_usuario:Sequelize.STRING,
        fecha_de_ingreso:Sequelize.STRING,
        sucursal:Sequelize.STRING,
        desc_agencia:Sequelize.STRING,
        tipo_estado:Sequelize.STRING,
        emitido:Sequelize.STRING,
        fecha_emision:Sequelize.STRING,
        anulado:Sequelize.STRING,
        fecha_anulado:Sequelize.STRING,
        apellido_paterno:Sequelize.STRING,
        apellido_materno:Sequelize.STRING,
        nombres:Sequelize.STRING,
        apellido_casada:Sequelize.STRING,
        tipo_documento_ID:Sequelize.STRING,
        ci_beneficiario:Sequelize.STRING,
        apellido_paterno_beneficiario:Sequelize.STRING,
        apellido_materno_beneficiario:Sequelize.STRING,
        nombres_beneficiario:Sequelize.STRING,
        parentesco:Sequelize.STRING,
        porcentaje:Sequelize.INTEGER,
        createdAt:Sequelize.DATE,
        updatedAt:Sequelize.DATE,
        id_instancia_poliza:Sequelize.INTEGER,
        cod_usuario_colibri:Sequelize.INTEGER,
        id_persona_beneficiario:Sequelize.INTEGER,
        FEC_INI_VIG_REAL:Sequelize.DATE,
        FEC_FIN_VIG_REAL:Sequelize.DATE,
        cod_agencia:Sequelize.INTEGER,
        cod_sucursal:Sequelize.INTEGER,
        nro_comprobante:Sequelize.STRING,
        nro_solicitud:Sequelize.STRING,


        Codigo_Agenda:Sequelize.STRING,
        Cuenta_Bancaria:Sequelize.STRING,
        Debito_Automatico:Sequelize.STRING,
        Localidad:Sequelize.STRING,
        Departamento:Sequelize.STRING,
        Tipo_Documento:Sequelize.STRING,
        Correo_electronico:Sequelize.STRING,
        Moneda:Sequelize.STRING,
        Zona:Sequelize.STRING,
        Nro_Direccion:Sequelize.STRING,
        Numero_transaccion:Sequelize.STRING,
        Fecha_transaccion:Sequelize.STRING,
        Importe_transaccion:Sequelize.STRING,
        moneda_transaccion:Sequelize.STRING,
        Detalle_transaccion:Sequelize.STRING,
        Cargo_oficial:Sequelize.STRING,
        Ciudad_Nacimiento:Sequelize.STRING,
        Ocupacion:Sequelize.STRING,
        Condicion_pep:Sequelize.STRING,
        Cargo_pep:Sequelize.STRING,
        Periodo_pep:Sequelize.STRING,
        Direccion_laboral:Sequelize.STRING,
        Descripcion_ocupacion:Sequelize.STRING,

});
    return mig_ecovida;
}
