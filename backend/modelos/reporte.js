module.exports = (sequelize, Sequelize) => {
    const reporte = sequelize.define('reporte', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        para:Sequelize.STRING,
        asunto:Sequelize.STRING,
        texto:Sequelize.TEXT,
        nombre_tarea:Sequelize.TEXT,
        html:Sequelize.TEXT,
        con_copia:Sequelize.STRING,
        con_copia_oculta:Sequelize.STRING,
        nombre_remitente:Sequelize.STRING,
        fecha_tarea:Sequelize.DATE,
        dia_tarea:Sequelize.STRING,
        hora_tarea:Sequelize.TIME,
        estado_tarea:Sequelize.BOOLEAN,
        nombre_archivo_adjunto: Sequelize.STRING,
        correo_remitente:Sequelize.STRING,
        fecha_inicio_reporte:Sequelize.DATE,
        fecha_fin_reporte:Sequelize.DATE,
        contraseña:Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
    });
    return reporte;
}
