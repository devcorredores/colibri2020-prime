module.exports = (sequelize, Sequelize) => {
    const anexo_poliza = sequelize.define('anexo_poliza', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_poliza: {
            type: Sequelize.BIGINT,
            references: {
                model: 'poliza',
                key: 'id'
            }
        },
        id_tipo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        id_moneda: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        monto_prima: Sequelize.DECIMAL,
        monto_capital: Sequelize.DECIMAL,
        codigo: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        abreviacion: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
        version: Sequelize.BIGINT,
        vigente: Sequelize.BOOLEAN,
    });
    return anexo_poliza;
}
