module.exports = (sequelize, Sequelize) => {
    const usuarioColibri = sequelize.define('usuarioColibri', {
        codusuario:Sequelize.STRING,
        paterno:Sequelize.STRING,
        materno:Sequelize.STRING,
        nombre:Sequelize.STRING,
        correo_electrónico:Sequelize.STRING,
        cargo:Sequelize.STRING,
        sucursal:Sequelize.STRING,
        Agencia:Sequelize.STRING,
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },

    });
    return usuarioColibri;
}
