module.exports = (sequelize, Sequelize) => {
    const documento_version_params = sequelize.define('documento_version_params', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        html: Sequelize.STRING,
        value: Sequelize.STRING,
        nombre: Sequelize.STRING,
        select: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
        pipe: Sequelize.STRING,
        select_value: Sequelize.STRING,
        id_documento_version: {
            type: Sequelize.BIGINT,
            references: {
                model: 'documento_version',
                key: 'id'
            }
        },
        id_reporte_query: {
            type: Sequelize.BIGINT,
            references: {
                model: 'reporte_query',
                key: 'id'
            }
        },
        id_padre: {
            type: Sequelize.BIGINT,
            references: {
                model: 'documento_version_params',
                key: 'id'
            }
        },
    });
    return documento_version_params;
}
