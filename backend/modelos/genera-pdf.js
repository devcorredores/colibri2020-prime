const pdf = require('html-pdf');
const path = require('path');
const fs = require('fs');

formatDate = (param) => {
    const date = new Date(param);
    let month = `${date.getMonth()+1}`;
    let day = `${date.getDate()}`;
    const year = `${date.getFullYear()}`;

    if (month.length < 2) month = `0${month}`;
    if (day.length < 2) day = `0${day}`;

    return [day,month,year].join('-');
  }

generar = (poliza, garantia, datos) => {


    const options = {
        height: '279.4mm',
        width: '215.9mm'
    }
    
    let tipo_garantia=garantia;
    let response = { status: 'OK', message: 'genera-pdf', data: {} };

    return new Promise((resolve, reject) => {

        
        const filename = path.join(__dirname, `../public/pdfs/${garantia}-${solicitud}.pdf`);

        const logo = path.join('file://', __dirname, '../plantillas/alianzamini.jpg');
        const firmas = path.join('file://', __dirname, '../plantillas/firmas.jpg');

        let pagesHtml = '';
        var dt = new Date();
        var mm = dt.getMonth() + 1;
        var dd = dt.getDate();
        var yyyy = dt.getFullYear();
        const fecha = dd + '/' + mm + '/' + yyyy

        const codAsfi = 'HV1,HV2,HV3,HV4,HV5';

        datos.datos_garantia.forEach((garantia,index) => {

            let tipoGarantia = `${tipo_garantia.split('-')[0]}-riesgo`;
        
            const respCod = codAsfi.indexOf(garantia.tipo_garantia_asfi.trim());
            if (respCod !== -1) {
                tipoGarantia = `${tipo_garantia.split('-')[0]}-auto`;
            }
            let plantilla = '';

            switch (tipoGarantia) {
                case 'certificado-riesgo':
                    plantilla = 'todoriesgo.html';
                    break;
                case 'slip-riesgo':
                    // plantilla = 'todoslip.html';
                    plantilla = 'sold-riesgo.html'
                    break;
                case 'certificado-auto':
                    plantilla = 'automotores.html';
                    break
                case 'slip-auto':
                    // plantilla = 'autoslip.html';
                    plantilla = 'sold-auto.html'
                    break
                default:
                    plantilla = 'pagina.html';
            }
            const template = path.join(__dirname, `../plantillas/${plantilla}`);

            let templateHtml = fs.readFileSync(template, 'utf8');
            let titular = `${datos.datos_deudor.nombre} ${datos.datos_deudor.paterno} ${datos.datos_deudor.materno}`;
            let num_ci = `${datos.datos_deudor.numero_documento}`;
            
            templateHtml = templateHtml.replace('{{logo}}', logo);
            templateHtml = templateHtml.replace('{{firmas}}', firmas);
            templateHtml = templateHtml.replace('{{firmasmini}}', firmas);

            templateHtml = templateHtml.replace('{{num_solicitud}}', `${datos.datos_solicitud.num_solicitud}-${index+1}` );
            templateHtml = templateHtml.replace('{{num_poliza}}', `${datos.datos_solicitud.num_solicitud}` );
            templateHtml = templateHtml.replace('{{compania_seguros}}', datos.datos_solicitud.compania_seguros);
            templateHtml = templateHtml.replace('{{fch_afiliacion}}', formatDate(datos.datos_solicitud.fch_afiliacion));
            templateHtml = templateHtml.replace('{{inicio_vigencia}}', formatDate(datos.datos_solicitud.inicio_vigencia));
            templateHtml = templateHtml.replace('{{fin_vigencia}}', formatDate(datos.datos_solicitud.fin_vigencia));

            templateHtml = templateHtml.replace('{{nombre}}', datos.datos_deudor.nombre);
            templateHtml = templateHtml.replace('{{paterno}}', datos.datos_deudor.paterno);
            templateHtml = templateHtml.replace('{{materno}}', datos.datos_deudor.materno);
            templateHtml = templateHtml.replace('{{casada}}', datos.datos_deudor.casada);
            templateHtml = templateHtml.replace('{{telefono_movil}}', datos.datos_deudor.telefono_movil);
            templateHtml = templateHtml.replace('{{numero_documento}}', datos.datos_deudor.numero_documento);
            templateHtml = templateHtml.replace('{{extension_documento}}', datos.datos_deudor.extension_documento);
            templateHtml = templateHtml.replace('{{zona_domicilio}}', datos.datos_deudor.zona_domicilio);
            templateHtml = templateHtml.replace('{{direccion_domicilio}}', datos.datos_deudor.direccion_domicilio);
            templateHtml = templateHtml.replace('{{numero_domicilio}}', datos.datos_deudor.numero_domicilio);
            templateHtml = templateHtml.replace('{{localidad_domicilio}}', datos.datos_deudor.localidad_domicilio);
            templateHtml = templateHtml.replace('{{telefono_domicilio}}', datos.datos_deudor.telefono_domicilio);
            templateHtml = templateHtml.replace('{{telefono_negocio_oficina}}', datos.datos_deudor.telefono_negocio_oficina);
            templateHtml = templateHtml.replace('{{telefono_movil}}', datos.datos_deudor.telefono_movil);
            templateHtml = templateHtml.replace('{{fecha_nacimiento}}', datos.datos_deudor.fecha_nacimiento.substring(0,10));
            templateHtml = templateHtml.replace('{{pais_nacimiento}}', datos.datos_deudor.pais_nacimiento);
            templateHtml = templateHtml.replace('{{estado_civil}}', datos.datos_deudor.estado_civil);
            templateHtml = templateHtml.replace('{{profesion_ocupacion}}', datos.datos_deudor.profesion_ocupacion);
            templateHtml = templateHtml.replace('{{datos_beneficiario}}', datos.datos_deudor.datos_beneficiario);
            templateHtml = templateHtml.replace('{{complemento_documento}}', datos.datos_deudor.complemento_documento);

            templateHtml = templateHtml.replace('{{plazo_propuesto_meses}}', datos.datos_operacion.plazo_propuesto_meses);
            templateHtml = templateHtml.replace('{{plazo_propuesto_anios}}', datos.datos_operacion.plazo_propuesto_anios);

            templateHtml = templateHtml.replace('{{tasa_seguro}}', garantia.tasa_seguro);
            //templateHtml = templateHtml.replace('{{prima_seguro}}', garantia.prima_seguro);
            templateHtml = templateHtml.replace('{{monto_operacion}}', datos.datos_operacion.monto_operacion);
            templateHtml = templateHtml.replace('{{moneda_operacion_propuesta}}',datos.datos_operacion.moneda_operacion_propuesta);
            
            templateHtml = templateHtml.replace('{{tipo_garantia_asfi}}', garantia.tipo_garantia_asfi);
            templateHtml = templateHtml.replace('{{descripcion_garantia}}', garantia.descripcion_garantia);
            templateHtml = templateHtml.replace('{{localidad_garantia}}', garantia.localidad_garantia);
            templateHtml = templateHtml.replace('{{direccion_garantia}}', garantia.direccion_garantia);
            templateHtml = templateHtml.replace('{{zona_garantia}}', garantia.zona_garantia);
            templateHtml = templateHtml.replace('{{nro_chasis}}', garantia.nro_chasis);
            templateHtml = templateHtml.replace('{{nro_motor}}', garantia.nro_motor);
            templateHtml = templateHtml.replace('{{placa}}', garantia.placa);
            templateHtml = templateHtml.replace('{{valor_comercial_tracto}}', garantia.valor_comercial_tracto);
            templateHtml = templateHtml.replace('{{valor_comercial_chata}}', garantia.valor_comercial_chata);
            templateHtml = templateHtml.replace('{{uso_vehiculo}}', garantia.uso_vehiculo);
            templateHtml = templateHtml.replace('{{marca_vehiculo}}', garantia.marca_vehiculo);
            templateHtml = templateHtml.replace('{{nro_poliza_importacion}}', garantia.nro_poliza_importacion);
            templateHtml = templateHtml.replace('{{valor_reposicion}}', garantia.valor_reposicion);
            templateHtml = templateHtml.replace('{{moneda_garantia}}', garantia.moneda_garantia);
            templateHtml = templateHtml.replace('{{numero_folio}}', garantia.numero_folio);
            templateHtml = templateHtml.replace('{{tipo_vehiculo}}', garantia.tipo_vehiculo);
            templateHtml = templateHtml.replace('{{marca_vehiculo}}', garantia.marca_vehiculo);
            templateHtml = templateHtml.replace('{{modelo}}', garantia.modelo);
            templateHtml = templateHtml.replace('{{placa}}', garantia.placa);
            templateHtml = templateHtml.replace('{{nro_motor}}', garantia.nro_motor);
            templateHtml = templateHtml.replace('{{nro_chasis}}', garantia.nro_chasis);
            templateHtml = templateHtml.replace('{{color_vehiculo}}', garantia.color_vehiculo);
            templateHtml = templateHtml.replace('{{anio}}', garantia.anio);
            templateHtml = templateHtml.replace('{{valor_total_comercial_v}}', garantia.valor_total_comercial_v);
            templateHtml = templateHtml.replace('{{uso_vehiculo}}', garantia.uso_vehiculo);
            let valor_asegurado_moneda=0;
            let  moneda_1='';
            let  moneda_2='';
            if (garantia.moneda_garantia==='DOLARES AMERICANOS'){
                valor_asegurado_moneda=(parseFloat(garantia.valor_asegurado)*7).toFixed(2);
                moneda_1='USD';
                moneda_2='BS'
            }else{
                valor_asegurado_moneda=(parseFloat(garantia.valor_asegurado)/7).toFixed(2);
                moneda_1='BS';
                moneda_2='USD';
            }
            templateHtml = templateHtml.replace('{{valor_asegurado}}', garantia.valor_asegurado+' '+moneda_1);
            templateHtml = templateHtml.replace('{{valor_asegurado_moneda}}','('+valor_asegurado_moneda+' '+moneda_2+')');
            templateHtml = templateHtml.replace('{{localidad_garantia}}', garantia.localidad_garantia);
            let prima_seguro_moneda=0;
            let  moneda_prima_1='';
            let  moneda_prima_2='';
            if (datos.datos_operacion.moneda_operacion_propuesta ==='DOLARES AMERICANOS'){
                prima_seguro_moneda=(parseFloat(garantia.prima_seguro)*7).toFixed(2);
                moneda_prima_1='USD';
                moneda_prima_2='BS'
            }else{
                prima_seguro_moneda=(parseFloat(garantia.prima_seguro)/7).toFixed(2);
                moneda_prima_1='BS';
                moneda_prima_2='USD';
            }
            templateHtml = templateHtml.replace('{{prima_seguro}}', garantia.prima_seguro+' '+moneda_prima_1);
            templateHtml = templateHtml.replace('{{prima_seguro_moneda}}', '('+prima_seguro_moneda+' '+moneda_prima_2+')');
            templateHtml = templateHtml.replace('{{tipo_garantia}}', garantia.tipo_garantia);

            templateHtml = templateHtml.replace('{{titular}}', titular);
            templateHtml = templateHtml.replace('{{fecha}}', fecha);
            templateHtml = templateHtml.replace('{{num_ci}}', num_ci);

            pagesHtml += templateHtml;

        });

        pdf.create(pagesHtml, options)
            .toFile(filename, (err, res) => {
                if (err) {
                    console.log(`ERROR, generando pdf : ${err}`);
                    response.status = 'ERROR';
                    response.message = JSON.stringify(err);
                    resolve(response);
                } else {
                    response.data = `${garantia}-${solicitud}.pdf`;
                    resolve(response);
                }
            });

    });

};

module.exports.generar = generar;






