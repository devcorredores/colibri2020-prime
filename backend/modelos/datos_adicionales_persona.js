module.exports = (sequelize, Sequelize) => {
    const datos_adicionales_persona = sequelize.define('datos_adicionales_persona', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_persona: {
            type: Sequelize.BIGINT,
            references: {
               model: 'persona', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         id_objeto_x_atributo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'objeto_x_atributo', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
        valor: Sequelize.STRING,
        tipo_error: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return datos_adicionales_persona;
}