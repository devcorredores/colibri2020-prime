module.exports = (sequelize, Sequelize) => {

    const reporte_query = sequelize.define('reporte_query', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        descripcion: Sequelize.STRING,
        query: Sequelize.TEXT,
        id_rol: {
            type: Sequelize.BIGINT,
            references: {
                model: 'rol', 
                key: 'id'
            }
        },
        id_poliza: {
            type: Sequelize.BIGINT,
            references: {
                model: 'poliza', 
                key: 'id'
            }
        },
        id_tipo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        estado: Sequelize.STRING,
        codigo: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return reporte_query;
}
