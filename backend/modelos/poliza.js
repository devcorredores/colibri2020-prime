module.exports = (sequelize, Sequelize) => {
    const poliza = sequelize.define('poliza', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_ramo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', 
                key: 'id'
            }
        },
        id_aseguradora: {
            type: Sequelize.BIGINT,
            references: {
                model: 'entidad', 
                key: 'id'
            }
        },
        id_tipo_numeracion : {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        numero: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        fecha_inicio_vigencia: Sequelize.DATE,
        fecha_fin_vigencia: Sequelize.DATE,
        fecha_emision: Sequelize.DATE,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
        dias_gracia: Sequelize.BIGINT,
        nro_carga: Sequelize.BIGINT,
        codigo_producto: Sequelize.STRING,
        plan_pago: Sequelize.BOOLEAN,
        renovacion_automatica: Sequelize.BOOLEAN,
        certificado_correlativo: Sequelize.BOOLEAN,
        dias_espera_renovacion: Sequelize.INTEGER,
        vigencia_meses: Sequelize.INTEGER,
        dias_anticipado_renovacion: Sequelize.INTEGER,
        dias_habil_siguiente: Sequelize.INTEGER,
        id_calendario: Sequelize.INTEGER
    });
    return poliza;
}
