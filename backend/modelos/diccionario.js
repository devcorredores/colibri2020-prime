// const crypto = require('crypto');

module.exports = (sequelize, Sequelize) => {
    const Diccionario = sequelize.define('diccionario', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        diccionario_codigo: Sequelize.STRING(50),
        diccionario_descripcion: Sequelize.STRING(150),
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return Diccionario;
}



