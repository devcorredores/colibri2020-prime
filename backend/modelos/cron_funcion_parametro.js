module.exports = (sequelize, Sequelize) => {
    const cron_funcion_parametro = sequelize.define('cron_funcion_parametro', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        nombre: Sequelize.STRING,
        id_cron_funcion: {
            type: Sequelize.BIGINT,
            references: {
                model: 'cron_funcion', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        tipo: Sequelize.STRING,
        valor: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        id_comparacion: {

            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_unidad: {

            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        order: Sequelize.INTEGER,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return cron_funcion_parametro;
}
