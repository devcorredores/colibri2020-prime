module.exports = (sequelize, Sequelize) => {
    const vista_objeto_x_atributo_persona = sequelize.define('vista_objeto_x_atributo_persona', {
        id_instancia_poliza: {
            type: Sequelize.BIGINT,
            references: {
                model: 'instancia_poliza', // 'objeto' refers to table name
                key: 'id', // 'id' refers to column name in objetos table
            }
        },
        id_objeto_x_atributo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'objeto_x_atributo', // 'objeto' refers to table name
                key: 'id', // 'id' refers to column name in objetos table
            }
        },
        id_objeto: {
            type: Sequelize.BIGINT,
            references: {
               model: 'objeto', // 'objeto' refers to table name
               key: 'id', // 'id' refers to column name in objetos table
            }
         },
         id_atributo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'atributo', // 'atributo' refers to table name
               key: 'id', // 'id' refers to column name in atributo table
            }
         },
        id_persona: {
            type: Sequelize.BIGINT,
            references: {
                model: 'persona', // 'atributo' refers to table name
                key: 'id', // 'id' refers to column name in atributo table
            }
        },
         persona_primer_nombre: Sequelize.STRING,
         persona_primer_apellido: Sequelize.STRING,
         objeto_descripcion: Sequelize.STRING,
         atributo_descripcion: Sequelize.STRING,
         valor: Sequelize.STRING
    });
    return vista_objeto_x_atributo_persona;
}
