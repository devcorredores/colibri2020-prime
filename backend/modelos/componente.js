module.exports = (sequelize, Sequelize) => {
    const componente = sequelize.define('componente', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_vista: {
            type: Sequelize.BIGINT,
            references: {
               model: 'vista', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
        codigo: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        tipo_componente: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return componente;
}



