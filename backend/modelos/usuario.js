// const crypto = require('crypto');

module.exports = (sequelize, Sequelize) => {
    const Usuario = sequelize.define('usuario', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        usuario_login: Sequelize.STRING(100),
        usuario_password: Sequelize.STRING(100),
        pwd_change: Sequelize.BOOLEAN,
        usuario_email: Sequelize.STRING(100),
        usuario_nombre_completo: Sequelize.STRING(150),
        usuario_img: Sequelize.BLOB,
        par_estado_usuario_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        id_persona: {
            type: Sequelize.BIGINT,
            references: {
                model: 'persona',
                key: 'id',
            }
        },
        par_local_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id',
            }
        },
        adicionado_por: Sequelize.STRING(100),
        modificado_por: Sequelize.STRING(100),
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
    });
    return Usuario;
}



