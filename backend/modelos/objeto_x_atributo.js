module.exports = (sequelize, Sequelize) => {

    const objeto_x_atributo = sequelize.define('objeto_x_atributo', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_objeto: {
            type: Sequelize.BIGINT,
            references: {
               model: 'objeto', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_atributo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'atributo', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
        par_editable_id: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        par_comportamiento_interfaz_id: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         valor_defecto:Sequelize.STRING,
         obligatorio:Sequelize.BOOLEAN,
         adicionado_por:Sequelize.STRING,
         modificado_por:Sequelize.STRING
    });
    return objeto_x_atributo;
}
