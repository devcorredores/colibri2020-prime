module.exports = (sequelize, Sequelize) => {
    const envio = sequelize.define('envio', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_estado:{
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        respuesta:Sequelize.STRING,
        fecha:Sequelize.DATE,
        id_forma_transporte:{
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_instancia_archivo:{
            type: Sequelize.BIGINT,
            references: {
                model: 'instancia_archivo', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_config_ftp:{
            type: Sequelize.BIGINT,
            references: {
                model: 'config_ftp', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        codigo:Sequelize.STRING,
        directorio_remoto:Sequelize.STRING,
    });
    return envio;
}
