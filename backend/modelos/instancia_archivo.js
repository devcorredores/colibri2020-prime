// const crypto = require('crypto');

module.exports = (sequelize, Sequelize) => {

    const instancia_archivo = sequelize.define('instancia_archivo', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        nombre: Sequelize.STRING,
        fecha_creacion: Sequelize.DATE,
        ubicacion: Sequelize.STRING,
        nro_envio: Sequelize.BIGINT,
        id_archivo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'archivo', 
                key: 'id'
            }
        },
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return instancia_archivo;
}



