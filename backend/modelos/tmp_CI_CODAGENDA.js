module.exports = (sequelize, Sequelize) => {
    const tmp_CI_CODAGENDA = sequelize.define('tmp_CI_CODAGENDA', {
        Numero_de_Identificacion:Sequelize.STRING,
        Numero_de_Credito:Sequelize.DECIMAL,
        Num_doc_sinExt:Sequelize.STRING,
        num_doc_ext:Sequelize.STRING,
        codAgenda:Sequelize.INTEGER,
        paterno:Sequelize.STRING,
        materno:Sequelize.STRING,
        NOMBRE:Sequelize.STRING,
        sexo:Sequelize.STRING,
        doc_id:Sequelize.STRING,
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },

    });
    return tmp_CI_CODAGENDA;
}
