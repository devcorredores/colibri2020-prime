module.exports = (sequelize, Sequelize) => {
    const mig_ecoaccidente = sequelize.define('mig_ecoaccidente', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        ID_CERTIFICADO:Sequelize.STRING,
        FECHA_CERTIFICADO:Sequelize.STRING,
        DOCUMENTO:Sequelize.STRING,
        EXTENSION:Sequelize.STRING,
        NOMBRES:Sequelize.STRING,
        APELLIDOS:Sequelize.STRING,
        FECHA_NACIMIENTO:Sequelize.STRING,
        PRIMA_TOTAL:Sequelize.STRING,
        MONEDA:Sequelize.STRING,
        FECHA_INICIO:Sequelize.STRING,
        FECHA_FIN:Sequelize.STRING,
        ESTADO:Sequelize.STRING,
        AGENCIA:Sequelize.STRING,
        NRO_SOLICITUD:Sequelize.STRING,
        USERNAME:Sequelize.STRING,
        NOMBRE_USUARIO:Sequelize.STRING,
        adicionado_por:Sequelize.STRING,
        agencia_cod:Sequelize.STRING,
        sucursal_cod:Sequelize.STRING,
        id_instancia_poliza:Sequelize.BIGINT,
        fin:Sequelize.BIGINT,
        num_doc_id:Sequelize.STRING,
        num_doc_id_tipo:Sequelize.STRING,
        num_doc_id_ext:Sequelize.STRING
    });
    return mig_ecoaccidente;
}
