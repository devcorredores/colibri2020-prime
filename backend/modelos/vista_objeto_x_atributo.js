module.exports = (sequelize, Sequelize) => {
    const vista_objeto_x_atributo = sequelize.define('vista_objeto_x_atributo', {
        id_objeto_x_atributo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'objeto_x_atributo', // 'objeto' refers to table name
                key: 'id', // 'id' refers to column name in objetos table
            }
        },
        id_objeto: {
            type: Sequelize.BIGINT,
            references: {
               model: 'objeto', // 'objeto' refers to table name
               key: 'id', // 'id' refers to column name in objetos table
            }
         },
         id_atributo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'atributo', // 'atributo' refers to table name
               key: 'id', // 'id' refers to column name in atributo table
            }
         },
         objeto_descripcion: Sequelize.STRING,
         atributo_descripcion: Sequelize.STRING
    });
    return vista_objeto_x_atributo;
}
