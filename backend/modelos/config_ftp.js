module.exports = (sequelize, Sequelize) => {
    const config_ftp = sequelize.define('config_ftp', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        usuario:Sequelize.STRING,
        password:Sequelize.STRING,
        host:Sequelize.STRING,
        puerto:Sequelize.STRING,
        certificado:Sequelize.STRING,
        codigo:Sequelize.STRING,
        remote_path:Sequelize.STRING,
        local_path:Sequelize.STRING,
        local_files:Sequelize.STRING,
        descripcion:Sequelize.STRING,
        remote_files:Sequelize.STRING,
        contador_inicial:Sequelize.BIGINT,
        contador_actual:Sequelize.BIGINT,
        id_estado:{
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_tipo_numeracion:{
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        adicionado_por:Sequelize.STRING,
        modificado_por:Sequelize.STRING
    });
    return config_ftp;
}
