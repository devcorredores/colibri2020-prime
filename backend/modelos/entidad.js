module.exports = (sequelize, Sequelize) => {

    const entidad = sequelize.define('entidad', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_persona: {
            type: Sequelize.BIGINT,
            references: {
               model: 'persona', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_persona_juridica: {
            type: Sequelize.BIGINT,
            references: {
               model: 'persona_juridica', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         tipo_entidad:Sequelize.STRING,
         adicionada_por:Sequelize.STRING,
         modificada_por:Sequelize.STRING
    });
    return entidad;
}
