module.exports = (sequelize, Sequelize) => {
    const tmp_verificados = sequelize.define('tmp_verificados', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        ci:Sequelize.STRING,
        old_fecha_nac:Sequelize.DATE,
        old_nro_cuenta:Sequelize.STRING,
        new_fecha_nac:Sequelize.DATE,
        new_nro_cuenta:Sequelize.STRING,
        id_poliza:Sequelize.BIGINT,
    });
    return tmp_verificados;
}
