module.exports = (sequelize, Sequelize) => {

    const intermediario = sequelize.define('intermediario', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_entidad: {
            type: Sequelize.BIGINT,
            references: {
               model: 'entidad', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_instancia_poliza: {
            type: Sequelize.BIGINT,
            references: {
               model: 'instancia_poliza', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         adicionado_por:Sequelize.STRING,
         modificado_por:Sequelize.STRING
    });
    return intermediario;
}
