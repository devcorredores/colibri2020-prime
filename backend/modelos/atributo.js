module.exports = (sequelize, Sequelize) => {
    const atributo = sequelize.define('atributo', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_tipo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_padre: {
            type: Sequelize.BIGINT,
            references: {
               model: 'atributo', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        codigo:Sequelize.STRING,    
        descripcion:Sequelize.STRING,
        adicionada_por:Sequelize.STRING,
        modificada_por:Sequelize.STRING
    });
    return atributo;
}
