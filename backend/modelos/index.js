var dbconfig = require("../config/env");
const Sequelize = require("sequelize");
var sequelize = new Sequelize(
  dbconfig.database,
  dbconfig.username,
  dbconfig.password,
  {
    host: dbconfig.host,
    dialect: dbconfig.dialect,
    dialectOptions: {
      encrypt: true,
      requestTimeout: 9000000,
      packetSize: 102768,
    },
    operatorsAliases: true,
    native: true,
    logging: true,
    define: {
      freezeTableName: true,
    },
    pool: {
      max: dbconfig.pool.max,
      min: dbconfig.pool.min,
      idleTimeoutMillis: 90000,
      acquire: dbconfig.pool.acquire,
      idle: dbconfig.pool.idle,
    },
    options: {
      encrypt: true,
      enableArithAbort: true,
    },
    timezone: "-04:00",
  }
);

const db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.usuario = require("./usuario")(sequelize, Sequelize);
db.persona = require("./persona")(sequelize, Sequelize);
db.parametro = require("./parametro")(sequelize, Sequelize);
db.rol = require("./rol")(sequelize, Sequelize);
db.usuario_x_rol = require("./usuario_x_rol")(sequelize, Sequelize);
db.autenticacion_usuario = require("./autenticacion_usuario")(sequelize, Sequelize);
db.perfil = require("./perfil")(sequelize, Sequelize);
db.rol_x_perfil = require("./rol_x_perfil")(sequelize, Sequelize);
db.modulo = require("./modulo")(sequelize, Sequelize);
db.perfil_x_modulo = require("./perfil_x_modulo")(sequelize, Sequelize);
db.vista = require("./vista")(sequelize, Sequelize);
db.perfil_x_vista = require("./perfil_x_vista")(sequelize, Sequelize);
db.componente = require("./componente")(sequelize, Sequelize);
db.perfil_x_componente = require("./perfil_x_componente")(sequelize, Sequelize);
db.persona_juridica = require("./persona_juridica")(sequelize, Sequelize);
db.entidad = require("./entidad")(sequelize, Sequelize);
db.poliza = require("./poliza")(sequelize, Sequelize);
db.asegurado = require("./asegurado")(sequelize, Sequelize);
db.tomador = require("./tomador")(sequelize, Sequelize);
db.intermediario = require("./intermediario")(sequelize, Sequelize);
db.beneficiario = require("./beneficiario")(sequelize, Sequelize);
db.objeto = require("./objeto")(sequelize, Sequelize);
db.atributo = require("./atributo")(sequelize, Sequelize);
db.objeto_x_atributo = require("./objeto_x_atributo")(sequelize, Sequelize);
db.instancia_poliza = require("./instancia_poliza")(sequelize, Sequelize);
db.instancia_poliza_transicion = require("./instancia_poliza_transicion")(sequelize, Sequelize);
db.atributo_instancia_poliza = require("./atributo_instancia_poliza")(sequelize, Sequelize);
db.diccionario = require("./diccionario")(sequelize, Sequelize);
db.documento = require("./documento")(sequelize, Sequelize);
db.documento_version = require("./documento_version")(sequelize, Sequelize);
db.documento_version_params = require("./documento_version_params")(sequelize, Sequelize);
db.instancia_documento = require("./instancia_documento")(sequelize, Sequelize);
db.atributo_instancia_documento = require("./atributo_instancia_documento")(sequelize, Sequelize);
db.datos_adicionales_persona = require("./datos_adicionales_persona")(sequelize, Sequelize);
db.vista_objeto_x_atributo = require("./vista_objeto_x_atributo")(sequelize, Sequelize);
db.vista_objeto_x_atributo_persona = require("./vista_objeto_x_atributo_persona")(sequelize, Sequelize);
db.anexo_poliza = require("./anexo_poliza")(sequelize, Sequelize);
db.clase_asegurado = require("./clase_asegurado")(sequelize, Sequelize);
db.anexo_asegurado = require("./anexo_asegurado")(sequelize, Sequelize);
db.dato_anexo_asegurado = require("./dato_anexo_asegurado")(sequelize, Sequelize);
db.instancia_anexo_asegurado = require("./instancia_anexo_asegurado")(sequelize, Sequelize);
db.atributo_instancia_anexo_asegurado = require("./atributo_instancia_anexo_asegurado")(sequelize, Sequelize);
db.tmp_CI_CODAGENDA = require("./tmp_CI_CODAGENDA")(sequelize, Sequelize);
db.tmp_CI_CUENTA = require("./tmp_CI_CUENTA")(sequelize, Sequelize);
db.usuarioColibri = require("./usuarioColibri")(sequelize, Sequelize);
db.reporte = require("./reporte")(sequelize, Sequelize);
db.config_cron = require("./config_cron")(sequelize, Sequelize);
db.config_correo = require("./config_correo")(sequelize, Sequelize);
db.config_ftp = require("./config_ftp")(sequelize, Sequelize);
db.cron_funcion = require("./cron_funcion")(sequelize, Sequelize);
db.cron_funcion_parametro = require("./cron_funcion_parametro")(sequelize, Sequelize);
db.reporte_query = require("./reporte_query")(sequelize, Sequelize);
db.reporte_query_parametro = require("./reporte_query_parametro")(sequelize, Sequelize);
db.planPago = require("./planPago")(sequelize, Sequelize);
db.plan_pago_detalle = require("./plan_pago_detalle")(sequelize, Sequelize);
db.tmp_ecoproteccion_complemento_beneficiario = require("./tmp_ecoproteccion_complemento_beneficiario")(sequelize, Sequelize);
db.mig_ecoproteccion = require("./mig_ecoproteccion")(sequelize, Sequelize);
db.mig_ecomedicv = require("./mig_ecomedicv")(sequelize, Sequelize);
db.mig_ecovida = require("./mig_ecovida")(sequelize, Sequelize);
db.mig_ecoriesgo = require("./mig_ecoriesgo")(sequelize, Sequelize);
db.mig_ecoaccidente = require("./mig_ecoaccidente")(sequelize, Sequelize);
db.mig_ecoresguardo = require("./mig_ecoresguardo")(sequelize, Sequelize);
db.mig_ecoaguinaldo = require("./mig_ecoaguinaldo")(sequelize, Sequelize);
db.mig_tarjetas = require("./mig_tarjetas")(sequelize, Sequelize);
db.tmp_cliente_cuenta = require("./tmp_cliente_cuenta")(sequelize, Sequelize);
db.envio = require("./envio")(sequelize, Sequelize);
db.envio_historico = require("./envio_historico")(sequelize, Sequelize);
db.archivo = require("./archivo")(sequelize, Sequelize);
db.instancia_archivo = require("./instancia_archivo")(sequelize, Sequelize);
db.tmp_verificados = require("./tmp_verificados")(sequelize, Sequelize);
db.int_debito_detalle = require("./int_debito_detalle")(sequelize, Sequelize);
db.cal_festivo = require("./cal_festivo")(sequelize, Sequelize);

//-- Relations --

//cron_funcion
db.cron_funcion.belongsTo(db.config_cron, { foreignKey: "id_cron" });
db.config_cron.hasMany(db.cron_funcion, { foreignKey: "id_cron" });

//cron_funcion_parametro
db.cron_funcion_parametro.belongsTo(db.cron_funcion, {foreignKey: "id_cron_funcion",});
db.cron_funcion.hasMany(db.cron_funcion_parametro, {foreignKey: "id_cron_funcion",});
db.cron_funcion_parametro.belongsTo(db.parametro, {foreignKey: "id_unidad", as: "unidad",});
db.parametro.hasMany(db.cron_funcion_parametro, {foreignKey: "id_unidad", as: "unidad",});
db.cron_funcion_parametro.belongsTo(db.parametro, {foreignKey: "id_comparacion", as: "comparacion",});
db.parametro.hasMany(db.cron_funcion_parametro, {foreignKey: "id_comparacion", as: "comparacion",});
db.cron_funcion_parametro.belongsTo(db.config_correo, {foreignKey: "valor", as: "config_correo", targetKey: "codigo",});
db.config_correo.hasMany(db.cron_funcion_parametro, {foreignKey: "valor", as: "config_correo", sourceKey: "codigo",});
db.cron_funcion_parametro.belongsTo(db.parametro, {foreignKey: "valor", as: "archivo_salida", targetKey: "parametro_cod",});
db.parametro.hasMany(db.cron_funcion_parametro, {foreignKey: "valor", as: "archivo_salida", sourceKey: "parametro_cod",});
db.cron_funcion_parametro.belongsTo(db.config_ftp, {foreignKey: "valor", as: "config_ftp", targetKey: "codigo",});
db.config_ftp.hasMany(db.cron_funcion_parametro, {foreignKey: "valor", as: "config_ftp", sourceKey: "codigo",});

// db.cron_funcion_parametro.belongsTo(db.config_correo, {foreignKey:'valor', as:'config_correo', targetKey: 'id'});
// db.config_correo.hasMany(db.cron_funcion_parametro,{foreignKey:'valor', as:'config_correo', sourceKey: 'id'});
// db.cron_funcion_parametro.belongsTo(db.parametro, {foreignKey:'valor', as:'archivo', targetKey: 'id'});
// db.parametro.hasMany(db.cron_funcion_parametro,{foreignKey:'valor', as:'archivo', sourceKey: 'id'});
// db.cron_funcion_parametro.belongsTo(db.config_ftp, {foreignKey:'valor', as:'config_ftp', targetKey: 'id'});
// db.config_ftp.hasMany(db.cron_funcion_parametro,{foreignKey:'valor', as:'config_ftp', sourceKey: 'id'});

db.cron_funcion_parametro.belongsTo(db.reporte_query, {foreignKey: "valor", as: "reporte_query", targetKey: "codigo",});
db.reporte_query.hasMany(db.cron_funcion_parametro, {foreignKey: "valor", as: "reporte_query", sourceKey: "codigo",});

//asegurado
db.asegurado.belongsTo(db.entidad, { foreignKey: "id_entidad" });
db.entidad.hasMany(db.asegurado, { foreignKey: "id_entidad" });
db.asegurado.belongsTo(db.usuario, {foreignKey: "adicionado_por", as: "usuarioAdicionado",});
db.usuario.hasMany(db.asegurado, {foreignKey: "adicionado_por", as: "usuarioAdicionado",});
db.asegurado.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza",});
db.instancia_poliza.hasMany(db.asegurado, {foreignKey: "id_instancia_poliza",});

//atributo
db.atributo.belongsTo(db.parametro, { foreignKey: "id_tipo" });
db.parametro.hasMany(db.atributo, { foreignKey: "id_tipo" });

db.diccionario.hasMany(db.atributo, { foreignKey: "id_diccionario" });
db.atributo.belongsTo(db.diccionario, { foreignKey: "id_diccionario" });

//atributo_instancia_documento
db.atributo_instancia_documento.belongsTo(db.instancia_documento, {foreignKey: "id_instancia_documento",});
db.instancia_documento.hasMany(db.atributo_instancia_documento, {foreignKey: "id_instancia_documento",});
db.atributo_instancia_documento.belongsTo(db.objeto_x_atributo, {foreignKey: "id_objeto_x_atributo",});
db.objeto_x_atributo.hasMany(db.atributo_instancia_documento, {foreignKey: "id_objeto_x_atributo",});

//atributo_instancia_poliza
db.atributo_instancia_poliza.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "agencia",});
db.instancia_poliza.hasOne(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza", as: "agencia",});
db.atributo_instancia_poliza.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "nro_solicitud_sci",});
db.instancia_poliza.hasOne(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza", as: "nro_solicitud_sci",});
db.atributo_instancia_poliza.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "sucursal",});
db.instancia_poliza.hasOne(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza", as: "sucursal",});
db.atributo_instancia_poliza.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "plan",});
db.instancia_poliza.hasOne(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza", as: "plan",});
db.atributo_instancia_poliza.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "tipo_pago",});
db.instancia_poliza.hasOne(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza", as: "tipo_pago",});
db.atributo_instancia_poliza.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "nro_transaccion",});
db.instancia_poliza.hasOne(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza", as: "nro_transaccion",});
db.atributo_instancia_poliza.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "aip_modo_pago",});
db.instancia_poliza.hasOne(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza", as: "aip_modo_pago",});
db.atributo_instancia_poliza.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "aip_debito",});
db.instancia_poliza.hasOne(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza", as: "aip_debito",});
db.atributo_instancia_poliza.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza",});
db.instancia_poliza.hasMany(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza",});

db.atributo_instancia_poliza.belongsTo(db.objeto_x_atributo, {foreignKey: "id_objeto_x_atributo",});
db.objeto_x_atributo.hasMany(db.atributo_instancia_poliza, {foreignKey: "id_objeto_x_atributo",});
db.atributo_instancia_poliza.belongsTo(db.asegurado, {foreignKey: "id_instancia_poliza",});
db.asegurado.hasMany(db.atributo_instancia_poliza, {foreignKey: "id_instancia_poliza",});

// parametros
db.parametro.belongsTo(db.atributo_instancia_poliza, {foreignKey: "id", sourceKey: "valor", as: "par_modo_pago"});
db.atributo_instancia_poliza.hasMany(db.parametro, {foreignKey: "id", sourceKey: "valor", as: "par_modo_pago"});
db.parametro.belongsTo(db.atributo_instancia_poliza, {foreignKey: "id", sourceKey: "valor", as: "par_debito"});
db.atributo_instancia_poliza.hasMany(db.parametro, {foreignKey: "id", sourceKey: "valor", as: "par_debito"});

db.parametro.belongsTo(db.atributo_instancia_documento, {foreignKey: "id", sourceKey: "valor", as: "par_motivo"});
db.atributo_instancia_documento.hasMany(db.parametro, {foreignKey: "id", sourceKey: "valor", as: "par_motivo"});

//instancia_poliza_transicion
db.instancia_poliza_transicion.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza",});
db.instancia_poliza.hasMany(db.instancia_poliza_transicion, {foreignKey: "id_instancia_poliza",});
db.instancia_poliza_transicion.belongsTo(db.parametro, {foreignKey: "par_estado_id", as: "par_estado",});
db.parametro.hasMany(db.instancia_poliza_transicion, {foreignKey: "par_estado_id", as: "par_estado",});
db.instancia_poliza_transicion.belongsTo(db.parametro, {foreignKey: "par_observacion_id", as: "par_observacion",});
db.parametro.hasMany(db.instancia_poliza_transicion, {foreignKey: "par_observacion_id", as: "par_observacion",});
db.instancia_poliza_transicion.belongsTo(db.usuario, {foreignKey: "adicionado_por", as: "ipt_usuario",});
db.usuario.hasMany(db.instancia_poliza_transicion, {foreignKey: "adicionado_por", as: "ipt_usuario",});

//autenticacion_usuario
db.autenticacion_usuario.belongsTo(db.usuario, { foreignKey: "usuario_id" });
db.usuario.hasOne(db.autenticacion_usuario, { foreignKey: "usuario_id" });
db.autenticacion_usuario.belongsTo(db.parametro, {foreignKey: "par_autenticacion_id", as: "par_autenticacion",});
db.parametro.hasOne(db.autenticacion_usuario, {foreignKey: "par_autenticacion_id", as: "par_autenticacion",});

//beneficiario
db.beneficiario.belongsTo(db.entidad, { foreignKey: "id_beneficiario" });
db.entidad.hasOne(db.beneficiario, { foreignKey: "id_beneficiario" });
db.beneficiario.belongsTo(db.asegurado, { foreignKey: "id_asegurado" });
db.asegurado.hasMany(db.beneficiario, { foreignKey: "id_asegurado" });
db.beneficiario.belongsTo(db.parametro, { foreignKey: "id_parentesco",as: "parentesco"});
db.parametro.hasOne(db.beneficiario, { foreignKey: "id_parentesco",as: "parentesco"});
db.beneficiario.belongsTo(db.parametro, { foreignKey: "estado", as: "estado_obj"});
db.beneficiario.belongsTo(db.parametro, { foreignKey: "condicion", as: "condicion_obj"});
db.parametro.hasOne(db.beneficiario, { foreignKey: "estado", as: "estado_obj"});
db.beneficiario.belongsTo(db.parametro, { foreignKey: "tipo", as: "tipo_obj" });
db.parametro.hasOne(db.beneficiario, { foreignKey: "tipo", as: "tipo_obj" });

//componente
db.componente.belongsTo(db.vista, { foreignKey: "id_vista" });
db.vista.hasOne(db.componente, { foreignKey: "id_vista" });

//datos_adicionales_persona

//diccionario

//entidad
db.entidad.belongsTo(db.persona, { foreignKey: "id_persona" });
db.persona.hasOne(db.entidad, { foreignKey: "id_persona" });
db.entidad.belongsTo(db.persona_juridica, { foreignKey: "id_persona" });
db.persona_juridica.hasOne(db.entidad, { foreignKey: "id_persona_juridica" });

//instancia_documento
db.instancia_documento.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza",});
db.instancia_poliza.hasMany(db.instancia_documento, {foreignKey: "id_instancia_poliza",});
db.instancia_documento.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "solicitudes",});
db.instancia_poliza.hasMany(db.instancia_documento, {foreignKey: "id_instancia_poliza", as: "solicitudes",});
db.instancia_documento.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "certificados",});
db.instancia_poliza.hasMany(db.instancia_documento, {foreignKey: "id_instancia_poliza", as: "certificados",});
db.instancia_documento.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza", as: "ordenes_cobro",});
db.instancia_poliza.hasMany(db.instancia_documento, {foreignKey: "id_instancia_poliza", as: "ordenes_cobro",});
db.instancia_documento.belongsTo(db.documento, { foreignKey: "id_documento" });
db.documento.hasMany(db.instancia_documento, { foreignKey: "id_documento" });
db.instancia_documento.belongsTo(db.asegurado, {foreignKey: "id_instancia_poliza",});
db.asegurado.hasMany(db.instancia_documento, {foreignKey: "id_instancia_poliza",});
db.instancia_documento.belongsTo(db.usuario, { foreignKey: "adicionada_por" });
db.usuario.hasMany(db.instancia_documento, { foreignKey: "adicionada_por" });
db.instancia_documento.belongsTo(db.documento_version, { foreignKey: "id_documento_version" });
db.documento_version.hasMany(db.instancia_documento, { foreignKey: "id_documento_version" });

//documento
db.documento.belongsTo(db.poliza, { foreignKey: "id_poliza" });
db.poliza.hasMany(db.documento, { foreignKey: "id_poliza" });
db.documento.belongsTo(db.archivo, {foreignKey: "id_archivo",});
db.archivo.hasMany(db.documento, {foreignKey: "id_archivo",});
db.documento.belongsTo(db.parametro, {foreignKey: "id_tipo_documento", as: "tipo_documento",});
db.parametro.hasMany(db.documento, {foreignKey: "id_tipo_documento", as: "tipo_documento",});
db.documento.belongsTo(db.documento_version, {foreignKey: "id_documento_version"});
db.documento_version.hasMany(db.documento, {foreignKey: "id_documento_version"});

//documento_version

db.documento_version.belongsTo(db.reporte_query, {foreignKey: "id_reporte_query"});
db.reporte_query.hasMany(db.documento_version, {foreignKey: "id_reporte_query"});

//documento_version_params

db.documento_version_params.belongsTo(db.documento_version, {foreignKey: "id_documento_version"});
db.documento_version.hasMany(db.documento_version_params, {foreignKey: "id_documento_version"});

// db.documento_version_params.belongsTo(db.documento_version_params, {foreignKey: "id_padre",sourceKey: "id_padre", as:'doc_param_hijos'});
db.documento_version_params.hasMany(db.documento_version_params, {foreignKey: "id_padre",sourceKey: "id", as:'doc_param_hijos'});

// db.documento_version_params.belongsTo(db.documento_version_params, {t: "id_padre", as:'doc_param_hijos'});

//instancia_poliza
db.instancia_poliza.belongsTo(db.poliza, { foreignKey: "id_poliza" });
db.poliza.hasMany(db.instancia_poliza, { foreignKey: "id_poliza" });
db.instancia_poliza.belongsTo(db.parametro, { foreignKey: "id_estado", as: "estado"});
db.parametro.hasMany(db.instancia_poliza, { foreignKey: "id_estado", as: "estado"});
db.instancia_poliza.belongsTo(db.usuario, { foreignKey: "adicionada_por" });
db.usuario.hasMany(db.instancia_poliza, { foreignKey: "adicionada_por" });
db.instancia_poliza.belongsTo(db.usuario, { foreignKey: "modificada_por", as: "usuario_modificado_por"});
db.usuario.hasMany(db.instancia_poliza, { foreignKey: "modificada_por", as: "usuario_modificado_por"});
db.instancia_poliza.belongsTo(db.anexo_poliza, { foreignKey: "id_anexo_poliza"});
db.anexo_poliza.hasMany(db.instancia_poliza, { foreignKey: "id_anexo_poliza"});
// db.instancia_poliza.belongsTo(db.instancia_poliza_transicion, {foreignKey:'id', targetKey:'id_instancia_poliza', as:'instanciaPolizaInstanciaPolizaTransicion'});
// db.instancia_poliza_transicion.hasMany(db.instancia_poliza,{foreignKey:'id', sourceKey:'id_instancia_poliza', as:'instanciaPolizaInstanciaPolizaTransicion'});

//intermediario
db.intermediario.belongsTo(db.entidad, { foreignKey: "id_entidad" });
db.entidad.hasOne(db.intermediario, { foreignKey: "id_entidad" });
db.intermediario.belongsTo(db.instancia_poliza, {
  foreignKey: "id_instancia_poliza",
});
db.instancia_poliza.hasOne(db.intermediario, {
  foreignKey: "id_instancia_poliza",
});

//modulo
db.modulo.belongsTo(db.modulo, {foreignKey: "id_modulo_super", as: "moduloPadre"});
db.modulo.hasOne(db.modulo, {  foreignKey: "id_modulo_super",as: "moduloPadre",});

db.modulo.belongsTo(db.modulo, {foreignKey: "id_modulo_super", as: "moduloPadre2"});
db.modulo.hasOne(db.modulo, {  foreignKey: "id_modulo_super",as: "moduloPadre2",});

//poliza
db.poliza.belongsTo(db.entidad, {foreignKey: "id_aseguradora", as: "aseguradora",});
db.entidad.hasMany(db.poliza, {foreignKey: "id_aseguradora", as: "aseguradora",});
db.poliza.belongsTo(db.parametro, { foreignKey: "id_ramo", as: "ramo" });
db.parametro.hasMany(db.poliza, { foreignKey: "id_ramo", as: "ramo" });
db.poliza.belongsTo(db.parametro, {foreignKey: "id_tipo_numeracion", as: "tipo_numeracion",});
db.parametro.hasMany(db.poliza, {foreignKey: "id_tipo_numeracion", as: "tipo_numeracion",});
db.poliza.belongsTo(db.objeto, {foreignKey: "id", targetKey: "id_poliza", as: "polizaObjeto",});
db.objeto.hasMany(db.poliza, {foreignKey: "id", sourceKey: "id_poliza", as: "polizaObjeto",});
db.poliza.belongsTo(db.objeto, {foreignKey: "id", targetKey: "id_poliza", as: "documentoObjeto",});
db.objeto.hasMany(db.poliza, {foreignKey: "id", sourceKey: "id_poliza", as: "documentoObjeto",});

//objeto
db.objeto.belongsTo(db.poliza, { foreignKey: "id_poliza" });
db.poliza.hasMany(db.objeto, { foreignKey: "id_poliza" });
db.objeto.belongsTo(db.objeto_x_atributo, {
  foreignKey: "id",
  targetKey: "id_objeto",
  as: "objetoObjetoXAtributos",
});
db.objeto_x_atributo.hasMany(db.objeto, {
  foreignKey: "id",
  sourceKey: "id_objeto",
  as: "objetoObjetoXAtributos",
});
db.objeto.belongsTo(db.objeto_x_atributo, {
  foreignKey: "id",
  targetKey: "id_objeto",
  as: "objetoObjetoXAtributo",
});
db.objeto_x_atributo.hasMany(db.objeto, {
  foreignKey: "id",
  sourceKey: "id_objeto",
  as: "objetoObjetoXAtributo",
});

//objeto_x_atributo
db.objeto_x_atributo.belongsTo(db.objeto, { foreignKey: "id_objeto" });
db.objeto.hasMany(db.objeto_x_atributo, { foreignKey: "id_objeto" });
db.objeto_x_atributo.belongsTo(db.atributo, { foreignKey: "id_atributo" });
db.atributo.hasMany(db.objeto_x_atributo, { foreignKey: "id_atributo" });
db.objeto_x_atributo.belongsTo(db.parametro, {
  foreignKey: "par_editable_id",
  as: "par_editable",
});
db.parametro.hasMany(db.objeto_x_atributo, {
  foreignKey: "par_editable_id",
  as: "par_editable",
});
db.objeto_x_atributo.belongsTo(db.parametro, {
  foreignKey: "par_comportamiento_interfaz_id",
  as: "par_comportamiento_interfaz",
});
db.parametro.hasMany(db.objeto_x_atributo, {
  foreignKey: "par_comportamiento_interfaz_id",
  as: "par_comportamiento_interfaz",
});
db.objeto_x_atributo.belongsTo(db.atributo, {
  foreignKey: "id_atributo",
  targetKey: "id",
  as: "objetoXAtributoAtributos",
});
db.atributo.hasMany(db.objeto_x_atributo, {
  foreignKey: "id_atributo",
  sourceKey: "id",
  as: "objetoXAtributoAtributos",
});
db.objeto_x_atributo.belongsTo(db.objeto, {
  foreignKey: "id_objeto",
  targetKey: "id",
  as: "objetoXAtributoObjetos",
});
db.objeto.hasMany(db.objeto_x_atributo, {
  foreignKey: "id_objeto",
  sourceKey: "id",
  as: "objetoXAtributoObjetos",
});

db.atributo.belongsTo(db.atributo, { foreignKey: "id_padre", as: "atributoPadre" });

//parametro
db.parametro.belongsTo(db.diccionario, { foreignKey: "diccionario_id" });
db.diccionario.hasMany(db.parametro, { foreignKey: "diccionario_id" });

db.parametro.belongsTo(db.parametro, { foreignKey: "id_padre" });

//perfil

//perfil_x_componente
db.perfil_x_componente.belongsTo(db.perfil, { foreignKey: "id_perfil" });
db.perfil.hasMany(db.perfil_x_componente, { foreignKey: "id_perfil" });
db.perfil_x_componente.belongsTo(db.componente, {foreignKey: "id_componente",});
db.componente.hasMany(db.perfil_x_componente, { foreignKey: "id_componente" });

//perfil_x_modulo
db.perfil_x_modulo.belongsTo(db.perfil, { foreignKey: "id_perfil" });
db.perfil.hasMany(db.perfil_x_modulo, { foreignKey: "id_perfil" });
db.perfil_x_modulo.belongsTo(db.modulo, { foreignKey: "id_modulo" });
db.modulo.hasMany(db.perfil_x_modulo, { foreignKey: "id_modulo" });

//perfil_x_vista
db.perfil_x_vista.belongsTo(db.perfil, { foreignKey: "id_perfil" });
db.perfil.hasMany(db.perfil_x_vista, { foreignKey: "id_perfil" });
db.perfil_x_vista.belongsTo(db.vista, { foreignKey: "id_vista" });
db.vista.hasMany(db.perfil_x_vista, { foreignKey: "id_vista" });

//persona
db.persona.belongsTo(db.parametro, {
  foreignKey: "par_nacionalidad_id",
  as: "par_nacionalidad",
});
db.parametro.hasMany(db.persona, {
  foreignKey: "par_nacionalidad_id",
  as: "par_nacionalidad",
});
db.persona.belongsTo(db.parametro, {
  foreignKey: "par_pais_nacimiento_id",
  as: "par_pais_nacimiento",
});
db.parametro.hasMany(db.persona, {
  foreignKey: "par_pais_nacimiento_id",
  as: "par_pais_nacimiento",
});
db.persona.belongsTo(db.parametro, {
  foreignKey: "par_sexo_id",
  as: "par_sexo",
});
db.parametro.hasMany(db.persona, { foreignKey: "par_sexo_id", as: "par_sexo" });
db.persona.belongsTo(db.parametro, {
  foreignKey: "par_tipo_documento_id",
  as: "par_tipo_documento",
});
db.parametro.hasMany(db.persona, {
  foreignKey: "par_tipo_documento_id",
  as: "par_tipo_documento",
});

// persona_juridica

// rol

// rol_x_perfil
db.rol_x_perfil.belongsTo(db.rol, { foreignKey: "id_rol" });
db.rol.hasMany(db.rol_x_perfil, { foreignKey: "id_rol" });
db.rol_x_perfil.belongsTo(db.perfil, { foreignKey: "id_perfil" });
db.perfil.hasMany(db.rol_x_perfil, { foreignKey: "id_perfil" });

// sysdiagrams

// tomador
db.tomador.belongsTo(db.entidad, { foreignKey: "id_entidad" });
db.entidad.hasMany(db.tomador, { foreignKey: "id_entidad" });
db.tomador.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza",});
db.instancia_poliza.hasMany(db.tomador, { foreignKey: "id_instancia_poliza" });

// usuario
db.usuario.belongsTo(db.parametro, {foreignKey: "par_estado_usuario_id", as: "par_estado_usuario",});
db.parametro.hasMany(db.usuario, {foreignKey: "par_estado_usuario_id", as: "par_estado_usuario",});
db.usuario.belongsTo(db.persona, { foreignKey: "id_persona" });
db.persona.hasMany(db.usuario, { foreignKey: "id_persona" });
db.usuario.belongsTo(db.parametro, {foreignKey: "par_local_id", as: "par_local",});
db.parametro.hasMany(db.usuario, {foreignKey: "par_local_id", as: "par_local",});

db.usuario.belongsToMany(db.rol, {through: "usuario_x_rol", as: "usuarioRoles", foreignKey: "id_usuario",});
db.rol.belongsToMany(db.usuario, {through: "usuario_x_rol", as: "usuarioRoles", foreignKey: "id_rol",});

db.rol.belongsToMany(db.perfil, {through: "rol_x_perfil", as: "rolPerfiles", foreignKey: "id_rol",});
db.perfil.belongsToMany(db.rol, {through: "rol_x_perfil", as: "rolPerfiles", foreignKey: "id_perfil",});

db.perfil.belongsToMany(db.modulo, {through: "perfil_x_modulo", as: "perfilModulos", foreignKey: "id_perfil",});
db.modulo.belongsToMany(db.perfil, {through: "perfil_x_modulo", as: "perfilModulos", foreignKey: "id_modulo",});

db.perfil.belongsToMany(db.vista, {through: "perfil_x_vista", as: "perfilVistas", foreignKey: "id_perfil",});
db.vista.belongsToMany(db.perfil, {through: "perfil_x_vista", as: "perfilVistas", foreignKey: "id_vista",});

db.perfil.belongsToMany(db.componente, {through: "perfil_x_componente", as: "perfilComponentes", foreignKey: "id_perfil",});
db.componente.belongsToMany(db.perfil, {through: "perfil_x_componente", as: "perfilComponentes", foreignKey: "id_componente",});

// usuario_x_rol
db.usuario_x_rol.belongsTo(db.usuario, { foreignKey: "id_usuario" });
db.usuario.hasMany(db.usuario_x_rol, { foreignKey: "id_usuario" });
db.usuario_x_rol.belongsTo(db.rol, { foreignKey: "id_rol" });
db.rol.hasMany(db.usuario_x_rol, { foreignKey: "id_rol" });

// vista
db.vista.belongsTo(db.modulo, { foreignKey: "id_modulo" });
db.modulo.hasMany(db.vista, { foreignKey: "id_modulo" });

//anexo_poliza
db.poliza.hasMany(db.anexo_poliza, { foreignKey: "id_poliza" });
db.anexo_poliza.belongsTo(db.poliza, { foreignKey: "id_poliza" });
db.anexo_poliza.hasMany(db.anexo_asegurado, { foreignKey: "id_anexo_poliza" });
db.anexo_asegurado.belongsTo(db.anexo_poliza, {foreignKey: "id_anexo_poliza",});
db.anexo_poliza.belongsTo(db.parametro, {foreignKey: "id_moneda", as: "plan_moneda",});
db.parametro.hasMany(db.anexo_poliza, {foreignKey: "id_moneda", as: "plan_moneda",});
db.anexo_poliza.belongsTo(db.parametro, { foreignKey: "id_moneda", as:'par_moneda' });
db.parametro.hasOne(db.anexo_poliza, { foreignKey: "id_moneda", as:'par_moneda' });

//anexo_asegurado
db.anexo_asegurado.hasMany(db.dato_anexo_asegurado, {foreignKey: "id_anexo_asegurado",});
db.dato_anexo_asegurado.belongsTo(db.anexo_asegurado, {foreignKey: "id_anexo_asegurado",});
db.parametro.hasMany(db.anexo_asegurado, { foreignKey: "id_tipo" });
db.anexo_asegurado.belongsTo(db.parametro, { foreignKey: "id_tipo" });
db.parametro.hasMany(db.anexo_asegurado, {foreignKey: "id_edad_unidad", as: "edad_unidad",});
db.anexo_asegurado.belongsTo(db.parametro, {foreignKey: "id_edad_unidad", as: "edad_unidad",});

//instancia_anexo_asegurado
db.anexo_asegurado.hasMany(db.instancia_anexo_asegurado, {foreignKey: "id_anexo_asegurado",});
db.instancia_anexo_asegurado.belongsTo(db.anexo_asegurado, {foreignKey: "id_anexo_asegurado",});
db.asegurado.hasMany(db.instancia_anexo_asegurado, {foreignKey: "id_asegurado",});
db.instancia_anexo_asegurado.belongsTo(db.asegurado, {foreignKey: "id_asegurado",});
db.entidad.hasMany(db.instancia_anexo_asegurado, { foreignKey: "id_entidad" });
db.instancia_anexo_asegurado.belongsTo(db.entidad, {foreignKey: "id_entidad",});
db.parametro.hasMany(db.instancia_anexo_asegurado, {foreignKey: "id_parentesco", as: "par_parentesco",});
db.instancia_anexo_asegurado.belongsTo(db.parametro, {foreignKey: "id_parentesco", as: "par_parentesco",});
db.parametro.hasMany(db.instancia_anexo_asegurado, {foreignKey: "id_relacion", as: "par_relacion",});
db.instancia_anexo_asegurado.belongsTo(db.parametro, {foreignKey: "id_relacion", as: "par_relacion",});
db.parametro.hasMany(db.instancia_anexo_asegurado, {foreignKey: "id_estado", as: "par_estado",});
db.instancia_anexo_asegurado.belongsTo(db.parametro, {foreignKey: "id_estado", as: "par_estado",});

//atributo_instancia_anexo_asegurado
db.instancia_anexo_asegurado.hasMany(db.atributo_instancia_anexo_asegurado, {foreignKey: "id_instancia_anexo_asegurado",});
db.atributo_instancia_anexo_asegurado.belongsTo(db.instancia_anexo_asegurado, {foreignKey: "id_instancia_anexo_asegurado",});

//reporte_query
db.reporte_query_parametro.belongsTo(db.reporte_query, {foreignKey: "id_reporte_query",});
db.reporte_query.hasMany(db.reporte_query_parametro, {foreignKey: "id_reporte_query",});
db.reporte_query_parametro.belongsTo(db.diccionario, {foreignKey: "id_diccionario",});
db.diccionario.hasMany(db.reporte_query_parametro, {foreignKey: "id_diccionario",});
db.reporte_query.belongsTo(db.poliza, { foreignKey: "id_poliza" });
db.poliza.hasMany(db.reporte_query, { foreignKey: "id_poliza" });

//config_ftp
db.config_ftp.belongsTo(db.parametro, {foreignKey: "id_estado", as: "estado",});
db.parametro.hasMany(db.config_ftp, { foreignKey: "id_estado", as: "estado" });
db.config_ftp.belongsTo(db.parametro, {foreignKey: "id_tipo_numeracion", as: "tipo_numeracion",});
db.parametro.hasMany(db.config_ftp, {foreignKey: "id_tipo_numeracion", as: "tipo_numeracion",});

//envio
db.envio.belongsTo(db.parametro, { foreignKey: "id_estado", as: "estado" });
db.parametro.hasMany(db.envio, { foreignKey: "id_estado", as: "estado" });

db.envio.belongsTo(db.parametro, {foreignKey: "id_forma_transporte", as: "forma_transporte",});
db.parametro.hasMany(db.envio, {foreignKey: "id_forma_transporte", as: "forma_transporte",});

db.envio.belongsTo(db.config_ftp, {foreignKey: "id_config_ftp", as: "config_ftp",});
db.config_ftp.hasMany(db.envio, {foreignKey: "id_config_ftp", as: "config_ftp",});

db.envio.belongsTo(db.instancia_archivo, {foreignKey: "id_instancia_archivo",});
db.instancia_archivo.hasMany(db.envio, { foreignKey: "id_instancia_archivo" });

//envio_historico
db.envio_historico.belongsTo(db.parametro, {foreignKey: "id_envio", as: "envio",});
db.parametro.hasMany(db.envio_historico, {foreignKey: "id_envio", as: "envio",});

//instancia_archivo
db.instancia_archivo.belongsTo(db.archivo, {foreignKey: "id_archivo", as: "archivo",});
db.archivo.hasMany(db.instancia_archivo, {foreignKey: "id_archivo", as: "archivo",});
db.instancia_documento.belongsTo(db.instancia_archivo, {foreignKey: "id_instancia_archivo"});
db.instancia_archivo.hasMany(db.instancia_documento, {foreignKey: "id_instancia_archivo"});

//plan_pago
db.planPago.belongsTo(db.instancia_poliza, {foreignKey: "id_instancia_poliza",});
db.instancia_poliza.hasMany(db.planPago, { foreignKey: "id_instancia_poliza" });

//plan_pago_detalle
db.plan_pago_detalle.belongsTo(db.planPago, { foreignKey: "id_planpago" });
db.planPago.hasMany(db.plan_pago_detalle, { foreignKey: "id_planpago" });
db.plan_pago_detalle.belongsTo(db.instancia_poliza, { foreignKey: "id_instancia_poliza" });
db.instancia_poliza.hasMany(db.plan_pago_detalle, { foreignKey: "id_instancia_poliza" });

//archivo

module.exports = db;
