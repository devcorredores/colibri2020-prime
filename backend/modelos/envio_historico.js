module.exports = (sequelize, Sequelize) => {
    const envio_historico = sequelize.define('envio_historico', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        contenido:Sequelize.STRING,
        id_envio:{
            type: Sequelize.BIGINT,
            references: {
                model: 'envio', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        respuesta:Sequelize.STRING,
    });
    return envio_historico;
}
