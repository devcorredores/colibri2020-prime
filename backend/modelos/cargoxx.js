module.exports = (sequelize, Sequelize) => {
    const cargoxx = sequelize.define('cargoxx', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        tipo: Sequelize.STRING,
        clasificacion: Sequelize.STRING,
        anio: Sequelize.BIGINT,
        tipo_activo: Sequelize.STRING,
        tasa_x_plazo: Sequelize.FLOAT,
        tasa: Sequelize.FLOAT
    });
    return cargoxx;
}