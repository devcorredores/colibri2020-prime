module.exports = (sequelize, Sequelize) => {
    const tmp_cliente_cuenta = sequelize.define('tmp_cliente_cuenta', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        ci:Sequelize.STRING,
        ext:Sequelize.INTEGER,
        colibri_cod_agenda:Sequelize.STRING,
        colibri_cuenta:Sequelize.STRING,
        revision_cod_agenda:Sequelize.STRING,
        revision_cuenta:Sequelize.STRING,
        id_instancia_poliza:Sequelize.INTEGER,
        solucionado:Sequelize.BOOLEAN,
        revision_cueenta_seleccionada:Sequelize.STRING
    });
    return tmp_cliente_cuenta;
}
