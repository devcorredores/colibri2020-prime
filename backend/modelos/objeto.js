module.exports = (sequelize, Sequelize) => {
    const objeto = sequelize.define('objeto', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_poliza: {
            type: Sequelize.BIGINT,
            references: {
               model: 'poliza', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_tipo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        descripcion:Sequelize.STRING,    
        adicionada_por:Sequelize.STRING,
        modificada_por:Sequelize.STRING
    });
    return objeto;
}
