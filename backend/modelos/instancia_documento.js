module.exports = (sequelize, Sequelize) => {

    const instancia_documento = sequelize.define('instancia_documento', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_instancia_poliza: {
            type: Sequelize.BIGINT,
            references: {
               model: 'instancia_poliza', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_instancia_archivo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'instancia_archivo', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_documento: {
            type: Sequelize.BIGINT,
            references: {
               model: 'documento', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        nro_documento:Sequelize.STRING,
        fecha_emision:Sequelize.DATE,
        fecha_inicio_vigencia:Sequelize.DATE,
        fecha_fin_vigencia:Sequelize.DATE,
        asegurado_doc_id:Sequelize.STRING,
        asegurado_doc_id_ext:Sequelize.STRING,
        asegurado_nombre1:Sequelize.STRING,
        asegurado_nombre2:Sequelize.STRING,
        asegurado_apellido1:Sequelize.STRING,
        asegurado_apellido2:Sequelize.STRING,
        asegurado_apellido3:Sequelize.STRING,
        tipo_persona:Sequelize.STRING,
        adicionada_por:Sequelize.STRING,
        modificada_por:Sequelize.STRING,
        nombre_archivo:Sequelize.STRING
    });
    return instancia_documento;
}
