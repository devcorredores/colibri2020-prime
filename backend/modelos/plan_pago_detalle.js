module.exports = (sequelize, Sequelize) => {
    const plan_pago_detalle = sequelize.define('plan_pago_detalle', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_instancia_poliza: {
            type: Sequelize.BIGINT,
            references: {
                model: 'instancia_poliza',
                key: 'id'
            }
        },
        id_planpago: {
            type: Sequelize.BIGINT,
            references: {
                model: 'plan_pago',
                key: 'id'
            }
        },
        nro_cuota_prog: Sequelize.BIGINT,
        fecha_couta_prog: Sequelize.DATE,
        pago_cuota_prog: Sequelize.FLOAT,
        interes_prog: Sequelize.FLOAT,
        prima_prog: Sequelize.FLOAT,
        pago_prima_acumulado: Sequelize.FLOAT,
        prima_pendiente: Sequelize.FLOAT,
        id_estado: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        id_estado_envio: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        id_instancia_archivo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'instancia_archivo',
                key: 'id'
            }
        },
        FechaPagoEjec: Sequelize.DATE,
        NroDocPagoEjec: Sequelize.BIGINT,
        OrigenActualizacion: Sequelize.STRING,
        adicionado_por: Sequelize.STRING(100),
        modificado_por: Sequelize.STRING(100),
        id_estado_det: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
    });
    return plan_pago_detalle;
}