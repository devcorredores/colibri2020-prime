// const crypto = require('crypto');

module.exports = (sequelize, Sequelize) => {

    const archivo = sequelize.define('archivo', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        descripcion: Sequelize.STRING,
        abreviacion: Sequelize.STRING,
        delimitador: Sequelize.STRING,
        formato: Sequelize.STRING,
        tipo: Sequelize.STRING,
        nro_actual: Sequelize.BIGINT,
        id_poliza: {
            type: Sequelize.BIGINT,
            references: {
                model: 'poliza',
                key: 'id'
            }
        },
        ubicacion_remota: Sequelize.STRING,
        ubicacion_local: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return archivo;
}
