module.exports = (sequelize, Sequelize) => {
    const planpago = sequelize.define('plan_pago', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_instancia_poliza: {
            type: Sequelize.BIGINT,
            references: {
                model: 'instancia_poliza',
                key: 'id'
            }
        },
        total_prima: Sequelize.FLOAT,
        interes: Sequelize.FLOAT,
        plazo_anos: Sequelize.BIGINT,
        periodicidad_anual: Sequelize.BIGINT,
        prepagable_postpagable: Sequelize.BIGINT,
        fecha_inicio: Sequelize.DATE,
        id_moneda: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        id_estado: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        adicionado_por: Sequelize.STRING(100),
        modificado_por: Sequelize.STRING(100),
    });
    return planpago;
}