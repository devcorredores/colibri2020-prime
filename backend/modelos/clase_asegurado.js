module.exports = (sequelize, Sequelize) => {
const clase_asegurado = sequelize.define('clase_asegurado', {
    id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true
    },
    id_poliza: {
        type: Sequelize.BIGINT,
        references: {
            model: 'poliza', 
            key: 'id'
        }
    },
    edad_minima:Sequelize.INTEGER,
    edad_maxima:Sequelize.INTEGER,
    nro_obligatorios: Sequelize.INTEGER,
    nro_requeridos: Sequelize.INTEGER,
    adicionado_por: Sequelize.STRING,
    modificado_por: Sequelize.STRING
});
return clase_asegurado;
}