module.exports = (sequelize, Sequelize) => {
    const rol_x_perfil = sequelize.define('rol_x_perfil', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_rol: {
            type: Sequelize.BIGINT,
            references: {
               model: 'rol', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         id_perfil: {
            type: Sequelize.BIGINT,
            references: {
               model: 'perfil', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },       
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return rol_x_perfil;
}
