module.exports = (sequelize, Sequelize) => {
    const reporte_query_parametro = sequelize.define('reporte_query_parametro', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        nombre: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        tipo: Sequelize.TEXT,
        id_reporte_query: {
            type: Sequelize.BIGINT,
            references: {
                model: 'reporte_query', 
                key: 'id'
            }
        },
        id_diccionario: {
            type: Sequelize.BIGINT,
            references: {
                model: 'diccionario', 
                key: 'id'
            }
        },
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return reporte_query_parametro;
}