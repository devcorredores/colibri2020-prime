module.exports = (sequelize, Sequelize) => {
    const rol = sequelize.define('rol', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        codigo: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        abreviacion: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
        cod_rel_banco: Sequelize.STRING
    });
    return rol;
}
