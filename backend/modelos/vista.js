module.exports = (sequelize, Sequelize) => {

    const vista = sequelize.define('vista', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_modulo: {
            type: Sequelize.BIGINT,
            references: {
               model: 'modulo', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
        codigo: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        ruta: Sequelize.STRING,
        icon: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return vista;
}



