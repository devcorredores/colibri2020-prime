module.exports = (sequelize, Sequelize) => {
    const cron_funcion = sequelize.define('cron_funcion', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        codigo: Sequelize.STRING,
        funcion: Sequelize.STRING,
        id_cron:{
            type: Sequelize.BIGINT,
            references: {
                model: 'cron', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        descripcion: Sequelize.STRING,
        id_tipo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return cron_funcion;
}
