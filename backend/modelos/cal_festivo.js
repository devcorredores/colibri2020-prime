module.exports = (sequelize, Sequelize) => {
    const cal_festivo = sequelize.define('Cal_festivo', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        fec_festivo:Sequelize.DATE,
        Descripcion:Sequelize.STRING,
        id_TipCalendario:Sequelize.BIGINT
    },{
        timestamps: false
    });
    return cal_festivo;
}
