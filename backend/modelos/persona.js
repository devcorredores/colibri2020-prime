// const crypto = require('crypto');

module.exports = (sequelize, Sequelize) => {
    const Persona = sequelize.define('persona', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        persona_doc_id: Sequelize.STRING(50),
        persona_doc_id_ext: Sequelize.STRING(10),
        persona_doc_id_comp: Sequelize.STRING(10),
        persona_primer_apellido: Sequelize.STRING(50),
        persona_segundo_apellido: Sequelize.STRING(50),
        persona_primer_nombre: Sequelize.STRING(50),
        persona_segundo_nombre: Sequelize.STRING(50),
        persona_apellido_casada: Sequelize.STRING(50),
        persona_direccion_domicilio: Sequelize.STRING(200),
        persona_direcion_trabajo: Sequelize.STRING(200),
        persona_fecha_nacimiento: Sequelize.DATE,
        persona_celular: Sequelize.STRING(50),
        persona_telefono_domicilio: Sequelize.STRING(50),
        persona_telefono_trabajo: Sequelize.STRING(50),
        persona_profesion: Sequelize.STRING(100),
        par_tipo_documento_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id',
            }
        },
        par_nacionalidad_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id',
            }
        },
        par_pais_nacimiento_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id',
            }
        }
        ,
        par_sexo_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id',
            }
        },
        persona_origen: Sequelize.STRING(50),
        adicionada_por:Sequelize.STRING(50),
        modificada_por:Sequelize.STRING(50),
        persona_email_personal:Sequelize.STRING(50)
    });
    return Persona;
}



