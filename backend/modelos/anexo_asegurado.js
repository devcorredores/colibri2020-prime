module.exports = (sequelize, Sequelize) => {
    const anexo_asegurado = sequelize.define('anexo_asegurado', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_anexo_poliza: {
            type: Sequelize.BIGINT,
            references: {
                model: 'anexo_poliza', 
                key: 'id'
            }
        },
        id_tipo: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', 
                key: 'id'
            }
        },
        edad_minima:Sequelize.INTEGER,
        edad_maxima:Sequelize.INTEGER,
        nro_obligatorios: Sequelize.INTEGER,
        nro_requeridos: Sequelize.INTEGER,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
        descripcion_otro: Sequelize.STRING,
        edad_min_permanencia: Sequelize.INTEGER,
        edad_max_permanencia: Sequelize.INTEGER,
        id_edad_unidad: Sequelize.INTEGER
    });
    return anexo_asegurado;
}
