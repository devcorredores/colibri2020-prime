module.exports = (sequelize, Sequelize) => {
    const documento = sequelize.define('documento', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_poliza: {
            type: Sequelize.BIGINT,
            references: {
                model: 'poliza', 
                key: 'id'
            }
        },
        descripcion: Sequelize.STRING,
        fecha_inicio_vigencia: Sequelize.DATE,
        fecha_fin_vigencia: Sequelize.DATE,
        fecha_emision: Sequelize.DATE,
        nro_inicial: Sequelize.BIGINT,
        nro_actual: Sequelize.BIGINT,
        id_tipo_documento: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        },
        id_documento_version: {
            type: Sequelize.BIGINT,
            references: {
                model: 'documento_version',
                key: 'id'
            }
        },
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
    });
    return documento;
}
