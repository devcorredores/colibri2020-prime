module.exports = (sequelize, Sequelize) => {
    const dato_anexo = sequelize.define('dato_anexo_asegurado', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_anexo_asegurado: {
            type: Sequelize.BIGINT,
            references: {
                model: 'anexo_asegurado', 
                key: 'id'
            }
        },
        descripcion: Sequelize.STRING,
        visible: Sequelize.BOOLEAN,
        editable: Sequelize.BOOLEAN,
        obligatorio: Sequelize.BOOLEAN,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return dato_anexo;
}
