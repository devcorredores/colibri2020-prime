module.exports = (sequelize, Sequelize) => {

    const instancia_poliza = sequelize.define('instancia_poliza', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_poliza: {
            type: Sequelize.BIGINT,
            references: {
               model: 'poliza', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
        },
        id_estado: {
            type: Sequelize.BIGINT,
            references: {
               model: 'parametro', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
        id_anexo_poliza: {
            type: Sequelize.BIGINT,
            references: {
               model: 'anexo_poliza', // 'persons' refers to table name
               key: 'id', // 'id' refers to column name in persons table
            }
         },
         fecha_registro:Sequelize.DATE,
         adicionada_por:Sequelize.STRING,
         modificada_por:Sequelize.STRING,
         id_instancia_renovada:Sequelize.BIGINT,
    });
    return instancia_poliza;
}
