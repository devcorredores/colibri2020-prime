module.exports = (sequelize, Sequelize) => {

    const perfil_x_componente = sequelize.define('perfil_x_componente', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_perfil: {
           type: Sequelize.BIGINT,
           references: {
              model: 'perfil', // 'persons' refers to table name
              key: 'id', // 'id' refers to column name in persons table
           }
        },
        id_componente: {
           type: Sequelize.BIGINT,
           references: {
              model: 'componente', // 'persons' refers to table name
              key: 'id', // 'id' refers to column name in persons table
           }
        },
        observacion: Sequelize.STRING,
        estado: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return perfil_x_componente;
}
