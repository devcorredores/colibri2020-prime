// const crypto = require('crypto');

module.exports = (sequelize, Sequelize) => {

    const perfil = sequelize.define('perfil', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        codigo: Sequelize.STRING,
        descripcion: Sequelize.STRING,
        abreviacion: Sequelize.STRING,
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return perfil;
}



