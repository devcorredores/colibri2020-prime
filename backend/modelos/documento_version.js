module.exports = (sequelize, Sequelize) => {
    const documento_version = sequelize.define('documento_version', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        descripcion: Sequelize.STRING,
        html: Sequelize.STRING,
        top: Sequelize.STRING,
        rigth: Sequelize.STRING,
        bottom: Sequelize.STRING,
        left: Sequelize.STRING,
        height: Sequelize.STRING,
        width: Sequelize.STRING,

        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING,
        id_reporte_query: {
            type: Sequelize.BIGINT,
            references: {
                model: 'reporte_query', // 'persons' refers to table name
                key: 'id', // 'id' refers to column name in persons table
            }
        },
    });
    return documento_version;
}
