module.exports = (sequelize, Sequelize) => {
    const instancia_anexo_asegurado = sequelize.define('instancia_anexo_asegurado', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        id_entidad: {
            type: Sequelize.BIGINT,
            references: {
                model: 'entidad', 
                key: 'id'
            }
        },
        id_anexo_asegurado: {
            type: Sequelize.BIGINT,
            references: {
                model: 'anexo_asegurado', 
                key: 'id'
            }
        },
        id_asegurado: {
            type: Sequelize.BIGINT,
            references: {
                model: 'asegurado', 
                key: 'id'
            }
        },
        id_parentesco: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', 
                key: 'id'
            }
        },
        id_relacion: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', 
                key: 'id'
            }
        },
        id_estado: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro', 
                key: 'id'
            }
        },
        adicionado_por: Sequelize.STRING,
        modificado_por: Sequelize.STRING
    });
    return instancia_anexo_asegurado;
}
