
module.exports = (sequelize, Sequelize) => {
    const Usuario_x_parametro = sequelize.define('usuario_x_parametro', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        usuario_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'diccionario',
                key: 'id'
            }
        },
        par_autenticacion_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'parametro',
                key: 'id'
            }
        }
    });
    return Usuario_x_parametro;
}