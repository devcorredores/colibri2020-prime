const modelos = require('../modelos');

function generarMenu(modulos){
    let menus_padres=modulos.find(menu => menu.id_modulo_super === null );

    menus_padres.forEach(element => {
        BuscarMenusHijos(element.id, modulos);
    });
};

function BuscarMenusHijos(id_super, modulos){
    let menus_hijos=modulos.find(menu => menu.id_modulo_super === id_super );
    menus_hijos.forEach(element => {
        BuscarMenusHijos(element.id,modulos);
    });
    BuscarVistas(id_super);
};

async function BuscarVistas(id_super){
    await modelos.sequelize.query(`select * from perfil_x_vista A inner join vista B on A.id_vista=B.id and B.id_modulo=${id_super} and A.estado='A'`)
    .then(([results, metadata]) => {
        
     });
};

module.exports = generarMenu;
