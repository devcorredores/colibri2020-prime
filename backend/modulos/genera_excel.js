const modelos = require("../modelos");
const path = require('path');
generarReporteExcelTarjetas = async (fecha_emision, nombre_archivo) => {
    return new Promise(async (resolve, reject) => {
     modelos.sequelize.query(`select
    --A.id ,
    isnull((select parametro_descripcion from parametro where J.valor=parametro_cod  and diccionario_id=38),'') SUCURSAL,
    isnull((select parametro_descripcion from parametro where I.valor=parametro_cod  and diccionario_id=40),'') AGENCIAa,
    '' TIPO_AGENCIA,
    isnull((select DISTINCT valor from atributo_instancia_poliza where id_instancia_poliza=A.id and id_objeto_x_atributo=167),'') CARGO,
    F.usuario_nombre_completo FUNCIONARIO,
    E.nro_documento NRO_DE_CERTIFICAOD,
    isnull(D.persona_primer_nombre,'')+' '+isnull(D.persona_segundo_nombre,'') NOMBRES,
    isnull(D.persona_primer_apellido,'') PATERNO,
    isnull(D.persona_segundo_apellido ,'') MATERNO,
    isnull(D.persona_apellido_casada,'') APELLIDO_CASADA,
    H.parametro_cod  TIPO_DOCUMENTO,
    D.persona_doc_id NUMERO_DOCUMENTO,
    D.persona_doc_id_comp COMPLEMENTO,
    G.parametro_descripcion EXTENSION,
    case K.parametro_descripcion 
     when 'FEMENINO' THEN  'mujer'
     when 'MASCULINO' THEN  'hombre'
     end SEXO,
    FORMAT(D.persona_fecha_nacimiento,'dd/MM/yyyy') FECHA_DE_NACIMIENTO,
    isnull(D.persona_celular ,'') TELEFONO,
    D.persona_direccion_domicilio DIRECCION,
    'Si' EMITIDO,
    FORMAT(E.fecha_emision,'dd/MM/yyyy') FECHA_REAL_DE_EMISION
   from instancia_poliza A 
   inner join asegurado B on A.id=B.id_instancia_poliza
   inner join entidad C on B.id_entidad=C.id
   inner join persona D on C.id_persona=D.id
   inner join instancia_documento E on A.id=E.id_instancia_poliza and id_documento=6
   inner join usuario F on A.adicionada_por=F.id+''
   inner join parametro G on D.persona_doc_id_ext=G.parametro_cod and diccionario_id=18  
   inner join parametro H on D.par_tipo_documento_id=H.id                                    
   inner join atributo_instancia_poliza I on I.id_objeto_x_atributo=148 and I.id_instancia_poliza=A.id 
   inner join atributo_instancia_poliza J on J.id_objeto_x_atributo=149 and J.id_instancia_poliza=A.id
   inner join parametro K on D.par_sexo_id=K.id   
   inner join atributo_instancia_poliza L on L.id_objeto_x_atributo=167 and L.id_instancia_poliza=A.id
   and A.id_poliza=2
   and A.id_estado=59
   and  convert(varchar(8),E.fecha_emision,112) = ${fecha_emision}
   order by 8,9`)
            .then(async result => {
                await exportExcel(result[0],nombre_archivo);
                resolve(result[0]);
            }
            ).catch(err => {
                resolve(err);
            });
    });
}

async function exportExcel(Datos,nombre_archivo) {
    if(typeof XLSX == 'undefined') XLSX = require('xlsx');

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(Datos);
    
    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");
    
    /* generate an XLSX file */
    const archivo = path.join(__dirname, `../public/xlsx/${nombre_archivo}.xlsx`);
    await XLSX.writeFile(wb,archivo);
  }

module.exports.generarReporteExcelTarjetas = generarReporteExcelTarjetas;
module.exports.exportExcel = exportExcel;
