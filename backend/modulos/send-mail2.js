
const nodemailer = require("nodemailer");

class SendMail {
  static sendMail(mailOptions) {
    let resp = "";

    const transporter = nodemailer.createTransport({
      host: "smtp.office365.com",
      port: 587,
      secure: false, // upgrade later with STARTTLS
      secureConnection: false,
      auth: {
        user: "contacto@corredoresecofuturo.com.bo",
        pass: "Password123"
      }
    });

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log("=====> ERROR_SEND_MAIL:".error);
        resp = `ERROR,${error}`;
      } else {
        console.log(`=====> Email sent: ${info.response}`);
        resp = `enviado,${JSON.stringify(info.response)}`;
      }
    });

    return resp;
  }
}

module.exports = SendMail;
