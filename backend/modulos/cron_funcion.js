require('../utils/Prototipes');
const fechas = require('fechas');
const modelos = require("../modelos");
const moment = require("moment");
const path = require('path');
const SendMailModule = require("./send-mail3");
const sftp = require("./sftp");
const fs = require('fs');
const vigenciaService = require('../servicios/vigencia.service');
const {loggerSolicitud} = require("../modulos/winston");
const funcionesArchivo = require("../modulos/funciones_archivo");

module.exports.tareaGenerica = async function (cronFuncionParametros, cronFuncion = null, callback = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams, reporteFunction] = await distribuirParametros(cronFuncionParametros);

             await excuteFunction(reporteFunction, parametros, null, async () => {

                 [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, null, cronFuncion);

                 [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros, null, null);

                 [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir, async (fileExt,dirFileExt,dir) => {

                     response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros, async () => {

                         if(typeof callback == 'function') {
                             await callback();
                         }
                     });
                 });
             });
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.tareaDescargasFTP = async function (cronFuncionParametros, cronFuncion = null, callback = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams, reporteFunction] = await distribuirParametros(cronFuncionParametros);

            await verificaDescargasFTP(configFtp,parametros, async (file, fileExt, dirFileExt, dir, parametros) => {

                await excuteFunction(reporteFunction, parametros, file, async () => {

                    [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, file, cronFuncion);

                    [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros, fileExt, dirFileExt, dir);

                    [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir, async (fileExt,dirFileExt,dir) => {

                         response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros, async () => {

                             if(typeof callback == 'function') {
                                 await callback();
                             }
                         });
                    });
                });
            });
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.tareaCargasFTP = async function (cronFuncionParametros, cronFuncion = null, callback = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams, reporteFunction] = await distribuirParametros(cronFuncionParametros);

            await verificaDescargasFTP(configFtp,parametros, async (file, fileExt, dirFileExt, dir, parametros) => {

                await excuteFunction(reporteFunction, parametros, file, async () => {

                    [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, file, cronFuncion);

                    [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros, fileExt, dirFileExt, dir);

                    [fileExt,dirFileExt,dir] = await creacionArchivosWithBom(respReporteQuery, fileExt, dirFileExt, dir, async (fileExt,dirFileExt,dir) => {

                        await verificaCargasFTP(configFtp, configArchivo, fileExt, dirFileExt, dir, parametros, async (envio, fileExt, dirFileExt, dir, localDirectory, remoteDirectory) => {

                            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros, async () => {

                                if(typeof callback == 'function') {
                                    callback();
                                }
                            });
                        });
                    });
                });
            });
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

// Tareas dedicadas

module.exports.cambioEstadoCaducado = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.cambioEstadoVencido = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.reporteSemanalEcotarjetas = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.reporteSemanalEcoAguinaldoEcoPasanaku = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.reporteSemanalEcopasanaku = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.reporteDiarioEcomedicv = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, parComparaciones, parUnidades, funParams, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.reporteFinDeMesEcoTarjetas = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.reporteFinDeMesEcoPasanakuEcoAguinaldo = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.reporteSemanalEcoVidaEcoProteccion = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.reporteDiarioEcopasanaku = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams] = await distribuirParametros(cronFuncionParametros);

            [respReporteQuery] = await verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, cronFuncion);

            [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

            [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir);

            response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.envioFtpReporte = async function (cronFuncionParametros, cronFuncion = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (cronFuncionParametros.length) {

            let [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams, reporteFuncion] = await distribuirParametros(cronFuncionParametros);

            [dirFileExt, fileExt] = await enviarArchivosViaFtp(reporteFuncion, parametros, async (err, localPath, localFile, remotePath, remoteFile) => {
                if (err) {
                    console.log(err);
                } else {
                    response = await enviarCorreoArchivoAdjunto(configCorreo, localPath+localFile, localFile, null, parametros);
                }
            });

        }
        return response;
    } catch (e) {
        console.log(e)
    }
};

module.exports.renuevaSolicitudesVencidas = async function (cronFuncionParametros, cronFuncion = null) {
    let dete = new Date;

    let response = {status: "OK", message: "", data: {}};

    [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams, reporteFunction] = await distribuirParametros(cronFuncionParametros);

    let respReporteQuery = await renovarSolicitudes(parametros);

    [fileExt,dirFileExt,dir] = await verificacionArchivoSalida(configArchivo, parametros);

    [fileExt,dirFileExt,dir] = await creacionArchivos(respReporteQuery, fileExt, dirFileExt, dir,async (fileExt,dirFileExt) => {

        response = await enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros);
    });


}

async function renovarSolicitudes(parametros) {
    let responseRenovaciones;

    for (let i = 0 ; i < parametros.length ; i++) {
        let parametro = parametros[i];
        let date = new Date();
        let polizas = parametro.valor.split(',');
        loggerSolicitud.info(`--- Se inicia la renovación de polizas automatica ${date.toString()} ---`);
        for (let j = 0 ; j < polizas.length ; j++) {
            let idPoliza = polizas[j];
            let req = {};
            req.params = {};
            req.params.idPoliza = idPoliza;
            req.params.today = date;
            responseRenovaciones = await vigenciaService.verificaVigenciaRenovando(req);
            loggerSolicitud.info(`Se renovaron las solicitudes vencidas (${date.toString()}) para la poliza id: ${idPoliza}`,responseRenovaciones);
        }
    }
    return responseRenovaciones;
}

async function distribuirParametros(cronFuncionParametros) {
    try {
        let parametros = [], configCorreo, parComparaciones = [], parUnidades = [], funParams = [], configFtp, reporteQuery, reporteProcedimiento, reporteFuncion, configArchivo;
        let numParams = 0;
        let poliza, polizas = await modelos.poliza.findAll();
        let respReporte, respReporteProcedimiento;
        let procedimiento, query, unidad, day, month, year, momDate, fechaInv;
        let parRepTipoMensual = 268;
        let date = new Date();
        let parRepTipoSemanal = 269;
        let parRepTipoDiario = 290;
        let parComparacionAbsoluta = 245;
        let parComparacionRelativa = 246;
        let parDay = 239;
        let parMOnth = 240;
        let parYear = 241;
        let dateStr;
        let cronFuncionParametrosFiltered = cronFuncionParametros.filter(param => param.dataValues.comparacion);
        for (let i = 0; i < cronFuncionParametrosFiltered.length; i++) {
            let cronFuncionParametroFiltered = cronFuncionParametrosFiltered[i];
            if (cronFuncionParametroFiltered.comparacion && Object.keys(cronFuncionParametroFiltered.comparacion).length) {
                if(cronFuncionParametroFiltered.dataValues.unidad){
                    parUnidades[numParams] = cronFuncionParametroFiltered.dataValues.unidad.dataValues;
                }
                if(cronFuncionParametroFiltered.dataValues.comparacion){
                    parComparaciones[numParams] = cronFuncionParametroFiltered.dataValues.comparacion.dataValues;
                }
                numParams++;
                funParams.push(cronFuncionParametroFiltered.dataValues);
                if (cronFuncionParametroFiltered.dataValues.valor) {
                    let valorParametro = cronFuncionParametroFiltered.dataValues.valor;
                    if(cronFuncionParametroFiltered.dataValues.tipo == 'query'){
                        let currentQueryParamFound = cronFuncionParametrosFiltered.find(param => cronFuncionParametroFiltered.valor.includes(param.nombre));
                        let parametroQuery = currentQueryParamFound ? cronFuncionParametroFiltered.dataValues.valor.replaceAll(currentQueryParamFound.nombre, currentQueryParamFound.valor) : cronFuncionParametroFiltered.valor;
                        let respParametroQuery = await modelos.sequelize.query(parametroQuery);
                        let objValorParametro = respParametroQuery[0][0];
                        for (const property in objValorParametro) {
                            valorParametro = objValorParametro[property];
                        }
                    }
                    if (cronFuncionParametroFiltered.dataValues.tipo.includes('poliza')) {
                        let polizaFound = polizas && polizas.length ? polizas.find(param => param.id == cronFuncionParametroFiltered.dataValues.valor) : null;
                        cronFuncionParametroFiltered.dataValues.sub_valor = polizaFound ? polizaFound.descripcion : null;
                    }
                    if (cronFuncionParametroFiltered.dataValues.tipo.includes('date')) {
                        let format;
                        if(cronFuncionParametroFiltered.dataValues.tipo.includes('date_format_')) {
                            format = cronFuncionParametroFiltered.dataValues.tipo.split('date_format_')[1];
                        }
                        if (cronFuncionParametroFiltered.dataValues.comparacion.id == parComparacionRelativa) {
                            let dateStr = moment(date).format("DD/MM/YYYY");
                            unidad = parUnidades[i];
                            if (unidad.id == parDay) {
                                momDate = moment(date).add('days',parseInt(cronFuncionParametroFiltered.dataValues.valor));
                            }
                            if (unidad.id == parMOnth) {
                                momDate = moment(date).add('months',parseInt(cronFuncionParametroFiltered.dataValues.valor));
                            }
                            if (unidad.id == parYear) {
                                momDate = moment(date).add('years',parseInt(cronFuncionParametroFiltered.dataValues.valor));
                            }
                            if (format) {
                                valorParametro = cronFuncionParametroFiltered.dataValues.valor = `'${format.replaceAll('dd',momDate.get('day')).replaceAll('mm',momDate.get('month')).replaceAll('aaaa',momDate.get('year'))}'` ;
                            } else {
                                valorParametro = cronFuncionParametroFiltered.dataValues.valor = fechas.setFormatoFecha(momDate.format('DD/MM/YYYY'), 'INVERSOPLANO');
                            }
                            cronFuncionParametroFiltered.dataValues.sub_valor = momDate.format('DD-MM-YYYY');
                            cronFuncionParametroFiltered.dataValues.dateStr = momDate.format('DD/MM/YYYY');
                        } else if (cronFuncionParametroFiltered.dataValues.comparacion.id == parComparacionAbsoluta) {
                            unidad = parUnidades[i];
                            [day, month, year] = cronFuncionParametroFiltered.dataValues.valor.split('-')
                            day = day.replaceAll('dd',date.getDate()).padZeros(2);
                            month = month.replaceAll('mm',date.getMonth() + 1).padZeros(2);
                            year = year.replaceAll('aaaa',date.getFullYear());
                            dateStr = day+'/'+month+'/'+year;
                            if (format) {
                                valorParametro = cronFuncionParametroFiltered.dataValues.valor = `'${format.replaceAll('dd',day).replaceAll('mm',month).replaceAll('aaaa',year)}'` ;
                            } else {
                                valorParametro = cronFuncionParametroFiltered.dataValues.valor = fechas.setFormatoFecha(dateStr, 'INVERSOPLANO');
                            }
                            cronFuncionParametroFiltered.dataValues.sub_valor = dateStr.replaceAll('/','-');
                            cronFuncionParametroFiltered.dataValues.dateStr = dateStr;
                        }
                    }
                    parametros.push({
                        nombre: cronFuncionParametroFiltered.nombre,
                        comparacion: cronFuncionParametroFiltered.dataValues.comparacion ? cronFuncionParametroFiltered.dataValues.comparacion.dataValues : null,
                        unidad: cronFuncionParametroFiltered.dataValues.unidad ? cronFuncionParametroFiltered.dataValues.unidad.dataValues : null,
                        valor: valorParametro,
                        tipo: cronFuncionParametroFiltered.dataValues.tipo,
                        order: cronFuncionParametroFiltered.dataValues.order,
                        sub_valor:cronFuncionParametroFiltered.dataValues.sub_valor,
                        dateStr:cronFuncionParametroFiltered.dataValues.dateStr
                    });
                }
            }
        }
        for (let i = 0; i < cronFuncionParametros.length; i++) {
            let cronFuncionParametro = cronFuncionParametros[i];
            if (cronFuncionParametro.dataValues.tipo == 'poliza') {
                poliza = polizas.find(param => param.id == cronFuncionParametro.dataValues.valor)
            }
            configCorreo = cronFuncionParametro && !cronFuncionParametro.comparacion && cronFuncionParametro.tipo == 'correo' && cronFuncionParametro.config_correo && !configCorreo ? cronFuncionParametro.config_correo.dataValues : configCorreo;
            configFtp = cronFuncionParametro && !cronFuncionParametro.comparacion && cronFuncionParametro.tipo == 'ftp' && cronFuncionParametro.config_ftp && !configFtp ? cronFuncionParametro.config_ftp.dataValues : configFtp;
            configArchivo = cronFuncionParametro && !cronFuncionParametro.comparacion && cronFuncionParametro.tipo == 'archivo' && !configArchivo ? cronFuncionParametro.dataValues : configArchivo;
            reporteQuery = cronFuncionParametro && !cronFuncionParametro.comparacion && cronFuncionParametro.tipo == 'query' && cronFuncionParametro.reporte_query && !reporteQuery ? cronFuncionParametro.reporte_query.dataValues : reporteQuery;
            if(cronFuncionParametro.dataValues.tipo && !cronFuncionParametro.comparacion && cronFuncionParametro.dataValues.tipo == 'procedimiento') {
                reporteProcedimiento = reporteProcedimiento == null ? cronFuncionParametro.dataValues : reporteProcedimiento;
            }
            if(cronFuncionParametro.dataValues.tipo && cronFuncionParametro.dataValues.tipo == 'funcion') {
                reporteFuncion = reporteFuncion == null ? cronFuncionParametro.dataValues : reporteFuncion;
            }
        }
        return [reporteProcedimiento, reporteQuery, parametros, parComparaciones, parUnidades, configCorreo, configFtp, configArchivo, funParams, reporteFuncion]
    } catch (e) {
        console.log(e)
    }
}

async function enviarArchivosViaFtp(reporteFuncion, parametros, callback = null) {
    let response = { status: 'OK', message: '', data: '' };
    let dirFile, file, param1, param2, param3, param4, param5, param6;
    for (let i = 0 ; i < parametros.length ; i++) {
        let parametro = parametros[i];
        parametro.valor = await setParametro(parametro.valor);

        switch (parametro.order) {
            case 1: param1 = parametro; break;
            case 2: param2 = parametro; break;
            case 3: param3 = parametro; break;
            case 4: param4 = parametro; break;
            case 5: param5 = parametro; break;
            case 6: param6 = parametro; break;
        }
    }
    if (reporteFuncion) {
        let objData = await planPagoService.getPlanPagoProcEcoproteccion(param1, param2, param3, param4);
        response = await funcionesArchivo.crearTextoArchivoEnviar(objData,param5,param2);
    }
    return response;
}

async function excuteFunction(configFunction, parametros, extraFile = null, callback = null) {
    try {
        if (configFunction) {
            if (typeof funcionesArchivo[configFunction.valor] == 'function') {
                return await funcionesArchivo[configFunction.valor](parametros,extraFile,callback);
            } else {
                if (typeof callback == 'function') {
                    await callback()
                }
            }
        } else {
            if (typeof callback == 'function') {
                await callback()
            }
        }
    } catch (e) {
        console.log(e);
    }
}

async function enviarCorreoArchivoAdjunto(configCorreo, dirFileExt, fileExt, dir, parametros, callback = null) {
    try {
        let response = {status: "OK", message: "", data: {}};
        if (Object.keys(configCorreo).length) {
            let date = new Date(), day, month, year;
            day = date.getDate();
            month = date.getMonth() + 1;
            year = date.getFullYear();
            if(configCorreo.asunto) {
                configCorreo.asunto = configCorreo.asunto.replaceAll('dd', day);
                configCorreo.asunto = configCorreo.asunto.replaceAll('mm', month);
                configCorreo.asunto = configCorreo.asunto.replaceAll('aaaa', year);
                if (parametros) {
                    parametros.forEach((parametro) => {
                        if (parametro.sub_valor) {
                            configCorreo.asunto = configCorreo.asunto.replaceAll(parametro.nombre, parametro.sub_valor);
                        } else if (parametro.valor) {
                            configCorreo.asunto = configCorreo.asunto.replaceAll(parametro.nombre, parametro.valor);
                        }
                    });
                }
            }
            if(configCorreo.texto) {
                configCorreo.texto = configCorreo.texto.replaceAll('dd', day);
                configCorreo.texto = configCorreo.texto.replaceAll('mm', month);
                configCorreo.texto = configCorreo.texto.replaceAll('aaaa', year);
                if (parametros) {
                    parametros.forEach((parametro) => {
                        if (parametro.sub_valor) {
                            configCorreo.texto = configCorreo.texto.replaceAll(parametro.nombre, parametro.sub_valor);
                        } else if(parametro.valor) {
                            configCorreo.texto = configCorreo.texto.replaceAll(parametro.nombre, parametro.valor);
                        }
                    });
                }
            }
            if(configCorreo.texto_vacio) {
                configCorreo.texto_vacio = configCorreo.texto_vacio.replaceAll('dd', day);
                configCorreo.texto_vacio = configCorreo.texto_vacio.replaceAll('mm', month);
                configCorreo.texto_vacio = configCorreo.texto_vacio.replaceAll('aaaa', year);
                if (parametros) {
                    parametros.forEach((parametro) => {
                        if(parametro.sub_valor) {
                            configCorreo.texto_vacio = configCorreo.texto_vacio.replaceAll(parametro.nombre, parametro.sub_valor);
                        } else if(parametro.valor) {
                            configCorreo.texto_vacio = configCorreo.texto_vacio.replaceAll(parametro.nombre, parametro.valor);
                        }
                    });
                }
            }
            if (configCorreo.html) {
                configCorreo.html = configCorreo.html.replaceAll('dd', day);
                configCorreo.html = configCorreo.html.replaceAll('mm', month);
                configCorreo.html = configCorreo.html.replaceAll('aaaa', year);
                if (parametros) {
                    parametros.forEach((parametro) => {
                        if(parametro.sub_valor) {
                            configCorreo.html = configCorreo.html.replaceAll(parametro.nombre, parametro.sub_valor)
                        } else if(parametro.valor) {
                            configCorreo.html = configCorreo.html.replaceAll(parametro.nombre, parametro.valor)
                        }
                    });
                }
            }
            if(fileExt) {
                fileExt = fileExt.replaceAll('dd', day);
                fileExt = fileExt.replaceAll('mm', month);
                fileExt = fileExt.replaceAll('aaaa', year);
                if (parametros) {
                    parametros.forEach((parametro) => {
                        if(parametro.sub_valor) {
                            fileExt = fileExt.replaceAll(parametro.nombre, parametro.sub_valor)
                        } else if(parametro.valor) {
                            fileExt = fileExt.replaceAll(parametro.nombre, parametro.valor)
                        }
                    });
                }
            }
            if(dirFileExt) {
                dirFileExt = dirFileExt.replaceAll('dd', day);
                dirFileExt = dirFileExt.replaceAll('mm', month);
                dirFileExt = dirFileExt.replaceAll('aaaa', year);
                if (parametros) {
                    parametros.forEach((parametro) => {
                        if(parametro.sub_valor) {
                            dirFileExt = dirFileExt.replaceAll(parametro.nombre, parametro.sub_valor)
                        } else if(parametro.valor) {
                            dirFileExt = dirFileExt.replaceAll(parametro.nombre, parametro.valor)
                        }
                    });
                }
            }
            let resp = '';
            let mailOptions = {};
            if (dirFileExt && fs.existsSync(dirFileExt)) {
                mailOptions = {
                    from: '"' + configCorreo.nombre_remitente + '" <' + configCorreo.correo_remitente + '>', // sender address
                    html: configCorreo.html,
                    to: configCorreo.para,
                    cc: configCorreo.con_copia,
                    bcc: configCorreo.con_copia_oculta,
                    attachments: [
                        {
                            filename: fileExt,
                            path: dirFileExt
                        }
                    ],
                    subject: configCorreo.asunto,
                    text: configCorreo.texto
                };
                resp = await SendMailModule.toSendMail(mailOptions, configCorreo.correo_remitente, configCorreo.contraseña_remitente,configCorreo.host,configCorreo.port);
                response.message = resp;
                if (typeof callback == 'function') {
                    await callback();
                }
            } else {
                mailOptions = {
                    from: '"' + configCorreo.nombre_remitente + '" <' + configCorreo.correo_remitente + '>', // sender address
                    html: configCorreo.html, // html body
                    to: configCorreo.para,
                    cc: configCorreo.con_copia,
                    bcc: configCorreo.con_copia_oculta,
                    subject: configCorreo.asunto,
                    text: configCorreo.texto_vacio
                };
                resp = await SendMailModule.toSendMail(mailOptions, configCorreo.correo_remitente, configCorreo.contraseña_remitente,configCorreo.host,configCorreo.port);
                response.message = resp;
                if (typeof callback == 'function') {
                    await callback();
                }
            }
            response.data = resp;
        } else {
            if (typeof callback == 'function') {
                await callback();
            }
        }
        return response;
    } catch (e) {
        console.log(e)
    }
}

async function verificacionArchivoSalida(configArchivo, parametros, fileExt = null, dirFileExt = null, dir = null) {
    let file, ext;
    let date = new Date(), day, month, year;
    let parPath = parametros.find(param => param.nombre == '@path');
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();

    if (configArchivo && configArchivo.tipo == 'archivo') {
	    if (configArchivo.nombre) {
	      configArchivo.nombre = configArchivo.nombre.replaceAll('dd', (day+'').padZeros(2));
	      configArchivo.nombre = configArchivo.nombre.replaceAll('mm', (month+'').padZeros(2));
	      configArchivo.nombre = configArchivo.nombre.replaceAll('aaaa', year);
	      if (parametros) {
	          parametros.forEach((parametro) => {
                  if (parametro.valor) {
                      configArchivo.nombre = configArchivo.nombre.replaceAll(parametro.nombre, parametro.valor)
                  } else if (parametro.sub_valor) {
	                  configArchivo.nombre = configArchivo.nombre.replaceAll(parametro.nombre, parametro.sub_valor)
	              }
	          });
	      }
	    }

	    if (Object.keys(configArchivo).length) {
	        ext = configArchivo.nombre.indexOf('.') >= 0 ? configArchivo.nombre.split('.')[1] : configArchivo.valor;
	        file = configArchivo.nombre.indexOf('.') >= 0 ? configArchivo.nombre.split('.')[0] : configArchivo.nombre;
	        fileExt = file + '.' + ext;
	        if (parPath) {
                dir = path.join(__dirname,`..${parPath.valor}`);
            } else {
                dir = path.join(__dirname,`../public/${ext}/`);
            }
	        dirFileExt = path.join(dir + fileExt);
	    }
    }
    return [fileExt,dirFileExt,dir]
}

async function verificaDescargasFTP(configFtp, parametros, callback) {
    try {
        if (configFtp && configFtp.codigo == 353) {
            var config = {
                host: configFtp.host,
                port: configFtp.puerto,
                username: configFtp.usuario,
                password: configFtp.password,
                type : parseInt(configFtp.puerto+'') == 22 ? 'sftp' : parseInt(configFtp.puerto+'') == 21 ? 'ftp' : null
            };
            sftp.downloadFileTipoPago(config, configFtp, parametros, configFtp.remote_path,configFtp.local_path,callback);
        } else {
            if (typeof callback == 'function') {
                await callback(null, null, null, null, parametros);
            }
        }
    } catch (e) {
        console.log(e);
    }
}

async function verificaCargasFTP(configFtp, configArchivo, fileExt, dirFileExt, dir, parametros, callback) {
    try {
        if (configFtp && configArchivo && fileExt && dirFileExt && configFtp.codigo == 354) {
            var config = {
                host: configFtp.host,
                port: configFtp.puerto,
                username: configFtp.usuario,
                password: configFtp.password,
                type : parseInt(configFtp.puerto+'') == 22 ? 'sftp' : parseInt(configFtp.puerto+'') == 21 ? 'ftp' : null
            };
            sftp.uploadFileTipoCobro(config, configFtp, configArchivo, fileExt, dirFileExt, dir, parametros, configFtp.remote_path,configFtp.local_path,callback);
        } else {
            if (typeof callback == 'function') {
                await callback(null, fileExt, dirFileExt, dir);
            }
        }
    } catch (e) {
        console.log(e);
    }
}

async function verificacionReporteQuery(reporteQuery, reporteProcedimiento, parametros, extraFile, cronFuncion) {
    // Verificacion reporte query
    try {
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        let respReporte, respReporteProcedimiento;
        let procedimiento, query;
        let parRepTipoMensual = 268;
        if (parametros.length) {
            // let parFechas = [];
            // if (cronFuncion && cronFuncion.id_tipo == parRepTipoMensual) {
            //     let months = [];
            //     for (let j = 0 ; j < parametros.length ; j++) {
            //         let parametro = parametros[j];
            //         if (parametro.tipo == 'date') {
            //             let [repDay,repMonth,repYear] = parametro.dateStr.split('/');
            //             months.push(parseInt(repMonth));
            //             if (repDay && repMonth && repYear) {
            //                 parFechas[j] = {day:repDay, month:repMonth, year:repYear};
            //             }
            //         }
            //     }
            //     let minMonth = Math.min.apply(Math, months);
            //     for (let k = 0 ; k < parFechas.length ; k++) {
            //         let parFecha = parFechas[k];
            //         if (parFecha) {
            //             parFechas[k] = {day:parFecha.day, month:minTwoDigits(minMonth), year:parFecha.year};
            //         }
            //     }
            //     for (let l = 0 ; l < parametros.length ; l++) {
            //         if (parametros[l].tipo == 'date') {
            //             let objNewDate = parFechas[l];
            //             let newStrDate = objNewDate.day+'/'+objNewDate.month+'/'+objNewDate.year;
            //             parametros[l].valor = fechas.setFormatoFecha(newStrDate, 'INVERSOPLANO');
            //             parametros[l].sub_valor = newStrDate.replaceAll('/','-');
            //             parametros[l].dateStr = newStrDate;
            //         }
            //     }
            // }

            if(reporteProcedimiento != undefined) {
                procedimiento = reporteProcedimiento.valor;
                let params = '', setParams = [], setValues = {};
                await parametros.forEach(parametro => {
                    setParams.push(parametro.nombre + '=' + parametro.valor);
                });
                params = setParams.toString();
                let query;
                if (procedimiento.indexOf('exec') >= 0) {
                    query = `${procedimiento} `;
                } else {
                    query = `exec ${procedimiento} `;
                }

                if (query.indexOf('@') >= 0) {
                    for (let i = 0; i < parametros.length ; i++) {
                        let parametro = parametros[i];
                        if (query.includes(parametro.nombre)) {
                            query = query.replaceAll(parametro.nombre,parametro.valor);
                        }
                    }
                } else {
                    query += `${params}`;
                }
                query = query.replaceAll('dd',date.getDate()).padZeros(2);
                query = query.replaceAll('mm',date.getMonth() + 1).padZeros(2);
                query = query.replaceAll('aaaa',date.getFullYear());
                await modelos.sequelize
                  .query(query)
                  .then(resp1 => {
                        if (Array.isArray(resp1)) {
                            respReporte = resp1[0];
                        } else {
                            respReporte = JSON.stringify(resp1);
                        }
                    }
                  ).catch((err) => {
                      console.log(err);
                  });
            }
            if(reporteQuery != undefined) {
                query = reporteQuery.query;
                await parametros.forEach(element => {
                    query = query.replaceAll(element.nombre, element.valor);
                });
                // query = query.replaceAll('dd',date.getDate()).padZeros(2);
                // query = query.replaceAll('mm',date.getMonth() + 1).padZeros(2);
                // query = query.replaceAll('aaaa',date.getFullYear());
                let resp1 = await modelos.sequelize.query(query);
                // await modelos.sequelize.close();
                if (Array.isArray(resp1)) {
                    respReporte = resp1[0]
                } else {
                    respReporte = JSON.stringify(resp1)
                }
            }
        }
        return [respReporte]
    } catch (e) {
        console.log(e)
    }
}

async function setParametro(param) {

    let date = new Date();
    let dateStr = minTwoDigits(date.getDate())+'/'+minTwoDigits((date.getMonth()+1))+'/'+date.getFullYear();
    let [day,month,year] = dateStr.split('/');

    param = param.replaceAll('dd',day);
    param = param.replaceAll('mm',month);
    param = param.replaceAll('aaaa',year);

    return param;
}

async function creacionArchivos(respReporteQuery, fileExt, dirFileExt = null, dir = null, callback = null) {
    let file, ext;
    //Creacion de archivos
    if (respReporteQuery && respReporteQuery.length) {
        file = fileExt.split('.')[0];
        ext = fileExt.split('.')[1];

        if (ext == 'xlsx') {
            let book = await funcionesArchivo.arrayToExcel(respReporteQuery);
            await funcionesArchivo.writeExcel(fileExt, dirFileExt, dir, book, async (fileExt, dirFileExt, dir) => {
                if (typeof callback == 'function') {
                    await callback(fileExt, dirFileExt, dir);
                }
            });
        } else if(ext == 'csv') {
            let csv = await funcionesArchivo.arrayToCsv(respReporteQuery,'|');
            await funcionesArchivo.writeCsv(fileExt, dirFileExt, dir, csv, async (fileExt, dirFileExt, dir) => {
                if (typeof callback == 'function') {
                    await callback(fileExt, dirFileExt, dir);
                }
            });
        }
    } else {
        if( typeof callback == 'function') {
            await callback(fileExt, dirFileExt, dir)
        }
    }
    return [fileExt,dirFileExt,dir];
}

async function creacionArchivosWithBom(respReporteQuery, fileExt, dirFileExt = null, dir = null, callback = null) {
    let file, ext;
    //Creacion de archivos
    if (respReporteQuery && respReporteQuery.length) {
        file = fileExt.split('.')[0];
        ext = fileExt.split('.')[1];

        if (ext == 'xlsx') {
            let book = await funcionesArchivo.arrayToExcel(respReporteQuery);
            await funcionesArchivo.writeExcel(fileExt, dirFileExt, dir, book, callback);
        } else if(ext == 'csv') {
            let csv = await funcionesArchivo.arrayToCsv(respReporteQuery,'|');
            await funcionesArchivo.writeCsv(fileExt, dirFileExt, dir, csv, async (fileExt, dirFileExt,dir) => {
                await funcionesArchivo.addBom2ToNewFile(fileExt, dirFileExt, dir, async (fileExt, dirFileExt, dir) => {
                    if (typeof callback == 'function') {
                        await callback(fileExt, dirFileExt, dir);
                    }
                });
            });
        }
    } else {
        if( typeof callback == 'function') {
            await callback(fileExt, dirFileExt)
        }
    }
    return [fileExt,dirFileExt,dir];
}

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}


function minTwoDigits(n) {
    return (n < 10 ? '0' : '') + n;
}
