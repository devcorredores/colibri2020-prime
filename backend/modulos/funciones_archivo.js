require('../utils/Prototipes');
const { exec } = require("child_process");
const { text } = require("body-parser");
const {loggerError} = require("../modulos/winston");
const fs = require('fs');
const XLSX = require('xlsx');
const { readFile, writeFile } = require('fs').promises;
const sftp = require('../modulos/sftp');
const modelos = require("../modelos");
const funciones = require('../modulos/funciones');
const path = require('path');
const moment = require('moment');
const { resolve } = require("app-root-path");
const csvToJson = require('csvtojson');

crearTexto = async (pagos, id_usuario,id_poliza) => {
  let response = { status: 'OK', message: '', data: '' };
  try {
    let fecha = new Date();
    let cod_entidad = id_poliza == 7 ? 101 : id_poliza == 8 ? 104 : '';
    let fechaV = funciones.formatoFecha(fecha).split('/');
    let nombreArchivo = pagos[0].num_carga+'D'+fechaV[2]+fechaV[1]+fechaV[0]+cod_entidad+'.csv';
    let rows = await this.crearRows(pagos);
    let texto_archivo = rows.join('\r\n').decode_utf8();
	  let max = await modelos.archivo.max('nro_actual', {where: {id_poliza:id_poliza, abreviacion:'D'}});
    let nextNum = max+1;
    await modelos.archivo.update({ nro_actual: nextNum }, { where: { id_poliza, abreviacion:'D' } });
  } catch (e) {
    console.log(e);
    response.status = 'ERROR';
    response.message = e;
  }
};

arrayToExcel = async (data, bookName = 'Datos') => {
	/* make the worksheet */
	const sheet = XLSX.utils.json_to_sheet(data);

	/* add to workbook */
	const book = XLSX.utils.book_new();
	XLSX.utils.book_append_sheet(book, sheet, bookName);
	return book;
}

csvToArray = async (dirFileName) => {
	let csv = csvToJson({
		noheader:true,
		delimiter: '|',
	});
	let aData;
	if (fs.existsSync(dirFileName)) {
		aData = await csv.fromFile(dirFileName)
	}
	return aData;
}

arrayToCSV = async (data, delimiter = ',', bQuotes = false, bHeaders = false) => {
	try {
		let csv = data.map(row => Object.values(row));
		if (bHeaders) {
			csv.unshift(Object.keys(data[0]));
		}
		if (delimiter && bQuotes)  {
			return `${csv.join('"\r\n"').replaceAll(',',`"${delimiter}"`)}`;
		} else if (delimiter && bQuotes)  {
			return `${csv.join('"\r\n"').replaceAll(',',`"${delimiter}"`)}`;
		} else if (bQuotes)  {
			return `${csv.join('"\r\n"').replaceAll(',',`","`)}`;
		} else if (delimiter)  {
			return `${csv.join('\r\n').replaceAll(',',`${delimiter}`)}`;
		} else {
			return csv.join('\r\n');
		}
	} catch (e) {
		console.log(e)
	}
};

writeCSV = async (fileExt, dirFileName, dir, data, callback = null) => {
	try {
		await fs.writeFile(dirFileName, data, {
			encoding: "utf8",
			flag: "w",
			mode: 0o666
		}, async (err) => {
			if (err)
				loggerError.error(new Date(),`Ocurrio un error en la creacion del archivo csv`, err);
			else {
				console.log("File written successfully\n",dirFileName);
				if (typeof callback == 'function') {
					await callback(fileExt, dirFileName, dir);
				}
			}
		});
	} catch (err) {
		console.log(err);
		process.exit(1);
	}
}

addBomToNewFile = async (fileExt, dirFileName, dir, callback = null) => {
	await exec(`printf '\xEF\xBB\xBF' | cat - ${dirFileName} > ${dir + 'bom_'+fileExt} | mv ${dir + 'bom_'+fileExt} ${dirFileName}`, async (err, stdout, stderr) => {
		if (err)
			loggerError.error(new Date(),`Ocurrio un error la ejecucion de la linea de comando printf '\xEF\xBB\xBF'`, err);
		else {
			console.log(new Date(),"BOM added to File successfully\n", stdout, stderr);
			if (typeof callback == 'function') {
				await callback(fileExt, dirFileName, dir);
			}
		}
	});
}

addBom2ToNewFile = async (fileExt, dirFileName, dir, callback = null) => {
	try {
		const inputFile = dirFileName;
		const outputFile = path.join(dir,'./outputfile.txt');

		let inputText = fs.readFileSync(inputFile, 'utf8');
		let outputText = '\ufeff' + inputText;

		fs.writeFileSync(outputFile, outputText, 'utf8');
		fs.unlinkSync(dirFileName);
		fs.copyFileSync(outputFile, dirFileName);
		if (typeof callback == 'function') {
			await callback(fileExt, dirFileName, dir);
		}
	} catch (e) {
		console.log(e)
	}
}

writeExcel = async (fileExt, dirFileName, dir, book, callback = null) => {
	try {
		/* generate an XLSX file */
		await XLSX.writeFile(book,dirFileName);
		if (typeof callback == 'function') {
			await callback(fileExt, dirFileName, dir);
		}
	} catch (err) {
		console.log(err);
		process.exit(1);
	}
}

crearTextoArchivo = async (pagos, id_usuario,id_poliza) => {
  let response = { status: 'OK', message: '', data: '' };
  try {
    let fecha = new Date();
    let cod_entidad = id_poliza == 7 ? 101 : id_poliza == 8 ? 104 : '';
    let fechaV = funciones.formatoFecha(fecha).split('/');
    let nombreArchivo = pagos[0].num_carga+'D'+fechaV[2]+fechaV[1]+fechaV[0]+cod_entidad+'.csv';
    let rows = await this.crearRows(pagos);
    let texto_archivo = rows.join('\r\n').decode_utf8();
	  let max = await modelos.archivo.max('nro_actual', {where: {id_poliza:id_poliza, abreviacion:'D'}});
    let nextNum = max+1;
    await modelos.archivo.update({ nro_actual: nextNum }, { where: { id_poliza, abreviacion:'D' } });
    await crearArchivo(texto_archivo, id_usuario, pagos,nombreArchivo,id_poliza);
  } catch (e) {
    console.log(e);
    response.status = 'ERROR';
    response.message = e;
  }
};

crearTextoArchivoEnviar = async (pagos, id_usuario,id_poliza, callback = null) => {
  let response = { status: 'OK', message: '', data: '' };
  try {
    let fecha = new Date();
    let cod_entidad = id_poliza == 7 ? 101 : id_poliza == 8 ? 104 : '';
    let fechaV = funciones.formatoFecha(fecha).split('/');
    let nombreArchivo = pagos[0].num_carga+'D'+fechaV[2]+fechaV[1]+fechaV[0]+cod_entidad+'.csv';
    let rows = await crearRows(pagos);
    let texto_archivo = rows.join('\r\n').decode_utf8();
    let max = await modelos.archivo.max('nro_actual', {where: {id_poliza:id_poliza, abreviacion:'D'}});
    let idsPagos = Array.from(pagos, pago => pago.id_pago);
    let pagoDetalles = await modelos.plan_pago_detalle.findAll({where: {id_planpago: idsPagos}});
    let idsPagoDetalles = pagoDetalles ? Array.from(pagoDetalles, pagoDetalle => pagoDetalle.id) : [];
    let nextNum = max+1;
    await modelos.archivo.update({ nro_actual: nextNum }, { where: { id_poliza, abreviacion:'D' } });
    await crearArchivo(texto_archivo, id_usuario, nombreArchivo, async (newfile) => {
	    await sftp.envio_1(newfile,1,async (err, localPath, localFile, remotePath, remoteFile, envio, resp) => {
		    try {
		      if(err){
				    if (typeof callback == 'function') {
					    callback(pagos,err);
				    }
				    response.status='ERROR';
				    response.message = err;
			    } else {
				    if (typeof callback == 'function') {
					    callback(pagos,resp);
				    }
				    response.message = resp;
				    response.data = envio;
				    let respPlanPagoDetalle;
				    for (let i = 0; i < pagos.length; i++) {
					    let pago = pagos[i];
					    let nroCuotaProg = pago.nro_certificado.substring(0,3);
					    let idInstanciaPoliza = pago.nro_certificado.substring(4,pago.nro_certificado.length);
					    modelos.plan_pago_detalle.update({id_instancia_archivo: newfile.id, id_estado_envio:250},{where: {id_instancia_poliza:idInstanciaPoliza,nro_cuota_prog:parseInt(nroCuotaProg)}});
				    }
				    let respEnvioHistorico = await modelos.envio_historico.findOne({where: {id_envio:envio.id}});
				    let rows = await crearRows(pagos);
				    if (!respEnvioHistorico) {
					    let newEnvioHistorico = {
						    id_envio:envio.id,
						    respuesta: resp,
					    };
					    for (let i = 0; i < rows.length; i++) {
						    let row = rows[i];
						    newEnvioHistorico.contenido = row;
						    respEnvioHistorico = modelos.envio_historico.create(newEnvioHistorico)
					    }
				    }
			    }
		    } catch (e) {
			    console.log(e)
		    }
	    });
    });
  } catch (e) {
    console.log(e);
    response.status = 'ERROR';
    response.message = e;
  }
};

leerTextoArchivoRecibido = async (pagos, id_usuario,id_poliza, callback = null) => {
  let response = { status: 'OK', message: '', data: '' };
  try {
    let cod_entidad = id_poliza == 7 ? 101 : id_poliza == 8 ? 104 : id_poliza == 4 ? 105 : '';
    let fechaV = funciones.formatoFecha(fecha).split('/');
    let nombreArchivo = pagos[0].num_carga+'D'+fechaV[2]+fechaV[1]+fechaV[0]+cod_entidad+'.csv';
    let rows = await crearRows(pagos);
    let texto_archivo = rows.join('\r\n').decode_utf8();
    let max = await modelos.archivo.max('nro_actual', {where: {id_poliza:id_poliza, abreviacion:'D'}});
    let idsPagos = Array.from(pagos, pago => pago.id_pago);
    let pagoDetalles = await modelos.plan_pago_detalle.findAll({where: {id_planpago: idsPagos}});
    let idsPagoDetalles = pagoDetalles ? Array.from(pagoDetalles, pagoDetalle => pagoDetalle.id) : [];
    let nextNum = max+1;
    await modelos.archivo.update({ nro_actual: nextNum }, { where: { id_poliza, abreviacion:'D' } });
    await crearArchivo(texto_archivo, id_usuario, nombreArchivo, async (newfile) => {
	    await sftp.envio_1(newfile,1,async (err, localPath, localFile, remotePath, remoteFile, envio, resp) => {
		    try {
		      if(err){
				    if (typeof callback == 'function') {
					    callback(pagos,err);
				    }
				    response.status='ERROR';
				    response.message = err;
			    } else {
				    if (typeof callback == 'function') {
					    callback(pagos,resp);
				    }
				    response.message = resp;
				    response.data = envio;
				    let respPlanPagoDetalle;
				    for (let i = 0; i < pagos.length; i++) {
					    let pago = pagos[i];
					    let nroCuotaProg = pago.nro_certificado.substring(0,3);
					    let idInstanciaPoliza = pago.nro_certificado.substring(4,pago.nro_certificado.length);
					    modelos.plan_pago_detalle.update({id_instancia_archivo: newfile.id, id_estado_envio:250},{where: {id_instancia_poliza:idInstanciaPoliza,nro_cuota_prog:parseInt(nroCuotaProg)}});
				    }
				    let respEnvioHistorico = await modelos.envio_historico.findOne({where: {id_envio:envio.id}});
				    let rows = await crearRows(pagos);
				    if (!respEnvioHistorico) {
					    let newEnvioHistorico = {
						    id_envio:envio.id,
						    respuesta: resp,
					    };
					    for (let i = 0; i < rows.length; i++) {
						    let row = rows[i];
						    newEnvioHistorico.contenido = row;
						    respEnvioHistorico = modelos.envio_historico.create(newEnvioHistorico)
					    }
				    }
			    }
		    } catch (e) {
			    console.log(e)
		    }
	    });
    });
  } catch (e) {
    console.log(e);
    response.status = 'ERROR';
    response.message = e;
  }
};

crearRows = async (pagos) => {
  let rows = [];
  for (var i = 0; i < pagos.length; i++) {
    let pago = pagos[i];
    rows.push(pago.num_carga+'|'+pago.cod_agenda+'|'+pago.cuenta+'|'+funciones.formatter.format(pago.pago_cuota_prog)+'|'+pago.moneda+'|'+pago.codigo_producto+'|'+pago.nro_certificado+'|'+pago.correlativo+'|'+pago.glosa+'|'+pago.fecha_envio)
  }
  return rows;
}

crearArchivo = async (texto_archivo, id_usuario,nombreArchivo, callback = null) => {
    let id_archi = null;
    const filename = path.join(__dirname, `../public/archivo/out/${nombreArchivo}`);
     fs.writeFile( filename, texto_archivo,'utf8', async error => {
        if (error)
            console.log(error);
        else
            console.log('El archivo fue creado');
        let archivo = {
        	nombre: nombreArchivo,
	        fecha_creacion: new Date(),
	        ubicacion: 'backend/public/archivo/out',
	        nro_envio: 32,
	        id_archivo: 1,
	        adicionada_por: id_usuario,
	        modificada_por: id_usuario
        };
       	let newArchivo = await modelos.instancia_archivo.create(archivo);
       	if (typeof callback == 'function') {
       		callback(newArchivo);
       	}
    });
};

exportarArchivosFtpParaCobros = async (parametros, file, callback = null) =>{
	try {
		let parDate = parametros.find(param => param.nombre == '@date');
		let parPath = parametros.find(param => param.nombre == '@path');
		let parIdPoliza = parametros.find(param => param.nombre == '@poliza');
		let parCodigo = parametros.find(param => param.nombre == '@codigo');
		let parNroCarga = parametros.find(param => param.nombre == '@nroCarga');
		let parTipo = parametros.find(param => param.nombre == '@tipo');
		let delimiter = '|';
		// Busqueda del archivo a ser apropiado "D" por producto
		let aContentLocalFile;
		let toReturn = [];
		if (file) {
			if (fs.existsSync(file.localPath + file.filename)) {

			}
		}
	} catch (e) {
		console.log(e);
	}
}

apropiarPlanesPagosConArchivosFtp = async (parametros, file, callback = null) =>{
	try {
		let parDate = parametros.find(param => param.nombre == '@date');
		let parPath = parametros.find(param => param.nombre == '@path');
		let parIdPoliza = parametros.find(param => param.nombre == '@poliza');
		let parCodigo = parametros.find(param => param.nombre == '@codigo');
		let parNroCarga = parametros.find(param => param.nombre == '@nroCarga');
		let parTipo = parametros.find(param => param.nombre == '@tipo');
		let delimiter = '|';
		// Busqueda del archivo a ser apropiado "D" por producto
		let csv = csvToJson({
			noheader:true,
			delimiter: '|',
		});
		let aContentLocalFile;
		let toReturn = [];
		if (file) {
			await modelos.archivo.update({nro_actual:parNroCarga.valor},{where:{abreviacion:parTipo.valor,id_poliza:parIdPoliza.valor}});
			if (fs.existsSync(file.localPath + file.filename)) {
				aContentLocalFile = await csv.fromFile(file.localPath + file.filename).then(async (jsonObj) => {
					for (let i = 0; i < jsonObj.length; i++) {
						let obj = jsonObj[i];
						let debitoDetalle = {
							fuente: file.filename,
							Num_Carga: obj.field1,
							Cod_Agenda: obj.field2,
							Cuenta: obj.field3,
							Monto: obj.field4,
							Moneda: obj.field5,
							Cod_Prod: obj.field6,
							No_Pol: obj.field7,
							NTra: obj.field8,
							Estado: obj.field9,
							Motivo: obj.field10,
							Saldo: obj.field11,
						};
						let intDebitoDetalleFound = await modelos.int_debito_detalle.findAll({
							attributes:[
								"fuente",
								"Num_Carga",
								"Cod_Agenda",
								"Cuenta",
							],
							where:{
								fuente: file.filename,
								Num_Carga: obj.field1,
								Cod_Agenda: obj.field2,
								Cuenta: obj.field3
							}
						});
						if(intDebitoDetalleFound.length) {
							toReturn.push(debitoDetalle);
						} else {
							await modelos.int_debito_detalle.create(debitoDetalle);
						}
					}
				});
			}
			if (typeof callback == 'function') {
				await callback(toReturn);
			}
		}
	} catch (e) {
		console.log(e);
	}
}

module.exports.crearRows = crearRows;
module.exports.writeCsv = writeCSV;
module.exports.csvToArray = csvToArray;
module.exports.writeExcel = writeExcel;
module.exports.addBomToNewFile = addBomToNewFile;
module.exports.addBom2ToNewFile = addBom2ToNewFile;
module.exports.arrayToCsv = arrayToCSV;
module.exports.arrayToExcel = arrayToExcel;
module.exports.crearTexto = crearTexto;
module.exports.crearTextoArchivo = crearTextoArchivo;
module.exports.crearTextoArchivoEnviar = crearTextoArchivoEnviar;
module.exports.leerTextoArchivoRecibido = leerTextoArchivoRecibido;
module.exports.exportarArchivosFtpParaCobros = exportarArchivosFtpParaCobros;
module.exports.apropiarPlanesPagosConArchivosFtp = apropiarPlanesPagosConArchivosFtp;

// module.exports.crearTextoAux = crearTextoAux;
