const soap = require("soap");
const request = require("request");
const configSoap = require("../config/web-config");
const Util = require("../utils/util");
const util = new Util();
const specialRequest = request.defaults({ strictSSL: false });
const soapHeaderXml =
  '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';
const xmljson = require("xmljson");

soapExecute = async (body, operation, wsName) => {
  let wssoap;
  try {
    const request = body;
    wssoap = configSoap.wssoap.find((soap) => soap.nombre == wsName);
    const wsdl = wssoap.wsdl;
    let response = { status: "OK", message: "soapExecute", data: {} };
    return new Promise(function (resolve, reject) {
      soap.createClient(
        wsdl,
        { request: specialRequest, wsdl_options: { timeout: 5000 } },
        (err, client) => {
          if (!err) {
            let method = client[operation];
            client.addSoapHeader(soapHeaderXml);
            method(request, (err, result) => {
              if (!err) {
                const campo = Object.keys(result)[0];
                if (result[campo].errors) {
                  response.status = "ERROR";
                  if (result[campo].errors.descripcion) {
                    response.message = result[campo].errors.descripcion;
                  }
                }
                response.data = result;
                resolve(response);
              } else {
                response.status = "ERROR";
                response.message = "Error, Server was unable to read request";
                resolve(response);
              }
            });
          } else {
            response.status = "ERROR";
            response.message = "Error el servicio no esta disponible";
            resolve(response);
          }
        }
      );
    }).catch((e) => {
      console.log(e);
    });
  } catch (e) {
    console.log(e);
  }
}

module.exports.to_json = xmljson.to_json;
module.exports.soapExecute = soapExecute;
