const path = require('path');
const modelos = require("../modelos");
const moment = require("moment");
const EasyFtp = require('easy-ftp');
const FuncionesArchivo = require('./funciones_archivo');
const ftp = new EasyFtp();
const fs = require('fs');
const {loggerFtp} = require("./winston");

async function setDirectory(dir) {
	// let slash = path.normalize(dir) dir.substring(dir.length-1,dir.length);
	// if (slash == '/' || slash == '\\') {
	// 	return dir;
	// }
	return path.join(dir,'/');
}

async function saveOrUpdateEnvio(id, localFileContent, store = {}, resp) {
	let respCurrentEnvio = await modelos.envio.findOne({where:{id:id}});
	let currentEnvio = respCurrentEnvio.dataValues;
	let updateEnvio = {id_estado:265};
	if (!currentEnvio.fecha) {
		updateEnvio.fecha = new Date();
	}
	let respEnvio = await modelos.envio.update(updateEnvio,{where:{id:id}});

	return respEnvio;
}

async function sendAction(idEnvio, envio, localPath, localFiles, remotePath, remoteFiles, ftp, callback = null) {

	let date = new Date();
	let respEnvioHistorico, envioHistorico, newEnvioHistorico, localFileContent, resp;
	let localFile, remoteFile, arrToSend = [];
	// caso 1 : Un archivo en un directorio

	if (localFiles && localFiles.length) {
		localFile = localFiles[0];
	}
	if (remoteFiles && remoteFiles.length) {
		remoteFile = remoteFiles[0];
	}

	if (localPath && localFile && remotePath && remoteFile ) {

		localPath = await setDirectory(localPath);
		// remotePath = await setDirectory(remotePath);
		arrToSend = [{local: path.join(localPath,localFile), remote: remotePath+'/'+remoteFile}];

		if (fs.existsSync(path.join(localPath,localFile))) {

			localFileContent = await fs.readFileSync(path.join(localPath,localFile));
			if (localFileContent) {
				localFileContent = localFileContent.toString();
			}

			ftp.cd(remotePath, async (err, path) => {
				if (err) {
					resp = 'El directorio remoto no existe: "' +remotePath +'". '+ date.toString()
					loggerFtp.info(resp, {error: err, data:arrToSend});
				}
				ftp.ls(remotePath, async (err, list) => {
					if (list && list.length) {
						if (list.find(param => param.filename == remoteFile)) {
							resp = 'El archivo fue enviado exitosamente: "' +remotePath+remoteFile +'". '+ date.toString();
							let envio = await saveOrUpdateEnvio(idEnvio, localFileContent, resp);
							loggerFtp.info(resp, {error: err, data:arrToSend, envio:envio});
							ftp.close();
							if (typeof callback == 'function') {
								callback(err, localPath, localFile, remotePath, remoteFile, envio, resp);
							}
						}
					}
					ftp.upload(arrToSend, (err) => {
						if (err) {
							loggerFtp.info('No fue posible enviar el archivo: "' +remotePath+remoteFile +'". '+ date.toString(), {error: err, data:arrToSend});
						} else {
							ftp.ls(remotePath, async (err, list) => {
								if (list && list.length) {
									if (list.find(param => param.filename == remoteFile)) {
										resp = 'El archivo fue enviado exitosamente: "' +remotePath+remoteFile +'". '+ date.toString();
										let envio = await saveOrUpdateEnvio(idEnvio, localFileContent, resp);
										loggerFtp.info(resp, {error: err, data:arrToSend, envio:envio});
										ftp.close();
										if (typeof callback == 'function') {
											callback(err, localPath, localFile, remotePath, remoteFile, envio, resp);
										}
									}
								}
							});
						}
					});
				});
			});
		}
	}
}

module.exports.conect = async function (config, callback = null) {
	ftp.connect(config);
	if(typeof callback == 'function') {
		callback();
	}
}

module.exports.downloadFileTipoPago = async function (config, configFtp, parametros, remoteDirectory, localDirectory, callback = null) {
	let date = new Date();
	ftp.connect(config);
	ftp.cd(remoteDirectory, async (err, dir) => {
		if (err) {
			loggerFtp.info('El directorio remoto no existe: "' + remoteDirectory +'". '+ date.toString(), {error: err, data:dir});
			ftp.close();
		}
		ftp.ls(remoteDirectory, async (err, list) => {
			try {
				if (err) {
					loggerFtp.info('El directorio remoto no existe: "' + remoteDirectory +'". '+ date.toString(), {error: err, data:list});
					ftp.close();
				}
				localDirectory = path.join(__dirname,'../',localDirectory);
				let arr = [];
				let parTipo = parametros.find(param => param.nombre == '@tipo');
				let parCodigo = parametros.find(param => param.nombre == '@codigo');
				let parDate = parametros.find(param => param.nombre == '@date');
				let parNroCarga;
				let file = list.find(param => moment(param.date).format("YYYY/MM/DD").split('/').join('') == parDate.valor && param.filename.includes(parTipo.valor));
				if (file) {
					let parFound = parametros.find(param => param.nombre == '@nroCarga');
					let parFoundIndex = parametros.findIndex(param => param.nombre == '@nroCarga');
					parNroCarga = file.filename.substring(0,4);
					parFound.valor = parNroCarga;
					parametros[parFoundIndex] = parFound;
					file.localPath = localDirectory;
					file.remotePath = remoteDirectory;
					let remoteFile = remoteDirectory+file.filename;
					let localFile = localDirectory+file.filename;
					if (!fs.existsSync(localFile)) {
						if (file.filename == parNroCarga + parTipo.valor + parDate.valor + parCodigo.valor + '.csv' ) {
							arr.push({remote:remoteFile,local:localFile})
						}
					}
					ftp.download(arr, function(err){
						if (err) {
							loggerFtp.info('El directorio remoto no existe: "' + remoteDirectory +'". '+ date.toString(), {error: err});
						}
						if(typeof callback == 'function') {
							callback(file,file.filename,file.localPath+file.filename,file.localPath,parametros);
						}
						ftp.close();
					});
				} else {
					ftp.close();
				}
			} catch (e) {
				console.log(e);
				ftp.close();
			}
		});
	});
}

module.exports.uploadFileTipoCobro = async function (config, configFtp, configArchivo, fileExt, dirFileExt, dir, parametros, remoteDirectory, localDirectory, callback = null) {
	try {
		let date = new Date();
		let parDate = parametros.find(param => param.nombre == '@date');
		let oldInstanciaArchivo = await modelos.instancia_archivo.find({where:{nombre:{$like:fileExt},ubicacion:{$like:dirFileExt}}});
		if (!oldInstanciaArchivo) {
			let newInstanciaArchivo = {
				nombre:fileExt,
				fecha_creacion:moment(parDate.valor,'YYYYMMDD').toDate(),
				ubicacion:dirFileExt,
				nro_envio:configArchivo.archivo_salida.nro_actual,
				id_archivo:configArchivo.archivo_salida.id,
				adicionado_por: 784,
				modificado_por: 784,
				createdAt: new Date(),
				updatedAt: new Date()
			};
			let instanciaArchivo = await modelos.instancia_archivo.create(newInstanciaArchivo);
			let newEnvio = {
				id_instancia_archivo:instanciaArchivo.id,
				id_estado:264,
				fecha: moment(parDate.valor,'YYYYMMDD').toDate(),
				id_forma_transporte:266,
				id_config_ftp:configFtp.id,
				codigo:instanciaArchivo.nro_envio,
				directorio_remoto:configFtp.remote_path,
				adicionado_por: 784,
				modificado_por: 784,
				createdAt: new Date(),
				updatedAt: new Date(),
				respuesta: 'Preparando envio...'
			};
			let envio = await modelos.envio.create(newEnvio);

			ftp.connect(config);
			ftp.cd(remoteDirectory, async (err, dir) => {
				if (err) {
					loggerFtp.info('El directorio remoto no existe: "' + remoteDirectory +'". '+ date.toString(), {error: err, data:dir});
					ftp.close();
				}
				let arrToSend = [{local: dirFileExt, remote: remoteDirectory+fileExt}];
				ftp.upload(arrToSend, async (err) => {
					if (err) {
						await modelos.envio.update({id_estado:264, respuesta:err},{where:{id:envio.id}});
						loggerFtp.info('No fue posible enviar el archivo: "' + remoteDirectory+fileExt +'". '+ date.toString(), {error: err, data:arrToSend});
						ftp.close();
					} else {
						ftp.ls(remoteDirectory, async (err, list) => {
							if (list && list.length) {
								if (list.find(param => param.filename == fileExt)) {
									let resp = 'El archivo fue enviado exitosamente: "' + remoteDirectory+fileExt +'", '+ new Date().toString();
									await modelos.envio.update({id_estado:265, respuesta:resp},{where:{id:envio.id}});
									envio = await modelos.envio.findOne({where:{id: envio.id}});
									let aData = await FuncionesArchivo.csvToArray(path.join(__dirname, '../'+localDirectory+fileExt));
									for (let i = 0; i < aData.length; i++) {
										let data = aData[i];
										let newEnvioHistorico = {
											contenido: data.field1+'|'+data.field2+'|'+data.field3+'|'+data.field4+'|'+data.field5+'|'+data.field6+'|'+data.field7+'|'+data.field8+'|'+data.field9+'|'+data.field10,
											id_envio: envio.id,
											adicionado_por: envio.adicionado_por,
											modificado_por: envio.modificado_por,
											createdAt: new Date(),
											updatedAt: new Date()
										}
										await modelos.envio_historico.create(newEnvioHistorico);
									}
									loggerFtp.info(resp, {data:arrToSend, envio:envio});
									ftp.close();
									if (typeof callback == 'function') {
										callback(envio, fileExt, dirFileExt, dir, localDirectory, remoteDirectory);
									}
								} else {
									ftp.close();
								}
							} else {
								ftp.close();
							}
						});
					}
				});
			});
		}
	} catch (e) {
		console.log(e)
	}
}

module.exports.envio_1 = async function (instanciArchivo, idConfigFtp = 1, callback = null) {
	try {
		let idInstanciArchivo = instanciArchivo.id
		let respInstanciaArchivo = await modelos.instancia_archivo.findOne({
			where:{id:idInstanciArchivo},
			include:{model:modelos.archivo, as:'archivo'}
		});
		let todayEnd = new Date();
		let todayBegin =  new Date(new Date().setHours(0,0,0,0));
		let instanciaArchivo = respInstanciaArchivo.dataValues;
		let oldEnvios = await modelos.envio.findAll({
			where:{fecha:{$between:[todayBegin, todayEnd]}},
			include:{model:modelos.instancia_archivo}
		});
		if (!oldEnvios.length) {
			let newDataEnvio = {
				id_instancia_archivo:idInstanciArchivo,
				id_estado:264,
				fecha: new Date(),
				id_forma_transporte:266,
				id_config_ftp:idConfigFtp,
				directorio_remoto:instanciaArchivo.archivo.ubicacion_remota,
				codigo:instanciaArchivo.nro_envio,
			};
			let respNewEnvio = await modelos.envio.create(newDataEnvio);
			let newEnvio = respNewEnvio.dataValues;
			let respEnvio = await modelos.envio.findOne({
				where: {id: newEnvio.id},
				include: [
					{model: modelos.config_ftp, as: 'config_ftp'},
					{
						model: modelos.instancia_archivo, as: 'instancia_archivo',
						include: {model: modelos.archivo, as: 'archivo'}
					},
				]
			});
			let envio = respEnvio.dataValues;
			const config = {
				host: envio.config_ftp.host,
				port: envio.config_ftp.puerto,
				username: envio.config_ftp.usuario,
				password: envio.config_ftp.password,
				type: 'sftp'
			};
			ftp.connect(config);
			let root = await path.join(__dirname, '../../');
			root = await setDirectory(root);

			let localPath;
			if (envio.instancia_archivo.ubicacion) {
				localPath = path.join(root, envio.instancia_archivo.ubicacion);
			} else if (envio.config_ftp.local_path) {
	            localPath = path.join(root, envio.config_ftp.local_path);
			}

			let localFile;
			if (envio.instancia_archivo.nombre) {
				localFile = envio.instancia_archivo.nombre;
			} else if (envio.config_ftp.local_files) {
				localFile = envio.config_ftp.local_files;
			}
			let localFiles = [];
			if (localFile.indexOf(',') >= 0) {
				localFiles = localFile.split(',');
			} else {
				localFiles[0] = localFile;
			}

			let remotePath;
			if (envio.directorio_remoto) {
				remotePath = envio.directorio_remoto;
			} else if (envio.config_ftp.local_path) {
				remotePath = envio.config_ftp.remote_path;
			}

			let remoteFile;
			if (envio.instancia_archivo.ubicacion) {
				remoteFile = envio.instancia_archivo.nombre;
			} else if (envio.config_ftp.local_path) {
				remoteFile = envio.config_ftp.remote_files;
			}
			let remoteFiles = [];
			if (remoteFile.indexOf(',') >= 0) {
				remoteFiles = remoteFile.split(',')
			} else {
				remoteFiles[0] = remoteFile;
			}
			await sendAction(newEnvio.id, envio, localPath, localFiles, remotePath, remoteFiles, ftp, callback);
		} else {
			if (typeof callback == 'function') {
				if (oldEnvios.length == 1) {
						callback(`Ya se envio el archivo al banco el dia de hoy: ${new Date()}`, oldEnvios[0].instancia_archivo.ubicacion, oldEnvios[0].instancia_archivo.nombre, oldEnvios[0].directorio_remoto, oldEnvios[0].directorio_remoto, oldEnvios[0], oldEnvios[0].respuesta);
				} else {
						callback(`Ya se envio el archivo al banco el dia de hoy: ${new Date()}`, oldEnvios[0].instancia_archivo.ubicacion, oldEnvios[0].instancia_archivo.nombre, oldEnvios[0].directorio_remoto, oldEnvios[0].directorio_remoto, oldEnvios[0], oldEnvios[0].respuesta);
				}
			}
		}
	} catch (e) {
		console.log(e)
	}
};
