
const nodemailer = require("nodemailer");
const {loggerEmail} = require("./winston");

class SendMailModule {
  static toSendMail(mailOptions, user, password, host, port) {
    let resp = "";

    const transporter = nodemailer.createTransport({
      host: host,
      port: port,
      secure: false, // upgrade later with STARTTLS
      secureConnection: false,
      auth: {
        user: user,
        pass: password
      }
    });
    let date = new Date();
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        resp = `ERROR,${error}`;
        console.log("=====> ERROR_SEND_MAIL:",error);
        loggerEmail.error('Correo no enviado: ' + date.toString(), error, mailOptions);
      } else {
        resp = `enviado,${JSON.stringify(info.response)}`;
        console.log(`=====> Email sent: ${info.response}`);
        loggerEmail.info('Correo enviado: ' + date.toString(), mailOptions);
      }
      transporter.close(); // shut down the connection pool, no more messages.  Comment this line out to continue sending emails.
    });

    return resp;
  }
}

module.exports = SendMailModule;
