const cron = require("node-cron");
const fs = require("fs");
const modelos = require("../modelos");
const cronFunciones = require("./cron_funcion");

module.exports = async (app) => {

// CRON OFICIAL

    // let configCronJobs = await modelos.config_cron.findAll({where:{estado:true}});
    // let respCronJobs = [configCronJobs];
    let database = modelos.sequelize.dialect.connectionManager.config.database;
    let hostname = app.get('hostname')
    let respCronJobs;
    if (database == 'Colibri2020' && hostname == 'CorredoresEco') {
        respCronJobs = await modelos.sequelize.query(`
            select *
            from Colibri2020.dbo.config_cron
            where estado is not null
              AND estado = 1
        `).catch(error => {
            let resp = error;
        });
    }

    if (respCronJobs) {
        let cronJobs = respCronJobs[0];
        for (let i = 0; i < cronJobs.length; i++) {
            let cronJob = cronJobs[i];
            let expresion = cronJob.expresion ? cronJob.expresion : '* * * * * *';
            cron.schedule(expresion, async () => {
                try {
                    console.log("--------------------------");
                    console.log("Iniciando Cron Job: " + cronJob.nombre);
                    let response = {status: "OK", message: "", data: {}};
                    let configCron = await modelos.config_cron.find({where: {id: cronJob.id, estado: true}});
                    if (configCron) {
                        await findCronFuncion(configCron.id, async (respCronFuncion) => {
                            if (Object.keys(respCronFuncion.dataValues).length) {
                                let cronFuncion = respCronFuncion.dataValues;
                                response.data = cronFuncion;
                                let cronFuncionParametros = cronFuncion.cron_funcion_parametros;
                                let respFuncion;
                                if (cronFunciones[cronFuncion.funcion]) {
                                    await cronFunciones[cronFuncion.funcion](cronFuncionParametros, cronFuncion, (resp) => {
                                        let fechaFinTarea = new Date();
                                        console.log("Cron finalizado: " + cronJob.nombre + ' - ' + cronFuncion.funcion + " - " + fechaFinTarea.toString(), resp, response);
                                    });
                                } else {
                                    let fechaFinTarea = new Date();
                                    console.log("Cron finalizado: " + cronJob.nombre + ' - ' + cronFuncion.funcion + " - " + fechaFinTarea.toString(), response);
                                }
                            }
                        });
                    } else {
                        console.log("Cron finalizado: la tarea no se ejecutó debido a la fecha festiva", fechaFestiva);
                    }
                } catch (e) {
                    console.log(e);
                }
            });
        }
    }
    // schedule tasks to be run on the server, deletes log files every 18th at 00:00
    cron.schedule("0 0 0 18 * *", function () {
        console.log("---------------------");
        console.log("Running Cron Job");
        fs.readdir(path.join(__dirname, `../logs`), (err, files) => {
            if (err) throw err;
            for (const file of files) {
                if (file.indexOf('-info.log') >= 0) {
                    fs.unlink(directory + file, err => {
                        if (err) throw err;
                        console.log(`Log file succesfully deleted at: ${new Date()}`);
                    });
                }
            }
        });
    });

    async function findCronFuncion(idCron, callback) {
        let response = {status: 'OK', message: '', data: {}};
        let cronFuncion = await modelos.cron_funcion.find({
            where: {id_cron: idCron},
            include: [
                {
                    model: modelos.config_cron
                },
                {
                    model: modelos.cron_funcion_parametro,
                    include: [
                        {
                            model: modelos.parametro,
                            as: 'unidad'
                        }, {
                            model: modelos.parametro,
                            as: 'comparacion'
                        },
                        // {
                        //     model: modelos.config_correo,
                        //     as: 'config_correo'
                        // },
                        // {
                        //     model: modelos.config_ftp,
                        //     as: 'config_ftp'
                        // },
                        // {
                        //     model: modelos.reporte_query,
                        //     as: 'reporte_query',
                        //     include: [
                        //         {
                        //             model: modelos.reporte_query_parametro,
                        //             order: [['order', 'DESC']]
                        //         }
                        //     ]
                        // }
                    ]
                }
            ]
        }).catch(err => {
              response.status = 'ERROR';
              response.message = err;
        });
        for (let i = 0; i < cronFuncion.cron_funcion_parametros.length; i++) {
            let cronFuncionParametro = cronFuncion.cron_funcion_parametros[i];
            if (!cronFuncionParametro.dataValues.comparacion) {
                switch (cronFuncionParametro.dataValues.tipo) {
                    case 'query':
                        cronFuncion.cron_funcion_parametros[i].dataValues.reporte_query = await modelos.reporte_query.findOne({
                            where:{id:parseInt(cronFuncionParametro.dataValues.valor)},
                            include: {
                                model: modelos.reporte_query_parametro,
                                order: [['order', 'DESC']]
                            }
                        });
                        cronFuncion.cron_funcion_parametros[i].reporte_query = cronFuncion.cron_funcion_parametros[i].dataValues.reporte_query;
                        break;
                    case 'correo':
                        cronFuncion.cron_funcion_parametros[i].dataValues.config_correo = await modelos.config_correo.findOne({where:{id:parseInt(cronFuncionParametro.dataValues.valor)}});
                        cronFuncion.cron_funcion_parametros[i].config_correo = cronFuncion.cron_funcion_parametros[i].dataValues.config_correo;
                        break;
                    case 'ftp':
                        cronFuncion.cron_funcion_parametros[i].dataValues.config_ftp = await modelos.config_ftp.findOne({where:{id:parseInt(cronFuncionParametro.dataValues.valor)}});
                        cronFuncion.cron_funcion_parametros[i].config_ftp = cronFuncion.cron_funcion_parametros[i].dataValues.config_ftp;
                        break;
                    case 'archivo':
                        if (!isNaN(cronFuncionParametro.dataValues.valor)) {
                            cronFuncion.cron_funcion_parametros[i].dataValues.archivo_salida = await modelos.archivo.findOne({where:{id:parseInt(cronFuncionParametro.dataValues.valor)}});
                            cronFuncion.cron_funcion_parametros[i].archivo_salida = cronFuncion.cron_funcion_parametros[i].dataValues.archivo_salida;
                        }
                        break;
                }
            }
        }
        response.data = cronFuncion;
        await callback(response.data);
    }
}
