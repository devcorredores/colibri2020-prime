const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt-nodejs');
var modelos = require('../modelos');
const invoke = require('../modulos/soap-request');
const {loggerLogin} = require("../modulos/winston");
const util = require("../utils/util");

passport.serializeUser(function (user, done) {
    done(null, user ? user.id : null);
});

// used to deserialize the user
passport.deserializeUser(function (id, done) {
    let response = { status: 'OK', message: '', data: '' };
    modelos.usuario.findOne({ where: { id } })
        .then(resp => {
            if (resp) {
                response.data = resp;
                done(null, response.data.id);
            }
        }
        ).catch(err => {
            done(err, 'Error');
        });
});

passport.use(new LocalStrategy(async (login, password, done) => {
        let date = new Date()
        let response = { status: 'OK', message: '', data: '' };
        console.log('ENTRADA USUARIO ===>', login, password);
        try {
            let usuario_login=login;
            let usuario = await modelos.usuario.findOne({
                where:{usuario_login: login},
                include: [
                    {model:modelos.persona},
                    {
                        model: modelos.rol,
                        as: 'usuarioRoles',
                        include: {
                            model: modelos.perfil,
                            as: 'rolPerfiles'
                        }
                    },
                    {model: modelos.autenticacion_usuario,}
                ]
            });
            if (usuario) {
                response.data = usuario;
                let responseExterno = null;
                let autenticacion = await modelos.autenticacion_usuario.find({ where: { usuario_id: usuario.id } });
                console.log(autenticacion);
                switch (autenticacion.par_autenticacion_id+'') {
                    case "10": /* USUARIO LOCAL */
                        bcrypt.compare(password, response.data.usuario_password, async function (err, res) {
                            if (!res) {
                                loggerLogin.info(`Password Incorrecto: username: ${usuario_login}, password: ${password} - ${date.toString()}`);
                                return done(null, false, { message: 'Password incorrecto.' });
                            } else {
                                response.data = usuario;
                                if (usuario.usuarioRoles && usuario.usuarioRoles.length) {
                                    loggerLogin.info(`Usuario Ingreso localmente: username: ${usuario_login}, password: ${password} - ${date.toString()}`, usuario.usuarioRoles.map(param => param.descripcion));
                                    return done(null, response.data.dataValues);
                                } else {
                                    loggerLogin.info(`El usuario no tiene ningun ROL asignado: username: ${usuario_login}, password: ${password} - ${date.toString()}`, usuario.usuarioRoles.map(param => param.descripcion));
                                    return done(null, false, { message: 'El usuario no tiene ningun ROL asignado.' });
                                }
                            }
                        });
                        break;
                    case "11": /* USUARIO EXTERNA ECOFUTURO */
                        responseExterno = await invoke.soapExecuteAutentica({ UsuarioIn: { usuario: login, password, idsistema: autenticacion.atributo1 ? autenticacion.atributo1 : 58 } }, 'getValida', 'WS-SOAP-ECOFUTURO-AUTH');
                        response.status = responseExterno.status;
                        response.message = responseExterno.message;

                        if (response.status === 'OK') {
                            usuario.dataValues.usuario_banco = responseExterno.data.getValidaResult;
                            usuario.usuario_banco = responseExterno.data.getValidaResult;
                            response.data = usuario;
                            if (usuario && usuario.usuarioRoles.length){
                                loggerLogin.info(`Usuario Ingreso Externamente: username: ${login}, password: ${password} - ${date.toString()}`,response.data.dataValues.usuario_banco);
                                return done(null, response.data.dataValues);
                            } else{
                                loggerLogin.info(`El usuario no tiene ningun ROL asignado: username: ${login}, password: ${password} - ${date.toString()}`, response.data.dataValues.usuario_banco);
                                return done(null, false, { message: 'El usuario no tiene ningun ROL asignado.' });
                            }
                        } else {
                            loggerLogin.info(`Usuario no encontrado: ${login}, password: ${password} - ${date.toString()}`);
                            return done(null, false, { message: 'Usuario no encontrado.'});
                        }
                        break;
                    case "357": /* USUARIO EXTERNA ECOFUTURO 2 */
                        console.log('entro 357')
                        responseExterno = await invoke.soapExecuteAutentica({ UsuarioIn: { usuario: login, password, idsistema: autenticacion.atributo1 ? autenticacion.atributo1 : 58 } }, 'getValida', 'WS-SOAP-ECOFUTURO-AUTH');
                        response.status = responseExterno.status;
                        response.message = responseExterno.message;
                        if (response.status === 'OK') {
                            usuario.dataValues.usuario_banco = responseExterno.data.getValidaResult;
                            usuario.usuario_banco = responseExterno.data.getValidaResult;
                            let usuarioRol = usuario.usuarioRoles.find(param => param.cod_rel_banco.includes(usuario.dataValues.usuario_banco.us_codcargo));
                            if (usuario.dataValues.usuario_banco) {

                                let usuarioUpdated = await modelos.usuario.update({
                                    usuario_email:usuario.usuario_banco.us_correoe,
                                    usuario_nombre_completo:usuario.usuario_banco.us_paterno+' '+usuario.usuario_banco.us_materno+' '+usuario.usuario_banco.us_nombre
                                },{where:{id:usuario.id}});
                                let personaUpdated = await modelos.persona.update({
                                    persona_doc_id:usuario.usuario_banco.us_doc_id,
                                    persona_primer_apellido:usuario.usuario_banco.us_paterno,
                                    persona_segundo_apellido:usuario.usuario_banco.us_materno,
                                    persona_primer_nombre:usuario.usuario_banco.us_nombre,
                                }, {where:{id:usuario.persona.id}});
                                if (usuarioRol) {
                                    if (usuario && usuario.usuarioRoles.length){
                                        loggerLogin.info(`Usuario Ingreso Externamente: username: ${login}, password: ${password} - ${date.toString()}`,response.data.dataValues.usuario_banco);
                                        return done(null, response.data.dataValues);
                                    } else{
                                        loggerLogin.info(`El usuario no tiene ningun ROL asignado: username: ${login}, password: ${password} - ${date.toString()}`, response.data.dataValues.usuario_banco);
                                        return done(null, false, { message: 'El usuario no tiene ningun ROL asignado.' });
                                    }
                                } else {
                                    let rolesToDelete = await modelos.rol.findAll({where:{id:[5,9,13,23]}});
                                    let idsRolesToDelete = rolesToDelete.map(param => param.id);
                                    let rolFound = await modelos.rol.find({where:{cod_rel_banco:{$like:`%${usuario.dataValues.usuario_banco.us_codcargo}%`}}})
                                    let usuarioXRolDeleted = await modelos.usuario_x_rol.destroy({where: {id_rol:idsRolesToDelete, id_usuario:usuario.id }});
                                    let usuarioXRolCreated = await modelos.usuario_x_rol.create({id_usuario: usuario.id, id_rol:rolFound.id, adicionado_por:usuario.id, modificado_por:usuario.id});
                                    setTimeout(async () => {
                                        usuarioUpdated = await modelos.usuario.findOne({
                                            where:{id: usuario.id},
                                            include: [
                                                {model:modelos.persona},
                                                {
                                                    model: modelos.rol,
                                                    as: 'usuarioRoles',
                                                    include: {
                                                        model: modelos.perfil,
                                                        as: 'rolPerfiles'
                                                    }
                                                }
                                            ]
                                        });
                                        console.log('usuarioUpdated',usuarioUpdated.usuarioRoles);
                                        response.data = usuario = usuarioUpdated;
                                        if (usuario && usuario.dataValues.usuarioRoles.length){
                                            loggerLogin.info(`Usuario Ingreso Externamente: username: ${login}, password: ${password} - ${date.toString()}`,response.data.dataValues.usuario_banco);
                                            return done(null, response.data.dataValues);
                                        } else{
                                            loggerLogin.info(`El usuario no tiene ningun ROL asignado: username: ${login}, password: ${password} - ${date.toString()}`, response.data.dataValues.usuario_banco);
                                            return done(null, false, { message: 'El usuario no tiene ningun ROL asignado.' });
                                        }
                                    },1000)
                                }
                            }
                        } else {
                            loggerLogin.info(`Usuario no encontrado: ${login}, password: ${password} - ${date.toString()}`);
                            return done(null, false, { message: 'Usuario no encontrado.'});
                        }
                        break;
                    default:
                        //Declaraciones ejecutadas cuando ninguno de los valores coincide con el valor de la expresión
                        break;
                }
            } else {
                loggerLogin.info(`Usuario no registrado: ${login}, password: ${password} - ${date.toString()}`);
                return done(null, false, { message: 'Usuario no registrado.' });
            }
        } catch (e) {
            console.log(e);
        }
    }
)),

auth = () => {
    return (req, res, next) => {
        passport.authenticate('local', (error, user, info) => {
            if (info)
            {
                res.status(400).json({ "statusCode": 200, "message": info });
            }else{
                req.login(user, function (error) {
                    if (error) return next(error);
                    next();
                });
            }
        })(req, res, next);
    }
},

auth2 = () => {
    return (req, res, next) => {
        req.body.username = "guest";
        req.body.password = "guest";
        passport.authenticate('local', (error, user, info) => {
            if (info) 
            {
                res.status(400).json({ "statusCode": 200, "message": info });
            }else{
                req.login(user, function (error) {
                    if (error) return next(error);
                    next();
                });
            }
        })(req, res, next);
    }
},

salir = () => {
    return (req, res, next) => {
        passport.authenticate('local', (error, user, info) => {
            if (error) res.status(400).json({ "statusCode": 200, "message": error });
            req.login(user, function (error) {
                if (error) return next(error);
                next();
            });
        })(req, res, next);
    }
}

module.exports.auth = auth;
module.exports.auth2 = auth2;
