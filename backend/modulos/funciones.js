QuitarCamposNullos = (objeto) => {
    for(atributo in objeto){
        if(objeto[atributo]===null || objeto[atributo]===undefined){
            objeto[atributo]='';
        }
    }
    return objeto;
};

formatoFechaDe = (param) => {
    const date = new Date(param);
    let month = `${date.getMonth()+1}`;
    let day = `${date.getDate()}`;
    const year = `${date.getFullYear()}`;

    if (month.length < 2) month = `0${month}`;
    if (day.length < 2) day = `0${day}`;

    return [day,month,year].join(' de ');
  }

  formatoFecha = (param) => {
    if(param!==null && param!==''){
      let date = new Date(param);
      let month = `${date.getMonth()+1}`;
      let day = `${date.getDate()}`;
      const year = `${date.getFullYear()}`;
  
      if (month.length < 2) month = `0${month}`;
      if (day.length < 2) day = `0${day}`;
  
      return [day,month,year].join('/');
    }else{
      return '';
    } 
  }

  formatoFechaBarrasMedia = (param) => {
    if(param!==null && param!==''){
      let date = new Date(param);
      let month = `${date.getMonth()+1}`;
      let day = `${date.getDate()}`;
      const year = `${date.getFullYear()}`;
  
      if (month.length < 2) month = `0${month}`;
      if (day.length < 2) day = `0${day}`;
  
      return [year,month,day].join('-');
    }else{
      return '';
    }
    
  }

  formatoFechaReporte = (param) => {
    if(param!==null && param!==''){
      let date = new Date(param);
      let month = `${date.getMonth()+1}`;
      let day = `${date.getDate()}`;
      const year = `${date.getFullYear()}`;

      if (month.length < 2) month = `0${month}`;
      if (day.length < 2) day = `0${day}`;

      return [year,month,day].join('');
    }else{
      return '';
    }

  };

  CalcularEdad = (dateString) => {
  let hoy = new Date()
        let fechaNacimiento = new Date(dateString)
        let edad = hoy.getFullYear() - fechaNacimiento.getFullYear()
        let diferenciaMeses = hoy.getMonth() - fechaNacimiento.getMonth()
        if (
          diferenciaMeses < 0 ||
          (diferenciaMeses === 0 && hoy.getDate() < fechaNacimiento.getDate())
        ) {
          edad--
        }
        return edad
    }

  formatoFechaQuery = (param) => {
      const date = new Date(param);
      let month = `${date.getMonth()+1}`;
      let day = `${date.getDate()}`;
      const year = `${date.getFullYear()}`;
  
      if (month.length < 2) month = `0${month}`;
      if (day.length < 2) day = `0${day}`;
  
      return [year,month,day].join('');
    }

let formatter = new Intl.NumberFormat('en-US', {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

module.exports.QuitarCamposNullos=QuitarCamposNullos;
module.exports.formatoFechaDe=formatoFechaDe;
module.exports.CalcularEdad=CalcularEdad;
module.exports.formatoFecha=formatoFecha;
module.exports.formatter=formatter;
module.exports.formatoFechaReporte=formatoFechaReporte;
module.exports.formatoFechaBarrasMedia=formatoFechaBarrasMedia;
module.exports.formatoFechaQuery=formatoFechaQuery;
