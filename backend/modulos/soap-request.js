const soap = require('soap');
const request = require('request');
const webconfig = require('../config/web-config');
const modelos = require('../modelos');

const specialRequest = request.defaults({
    strictSSL: false
});

const soapHeaderXml = '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';

// {"solicitud":198528}
function soapExecute(body, operation, wsName) {

    const request = body;
    const wssoap = webconfig.wssoap.find(soap => soap.nombre === wsName);
    const wsdl = wssoap.wsdl;
    let response = { status: 'OK', message: 'soapExecute', data: {} };

    return new Promise(function (resolve, reject) {

        soap.createClient(wsdl, { request: specialRequest, wsdl_options: { timeout: 5000 } }, (err, client) => {
            if (!err) {
                let method = client[operation];
                client.addSoapHeader(soapHeaderXml);
                method(request, (err, result) => {
                    if (!err) {
                        const campo = Object.keys(result)[0];
                        if (result[campo].errors) {
                            response.status = 'ERROR';
                            if (result[campo].errors.descripcion) {
                                response.message = result[campo].errors.descripcion;
                            }
                        }
                        response.data = result;
                        resolve(response);
                    } else {
                        response.status = 'ERROR';
                        response.message = 'Error, Server was unable to read request';
                        resolve(response);
                    }
                });
            } else {
                response.status = 'ERROR';
                response.message = 'Error el servicio no esta disponible';
                resolve(response);
            }

        });

    });

}

// invoke autenticacion
function soapExecuteAutentica(body, operation, wsName) {

    const request = body;
    // modelos.log_message.create({name:'INVOKE_SOAP_AUTENTICA',message:`request:${request.UsuarioIn.usuario}`}).then(resp => {});

    const wssoap = webconfig.wssoap.find(soap => soap.nombre === wsName);
    const wsdl = wssoap.wsdl;
    let response = { status: 'OK', message: 'soapExecute', data: {} };

    return new Promise(function (resolve, reject) {
        soap.createClient(wsdl, { request: specialRequest, wsdl_options: { timeout: 5000 } }, (err, client) => {
            if (!err) {
                method = client[operation];
                client.addSoapHeader(soapHeaderXml);
                method(request, (err, result) => {
                    if (!err) {
                        // modelos.log_message.create({name:'INVOKE_SOAP_AUTENTICA',message:`response:${JSON.stringify(result)}`}).then(resp => {});
                        response.data = result;
                        if (result.getValidaResult.ui_usuario){
                            const ui_usuario = result.getValidaResult.ui_usuario;
                            if(ui_usuario === 0){
                                response.status = 'ERROR';
                                response.message = 'Autenticación no valida';
                            }else{
                                response.message = 'Autenticación valida'
                            }
                        }else{
                            response.status = 'ERROR';
                            response.message = 'Autenticación no valida';
                        }
                        resolve(response);
                    } else {
                        response.status = 'ERROR';
                        response.message = 'Error, Server was unable to read request';
                        resolve(response);
                    }
                });
            } else {
                response.status = 'ERROR';
                response.message = 'Error el servicio no esta disponible';
                resolve(response);
            }

        });

    });

}

module.exports.soapExecute = soapExecute;
module.exports.soapExecuteAutentica= soapExecuteAutentica;
