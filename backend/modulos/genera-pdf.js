require('moment/locale/es');
require('../utils/Prototipes');
const util = require("../utils/util");
const pdf = require("html-pdf");
const path = require("path");
const fs = require("fs");
const funciones = require("./funciones");
const moment = require("moment");
const { loggerCatch } = require("../modulos/winston");
const conversor = require('conversor-numero-a-letras-es-ar');
const ClaseConversor = conversor.conversorNumerosALetras;
const miConversor = new ClaseConversor();
const solicitudService = require('../servicios/solicitud.service');
const reporteQueryService = require('../servicios/reporteQuery.service');
moment.locale('es');

formatDate = (param) => {
  const date = new Date(param);
  let month = `${date.getMonth() + 1}`;
  let day = `${date.getDate()}`;
  const year = `${date.getFullYear()}`;

  if (month.length < 2) month = `0${month}`;
  if (day.length < 2) day = `0${day}`;

  return [day, month, year].join("-");
};

generar = (datos, reporte, nombre_archivo) => {
  if (reporte === 1) {
    generarSolicitudEcoaguinaldo(datos, reporte, nombre_archivo);
  }
  if (reporte === 2) {
    generarCertificadoEcoaguinaldo(datos, reporte, nombre_archivo);
  }
  if (reporte === 3) {
    generarComprobanteEcoaguinaldo(datos, reporte, nombre_archivo);
  }
};

generarCertificadoEcoaguinaldo = (datos, reporte, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "10mm", // default is 0, units: mm, cm, in, px
      right: "10mm",
      bottom: "3mm",
      left: "10mm",
    },
    paginationOffset: 1, // Override the initial pagination number
    footer: {
      height: "8mm",
      contents: {
        default: `<hr>
              <span style="text-align: center; font-size: 7px">ALIANZA VIDA S.A.: SANTA CRUZ: Calle  Mario Gutiérrez No. 3325 Esq. Roca y Coronado (Esq. Tercer Anillo Ext.),  Teléfono: 3 3632727, Fax: 3 3632700 – LA PAZ: Zona: Achumani, Calle 6 (Juana  Parada) N° 683, Teléfono: 2793232, Fax: 2799191</span><br /><h3 style="text-align: center;">{{page}} de {{pages}}</h3>`, // fallback value
      },
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/Certificado de Cobertuta ECO-Pasanaku y Aguinaldo.html`
    );
    const logo_alianza = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/clip_image001_0001.jpg"
    );
    const logo_ecoaguinaldo = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/ECOAGUINALDO.png"
    );
    const logo_ecoriesgo = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/ECORIESGO.png"
    );
    const logo_ecopasanaku = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/ECOPASANAKU.png"
    );
    const logo_banco = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/ecofuturo.jpg"
    );
    let templateHtml = fs.readFileSync(template, "utf8");
    const firmas = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/firmas.png"
    );

    templateHtml = templateHtml.replace("{{logo_alianza}}", logo_alianza);
    templateHtml = templateHtml.replace(
      "{{logo_ecopasanaku}}",
      logo_ecopasanaku
    );
    templateHtml = templateHtml.replace(
      "{{logo_ecoaguinaldo}}",
      logo_ecoaguinaldo
    );
    templateHtml = templateHtml.replace(
      "{{logo_ecoriesgo}}",
      logo_ecoriesgo
    );
    templateHtml = templateHtml.replace("{{logo_banco}}", logo_banco);
    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento
    );
    templateHtml = templateHtml.replace(
      "{{asegurado}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada
          : "") +
        " " +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{fecha de nacimiento}}",
      funciones.formatoFecha(datos[0].persona_fecha_nacimiento)
    );
    templateHtml = templateHtml.replace("{{numero de poliza}}", datos[0].id);
    templateHtml = templateHtml.replace(
      "{{fecha desde}}",
      funciones.formatoFecha(datos[0].fecha_inicio_vigencia)
    );
    templateHtml = templateHtml.replace(
      "{{fecha hasta}}",
      funciones.formatoFecha(datos[0].fecha_fin_vigencia)
    );
    templateHtml = templateHtml.replace("{{firmas}}", firmas);
    let capital_asegurado = "";
    if (datos[0].id_poliza + "" === "1") {
      capital_asegurado = "7000";
    } else {
      capital_asegurado = "10000";
    }
    templateHtml = templateHtml.replace(
      "{{CAPITAL ASEGURADO}}",
      capital_asegurado
    );

    let filas = "";
    datos.forEach((element) => {
      filas =
        filas +
        `<tr>
            <td width="315">${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b}</td>
            <td width="115">${element.parametro_descripcion}</td>
            <td width="128">${element.porcentaje}</td>
            </tr>`;
    });
    templateHtml = templateHtml.replace("{{filas}}", filas);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
        loggerCatch.info(new Date(), err);
      } else {
        resolve(response);
      }
    });
  });
};

generarCertificadoEcoriesgo = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "10mm", // default is 0, units: mm, cm, in, px
      right: "10mm",
      bottom: "3mm",
      left: "10mm",
    },
    paginationOffset: 1, // Override the initial pagination number
    footer: {
      height: "10mm",
      contents: {
        default: `<hr>
              <span style="text-align: center">ALIANZA VIDA S.A.: SANTA CRUZ: Calle  Mario Gutiérrez No. 3325 Esq. Roca y Coronado (Esq. Tercer Anillo Ext.),  Teléfono: 3 3632727, Fax: 3 3632700 – LA PAZ: Zona: Achumani, Calle 6 (Juana  Parada) N° 683, Teléfono: 2793232, Fax: 2799191</span><br /><h3 style="text-align: center;">{{page}} de {{pages}}</h3>`, // fallback value
      },
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/Certificado de Cobertuta ECO-Riesgo.html`
    );
    const logo_alianza = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/clip_image001_0001.jpg"
    );
    const logo_ecoaguinaldo = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/ECOAGUINALDO.png"
    );
    const logo_ecoriesgo = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/ECORIESGO.png"
    );
    const logo_ecopasanaku = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/ECOPASANAKU.png"
    );
    const logo_banco = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/ecofuturo.jpg"
    );
    const logo_corredora = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/logoKerkus-light.jpg"
    );
    let templateHtml = fs.readFileSync(template, "utf8");
    const firmas = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/firmas.png"
    );

    templateHtml = templateHtml.replace("{{logo_alianza}}", logo_alianza);
    templateHtml = templateHtml.replace("{{logo_corredora}}", logo_corredora);
    templateHtml = templateHtml.replace("{{logo_ecopasanaku}}", logo_ecopasanaku);
    templateHtml = templateHtml.replace("{{logo_ecoaguinaldo}}", logo_ecoaguinaldo);
    templateHtml = templateHtml.replace("{{logo_banco}}", logo_banco);
    templateHtml = templateHtml.replace("{{numero de certificado}}", datos[0].nro_documento);
    templateHtml = templateHtml.replace("{{monto_prima}}", util.formatNumber(datos[0].monto_prima,0));
    templateHtml = templateHtml.replace("{{monto_capital}}", util.formatNumber(datos[0].monto_capital,0));
    templateHtml = templateHtml.replace(
      "{{asegurado}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada
          : "") +
        " " +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{fecha de nacimiento}}",
      funciones.formatoFecha(datos[0].persona_fecha_nacimiento)
    );
    templateHtml = templateHtml.replace("{{numero de poliza}}", datos[0].id);
    templateHtml = templateHtml.replace(
      "{{fecha desde}}",
      funciones.formatoFecha(datos[0].fecha_inicio_vigencia)
    );
    templateHtml = templateHtml.replace(
      "{{fecha hasta}}",
      funciones.formatoFecha(datos[0].fecha_fin_vigencia)
    );
    templateHtml = templateHtml.replace("{{firmas}}", firmas);
    let capital_asegurado = "";
    if (datos[0].id_poliza + "" === "1") {
      capital_asegurado = "7000";
    } else {
      capital_asegurado = "10000";
    }
    templateHtml = templateHtml.replace(
      "{{CAPITAL ASEGURADO}}",
      capital_asegurado
    );

    let filas = "";
    datos.forEach((element) => {
      filas =
        filas +
        `<tr>
            <td width="315">${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b}</td>
            <td width="115">${element.parametro_descripcion}</td>
            <td width="128">${element.porcentaje}</td>
            </tr>`;
    });
    templateHtml = templateHtml.replace("{{filas}}", filas);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
        loggerCatch.info(new Date(), err);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoaguinaldo = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/orden de cobro.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hora}}",
      datos[0].fecha_emision
    );
    templateHtml = templateHtml.replace(
      "{{numero de documento}}",
      datos[0].nro_documento
    );

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "1") {
      titulo_poliza = "Ecoaguinaldo - Ecopasanaku";
    } else {
      titulo_poliza = "Ecopasanaku";
    }
    templateHtml = templateHtml.replace(/{{Ecoaguinaldo}}/g, titulo_poliza);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoriesgo = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/orden de cobro ECO-Riesgo.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace("{{fecha_hora}}", datos[0].fecha_emision);
    templateHtml = templateHtml.replace("{{numero de documento}}", datos[0].nro_documento);
    templateHtml = templateHtml.replace("{{monto_prima}}", util.formatNumber(datos[0].monto_prima,0));

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "1") {
      // titulo_poliza = "Ecoaguinaldo - Ecopasanaku";
      titulo_poliza = "Ecoriesgo";
    } else if (datos[0].id_poliza + "" === "11") {
      titulo_poliza = "Ecoriesgo";
    } else {
      titulo_poliza = "Ecopasanaku";
    }
    templateHtml = templateHtml.replace(/{{Ecoriesgo}}/g, titulo_poliza);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoVida = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoVida/orden de cobro.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hora}}",
      datos[0].fecha_emision
    );
    templateHtml = templateHtml.replace(
      "{{numero de documento}}",
      datos[0].nro_documento
    );

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "1") {
      titulo_poliza = "Ecoaguinaldo";
    } else {
      titulo_poliza = "Ecopasanaku";
    }
    //templateHtml = templateHtml.replace(/{{Ecoaguinaldo}}/g, titulo_poliza);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoAccidentes = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoAccidentes/orden de cobro.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hora}}",
      datos[0].fecha_emision
    );
    templateHtml = templateHtml.replace(
      "{{numero de documento}}",
      datos[0].nro_documento
    );

    let titulo_poliza = "EcopAccidentes";
    //templateHtml = templateHtml.replace(/{{Ecoaguinaldo}}/g, titulo_poliza);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoResguardo = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoResguardo/orden de cobro.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hora}}",
      datos[0].fecha_emision
    );
    templateHtml = templateHtml.replace(
      "{{numero de documento}}",
      datos[0].nro_documento
    );

    let titulo_poliza = "EcopResguardo";
    //templateHtml = templateHtml.replace(/{{Ecoaguinaldo}}/g, titulo_poliza);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoMedic = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoMedic/orden de cobro.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hora}}",
      datos[0].fecha_emision
    );
    templateHtml = templateHtml.replace(
      "{{numero de documento}}",
      datos[0].nro_documento
    );

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroTarCreV2 = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/orden de cobro ECO-Riesgo.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace("{{fecha_hora}}", datos[0].fecha_emision);
    templateHtml = templateHtml.replace("{{numero de documento}}", datos[0].nro_documento);
    templateHtml = templateHtml.replace("{{monto_prima}}", util.formatNumber(datos[0].monto_prima,0));

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "1") {
      // titulo_poliza = "Ecoaguinaldo - Ecopasanaku";
      titulo_poliza = "Ecoriesgo";
    } else if (datos[0].id_poliza + "" === "11") {
      titulo_poliza = "Ecoriesgo";
    } else {
      titulo_poliza = "Ecopasanaku";
    }
    templateHtml = templateHtml.replace(/{{Ecoriesgo}}/g, titulo_poliza);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoVida = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoVida/orden de cobro.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hora}}",
      datos[0].fecha_emision
    );
    templateHtml = templateHtml.replace(
      "{{numero de documento}}",
      datos[0].nro_documento
    );

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "1") {
      titulo_poliza = "Ecoaguinaldo";
    } else {
      titulo_poliza = "Ecopasanaku";
    }
    //templateHtml = templateHtml.replace(/{{Ecoaguinaldo}}/g, titulo_poliza);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoAccidentes = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoAccidentes/orden de cobro.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hora}}",
      datos[0].fecha_emision
    );
    templateHtml = templateHtml.replace(
      "{{numero de documento}}",
      datos[0].nro_documento
    );

    let titulo_poliza = "EcopAccidentes";
    //templateHtml = templateHtml.replace(/{{Ecoaguinaldo}}/g, titulo_poliza);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoResguardo = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoResguardo/orden de cobro.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hora}}",
      datos[0].fecha_emision
    );
    templateHtml = templateHtml.replace(
      "{{numero de documento}}",
      datos[0].nro_documento
    );

    let titulo_poliza = "EcopResguardo";
    //templateHtml = templateHtml.replace(/{{Ecoaguinaldo}}/g, titulo_poliza);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarOrdenCobroEcoMedic = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });

  const options = {
    height: "65.4mm",
    width: "210.9mm",
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoMedic/orden de cobro.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(
      "{{numero de certificado}}",
      datos[0].nro_documento_sol
    );
    templateHtml = templateHtml.replace(
      "{{nombres}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{numero carnet}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{extencion}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{nombres_adicionado}}",
      datos[0].usuario_nombre_completo
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hora}}",
      datos[0].fecha_emision
    );
    templateHtml = templateHtml.replace(
      "{{numero de documento}}",
      datos[0].nro_documento
    );

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteSolicitud = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "10mm", // default is 0, units: mm, cm, in, px
      right: "10mm",
      bottom: "3mm",
      left: "10mm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/Formulario Solicitud ECO-Pasanaku y Aguinaldo.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace(
      "{{nro_documento}}",
      datos[0].nro_documento
    );
    templateHtml = templateHtml.replace(
      /{{nombres}}/g,
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{tipo_documento}}",
      datos[0].tipo_documento
    );
    templateHtml = templateHtml.replace(
      "{{persona_apellido_casada}}",
      datos[0].persona_apellido_casada
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_ext}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_comp}}",
      datos[0].persona_doc_id_comp
    );
    templateHtml = templateHtml.replace("{{sexo}}", datos[0].sexo);
    templateHtml = templateHtml.replace(
      "{{persona_direccion_domicilio}}",
      datos[0].persona_direccion_domicilio
    );
    templateHtml = templateHtml.replace(
      "{{persona_fecha_nacimiento}}",
      funciones.formatoFecha(datos[0].persona_fecha_nacimiento)
    );
    templateHtml = templateHtml.replace("{{cta}}", datos[0].cta);
    templateHtml = templateHtml.replace(
      "{{fecha_emision}}",
      funciones.formatoFecha(datos[0].fecha_emision)
    );
    templateHtml = templateHtml.replace(
      "{{persona_celular}}",
      datos[0].persona_celular
    );

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "1") {
      titulo_poliza = "ECOAGUINALDO - ECOPASANAKU";
    } else {
      titulo_poliza = "ECOPASANAKU";
    }
    templateHtml = templateHtml.replace("{{Ecoaguinaldo}}", titulo_poliza);

    let tabla_beneficiarios = "";
    datos.forEach((element, index) => {
      element = funciones.QuitarCamposNullos(element);
      tabla_beneficiarios =
        tabla_beneficiarios +
        `<tr><td colspan="2" style="font-size: 8pt"><strong> DATOS DEl BENEFICIARIO: Nro. ${
          index + 1
        }</strong></td></tr>
            <tr><td style="font-size: 8pt">Nombre Completo:</td><td style="font-size: 8pt">${
              element.persona_primer_apellido_b
            } ${element.persona_segundo_apellido_b} ${
          element.persona_primer_nombre_b
        } ${element.persona_segundo_nombre_b}</td></tr>
            <tr><td style="font-size: 8pt">No. Documento Identidad:</td><td style="font-size: 8pt">${
              element.persona_doc_id_b
            }</td></tr>
            <tr><td style="font-size: 8pt">Teléfono de Referencia:</td><td style="font-size: 8pt">${
              element.persona_celular_b
            }</td></tr>
            <tr><td style="font-size: 8pt">Parentesco</td><td style="font-size: 8pt">${
              element.parentesco
            }</td></tr>`;
    });
    templateHtml = templateHtml.replace(
      "{{tabla_beneficiarios}}",
      tabla_beneficiarios
    );
    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteSolicitudEcoRiesgo = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "10mm", // default is 0, units: mm, cm, in, px
      right: "10mm",
      bottom: "3mm",
      left: "10mm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/Formulario Solicitud ECO-Riesgo.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace(
      "{{nro_documento}}",
      datos[0].nro_documento
    );
    templateHtml = templateHtml.replace(
      /{{nombres}}/g,
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{tipo_documento}}",
      datos[0].tipo_documento
    );
    templateHtml = templateHtml.replace(
      "{{persona_apellido_casada}}",
      datos[0].persona_apellido_casada
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_ext}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_comp}}",
      datos[0].persona_doc_id_comp
    );
    templateHtml = templateHtml.replace("{{sexo}}", datos[0].sexo);
    templateHtml = templateHtml.replace(
      "{{persona_direccion_domicilio}}",
      datos[0].persona_direccion_domicilio
    );
    templateHtml = templateHtml.replace(
      "{{persona_fecha_nacimiento}}",
      funciones.formatoFecha(datos[0].persona_fecha_nacimiento)
    );
    templateHtml = templateHtml.replace("{{cta}}", datos[0].cta);
    templateHtml = templateHtml.replace(
      "{{fecha_emision}}",
      funciones.formatoFecha(datos[0].fecha_emision)
    );
    templateHtml = templateHtml.replace(
      "{{persona_celular}}",
      datos[0].persona_celular
    );

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "1") {
      // titulo_poliza = "ECOAGUINALDO - ECOPASANAKU";
      titulo_poliza = "ECORIESGO";
    } else if (datos[0].id_poliza + "" === "11") {
      titulo_poliza = "ECORIESGO";
    } else {
      titulo_poliza = "ECOPASANAKU";
    }
    templateHtml = templateHtml.replace("{{Ecoriesgo}}", titulo_poliza);

    let tabla_beneficiarios = "";
    datos.forEach((element, index) => {
      element = funciones.QuitarCamposNullos(element);
      tabla_beneficiarios =
        tabla_beneficiarios +
        `<tr><td colspan="2" style="font-size: 8pt"><strong> DATOS DEl BENEFICIARIO: Nro. ${index + 1}</strong></td></tr>
         <tr><td style="font-size: 8pt">Nombre Completo:</td><td style="font-size: 8pt">${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b}</td></tr>
         <tr><td style="font-size: 8pt">No. Documento Identidad:</td><td style="font-size: 8pt">${element.persona_doc_id_b}</td></tr>
         <tr><td style="font-size: 8pt">Teléfono de Referencia:</td><td style="font-size: 8pt">${element.persona_celular_b}</td></tr>
         <tr><td style="font-size: 8pt">Parentesco</td><td style="font-size: 8pt">${element.parentesco}</td></tr>`;
    });
    templateHtml = templateHtml.replace(
      "{{tabla_beneficiarios}}",
      tabla_beneficiarios
    );
    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteSolicitudEcoVida = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "10mm", // default is 0, units: mm, cm, in, px
      right: "10mm",
      bottom: "3mm",
      left: "10mm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoVida/SOLICITUD.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{numero}}", datos[0].nro_documento);
    templateHtml = templateHtml.replace("{{oficina}}", datos[0].agencia);
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace(
      /{{asegurado}}/g,
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{par_tipo_documento_id}}",
      datos[0].tipo_documento
    );
    templateHtml = templateHtml.replace(
      "{{persona_apellido_casada}}",
      datos[0].persona_apellido_casada
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_ext}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_comp}}",
      datos[0].persona_doc_id_comp
    );
    templateHtml = templateHtml.replace("{{par_sexo_id}}", datos[0].sexo);
    templateHtml = templateHtml.replace(
      "{{persona_celular}}",
      datos[0].persona_celular
    );
    templateHtml = templateHtml.replace(
      "{{persona_direccion_domicilio}}",
      datos[0].persona_direccion_domicilio
    );
    templateHtml = templateHtml.replace(
      "{{persona_fecha_nacimiento}}",
      funciones.formatoFecha(datos[0].persona_fecha_nacimiento)
    );
    templateHtml = templateHtml.replace("{{cta}}", datos[0].cta);
    templateHtml = templateHtml.replace(
      "{{fecha_emision}}",
      funciones.formatoFecha(datos[0].fecha_emision)
    );

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "1") {
      titulo_poliza = "ECOAGUINALDO";
    } else {
      titulo_poliza = "ECOPASANAKU";
    }
    templateHtml = templateHtml.replace("{{Ecoaguinaldo}}", titulo_poliza);

    let filas = "";
    datos.forEach((element, index) => {
      element = funciones.QuitarCamposNullos(element);
      let primera_celda = "";
      if (index === 0) {
        primera_celda = `C&oacute;nyuge o Beneficiario ${index + 1}`;
      } else {
        primera_celda = `Beneficiario ${index + 1}`;
      }
      let parametro_doc_id_ext;
      filas =
        filas +
        `<br>
    <tr>
                                        <td> 
                                            <p>
                                                ${element.persona_primer_apellido_b}&nbsp; 
                                                ${element.persona_segundo_apellido_b}&nbsp;
                                                ${element.persona_primer_nombre_b}&nbsp;
                                                ${element.persona_segundo_nombre_b} 
                                            </p>
                                        </td>            
                                        <td> 
                                        <p>${element.persona_doc_id_b} ${element.ext_b}</p>
                                                    </td>
                                                    
                                        <td> 
                                        <p>${element.porcentaje}</p>
                                                    </td>        
                                        <td> 
                                        <p>${element.parentesco}</p>
                                        </td>
                            </tr>`;
    });
    templateHtml = templateHtml.replace("{{tabla_beneficiarios}}", filas);
    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteSolicitudEcoAccidentes = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "10mm", // default is 0, units: mm, cm, in, px
      right: "10mm",
      bottom: "2mm",
      left: "10mm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoAccidentes/SOLICITUD_2.html`
    );
    const imagen1 = path.join(
        "file://",
        __dirname,
        "../plantillas_pdf/EcoAccidentes/logo_alianza.png"
    );
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{numero}}", datos[0].nro_documento);
    templateHtml = templateHtml.replace("{{oficina}}", datos[0].agencia);
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace(/{{asegurado}}/g, datos[0].persona_primer_apellido + " " + datos[0].persona_segundo_apellido + " " + (datos[0].persona_apellido_casada ? "de " + datos[0].persona_apellido_casada + " " : "") + datos[0].persona_primer_nombre + " " + datos[0].persona_segundo_nombre);
    templateHtml = templateHtml.replace(/{{asegurado_apellidos}}/g, datos[0].persona_primer_apellido + " " + datos[0].persona_segundo_apellido + " " + (datos[0].persona_apellido_casada ? "de " + datos[0].persona_apellido_casada + " " : ""));
    templateHtml = templateHtml.replace(/{{asegurado_nombres}}/g, datos[0].persona_primer_nombre + " " + datos[0].persona_segundo_nombre);
    templateHtml = templateHtml.replace("{{asegurado_masculino}}", datos[0].sexo_id ? datos[0].sexo_id == 6 ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_femenino}}", datos[0].sexo_id ? datos[0].sexo_id == 7 ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_fecha_nacimiento}}", funciones.formatoFecha(datos[0].persona_fecha_nacimiento));
    templateHtml = templateHtml.replace("{{asegurado_lugar_nacimiento}}", 'xxxxx');
    templateHtml = templateHtml.replace("{{asegurado_ciudad}}", datos[0].asegurado_ciudad_nacimiento);
    templateHtml = templateHtml.replace("{{asegurado_provincia}}", 'xxxxx');
    templateHtml = templateHtml.replace("{{asegurado_departamento}}", 'xxxxxx');
    templateHtml = templateHtml.replace("{{asegurado_direccion_domicilio}}", datos[0].persona_direccion_domicilio);
    templateHtml = templateHtml.replace("{{asegurado_telefono_domicilio}}", datos[0].persona_telefono_domicilio);
    templateHtml = templateHtml.replace("{{asegurado_celular}}", datos[0].persona_celular);
    templateHtml = templateHtml.replace("{{asegurado_correo}}", datos[0].asegurado_correo);
    templateHtml = templateHtml.replace("{{asegurado_edad}}", datos[0].asegurado_edad);
    templateHtml = templateHtml.replace("{{asegurado_doc_id}}", datos[0].persona_doc_id);
    templateHtml = templateHtml.replace("{{asegurado_doc_id_ext}}", datos[0].persona_doc_id_ext);
    templateHtml = templateHtml.replace("{{asegurado_soltero}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('solter') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_casado}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('casad') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_viudo}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('viud') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_divorciado}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('divorciad') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_conviviente}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('convivi') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{contado}}", datos[0].forma_pago && datos[0].forma_pago+'' == '304' ? datos[0].prima_total : '');
    templateHtml = templateHtml.replace("{{credito}}", datos[0].forma_pago && datos[0].forma_pago+'' == '303' ? datos[0].prima_total : '');
    templateHtml = templateHtml.replace("{{efectivo}}", datos[0].metodo_pago && datos[0].metodo_pago+'' == '57' ? datos[0].prima_total : '');
    templateHtml = templateHtml.replace("{{fecha_hoy}}", funciones.formatoFecha(new Date().toString()));
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace("{{agencia}}", datos[0].agencia);
    templateHtml = templateHtml.replace("{{imagen1}}", imagen1);

    templateHtml = templateHtml.replace(
      "{{par_tipo_documento_id}}",
      datos[0].tipo_documento
    );
    templateHtml = templateHtml.replace(
      "{{persona_apellido_casada}}",
      datos[0].persona_apellido_casada
    );
    templateHtml = templateHtml.replace("{{persona_doc_id}}", datos[0].persona_doc_id);
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_ext}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_comp}}",
      datos[0].persona_doc_id_comp
    );
    templateHtml = templateHtml.replace("{{par_sexo_id}}", datos[0].sexo);
    templateHtml = templateHtml.replace(
      "{{persona_celular}}",
      datos[0].persona_celular
    );
    templateHtml = templateHtml.replace(
      "{{persona_direccion_domicilio}}",
      datos[0].persona_direccion_domicilio
    );
    templateHtml = templateHtml.replace(
      "{{persona_fecha_nacimiento}}",
      funciones.formatoFecha(datos[0].persona_fecha_nacimiento)
    );
    templateHtml = templateHtml.replace("{{cta}}", datos[0].cta);
    templateHtml = templateHtml.replace(
      "{{fecha_emision}}",
      funciones.formatoFecha(datos[0].fecha_emision)
    );

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "10") {
      titulo_poliza = "ECOACCIDENTES";
    }
    templateHtml = templateHtml.replace("{{Ecoaguinaldo}}", titulo_poliza);

    let beneficiarios_primarios = "";
    let beneficiarios_contingentes = "";
    datos.forEach((element, index) => {
      element = funciones.QuitarCamposNullos(element);
      let primera_celda = "";
      if (index === 0) {
        primera_celda = `C&oacute;nyuge o Beneficiario ${index + 1}`;
      } else {
        primera_celda = `Beneficiario ${index + 1}`;
      }
      let parametro_doc_id_ext;
      if (element.condicion == 300) {
        beneficiarios_primarios = beneficiarios_primarios + `Nombres: ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b} Apellidos: ${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} Parentesco ${element.parentesco}<br><br>`;
      } else if( element.condicion == 301) {
        beneficiarios_contingentes = beneficiarios_contingentes + `Nombres: ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b} Apellidos: ${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} Parentesco ${element.parentesco}<br><br>`;
      } else {

      }
    });
    templateHtml = templateHtml.replace("{{beneficiarios_primarios}}", beneficiarios_primarios);
    templateHtml = templateHtml.replace("{{beneficiarios_contingentes}}", beneficiarios_contingentes);
    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteSolicitudEcoResguardo = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "10mm", // default is 0, units: mm, cm, in, px
      right: "10mm",
      bottom: "3mm",
      left: "10mm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(__dirname, `../public/pdf/${nombre_archivo}.pdf`);
    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;
    const template = path.join(__dirname, `../plantillas_pdf/EcoResguardo/SOLICITUD_2.html`);
    const imagen1 = path.join("file://", __dirname, "../plantillas_pdf/EcoResguardo/logo_alianza.png");
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{numero}}", datos[0].nro_documento);
    templateHtml = templateHtml.replace("{{oficina}}", datos[0].agencia);
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace(/{{asegurado}}/g, datos[0].persona_primer_apellido + " " + datos[0].persona_segundo_apellido + " " + (datos[0].persona_apellido_casada ? "de " + datos[0].persona_apellido_casada + " " : "") + datos[0].persona_primer_nombre + " " + datos[0].persona_segundo_nombre);
    templateHtml = templateHtml.replace(/{{asegurado_apellidos}}/g, datos[0].persona_primer_apellido + " " + datos[0].persona_segundo_apellido + " " + (datos[0].persona_apellido_casada ? "de " + datos[0].persona_apellido_casada + " " : ""));
    templateHtml = templateHtml.replace(/{{asegurado_nombres}}/g, datos[0].persona_primer_nombre + " " + datos[0].persona_segundo_nombre);
    templateHtml = templateHtml.replace("{{asegurado_masculino}}", datos[0].sexo_id ? datos[0].sexo_id == 6 ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_femenino}}", datos[0].sexo_id ? datos[0].sexo_id == 7 ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_fecha_nacimiento}}", funciones.formatoFecha(datos[0].persona_fecha_nacimiento));
    templateHtml = templateHtml.replace("{{asegurado_lugar_nacimiento}}", 'xxxxx');
    templateHtml = templateHtml.replace("{{asegurado_ciudad}}", datos[0].asegurado_ciudad_nacimiento);
    templateHtml = templateHtml.replace("{{asegurado_provincia}}", 'xxxxx');
    templateHtml = templateHtml.replace("{{asegurado_departamento}}", 'xxxxxx');
    templateHtml = templateHtml.replace("{{asegurado_direccion_domicilio}}", datos[0].persona_direccion_domicilio);
    templateHtml = templateHtml.replace("{{asegurado_telefono_domicilio}}", datos[0].persona_telefono_domicilio);
    templateHtml = templateHtml.replace("{{asegurado_celular}}", datos[0].persona_celular);
    templateHtml = templateHtml.replace("{{asegurado_correo}}", datos[0].asegurado_correo);
    templateHtml = templateHtml.replace("{{asegurado_edad}}", datos[0].asegurado_edad);
    templateHtml = templateHtml.replace("{{asegurado_doc_id}}", datos[0].persona_doc_id);
    templateHtml = templateHtml.replace("{{asegurado_doc_id_ext}}", datos[0].persona_doc_id_ext);
    templateHtml = templateHtml.replace("{{asegurado_soltero}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('solter') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_casado}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('casad') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_viudo}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('viud') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_divorciado}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('divorciad') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{asegurado_conviviente}}", datos[0].asegurado_estado_civil ? datos[0].asegurado_estado_civil.toLowerCase().includes('convivi') ? 'X':'':'');
    templateHtml = templateHtml.replace("{{contado}}", datos[0].forma_pago && datos[0].forma_pago+'' == '304' ? datos[0].prima_total : '');
    templateHtml = templateHtml.replace("{{credito}}", datos[0].forma_pago && datos[0].forma_pago+'' == '303' ? datos[0].prima_total : '');
    templateHtml = templateHtml.replace("{{efectivo}}", datos[0].metodo_pago && datos[0].metodo_pago+'' == '57' ? datos[0].prima_total : '');
    templateHtml = templateHtml.replace("{{fecha_hoy}}", funciones.formatoFecha(new Date().toString()));
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace("{{agencia}}", datos[0].agencia);
    templateHtml = templateHtml.replace("{{imagen1}}", imagen1);
    templateHtml = templateHtml.replace("{{par_tipo_documento_id}}", datos[0].tipo_documento);
    templateHtml = templateHtml.replace("{{persona_apellido_casada}}", datos[0].persona_apellido_casada);
    templateHtml = templateHtml.replace("{{persona_doc_id}}", datos[0].persona_doc_id);
    templateHtml = templateHtml.replace("{{persona_doc_id_ext}}", datos[0].persona_doc_id_ext);
    templateHtml = templateHtml.replace("{{persona_doc_id_comp}}", datos[0].persona_doc_id_comp);
    templateHtml = templateHtml.replace("{{par_sexo_id}}", datos[0].sexo);
    templateHtml = templateHtml.replace("{{persona_celular}}", datos[0].persona_celular);
    templateHtml = templateHtml.replace("{{persona_direccion_domicilio}}", datos[0].persona_direccion_domicilio);
    templateHtml = templateHtml.replace("{{persona_fecha_nacimiento}}", funciones.formatoFecha(datos[0].persona_fecha_nacimiento));
    templateHtml = templateHtml.replace("{{cta}}", datos[0].cta);
    templateHtml = templateHtml.replace("{{fecha_emision}}", funciones.formatoFecha(datos[0].fecha_emision));

    let titulo_poliza = "";
    if (datos[0].id_poliza + "" === "12") {
      titulo_poliza = "ECORESGUARDO";
    }
    templateHtml = templateHtml.replace("{{Ecoresguardo}}", titulo_poliza);

    let beneficiarios_primarios = "";
    let beneficiarios_contingentes = "";
    datos.forEach((element, index) => {
      element = funciones.QuitarCamposNullos(element);
      let primera_celda = "";
      if (index === 0) {
        primera_celda = `C&oacute;nyuge o Beneficiario ${index + 1}`;
      } else {
        primera_celda = `Beneficiario ${index + 1}`;
      }
      let parametro_doc_id_ext;
      if (element.condicion == 300) {
        beneficiarios_primarios = beneficiarios_primarios + `Nombres: ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b} Apellidos: ${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} Parentesco ${element.parentesco}<br><br>`;
      } else if( element.condicion == 301) {
        beneficiarios_contingentes = beneficiarios_contingentes + `Nombres: ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b} Apellidos: ${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} Parentesco ${element.parentesco}<br><br>`;
      } else {

      }
    });
    templateHtml = templateHtml.replace("{{beneficiarios_primarios}}", beneficiarios_primarios);
    templateHtml = templateHtml.replace("{{beneficiarios_contingentes}}", beneficiarios_contingentes);
    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

GenerarReporteSolicitudEcoMedic = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "10mm", // default is 0, units: mm, cm, in, px
      right: "10mm",
      bottom: "3mm",
      left: "10mm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(
      __dirname,
      `../plantillas_pdf/EcoMedic/SOLICITUD2.html`
    );
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{poliza}}", datos[0].poliza);
    templateHtml = templateHtml.replace("{{numero}}", datos[0].nro_documento);
    templateHtml = templateHtml.replace("{{oficina}}", datos[0].agencia);
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace(
      /{{asegurado}}/g,
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{par_tipo_documento_id}}",
      datos[0].tipo_documento
    );
    templateHtml = templateHtml.replace(
      "{{persona_apellido_casada}}",
      datos[0].persona_apellido_casada
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id}}",
      datos[0].persona_doc_id
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_ext}}",
      datos[0].persona_doc_id_ext
    );
    templateHtml = templateHtml.replace(
      "{{persona_doc_id_comp}}",
      datos[0].persona_doc_id_comp
    );
    templateHtml = templateHtml.replace("{{cta}}", datos[0].cta);
    templateHtml = templateHtml.replace("{{par_sexo_id}}", datos[0].sexo);
    templateHtml = templateHtml.replace(
      "{{persona_celular}}",
      datos[0].persona_celular
    );
    templateHtml = templateHtml.replace(
      "{{persona_direccion_domicilio}}",
      datos[0].persona_direccion_domicilio
    );
    templateHtml = templateHtml.replace(
      "{{persona_fecha_nacimiento}}",
      funciones.formatoFecha(datos[0].persona_fecha_nacimiento)
    );
    //templateHtml = templateHtml.replace('{{cuenta}}', datos[0].cuenta);
    templateHtml = templateHtml.replace(
      "{{fecha_emision}}",
      funciones.formatoFecha(datos[0].fecha_emision)
    );

    let texto = "";
    let debito_cuenta = "";
    let cuenta = "";
    if (datos[0].pago_efectivo !== "57") {
      texto =
        "En caso de ser admitida mi solicitud, me comprometo a cancelar el monto de la prima del seguro, mediante débito de mi cuenta de ahorros que se indica líneas más abajo:";
      debito_cuenta = "Y AUTORIZACI&Oacute;N DE D&Eacute;BITO EN CUENTA";
      cuenta = `<tr> 
            <td width="20%">Cuenta</td>
            <td>${datos[0].cuenta}</td>
          </tr>`;
    } else {
    }
    templateHtml = templateHtml.replace("{{texto}}", texto);
    templateHtml = templateHtml.replace("{{debito_cuenta}}", debito_cuenta);
    templateHtml = templateHtml.replace("{{cuenta}}", cuenta);
    let filas = "";
    datos.forEach((element, index) => {
      element = funciones.QuitarCamposNullos(element);
      let primera_celda = "";
      if (index === 0) {
        primera_celda = `C&oacute;nyuge o Beneficiario ${index + 1}`;
      } else {
        primera_celda = `Beneficiario ${index + 1}`;
      }
      let parametro_doc_id_ext;
      filas =
        filas +
        `<br>
    <tr>
                                        <td> 
                                            <p>
                                                ${element.persona_primer_apellido_b}&nbsp; 
                                                ${element.persona_segundo_apellido_b}&nbsp;
                                                ${element.persona_primer_nombre_b}&nbsp;
                                                ${element.persona_segundo_nombre_b} 
                                            </p>
                                        </td>            
                                        <td> 
                                        <p>${element.persona_doc_id_b} ${element.ext_b}</p>
                                                    </td>
                                                    
                                        <td> 
                                        <p>${element.porcentaje}</p>
                                                    </td>        
                                        <td> 
                                        <p>${element.parentesco}</p>
                                        </td>
                            </tr>`;
    });
    templateHtml = templateHtml.replace("{{tabla_beneficiarios}}", filas);
    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteSolicitudEcoProteccion = (datos, nombre_archivo) => {
  datos.forEach((element) => {
    element = funciones.QuitarCamposNullos(element);
  });
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "10mm", // default is 0, units: mm, cm, in, px
      right: "10mm",
      bottom: "3mm",
      left: "10mm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const template = path.join(__dirname,`../plantillas_pdf/EcoProteccion/SOLICITUD.html`);
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{poliza}}", datos[0].poliza);
    templateHtml = templateHtml.replace("{{numero}}", datos[0].nro_documento);
    templateHtml = templateHtml.replace("{{oficina}}", datos[0].agencia);
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace(/{{asegurado}}/g,datos[0].persona_primer_apellido +" " +datos[0].persona_segundo_apellido + " " + (datos[0].persona_apellido_casada ? "de " + datos[0].persona_apellido_casada + " " : "") + datos[0].persona_primer_nombre + " " + datos[0].persona_segundo_nombre);
    templateHtml = templateHtml.replace("{{par_tipo_documento_id}}",datos[0].tipo_documento);
    templateHtml = templateHtml.replace("{{persona_apellido_casada}}",datos[0].persona_apellido_casada);
    templateHtml = templateHtml.replace("{{persona_doc_id}}",datos[0].persona_doc_id);
    templateHtml = templateHtml.replace("{{persona_doc_id_ext}}",datos[0].persona_doc_id_ext);
    templateHtml = templateHtml.replace("{{persona_doc_id_comp}}",datos[0].persona_doc_id_comp);
    templateHtml = templateHtml.replace("{{par_sexo_id}}", datos[0].sexo);
    templateHtml = templateHtml.replace("{{persona_celular}}",datos[0].persona_celular);
    templateHtml = templateHtml.replace("{{persona_direccion_domicilio}}",datos[0].persona_direccion_domicilio);
    templateHtml = templateHtml.replace("{{persona_fecha_nacimiento}}",funciones.formatoFecha(datos[0].persona_fecha_nacimiento));
    templateHtml = templateHtml.replace("{{cta}}", datos[0].cta);
    templateHtml = templateHtml.replace("{{fecha_emision}}",funciones.formatoFecha(datos[0].fecha_emision));
    templateHtml = templateHtml.replace("{{modalidad}}", datos[0].modalidad);

    let titulo_poliza = "";
    if (datos[0].cod_modalidad + "" === "93") {
      titulo_poliza = "Bs176.- (ciento setenta y seis  00/100 BOLIVIANOS)";
    }
    if (datos[0].cod_modalidad + "" === "90") {
      titulo_poliza = "Bs16.- (dieciséis  00/100 BOLIVIANOS)";
    }
    templateHtml = templateHtml.replace("{{prima}}", titulo_poliza);

    let filas = "";
    datos.forEach((element, index) => {
      element = funciones.QuitarCamposNullos(element);
      let primera_celda = "";
      if (index === 0) {
        primera_celda = `C&oacute;nyuge o Beneficiario ${index + 1}`;
      } else {
        primera_celda = `Beneficiario ${index + 1}`;
      }
      let parametro_doc_id_ext;
      filas = filas +
        `<br>
    <tr>
                                        <td> 
                                            <p>
                                                ${element.persona_primer_apellido_b}&nbsp; 
                                                ${element.persona_segundo_apellido_b}&nbsp;
                                                ${element.persona_primer_nombre_b}&nbsp;
                                                ${element.persona_segundo_nombre_b} 
                                            </p>
                                        </td>            
                                        <td> 
                                        <p>${element.persona_doc_id_b} ${element.ext_b}</p>
                                                    </td>
                                                    
                                        <td> 
                                        <p>${element.porcentaje}</p>
                                                    </td>        
                                        <td> 
                                        <p>${element.parentesco}</p>
                                        </td>
                                        <td> 
                                        <p>${element.condicion}</p>
                                        </td>
                            </tr>`;
    });
    templateHtml = templateHtml.replace("{{tabla_beneficiarios}}", filas);
    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteSolicitudTarjetas = (datos, nombre_archivo) => {
  datos = funciones.QuitarCamposNullos(datos);
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "1.0cm", // default is 0, units: mm, cm, in, px
      right: "1cm",
      bottom: "1.0cm",
      left: "1cm",
    },
    footer: {
      height: "0.2cm",
      contents: {
        default: ``,
      },
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(__dirname,      `../public/pdf/${nombre_archivo}.pdf`);

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    const logo_banco = path.join("file://",__dirname,"../plantillas_pdf/TarjetaHabientes/clip_image002.jpg");
    const logo_aseguradora = path.join("file://",__dirname,"../plantillas_pdf/TarjetaHabientes/clip_image004.gif");
    let template = "";
    if (datos.id_poliza === "3") {
      template = path.join(__dirname,`../plantillas_pdf/TarjetaHabientes/Solicitud_Tarjeta_Credito.html`);
    }
    if (datos.id_poliza === "13") {
      template = path.join(__dirname,`../plantillas_pdf/TarjetaHabientes/Solicitud_Tarjeta_Credito_V1.html`);
    }
    if (datos.id_poliza === "4") {
      template = path.join(__dirname,`../plantillas_pdf/TarjetaHabientes/Solicitud_Tarjeta_Debito.html`);
    }
    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(/{{nro_sol}}/g, datos.nro_documento);
    templateHtml = templateHtml.replace(/{{logo_banco}}/g, logo_banco);
    templateHtml = templateHtml.replace(/{{logo_aseguradora}}/g,logo_aseguradora);
    templateHtml = templateHtml.replace(/{{nombres}}/g,datos.persona_primer_apellido +" " +datos.persona_segundo_apellido +" " +(datos.persona_apellido_casada? "de " + datos.persona_apellido_casada + " ": "") +datos.persona_primer_nombre +" " +datos.persona_segundo_nombre);
    templateHtml = templateHtml.replace(/{{carnet}}/g,datos.persona_doc_id +datos.persona_doc_id_comp +" " +datos.persona_doc_id_ext);
    templateHtml = templateHtml.replace(/{{direccion}}/g,datos.persona_direccion_domicilio);
    templateHtml = templateHtml.replace(/{{correo_electronico}}/g, datos.mail);
    templateHtml = templateHtml.replace(/{{numero_cuenta}}/g, datos.cta);
    templateHtml = templateHtml.replace(/{{fecha}}/g,funciones.formatoFechaDe(datos.fecha_registro));
    templateHtml = templateHtml.replace(/{{moneda}}/g, datos.moneda);
    templateHtml = templateHtml.replace(/{{monto_prima_des}}/g, miConversor.convertToText(datos.monto_prima).toUpperCase());
    templateHtml = templateHtml.replace(/{{moneda_prima}}/g, datos.moneda_prima);
    templateHtml = templateHtml.replace(/{{moneda_prima_des}}/g, datos.moneda_prima_des);
    templateHtml = templateHtml.replace(/{{modalidad}}/g, datos.modalidad);

    templateHtml = templateHtml.replace("{{poliza}}", datos.poliza);
    templateHtml = templateHtml.replace("{{numero}}", datos.nro_documento);
    templateHtml = templateHtml.replace("{{oficina}}", datos.agencia);
    templateHtml = templateHtml.replace("{{sucursal}}", datos.sucursal);
    templateHtml = templateHtml.replace(/{{asegurado}}/g,datos.persona_primer_apellido +" " +datos.persona_segundo_apellido + " " + (datos.persona_apellido_casada ? "de " + datos.persona_apellido_casada + " " : "") + datos.persona_primer_nombre + " " + datos.persona_segundo_nombre);
    templateHtml = templateHtml.replace("{{par_tipo_documento_id}}",datos.tipo_documento);
    templateHtml = templateHtml.replace("{{par_sexo_id}}",datos.sexo);
    templateHtml = templateHtml.replace("{{monto_prima}}",datos.monto_prima);
    templateHtml = templateHtml.replace("{{fecha_solicitud}}",new moment(datos.fecha_emision).format('DD/MM/YYYY'));
    templateHtml = templateHtml.replace("{{persona_apellido_casada}}",datos.persona_apellido_casada);
    templateHtml = templateHtml.replace("{{persona_doc_id}}",datos.persona_doc_id);
    templateHtml = templateHtml.replace("{{persona_doc_id_ext}}",datos.persona_doc_id_ext);
    templateHtml = templateHtml.replace("{{persona_doc_id_comp}}",datos.persona_doc_id_comp);
    templateHtml = templateHtml.replace("{{par_sexo_id}}", datos.sexo);
    templateHtml = templateHtml.replace("{{persona_celular}}",datos.persona_celular);
    templateHtml = templateHtml.replace("{{persona_direccion_domicilio}}",datos.persona_direccion_domicilio);
    templateHtml = templateHtml.replace("{{persona_fecha_nacimiento}}",funciones.formatoFecha(datos.persona_fecha_nacimiento));
    templateHtml = templateHtml.replace("{{cta}}", datos.cta);
    templateHtml = templateHtml.replace("{{fecha_emision}}",funciones.formatoFecha(datos.fecha_emision));
    templateHtml = templateHtml.replace("{{modalidad}}", datos.modalidad);
    datos = Array.isArray(datos) ? datos : [datos];
let filas = "";
    datos.forEach((element, index) => {
      element = funciones.QuitarCamposNullos(element);
      let primera_celda = "";
      if (index === 0) {
        primera_celda = `C&oacute;nyuge o Beneficiario ${index + 1}`;
      } else {
        primera_celda = `Beneficiario ${index + 1}`;
      }
      let parametro_doc_id_ext;
      filas = filas +
        `<br>
    <tr>
                                        <td> 
                                            <p>
                                                ${element.persona_primer_apellido_b}&nbsp; 
                                                ${element.persona_segundo_apellido_b}&nbsp;
                                                ${element.persona_primer_nombre_b}&nbsp;
                                                ${element.persona_segundo_nombre_b} 
                                            </p>
                                        </td>            
                                        <td> 
                                        <p>${element.persona_doc_id_b} ${element.ext_b}</p>
                                                    </td>
                                                    
                                        <td> 
                                        <p>${element.porcentaje}</p>
                                                    </td>        
                                        <td> 
                                        <p>${element.parentesco}</p>
                                        </td>
                            </tr>`;
    });
    templateHtml = templateHtml.replace("{{tabla_beneficiarios}}", filas);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteSolicitudEcoMedicEjemplo = (datos, nombre_archivo) => {
  datos = funciones.QuitarCamposNullos(datos);
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "0.63cm", // default is 0, units: mm, cm, in, px
      right: "0.35cm",
      bottom: "7.9cm",
      left: "0.35cm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;

    let template = "";
    template = path.join(
      __dirname,
      `../plantillas_pdf/TarjetaHabientes/Solicitud_Tarjeta_Debito.html`
    );

    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace(/{{numero}}/g, "");
    templateHtml = templateHtml.replace(/{{oficina}}/g, "");
    templateHtml = templateHtml.replace(/{{sucursal}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_primer_nombre}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_segundo_nombre}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_primer_apellido}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_segundo_apellido}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_apellido_casada}}/g, "");
    templateHtml = templateHtml.replace(/{{par_tipo_documento_id}}/g, "");
    templateHtml = templateHtml.replace(/{{{{persona_doc_id}}}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_doc_id_ext}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_doc_id_comp}}/g, "");
    templateHtml = templateHtml.replace(/{{par_sexo_id}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_celular}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_direccion_domicilio}}/g, "");
    templateHtml = templateHtml.replace(/{{persona_fecha_nacimiento}}/g, "");
    templateHtml = templateHtml.replace(/{{cta}}/g, "");
    templateHtml = templateHtml.replace(/{{plan}}/g, "");
    templateHtml = templateHtml.replace(/{{fecha_registro}}/g, "");

    let tabla_beneficiarios = "";
    datos.forEach((element) => {
      element = funciones.QuitarCamposNullos(element);
      tabla_beneficiarios =
        tabla_beneficiarios +
        `<tr>
            <td width="35%">${element.persona_primer_apellido} ${element.persona_segundo_apellido} ${element.persona_primer_nombre} ${element.persona_segundo_nombre}</td>
            <td width="25%">${element.persona_doc_id_b}&nbsp;${element.persona_doc_id__ext_b}</td>
            <td width="20%">${element.persona_fecha_nacimiento}</td>
            <td width="20%">${element.parentesco}</td>
            </tr>`;
    });
    templateHtml = templateHtml.replace(
      "{{tabla_beneficiarios}}",
      tabla_beneficiarios
    );

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteCertificadoTarjetas = (datos, nombre_archivo) => {
  datos = funciones.QuitarCamposNullos(datos);
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "0.5cm", // default is 0, units: mm, cm, in, px
      right: "1cm",
      bottom: "1cm",
      left: "1cm",
    },
    footer: {
      height: "0.2cm",
      contents: {
        default: `<span style="color: #444";><p align="center" style="font-family:Arial Narrow;font-size:8px;">
            SANTA CRUZ: Av. Santa Cruz Esq. Calle Jaurú No. 333 · Teléfono: 371-6262 · Fax: 333-7969 · LA PAZ: Av. Costanera Nro. 21, entre calles 8 y 9 Calacoto - <br> Edificio Nacional Seguros · Teléfono: 244-2942 · Fax: 244-2905 · COCHABAMBA: Av. América Norte Esq. Av. Adres Ibañez, No. 385 · Teléfono: 445-7100 · Fax: 445-7103
        </p></span>`,
      },
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(__dirname,`../public/pdf/${nombre_archivo}.pdf`);

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;
    let template = "";
    if (datos.id_poliza === "3") {
      template = path.join(__dirname,`../plantillas_pdf/TarjetaHabientes/Certificado_Tarjeta_Credito.html`);
    }
    if (datos.id_poliza === "13") {
      template = path.join(__dirname,`../plantillas_pdf/TarjetaHabientes/Certificado_Tarjeta_Credito_V1.html`);
    }
    if (datos.id_poliza === "4") {
      template = path.join(__dirname,`../plantillas_pdf/TarjetaHabientes/Certificado_Tarjeta_Debito.html`);
    }
    const codigo_qr = path.join("file://",__dirname, "../plantillas_pdf/TarjetaHabientes/codigo_qr_2.png");
    const firma_1 = path.join("file://",__dirname, "../plantillas_pdf/TarjetaHabientes/firma_1.png");
    const firma_2 = path.join("file://",__dirname, "../plantillas_pdf/TarjetaHabientes/firma_2.png");
    const logo_ecofuturo = path.join("file://",__dirname, "../plantillas_pdf/TarjetaHabientes/logo_ecofuturo.png");
    const logo_kerkus = path.join("file://",__dirname, "../plantillas_pdf/TarjetaHabientes/logo_kerkus.png");
    const logo_nacional_seguros = path.join("file://",__dirname, "../plantillas_pdf/TarjetaHabientes/logo_nacional_seguros.png");

    let templateHtml = fs.readFileSync(template, "utf8");

    templateHtml = templateHtml.replace("{{codigo_qr}}", codigo_qr);
    templateHtml = templateHtml.replace("{{firma_1}}", firma_1);
    templateHtml = templateHtml.replace("{{firma_2}}", firma_2);
    templateHtml = templateHtml.replace("{{logo_ecofuturo}}", logo_ecofuturo);
    templateHtml = templateHtml.replace("{{logo_kerkus}}",logo_kerkus);
    templateHtml = templateHtml.replace("{{logo_nacional_seguros}}",logo_nacional_seguros);

    templateHtml = templateHtml.replace("{{id_instancia_poliza}}",datos.numero);
    templateHtml = templateHtml.replace("{{nro_certificado}}",datos.nro_documento);
    templateHtml = templateHtml.replace("{{nro_tarjeta}}",datos.nro_tarjeta);
    templateHtml = templateHtml.replace("{{poliza}}",datos.poliza);
    templateHtml = templateHtml.replace("{{numero}}",datos.numero);
    templateHtml = templateHtml.replace("{{nombres}}",datos.persona_primer_apellido +" " +datos.persona_segundo_apellido +" " +(datos.persona_apellido_casada? "de " + datos.persona_apellido_casada + " ": "") +datos.persona_primer_nombre +" " +datos.persona_segundo_nombre);
    templateHtml = templateHtml.replace("{{fecha}}",new moment(datos.fecha_emision).format('DD/MM/YYYY'));

    let tabla_beneficiarios = "";
    datos = Array.isArray(datos) ? datos : [datos];
    datos.forEach((element) => {
      //if (element.id_beneficiario) {
        element = funciones.QuitarCamposNullos(element);
        tabla_beneficiarios = tabla_beneficiarios +
          `<span style="font-family: Arial, Helvetica, sans-serif; font-size: 8px; line-height: 1.15;">${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b} &ndash; ${element.persona_doc_id_b ? 'CI: ' + element.persona_doc_id_b +' '+element.persona_doc_id_ext_b : ''} - ${element.parentesco} - ${element.porcentaje}</span>`;
      //}
    });
    templateHtml = templateHtml.replace("{{filas}}", tabla_beneficiarios);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteCertificadoEcoVida = (datos, nombre_archivo) => {
  datos = funciones.QuitarCamposNullos(datos);
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "0.02cm", // default is 0, units: mm, cm, in, px
      right: "0.84cm",
      bottom: "0.25cm",
      left: "1cm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );
    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();

    const fecha = dd + "/" + mm + "/" + yyyy;

    let template = path.join(__dirname,`../plantillas_pdf/EcoVida/Certificado de Cobertura ECOVIDA v1.html`);
    const imagen1 = path.join("file://",__dirname,"../plantillas_pdf/EcoVida/ecopro1.jpg");
    const imagen2 = path.join("file://",__dirname,"../plantillas_pdf/EcoVida/ecoprotec2.jpg");
    const firma2 = path.join("file://",__dirname,"../plantillas_pdf/EcoVida/Gerente-General - Diego-Noriega_.png");
    const firma1 = path.join("file://",__dirname,"../plantillas_pdf/EcoVida/Sub Gerente Comercial Alianzas y Banca Seguros - Claudia Peñaranda.png");
    let persona_primer_apellido = datos[0].persona_primer_apellido ? datos[0].persona_primer_apellido  : "";
    let persona_segundo_apellido = datos[0].persona_segundo_apellido ? datos[0].persona_segundo_apellido : "";
    let persona_apellido_casada = datos[0].persona_apellido_casada ? "de " + datos[0].persona_apellido_casada : "";
    let persona_primer_nombre = datos[0].persona_primer_nombre ? datos[0].persona_primer_nombre : "";
    let persona_segundo_nombre = datos[0].persona_segundo_nombre ? datos[0].persona_segundo_nombre : "";
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{imagen1}}", imagen1);
    templateHtml = templateHtml.replace("{{imagen2}}", imagen2);
    templateHtml = templateHtml.replace("{{firma1}}", firma1);
    templateHtml = templateHtml.replace("{{firma2}}", firma2);
    templateHtml = templateHtml.replace( "{{asegurado}}", persona_primer_apellido + " " + persona_segundo_apellido + " " + persona_apellido_casada + " " + persona_primer_nombre + " " + persona_segundo_nombre );
    templateHtml = templateHtml.replace( "{{carnet}}", datos[0].persona_doc_id + " " + datos[0].extension);
    templateHtml = templateHtml.replace("{{fecha_nacimiento}}", datos[0].fecha_nacimiento );
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace("{{fecha_hoy}}", funciones.formatoFecha(datos[0].fecha_emision));
    templateHtml = templateHtml.replace("{{nro_certificado}}", datos[0].nro_certificado);
    templateHtml = templateHtml.replace("{{nro_solicitud}}",datos[0].nro_solicitud);
    templateHtml = templateHtml.replace("{{telefono}}", datos[0].persona_telefono_domicilio);

    let tabla_beneficiarios = "";
    datos.forEach((element) => {
      if (element.id_beneficiario) {
        element = funciones.QuitarCamposNullos(element);
        tabla_beneficiarios =
          tabla_beneficiarios +
          `<tr>
                <td width="254" nowrap valign="bottom"><p>${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b}</p></td>
                <td width="257" nowrap valign="bottom"><p>&nbsp;</p></td>
                <td width="12" nowrap valign="bottom"><p>&nbsp;</p>${element.porcentaje}</td>
                <td width="137" nowrap valign="bottom"><p>${element.parentesco}</p></td>
              </tr>`;
      }
    });

    templateHtml = templateHtml.replace("{{filas}}", tabla_beneficiarios);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteCertificadoEcoAccidentes = (datos, nombre_archivo, origin = null) => {
  datos = funciones.QuitarCamposNullos(datos);

  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "0.01cm", // default is 0, units: mm, cm, in, px
      right: "0.02cm",
      bottom: "0.01cm",
      left: "0.02cm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );
    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;
    let template = path.join(
      __dirname,
      `../plantillas_pdf/EcoAccidentes/Certificado de Cobertura ECOACCIDENTES v2.html`
    );
    const imagen1 = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoAccidentes/logo_alianza.png"
    );
    const imagen2 = "assets/borde_certificado.png";
    const firma2 = path.join("file://", __dirname, "../plantillas_pdf/EcoAccidentes/Sergio_Manuel_Molina_Comboni.png");
    const firma1 = path.join("file://", __dirname, "../plantillas_pdf/EcoAccidentes/Fabricio_Amelunge _Mendez.png");
    let persona_primer_apellido = datos[0].persona_primer_apellido ? datos[0].persona_primer_apellido : "";
    let persona_segundo_apellido = datos[0].persona_segundo_apellido ? datos[0].persona_segundo_apellido : "";
    let persona_apellido_casada = datos[0].persona_apellido_casada ? "de " + datos[0].persona_apellido_casada : "";
    let persona_primer_nombre = datos[0].persona_primer_nombre ? datos[0].persona_primer_nombre : "";
    let persona_segundo_nombre = datos[0].persona_segundo_nombre ? datos[0].persona_segundo_nombre : "";
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{origin}}", origin);
    templateHtml = templateHtml.replace("{{imagen1}}", imagen1);
    templateHtml = templateHtml.replace("{{imagen2}}", imagen2);
    templateHtml = templateHtml.replace("{{firma1}}", firma1);
    templateHtml = templateHtml.replace("{{firma2}}", firma2);
    templateHtml = templateHtml.replace("{{asegurado}}", persona_primer_apellido + " " + persona_segundo_apellido + " " + persona_apellido_casada + " " + persona_primer_nombre + " " + persona_segundo_nombre);
    templateHtml = templateHtml.replace("{{carnet}}", datos[0].persona_doc_id + " " + datos[0].extension);
    templateHtml = templateHtml.replace("{{fecha_nacimiento}}", datos[0].fecha_nacimiento);
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace("{{fecha_hoy}}", funciones.formatoFecha(datos[0].fecha_emision));
    templateHtml = templateHtml.replace("{{nro_certificado}}", datos[0].nro_certificado);
    templateHtml = templateHtml.replace("{{nro_poliza}}", datos[0].poliza);
    templateHtml = templateHtml.replace("{{fecha_desde}}", datos[0].fecha_inicio_vigencia);
    templateHtml = templateHtml.replace(
      "{{numero}}",
        datos[0].numero
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hasta}}",
        datos[0].fecha_fin_vigencia
    );
    templateHtml = templateHtml.replace(
      "{{nro_solicitud}}",
      datos[0].nro_solicitud
    );
    templateHtml = templateHtml.replace(
      "{{telefono}}",
      datos[0].persona_telefono_domicilio
    );

    let tabla_beneficiarios = "";
    datos.forEach((element) => {
      if (element.id_beneficiario) {
        element = funciones.QuitarCamposNullos(element);
        tabla_beneficiarios =
          tabla_beneficiarios +
          `<tr>
                <td width="254" nowrap valign="bottom"><p>${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b}</p></td>
                <td width="257" nowrap valign="bottom"><p>&nbsp;</p></td>
                <td width="12" nowrap valign="bottom"><p>&nbsp;</p>${element.porcentaje}</td>
                <td width="137" nowrap valign="bottom"><p>${element.parentesco}</p></td>
              </tr>`;
      }
    });

    templateHtml = templateHtml.replace("{{filas}}", tabla_beneficiarios);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteCertificadoEcoResguardo = (datos, nombre_archivo, origin = null) => {
  datos = funciones.QuitarCamposNullos(datos);

  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "0.01cm", // default is 0, units: mm, cm, in, px
      right: "0.02cm",
      bottom: "0.01cm",
      left: "0.02cm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );
    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;
    let template = path.join(
      __dirname,
      `../plantillas_pdf/EcoResguardo/Certificado de Cobertura ECORESGUARDO v2.html`
    );
    const imagen1 = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoResguardo/logo_alianza.png"
    );
    const imagen2 = "assets/borde_certificado.png";
    const firma2 = path.join("file://", __dirname, "../plantillas_pdf/EcoResguardo/Sergio_Manuel_Molina_Comboni.png");
    const firma1 = path.join("file://", __dirname, "../plantillas_pdf/EcoResguardo/Fabricio_Amelunge _Mendez.png");
    let persona_primer_apellido = datos[0].persona_primer_apellido ? datos[0].persona_primer_apellido : "";
    let persona_segundo_apellido = datos[0].persona_segundo_apellido ? datos[0].persona_segundo_apellido : "";
    let persona_apellido_casada = datos[0].persona_apellido_casada ? "de " + datos[0].persona_apellido_casada : "";
    let persona_primer_nombre = datos[0].persona_primer_nombre ? datos[0].persona_primer_nombre : "";
    let persona_segundo_nombre = datos[0].persona_segundo_nombre ? datos[0].persona_segundo_nombre : "";
    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{origin}}", origin);
    templateHtml = templateHtml.replace("{{imagen1}}", imagen1);
    templateHtml = templateHtml.replace("{{imagen2}}", imagen2);
    templateHtml = templateHtml.replace("{{firma1}}", firma1);
    templateHtml = templateHtml.replace("{{firma2}}", firma2);
    templateHtml = templateHtml.replace("{{asegurado}}", persona_primer_apellido + " " + persona_segundo_apellido + " " + persona_apellido_casada + " " + persona_primer_nombre + " " + persona_segundo_nombre);
    templateHtml = templateHtml.replace("{{carnet}}", datos[0].persona_doc_id + " " + datos[0].extension);
    templateHtml = templateHtml.replace("{{fecha_nacimiento}}", datos[0].fecha_nacimiento);
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace("{{fecha_hoy}}", funciones.formatoFecha(datos[0].fecha_emision));
    templateHtml = templateHtml.replace("{{nro_certificado}}", datos[0].nro_certificado);
    templateHtml = templateHtml.replace("{{fecha_desde}}", datos[0].fecha_inicio_vigencia);
    templateHtml = templateHtml.replace(
      "{{numero}}",
        datos[0].numero
    );
    templateHtml = templateHtml.replace(
      "{{fecha_hasta}}",
        datos[0].fecha_fin_vigencia
    );
    templateHtml = templateHtml.replace(
      "{{nro_solicitud}}",
      datos[0].nro_solicitud
    );
    templateHtml = templateHtml.replace(
      "{{telefono}}",
      datos[0].persona_telefono_domicilio
    );

    let tabla_beneficiarios = "";
    datos.forEach((element) => {
      if (element.id_beneficiario) {
        element = funciones.QuitarCamposNullos(element);
        tabla_beneficiarios =
          tabla_beneficiarios +
          `<tr>
                <td width="254" nowrap valign="bottom"><p>${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b}</p></td>
                <td width="257" nowrap valign="bottom"><p>&nbsp;</p></td>
                <td width="12" nowrap valign="bottom"><p>&nbsp;</p>${element.porcentaje}</td>
                <td width="137" nowrap valign="bottom"><p>${element.parentesco}</p></td>
              </tr>`;
      }
    });

    templateHtml = templateHtml.replace("{{filas}}", tabla_beneficiarios);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteCertificadoEcoProteccion = (datos, nombre_archivo) => {
  datos = funciones.QuitarCamposNullos(datos);
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "0.72cm", // default is 0, units: mm, cm, in, px
      right: "0.84cm",
      bottom: "0.25cm",
      left: "1cm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "/" + mm + "/" + yyyy;
    let template = path.join(
      __dirname,
      `../plantillas_pdf/EcoProteccion/Certificado de Cobertura ECOPROTECCION.html`
    );
    const imagen = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoProteccion/untitled.jpg"
    );
    const imagen2 = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoProteccion/untitled3.jpg"
    );
    const imagen3 = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoProteccion/untitled2.jpg"
    );
    const imagen4 = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoProteccion/logo_corredores_kerkus.png"
    );
    const firma2 = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoVida/Gerente-General - Diego-Noriega_.png"
    );
    const firma1 = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoVida/Sub Gerente Comercial Alianzas y Banca Seguros - Claudia Peñaranda.png"
    );

    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{imagen}}", imagen);
    templateHtml = templateHtml.replace("{{imagen2}}", imagen2);
    templateHtml = templateHtml.replace("{{imagen3}}", imagen3);
    templateHtml = templateHtml.replace("{{imagen4}}", imagen4);
    templateHtml = templateHtml.replace("{{firma1}}", firma1);
    templateHtml = templateHtml.replace("{{firma2}}", firma2);

    templateHtml = templateHtml.replace("{{cta}}", datos[0].cta);
    templateHtml = templateHtml.replace(
      "{{asegurado}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{carnet}}",
      datos[0].persona_doc_id + " " + datos[0].extension
    );
    templateHtml = templateHtml.replace(
      "{{fecha_nacimiento}}",
      datos[0].fecha_nacimiento
    );
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace(
      "{{fecha_hoy}}",
      funciones.formatoFecha(datos[0].fecha_emision)
    );
    templateHtml = templateHtml.replace(
      "{{nro_certificado}}",
      datos[0].nro_certificado
    );
    templateHtml = templateHtml.replace(
      "{{nro_solicitud}}",
      datos[0].nro_solicitud
    );

    let tabla_beneficiarios = "";
    datos.forEach((element) => {
      if (element.id_beneficiario) {
        element = funciones.QuitarCamposNullos(element);
        tabla_beneficiarios =
          tabla_beneficiarios +
          `<tr>
                <td width="38.25%" nowrap colspan="2" valign="bottom"><p>${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b}</p></td>
                <td width="41.56%" nowrap colspan="2" valign="bottom"><p>&nbsp;</p></td>
                <td width="2.87%" nowrap valign="bottom"><p>&nbsp;</p>${element.porcentaje}</td>
                <td width="17.32%" nowrap colspan="2" valign="bottom"><p>${element.parentesco}</p></td>
              </tr>`;
      }
    });
    templateHtml = templateHtml.replace("{{filas}}", tabla_beneficiarios);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

generarReporteCertificadoEcoMedic2 = (datos, nombre_archivo, documentoVersion) => {
  datos = funciones.QuitarCamposNullos(datos);
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "0.72cm", // default is 0, units: mm, cm, in, px
      right: "0.4cm",
      bottom: "2.54cm",
      left: "0.4cm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    const filename = path.join(
      __dirname,
      `../public/pdf/${nombre_archivo}.pdf`
    );

    let pagesHtml = "";
    var dt = new Date();
    var mm = dt.getMonth() + 1;
    var dd = dt.getDate();
    var yyyy = dt.getFullYear();
    const fecha = dd + "-" + mm + "-" + yyyy;
    let template = path.join(
      __dirname,
      `../plantillas_pdf/EcoMedic/CertEcoMedic2.html`
    );
    const imagen = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoMedic/clip_image002_0000.jpg"
    );
    const firma2 = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoVida/Gerente-General - Diego-Noriega_.png"
    );
    const firma1 = path.join(
      "file://",
      __dirname,
      "../plantillas_pdf/EcoVida/Sub Gerente Comercial Alianzas y Banca Seguros - Claudia Peñaranda.png"
    );

    let templateHtml = fs.readFileSync(template, "utf8");
    templateHtml = templateHtml.replace("{{imagen}}", imagen);
    templateHtml = templateHtml.replace("{{firma1}}", firma1);
    templateHtml = templateHtml.replace("{{firma2}}", firma2);
    templateHtml = templateHtml.replace(
      "{{numero_poliza}}",
      datos[0].numero_poliza
    );
    templateHtml = templateHtml.replace(
      "{{asegurado}}",
      datos[0].persona_primer_apellido +
        " " +
        datos[0].persona_segundo_apellido +
        " " +
        (datos[0].persona_apellido_casada
          ? "de " + datos[0].persona_apellido_casada + " "
          : "") +
        datos[0].persona_primer_nombre +
        " " +
        datos[0].persona_segundo_nombre
    );
    templateHtml = templateHtml.replace(
      "{{carnet}}",
      datos[0].persona_doc_id + " " + datos[0].extension
    );
    templateHtml = templateHtml.replace(
      "{{fecha_nacimiento}}",
      datos[0].fecha_nacimiento
    );
    templateHtml = templateHtml.replace("{{sucursal}}", datos[0].sucursal);
    templateHtml = templateHtml.replace(
      "{{fecha_hoy}}",
      funciones.formatoFecha(datos[0].fecha_emision)
    );
    templateHtml = templateHtml.replace(
      "{{nro_certificado}}",
      datos[0].nro_certificado
    );
    templateHtml = templateHtml.replace(
      "{{nro_solicitud}}",
      datos[0].nro_solicitud
    );

    let tabla_beneficiarios = "";
    datos.forEach((element) => {
      if (element.id_beneficiario) {
        element = funciones.QuitarCamposNullos(element);
        tabla_beneficiarios =
          tabla_beneficiarios +
          `<tr>
                <td width="246" valign="top">${element.persona_primer_apellido_b} ${element.persona_segundo_apellido_b} ${element.persona_primer_nombre_b} ${element.persona_segundo_nombre_b}</td>
                <td width="225" valign="top">${element.parentesco}</td>
                <td width="225" valign="top">${element.porcentaje}</td>
                
              </tr>`;
      }
    });
    templateHtml = templateHtml.replace("{{filas}}", tabla_beneficiarios);

    pagesHtml += templateHtml;

    pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
      if (err) {
        console.log(`ERROR, generando pdf : ${err}`);
        response.status = "ERROR";
        response.message = JSON.stringify(err);
        resolve(response);
      } else {
        resolve(response);
      }
    });
  });
};

GenerarReporteCertificadoEcoMedic = (asegurado,anexos,parametros,nombre_archivo) => {
  //datos=funciones.QuitarCamposNullos(datos);
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "1.50cm", // default is 0, units: mm, cm, in, px
      right: "0.39cm",
      bottom: "1cm",
      left: "0.28cm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    try {
      const filename = path.join(
        __dirname,
        `../public/pdf/${nombre_archivo}.pdf`
      );

      let pagesHtml = "";
      var template;
      var dt = new Date();
      var mm = dt.getMonth() + 1;
      var dd = dt.getDate();
      var yyyy = dt.getFullYear();
      const fecha = dd + "/" + mm + "/" + yyyy;
      const logo_credinform = path.join(
        "file://",
        __dirname,
        "../plantillas_pdf/EcoMedic/image001.gif"
      );
      const firma1 = path.join(
        "file://",
        __dirname,
        "../plantillas_pdf/EcoMedic/image009.jpg"
      );
      const firma2 = path.join(
        "file://",
        __dirname,
        "../plantillas_pdf/EcoMedic/image010.jpg"
      );
      asegurado.instancia_poliza.atributo_instancia_polizas.forEach(
        (atributo_instancia_poliza) => {
          switch (
            atributo_instancia_poliza.objeto_x_atributo.atributo.id.toString()
          ) {
            case "38":
              switch (atributo_instancia_poliza.valor.toString()) {
                case "1":
                  template = path.join(
                    __dirname,
                    `../plantillas_pdf/EcoMedic/CertEcoMedic.html`
                  );
                  break;
                case "2":
                  template = path.join(
                    __dirname,
                    `../plantillas_pdf/EcoMedic/CertEcoMedicHost.html`
                  );
                  break;
              }
              break;
          }
        }
      );
      let templateHtml = fs.readFileSync(template, "utf8");
      asegurado.instancia_poliza.atributo_instancia_polizas.forEach(
        (atributo_instancia_poliza) => {
          switch (
            atributo_instancia_poliza.objeto_x_atributo.atributo.id.toString()
          ) {
            case "4":
              templateHtml = templateHtml.replace(
                /{{atributo.email}}/g,
                atributo_instancia_poliza.valor
              );
              break;
            case "58":
              templateHtml = templateHtml.replace(
                /{{atributo.plazo}}/g,
                atributo_instancia_poliza.valor
              );
              break;
            case "59":
              parametros.forEach((parametro) => {
                if (
                  parametro.parametro_cod == atributo_instancia_poliza.valor
                ) {
                  templateHtml = templateHtml.replace(
                    /{{atributo.sucursal}}/g,
                    parametro.parametro_descripcion
                  );
                }
              });
              break;
            case "28":
              parametros.forEach((parametro) => {
                if (parametro.id == atributo_instancia_poliza.valor) {
                  templateHtml = templateHtml.replace(
                    /{{atributo.plazo_desc}}/g,
                    parametro.parametro_descripcion
                  );
                }
              });
              break;
            case "63":
              templateHtml = templateHtml.replace(
                /{{atributo.ciudad_nacimiento}}/g,
                atributo_instancia_poliza.valor
              );
              break;
          }
        }
      );
      templateHtml = templateHtml.replace(/{{atributo.sucursal}}/g, "");
      templateHtml = templateHtml.replace(/{{atributo.oficina}}/g, "");
      templateHtml = templateHtml.replace(/{{atributo.plazo_desc}}/g, "");

      templateHtml = templateHtml.replace(/{{firma1}}/g, firma1);
      templateHtml = templateHtml.replace(/{{firma2}}/g, firma2);
      templateHtml = templateHtml.replace(
        /{{logo_credinform}}/g,
        logo_credinform
      );
      templateHtml = templateHtml.replace(
        /{{poliza.numero}}/g,
        asegurado.instancia_poliza.poliza.numero
      );
      asegurado.instancia_poliza.instancia_documentos.forEach(
        (instancia_documento) => {
          switch (instancia_documento.documento.id.toString()) {
            case "15":
              templateHtml = templateHtml.replace(
                /{{instancia_documento.nro_documento}}/g,
                instancia_documento.nro_documento
              );
              templateHtml = templateHtml.replace(
                /{{instancia_documento.fecha_emision}}/g,
                funciones.formatoFecha(instancia_documento.fecha_emision)
              );
              break;
          }
        }
      );
      templateHtml = templateHtml.replace(
        /{{asegurado.entidad.persona.persona_primer_apellido}}/g,
        asegurado.entidad.persona.persona_primer_apellido
      ); //nro de certificado
      templateHtml = templateHtml.replace(
        /{{asegurado.entidad.persona.persona_segundo_apellido}}/g,
        asegurado.entidad.persona.persona_segundo_apellido
      ); //nro de certificado
      templateHtml = templateHtml.replace(
        /{{asegurado.entidad.persona.persona_primer_nombre}}/g,
        asegurado.entidad.persona.persona_primer_nombre
      ); //nro de certificado
      templateHtml = templateHtml.replace(
        /{{asegurado.entidad.persona.persona_segundo_nombre}}/g,
        asegurado.entidad.persona.persona_segundo_nombre
      );
      templateHtml = templateHtml.replace(
        /{{asegurado.entidad.persona.persona_segundo_nombre}}/g,
        ""
      );

      templateHtml = templateHtml.replace(
        /{{asegurado.entidad.persona.persona_doc_id}}/g,
        asegurado.entidad.persona.persona_doc_id
      );
      parametros.forEach((parametro) => {
        if (
          parametro.parametro_cod ==
          asegurado.entidad.persona.persona_doc_id_ext
        ) {
          templateHtml = templateHtml.replace(
            /{{asegurado.entidad.persona.persona_doc_id_ext}}/g,
            parametro.parametro_abreviacion
          );
        }
      });
      parametros.forEach((parametro) => {
        if (
          parametro.parametro_cod ==
          asegurado.entidad.persona.persona_doc_id_ext
        ) {
          templateHtml = templateHtml.replace(
            /{{asegurado.entidad.persona.persona_doc_id_ext_desc}}/g,
            parametro.parametro_descripcion
          );
        }
      });

      templateHtml = templateHtml.replace(
        /{{asegurado.entidad.persona.persona_fecha_nacimiento}}/g,
        funciones.formatoFecha(
          asegurado.entidad.persona.persona_fecha_nacimiento
        )
      );
      templateHtml = templateHtml.replace(/{{asegurado.entidad.persona.persona_edad}}/g,funciones.CalcularEdad(asegurado.entidad.persona.persona_fecha_nacimiento)
      );
      if (asegurado.entidad.persona.par_sexo.parametro_abreviacion == "M") {
        templateHtml = templateHtml.replace(/{{asegurado.entidad.persona.par_sexo.parametro_abreviacion.Masculino}}/g,"X");
      } else if (
        asegurado.entidad.persona.par_sexo.parametro_abreviacion == "F"
      ) {
        templateHtml = templateHtml.replace(/{{asegurado.entidad.persona.par_sexo.parametro_abreviacion.Femenino}}/g,"X");
      }
      templateHtml = templateHtml.replace(/{{asegurado.entidad.persona.par_sexo.parametro_abreviacion.Femenino}}/g,"");
      templateHtml = templateHtml.replace(/{{asegurado.entidad.persona.par_sexo.parametro_abreviacion.Masculino}}/g,"");
      templateHtml = templateHtml.replace(/{{titular_relacion}}/g, "TITULAR");
      templateHtml = templateHtml.replace(/{{asegurado.entidad.persona.persona_direccion_domicilio}}/g,asegurado.entidad.persona.persona_direccion_domicilio);
      templateHtml = templateHtml.replace(/{{asegurado.entidad.persona.persona_celular}}/g,asegurado.entidad.persona.persona_celular);
      asegurado.instancia_poliza.poliza.anexo_polizas.forEach(
        (anexo_poliza) => {
          switch (anexo_poliza.id.toString()) {
            case "1":
              templateHtml = templateHtml.replace(
                /{{atributo.moneda}}/g,
                anexo_poliza.plan_moneda.parametro_abreviacion
              );
              break;
          }
        });
      templateHtml = templateHtml.replace(/{{asegurado.entidad.persona.persona_doc_id}}/g,asegurado.entidad.persona.persona_doc_id);
      templateHtml = templateHtml.replace(/{{asegurado.entidad.persona.persona_doc_id_ext}}/g,asegurado.entidad.persona.par_tipo_documento.parametro_abreviacion);

      let filas = "";
      datos = [];
      for (let i = 0; i < asegurado.instancia_anexo_asegurados.length; i++) {
        let element = asegurado.instancia_anexo_asegurados[i];
        let primera_celda = "";
        if (i === 0) {
          primera_celda = `Dependiente ${i + 1}`;
        } else {
          primera_celda = `Dependiente ${i + 1}`;
        }
        let parametro_doc_id_ext;
        parametros.forEach((parametro) => {
          if (
            parametro.parametro_cod ==
            element.entidad.persona.persona_doc_id_ext
          ) {
            parametro_doc_id_ext = parametro.parametro_abreviacion;
          }
        });
        let femenino = "",
          masculino = "";
        if (element.entidad.persona.par_sexo.parametro_abreviacion == "M") {
          masculino = "X";
        } else if (
          element.entidad.persona.par_sexo.parametro_abreviacion == "F"
        ) {
          femenino = "X";
        }
        filas =
          filas +
          `<tr>
                                    <td>
                                        <p>${primera_celda}</p>
                                    </td>
                                    
                                    <td> 
                                    <p>${
                                      element.entidad.persona
                                        .persona_primer_apellido
                                    }</p>
                                                </td>
                                                
                                    <td> 
                                    <p>${
                                      element.entidad.persona
                                        .persona_segundo_apellido
                                    }</p>
                                                </td>
                                                
                                    <td> 
                                    <p>${
                                      element.entidad.persona
                                        .persona_primer_nombre
                                    } ${
            element.entidad.persona.persona_segundo_nombre
          } </p>
                                                </td>
                                                
                                    <td> 
                                    <p>${
                                      element.entidad.persona.persona_doc_id
                                    } ${parametro_doc_id_ext}</p>
                                                </td>
                                                
                                    <td> 
                                    <p>${funciones.formatoFecha(
                                      element.entidad.persona
                                        .persona_fecha_nacimiento
                                    )}</p>
                                                </td>
                                                
                                    <td> 
                                    <p>${funciones.CalcularEdad(
                                      element.entidad.persona
                                        .persona_fecha_nacimiento
                                    )}</p>
                                                </td>
                                                <td>
                                                    <table border="0" width="100%" style="font-family:Times New Roman;font-size:7pt;text-align: justify;">
                                                        <tbody>
                                                            <tr>
                                                              
                                        <td width="50%"> 
                                            <p>${femenino}</p>
                                                                </td>
                                                                <td width="50%">
                                                                                                                                    
                                            <p>${masculino}</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                
                                    <td> 
                                    <p>${
                                      element.par_parentesco
                                        .parametro_descripcion
                                    }</p>
                                    </td>
                        </tr>`;
      }
      templateHtml = templateHtml.replaceAll("{{filas}}", filas);

      pagesHtml += templateHtml;

      pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
        if (err) {
          console.log(`ERROR, generando pdf : ${err}`);
          response.status = "ERROR";
          response.message = JSON.stringify(err);
          resolve(response);
        } else {
          resolve(response);
        }
      });
    } catch (e) {
      console.log(e);
    }
  });
};

generarReportePlanPago = (datos, nombre_archivo) => {
  //datos=funciones.QuitarCamposNullos(datos);
  const options = {
    height: "279.4mm",
    width: "215.9mm",
    border: {
      top: "1cm", // default is 0, units: mm, cm, in, px
      right: "1cm",
      bottom: "1cm",
      left: "1cm",
    },
  };

  let response = { status: "OK", message: "genera-pdf", data: {} };

  return new Promise((resolve, reject) => {
    try {
      const filename = path.join(
        __dirname,
        `../public/pdf/${nombre_archivo}.pdf`
      );
      let pagesHtml = "";
      var template;
      var dt = new Date();
      var mm = dt.getMonth() + 1;
      var dd = dt.getDate();
      var yyyy = dt.getFullYear();
      const fecha = dd + "/" + mm + "/" + yyyy;
      template = path.join(__dirname, `../plantillas_pdf/Plan Pago.html`);
      let templateHtml = fs.readFileSync(template, "utf8");
      templateHtml = templateHtml.replace(/{{asegurado}}/g, datos[0].asegurado);
      templateHtml = templateHtml.replace("{{poliza}}", datos[0].poliza);
      templateHtml = templateHtml.replace("{{fecha}}", fecha);
      templateHtml = templateHtml.replace(
        "{{total_prima}}",
        datos[0].total_prima
      );
      templateHtml = templateHtml.replace(
        "{{nro_certificado}}",
        datos[0].nro_certificado
      );
      templateHtml = templateHtml.replace(
        "{{periodicidad_anual}}",
        datos[0].periodicidad_anual
      );
      templateHtml = templateHtml.replace(
        "{{fecha_emision}}",
        datos[0].fecha_emision
      );
      templateHtml = templateHtml.replace(
        "{{fecha_inicio_vigencia}}",
        datos[0].fecha_inicio_vigencia
      );
      templateHtml = templateHtml.replace(
        "{{fecha_fin_vigencia}}",
        datos[0].fecha_fin_vigencia
      );
      templateHtml = templateHtml.replace(
        "{{plazo_anos}}",
        datos[0].plazo_anos
      );
      templateHtml = templateHtml.replace("{{moneda}}", datos[0].moneda);

      let tabla_beneficiarios = "";
      let suma = 0;
      datos.forEach((element) => {
        suma = suma + element.pago_cuota_prog;
        element = funciones.QuitarCamposNullos(element);
        tabla_beneficiarios =
          tabla_beneficiarios +
          `<tr>
                    <td width="56" valign="top"><p align="center">${element.nro_cuota_prog}</p></td>
                    <td width="132" valign="top"><p align="center">${element.fecha_couta_prog}</p></td>
                    <td width="132" valign="top"><p align="right">${element.pago_cuota_prog}</p></td>
                    <td width="132" valign="top"><p align="right">${element.fecha_limite}</p></td>
                    <td width="123" valign="top"><p align="right">${element.pago_prima_acumulado}</p></td>
                    <td width="113" valign="top"><p align="right">${element.prima_pendiente}</p></td>
                  </tr>`;
      });
      templateHtml = templateHtml.replace("{{filas}}", tabla_beneficiarios);
      totales = `<tr>
                    <td width="56" valign="top"><p align="center"></p></td>
                    <td width="132" valign="top"><p align="center"></p></td>
                    <td width="132" valign="top"><p align="right"><b>${suma}</b></p></td>
                    <td width="132" valign="top"><p align="right"><b></b></p></td>
                    <td width="123" valign="top"><p align="right"><b></b></p></td>
                    <td width="113" valign="top"><p align="right"></p></td>
                  </tr>`;
      templateHtml = templateHtml.replace("{{totales}}", totales);
      pagesHtml += templateHtml;

      pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
        if (err) {
          console.log(`ERROR, generando pdf : ${err}`);
          response.status = "ERROR";
          response.message = JSON.stringify(err);
          resolve(response);
        } else {
          resolve(response);
        }
      });
    } catch (e) {
      console.log(e);
    }
  });
};

module.exports.generarCertificadoEcoaguinaldo = generarCertificadoEcoaguinaldo;
module.exports.generarCertificadoEcoriesgo = generarCertificadoEcoriesgo;
module.exports.generarOrdenCobroEcoaguinaldo = generarOrdenCobroEcoaguinaldo;
module.exports.generarOrdenCobroEcoriesgo = generarOrdenCobroEcoriesgo;
module.exports.generarReporteSolicitud = generarReporteSolicitud;
module.exports.generarReporteSolicitudEcoRiesgo = generarReporteSolicitudEcoRiesgo;
module.exports.generarReporteSolicitudTarjetas =   generarReporteSolicitudTarjetas;
module.exports.generarReporteCertificadoTarjetas = generarReporteCertificadoTarjetas;
module.exports.GenerarReporteCertificadoEcoMedic = GenerarReporteCertificadoEcoMedic;
module.exports.GenerarReporteSolicitudEcoMedic = GenerarReporteSolicitudEcoMedic;
module.exports.generarReporteCertificadoEcoVida = generarReporteCertificadoEcoVida;
module.exports.generarReporteCertificadoEcoAccidentes = generarReporteCertificadoEcoAccidentes;
module.exports.generarReporteCertificadoEcoResguardo = generarReporteCertificadoEcoResguardo;
module.exports.generarReporteCertificadoEcoProteccion = generarReporteCertificadoEcoProteccion;
module.exports.generarOrdenCobroEcoVida = generarOrdenCobroEcoVida;
module.exports.generarOrdenCobroEcoAccidentes = generarOrdenCobroEcoAccidentes;
module.exports.generarOrdenCobroEcoResguardo = generarOrdenCobroEcoResguardo;
module.exports.generarReporteSolicitudEcoVida = generarReporteSolicitudEcoVida;
module.exports.generarReporteSolicitudEcoAccidentes = generarReporteSolicitudEcoAccidentes;
module.exports.generarReporteSolicitudEcoResguardo = generarReporteSolicitudEcoResguardo;
module.exports.generarReporteSolicitudEcoProteccion = generarReporteSolicitudEcoProteccion;
module.exports.generarReporteCertificadoEcoMedic2 = generarReporteCertificadoEcoMedic2;
module.exports.generarOrdenCobroEcoMedic = generarOrdenCobroEcoMedic;
module.exports.generarReportePlanPago = generarReportePlanPago;


function getfunctionByDocumentoVersionPArametro(documentoVersionParametro) {

}

module.exports.generarArchivo = (datos, nombre_archivo, documentoVersion, reporte_query_parametros,poliza = null) => {
    datos = funciones.QuitarCamposNullos(datos);
    let polizaDescripcion = poliza ? poliza.descripcion : '';
    let usuarioLogins = datos.map(param => param.usuario_login || param.Sol_CodOficial);
    let usuarioNombres = datos.map(param => param.usuario_nombre_completo || param.Sol_Oficial);
    const options = {
      height: documentoVersion.height ? documentoVersion.height : "279.4mm",
      width: documentoVersion.width ? documentoVersion.width : "215.9mm",
      border: {
        top: documentoVersion.top ? documentoVersion.top : "0.5cm", // default is 0, units: mm, cm, in, px
        right: documentoVersion.right ? documentoVersion.right : "0.84cm",
        bottom: documentoVersion.bottom ? documentoVersion.bottom : "0.5cm",
        left: documentoVersion.left ? documentoVersion.left : "1cm",
      },
      header: {
        height: "0.2cm",
        contents: {
          //first: 'Página 1/2',
          //second: 'Página 2/2',
          //2: 'Second page', // Any page number is working. 1-based index
          default: `<div style="color: #444; font-size: 7px; text-align: right;">Páginas {{page}}/{{pages}}</div>`
          // fallback value
          //last: 'Last Page'
        }
      },
      footer: {
        height: "0.5cm",
        contents: {
          default: `
            <table>
              <tr>
                <td style="color: #444; font-size: 7px; text-align: left">Usuario: ${usuarioLogins[0]}</td>
                <td style="color: #444; font-size: 7px; text-align: rigth">Fecha: ${moment(new Date()).format('DD/MM/YYYY')}</td>
                ${polizaDescripcion ? '<td style="color: #444; font-size: 7px; text-align: rigth">Seguro:' + polizaDescripcion + '</td>' : ''}
              </tr>
            </table>`,
        },
      },
    };

    let response = { status: "OK", message: "genera-pdf", data: {} };
    return new Promise(async (resolve, reject) => {
      try {
        let pipe,param;
        let filename = path.join(__dirname, `../public/pdf/${nombre_archivo}.pdf`);
        let pagesHtml = "";
        let dt = new Date();
        let mm = dt.getMonth() + 1;
        let dd = dt.getDate();
        let yyyy = dt.getFullYear();
        let fecha = dd + "/" + mm + "/" + yyyy;
        let templateHtml = documentoVersion.html;
        for (let i = 0; i < documentoVersion.documento_version_params.length; i++) {
          let value = '',newValue = '';
          let documentoVersionParametro = documentoVersion.documento_version_params[i];
          if(documentoVersionParametro.nombre == '{{fecha_emision}}') {
            // console.log(true);
          }
          if(documentoVersionParametro.nombre == '{{contado}}') {
            console.log(true);
          }
          let templateHtmlParametro = documentoVersionParametro.html;
          let datoSelectValueNew = '', datoSelectValueNewInt = 0, datoSelectValueNewDate = null;
          if (documentoVersionParametro.nombre) {

            let docParamHijo, datoSelectValue,  htmlHijo, aQueryParamData;
            if (documentoVersionParametro.doc_param_hijos && documentoVersionParametro.doc_param_hijos.length) {

              let docParamHtml;
              if (documentoVersionParametro.html) {
                if (documentoVersionParametro.id_reporte_query) {
                  aQueryParamData = await reporteQueryService.EjecutarQuery(documentoVersionParametro.id_reporte_query, reporte_query_parametros);
                  if (aQueryParamData && aQueryParamData.length) {
                    for (let l = 0; l < aQueryParamData.length ; l++) {
                      if(documentoVersionParametro.nombre == '{{persona_fecha_nacimiento}}') {
                        console.log(true);
                      }
                      let queryParamData = aQueryParamData[l];
                      docParamHtml = documentoVersionParametro.html;
                      for (let k = 0; k < documentoVersionParametro.doc_param_hijos.length; k++) {
                        docParamHijo = documentoVersionParametro.doc_param_hijos[k];
                        datoSelectValue = docParamHijo && docParamHijo.select ? queryParamData[docParamHijo.select] ? queryParamData[docParamHijo.select] : '' : '';
                        datoSelectValue = typeof datoSelectValue == 'string' ? datoSelectValue.trim() : datoSelectValue;
                        [pipe,param] = docParamHijo.pipe ? docParamHijo.pipe.split('|') : [null,null];
                        switch (pipe) {
                          case 'conversor-numero-a-letras-es-ar':
                            newValue = datoSelectValue ? miConversor.convertToText(datoSelectValue) : datoSelectValue;
                            break;
                          case 'ofuscar-numero':
                            newValue = datoSelectValue ? datoSelectValue.substring(0,6)+'XXXXXX'+datoSelectValue.substring(datoSelectValue.length-5,datoSelectValue.length-1) : datoSelectValue;
                            break;
                          case 'decimal':
                            if (datoSelectValue) {
                              newValue = (Number(datoSelectValue)).toLocaleString(
                                  undefined, // leave undefined to use the visitor's browser
                                  // locale or a string like 'en-US' to override it.
                                  { minimumFractionDigits: 2 }
                              );
                            } else {
                              newValue = datoSelectValue;
                            }
                            break;
                          case 'file':
                            newValue = path.join("file://",__dirname, datoSelectValue);
                            break;
                          case 'date':
                            if (typeof datoSelectValue == 'string') {
                              if (datoSelectValue.indexOf('/') >= 0) {
                                newValue = moment(datoSelectValue,"MM/DD/YYYY").format(param ? param : "DD/MM/YYYY");
                              }  else {
                                newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                              }
                            }  else {
                              newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                            }
                            break;
                          case 'comparar':
                            if(datoSelectValue.substring(0,docParamHijo.select_value.length) == docParamHijo.select_value) {
                              datoSelectValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre,datoSelectValue)
                            } else {
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre,' ')
                            }
                            break;
                          case 'include':
                            if(datoSelectValue.toLowerCase().includes(docParamHijo.select_value.toLowerCase())) {
                              datoSelectValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre,datoSelectValue)
                            } else {
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre,' ')
                            }
                            break;
                          case '==':
                            if(datoSelectValue == docParamHijo.select_value) {
                              datoSelectValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre,datoSelectValue)
                            } else {
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre,' ')
                            }
                            break;
                          case '!=':
                            if(datoSelectValue != docParamHijo.select_value) {
                              datoSelectValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre,datoSelectValue)
                            } else {
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre,' ')
                            }
                            break;
                          case 'sumar':
                            datoSelectValueNewInt += parseInt(datoSelectValue+'');
                            if(datoSelectValueNewInt) {
                              datoSelectValue = datoSelectValueNewInt;
                            }
                            break;
                          case 'menor':
                            if(parseInt(datoSelectValue+'') < parseInt(docParamHijo.select_value+'')) {
                              datoSelectValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre, datoSelectValue);
                            }
                            break;
                          case '<':
                            if(parseInt(datoSelectValue+'') < parseInt(docParamHijo.select_value+'')) {
                              datoSelectValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre, datoSelectValue);
                            }
                            break;
                          case '<=':
                            if(parseInt(datoSelectValue+'') <= parseInt(docParamHijo.select_value+'')) {
                              datoSelectValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre, datoSelectValue);
                            }
                            break;
                          case '>':
                            if(parseInt(datoSelectValue+'') > parseInt(docParamHijo.select_value+'')) {
                              datoSelectValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre, datoSelectValue);
                            }
                            break;
                          case '>=':
                            if(parseInt(datoSelectValue+'') >= parseInt(docParamHijo.select_value+'')) {
                              datoSelectValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre, datoSelectValue);
                            }
                            break;
                          case 'default':
                            datoSelectValueNew = !docParamHijo.select_value ? docParamHijo.value : '';
                            if(datoSelectValueNew) {
                              datoSelectValue = datoSelectValueNew;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre, datoSelectValue);
                            }
                            break;
                          default:
                            docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre, datoSelectValue+' ');
                            break;
                        }
                      }
                      newValue += docParamHtml
                    }
                    templateHtml = documentoVersionParametro.nombre ? templateHtml.replaceAll(documentoVersionParametro.nombre, newValue) : templateHtml;
                  }
                }
              } else {
                docParamHtml = templateHtml;
                for (let k = 0; k < documentoVersionParametro.doc_param_hijos.length; k++) {
                  docParamHijo = documentoVersionParametro.doc_param_hijos[k];
                  htmlHijo = docParamHijo.html ? docParamHijo.html : '';
                  if (docParamHijo.id_reporte_query) {
                    aQueryParamData = await reporteQueryService.EjecutarQuery(docParamHijo.id_reporte_query,reporte_query_parametros);
                    if (aQueryParamData && aQueryParamData.length) {
                      for (let l = 0; l < aQueryParamData.length ; l++) {
                        let queryParamData = aQueryParamData[l];
                        if(documentoVersionParametro.nombre == '{{persona_fecha_nacimiento}}') {
                          console.log(true);
                        }
                        datoSelectValue = docParamHijo && docParamHijo.select ? queryParamData[docParamHijo.select] ? queryParamData[docParamHijo.select] : '' : '';
                        datoSelectValue = typeof datoSelectValue == 'string' ? datoSelectValue.trim() : datoSelectValue;
                        [pipe,param] = documentoVersionParametro.pipe ? documentoVersionParametro.pipe.split('|') : [null,null];
                        if (documentoVersionParametro.pipe) {
                          switch (pipe) {
                            case 'sumar':
                              switch (docParamHijo.pipe) {
                                case 'date':
                                  if (datoSelectValue.indexOf('/') >= 0) {
                                    datoSelectValueNewDate = moment(datoSelectValue, "MM/DD/YYYY").format(param ? param : "DD/MM/YYYY");
                                  } else {
                                    datoSelectValueNewDate = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                                  }
                                  if (datoSelectValueNewDate) {
                                    newValue = datoSelectValueNewDate;
                                  }
                                  break;
                                case 'months':
                                  if (datoSelectValueNewDate) {
                                    if (typeof datoSelectValueNewDate.indexOf == 'function' && datoSelectValueNewDate.indexOf('/') >= 0) {
                                      datoSelectValueNewDate = moment(datoSelectValueNewDate, "DD/MM/YYYY");
                                    }
                                    if (datoSelectValueNewDate) {
                                      datoSelectValueNewDate = datoSelectValueNewDate.add('months', datoSelectValue);
                                      newValue = datoSelectValueNewDate.format(param ? param : "DD/MM/YYYY");
                                    }
                                  }
                                  break;
                                default:
                                  datoSelectValueNewInt += parseInt(datoSelectValue + '');
                                  if (datoSelectValueNewInt) {
                                    newValue = datoSelectValueNewInt;
                                  }
                                  break;
                              }
                          }
                        } else {
                          [pipe,param] = docParamHijo.pipe ? docParamHijo.pipe.split('|') : [null,null];
                          switch (pipe) {
                            case 'conversor-numero-a-letras-es-ar':
                              newValue = datoSelectValue ? miConversor.convertToText(datoSelectValue) : datoSelectValue;
                              break;
                            case 'ofuscar-numero':
                              newValue = datoSelectValue ? datoSelectValue.substring(0,6)+'XXXXXX'+datoSelectValue.substring(datoSelectValue.length-5,datoSelectValue.length-1) : datoSelectValue;
                              break;
                            case 'decimal':
                              if (datoSelectValue) {
                                newValue = (Number(datoSelectValue)).toLocaleString(
                                    undefined, // leave undefined to use the visitor's browser
                                    // locale or a string like 'en-US' to override it.
                                    { minimumFractionDigits: 2 }
                                );
                              } else {
                                newValue = datoSelectValue;
                              }
                              break;
                            case 'file':
                              newValue = path.join("file://",__dirname, datoSelectValue);
                              break;
                            case 'date':
                              if (typeof datoSelectValue == 'string') {
                                if (datoSelectValue.indexOf('/') >= 0) {
                                  newValue = moment(datoSelectValue,"MM/DD/YYYY").format(param ? param : "DD/MM/YYYY");
                                }  else {
                                  newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");;
                                }
                              } else {
                                newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");;
                              }
                              break;
                            case 'comparar':
                              if(datoSelectValue.substring(0,docParamHijo.select_value.length) == docParamHijo.select_value) {
                                datoSelectValue = docParamHijo.value;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              } else {
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre,' ')
                              }
                              break;
                            case 'include':
                              if(datoSelectValue.toLowerCase().includes(docParamHijo.select_value.toLowerCase())) {
                                datoSelectValue = docParamHijo.value;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              } else {
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre,' ')
                              }
                              break;
                            case '==':
                              if(datoSelectValue == docParamHijo.select_value) {
                                datoSelectValue = docParamHijo.value;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              } else {
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre,' ')
                              }
                              break;
                            case '!=':
                              if(datoSelectValue != docParamHijo.select_value) {
                                datoSelectValue = docParamHijo.value;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              } else {
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre,' ')
                              }
                              break;
                            case 'sumar':
                              datoSelectValueNewInt += parseInt(datoSelectValue+'');
                              if(datoSelectValueNewInt) {
                                newValue = datoSelectValueNewInt;
                              }
                              break;
                            case 'menor':
                              if(parseInt(datoSelectValue+'') < parseInt(docParamHijo.select_value+'')) {
                                datoSelectValue = docParamHijo.value;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              }
                              break;
                            case '<':
                              if(parseInt(datoSelectValue+'') < parseInt(docParamHijo.select_value+'')) {
                                datoSelectValue = docParamHijo.value;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              }
                              break;
                            case '>':
                              if(parseInt(datoSelectValue+'') > parseInt(docParamHijo.select_value+'')) {
                                datoSelectValue = docParamHijo.value;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              }
                              break;
                            case '<=':
                              if(parseInt(datoSelectValue+'') <= parseInt(docParamHijo.select_value+'')) {
                                datoSelectValue = docParamHijo.value;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              }
                              break;
                            case '>=':
                              if(parseInt(datoSelectValue+'') >= parseInt(docParamHijo.select_value+'')) {
                                datoSelectValue = docParamHijo.value;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              }
                              break;
                            case 'default':
                              datoSelectValueNew = !docParamHijo.select_value ? docParamHijo.value : '';
                              if(datoSelectValueNew) {
                                datoSelectValue = datoSelectValueNew;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue);
                              }
                              break;
                            default:
                              if(documentoVersionParametro.select) {
                                datoSelectValue = value = dato[documentoVersionParametro.select] ? dato[documentoVersionParametro.select] : '';
                                value = documentoVersionParametro.value ? documentoVersionParametro.value + datoSelectValue : datoSelectValue;
                                docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, datoSelectValue+' ');
                              }
                              break;
                          }
                        }
                      }
                      docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                    } else {

                    }
                  } else {
                    for (let j = 0; j < datos.length; j++) {
                      let dato = datos[j];
                      datoSelectValue = docParamHijo.select ? dato[docParamHijo.select] ? dato[docParamHijo.select] : '' : '';
                      datoSelectValue = typeof datoSelectValue == 'string' ? datoSelectValue.trim() : datoSelectValue;
                      if (documentoVersionParametro.pipe) {
                        [pipe,param] = documentoVersionParametro.pipe ? documentoVersionParametro.pipe.split('|') : [null,null];
                        switch (pipe) {
                          case 'sumar':
                            switch (docParamHijo.pipe) {
                              case 'date':
                                if (typeof datoSelectValue == 'string') {
                                  if (datoSelectValue.indexOf('/') >= 0) {
                                    datoSelectValueNewDate = moment(datoSelectValue, "MM/DD/YYYY").format(param ? param : "DD/MM/YYYY");
                                  } else {
                                    datoSelectValueNewDate = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                                  }
                                } else {
                                  datoSelectValueNewDate = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                                }
                                if (datoSelectValueNewDate) {
                                  newValue = datoSelectValueNewDate;
                                }
                                break;
                              case 'months':
                                if (datoSelectValueNewDate) {
                                  if (typeof datoSelectValueNewDate.indexOf == 'function' && datoSelectValueNewDate.indexOf('/') >= 0) {
                                    datoSelectValueNewDate = moment(datoSelectValueNewDate, "DD/MM/YYYY");
                                  }
                                  if (datoSelectValueNewDate) {
                                    datoSelectValueNewDate = datoSelectValueNewDate.add('months', datoSelectValue);
                                    newValue = datoSelectValueNewDate.format(param ? param : "DD/MM/YYYY");
                                  }
                                }
                                break;
                              default:
                                datoSelectValueNewInt += parseInt(datoSelectValue + '');
                                if (datoSelectValueNewInt) {
                                  newValue = datoSelectValueNewInt;
                                }
                                break;
                            }
                            break;
                          case 'decimal':
                            if (docParamHijo.pipe) {
                              switch (docParamHijo.pipe) {
                                case 'sumar':
                                  datoSelectValueNewInt += parseInt(datoSelectValue+'');
                                  if(datoSelectValueNewInt) {
                                    newValue = (Number(datoSelectValueNewInt)).toLocaleString(
                                        undefined, // leave undefined to use the visitor's browser
                                        // locale or a string like 'en-US' to override it.
                                        { minimumFractionDigits: 2 }
                                    );
                                  }
                                  break;
                              }
                            } else {
                              if (datoSelectValue) {
                                newValue = (Number(datoSelectValue)).toLocaleString(
                                    undefined, // leave undefined to use the visitor's browser
                                    // locale or a string like 'en-US' to override it.
                                    { minimumFractionDigits: 2 }
                                );
                              } else {
                                newValue = datoSelectValue;
                              }
                            }
                            break;
                        }
                      } else {
                        [pipe,param] = docParamHijo.pipe ? docParamHijo.pipe.split('|') : [null,null];
                        switch (pipe) {
                          case 'conversor-numero-a-letras-es-ar':
                            newValue = datoSelectValue ? miConversor.convertToText(datoSelectValue) : datoSelectValue;
                            break;
                          case 'ofuscar-numero':
                            newValue = datoSelectValue ? datoSelectValue.substring(0,6)+'XXXXXX'+datoSelectValue.substring(datoSelectValue.length-5,datoSelectValue.length-1) : datoSelectValue;
                            break;
                          case 'decimal':
                            if (datoSelectValue) {
                              newValue = (Number(datoSelectValue)).toLocaleString(
                                  undefined, // leave undefined to use the visitor's browser
                                  // locale or a string like 'en-US' to override it.
                                  { minimumFractionDigits: 2 }
                              );
                            } else {
                              newValue = datoSelectValue;
                            }
                            break;
                          case 'file':
                            newValue = path.join("file://",__dirname, datoSelectValue);
                            break;
                          case 'date':
                            if (typeof datoSelectValue == 'string') {
                              if (datoSelectValue.indexOf('/') >= 0) {
                                newValue = moment(datoSelectValue,"MM/DD/YYYY").format(param ? param : "DD/MM/YYYY");
                              }  else {
                                newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                              }
                            } else {
                              newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                            }
                            break;
                          case 'comparar':
                            if(datoSelectValue.substring(0,docParamHijo.select_value.length) == docParamHijo.select_value) {
                              newValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            } else {
                              //docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre,' ')
                            }
                            break;
                          case 'include':
                            if(datoSelectValue.toLowerCase().include(docParamHijo.select_value.toLowerCase())) {
                              newValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            } else {
                              //docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre,' ')
                            }
                            break;
                          case '==':
                            if(datoSelectValue == docParamHijo.select_value) {
                              newValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            } else {
                              //docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre,' ')
                            }
                            break;
                          case '!=':
                            if(datoSelectValue != docParamHijo.select_value) {
                              newValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            } else {
                              //docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre,' ')
                            }
                            break;
                          case 'sumar':
                            datoSelectValueNewInt += parseInt(datoSelectValue+'');
                            if(datoSelectValueNewInt) {
                              newValue = datoSelectValueNewInt;
                            }
                            break;
                          case 'menor':
                            if(parseInt(datoSelectValue+'') < parseInt(docParamHijo.select_value+'')) {
                              newValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            }
                            break;
                          case '<':
                            if(parseInt(datoSelectValue+'') < parseInt(docParamHijo.select_value+'')) {
                              newValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            }
                            break;
                          case '>':
                            if(parseInt(datoSelectValue+'') > parseInt(docParamHijo.select_value+'')) {
                              newValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            }
                            break;
                          case '<=':
                            if(parseInt(datoSelectValue+'') <= parseInt(docParamHijo.select_value+'')) {
                              newValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            }
                            break;
                          case '>=':
                            if(parseInt(datoSelectValue+'') >= parseInt(docParamHijo.select_value+'')) {
                              newValue = docParamHijo.value;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            }
                            break;
                          case 'default':
                            datoSelectValueNew = !docParamHijo.select_value ? docParamHijo.value : '';
                            if(datoSelectValueNew) {
                              newValue = datoSelectValueNew;
                              docParamHtml = docParamHtml.replaceAll(documentoVersionParametro.nombre, newValue);
                            }
                            break;
                          default:
                            if(docParamHijo.select) {
                              datoSelectValue = value = dato[docParamHijo.select] ? dato[docParamHijo.select] : '';
                              value = docParamHijo.value ? docParamHijo.value + datoSelectValue : datoSelectValue;
                              docParamHtml = docParamHtml.replaceAll(docParamHijo.nombre, datoSelectValue+' ');
                            }
                            break;
                        }
                      }
                    }
                  }
                }
                templateHtml = docParamHtml;
              }
              templateHtml = documentoVersionParametro.nombre ? templateHtml.replaceAll(documentoVersionParametro.nombre, newValue) : templateHtml;
            } else if (documentoVersionParametro.value && !documentoVersionParametro.select) {
              newValue = documentoVersionParametro.value;
              [pipe,param] = documentoVersionParametro.pipe ? documentoVersionParametro.pipe.split('|') : [null,null];
              switch (pipe) {
                case 'conversor-numero-a-letras-es-ar':
                  newValue = miConversor.convertToText(documentoVersionParametro.value);
                  break;
                case 'ofuscar-numero':
                  newValue = documentoVersionParametro.value.substring(0,6)+'XXXXXX'+documentoVersionParametro.value.substring(documentoVersionParametro.value.length-5,documentoVersionParametro.value.length-1)
                  break;
                case 'decimal':
                  if (documentoVersionParametro.value) {
                    newValue = (Number(documentoVersionParametro.value)).toLocaleString(
                        undefined, // leave undefined to use the visitor's browser
                        // locale or a string like 'en-US' to override it.
                        { minimumFractionDigits: 2 }
                    );
                  } else {
                    newValue = documentoVersionParametro.value;
                  }
                  break;
                case 'file':
                  newValue = path.join("file://",__dirname, documentoVersionParametro.value);
                  break;
                case 'date':
                  if (typeof datoSelectValue == 'string') {
                    if (documentoVersionParametro.value.indexOf('/') >= 0) {
                      newValue = moment(documentoVersionParametro.value,"MM/DD/YYYY").format(param ? param : "DD/MM/YYYY");
                    }  else {
                      newValue = moment(documentoVersionParametro.value).format(param ? param : "DD/MM/YYYY");
                    }
                  } else {
                    newValue = moment(documentoVersionParametro.value).format(param ? param : "DD/MM/YYYY");
                  }
                  break;
                case 'default':
                  newValue = documentoVersionParametro.value;
                  break;
                default:
                  newValue = documentoVersionParametro.value;
                  break;

              }
              templateHtml = documentoVersionParametro.nombre ? templateHtml.replaceAll(documentoVersionParametro.nombre, newValue) : templateHtml;
            } else if(documentoVersionParametro.select && documentoVersionParametro.id_reporte_query && !documentoVersionParametro.html ) {
              let aDataParamQuery = await reporteQueryService.EjecutarQuery(documentoVersionParametro.id_reporte_query,reporte_query_parametros);
              let space = '', replaced = '';
              if (aDataParamQuery && aDataParamQuery.length) {
                for (let j = 0; j < aDataParamQuery.length; j++) {
                  let dataParamQuery = aDataParamQuery[j];
                  if(documentoVersionParametro.nombre == '{{persona_fecha_nacimiento}}') {
                    console.log(true);
                  }
                  for (let property in dataParamQuery) {
                    if (templateHtml.indexOf(`{{${property}}}`) >= 0) {
                      if (dataParamQuery[property]) {
                        templateHtml = templateHtml.replaceAll(`{{${property}}}`, dataParamQuery[property]+' ');
                      } else {
                        templateHtml = templateHtml.replaceAll(`{{${property}}}`, '');
                      }
                    }
                  }
                }
              }
            } else if(documentoVersionParametro.select && !documentoVersionParametro.html ) {
              let aKeyFoundOnDocVersionParamValues = documentoVersionParametro.value ? documentoVersion.documento_version_params.filter(param => documentoVersionParametro.value.includes(param.nombre)) : [];
              for (let j = 0; j < datos.length; j++) {
                let dato = datos[j];
                datoSelectValue = documentoVersionParametro.select ? dato[documentoVersionParametro.select] ? dato[documentoVersionParametro.select] : '' : '';
                datoSelectValue = typeof datoSelectValue == 'string' ? datoSelectValue.trim() : datoSelectValue;
                [pipe,param] = documentoVersionParametro.pipe ? documentoVersionParametro.pipe.split('|') : [null,null];
                for (let k = 0; k < aKeyFoundOnDocVersionParamValues.length; k++) {
                  let keyFoundOnDocVersionParamValue = aKeyFoundOnDocVersionParamValues[k];
                  if (dato[keyFoundOnDocVersionParamValue.select]) {
                    documentoVersionParametro.value = documentoVersionParametro.value.replaceAll(keyFoundOnDocVersionParamValue.nombre,dato[keyFoundOnDocVersionParamValue.select])
                  }
                }
                switch (pipe) {
                  case 'conversor-numero-a-letras-es-ar':
                    newValue = datoSelectValue ? miConversor.convertToText(datoSelectValue) : datoSelectValue;
                    break;
                  case 'ofuscar-numero':
                    newValue = datoSelectValue ? datoSelectValue.substring(0,6)+'XXXXXX'+datoSelectValue.substring(datoSelectValue.length-5,datoSelectValue.length-1) : datoSelectValue;
                    break;
                  case 'decimal':
                    if (datoSelectValue) {
                      newValue = (Number(datoSelectValue)).toLocaleString(
                          undefined, // leave undefined to use the visitor's browser
                          // locale or a string like 'en-US' to override it.
                          { minimumFractionDigits: 2 }
                      );
                    } else {
                      newValue = datoSelectValue;
                    }
                    break;
                  case 'file':
                    newValue = path.join("file://",__dirname, datoSelectValue);
                    break;
                  case 'date':
                    if (typeof datoSelectValue == 'string') {
                      if (datoSelectValue.indexOf('/') >= 0) {
                        newValue = moment(datoSelectValue,"MM/DD/YYYY").format(param ? param : "DD/MM/YYYY");
                      }  else {
                        newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                      }
                    } else {
                      newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                    }
                    break;
                  case 'comparar':
                    if(datoSelectValue.substring(0,documentoVersionParametro.select_value.length) == documentoVersionParametro.select_value) {
                        newValue = documentoVersionParametro.value ? dato[documentoVersionParametro.value] ? dato[documentoVersionParametro.value] : documentoVersionParametro.value : datoSelectValue;
                    }
                    break;
                  case 'include':
                    if(datoSelectValue.toLowerCase().includes(documentoVersionParametro.select_value.toLowerCase())) {
                        newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                    }
                    break;
                  case '==':
                    if(datoSelectValue == documentoVersionParametro.select_value) {
                        newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                    }
                    break;
                  case '!=':
                    if(datoSelectValue != documentoVersionParametro.select_value) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                    }
                    break;
                  case 'sumar':
                    datoSelectValueNewInt += parseInt(datoSelectValue+'');
                    if(datoSelectValueNewInt) {
                      newValue = datoSelectValueNewInt;
                    }
                    break;
                  case 'menor':
                    if(parseInt(datoSelectValueNewInt+'') < parseInt(documentoVersionParametro.select_value+'')) {
                        newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                    }
                    break;
                  case '<':
                    if(parseInt(datoSelectValueNewInt+'') < parseInt(documentoVersionParametro.select_value+'')) {
                        newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                    }
                    break;
                  case '>':
                    if(parseInt(datoSelectValueNewInt+'') > parseInt(documentoVersionParametro.select_value+'')) {
                        newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                    }
                    break;
                  case '>=':
                    if(parseInt(datoSelectValueNewInt+'') >= parseInt(documentoVersionParametro.select_value+'')) {
                        newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                    }
                    break;
                  case '<=':
                    if(parseInt(datoSelectValueNewInt+'') <= parseInt(documentoVersionParametro.select_value+'')) {
                        newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                    }
                    break;
                  case 'default':
                    datoSelectValue = !documentoVersionParametro.select_value ? documentoVersionParametro.value : '';
                    if(datoSelectValue) {
                      newValue = datoSelectValue;
                    }
                    break;
                  default:
                    newValue = datoSelectValue ? datoSelectValue : documentoVersionParametro.value ? documentoVersionParametro.value : '';
                    break;
                }
                templateHtml = documentoVersionParametro.nombre ? templateHtml.replaceAll(documentoVersionParametro.nombre, newValue+' ') : templateHtml;
              }
            } else if(documentoVersionParametro.html && documentoVersionParametro.id_reporte_query && !documentoVersionParametro.select) {
              let aDataParamQuery = await reporteQueryService.EjecutarQuery(documentoVersionParametro.id_reporte_query,reporte_query_parametros);
              let html = documentoVersionParametro.html;
              let space = '', replaced = '';
              if (aDataParamQuery && aDataParamQuery.length) {
                for (let j = 0; j < aDataParamQuery.length; j++) {
                  let dataParamQuery = aDataParamQuery[j];
                  if(documentoVersionParametro.nombre == '{{persona_fecha_nacimiento}}') {
                    console.log(true);
                  }
                  replaced = html;
                  for (let property in dataParamQuery) {
                    if (html.indexOf(`{{${property}}}`) >= 0) {
                      if (dataParamQuery[property]) {
                        replaced = replaced.replaceAll(`{{${property}}}`, dataParamQuery[property]+' ');
                      } else {
                        replaced = replaced.replaceAll(`{{${property}}}`, '');
                      }
                    }
                  }
                  space += replaced;
                }
              }
              value = space;
              templateHtml = documentoVersionParametro.nombre ? templateHtml.replaceAll(documentoVersionParametro.nombre, space) : templateHtml;
            } else if (documentoVersionParametro.value && documentoVersionParametro.pipe) {
              [pipe,param] = documentoVersionParametro.pipe ? documentoVersionParametro.pipe.split('|') : [null,null];
              switch (pipe) {
                case 'conversor-numero-a-letras-es-ar':
                  newValue = datoSelectValue ? miConversor.convertToText(datoSelectValue) : datoSelectValue;
                  break;
                case 'ofuscar-numero':
                  newValue = datoSelectValue ? datoSelectValue.substring(0,6)+'XXXXXX'+datoSelectValue.substring(datoSelectValue.length-5,datoSelectValue.length-1) : datoSelectValue;
                  break;
                case 'decimal':
                  if (datoSelectValue) {
                    newValue = (Number(datoSelectValue)).toLocaleString(
                        undefined, // leave undefined to use the visitor's browser
                        // locale or a string like 'en-US' to override it.
                        { minimumFractionDigits: 2 }
                    );
                  } else {
                    newValue = datoSelectValue;
                  }
                  break;
                case 'file':
                  newValue = path.join("file://",__dirname, datoSelectValue);
                  break;
                case 'date':
                  if (typeof datoSelectValue == 'string') {
                    if (datoSelectValue.indexOf('/') >= 0) {
                      newValue = moment(datoSelectValue,"MM/DD/YYYY").format(param ? param : "DD/MM/YYYY");
                    }  else {
                      newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                    }
                  } else {
                    newValue = moment(datoSelectValue).format(param ? param : "DD/MM/YYYY");
                  }
                  break;
                case 'comparar':
                  if(datoSelectValue.substring(0,documentoVersionParametro.select_value.length) == documentoVersionParametro.select_value) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                  }
                  break;
                case 'include':
                  if(datoSelectValue.toLowerCase().includes(documentoVersionParametro.select_value.toLowerCase())) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                  }
                  break;
                case '==':
                  if(datoSelectValue == documentoVersionParametro.select_value) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                  }
                  break;
                case '!=':
                  if(datoSelectValue != documentoVersionParametro.select_value) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                  }
                  break;
                case 'sumar':
                  datoSelectValueNewInt += parseInt(datoSelectValue+'');
                  if(datoSelectValueNewInt) {
                    newValue = datoSelectValueNewInt;
                  }
                  break;
                case 'menor':
                  if(parseInt(datoSelectValueNewInt+'') < parseInt(documentoVersionParametro.select_value+'')) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                  }
                  break;
                case '<':
                  if(parseInt(datoSelectValueNewInt+'') < parseInt(documentoVersionParametro.select_value+'')) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                  }
                  break;
                case '>':
                  if(parseInt(datoSelectValueNewInt+'') > parseInt(documentoVersionParametro.select_value+'')) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                  }
                  break;
                case '>=':
                  if(parseInt(datoSelectValueNewInt+'') >= parseInt(documentoVersionParametro.select_value+'')) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                  }
                  break;
                case '<=':
                  if(parseInt(datoSelectValueNewInt+'') <= parseInt(documentoVersionParametro.select_value+'')) {
                      newValue = documentoVersionParametro.value ? documentoVersionParametro.value : datoSelectValue;
                  }
                  break;
                case 'default':
                  datoSelectValue = !documentoVersionParametro.select_value ? documentoVersionParametro.value : '';
                  if(datoSelectValue) {
                    newValue = datoSelectValue;
                  }
                  break;
                default:
                  if(datoSelectValue) {
                    newValue = documentoVersionParametro.value ? documentoVersionParametro.value + datoSelectValue : datoSelectValue;
                  }
                  break;
              }
              templateHtml = documentoVersionParametro.nombre ? templateHtml.replaceAll(documentoVersionParametro.nombre, newValue+' ') : templateHtml;
            } else if (documentoVersionParametro.select){

            }
          } else if(documentoVersionParametro.html && documentoVersionParametro.id_reporte_query && documentoVersionParametro.select) {
              let aDataParamQuery = await reporteQueryService.EjecutarQuery(documentoVersionParametro.id_reporte_query,reporte_query_parametros);
              let html = documentoVersionParametro.html;
              let seccionIndex = html ? html.substring(0,3) == '<<<' && html.substring(html.length-3,html.length) == '>>>' ? html.split('<<<')[1].split('>>>')[0] : '' : '';
              let space = '', replaced = '', beginIndex,endIndex;
              let newTemplateHtml, aSeccionIndex;
              if (seccionIndex) {
                if(seccionIndex.indexOf('|') >= 0) {
                  aSeccionIndex = seccionIndex.split('|');
                  for (let j = 0; j < aSeccionIndex.length; j++) {
                    let seccionIndex = aSeccionIndex[j];
                    beginIndex = '<<<' + seccionIndex + '>>>';
                    endIndex = '>>>' + seccionIndex + '<<<';
                    let aDocParamSelectValues = documentoVersionParametro.select_value ? documentoVersionParametro.select_value.split('|') : documentoVersionParametro.select_value;
                    if (aDataParamQuery && aDataParamQuery.length) {
                      // let dataParamQueryValues = aDataParamQuery.map(param => param[documentoVersionParametro.select]);
                      switch (documentoVersionParametro.pipe) {
                        case 'include':
                          for (let k = 0; k < datos.length; k++) {
                            let dato = datos[k];
                            let datoSelectValue = documentoVersionParametro.select ? dato[documentoVersionParametro.select] ? dato[documentoVersionParametro.select] : '' : '';
                            let aDataParamQueryFiltered = aDataParamQuery.filter(param => param.diccionario_codigo.toLowerCase().replaceAll('_','').includes(seccionIndex));
                            let dataParamQuerySelectValue = aDataParamQueryFiltered.find(param => datoSelectValue.includes(param[documentoVersionParametro.select]));
                            let selectValues = documentoVersionParametro.select_value ? documentoVersionParametro.select_value.split('|') : documentoVersionParametro.select_value;
                            let displace = false;
                            if (datoSelectValue) {
                              if (aDocParamSelectValues) {
                                if(aDocParamSelectValues.find(param => dataSelectValue.toLowerCase().includes(param.toLowerCase()))) {
                                  templateHtml = templateHtml.replaceAll(beginIndex,'').replaceAll(endIndex,'');
                                  displace = false;
                                  break;
                                } else {
                                  displace = true;
                                }
                              } else if (dataParamQuerySelectValue){
                                templateHtml = templateHtml.replaceAll(beginIndex,'').replaceAll(endIndex,'');
                                displace = false;
                              } else {
                                displace = true;
                              }
                            }
                            newTemplateHtml = '';
                            if (displace) {
                              if (templateHtml.indexOf(beginIndex) >= 0 && templateHtml.indexOf(endIndex) >= 0) {
                                newTemplateHtml += templateHtml.split(beginIndex)[0];
                                newTemplateHtml += templateHtml.split(endIndex)[1];
                                templateHtml = newTemplateHtml;
                              }
                            }
                          }
                          break;
                        case 'comparar':
                          for (let k = 0; k < datos.length; k++) {
                            let dato = datos[k];
                            let datoSelectValue = documentoVersionParametro.select ? dato[documentoVersionParametro.select] ? dato[documentoVersionParametro.select] : '' : '';
                            let dataSelectValue = datoSelectValue ? datoSelectValue : documentoVersionParametro.select ? dataParamQuery[documentoVersionParametro.select] ? dataParamQuery[documentoVersionParametro.select] : '' : '';
                            let displace = false;
                            if (dataSelectValue) {
                              if(aDocParamSelectValues.find(param => datoSelectValue.toLowerCase().substring(0,param.length) == param.toLowerCase())) {
                                templateHtml = templateHtml.replaceAll(beginIndex,'').replaceAll(endIndex,'');
                                displace = false;
                                break;
                              } else {
                                displace = true;
                              }
                            }
                            newTemplateHtml = '';
                            if (displace) {
                              if (templateHtml.indexOf(beginIndex) >= 0 && templateHtml.indexOf(endIndex) >= 0) {
                                newTemplateHtml += templateHtml.split(beginIndex)[0];
                                newTemplateHtml += templateHtml.split(endIndex)[1];
                                templateHtml = newTemplateHtml;
                              }
                            }
                          }
                          break;
                      }
                    } else {
                      if (templateHtml.indexOf(beginIndex) >= 0 && templateHtml.indexOf(endIndex) >= 0) {
                        newTemplateHtml = '';
                        newTemplateHtml += templateHtml.split(beginIndex)[0];
                        newTemplateHtml += templateHtml.split(endIndex)[1];
                        templateHtml = newTemplateHtml;
                      }
                    }
                  }
                }
              } else {
                if (data) {
                  replaced = html;
                  for (let property in data) {
                    if (html.indexOf(`{{${property}}}`) >= 0) {
                      if (data[property]) {
                        replaced = replaced.replaceAll(`{{${property}}}`, data[property]+' ');
                      } else {
                        replaced = replaced.replaceAll(`{{${property}}}`, '');
                      }
                    }
                  }
                  space += replaced;
                }
              }
              value = space;
              templateHtml = documentoVersionParametro.nombre ? templateHtml.replaceAll(documentoVersionParametro.nombre, space) : templateHtml;
          } else {
            let html = documentoVersionParametro.html;
            let seccionIndex = html ? html.substring(0,3) == '<<<' && html.substring(html.length-3,html.length) == '>>>' ? html.split('<<<')[1].split('>>>')[0] : '' : '';
            let space = '', replaced = '', beginIndex,endIndex;
            let newTemplateHtml,aSeccionIndex;
            if (seccionIndex) {
              if (seccionIndex.indexOf('|') >= 0) {
                aSeccionIndex = seccionIndex.split('|');
              } else {
                aSeccionIndex = [seccionIndex];
              }
              for (let j = 0; j < aSeccionIndex.length; j++) {
                let seccionIndex = aSeccionIndex[j];
                beginIndex = '<<<' + seccionIndex + '>>>';
                endIndex = '>>>' + seccionIndex + '<<<';
                let aDocParamSelectValues = documentoVersionParametro.select_value ? documentoVersionParametro.select_value.split('|') : documentoVersionParametro.select_value;
                if (datos) {
                  switch (documentoVersionParametro.pipe) {
                    case 'include':
                      for (let j = 0; j < datos.length; j++) {
                        let dato = datos[j];
                        let datoSelectValue = documentoVersionParametro.select ? dato[documentoVersionParametro.select] ? dato[documentoVersionParametro.select] : '' : '';
                        let dataParamQuerySelectValue = documentoVersionParametro.select_value ? documentoVersionParametro.select_value.split('|') : documentoVersionParametro.select_value;
                        let displace = false;
                        if (datoSelectValue) {
                          if (aDocParamSelectValues) {
                            if(aDocParamSelectValues.find(param => datoSelectValue.toLowerCase().includes(param.toLowerCase()))) {
                              templateHtml = templateHtml.replaceAll(beginIndex,'').replaceAll(endIndex,'');
                              displace = false;
                              break;
                            } else {
                              displace = true;
                            }
                          } else if (dataParamQuerySelectValue){
                            templateHtml = templateHtml.replaceAll(beginIndex,'').replaceAll(endIndex,'');
                            displace = false;
                          } else {
                            displace = true;
                          }
                        }
                        newTemplateHtml = '';
                        if (displace) {
                          if (templateHtml.indexOf(beginIndex) >= 0 && templateHtml.indexOf(endIndex) >= 0) {
                            newTemplateHtml += templateHtml.split(beginIndex)[0];
                            newTemplateHtml += templateHtml.split(endIndex)[1];
                            templateHtml = newTemplateHtml;
                          }
                        }
                      }
                      break;
                    case 'comparar':
                      for (let j = 0; j < datos.length; j++) {
                        let dato = datos[j];
                        let datoSelectValue = documentoVersionParametro.select ? dato[documentoVersionParametro.select] ? dato[documentoVersionParametro.select] : '' : '';
                        let displace = false;
                        if (datoSelectValue) {
                          if(aDocParamSelectValues.find(param => datoSelectValue.toLowerCase().substring(0,param.length) == param.toLowerCase())) {
                            templateHtml = templateHtml.replaceAll(beginIndex,'').replaceAll(endIndex,'');
                            displace = false;
                            break;
                          } else {
                            displace = true;
                          }
                        } else {
                          displace = true;
                        }
                        newTemplateHtml = '';
                        if (displace) {
                          if (templateHtml.indexOf(beginIndex) >= 0 && templateHtml.indexOf(endIndex) >= 0) {
                            newTemplateHtml += templateHtml.split(beginIndex)[0];
                            newTemplateHtml += templateHtml.split(endIndex)[1];
                            templateHtml = newTemplateHtml;
                          }
                        }
                      }
                      break;
                  }
                } else {
                  if (templateHtml.indexOf(beginIndex) >= 0 && templateHtml.indexOf(endIndex) >= 0) {
                    newTemplateHtml = '';
                    newTemplateHtml += templateHtml.split(beginIndex)[0];
                    newTemplateHtml += templateHtml.split(endIndex)[1];
                    templateHtml = newTemplateHtml;
                  }
                }
              }
            }
          }
        }

        pagesHtml += templateHtml;

        pdf.create(pagesHtml, options).toFile(filename, (err, res) => {
          if (err) {
            console.log(`ERROR, generando pdf : ${err}`);
            response.status = "ERROR";
            response.message = JSON.stringify(err);
            resolve(response);
          } else {
            response.data = path.parse(filename);
            response.data.filename = filename;
            resolve(response);
          }
        });
      } catch (e) {
        console.log(e)
      }
    });
};
