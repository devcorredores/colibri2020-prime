const { text } = require("body-parser");
const modelos = require("../modelos");
const funciones = require('../modulos/funciones');
const {loggerPlanPago} = require("../modulos/winston");


GenerarPlanPagos = async (plan_pago) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const v_pp = plan_pago;
    let date = new Date();
    // const id_solicitud = idSolicitud;

    let v_pp_g = {};
    let datos;
    //OBTENER DATOS SOLICITUD

    let alreadyPlanPago = await modelos.planPago.findOne({where:{id_instancia_poliza:v_pp.id_instancia_poliza}});
    if (!alreadyPlanPago) {
        await modelos.planPago.create(v_pp)
        .then(async resp => {
            if (resp) {
                loggerPlanPago.info(`Plan Pago Creado, id_instancia_poliza: ${v_pp.id_instancia_poliza} ${date.toString()}: `, v_pp);
                if (!v_pp.total_prima) {
                    loggerPlanPago.info(`Error: Plan Pago con valor 0: ${v_pp.id_instancia_poliza} ${date.toString()}: `, v_pp);
                }
                datos = resp.dataValues;
                let v_ppd={};
                v_ppd.id_instancia_poliza=parseInt(v_pp.id_instancia_poliza);
                v_ppd.id_planpago=parseInt(datos.id);
                v_ppd.pago_cuota_prog=datos.total_prima/(datos.periodicidad_anual*datos.plazo_anos);
                v_ppd.adicionado_por=datos.adicionado_por;
                v_ppd.modificado_por=datos.modificado_por;
                for(var i=0;i<(datos.periodicidad_anual*datos.plazo_anos);i++){
                    v_ppd.nro_cuota_prog=(i+1);
                    let fecha=new Date(datos.fecha_inicio);
                    var ultimoDia = new Date(fecha.getFullYear(), fecha.getMonth() + 1, 0);
                    ultimoDia.setDate(ultimoDia.getDate()-2);
                    fecha = fecha.add(i).month();
                    v_ppd.fecha_couta_prog=fecha;
                    console.log(v_ppd.fecha_couta_prog);
                    v_ppd.interes_prog=(datos.total_prima-(v_ppd.pago_cuota_prog*i))*datos.interes;
                    v_ppd.prima_prog=(v_ppd.pago_cuota_prog + v_ppd.interes_prog);
                    v_ppd.pago_prima_acumulado=v_ppd.pago_cuota_prog*(i+1);
                    v_ppd.prima_pendiente=datos.total_prima-v_ppd.pago_prima_acumulado;
                    v_ppd.id_estado=253;
                    await modelos.plan_pago_detalle.create(v_ppd)
                    .then(resp2 => {
                        response.data = resp2;
                        response.message = 'Registro creado';
                    })
                    .catch((err) => {
                        response.status = 'ERROR';
                        response.message = err;
                    });
                }

            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    }
    return response;
};
module.exports.GenerarPlanPagos = GenerarPlanPagos;
