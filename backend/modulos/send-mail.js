
const nodemailer = require("nodemailer");

class SendMail {
  static sendMail(mailOptions) {
    let resp = "";

    const transporter = nodemailer.createTransport({
      host: "smtp.office365.com",
      port: 587,
      secure: false, // upgrade later with STARTTLS
      auth: {
        user: "colibri.garantias@corredoresecofuturo.com.bo",
        pass: "Fac60710"
      }
    });

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log("=====> ERROR_SEND_MAIL:".error);
        resp = `ERROR,${error}`;
      } else {
        console.log(`=====> Email sent: ${info.response}`);
        resp = `enviado,${JSON.stringify(info.response)}`;
      }
    });

    return resp;
  }
}

module.exports = SendMail;
