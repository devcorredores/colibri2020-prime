(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["seguros-seguros-module"],{

/***/ "../../node_modules/sweetalert2/dist/sweetalert2.all.js":
/*!**********************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/node_modules/sweetalert2/dist/sweetalert2.all.js ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*!
* sweetalert2 v10.16.9
* Released under the MIT License.
*/
(function (global, factory) {
   true ? module.exports = factory() :
  undefined;
}(this, function () { 'use strict';

  function _typeof(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _construct(Parent, args, Class) {
    if (_isNativeReflectConstruct()) {
      _construct = Reflect.construct;
    } else {
      _construct = function _construct(Parent, args, Class) {
        var a = [null];
        a.push.apply(a, args);
        var Constructor = Function.bind.apply(Parent, a);
        var instance = new Constructor();
        if (Class) _setPrototypeOf(instance, Class.prototype);
        return instance;
      };
    }

    return _construct.apply(null, arguments);
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _createSuper(Derived) {
    var hasNativeReflectConstruct = _isNativeReflectConstruct();

    return function _createSuperInternal() {
      var Super = _getPrototypeOf(Derived),
          result;

      if (hasNativeReflectConstruct) {
        var NewTarget = _getPrototypeOf(this).constructor;

        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }

      return _possibleConstructorReturn(this, result);
    };
  }

  function _superPropBase(object, property) {
    while (!Object.prototype.hasOwnProperty.call(object, property)) {
      object = _getPrototypeOf(object);
      if (object === null) break;
    }

    return object;
  }

  function _get(target, property, receiver) {
    if (typeof Reflect !== "undefined" && Reflect.get) {
      _get = Reflect.get;
    } else {
      _get = function _get(target, property, receiver) {
        var base = _superPropBase(target, property);

        if (!base) return;
        var desc = Object.getOwnPropertyDescriptor(base, property);

        if (desc.get) {
          return desc.get.call(receiver);
        }

        return desc.value;
      };
    }

    return _get(target, property, receiver || target);
  }

  var consolePrefix = 'SweetAlert2:';
  /**
   * Filter the unique values into a new array
   * @param arr
   */

  var uniqueArray = function uniqueArray(arr) {
    var result = [];

    for (var i = 0; i < arr.length; i++) {
      if (result.indexOf(arr[i]) === -1) {
        result.push(arr[i]);
      }
    }

    return result;
  };
  /**
   * Capitalize the first letter of a string
   * @param str
   */

  var capitalizeFirstLetter = function capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };
  /**
   * Returns the array of object values (Object.values isn't supported in IE11)
   * @param obj
   */

  var objectValues = function objectValues(obj) {
    return Object.keys(obj).map(function (key) {
      return obj[key];
    });
  };
  /**
   * Convert NodeList to Array
   * @param nodeList
   */

  var toArray = function toArray(nodeList) {
    return Array.prototype.slice.call(nodeList);
  };
  /**
   * Standardise console warnings
   * @param message
   */

  var warn = function warn(message) {
    console.warn("".concat(consolePrefix, " ").concat(_typeof(message) === 'object' ? message.join(' ') : message));
  };
  /**
   * Standardise console errors
   * @param message
   */

  var error = function error(message) {
    console.error("".concat(consolePrefix, " ").concat(message));
  };
  /**
   * Private global state for `warnOnce`
   * @type {Array}
   * @private
   */

  var previousWarnOnceMessages = [];
  /**
   * Show a console warning, but only if it hasn't already been shown
   * @param message
   */

  var warnOnce = function warnOnce(message) {
    if (!(previousWarnOnceMessages.indexOf(message) !== -1)) {
      previousWarnOnceMessages.push(message);
      warn(message);
    }
  };
  /**
   * Show a one-time console warning about deprecated params/methods
   */

  var warnAboutDeprecation = function warnAboutDeprecation(deprecatedParam, useInstead) {
    warnOnce("\"".concat(deprecatedParam, "\" is deprecated and will be removed in the next major release. Please use \"").concat(useInstead, "\" instead."));
  };
  /**
   * If `arg` is a function, call it (with no arguments or context) and return the result.
   * Otherwise, just pass the value through
   * @param arg
   */

  var callIfFunction = function callIfFunction(arg) {
    return typeof arg === 'function' ? arg() : arg;
  };
  var hasToPromiseFn = function hasToPromiseFn(arg) {
    return arg && typeof arg.toPromise === 'function';
  };
  var asPromise = function asPromise(arg) {
    return hasToPromiseFn(arg) ? arg.toPromise() : Promise.resolve(arg);
  };
  var isPromise = function isPromise(arg) {
    return arg && Promise.resolve(arg) === arg;
  };

  var DismissReason = Object.freeze({
    cancel: 'cancel',
    backdrop: 'backdrop',
    close: 'close',
    esc: 'esc',
    timer: 'timer'
  });

  var isJqueryElement = function isJqueryElement(elem) {
    return _typeof(elem) === 'object' && elem.jquery;
  };

  var isElement = function isElement(elem) {
    return elem instanceof Element || isJqueryElement(elem);
  };

  var argsToParams = function argsToParams(args) {
    var params = {};

    if (_typeof(args[0]) === 'object' && !isElement(args[0])) {
      _extends(params, args[0]);
    } else {
      ['title', 'html', 'icon'].forEach(function (name, index) {
        var arg = args[index];

        if (typeof arg === 'string' || isElement(arg)) {
          params[name] = arg;
        } else if (arg !== undefined) {
          error("Unexpected type of ".concat(name, "! Expected \"string\" or \"Element\", got ").concat(_typeof(arg)));
        }
      });
    }

    return params;
  };

  var swalPrefix = 'swal2-';
  var prefix = function prefix(items) {
    var result = {};

    for (var i in items) {
      result[items[i]] = swalPrefix + items[i];
    }

    return result;
  };
  var swalClasses = prefix(['container', 'shown', 'height-auto', 'iosfix', 'popup', 'modal', 'no-backdrop', 'no-transition', 'toast', 'toast-shown', 'show', 'hide', 'close', 'title', 'header', 'content', 'html-container', 'actions', 'confirm', 'deny', 'cancel', 'footer', 'icon', 'icon-content', 'image', 'input', 'file', 'range', 'select', 'radio', 'checkbox', 'label', 'textarea', 'inputerror', 'input-label', 'validation-message', 'progress-steps', 'active-progress-step', 'progress-step', 'progress-step-line', 'loader', 'loading', 'styled', 'top', 'top-start', 'top-end', 'top-left', 'top-right', 'center', 'center-start', 'center-end', 'center-left', 'center-right', 'bottom', 'bottom-start', 'bottom-end', 'bottom-left', 'bottom-right', 'grow-row', 'grow-column', 'grow-fullscreen', 'rtl', 'timer-progress-bar', 'timer-progress-bar-container', 'scrollbar-measure', 'icon-success', 'icon-warning', 'icon-info', 'icon-question', 'icon-error']);
  var iconTypes = prefix(['success', 'warning', 'info', 'question', 'error']);

  var getContainer = function getContainer() {
    return document.body.querySelector(".".concat(swalClasses.container));
  };
  var elementBySelector = function elementBySelector(selectorString) {
    var container = getContainer();
    return container ? container.querySelector(selectorString) : null;
  };

  var elementByClass = function elementByClass(className) {
    return elementBySelector(".".concat(className));
  };

  var getPopup = function getPopup() {
    return elementByClass(swalClasses.popup);
  };
  var getIcon = function getIcon() {
    return elementByClass(swalClasses.icon);
  };
  var getTitle = function getTitle() {
    return elementByClass(swalClasses.title);
  };
  var getContent = function getContent() {
    return elementByClass(swalClasses.content);
  };
  var getHtmlContainer = function getHtmlContainer() {
    return elementByClass(swalClasses['html-container']);
  };
  var getImage = function getImage() {
    return elementByClass(swalClasses.image);
  };
  var getProgressSteps = function getProgressSteps() {
    return elementByClass(swalClasses['progress-steps']);
  };
  var getValidationMessage = function getValidationMessage() {
    return elementByClass(swalClasses['validation-message']);
  };
  var getConfirmButton = function getConfirmButton() {
    return elementBySelector(".".concat(swalClasses.actions, " .").concat(swalClasses.confirm));
  };
  var getDenyButton = function getDenyButton() {
    return elementBySelector(".".concat(swalClasses.actions, " .").concat(swalClasses.deny));
  };
  var getInputLabel = function getInputLabel() {
    return elementByClass(swalClasses['input-label']);
  };
  var getLoader = function getLoader() {
    return elementBySelector(".".concat(swalClasses.loader));
  };
  var getCancelButton = function getCancelButton() {
    return elementBySelector(".".concat(swalClasses.actions, " .").concat(swalClasses.cancel));
  };
  var getActions = function getActions() {
    return elementByClass(swalClasses.actions);
  };
  var getHeader = function getHeader() {
    return elementByClass(swalClasses.header);
  };
  var getFooter = function getFooter() {
    return elementByClass(swalClasses.footer);
  };
  var getTimerProgressBar = function getTimerProgressBar() {
    return elementByClass(swalClasses['timer-progress-bar']);
  };
  var getCloseButton = function getCloseButton() {
    return elementByClass(swalClasses.close);
  }; // https://github.com/jkup/focusable/blob/master/index.js

  var focusable = "\n  a[href],\n  area[href],\n  input:not([disabled]),\n  select:not([disabled]),\n  textarea:not([disabled]),\n  button:not([disabled]),\n  iframe,\n  object,\n  embed,\n  [tabindex=\"0\"],\n  [contenteditable],\n  audio[controls],\n  video[controls],\n  summary\n";
  var getFocusableElements = function getFocusableElements() {
    var focusableElementsWithTabindex = toArray(getPopup().querySelectorAll('[tabindex]:not([tabindex="-1"]):not([tabindex="0"])')) // sort according to tabindex
    .sort(function (a, b) {
      a = parseInt(a.getAttribute('tabindex'));
      b = parseInt(b.getAttribute('tabindex'));

      if (a > b) {
        return 1;
      } else if (a < b) {
        return -1;
      }

      return 0;
    });
    var otherFocusableElements = toArray(getPopup().querySelectorAll(focusable)).filter(function (el) {
      return el.getAttribute('tabindex') !== '-1';
    });
    return uniqueArray(focusableElementsWithTabindex.concat(otherFocusableElements)).filter(function (el) {
      return isVisible(el);
    });
  };
  var isModal = function isModal() {
    return !isToast() && !document.body.classList.contains(swalClasses['no-backdrop']);
  };
  var isToast = function isToast() {
    return document.body.classList.contains(swalClasses['toast-shown']);
  };
  var isLoading = function isLoading() {
    return getPopup().hasAttribute('data-loading');
  };

  var states = {
    previousBodyPadding: null
  };
  var setInnerHtml = function setInnerHtml(elem, html) {
    // #1926
    elem.textContent = '';

    if (html) {
      var parser = new DOMParser();
      var parsed = parser.parseFromString(html, "text/html");
      toArray(parsed.querySelector('head').childNodes).forEach(function (child) {
        elem.appendChild(child);
      });
      toArray(parsed.querySelector('body').childNodes).forEach(function (child) {
        elem.appendChild(child);
      });
    }
  };
  var hasClass = function hasClass(elem, className) {
    if (!className) {
      return false;
    }

    var classList = className.split(/\s+/);

    for (var i = 0; i < classList.length; i++) {
      if (!elem.classList.contains(classList[i])) {
        return false;
      }
    }

    return true;
  };

  var removeCustomClasses = function removeCustomClasses(elem, params) {
    toArray(elem.classList).forEach(function (className) {
      if (!(objectValues(swalClasses).indexOf(className) !== -1) && !(objectValues(iconTypes).indexOf(className) !== -1) && !(objectValues(params.showClass).indexOf(className) !== -1)) {
        elem.classList.remove(className);
      }
    });
  };

  var applyCustomClass = function applyCustomClass(elem, params, className) {
    removeCustomClasses(elem, params);

    if (params.customClass && params.customClass[className]) {
      if (typeof params.customClass[className] !== 'string' && !params.customClass[className].forEach) {
        return warn("Invalid type of customClass.".concat(className, "! Expected string or iterable object, got \"").concat(_typeof(params.customClass[className]), "\""));
      }

      addClass(elem, params.customClass[className]);
    }
  };
  function getInput(content, inputType) {
    if (!inputType) {
      return null;
    }

    switch (inputType) {
      case 'select':
      case 'textarea':
      case 'file':
        return getChildByClass(content, swalClasses[inputType]);

      case 'checkbox':
        return content.querySelector(".".concat(swalClasses.checkbox, " input"));

      case 'radio':
        return content.querySelector(".".concat(swalClasses.radio, " input:checked")) || content.querySelector(".".concat(swalClasses.radio, " input:first-child"));

      case 'range':
        return content.querySelector(".".concat(swalClasses.range, " input"));

      default:
        return getChildByClass(content, swalClasses.input);
    }
  }
  var focusInput = function focusInput(input) {
    input.focus(); // place cursor at end of text in text input

    if (input.type !== 'file') {
      // http://stackoverflow.com/a/2345915
      var val = input.value;
      input.value = '';
      input.value = val;
    }
  };
  var toggleClass = function toggleClass(target, classList, condition) {
    if (!target || !classList) {
      return;
    }

    if (typeof classList === 'string') {
      classList = classList.split(/\s+/).filter(Boolean);
    }

    classList.forEach(function (className) {
      if (target.forEach) {
        target.forEach(function (elem) {
          condition ? elem.classList.add(className) : elem.classList.remove(className);
        });
      } else {
        condition ? target.classList.add(className) : target.classList.remove(className);
      }
    });
  };
  var addClass = function addClass(target, classList) {
    toggleClass(target, classList, true);
  };
  var removeClass = function removeClass(target, classList) {
    toggleClass(target, classList, false);
  };
  var getChildByClass = function getChildByClass(elem, className) {
    for (var i = 0; i < elem.childNodes.length; i++) {
      if (hasClass(elem.childNodes[i], className)) {
        return elem.childNodes[i];
      }
    }
  };
  var applyNumericalStyle = function applyNumericalStyle(elem, property, value) {
    if (value === "".concat(parseInt(value))) {
      value = parseInt(value);
    }

    if (value || parseInt(value) === 0) {
      elem.style[property] = typeof value === 'number' ? "".concat(value, "px") : value;
    } else {
      elem.style.removeProperty(property);
    }
  };
  var show = function show(elem) {
    var display = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'flex';
    elem.style.display = display;
  };
  var hide = function hide(elem) {
    elem.style.display = 'none';
  };
  var setStyle = function setStyle(parent, selector, property, value) {
    var el = parent.querySelector(selector);

    if (el) {
      el.style[property] = value;
    }
  };
  var toggle = function toggle(elem, condition, display) {
    condition ? show(elem, display) : hide(elem);
  }; // borrowed from jquery $(elem).is(':visible') implementation

  var isVisible = function isVisible(elem) {
    return !!(elem && (elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length));
  };
  var allButtonsAreHidden = function allButtonsAreHidden() {
    return !isVisible(getConfirmButton()) && !isVisible(getDenyButton()) && !isVisible(getCancelButton());
  };
  var isScrollable = function isScrollable(elem) {
    return !!(elem.scrollHeight > elem.clientHeight);
  }; // borrowed from https://stackoverflow.com/a/46352119

  var hasCssAnimation = function hasCssAnimation(elem) {
    var style = window.getComputedStyle(elem);
    var animDuration = parseFloat(style.getPropertyValue('animation-duration') || '0');
    var transDuration = parseFloat(style.getPropertyValue('transition-duration') || '0');
    return animDuration > 0 || transDuration > 0;
  };
  var contains = function contains(haystack, needle) {
    if (typeof haystack.contains === 'function') {
      return haystack.contains(needle);
    }
  };
  var animateTimerProgressBar = function animateTimerProgressBar(timer) {
    var reset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var timerProgressBar = getTimerProgressBar();

    if (isVisible(timerProgressBar)) {
      if (reset) {
        timerProgressBar.style.transition = 'none';
        timerProgressBar.style.width = '100%';
      }

      setTimeout(function () {
        timerProgressBar.style.transition = "width ".concat(timer / 1000, "s linear");
        timerProgressBar.style.width = '0%';
      }, 10);
    }
  };
  var stopTimerProgressBar = function stopTimerProgressBar() {
    var timerProgressBar = getTimerProgressBar();
    var timerProgressBarWidth = parseInt(window.getComputedStyle(timerProgressBar).width);
    timerProgressBar.style.removeProperty('transition');
    timerProgressBar.style.width = '100%';
    var timerProgressBarFullWidth = parseInt(window.getComputedStyle(timerProgressBar).width);
    var timerProgressBarPercent = parseInt(timerProgressBarWidth / timerProgressBarFullWidth * 100);
    timerProgressBar.style.removeProperty('transition');
    timerProgressBar.style.width = "".concat(timerProgressBarPercent, "%");
  };

  // Detect Node env
  var isNodeEnv = function isNodeEnv() {
    return typeof window === 'undefined' || typeof document === 'undefined';
  };

  var sweetHTML = "\n <div aria-labelledby=\"".concat(swalClasses.title, "\" aria-describedby=\"").concat(swalClasses.content, "\" class=\"").concat(swalClasses.popup, "\" tabindex=\"-1\">\n   <div class=\"").concat(swalClasses.header, "\">\n     <ul class=\"").concat(swalClasses['progress-steps'], "\"></ul>\n     <div class=\"").concat(swalClasses.icon, "\"></div>\n     <img class=\"").concat(swalClasses.image, "\" />\n     <h2 class=\"").concat(swalClasses.title, "\" id=\"").concat(swalClasses.title, "\"></h2>\n     <button type=\"button\" class=\"").concat(swalClasses.close, "\"></button>\n   </div>\n   <div class=\"").concat(swalClasses.content, "\">\n     <div id=\"").concat(swalClasses.content, "\" class=\"").concat(swalClasses['html-container'], "\"></div>\n     <input class=\"").concat(swalClasses.input, "\" />\n     <input type=\"file\" class=\"").concat(swalClasses.file, "\" />\n     <div class=\"").concat(swalClasses.range, "\">\n       <input type=\"range\" />\n       <output></output>\n     </div>\n     <select class=\"").concat(swalClasses.select, "\"></select>\n     <div class=\"").concat(swalClasses.radio, "\"></div>\n     <label for=\"").concat(swalClasses.checkbox, "\" class=\"").concat(swalClasses.checkbox, "\">\n       <input type=\"checkbox\" />\n       <span class=\"").concat(swalClasses.label, "\"></span>\n     </label>\n     <textarea class=\"").concat(swalClasses.textarea, "\"></textarea>\n     <div class=\"").concat(swalClasses['validation-message'], "\" id=\"").concat(swalClasses['validation-message'], "\"></div>\n   </div>\n   <div class=\"").concat(swalClasses.actions, "\">\n     <div class=\"").concat(swalClasses.loader, "\"></div>\n     <button type=\"button\" class=\"").concat(swalClasses.confirm, "\"></button>\n     <button type=\"button\" class=\"").concat(swalClasses.deny, "\"></button>\n     <button type=\"button\" class=\"").concat(swalClasses.cancel, "\"></button>\n   </div>\n   <div class=\"").concat(swalClasses.footer, "\"></div>\n   <div class=\"").concat(swalClasses['timer-progress-bar-container'], "\">\n     <div class=\"").concat(swalClasses['timer-progress-bar'], "\"></div>\n   </div>\n </div>\n").replace(/(^|\n)\s*/g, '');

  var resetOldContainer = function resetOldContainer() {
    var oldContainer = getContainer();

    if (!oldContainer) {
      return false;
    }

    oldContainer.parentNode.removeChild(oldContainer);
    removeClass([document.documentElement, document.body], [swalClasses['no-backdrop'], swalClasses['toast-shown'], swalClasses['has-column']]);
    return true;
  };

  var oldInputVal; // IE11 workaround, see #1109 for details

  var resetValidationMessage = function resetValidationMessage(e) {
    if (Swal.isVisible() && oldInputVal !== e.target.value) {
      Swal.resetValidationMessage();
    }

    oldInputVal = e.target.value;
  };

  var addInputChangeListeners = function addInputChangeListeners() {
    var content = getContent();
    var input = getChildByClass(content, swalClasses.input);
    var file = getChildByClass(content, swalClasses.file);
    var range = content.querySelector(".".concat(swalClasses.range, " input"));
    var rangeOutput = content.querySelector(".".concat(swalClasses.range, " output"));
    var select = getChildByClass(content, swalClasses.select);
    var checkbox = content.querySelector(".".concat(swalClasses.checkbox, " input"));
    var textarea = getChildByClass(content, swalClasses.textarea);
    input.oninput = resetValidationMessage;
    file.onchange = resetValidationMessage;
    select.onchange = resetValidationMessage;
    checkbox.onchange = resetValidationMessage;
    textarea.oninput = resetValidationMessage;

    range.oninput = function (e) {
      resetValidationMessage(e);
      rangeOutput.value = range.value;
    };

    range.onchange = function (e) {
      resetValidationMessage(e);
      range.nextSibling.value = range.value;
    };
  };

  var getTarget = function getTarget(target) {
    return typeof target === 'string' ? document.querySelector(target) : target;
  };

  var setupAccessibility = function setupAccessibility(params) {
    var popup = getPopup();
    popup.setAttribute('role', params.toast ? 'alert' : 'dialog');
    popup.setAttribute('aria-live', params.toast ? 'polite' : 'assertive');

    if (!params.toast) {
      popup.setAttribute('aria-modal', 'true');
    }
  };

  var setupRTL = function setupRTL(targetElement) {
    if (window.getComputedStyle(targetElement).direction === 'rtl') {
      addClass(getContainer(), swalClasses.rtl);
    }
  };
  /*
   * Add modal + backdrop to DOM
   */


  var init = function init(params) {
    // Clean up the old popup container if it exists
    var oldContainerExisted = resetOldContainer();
    /* istanbul ignore if */

    if (isNodeEnv()) {
      error('SweetAlert2 requires document to initialize');
      return;
    }

    var container = document.createElement('div');
    container.className = swalClasses.container;

    if (oldContainerExisted) {
      addClass(container, swalClasses['no-transition']);
    }

    setInnerHtml(container, sweetHTML);
    var targetElement = getTarget(params.target);
    targetElement.appendChild(container);
    setupAccessibility(params);
    setupRTL(targetElement);
    addInputChangeListeners();
  };

  var parseHtmlToContainer = function parseHtmlToContainer(param, target) {
    // DOM element
    if (param instanceof HTMLElement) {
      target.appendChild(param); // Object
    } else if (_typeof(param) === 'object') {
      handleObject(param, target); // Plain string
    } else if (param) {
      setInnerHtml(target, param);
    }
  };

  var handleObject = function handleObject(param, target) {
    // JQuery element(s)
    if (param.jquery) {
      handleJqueryElem(target, param); // For other objects use their string representation
    } else {
      setInnerHtml(target, param.toString());
    }
  };

  var handleJqueryElem = function handleJqueryElem(target, elem) {
    target.textContent = '';

    if (0 in elem) {
      for (var i = 0; (i in elem); i++) {
        target.appendChild(elem[i].cloneNode(true));
      }
    } else {
      target.appendChild(elem.cloneNode(true));
    }
  };

  var animationEndEvent = function () {
    // Prevent run in Node env

    /* istanbul ignore if */
    if (isNodeEnv()) {
      return false;
    }

    var testEl = document.createElement('div');
    var transEndEventNames = {
      WebkitAnimation: 'webkitAnimationEnd',
      OAnimation: 'oAnimationEnd oanimationend',
      animation: 'animationend'
    };

    for (var i in transEndEventNames) {
      if (Object.prototype.hasOwnProperty.call(transEndEventNames, i) && typeof testEl.style[i] !== 'undefined') {
        return transEndEventNames[i];
      }
    }

    return false;
  }();

  // https://github.com/twbs/bootstrap/blob/master/js/src/modal.js

  var measureScrollbar = function measureScrollbar() {
    var scrollDiv = document.createElement('div');
    scrollDiv.className = swalClasses['scrollbar-measure'];
    document.body.appendChild(scrollDiv);
    var scrollbarWidth = scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);
    return scrollbarWidth;
  };

  var renderActions = function renderActions(instance, params) {
    var actions = getActions();
    var loader = getLoader();
    var confirmButton = getConfirmButton();
    var denyButton = getDenyButton();
    var cancelButton = getCancelButton(); // Actions (buttons) wrapper

    if (!params.showConfirmButton && !params.showDenyButton && !params.showCancelButton) {
      hide(actions);
    } // Custom class


    applyCustomClass(actions, params, 'actions'); // Render buttons

    renderButton(confirmButton, 'confirm', params);
    renderButton(denyButton, 'deny', params);
    renderButton(cancelButton, 'cancel', params);
    handleButtonsStyling(confirmButton, denyButton, cancelButton, params);

    if (params.reverseButtons) {
      actions.insertBefore(cancelButton, loader);
      actions.insertBefore(denyButton, loader);
      actions.insertBefore(confirmButton, loader);
    } // Loader


    setInnerHtml(loader, params.loaderHtml);
    applyCustomClass(loader, params, 'loader');
  };

  function handleButtonsStyling(confirmButton, denyButton, cancelButton, params) {
    if (!params.buttonsStyling) {
      return removeClass([confirmButton, denyButton, cancelButton], swalClasses.styled);
    }

    addClass([confirmButton, denyButton, cancelButton], swalClasses.styled); // Buttons background colors

    if (params.confirmButtonColor) {
      confirmButton.style.backgroundColor = params.confirmButtonColor;
    }

    if (params.denyButtonColor) {
      denyButton.style.backgroundColor = params.denyButtonColor;
    }

    if (params.cancelButtonColor) {
      cancelButton.style.backgroundColor = params.cancelButtonColor;
    }
  }

  function renderButton(button, buttonType, params) {
    toggle(button, params["show".concat(capitalizeFirstLetter(buttonType), "Button")], 'inline-block');
    setInnerHtml(button, params["".concat(buttonType, "ButtonText")]); // Set caption text

    button.setAttribute('aria-label', params["".concat(buttonType, "ButtonAriaLabel")]); // ARIA label
    // Add buttons custom classes

    button.className = swalClasses[buttonType];
    applyCustomClass(button, params, "".concat(buttonType, "Button"));
    addClass(button, params["".concat(buttonType, "ButtonClass")]);
  }

  function handleBackdropParam(container, backdrop) {
    if (typeof backdrop === 'string') {
      container.style.background = backdrop;
    } else if (!backdrop) {
      addClass([document.documentElement, document.body], swalClasses['no-backdrop']);
    }
  }

  function handlePositionParam(container, position) {
    if (position in swalClasses) {
      addClass(container, swalClasses[position]);
    } else {
      warn('The "position" parameter is not valid, defaulting to "center"');
      addClass(container, swalClasses.center);
    }
  }

  function handleGrowParam(container, grow) {
    if (grow && typeof grow === 'string') {
      var growClass = "grow-".concat(grow);

      if (growClass in swalClasses) {
        addClass(container, swalClasses[growClass]);
      }
    }
  }

  var renderContainer = function renderContainer(instance, params) {
    var container = getContainer();

    if (!container) {
      return;
    }

    handleBackdropParam(container, params.backdrop);

    if (!params.backdrop && params.allowOutsideClick) {
      warn('"allowOutsideClick" parameter requires `backdrop` parameter to be set to `true`');
    }

    handlePositionParam(container, params.position);
    handleGrowParam(container, params.grow); // Custom class

    applyCustomClass(container, params, 'container'); // Set queue step attribute for getQueueStep() method

    var queueStep = document.body.getAttribute('data-swal2-queue-step');

    if (queueStep) {
      container.setAttribute('data-queue-step', queueStep);
      document.body.removeAttribute('data-swal2-queue-step');
    }
  };

  /**
   * This module containts `WeakMap`s for each effectively-"private  property" that a `Swal` has.
   * For example, to set the private property "foo" of `this` to "bar", you can `privateProps.foo.set(this, 'bar')`
   * This is the approach that Babel will probably take to implement private methods/fields
   *   https://github.com/tc39/proposal-private-methods
   *   https://github.com/babel/babel/pull/7555
   * Once we have the changes from that PR in Babel, and our core class fits reasonable in *one module*
   *   then we can use that language feature.
   */
  var privateProps = {
    promise: new WeakMap(),
    innerParams: new WeakMap(),
    domCache: new WeakMap()
  };

  var inputTypes = ['input', 'file', 'range', 'select', 'radio', 'checkbox', 'textarea'];
  var renderInput = function renderInput(instance, params) {
    var content = getContent();
    var innerParams = privateProps.innerParams.get(instance);
    var rerender = !innerParams || params.input !== innerParams.input;
    inputTypes.forEach(function (inputType) {
      var inputClass = swalClasses[inputType];
      var inputContainer = getChildByClass(content, inputClass); // set attributes

      setAttributes(inputType, params.inputAttributes); // set class

      inputContainer.className = inputClass;

      if (rerender) {
        hide(inputContainer);
      }
    });

    if (params.input) {
      if (rerender) {
        showInput(params);
      } // set custom class


      setCustomClass(params);
    }
  };

  var showInput = function showInput(params) {
    if (!renderInputType[params.input]) {
      return error("Unexpected type of input! Expected \"text\", \"email\", \"password\", \"number\", \"tel\", \"select\", \"radio\", \"checkbox\", \"textarea\", \"file\" or \"url\", got \"".concat(params.input, "\""));
    }

    var inputContainer = getInputContainer(params.input);
    var input = renderInputType[params.input](inputContainer, params);
    show(input); // input autofocus

    setTimeout(function () {
      focusInput(input);
    });
  };

  var removeAttributes = function removeAttributes(input) {
    for (var i = 0; i < input.attributes.length; i++) {
      var attrName = input.attributes[i].name;

      if (!(['type', 'value', 'style'].indexOf(attrName) !== -1)) {
        input.removeAttribute(attrName);
      }
    }
  };

  var setAttributes = function setAttributes(inputType, inputAttributes) {
    var input = getInput(getContent(), inputType);

    if (!input) {
      return;
    }

    removeAttributes(input);

    for (var attr in inputAttributes) {
      // Do not set a placeholder for <input type="range">
      // it'll crash Edge, #1298
      if (inputType === 'range' && attr === 'placeholder') {
        continue;
      }

      input.setAttribute(attr, inputAttributes[attr]);
    }
  };

  var setCustomClass = function setCustomClass(params) {
    var inputContainer = getInputContainer(params.input);

    if (params.customClass) {
      addClass(inputContainer, params.customClass.input);
    }
  };

  var setInputPlaceholder = function setInputPlaceholder(input, params) {
    if (!input.placeholder || params.inputPlaceholder) {
      input.placeholder = params.inputPlaceholder;
    }
  };

  var setInputLabel = function setInputLabel(input, prependTo, params) {
    if (params.inputLabel) {
      input.id = swalClasses.input;
      var label = document.createElement('label');
      var labelClass = swalClasses['input-label'];
      label.setAttribute('for', input.id);
      label.className = labelClass;
      addClass(label, params.customClass.inputLabel);
      label.innerText = params.inputLabel;
      prependTo.insertAdjacentElement('beforebegin', label);
    }
  };

  var getInputContainer = function getInputContainer(inputType) {
    var inputClass = swalClasses[inputType] ? swalClasses[inputType] : swalClasses.input;
    return getChildByClass(getContent(), inputClass);
  };

  var renderInputType = {};

  renderInputType.text = renderInputType.email = renderInputType.password = renderInputType.number = renderInputType.tel = renderInputType.url = function (input, params) {
    if (typeof params.inputValue === 'string' || typeof params.inputValue === 'number') {
      input.value = params.inputValue;
    } else if (!isPromise(params.inputValue)) {
      warn("Unexpected type of inputValue! Expected \"string\", \"number\" or \"Promise\", got \"".concat(_typeof(params.inputValue), "\""));
    }

    setInputLabel(input, input, params);
    setInputPlaceholder(input, params);
    input.type = params.input;
    return input;
  };

  renderInputType.file = function (input, params) {
    setInputLabel(input, input, params);
    setInputPlaceholder(input, params);
    return input;
  };

  renderInputType.range = function (range, params) {
    var rangeInput = range.querySelector('input');
    var rangeOutput = range.querySelector('output');
    rangeInput.value = params.inputValue;
    rangeInput.type = params.input;
    rangeOutput.value = params.inputValue;
    setInputLabel(rangeInput, range, params);
    return range;
  };

  renderInputType.select = function (select, params) {
    select.textContent = '';

    if (params.inputPlaceholder) {
      var placeholder = document.createElement('option');
      setInnerHtml(placeholder, params.inputPlaceholder);
      placeholder.value = '';
      placeholder.disabled = true;
      placeholder.selected = true;
      select.appendChild(placeholder);
    }

    setInputLabel(select, select, params);
    return select;
  };

  renderInputType.radio = function (radio) {
    radio.textContent = '';
    return radio;
  };

  renderInputType.checkbox = function (checkboxContainer, params) {
    var checkbox = getInput(getContent(), 'checkbox');
    checkbox.value = 1;
    checkbox.id = swalClasses.checkbox;
    checkbox.checked = Boolean(params.inputValue);
    var label = checkboxContainer.querySelector('span');
    setInnerHtml(label, params.inputPlaceholder);
    return checkboxContainer;
  };

  renderInputType.textarea = function (textarea, params) {
    textarea.value = params.inputValue;
    setInputPlaceholder(textarea, params);
    setInputLabel(textarea, textarea, params);

    var getPadding = function getPadding(el) {
      return parseInt(window.getComputedStyle(el).paddingLeft) + parseInt(window.getComputedStyle(el).paddingRight);
    };

    if ('MutationObserver' in window) {
      // #1699
      var initialPopupWidth = parseInt(window.getComputedStyle(getPopup()).width);

      var outputsize = function outputsize() {
        var contentWidth = textarea.offsetWidth + getPadding(getPopup()) + getPadding(getContent());

        if (contentWidth > initialPopupWidth) {
          getPopup().style.width = "".concat(contentWidth, "px");
        } else {
          getPopup().style.width = null;
        }
      };

      new MutationObserver(outputsize).observe(textarea, {
        attributes: true,
        attributeFilter: ['style']
      });
    }

    return textarea;
  };

  var renderContent = function renderContent(instance, params) {
    var htmlContainer = getHtmlContainer();
    applyCustomClass(htmlContainer, params, 'htmlContainer'); // Content as HTML

    if (params.html) {
      parseHtmlToContainer(params.html, htmlContainer);
      show(htmlContainer, 'block'); // Content as plain text
    } else if (params.text) {
      htmlContainer.textContent = params.text;
      show(htmlContainer, 'block'); // No content
    } else {
      hide(htmlContainer);
    }

    renderInput(instance, params); // Custom class

    applyCustomClass(getContent(), params, 'content');
  };

  var renderFooter = function renderFooter(instance, params) {
    var footer = getFooter();
    toggle(footer, params.footer);

    if (params.footer) {
      parseHtmlToContainer(params.footer, footer);
    } // Custom class


    applyCustomClass(footer, params, 'footer');
  };

  var renderCloseButton = function renderCloseButton(instance, params) {
    var closeButton = getCloseButton();
    setInnerHtml(closeButton, params.closeButtonHtml); // Custom class

    applyCustomClass(closeButton, params, 'closeButton');
    toggle(closeButton, params.showCloseButton);
    closeButton.setAttribute('aria-label', params.closeButtonAriaLabel);
  };

  var renderIcon = function renderIcon(instance, params) {
    var innerParams = privateProps.innerParams.get(instance);
    var icon = getIcon(); // if the given icon already rendered, apply the styling without re-rendering the icon

    if (innerParams && params.icon === innerParams.icon) {
      // Custom or default content
      setContent(icon, params);
      applyStyles(icon, params);
      return;
    }

    if (!params.icon && !params.iconHtml) {
      return hide(icon);
    }

    if (params.icon && Object.keys(iconTypes).indexOf(params.icon) === -1) {
      error("Unknown icon! Expected \"success\", \"error\", \"warning\", \"info\" or \"question\", got \"".concat(params.icon, "\""));
      return hide(icon);
    }

    show(icon); // Custom or default content

    setContent(icon, params);
    applyStyles(icon, params); // Animate icon

    addClass(icon, params.showClass.icon);
  };

  var applyStyles = function applyStyles(icon, params) {
    for (var iconType in iconTypes) {
      if (params.icon !== iconType) {
        removeClass(icon, iconTypes[iconType]);
      }
    }

    addClass(icon, iconTypes[params.icon]); // Icon color

    setColor(icon, params); // Success icon background color

    adjustSuccessIconBackgoundColor(); // Custom class

    applyCustomClass(icon, params, 'icon');
  }; // Adjust success icon background color to match the popup background color


  var adjustSuccessIconBackgoundColor = function adjustSuccessIconBackgoundColor() {
    var popup = getPopup();
    var popupBackgroundColor = window.getComputedStyle(popup).getPropertyValue('background-color');
    var successIconParts = popup.querySelectorAll('[class^=swal2-success-circular-line], .swal2-success-fix');

    for (var i = 0; i < successIconParts.length; i++) {
      successIconParts[i].style.backgroundColor = popupBackgroundColor;
    }
  };

  var setContent = function setContent(icon, params) {
    icon.textContent = '';

    if (params.iconHtml) {
      setInnerHtml(icon, iconContent(params.iconHtml));
    } else if (params.icon === 'success') {
      setInnerHtml(icon, "\n      <div class=\"swal2-success-circular-line-left\"></div>\n      <span class=\"swal2-success-line-tip\"></span> <span class=\"swal2-success-line-long\"></span>\n      <div class=\"swal2-success-ring\"></div> <div class=\"swal2-success-fix\"></div>\n      <div class=\"swal2-success-circular-line-right\"></div>\n    ");
    } else if (params.icon === 'error') {
      setInnerHtml(icon, "\n      <span class=\"swal2-x-mark\">\n        <span class=\"swal2-x-mark-line-left\"></span>\n        <span class=\"swal2-x-mark-line-right\"></span>\n      </span>\n    ");
    } else {
      var defaultIconHtml = {
        question: '?',
        warning: '!',
        info: 'i'
      };
      setInnerHtml(icon, iconContent(defaultIconHtml[params.icon]));
    }
  };

  var setColor = function setColor(icon, params) {
    if (!params.iconColor) {
      return;
    }

    icon.style.color = params.iconColor;
    icon.style.borderColor = params.iconColor;

    for (var _i = 0, _arr = ['.swal2-success-line-tip', '.swal2-success-line-long', '.swal2-x-mark-line-left', '.swal2-x-mark-line-right']; _i < _arr.length; _i++) {
      var sel = _arr[_i];
      setStyle(icon, sel, 'backgroundColor', params.iconColor);
    }

    setStyle(icon, '.swal2-success-ring', 'borderColor', params.iconColor);
  };

  var iconContent = function iconContent(content) {
    return "<div class=\"".concat(swalClasses['icon-content'], "\">").concat(content, "</div>");
  };

  var renderImage = function renderImage(instance, params) {
    var image = getImage();

    if (!params.imageUrl) {
      return hide(image);
    }

    show(image, ''); // Src, alt

    image.setAttribute('src', params.imageUrl);
    image.setAttribute('alt', params.imageAlt); // Width, height

    applyNumericalStyle(image, 'width', params.imageWidth);
    applyNumericalStyle(image, 'height', params.imageHeight); // Class

    image.className = swalClasses.image;
    applyCustomClass(image, params, 'image');
  };

  var currentSteps = [];
  /*
   * Global function for chaining sweetAlert popups
   */

  var queue = function queue(steps) {
    warnAboutDeprecation('Swal.queue()', "async/await");
    var Swal = this;
    currentSteps = steps;

    var resetAndResolve = function resetAndResolve(resolve, value) {
      currentSteps = [];
      resolve(value);
    };

    var queueResult = [];
    return new Promise(function (resolve) {
      (function step(i, callback) {
        if (i < currentSteps.length) {
          document.body.setAttribute('data-swal2-queue-step', i);
          Swal.fire(currentSteps[i]).then(function (result) {
            if (typeof result.value !== 'undefined') {
              queueResult.push(result.value);
              step(i + 1, callback);
            } else {
              resetAndResolve(resolve, {
                dismiss: result.dismiss
              });
            }
          });
        } else {
          resetAndResolve(resolve, {
            value: queueResult
          });
        }
      })(0);
    });
  };
  /*
   * Global function for getting the index of current popup in queue
   */

  var getQueueStep = function getQueueStep() {
    return getContainer() && getContainer().getAttribute('data-queue-step');
  };
  /*
   * Global function for inserting a popup to the queue
   */

  var insertQueueStep = function insertQueueStep(step, index) {
    if (index && index < currentSteps.length) {
      return currentSteps.splice(index, 0, step);
    }

    return currentSteps.push(step);
  };
  /*
   * Global function for deleting a popup from the queue
   */

  var deleteQueueStep = function deleteQueueStep(index) {
    if (typeof currentSteps[index] !== 'undefined') {
      currentSteps.splice(index, 1);
    }
  };

  var createStepElement = function createStepElement(step) {
    var stepEl = document.createElement('li');
    addClass(stepEl, swalClasses['progress-step']);
    setInnerHtml(stepEl, step);
    return stepEl;
  };

  var createLineElement = function createLineElement(params) {
    var lineEl = document.createElement('li');
    addClass(lineEl, swalClasses['progress-step-line']);

    if (params.progressStepsDistance) {
      lineEl.style.width = params.progressStepsDistance;
    }

    return lineEl;
  };

  var renderProgressSteps = function renderProgressSteps(instance, params) {
    var progressStepsContainer = getProgressSteps();

    if (!params.progressSteps || params.progressSteps.length === 0) {
      return hide(progressStepsContainer);
    }

    show(progressStepsContainer);
    progressStepsContainer.textContent = '';
    var currentProgressStep = parseInt(params.currentProgressStep === undefined ? getQueueStep() : params.currentProgressStep);

    if (currentProgressStep >= params.progressSteps.length) {
      warn('Invalid currentProgressStep parameter, it should be less than progressSteps.length ' + '(currentProgressStep like JS arrays starts from 0)');
    }

    params.progressSteps.forEach(function (step, index) {
      var stepEl = createStepElement(step);
      progressStepsContainer.appendChild(stepEl);

      if (index === currentProgressStep) {
        addClass(stepEl, swalClasses['active-progress-step']);
      }

      if (index !== params.progressSteps.length - 1) {
        var lineEl = createLineElement(params);
        progressStepsContainer.appendChild(lineEl);
      }
    });
  };

  var renderTitle = function renderTitle(instance, params) {
    var title = getTitle();
    toggle(title, params.title || params.titleText, 'block');

    if (params.title) {
      parseHtmlToContainer(params.title, title);
    }

    if (params.titleText) {
      title.innerText = params.titleText;
    } // Custom class


    applyCustomClass(title, params, 'title');
  };

  var renderHeader = function renderHeader(instance, params) {
    var header = getHeader(); // Custom class

    applyCustomClass(header, params, 'header'); // Progress steps

    renderProgressSteps(instance, params); // Icon

    renderIcon(instance, params); // Image

    renderImage(instance, params); // Title

    renderTitle(instance, params); // Close button

    renderCloseButton(instance, params);
  };

  var renderPopup = function renderPopup(instance, params) {
    var container = getContainer();
    var popup = getPopup(); // Width

    if (params.toast) {
      // #2170
      applyNumericalStyle(container, 'width', params.width);
      popup.style.width = '100%';
    } else {
      applyNumericalStyle(popup, 'width', params.width);
    } // Padding


    applyNumericalStyle(popup, 'padding', params.padding); // Background

    if (params.background) {
      popup.style.background = params.background;
    }

    hide(getValidationMessage()); // Classes

    addClasses(popup, params);
  };

  var addClasses = function addClasses(popup, params) {
    // Default Class + showClass when updating Swal.update({})
    popup.className = "".concat(swalClasses.popup, " ").concat(isVisible(popup) ? params.showClass.popup : '');

    if (params.toast) {
      addClass([document.documentElement, document.body], swalClasses['toast-shown']);
      addClass(popup, swalClasses.toast);
    } else {
      addClass(popup, swalClasses.modal);
    } // Custom class


    applyCustomClass(popup, params, 'popup');

    if (typeof params.customClass === 'string') {
      addClass(popup, params.customClass);
    } // Icon class (#1842)


    if (params.icon) {
      addClass(popup, swalClasses["icon-".concat(params.icon)]);
    }
  };

  var render = function render(instance, params) {
    renderPopup(instance, params);
    renderContainer(instance, params);
    renderHeader(instance, params);
    renderContent(instance, params);
    renderActions(instance, params);
    renderFooter(instance, params);

    if (typeof params.didRender === 'function') {
      params.didRender(getPopup());
    } else if (typeof params.onRender === 'function') {
      params.onRender(getPopup()); // @deprecated
    }
  };

  /*
   * Global function to determine if SweetAlert2 popup is shown
   */

  var isVisible$1 = function isVisible$$1() {
    return isVisible(getPopup());
  };
  /*
   * Global function to click 'Confirm' button
   */

  var clickConfirm = function clickConfirm() {
    return getConfirmButton() && getConfirmButton().click();
  };
  /*
   * Global function to click 'Deny' button
   */

  var clickDeny = function clickDeny() {
    return getDenyButton() && getDenyButton().click();
  };
  /*
   * Global function to click 'Cancel' button
   */

  var clickCancel = function clickCancel() {
    return getCancelButton() && getCancelButton().click();
  };

  function fire() {
    var Swal = this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _construct(Swal, args);
  }

  /**
   * Returns an extended version of `Swal` containing `params` as defaults.
   * Useful for reusing Swal configuration.
   *
   * For example:
   *
   * Before:
   * const textPromptOptions = { input: 'text', showCancelButton: true }
   * const {value: firstName} = await Swal.fire({ ...textPromptOptions, title: 'What is your first name?' })
   * const {value: lastName} = await Swal.fire({ ...textPromptOptions, title: 'What is your last name?' })
   *
   * After:
   * const TextPrompt = Swal.mixin({ input: 'text', showCancelButton: true })
   * const {value: firstName} = await TextPrompt('What is your first name?')
   * const {value: lastName} = await TextPrompt('What is your last name?')
   *
   * @param mixinParams
   */
  function mixin(mixinParams) {
    var MixinSwal = /*#__PURE__*/function (_this) {
      _inherits(MixinSwal, _this);

      var _super = _createSuper(MixinSwal);

      function MixinSwal() {
        _classCallCheck(this, MixinSwal);

        return _super.apply(this, arguments);
      }

      _createClass(MixinSwal, [{
        key: "_main",
        value: function _main(params, priorityMixinParams) {
          return _get(_getPrototypeOf(MixinSwal.prototype), "_main", this).call(this, params, _extends({}, mixinParams, priorityMixinParams));
        }
      }]);

      return MixinSwal;
    }(this);

    return MixinSwal;
  }

  /**
   * Shows loader (spinner), this is useful with AJAX requests.
   * By default the loader be shown instead of the "Confirm" button.
   */

  var showLoading = function showLoading(buttonToReplace) {
    var popup = getPopup();

    if (!popup) {
      Swal.fire();
    }

    popup = getPopup();
    var actions = getActions();
    var loader = getLoader();

    if (!buttonToReplace && isVisible(getConfirmButton())) {
      buttonToReplace = getConfirmButton();
    }

    show(actions);

    if (buttonToReplace) {
      hide(buttonToReplace);
      loader.setAttribute('data-button-to-replace', buttonToReplace.className);
    }

    loader.parentNode.insertBefore(loader, buttonToReplace);
    addClass([popup, actions], swalClasses.loading);
    show(loader);
    popup.setAttribute('data-loading', true);
    popup.setAttribute('aria-busy', true);
    popup.focus();
  };

  var RESTORE_FOCUS_TIMEOUT = 100;

  var globalState = {};

  var focusPreviousActiveElement = function focusPreviousActiveElement() {
    if (globalState.previousActiveElement && globalState.previousActiveElement.focus) {
      globalState.previousActiveElement.focus();
      globalState.previousActiveElement = null;
    } else if (document.body) {
      document.body.focus();
    }
  }; // Restore previous active (focused) element


  var restoreActiveElement = function restoreActiveElement(returnFocus) {
    return new Promise(function (resolve) {
      if (!returnFocus) {
        return resolve();
      }

      var x = window.scrollX;
      var y = window.scrollY;
      globalState.restoreFocusTimeout = setTimeout(function () {
        focusPreviousActiveElement();
        resolve();
      }, RESTORE_FOCUS_TIMEOUT); // issues/900

      if (typeof x !== 'undefined' && typeof y !== 'undefined') {
        // IE doesn't have scrollX/scrollY support
        window.scrollTo(x, y);
      }
    });
  };

  /**
   * If `timer` parameter is set, returns number of milliseconds of timer remained.
   * Otherwise, returns undefined.
   */

  var getTimerLeft = function getTimerLeft() {
    return globalState.timeout && globalState.timeout.getTimerLeft();
  };
  /**
   * Stop timer. Returns number of milliseconds of timer remained.
   * If `timer` parameter isn't set, returns undefined.
   */

  var stopTimer = function stopTimer() {
    if (globalState.timeout) {
      stopTimerProgressBar();
      return globalState.timeout.stop();
    }
  };
  /**
   * Resume timer. Returns number of milliseconds of timer remained.
   * If `timer` parameter isn't set, returns undefined.
   */

  var resumeTimer = function resumeTimer() {
    if (globalState.timeout) {
      var remaining = globalState.timeout.start();
      animateTimerProgressBar(remaining);
      return remaining;
    }
  };
  /**
   * Resume timer. Returns number of milliseconds of timer remained.
   * If `timer` parameter isn't set, returns undefined.
   */

  var toggleTimer = function toggleTimer() {
    var timer = globalState.timeout;
    return timer && (timer.running ? stopTimer() : resumeTimer());
  };
  /**
   * Increase timer. Returns number of milliseconds of an updated timer.
   * If `timer` parameter isn't set, returns undefined.
   */

  var increaseTimer = function increaseTimer(n) {
    if (globalState.timeout) {
      var remaining = globalState.timeout.increase(n);
      animateTimerProgressBar(remaining, true);
      return remaining;
    }
  };
  /**
   * Check if timer is running. Returns true if timer is running
   * or false if timer is paused or stopped.
   * If `timer` parameter isn't set, returns undefined
   */

  var isTimerRunning = function isTimerRunning() {
    return globalState.timeout && globalState.timeout.isRunning();
  };

  var bodyClickListenerAdded = false;
  var clickHandlers = {};
  function bindClickHandler() {
    var attr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'data-swal-template';
    clickHandlers[attr] = this;

    if (!bodyClickListenerAdded) {
      document.body.addEventListener('click', bodyClickListener);
      bodyClickListenerAdded = true;
    }
  }

  var bodyClickListener = function bodyClickListener(event) {
    // 1. using .parentNode instead of event.path because of better support by old browsers https://stackoverflow.com/a/39245638
    // 2. using .parentNode instead of .parentElement because of IE11 + SVG https://stackoverflow.com/a/36270354
    for (var el = event.target; el && el !== document; el = el.parentNode) {
      for (var attr in clickHandlers) {
        var template = el.getAttribute(attr);

        if (template) {
          clickHandlers[attr].fire({
            template: template
          });
          return;
        }
      }
    }
  };

  var defaultParams = {
    title: '',
    titleText: '',
    text: '',
    html: '',
    footer: '',
    icon: undefined,
    iconColor: undefined,
    iconHtml: undefined,
    template: undefined,
    toast: false,
    animation: true,
    showClass: {
      popup: 'swal2-show',
      backdrop: 'swal2-backdrop-show',
      icon: 'swal2-icon-show'
    },
    hideClass: {
      popup: 'swal2-hide',
      backdrop: 'swal2-backdrop-hide',
      icon: 'swal2-icon-hide'
    },
    customClass: {},
    target: 'body',
    backdrop: true,
    heightAuto: true,
    allowOutsideClick: true,
    allowEscapeKey: true,
    allowEnterKey: true,
    stopKeydownPropagation: true,
    keydownListenerCapture: false,
    showConfirmButton: true,
    showDenyButton: false,
    showCancelButton: false,
    preConfirm: undefined,
    preDeny: undefined,
    confirmButtonText: 'OK',
    confirmButtonAriaLabel: '',
    confirmButtonColor: undefined,
    denyButtonText: 'No',
    denyButtonAriaLabel: '',
    denyButtonColor: undefined,
    cancelButtonText: 'Cancel',
    cancelButtonAriaLabel: '',
    cancelButtonColor: undefined,
    buttonsStyling: true,
    reverseButtons: false,
    focusConfirm: true,
    focusDeny: false,
    focusCancel: false,
    returnFocus: true,
    showCloseButton: false,
    closeButtonHtml: '&times;',
    closeButtonAriaLabel: 'Close this dialog',
    loaderHtml: '',
    showLoaderOnConfirm: false,
    showLoaderOnDeny: false,
    imageUrl: undefined,
    imageWidth: undefined,
    imageHeight: undefined,
    imageAlt: '',
    timer: undefined,
    timerProgressBar: false,
    width: undefined,
    padding: undefined,
    background: undefined,
    input: undefined,
    inputPlaceholder: '',
    inputLabel: '',
    inputValue: '',
    inputOptions: {},
    inputAutoTrim: true,
    inputAttributes: {},
    inputValidator: undefined,
    returnInputValueOnDeny: false,
    validationMessage: undefined,
    grow: false,
    position: 'center',
    progressSteps: [],
    currentProgressStep: undefined,
    progressStepsDistance: undefined,
    onBeforeOpen: undefined,
    onOpen: undefined,
    willOpen: undefined,
    didOpen: undefined,
    onRender: undefined,
    didRender: undefined,
    onClose: undefined,
    onAfterClose: undefined,
    willClose: undefined,
    didClose: undefined,
    onDestroy: undefined,
    didDestroy: undefined,
    scrollbarPadding: true
  };
  var updatableParams = ['allowEscapeKey', 'allowOutsideClick', 'background', 'buttonsStyling', 'cancelButtonAriaLabel', 'cancelButtonColor', 'cancelButtonText', 'closeButtonAriaLabel', 'closeButtonHtml', 'confirmButtonAriaLabel', 'confirmButtonColor', 'confirmButtonText', 'currentProgressStep', 'customClass', 'denyButtonAriaLabel', 'denyButtonColor', 'denyButtonText', 'didClose', 'didDestroy', 'footer', 'hideClass', 'html', 'icon', 'iconColor', 'iconHtml', 'imageAlt', 'imageHeight', 'imageUrl', 'imageWidth', 'onAfterClose', 'onClose', 'onDestroy', 'progressSteps', 'returnFocus', 'reverseButtons', 'showCancelButton', 'showCloseButton', 'showConfirmButton', 'showDenyButton', 'text', 'title', 'titleText', 'willClose'];
  var deprecatedParams = {
    animation: 'showClass" and "hideClass',
    onBeforeOpen: 'willOpen',
    onOpen: 'didOpen',
    onRender: 'didRender',
    onClose: 'willClose',
    onAfterClose: 'didClose',
    onDestroy: 'didDestroy'
  };
  var toastIncompatibleParams = ['allowOutsideClick', 'allowEnterKey', 'backdrop', 'focusConfirm', 'focusDeny', 'focusCancel', 'returnFocus', 'heightAuto', 'keydownListenerCapture'];
  /**
   * Is valid parameter
   * @param {String} paramName
   */

  var isValidParameter = function isValidParameter(paramName) {
    return Object.prototype.hasOwnProperty.call(defaultParams, paramName);
  };
  /**
   * Is valid parameter for Swal.update() method
   * @param {String} paramName
   */

  var isUpdatableParameter = function isUpdatableParameter(paramName) {
    return updatableParams.indexOf(paramName) !== -1;
  };
  /**
   * Is deprecated parameter
   * @param {String} paramName
   */

  var isDeprecatedParameter = function isDeprecatedParameter(paramName) {
    return deprecatedParams[paramName];
  };

  var checkIfParamIsValid = function checkIfParamIsValid(param) {
    if (!isValidParameter(param)) {
      warn("Unknown parameter \"".concat(param, "\""));
    }
  };

  var checkIfToastParamIsValid = function checkIfToastParamIsValid(param) {
    if (toastIncompatibleParams.indexOf(param) !== -1) {
      warn("The parameter \"".concat(param, "\" is incompatible with toasts"));
    }
  };

  var checkIfParamIsDeprecated = function checkIfParamIsDeprecated(param) {
    if (isDeprecatedParameter(param)) {
      warnAboutDeprecation(param, isDeprecatedParameter(param));
    }
  };
  /**
   * Show relevant warnings for given params
   *
   * @param params
   */


  var showWarningsForParams = function showWarningsForParams(params) {
    for (var param in params) {
      checkIfParamIsValid(param);

      if (params.toast) {
        checkIfToastParamIsValid(param);
      }

      checkIfParamIsDeprecated(param);
    }
  };



  var staticMethods = /*#__PURE__*/Object.freeze({
    isValidParameter: isValidParameter,
    isUpdatableParameter: isUpdatableParameter,
    isDeprecatedParameter: isDeprecatedParameter,
    argsToParams: argsToParams,
    isVisible: isVisible$1,
    clickConfirm: clickConfirm,
    clickDeny: clickDeny,
    clickCancel: clickCancel,
    getContainer: getContainer,
    getPopup: getPopup,
    getTitle: getTitle,
    getContent: getContent,
    getHtmlContainer: getHtmlContainer,
    getImage: getImage,
    getIcon: getIcon,
    getInputLabel: getInputLabel,
    getCloseButton: getCloseButton,
    getActions: getActions,
    getConfirmButton: getConfirmButton,
    getDenyButton: getDenyButton,
    getCancelButton: getCancelButton,
    getLoader: getLoader,
    getHeader: getHeader,
    getFooter: getFooter,
    getTimerProgressBar: getTimerProgressBar,
    getFocusableElements: getFocusableElements,
    getValidationMessage: getValidationMessage,
    isLoading: isLoading,
    fire: fire,
    mixin: mixin,
    queue: queue,
    getQueueStep: getQueueStep,
    insertQueueStep: insertQueueStep,
    deleteQueueStep: deleteQueueStep,
    showLoading: showLoading,
    enableLoading: showLoading,
    getTimerLeft: getTimerLeft,
    stopTimer: stopTimer,
    resumeTimer: resumeTimer,
    toggleTimer: toggleTimer,
    increaseTimer: increaseTimer,
    isTimerRunning: isTimerRunning,
    bindClickHandler: bindClickHandler
  });

  /**
   * Hides loader and shows back the button which was hidden by .showLoading()
   */

  function hideLoading() {
    // do nothing if popup is closed
    var innerParams = privateProps.innerParams.get(this);

    if (!innerParams) {
      return;
    }

    var domCache = privateProps.domCache.get(this);
    hide(domCache.loader);
    var buttonToReplace = domCache.popup.getElementsByClassName(domCache.loader.getAttribute('data-button-to-replace'));

    if (buttonToReplace.length) {
      show(buttonToReplace[0], 'inline-block');
    } else if (allButtonsAreHidden()) {
      hide(domCache.actions);
    }

    removeClass([domCache.popup, domCache.actions], swalClasses.loading);
    domCache.popup.removeAttribute('aria-busy');
    domCache.popup.removeAttribute('data-loading');
    domCache.confirmButton.disabled = false;
    domCache.denyButton.disabled = false;
    domCache.cancelButton.disabled = false;
  }

  function getInput$1(instance) {
    var innerParams = privateProps.innerParams.get(instance || this);
    var domCache = privateProps.domCache.get(instance || this);

    if (!domCache) {
      return null;
    }

    return getInput(domCache.content, innerParams.input);
  }

  var fixScrollbar = function fixScrollbar() {
    // for queues, do not do this more than once
    if (states.previousBodyPadding !== null) {
      return;
    } // if the body has overflow


    if (document.body.scrollHeight > window.innerHeight) {
      // add padding so the content doesn't shift after removal of scrollbar
      states.previousBodyPadding = parseInt(window.getComputedStyle(document.body).getPropertyValue('padding-right'));
      document.body.style.paddingRight = "".concat(states.previousBodyPadding + measureScrollbar(), "px");
    }
  };
  var undoScrollbar = function undoScrollbar() {
    if (states.previousBodyPadding !== null) {
      document.body.style.paddingRight = "".concat(states.previousBodyPadding, "px");
      states.previousBodyPadding = null;
    }
  };

  /* istanbul ignore file */

  var iOSfix = function iOSfix() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream || navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;

    if (iOS && !hasClass(document.body, swalClasses.iosfix)) {
      var offset = document.body.scrollTop;
      document.body.style.top = "".concat(offset * -1, "px");
      addClass(document.body, swalClasses.iosfix);
      lockBodyScroll();
      addBottomPaddingForTallPopups(); // #1948
    }
  };

  var addBottomPaddingForTallPopups = function addBottomPaddingForTallPopups() {
    var safari = !navigator.userAgent.match(/(CriOS|FxiOS|EdgiOS|YaBrowser|UCBrowser)/i);

    if (safari) {
      var bottomPanelHeight = 44;

      if (getPopup().scrollHeight > window.innerHeight - bottomPanelHeight) {
        getContainer().style.paddingBottom = "".concat(bottomPanelHeight, "px");
      }
    }
  };

  var lockBodyScroll = function lockBodyScroll() {
    // #1246
    var container = getContainer();
    var preventTouchMove;

    container.ontouchstart = function (e) {
      preventTouchMove = shouldPreventTouchMove(e);
    };

    container.ontouchmove = function (e) {
      if (preventTouchMove) {
        e.preventDefault();
        e.stopPropagation();
      }
    };
  };

  var shouldPreventTouchMove = function shouldPreventTouchMove(event) {
    var target = event.target;
    var container = getContainer();

    if (isStylys(event) || isZoom(event)) {
      return false;
    }

    if (target === container) {
      return true;
    }

    if (!isScrollable(container) && target.tagName !== 'INPUT' && // #1603
    !(isScrollable(getContent()) && // #1944
    getContent().contains(target))) {
      return true;
    }

    return false;
  };

  var isStylys = function isStylys(event) {
    // #1786
    return event.touches && event.touches.length && event.touches[0].touchType === 'stylus';
  };

  var isZoom = function isZoom(event) {
    // #1891
    return event.touches && event.touches.length > 1;
  };

  var undoIOSfix = function undoIOSfix() {
    if (hasClass(document.body, swalClasses.iosfix)) {
      var offset = parseInt(document.body.style.top, 10);
      removeClass(document.body, swalClasses.iosfix);
      document.body.style.top = '';
      document.body.scrollTop = offset * -1;
    }
  };

  /* istanbul ignore file */

  var isIE11 = function isIE11() {
    return !!window.MSInputMethodContext && !!document.documentMode;
  }; // Fix IE11 centering sweetalert2/issues/933


  var fixVerticalPositionIE = function fixVerticalPositionIE() {
    var container = getContainer();
    var popup = getPopup();
    container.style.removeProperty('align-items');

    if (popup.offsetTop < 0) {
      container.style.alignItems = 'flex-start';
    }
  };

  var IEfix = function IEfix() {
    if (typeof window !== 'undefined' && isIE11()) {
      fixVerticalPositionIE();
      window.addEventListener('resize', fixVerticalPositionIE);
    }
  };
  var undoIEfix = function undoIEfix() {
    if (typeof window !== 'undefined' && isIE11()) {
      window.removeEventListener('resize', fixVerticalPositionIE);
    }
  };

  // Adding aria-hidden="true" to elements outside of the active modal dialog ensures that
  // elements not within the active modal dialog will not be surfaced if a user opens a screen
  // reader’s list of elements (headings, form controls, landmarks, etc.) in the document.

  var setAriaHidden = function setAriaHidden() {
    var bodyChildren = toArray(document.body.children);
    bodyChildren.forEach(function (el) {
      if (el === getContainer() || contains(el, getContainer())) {
        return;
      }

      if (el.hasAttribute('aria-hidden')) {
        el.setAttribute('data-previous-aria-hidden', el.getAttribute('aria-hidden'));
      }

      el.setAttribute('aria-hidden', 'true');
    });
  };
  var unsetAriaHidden = function unsetAriaHidden() {
    var bodyChildren = toArray(document.body.children);
    bodyChildren.forEach(function (el) {
      if (el.hasAttribute('data-previous-aria-hidden')) {
        el.setAttribute('aria-hidden', el.getAttribute('data-previous-aria-hidden'));
        el.removeAttribute('data-previous-aria-hidden');
      } else {
        el.removeAttribute('aria-hidden');
      }
    });
  };

  /**
   * This module containts `WeakMap`s for each effectively-"private  property" that a `Swal` has.
   * For example, to set the private property "foo" of `this` to "bar", you can `privateProps.foo.set(this, 'bar')`
   * This is the approach that Babel will probably take to implement private methods/fields
   *   https://github.com/tc39/proposal-private-methods
   *   https://github.com/babel/babel/pull/7555
   * Once we have the changes from that PR in Babel, and our core class fits reasonable in *one module*
   *   then we can use that language feature.
   */
  var privateMethods = {
    swalPromiseResolve: new WeakMap()
  };

  /*
   * Instance method to close sweetAlert
   */

  function removePopupAndResetState(instance, container, returnFocus, didClose) {
    if (isToast()) {
      triggerDidCloseAndDispose(instance, didClose);
    } else {
      restoreActiveElement(returnFocus).then(function () {
        return triggerDidCloseAndDispose(instance, didClose);
      });
      globalState.keydownTarget.removeEventListener('keydown', globalState.keydownHandler, {
        capture: globalState.keydownListenerCapture
      });
      globalState.keydownHandlerAdded = false;
    }

    if (container.parentNode && !document.body.getAttribute('data-swal2-queue-step')) {
      container.parentNode.removeChild(container);
    }

    if (isModal()) {
      undoScrollbar();
      undoIOSfix();
      undoIEfix();
      unsetAriaHidden();
    }

    removeBodyClasses();
  }

  function removeBodyClasses() {
    removeClass([document.documentElement, document.body], [swalClasses.shown, swalClasses['height-auto'], swalClasses['no-backdrop'], swalClasses['toast-shown']]);
  }

  function close(resolveValue) {
    var popup = getPopup();

    if (!popup) {
      return;
    }

    resolveValue = prepareResolveValue(resolveValue);
    var innerParams = privateProps.innerParams.get(this);

    if (!innerParams || hasClass(popup, innerParams.hideClass.popup)) {
      return;
    }

    var swalPromiseResolve = privateMethods.swalPromiseResolve.get(this);
    removeClass(popup, innerParams.showClass.popup);
    addClass(popup, innerParams.hideClass.popup);
    var backdrop = getContainer();
    removeClass(backdrop, innerParams.showClass.backdrop);
    addClass(backdrop, innerParams.hideClass.backdrop);
    handlePopupAnimation(this, popup, innerParams); // Resolve Swal promise

    swalPromiseResolve(resolveValue);
  }

  var prepareResolveValue = function prepareResolveValue(resolveValue) {
    // When user calls Swal.close()
    if (typeof resolveValue === 'undefined') {
      return {
        isConfirmed: false,
        isDenied: false,
        isDismissed: true
      };
    }

    return _extends({
      isConfirmed: false,
      isDenied: false,
      isDismissed: false
    }, resolveValue);
  };

  var handlePopupAnimation = function handlePopupAnimation(instance, popup, innerParams) {
    var container = getContainer(); // If animation is supported, animate

    var animationIsSupported = animationEndEvent && hasCssAnimation(popup);
    var onClose = innerParams.onClose,
        onAfterClose = innerParams.onAfterClose,
        willClose = innerParams.willClose,
        didClose = innerParams.didClose;
    runDidClose(popup, willClose, onClose);

    if (animationIsSupported) {
      animatePopup(instance, popup, container, innerParams.returnFocus, didClose || onAfterClose);
    } else {
      // Otherwise, remove immediately
      removePopupAndResetState(instance, container, innerParams.returnFocus, didClose || onAfterClose);
    }
  };

  var runDidClose = function runDidClose(popup, willClose, onClose) {
    if (willClose !== null && typeof willClose === 'function') {
      willClose(popup);
    } else if (onClose !== null && typeof onClose === 'function') {
      onClose(popup); // @deprecated
    }
  };

  var animatePopup = function animatePopup(instance, popup, container, returnFocus, didClose) {
    globalState.swalCloseEventFinishedCallback = removePopupAndResetState.bind(null, instance, container, returnFocus, didClose);
    popup.addEventListener(animationEndEvent, function (e) {
      if (e.target === popup) {
        globalState.swalCloseEventFinishedCallback();
        delete globalState.swalCloseEventFinishedCallback;
      }
    });
  };

  var triggerDidCloseAndDispose = function triggerDidCloseAndDispose(instance, didClose) {
    setTimeout(function () {
      if (typeof didClose === 'function') {
        didClose();
      }

      instance._destroy();
    });
  };

  function setButtonsDisabled(instance, buttons, disabled) {
    var domCache = privateProps.domCache.get(instance);
    buttons.forEach(function (button) {
      domCache[button].disabled = disabled;
    });
  }

  function setInputDisabled(input, disabled) {
    if (!input) {
      return false;
    }

    if (input.type === 'radio') {
      var radiosContainer = input.parentNode.parentNode;
      var radios = radiosContainer.querySelectorAll('input');

      for (var i = 0; i < radios.length; i++) {
        radios[i].disabled = disabled;
      }
    } else {
      input.disabled = disabled;
    }
  }

  function enableButtons() {
    setButtonsDisabled(this, ['confirmButton', 'denyButton', 'cancelButton'], false);
  }
  function disableButtons() {
    setButtonsDisabled(this, ['confirmButton', 'denyButton', 'cancelButton'], true);
  }
  function enableInput() {
    return setInputDisabled(this.getInput(), false);
  }
  function disableInput() {
    return setInputDisabled(this.getInput(), true);
  }

  function showValidationMessage(error) {
    var domCache = privateProps.domCache.get(this);
    var params = privateProps.innerParams.get(this);
    setInnerHtml(domCache.validationMessage, error);
    domCache.validationMessage.className = swalClasses['validation-message'];

    if (params.customClass && params.customClass.validationMessage) {
      addClass(domCache.validationMessage, params.customClass.validationMessage);
    }

    show(domCache.validationMessage);
    var input = this.getInput();

    if (input) {
      input.setAttribute('aria-invalid', true);
      input.setAttribute('aria-describedBy', swalClasses['validation-message']);
      focusInput(input);
      addClass(input, swalClasses.inputerror);
    }
  } // Hide block with validation message

  function resetValidationMessage$1() {
    var domCache = privateProps.domCache.get(this);

    if (domCache.validationMessage) {
      hide(domCache.validationMessage);
    }

    var input = this.getInput();

    if (input) {
      input.removeAttribute('aria-invalid');
      input.removeAttribute('aria-describedBy');
      removeClass(input, swalClasses.inputerror);
    }
  }

  function getProgressSteps$1() {
    var domCache = privateProps.domCache.get(this);
    return domCache.progressSteps;
  }

  var Timer = /*#__PURE__*/function () {
    function Timer(callback, delay) {
      _classCallCheck(this, Timer);

      this.callback = callback;
      this.remaining = delay;
      this.running = false;
      this.start();
    }

    _createClass(Timer, [{
      key: "start",
      value: function start() {
        if (!this.running) {
          this.running = true;
          this.started = new Date();
          this.id = setTimeout(this.callback, this.remaining);
        }

        return this.remaining;
      }
    }, {
      key: "stop",
      value: function stop() {
        if (this.running) {
          this.running = false;
          clearTimeout(this.id);
          this.remaining -= new Date() - this.started;
        }

        return this.remaining;
      }
    }, {
      key: "increase",
      value: function increase(n) {
        var running = this.running;

        if (running) {
          this.stop();
        }

        this.remaining += n;

        if (running) {
          this.start();
        }

        return this.remaining;
      }
    }, {
      key: "getTimerLeft",
      value: function getTimerLeft() {
        if (this.running) {
          this.stop();
          this.start();
        }

        return this.remaining;
      }
    }, {
      key: "isRunning",
      value: function isRunning() {
        return this.running;
      }
    }]);

    return Timer;
  }();

  var defaultInputValidators = {
    email: function email(string, validationMessage) {
      return /^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9-]{2,24}$/.test(string) ? Promise.resolve() : Promise.resolve(validationMessage || 'Invalid email address');
    },
    url: function url(string, validationMessage) {
      // taken from https://stackoverflow.com/a/3809435 with a small change from #1306 and #2013
      return /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-z]{2,63}\b([-a-zA-Z0-9@:%_+.~#?&/=]*)$/.test(string) ? Promise.resolve() : Promise.resolve(validationMessage || 'Invalid URL');
    }
  };

  function setDefaultInputValidators(params) {
    // Use default `inputValidator` for supported input types if not provided
    if (!params.inputValidator) {
      Object.keys(defaultInputValidators).forEach(function (key) {
        if (params.input === key) {
          params.inputValidator = defaultInputValidators[key];
        }
      });
    }
  }

  function validateCustomTargetElement(params) {
    // Determine if the custom target element is valid
    if (!params.target || typeof params.target === 'string' && !document.querySelector(params.target) || typeof params.target !== 'string' && !params.target.appendChild) {
      warn('Target parameter is not valid, defaulting to "body"');
      params.target = 'body';
    }
  }
  /**
   * Set type, text and actions on popup
   *
   * @param params
   * @returns {boolean}
   */


  function setParameters(params) {
    setDefaultInputValidators(params); // showLoaderOnConfirm && preConfirm

    if (params.showLoaderOnConfirm && !params.preConfirm) {
      warn('showLoaderOnConfirm is set to true, but preConfirm is not defined.\n' + 'showLoaderOnConfirm should be used together with preConfirm, see usage example:\n' + 'https://sweetalert2.github.io/#ajax-request');
    } // params.animation will be actually used in renderPopup.js
    // but in case when params.animation is a function, we need to call that function
    // before popup (re)initialization, so it'll be possible to check Swal.isVisible()
    // inside the params.animation function


    params.animation = callIfFunction(params.animation);
    validateCustomTargetElement(params); // Replace newlines with <br> in title

    if (typeof params.title === 'string') {
      params.title = params.title.split('\n').join('<br />');
    }

    init(params);
  }

  var swalStringParams = ['swal-title', 'swal-html', 'swal-footer'];
  var getTemplateParams = function getTemplateParams(params) {
    var template = typeof params.template === 'string' ? document.querySelector(params.template) : params.template;

    if (!template) {
      return {};
    }

    var templateContent = template.content || template; // IE11

    showWarningsForElements(templateContent);

    var result = _extends(getSwalParams(templateContent), getSwalButtons(templateContent), getSwalImage(templateContent), getSwalIcon(templateContent), getSwalInput(templateContent), getSwalStringParams(templateContent, swalStringParams));

    return result;
  };

  var getSwalParams = function getSwalParams(templateContent) {
    var result = {};
    toArray(templateContent.querySelectorAll('swal-param')).forEach(function (param) {
      showWarningsForAttributes(param, ['name', 'value']);
      var paramName = param.getAttribute('name');
      var value = param.getAttribute('value');

      if (typeof defaultParams[paramName] === 'boolean' && value === 'false') {
        value = false;
      }

      if (_typeof(defaultParams[paramName]) === 'object') {
        value = JSON.parse(value);
      }

      result[paramName] = value;
    });
    return result;
  };

  var getSwalButtons = function getSwalButtons(templateContent) {
    var result = {};
    toArray(templateContent.querySelectorAll('swal-button')).forEach(function (button) {
      showWarningsForAttributes(button, ['type', 'color', 'aria-label']);
      var type = button.getAttribute('type');
      result["".concat(type, "ButtonText")] = button.innerHTML;
      result["show".concat(capitalizeFirstLetter(type), "Button")] = true;

      if (button.hasAttribute('color')) {
        result["".concat(type, "ButtonColor")] = button.getAttribute('color');
      }

      if (button.hasAttribute('aria-label')) {
        result["".concat(type, "ButtonAriaLabel")] = button.getAttribute('aria-label');
      }
    });
    return result;
  };

  var getSwalImage = function getSwalImage(templateContent) {
    var result = {};
    var image = templateContent.querySelector('swal-image');

    if (image) {
      showWarningsForAttributes(image, ['src', 'width', 'height', 'alt']);

      if (image.hasAttribute('src')) {
        result.imageUrl = image.getAttribute('src');
      }

      if (image.hasAttribute('width')) {
        result.imageWidth = image.getAttribute('width');
      }

      if (image.hasAttribute('height')) {
        result.imageHeight = image.getAttribute('height');
      }

      if (image.hasAttribute('alt')) {
        result.imageAlt = image.getAttribute('alt');
      }
    }

    return result;
  };

  var getSwalIcon = function getSwalIcon(templateContent) {
    var result = {};
    var icon = templateContent.querySelector('swal-icon');

    if (icon) {
      showWarningsForAttributes(icon, ['type', 'color']);

      if (icon.hasAttribute('type')) {
        result.icon = icon.getAttribute('type');
      }

      if (icon.hasAttribute('color')) {
        result.iconColor = icon.getAttribute('color');
      }

      result.iconHtml = icon.innerHTML;
    }

    return result;
  };

  var getSwalInput = function getSwalInput(templateContent) {
    var result = {};
    var input = templateContent.querySelector('swal-input');

    if (input) {
      showWarningsForAttributes(input, ['type', 'label', 'placeholder', 'value']);
      result.input = input.getAttribute('type') || 'text';

      if (input.hasAttribute('label')) {
        result.inputLabel = input.getAttribute('label');
      }

      if (input.hasAttribute('placeholder')) {
        result.inputPlaceholder = input.getAttribute('placeholder');
      }

      if (input.hasAttribute('value')) {
        result.inputValue = input.getAttribute('value');
      }
    }

    var inputOptions = templateContent.querySelectorAll('swal-input-option');

    if (inputOptions.length) {
      result.inputOptions = {};
      toArray(inputOptions).forEach(function (option) {
        showWarningsForAttributes(option, ['value']);
        var optionValue = option.getAttribute('value');
        var optionName = option.innerHTML;
        result.inputOptions[optionValue] = optionName;
      });
    }

    return result;
  };

  var getSwalStringParams = function getSwalStringParams(templateContent, paramNames) {
    var result = {};

    for (var i in paramNames) {
      var paramName = paramNames[i];
      var tag = templateContent.querySelector(paramName);

      if (tag) {
        showWarningsForAttributes(tag, []);
        result[paramName.replace(/^swal-/, '')] = tag.innerHTML.trim();
      }
    }

    return result;
  };

  var showWarningsForElements = function showWarningsForElements(template) {
    var allowedElements = swalStringParams.concat(['swal-param', 'swal-button', 'swal-image', 'swal-icon', 'swal-input', 'swal-input-option']);
    toArray(template.querySelectorAll('*')).forEach(function (el) {
      if (el.parentNode !== template) {
        // can't use template.children because of IE11
        return;
      }

      var tagName = el.tagName.toLowerCase();

      if (allowedElements.indexOf(tagName) === -1) {
        warn("Unrecognized element <".concat(tagName, ">"));
      }
    });
  };

  var showWarningsForAttributes = function showWarningsForAttributes(el, allowedAttributes) {
    toArray(el.attributes).forEach(function (attribute) {
      if (allowedAttributes.indexOf(attribute.name) === -1) {
        warn(["Unrecognized attribute \"".concat(attribute.name, "\" on <").concat(el.tagName.toLowerCase(), ">."), "".concat(allowedAttributes.length ? "Allowed attributes are: ".concat(allowedAttributes.join(', ')) : 'To set the value, use HTML within the element.')]);
      }
    });
  };

  var SHOW_CLASS_TIMEOUT = 10;
  /**
   * Open popup, add necessary classes and styles, fix scrollbar
   *
   * @param params
   */

  var openPopup = function openPopup(params) {
    var container = getContainer();
    var popup = getPopup();

    if (typeof params.willOpen === 'function') {
      params.willOpen(popup);
    } else if (typeof params.onBeforeOpen === 'function') {
      params.onBeforeOpen(popup); // @deprecated
    }

    var bodyStyles = window.getComputedStyle(document.body);
    var initialBodyOverflow = bodyStyles.overflowY;
    addClasses$1(container, popup, params); // scrolling is 'hidden' until animation is done, after that 'auto'

    setTimeout(function () {
      setScrollingVisibility(container, popup);
    }, SHOW_CLASS_TIMEOUT);

    if (isModal()) {
      fixScrollContainer(container, params.scrollbarPadding, initialBodyOverflow);
      setAriaHidden();
    }

    if (!isToast() && !globalState.previousActiveElement) {
      globalState.previousActiveElement = document.activeElement;
    }

    runDidOpen(popup, params);
    removeClass(container, swalClasses['no-transition']);
  };

  var runDidOpen = function runDidOpen(popup, params) {
    if (typeof params.didOpen === 'function') {
      setTimeout(function () {
        return params.didOpen(popup);
      });
    } else if (typeof params.onOpen === 'function') {
      setTimeout(function () {
        return params.onOpen(popup);
      }); // @deprecated
    }
  };

  var swalOpenAnimationFinished = function swalOpenAnimationFinished(event) {
    var popup = getPopup();

    if (event.target !== popup) {
      return;
    }

    var container = getContainer();
    popup.removeEventListener(animationEndEvent, swalOpenAnimationFinished);
    container.style.overflowY = 'auto';
  };

  var setScrollingVisibility = function setScrollingVisibility(container, popup) {
    if (animationEndEvent && hasCssAnimation(popup)) {
      container.style.overflowY = 'hidden';
      popup.addEventListener(animationEndEvent, swalOpenAnimationFinished);
    } else {
      container.style.overflowY = 'auto';
    }
  };

  var fixScrollContainer = function fixScrollContainer(container, scrollbarPadding, initialBodyOverflow) {
    iOSfix();
    IEfix();

    if (scrollbarPadding && initialBodyOverflow !== 'hidden') {
      fixScrollbar();
    } // sweetalert2/issues/1247


    setTimeout(function () {
      container.scrollTop = 0;
    });
  };

  var addClasses$1 = function addClasses(container, popup, params) {
    addClass(container, params.showClass.backdrop); // the workaround with setting/unsetting opacity is needed for #2019 and 2059

    popup.style.setProperty('opacity', '0', 'important');
    show(popup);
    setTimeout(function () {
      // Animate popup right after showing it
      addClass(popup, params.showClass.popup); // and remove the opacity workaround

      popup.style.removeProperty('opacity');
    }, SHOW_CLASS_TIMEOUT); // 10ms in order to fix #2062

    addClass([document.documentElement, document.body], swalClasses.shown);

    if (params.heightAuto && params.backdrop && !params.toast) {
      addClass([document.documentElement, document.body], swalClasses['height-auto']);
    }
  };

  var handleInputOptionsAndValue = function handleInputOptionsAndValue(instance, params) {
    if (params.input === 'select' || params.input === 'radio') {
      handleInputOptions(instance, params);
    } else if (['text', 'email', 'number', 'tel', 'textarea'].indexOf(params.input) !== -1 && (hasToPromiseFn(params.inputValue) || isPromise(params.inputValue))) {
      handleInputValue(instance, params);
    }
  };
  var getInputValue = function getInputValue(instance, innerParams) {
    var input = instance.getInput();

    if (!input) {
      return null;
    }

    switch (innerParams.input) {
      case 'checkbox':
        return getCheckboxValue(input);

      case 'radio':
        return getRadioValue(input);

      case 'file':
        return getFileValue(input);

      default:
        return innerParams.inputAutoTrim ? input.value.trim() : input.value;
    }
  };

  var getCheckboxValue = function getCheckboxValue(input) {
    return input.checked ? 1 : 0;
  };

  var getRadioValue = function getRadioValue(input) {
    return input.checked ? input.value : null;
  };

  var getFileValue = function getFileValue(input) {
    return input.files.length ? input.getAttribute('multiple') !== null ? input.files : input.files[0] : null;
  };

  var handleInputOptions = function handleInputOptions(instance, params) {
    var content = getContent();

    var processInputOptions = function processInputOptions(inputOptions) {
      return populateInputOptions[params.input](content, formatInputOptions(inputOptions), params);
    };

    if (hasToPromiseFn(params.inputOptions) || isPromise(params.inputOptions)) {
      showLoading(getConfirmButton());
      asPromise(params.inputOptions).then(function (inputOptions) {
        instance.hideLoading();
        processInputOptions(inputOptions);
      });
    } else if (_typeof(params.inputOptions) === 'object') {
      processInputOptions(params.inputOptions);
    } else {
      error("Unexpected type of inputOptions! Expected object, Map or Promise, got ".concat(_typeof(params.inputOptions)));
    }
  };

  var handleInputValue = function handleInputValue(instance, params) {
    var input = instance.getInput();
    hide(input);
    asPromise(params.inputValue).then(function (inputValue) {
      input.value = params.input === 'number' ? parseFloat(inputValue) || 0 : "".concat(inputValue);
      show(input);
      input.focus();
      instance.hideLoading();
    })["catch"](function (err) {
      error("Error in inputValue promise: ".concat(err));
      input.value = '';
      show(input);
      input.focus();
      instance.hideLoading();
    });
  };

  var populateInputOptions = {
    select: function select(content, inputOptions, params) {
      var select = getChildByClass(content, swalClasses.select);

      var renderOption = function renderOption(parent, optionLabel, optionValue) {
        var option = document.createElement('option');
        option.value = optionValue;
        setInnerHtml(option, optionLabel);
        option.selected = isSelected(optionValue, params.inputValue);
        parent.appendChild(option);
      };

      inputOptions.forEach(function (inputOption) {
        var optionValue = inputOption[0];
        var optionLabel = inputOption[1]; // <optgroup> spec:
        // https://www.w3.org/TR/html401/interact/forms.html#h-17.6
        // "...all OPTGROUP elements must be specified directly within a SELECT element (i.e., groups may not be nested)..."
        // check whether this is a <optgroup>

        if (Array.isArray(optionLabel)) {
          // if it is an array, then it is an <optgroup>
          var optgroup = document.createElement('optgroup');
          optgroup.label = optionValue;
          optgroup.disabled = false; // not configurable for now

          select.appendChild(optgroup);
          optionLabel.forEach(function (o) {
            return renderOption(optgroup, o[1], o[0]);
          });
        } else {
          // case of <option>
          renderOption(select, optionLabel, optionValue);
        }
      });
      select.focus();
    },
    radio: function radio(content, inputOptions, params) {
      var radio = getChildByClass(content, swalClasses.radio);
      inputOptions.forEach(function (inputOption) {
        var radioValue = inputOption[0];
        var radioLabel = inputOption[1];
        var radioInput = document.createElement('input');
        var radioLabelElement = document.createElement('label');
        radioInput.type = 'radio';
        radioInput.name = swalClasses.radio;
        radioInput.value = radioValue;

        if (isSelected(radioValue, params.inputValue)) {
          radioInput.checked = true;
        }

        var label = document.createElement('span');
        setInnerHtml(label, radioLabel);
        label.className = swalClasses.label;
        radioLabelElement.appendChild(radioInput);
        radioLabelElement.appendChild(label);
        radio.appendChild(radioLabelElement);
      });
      var radios = radio.querySelectorAll('input');

      if (radios.length) {
        radios[0].focus();
      }
    }
  };
  /**
   * Converts `inputOptions` into an array of `[value, label]`s
   * @param inputOptions
   */

  var formatInputOptions = function formatInputOptions(inputOptions) {
    var result = [];

    if (typeof Map !== 'undefined' && inputOptions instanceof Map) {
      inputOptions.forEach(function (value, key) {
        var valueFormatted = value;

        if (_typeof(valueFormatted) === 'object') {
          // case of <optgroup>
          valueFormatted = formatInputOptions(valueFormatted);
        }

        result.push([key, valueFormatted]);
      });
    } else {
      Object.keys(inputOptions).forEach(function (key) {
        var valueFormatted = inputOptions[key];

        if (_typeof(valueFormatted) === 'object') {
          // case of <optgroup>
          valueFormatted = formatInputOptions(valueFormatted);
        }

        result.push([key, valueFormatted]);
      });
    }

    return result;
  };

  var isSelected = function isSelected(optionValue, inputValue) {
    return inputValue && inputValue.toString() === optionValue.toString();
  };

  var handleConfirmButtonClick = function handleConfirmButtonClick(instance, innerParams) {
    instance.disableButtons();

    if (innerParams.input) {
      handleConfirmOrDenyWithInput(instance, innerParams, 'confirm');
    } else {
      confirm(instance, innerParams, true);
    }
  };
  var handleDenyButtonClick = function handleDenyButtonClick(instance, innerParams) {
    instance.disableButtons();

    if (innerParams.returnInputValueOnDeny) {
      handleConfirmOrDenyWithInput(instance, innerParams, 'deny');
    } else {
      deny(instance, innerParams, false);
    }
  };
  var handleCancelButtonClick = function handleCancelButtonClick(instance, dismissWith) {
    instance.disableButtons();
    dismissWith(DismissReason.cancel);
  };

  var handleConfirmOrDenyWithInput = function handleConfirmOrDenyWithInput(instance, innerParams, type
  /* type is either 'confirm' or 'deny' */
  ) {
    var inputValue = getInputValue(instance, innerParams);

    if (innerParams.inputValidator) {
      handleInputValidator(instance, innerParams, inputValue);
    } else if (!instance.getInput().checkValidity()) {
      instance.enableButtons();
      instance.showValidationMessage(innerParams.validationMessage);
    } else if (type === 'deny') {
      deny(instance, innerParams, inputValue);
    } else {
      confirm(instance, innerParams, inputValue);
    }
  };

  var handleInputValidator = function handleInputValidator(instance, innerParams, inputValue) {
    instance.disableInput();
    var validationPromise = Promise.resolve().then(function () {
      return asPromise(innerParams.inputValidator(inputValue, innerParams.validationMessage));
    });
    validationPromise.then(function (validationMessage) {
      instance.enableButtons();
      instance.enableInput();

      if (validationMessage) {
        instance.showValidationMessage(validationMessage);
      } else {
        confirm(instance, innerParams, inputValue);
      }
    });
  };

  var deny = function deny(instance, innerParams, value) {
    if (innerParams.showLoaderOnDeny) {
      showLoading(getDenyButton());
    }

    if (innerParams.preDeny) {
      var preDenyPromise = Promise.resolve().then(function () {
        return asPromise(innerParams.preDeny(value, innerParams.validationMessage));
      });
      preDenyPromise.then(function (preDenyValue) {
        if (preDenyValue === false) {
          instance.hideLoading();
        } else {
          instance.closePopup({
            isDenied: true,
            value: typeof preDenyValue === 'undefined' ? value : preDenyValue
          });
        }
      });
    } else {
      instance.closePopup({
        isDenied: true,
        value: value
      });
    }
  };

  var succeedWith = function succeedWith(instance, value) {
    instance.closePopup({
      isConfirmed: true,
      value: value
    });
  };

  var confirm = function confirm(instance, innerParams, value) {
    if (innerParams.showLoaderOnConfirm) {
      showLoading(); // TODO: make showLoading an *instance* method
    }

    if (innerParams.preConfirm) {
      instance.resetValidationMessage();
      var preConfirmPromise = Promise.resolve().then(function () {
        return asPromise(innerParams.preConfirm(value, innerParams.validationMessage));
      });
      preConfirmPromise.then(function (preConfirmValue) {
        if (isVisible(getValidationMessage()) || preConfirmValue === false) {
          instance.hideLoading();
        } else {
          succeedWith(instance, typeof preConfirmValue === 'undefined' ? value : preConfirmValue);
        }
      });
    } else {
      succeedWith(instance, value);
    }
  };

  var addKeydownHandler = function addKeydownHandler(instance, globalState, innerParams, dismissWith) {
    if (globalState.keydownTarget && globalState.keydownHandlerAdded) {
      globalState.keydownTarget.removeEventListener('keydown', globalState.keydownHandler, {
        capture: globalState.keydownListenerCapture
      });
      globalState.keydownHandlerAdded = false;
    }

    if (!innerParams.toast) {
      globalState.keydownHandler = function (e) {
        return keydownHandler(instance, e, dismissWith);
      };

      globalState.keydownTarget = innerParams.keydownListenerCapture ? window : getPopup();
      globalState.keydownListenerCapture = innerParams.keydownListenerCapture;
      globalState.keydownTarget.addEventListener('keydown', globalState.keydownHandler, {
        capture: globalState.keydownListenerCapture
      });
      globalState.keydownHandlerAdded = true;
    }
  }; // Focus handling

  var setFocus = function setFocus(innerParams, index, increment) {
    var focusableElements = getFocusableElements(); // search for visible elements and select the next possible match

    if (focusableElements.length) {
      index = index + increment; // rollover to first item

      if (index === focusableElements.length) {
        index = 0; // go to last item
      } else if (index === -1) {
        index = focusableElements.length - 1;
      }

      return focusableElements[index].focus();
    } // no visible focusable elements, focus the popup


    getPopup().focus();
  };
  var arrowKeysNextButton = ['ArrowRight', 'ArrowDown', 'Right', 'Down' // IE11
  ];
  var arrowKeysPreviousButton = ['ArrowLeft', 'ArrowUp', 'Left', 'Up' // IE11
  ];
  var escKeys = ['Escape', 'Esc' // IE11
  ];

  var keydownHandler = function keydownHandler(instance, e, dismissWith) {
    var innerParams = privateProps.innerParams.get(instance);

    if (!innerParams) {
      return; // This instance has already been destroyed
    }

    if (innerParams.stopKeydownPropagation) {
      e.stopPropagation();
    } // ENTER


    if (e.key === 'Enter') {
      handleEnter(instance, e, innerParams); // TAB
    } else if (e.key === 'Tab') {
      handleTab(e, innerParams); // ARROWS - switch focus between buttons
    } else if ([].concat(arrowKeysNextButton, arrowKeysPreviousButton).indexOf(e.key) !== -1) {
      handleArrows(e.key); // ESC
    } else if (escKeys.indexOf(e.key) !== -1) {
      handleEsc(e, innerParams, dismissWith);
    }
  };

  var handleEnter = function handleEnter(instance, e, innerParams) {
    // #720 #721
    if (e.isComposing) {
      return;
    }

    if (e.target && instance.getInput() && e.target.outerHTML === instance.getInput().outerHTML) {
      if (['textarea', 'file'].indexOf(innerParams.input) !== -1) {
        return; // do not submit
      }

      clickConfirm();
      e.preventDefault();
    }
  };

  var handleTab = function handleTab(e, innerParams) {
    var targetElement = e.target;
    var focusableElements = getFocusableElements();
    var btnIndex = -1;

    for (var i = 0; i < focusableElements.length; i++) {
      if (targetElement === focusableElements[i]) {
        btnIndex = i;
        break;
      }
    }

    if (!e.shiftKey) {
      // Cycle to the next button
      setFocus(innerParams, btnIndex, 1);
    } else {
      // Cycle to the prev button
      setFocus(innerParams, btnIndex, -1);
    }

    e.stopPropagation();
    e.preventDefault();
  };

  var handleArrows = function handleArrows(key) {
    var confirmButton = getConfirmButton();
    var denyButton = getDenyButton();
    var cancelButton = getCancelButton();

    if (!([confirmButton, denyButton, cancelButton].indexOf(document.activeElement) !== -1)) {
      return;
    }

    var sibling = arrowKeysNextButton.indexOf(key) !== -1 ? 'nextElementSibling' : 'previousElementSibling';
    var buttonToFocus = document.activeElement[sibling];

    if (buttonToFocus) {
      buttonToFocus.focus();
    }
  };

  var handleEsc = function handleEsc(e, innerParams, dismissWith) {
    if (callIfFunction(innerParams.allowEscapeKey)) {
      e.preventDefault();
      dismissWith(DismissReason.esc);
    }
  };

  var handlePopupClick = function handlePopupClick(instance, domCache, dismissWith) {
    var innerParams = privateProps.innerParams.get(instance);

    if (innerParams.toast) {
      handleToastClick(instance, domCache, dismissWith);
    } else {
      // Ignore click events that had mousedown on the popup but mouseup on the container
      // This can happen when the user drags a slider
      handleModalMousedown(domCache); // Ignore click events that had mousedown on the container but mouseup on the popup

      handleContainerMousedown(domCache);
      handleModalClick(instance, domCache, dismissWith);
    }
  };

  var handleToastClick = function handleToastClick(instance, domCache, dismissWith) {
    // Closing toast by internal click
    domCache.popup.onclick = function () {
      var innerParams = privateProps.innerParams.get(instance);

      if (innerParams.showConfirmButton || innerParams.showDenyButton || innerParams.showCancelButton || innerParams.showCloseButton || innerParams.timer || innerParams.input) {
        return;
      }

      dismissWith(DismissReason.close);
    };
  };

  var ignoreOutsideClick = false;

  var handleModalMousedown = function handleModalMousedown(domCache) {
    domCache.popup.onmousedown = function () {
      domCache.container.onmouseup = function (e) {
        domCache.container.onmouseup = undefined; // We only check if the mouseup target is the container because usually it doesn't
        // have any other direct children aside of the popup

        if (e.target === domCache.container) {
          ignoreOutsideClick = true;
        }
      };
    };
  };

  var handleContainerMousedown = function handleContainerMousedown(domCache) {
    domCache.container.onmousedown = function () {
      domCache.popup.onmouseup = function (e) {
        domCache.popup.onmouseup = undefined; // We also need to check if the mouseup target is a child of the popup

        if (e.target === domCache.popup || domCache.popup.contains(e.target)) {
          ignoreOutsideClick = true;
        }
      };
    };
  };

  var handleModalClick = function handleModalClick(instance, domCache, dismissWith) {
    domCache.container.onclick = function (e) {
      var innerParams = privateProps.innerParams.get(instance);

      if (ignoreOutsideClick) {
        ignoreOutsideClick = false;
        return;
      }

      if (e.target === domCache.container && callIfFunction(innerParams.allowOutsideClick)) {
        dismissWith(DismissReason.backdrop);
      }
    };
  };

  function _main(userParams) {
    var mixinParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    showWarningsForParams(_extends({}, mixinParams, userParams));

    if (globalState.currentInstance) {
      globalState.currentInstance._destroy();
    }

    globalState.currentInstance = this;
    var innerParams = prepareParams(userParams, mixinParams);
    setParameters(innerParams);
    Object.freeze(innerParams); // clear the previous timer

    if (globalState.timeout) {
      globalState.timeout.stop();
      delete globalState.timeout;
    } // clear the restore focus timeout


    clearTimeout(globalState.restoreFocusTimeout);
    var domCache = populateDomCache(this);
    render(this, innerParams);
    privateProps.innerParams.set(this, innerParams);
    return swalPromise(this, domCache, innerParams);
  }

  var prepareParams = function prepareParams(userParams, mixinParams) {
    var templateParams = getTemplateParams(userParams);

    var params = _extends({}, defaultParams, mixinParams, templateParams, userParams); // precedence is described in #2131


    params.showClass = _extends({}, defaultParams.showClass, params.showClass);
    params.hideClass = _extends({}, defaultParams.hideClass, params.hideClass); // @deprecated

    if (userParams.animation === false) {
      params.showClass = {
        popup: 'swal2-noanimation',
        backdrop: 'swal2-noanimation'
      };
      params.hideClass = {};
    }

    return params;
  };

  var swalPromise = function swalPromise(instance, domCache, innerParams) {
    return new Promise(function (resolve) {
      // functions to handle all closings/dismissals
      var dismissWith = function dismissWith(dismiss) {
        instance.closePopup({
          isDismissed: true,
          dismiss: dismiss
        });
      };

      privateMethods.swalPromiseResolve.set(instance, resolve);

      domCache.confirmButton.onclick = function () {
        return handleConfirmButtonClick(instance, innerParams);
      };

      domCache.denyButton.onclick = function () {
        return handleDenyButtonClick(instance, innerParams);
      };

      domCache.cancelButton.onclick = function () {
        return handleCancelButtonClick(instance, dismissWith);
      };

      domCache.closeButton.onclick = function () {
        return dismissWith(DismissReason.close);
      };

      handlePopupClick(instance, domCache, dismissWith);
      addKeydownHandler(instance, globalState, innerParams, dismissWith);
      handleInputOptionsAndValue(instance, innerParams);
      openPopup(innerParams);
      setupTimer(globalState, innerParams, dismissWith);
      initFocus(domCache, innerParams); // Scroll container to top on open (#1247, #1946)

      setTimeout(function () {
        domCache.container.scrollTop = 0;
      });
    });
  };

  var populateDomCache = function populateDomCache(instance) {
    var domCache = {
      popup: getPopup(),
      container: getContainer(),
      content: getContent(),
      actions: getActions(),
      confirmButton: getConfirmButton(),
      denyButton: getDenyButton(),
      cancelButton: getCancelButton(),
      loader: getLoader(),
      closeButton: getCloseButton(),
      validationMessage: getValidationMessage(),
      progressSteps: getProgressSteps()
    };
    privateProps.domCache.set(instance, domCache);
    return domCache;
  };

  var setupTimer = function setupTimer(globalState$$1, innerParams, dismissWith) {
    var timerProgressBar = getTimerProgressBar();
    hide(timerProgressBar);

    if (innerParams.timer) {
      globalState$$1.timeout = new Timer(function () {
        dismissWith('timer');
        delete globalState$$1.timeout;
      }, innerParams.timer);

      if (innerParams.timerProgressBar) {
        show(timerProgressBar);
        setTimeout(function () {
          if (globalState$$1.timeout && globalState$$1.timeout.running) {
            // timer can be already stopped or unset at this point
            animateTimerProgressBar(innerParams.timer);
          }
        });
      }
    }
  };

  var initFocus = function initFocus(domCache, innerParams) {
    if (innerParams.toast) {
      return;
    }

    if (!callIfFunction(innerParams.allowEnterKey)) {
      return blurActiveElement();
    }

    if (!focusButton(domCache, innerParams)) {
      setFocus(innerParams, -1, 1);
    }
  };

  var focusButton = function focusButton(domCache, innerParams) {
    if (innerParams.focusDeny && isVisible(domCache.denyButton)) {
      domCache.denyButton.focus();
      return true;
    }

    if (innerParams.focusCancel && isVisible(domCache.cancelButton)) {
      domCache.cancelButton.focus();
      return true;
    }

    if (innerParams.focusConfirm && isVisible(domCache.confirmButton)) {
      domCache.confirmButton.focus();
      return true;
    }

    return false;
  };

  var blurActiveElement = function blurActiveElement() {
    if (document.activeElement && typeof document.activeElement.blur === 'function') {
      document.activeElement.blur();
    }
  };

  /**
   * Updates popup parameters.
   */

  function update(params) {
    var popup = getPopup();
    var innerParams = privateProps.innerParams.get(this);

    if (!popup || hasClass(popup, innerParams.hideClass.popup)) {
      return warn("You're trying to update the closed or closing popup, that won't work. Use the update() method in preConfirm parameter or show a new popup.");
    }

    var validUpdatableParams = {}; // assign valid params from `params` to `defaults`

    Object.keys(params).forEach(function (param) {
      if (Swal.isUpdatableParameter(param)) {
        validUpdatableParams[param] = params[param];
      } else {
        warn("Invalid parameter to update: \"".concat(param, "\". Updatable params are listed here: https://github.com/sweetalert2/sweetalert2/blob/master/src/utils/params.js\n\nIf you think this parameter should be updatable, request it here: https://github.com/sweetalert2/sweetalert2/issues/new?template=02_feature_request.md"));
      }
    });

    var updatedParams = _extends({}, innerParams, validUpdatableParams);

    render(this, updatedParams);
    privateProps.innerParams.set(this, updatedParams);
    Object.defineProperties(this, {
      params: {
        value: _extends({}, this.params, params),
        writable: false,
        enumerable: true
      }
    });
  }

  function _destroy() {
    var domCache = privateProps.domCache.get(this);
    var innerParams = privateProps.innerParams.get(this);

    if (!innerParams) {
      return; // This instance has already been destroyed
    } // Check if there is another Swal closing


    if (domCache.popup && globalState.swalCloseEventFinishedCallback) {
      globalState.swalCloseEventFinishedCallback();
      delete globalState.swalCloseEventFinishedCallback;
    } // Check if there is a swal disposal defer timer


    if (globalState.deferDisposalTimer) {
      clearTimeout(globalState.deferDisposalTimer);
      delete globalState.deferDisposalTimer;
    }

    runDidDestroy(innerParams);
    disposeSwal(this);
  }

  var runDidDestroy = function runDidDestroy(innerParams) {
    if (typeof innerParams.didDestroy === 'function') {
      innerParams.didDestroy();
    } else if (typeof innerParams.onDestroy === 'function') {
      innerParams.onDestroy(); // @deprecated
    }
  };

  var disposeSwal = function disposeSwal(instance) {
    // Unset this.params so GC will dispose it (#1569)
    delete instance.params; // Unset globalState props so GC will dispose globalState (#1569)

    delete globalState.keydownHandler;
    delete globalState.keydownTarget; // Unset WeakMaps so GC will be able to dispose them (#1569)

    unsetWeakMaps(privateProps);
    unsetWeakMaps(privateMethods);
  };

  var unsetWeakMaps = function unsetWeakMaps(obj) {
    for (var i in obj) {
      obj[i] = new WeakMap();
    }
  };



  var instanceMethods = /*#__PURE__*/Object.freeze({
    hideLoading: hideLoading,
    disableLoading: hideLoading,
    getInput: getInput$1,
    close: close,
    closePopup: close,
    closeModal: close,
    closeToast: close,
    enableButtons: enableButtons,
    disableButtons: disableButtons,
    enableInput: enableInput,
    disableInput: disableInput,
    showValidationMessage: showValidationMessage,
    resetValidationMessage: resetValidationMessage$1,
    getProgressSteps: getProgressSteps$1,
    _main: _main,
    update: update,
    _destroy: _destroy
  });

  var currentInstance;

  var SweetAlert = /*#__PURE__*/function () {
    function SweetAlert() {
      _classCallCheck(this, SweetAlert);

      // Prevent run in Node env
      if (typeof window === 'undefined') {
        return;
      } // Check for the existence of Promise


      if (typeof Promise === 'undefined') {
        error('This package requires a Promise library, please include a shim to enable it in this browser (See: https://github.com/sweetalert2/sweetalert2/wiki/Migration-from-SweetAlert-to-SweetAlert2#1-ie-support)');
      }

      currentInstance = this;

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      var outerParams = Object.freeze(this.constructor.argsToParams(args));
      Object.defineProperties(this, {
        params: {
          value: outerParams,
          writable: false,
          enumerable: true,
          configurable: true
        }
      });

      var promise = this._main(this.params);

      privateProps.promise.set(this, promise);
    } // `catch` cannot be the name of a module export, so we define our thenable methods here instead


    _createClass(SweetAlert, [{
      key: "then",
      value: function then(onFulfilled) {
        var promise = privateProps.promise.get(this);
        return promise.then(onFulfilled);
      }
    }, {
      key: "finally",
      value: function _finally(onFinally) {
        var promise = privateProps.promise.get(this);
        return promise["finally"](onFinally);
      }
    }]);

    return SweetAlert;
  }(); // Assign instance methods from src/instanceMethods/*.js to prototype


  _extends(SweetAlert.prototype, instanceMethods); // Assign static methods from src/staticMethods/*.js to constructor


  _extends(SweetAlert, staticMethods); // Proxy to instance methods to constructor, for now, for backwards compatibility


  Object.keys(instanceMethods).forEach(function (key) {
    SweetAlert[key] = function () {
      if (currentInstance) {
        var _currentInstance;

        return (_currentInstance = currentInstance)[key].apply(_currentInstance, arguments);
      }
    };
  });
  SweetAlert.DismissReason = DismissReason;
  SweetAlert.version = '10.16.9';

  var Swal = SweetAlert;
  Swal["default"] = Swal;

  return Swal;

}));
if (typeof this !== 'undefined' && this.Sweetalert2){  this.swal = this.sweetAlert = this.Swal = this.SweetAlert = this.Sweetalert2}

"undefined"!=typeof document&&function(e,t){var n=e.createElement("style");if(e.getElementsByTagName("head")[0].appendChild(n),n.styleSheet)n.styleSheet.disabled||(n.styleSheet.cssText=t);else try{n.innerHTML=t}catch(e){n.innerText=t}}(document,".swal2-popup.swal2-toast{flex-direction:column;align-items:stretch;width:auto;padding:1.25em;overflow-y:hidden;background:#fff;box-shadow:0 0 .625em #d9d9d9}.swal2-popup.swal2-toast .swal2-header{flex-direction:row;padding:0}.swal2-popup.swal2-toast .swal2-title{flex-grow:1;justify-content:flex-start;margin:0 .625em;font-size:1em}.swal2-popup.swal2-toast .swal2-loading{justify-content:center}.swal2-popup.swal2-toast .swal2-input{height:2em;margin:.3125em auto;font-size:1em}.swal2-popup.swal2-toast .swal2-validation-message{font-size:1em}.swal2-popup.swal2-toast .swal2-footer{margin:.5em 0 0;padding:.5em 0 0;font-size:.8em}.swal2-popup.swal2-toast .swal2-close{position:static;width:.8em;height:.8em;line-height:.8}.swal2-popup.swal2-toast .swal2-content{justify-content:flex-start;margin:0 .625em;padding:0;font-size:1em;text-align:initial}.swal2-popup.swal2-toast .swal2-html-container{padding:.625em 0 0}.swal2-popup.swal2-toast .swal2-html-container:empty{padding:0}.swal2-popup.swal2-toast .swal2-icon{width:2em;min-width:2em;height:2em;margin:0 .5em 0 0}.swal2-popup.swal2-toast .swal2-icon .swal2-icon-content{display:flex;align-items:center;font-size:1.8em;font-weight:700}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){.swal2-popup.swal2-toast .swal2-icon .swal2-icon-content{font-size:.25em}}.swal2-popup.swal2-toast .swal2-icon.swal2-success .swal2-success-ring{width:2em;height:2em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line]{top:.875em;width:1.375em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=left]{left:.3125em}.swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=right]{right:.3125em}.swal2-popup.swal2-toast .swal2-actions{flex:1;flex-basis:auto!important;align-self:stretch;width:auto;height:2.2em;height:auto;margin:0 .3125em;margin-top:.3125em;padding:0}.swal2-popup.swal2-toast .swal2-styled{margin:.125em .3125em;padding:.3125em .625em;font-size:1em}.swal2-popup.swal2-toast .swal2-styled:focus{box-shadow:0 0 0 1px #fff,0 0 0 3px rgba(100,150,200,.5)}.swal2-popup.swal2-toast .swal2-success{border-color:#a5dc86}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line]{position:absolute;width:1.6em;height:3em;transform:rotate(45deg);border-radius:50%}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=left]{top:-.8em;left:-.5em;transform:rotate(-45deg);transform-origin:2em 2em;border-radius:4em 0 0 4em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=right]{top:-.25em;left:.9375em;transform-origin:0 1.5em;border-radius:0 4em 4em 0}.swal2-popup.swal2-toast .swal2-success .swal2-success-ring{width:2em;height:2em}.swal2-popup.swal2-toast .swal2-success .swal2-success-fix{top:0;left:.4375em;width:.4375em;height:2.6875em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line]{height:.3125em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=tip]{top:1.125em;left:.1875em;width:.75em}.swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=long]{top:.9375em;right:.1875em;width:1.375em}.swal2-popup.swal2-toast .swal2-success.swal2-icon-show .swal2-success-line-tip{-webkit-animation:swal2-toast-animate-success-line-tip .75s;animation:swal2-toast-animate-success-line-tip .75s}.swal2-popup.swal2-toast .swal2-success.swal2-icon-show .swal2-success-line-long{-webkit-animation:swal2-toast-animate-success-line-long .75s;animation:swal2-toast-animate-success-line-long .75s}.swal2-popup.swal2-toast.swal2-show{-webkit-animation:swal2-toast-show .5s;animation:swal2-toast-show .5s}.swal2-popup.swal2-toast.swal2-hide{-webkit-animation:swal2-toast-hide .1s forwards;animation:swal2-toast-hide .1s forwards}.swal2-container{display:flex;position:fixed;z-index:1060;top:0;right:0;bottom:0;left:0;flex-direction:row;align-items:center;justify-content:center;padding:.625em;overflow-x:hidden;transition:background-color .1s;-webkit-overflow-scrolling:touch}.swal2-container.swal2-backdrop-show,.swal2-container.swal2-noanimation{background:rgba(0,0,0,.4)}.swal2-container.swal2-backdrop-hide{background:0 0!important}.swal2-container.swal2-top{align-items:flex-start}.swal2-container.swal2-top-left,.swal2-container.swal2-top-start{align-items:flex-start;justify-content:flex-start}.swal2-container.swal2-top-end,.swal2-container.swal2-top-right{align-items:flex-start;justify-content:flex-end}.swal2-container.swal2-center{align-items:center}.swal2-container.swal2-center-left,.swal2-container.swal2-center-start{align-items:center;justify-content:flex-start}.swal2-container.swal2-center-end,.swal2-container.swal2-center-right{align-items:center;justify-content:flex-end}.swal2-container.swal2-bottom{align-items:flex-end}.swal2-container.swal2-bottom-left,.swal2-container.swal2-bottom-start{align-items:flex-end;justify-content:flex-start}.swal2-container.swal2-bottom-end,.swal2-container.swal2-bottom-right{align-items:flex-end;justify-content:flex-end}.swal2-container.swal2-bottom-end>:first-child,.swal2-container.swal2-bottom-left>:first-child,.swal2-container.swal2-bottom-right>:first-child,.swal2-container.swal2-bottom-start>:first-child,.swal2-container.swal2-bottom>:first-child{margin-top:auto}.swal2-container.swal2-grow-fullscreen>.swal2-modal{display:flex!important;flex:1;align-self:stretch;justify-content:center}.swal2-container.swal2-grow-row>.swal2-modal{display:flex!important;flex:1;align-content:center;justify-content:center}.swal2-container.swal2-grow-column{flex:1;flex-direction:column}.swal2-container.swal2-grow-column.swal2-bottom,.swal2-container.swal2-grow-column.swal2-center,.swal2-container.swal2-grow-column.swal2-top{align-items:center}.swal2-container.swal2-grow-column.swal2-bottom-left,.swal2-container.swal2-grow-column.swal2-bottom-start,.swal2-container.swal2-grow-column.swal2-center-left,.swal2-container.swal2-grow-column.swal2-center-start,.swal2-container.swal2-grow-column.swal2-top-left,.swal2-container.swal2-grow-column.swal2-top-start{align-items:flex-start}.swal2-container.swal2-grow-column.swal2-bottom-end,.swal2-container.swal2-grow-column.swal2-bottom-right,.swal2-container.swal2-grow-column.swal2-center-end,.swal2-container.swal2-grow-column.swal2-center-right,.swal2-container.swal2-grow-column.swal2-top-end,.swal2-container.swal2-grow-column.swal2-top-right{align-items:flex-end}.swal2-container.swal2-grow-column>.swal2-modal{display:flex!important;flex:1;align-content:center;justify-content:center}.swal2-container.swal2-no-transition{transition:none!important}.swal2-container:not(.swal2-top):not(.swal2-top-start):not(.swal2-top-end):not(.swal2-top-left):not(.swal2-top-right):not(.swal2-center-start):not(.swal2-center-end):not(.swal2-center-left):not(.swal2-center-right):not(.swal2-bottom):not(.swal2-bottom-start):not(.swal2-bottom-end):not(.swal2-bottom-left):not(.swal2-bottom-right):not(.swal2-grow-fullscreen)>.swal2-modal{margin:auto}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){.swal2-container .swal2-modal{margin:0!important}}.swal2-popup{display:none;position:relative;box-sizing:border-box;flex-direction:column;justify-content:center;width:32em;max-width:100%;padding:1.25em;border:none;border-radius:5px;background:#fff;font-family:inherit;font-size:1rem}.swal2-popup:focus{outline:0}.swal2-popup.swal2-loading{overflow-y:hidden}.swal2-header{display:flex;flex-direction:column;align-items:center;padding:0 1.8em}.swal2-title{position:relative;max-width:100%;margin:0 0 .4em;padding:0;color:#595959;font-size:1.875em;font-weight:600;text-align:center;text-transform:none;word-wrap:break-word}.swal2-actions{display:flex;z-index:1;box-sizing:border-box;flex-wrap:wrap;align-items:center;justify-content:center;width:100%;margin:1.25em auto 0;padding:0}.swal2-actions:not(.swal2-loading) .swal2-styled[disabled]{opacity:.4}.swal2-actions:not(.swal2-loading) .swal2-styled:hover{background-image:linear-gradient(rgba(0,0,0,.1),rgba(0,0,0,.1))}.swal2-actions:not(.swal2-loading) .swal2-styled:active{background-image:linear-gradient(rgba(0,0,0,.2),rgba(0,0,0,.2))}.swal2-loader{display:none;align-items:center;justify-content:center;width:2.2em;height:2.2em;margin:0 1.875em;-webkit-animation:swal2-rotate-loading 1.5s linear 0s infinite normal;animation:swal2-rotate-loading 1.5s linear 0s infinite normal;border-width:.25em;border-style:solid;border-radius:100%;border-color:#2778c4 transparent #2778c4 transparent}.swal2-styled{margin:.3125em;padding:.625em 1.1em;box-shadow:none;font-weight:500}.swal2-styled:not([disabled]){cursor:pointer}.swal2-styled.swal2-confirm{border:0;border-radius:.25em;background:initial;background-color:#2778c4;color:#fff;font-size:1em}.swal2-styled.swal2-deny{border:0;border-radius:.25em;background:initial;background-color:#d14529;color:#fff;font-size:1em}.swal2-styled.swal2-cancel{border:0;border-radius:.25em;background:initial;background-color:#757575;color:#fff;font-size:1em}.swal2-styled:focus{outline:0;box-shadow:0 0 0 3px rgba(100,150,200,.5)}.swal2-styled::-moz-focus-inner{border:0}.swal2-footer{justify-content:center;margin:1.25em 0 0;padding:1em 0 0;border-top:1px solid #eee;color:#545454;font-size:1em}.swal2-timer-progress-bar-container{position:absolute;right:0;bottom:0;left:0;height:.25em;overflow:hidden;border-bottom-right-radius:5px;border-bottom-left-radius:5px}.swal2-timer-progress-bar{width:100%;height:.25em;background:rgba(0,0,0,.2)}.swal2-image{max-width:100%;margin:1.25em auto}.swal2-close{position:absolute;z-index:2;top:0;right:0;align-items:center;justify-content:center;width:1.2em;height:1.2em;padding:0;overflow:hidden;transition:color .1s ease-out;border:none;border-radius:5px;background:0 0;color:#ccc;font-family:serif;font-size:2.5em;line-height:1.2;cursor:pointer}.swal2-close:hover{transform:none;background:0 0;color:#f27474}.swal2-close:focus{outline:0;box-shadow:inset 0 0 0 3px rgba(100,150,200,.5)}.swal2-close::-moz-focus-inner{border:0}.swal2-content{z-index:1;justify-content:center;margin:0;padding:0 1.6em;color:#545454;font-size:1.125em;font-weight:400;line-height:normal;text-align:center;word-wrap:break-word}.swal2-checkbox,.swal2-file,.swal2-input,.swal2-radio,.swal2-select,.swal2-textarea{margin:1em auto}.swal2-file,.swal2-input,.swal2-textarea{box-sizing:border-box;width:100%;transition:border-color .3s,box-shadow .3s;border:1px solid #d9d9d9;border-radius:.1875em;background:inherit;box-shadow:inset 0 1px 1px rgba(0,0,0,.06);color:inherit;font-size:1.125em}.swal2-file.swal2-inputerror,.swal2-input.swal2-inputerror,.swal2-textarea.swal2-inputerror{border-color:#f27474!important;box-shadow:0 0 2px #f27474!important}.swal2-file:focus,.swal2-input:focus,.swal2-textarea:focus{border:1px solid #b4dbed;outline:0;box-shadow:0 0 0 3px rgba(100,150,200,.5)}.swal2-file::-moz-placeholder,.swal2-input::-moz-placeholder,.swal2-textarea::-moz-placeholder{color:#ccc}.swal2-file:-ms-input-placeholder,.swal2-input:-ms-input-placeholder,.swal2-textarea:-ms-input-placeholder{color:#ccc}.swal2-file::placeholder,.swal2-input::placeholder,.swal2-textarea::placeholder{color:#ccc}.swal2-range{margin:1em auto;background:#fff}.swal2-range input{width:80%}.swal2-range output{width:20%;color:inherit;font-weight:600;text-align:center}.swal2-range input,.swal2-range output{height:2.625em;padding:0;font-size:1.125em;line-height:2.625em}.swal2-input{height:2.625em;padding:0 .75em}.swal2-input[type=number]{max-width:10em}.swal2-file{background:inherit;font-size:1.125em}.swal2-textarea{height:6.75em;padding:.75em}.swal2-select{min-width:50%;max-width:100%;padding:.375em .625em;background:inherit;color:inherit;font-size:1.125em}.swal2-checkbox,.swal2-radio{align-items:center;justify-content:center;background:#fff;color:inherit}.swal2-checkbox label,.swal2-radio label{margin:0 .6em;font-size:1.125em}.swal2-checkbox input,.swal2-radio input{flex-shrink:0;margin:0 .4em}.swal2-input-label{display:flex;justify-content:center;margin:1em auto}.swal2-validation-message{align-items:center;justify-content:center;margin:0 -2.7em;padding:.625em;overflow:hidden;background:#f0f0f0;color:#666;font-size:1em;font-weight:300}.swal2-validation-message::before{content:\"!\";display:inline-block;width:1.5em;min-width:1.5em;height:1.5em;margin:0 .625em;border-radius:50%;background-color:#f27474;color:#fff;font-weight:600;line-height:1.5em;text-align:center}.swal2-icon{position:relative;box-sizing:content-box;justify-content:center;width:5em;height:5em;margin:1.25em auto 1.875em;border:.25em solid transparent;border-radius:50%;border-color:#000;font-family:inherit;line-height:5em;cursor:default;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.swal2-icon .swal2-icon-content{display:flex;align-items:center;font-size:3.75em}.swal2-icon.swal2-error{border-color:#f27474;color:#f27474}.swal2-icon.swal2-error .swal2-x-mark{position:relative;flex-grow:1}.swal2-icon.swal2-error [class^=swal2-x-mark-line]{display:block;position:absolute;top:2.3125em;width:2.9375em;height:.3125em;border-radius:.125em;background-color:#f27474}.swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=left]{left:1.0625em;transform:rotate(45deg)}.swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=right]{right:1em;transform:rotate(-45deg)}.swal2-icon.swal2-error.swal2-icon-show{-webkit-animation:swal2-animate-error-icon .5s;animation:swal2-animate-error-icon .5s}.swal2-icon.swal2-error.swal2-icon-show .swal2-x-mark{-webkit-animation:swal2-animate-error-x-mark .5s;animation:swal2-animate-error-x-mark .5s}.swal2-icon.swal2-warning{border-color:#facea8;color:#f8bb86}.swal2-icon.swal2-info{border-color:#9de0f6;color:#3fc3ee}.swal2-icon.swal2-question{border-color:#c9dae1;color:#87adbd}.swal2-icon.swal2-success{border-color:#a5dc86;color:#a5dc86}.swal2-icon.swal2-success [class^=swal2-success-circular-line]{position:absolute;width:3.75em;height:7.5em;transform:rotate(45deg);border-radius:50%}.swal2-icon.swal2-success [class^=swal2-success-circular-line][class$=left]{top:-.4375em;left:-2.0635em;transform:rotate(-45deg);transform-origin:3.75em 3.75em;border-radius:7.5em 0 0 7.5em}.swal2-icon.swal2-success [class^=swal2-success-circular-line][class$=right]{top:-.6875em;left:1.875em;transform:rotate(-45deg);transform-origin:0 3.75em;border-radius:0 7.5em 7.5em 0}.swal2-icon.swal2-success .swal2-success-ring{position:absolute;z-index:2;top:-.25em;left:-.25em;box-sizing:content-box;width:100%;height:100%;border:.25em solid rgba(165,220,134,.3);border-radius:50%}.swal2-icon.swal2-success .swal2-success-fix{position:absolute;z-index:1;top:.5em;left:1.625em;width:.4375em;height:5.625em;transform:rotate(-45deg)}.swal2-icon.swal2-success [class^=swal2-success-line]{display:block;position:absolute;z-index:2;height:.3125em;border-radius:.125em;background-color:#a5dc86}.swal2-icon.swal2-success [class^=swal2-success-line][class$=tip]{top:2.875em;left:.8125em;width:1.5625em;transform:rotate(45deg)}.swal2-icon.swal2-success [class^=swal2-success-line][class$=long]{top:2.375em;right:.5em;width:2.9375em;transform:rotate(-45deg)}.swal2-icon.swal2-success.swal2-icon-show .swal2-success-line-tip{-webkit-animation:swal2-animate-success-line-tip .75s;animation:swal2-animate-success-line-tip .75s}.swal2-icon.swal2-success.swal2-icon-show .swal2-success-line-long{-webkit-animation:swal2-animate-success-line-long .75s;animation:swal2-animate-success-line-long .75s}.swal2-icon.swal2-success.swal2-icon-show .swal2-success-circular-line-right{-webkit-animation:swal2-rotate-success-circular-line 4.25s ease-in;animation:swal2-rotate-success-circular-line 4.25s ease-in}.swal2-progress-steps{flex-wrap:wrap;align-items:center;max-width:100%;margin:0 0 1.25em;padding:0;background:inherit;font-weight:600}.swal2-progress-steps li{display:inline-block;position:relative}.swal2-progress-steps .swal2-progress-step{z-index:20;flex-shrink:0;width:2em;height:2em;border-radius:2em;background:#2778c4;color:#fff;line-height:2em;text-align:center}.swal2-progress-steps .swal2-progress-step.swal2-active-progress-step{background:#2778c4}.swal2-progress-steps .swal2-progress-step.swal2-active-progress-step~.swal2-progress-step{background:#add8e6;color:#fff}.swal2-progress-steps .swal2-progress-step.swal2-active-progress-step~.swal2-progress-step-line{background:#add8e6}.swal2-progress-steps .swal2-progress-step-line{z-index:10;flex-shrink:0;width:2.5em;height:.4em;margin:0 -1px;background:#2778c4}[class^=swal2]{-webkit-tap-highlight-color:transparent}.swal2-show{-webkit-animation:swal2-show .3s;animation:swal2-show .3s}.swal2-hide{-webkit-animation:swal2-hide .15s forwards;animation:swal2-hide .15s forwards}.swal2-noanimation{transition:none}.swal2-scrollbar-measure{position:absolute;top:-9999px;width:50px;height:50px;overflow:scroll}.swal2-rtl .swal2-close{right:auto;left:0}.swal2-rtl .swal2-timer-progress-bar{right:0;left:auto}@supports (-ms-accelerator:true){.swal2-range input{width:100%!important}.swal2-range output{display:none}}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){.swal2-range input{width:100%!important}.swal2-range output{display:none}}@-webkit-keyframes swal2-toast-show{0%{transform:translateY(-.625em) rotateZ(2deg)}33%{transform:translateY(0) rotateZ(-2deg)}66%{transform:translateY(.3125em) rotateZ(2deg)}100%{transform:translateY(0) rotateZ(0)}}@keyframes swal2-toast-show{0%{transform:translateY(-.625em) rotateZ(2deg)}33%{transform:translateY(0) rotateZ(-2deg)}66%{transform:translateY(.3125em) rotateZ(2deg)}100%{transform:translateY(0) rotateZ(0)}}@-webkit-keyframes swal2-toast-hide{100%{transform:rotateZ(1deg);opacity:0}}@keyframes swal2-toast-hide{100%{transform:rotateZ(1deg);opacity:0}}@-webkit-keyframes swal2-toast-animate-success-line-tip{0%{top:.5625em;left:.0625em;width:0}54%{top:.125em;left:.125em;width:0}70%{top:.625em;left:-.25em;width:1.625em}84%{top:1.0625em;left:.75em;width:.5em}100%{top:1.125em;left:.1875em;width:.75em}}@keyframes swal2-toast-animate-success-line-tip{0%{top:.5625em;left:.0625em;width:0}54%{top:.125em;left:.125em;width:0}70%{top:.625em;left:-.25em;width:1.625em}84%{top:1.0625em;left:.75em;width:.5em}100%{top:1.125em;left:.1875em;width:.75em}}@-webkit-keyframes swal2-toast-animate-success-line-long{0%{top:1.625em;right:1.375em;width:0}65%{top:1.25em;right:.9375em;width:0}84%{top:.9375em;right:0;width:1.125em}100%{top:.9375em;right:.1875em;width:1.375em}}@keyframes swal2-toast-animate-success-line-long{0%{top:1.625em;right:1.375em;width:0}65%{top:1.25em;right:.9375em;width:0}84%{top:.9375em;right:0;width:1.125em}100%{top:.9375em;right:.1875em;width:1.375em}}@-webkit-keyframes swal2-show{0%{transform:scale(.7)}45%{transform:scale(1.05)}80%{transform:scale(.95)}100%{transform:scale(1)}}@keyframes swal2-show{0%{transform:scale(.7)}45%{transform:scale(1.05)}80%{transform:scale(.95)}100%{transform:scale(1)}}@-webkit-keyframes swal2-hide{0%{transform:scale(1);opacity:1}100%{transform:scale(.5);opacity:0}}@keyframes swal2-hide{0%{transform:scale(1);opacity:1}100%{transform:scale(.5);opacity:0}}@-webkit-keyframes swal2-animate-success-line-tip{0%{top:1.1875em;left:.0625em;width:0}54%{top:1.0625em;left:.125em;width:0}70%{top:2.1875em;left:-.375em;width:3.125em}84%{top:3em;left:1.3125em;width:1.0625em}100%{top:2.8125em;left:.8125em;width:1.5625em}}@keyframes swal2-animate-success-line-tip{0%{top:1.1875em;left:.0625em;width:0}54%{top:1.0625em;left:.125em;width:0}70%{top:2.1875em;left:-.375em;width:3.125em}84%{top:3em;left:1.3125em;width:1.0625em}100%{top:2.8125em;left:.8125em;width:1.5625em}}@-webkit-keyframes swal2-animate-success-line-long{0%{top:3.375em;right:2.875em;width:0}65%{top:3.375em;right:2.875em;width:0}84%{top:2.1875em;right:0;width:3.4375em}100%{top:2.375em;right:.5em;width:2.9375em}}@keyframes swal2-animate-success-line-long{0%{top:3.375em;right:2.875em;width:0}65%{top:3.375em;right:2.875em;width:0}84%{top:2.1875em;right:0;width:3.4375em}100%{top:2.375em;right:.5em;width:2.9375em}}@-webkit-keyframes swal2-rotate-success-circular-line{0%{transform:rotate(-45deg)}5%{transform:rotate(-45deg)}12%{transform:rotate(-405deg)}100%{transform:rotate(-405deg)}}@keyframes swal2-rotate-success-circular-line{0%{transform:rotate(-45deg)}5%{transform:rotate(-45deg)}12%{transform:rotate(-405deg)}100%{transform:rotate(-405deg)}}@-webkit-keyframes swal2-animate-error-x-mark{0%{margin-top:1.625em;transform:scale(.4);opacity:0}50%{margin-top:1.625em;transform:scale(.4);opacity:0}80%{margin-top:-.375em;transform:scale(1.15)}100%{margin-top:0;transform:scale(1);opacity:1}}@keyframes swal2-animate-error-x-mark{0%{margin-top:1.625em;transform:scale(.4);opacity:0}50%{margin-top:1.625em;transform:scale(.4);opacity:0}80%{margin-top:-.375em;transform:scale(1.15)}100%{margin-top:0;transform:scale(1);opacity:1}}@-webkit-keyframes swal2-animate-error-icon{0%{transform:rotateX(100deg);opacity:0}100%{transform:rotateX(0);opacity:1}}@keyframes swal2-animate-error-icon{0%{transform:rotateX(100deg);opacity:0}100%{transform:rotateX(0);opacity:1}}@-webkit-keyframes swal2-rotate-loading{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}@keyframes swal2-rotate-loading{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){overflow:hidden}body.swal2-height-auto{height:auto!important}body.swal2-no-backdrop .swal2-container{top:auto;right:auto;bottom:auto;left:auto;max-width:calc(100% - .625em * 2);background-color:transparent!important}body.swal2-no-backdrop .swal2-container>.swal2-modal{box-shadow:0 0 10px rgba(0,0,0,.4)}body.swal2-no-backdrop .swal2-container.swal2-top{top:0;left:50%;transform:translateX(-50%)}body.swal2-no-backdrop .swal2-container.swal2-top-left,body.swal2-no-backdrop .swal2-container.swal2-top-start{top:0;left:0}body.swal2-no-backdrop .swal2-container.swal2-top-end,body.swal2-no-backdrop .swal2-container.swal2-top-right{top:0;right:0}body.swal2-no-backdrop .swal2-container.swal2-center{top:50%;left:50%;transform:translate(-50%,-50%)}body.swal2-no-backdrop .swal2-container.swal2-center-left,body.swal2-no-backdrop .swal2-container.swal2-center-start{top:50%;left:0;transform:translateY(-50%)}body.swal2-no-backdrop .swal2-container.swal2-center-end,body.swal2-no-backdrop .swal2-container.swal2-center-right{top:50%;right:0;transform:translateY(-50%)}body.swal2-no-backdrop .swal2-container.swal2-bottom{bottom:0;left:50%;transform:translateX(-50%)}body.swal2-no-backdrop .swal2-container.swal2-bottom-left,body.swal2-no-backdrop .swal2-container.swal2-bottom-start{bottom:0;left:0}body.swal2-no-backdrop .swal2-container.swal2-bottom-end,body.swal2-no-backdrop .swal2-container.swal2-bottom-right{right:0;bottom:0}@media print{body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){overflow-y:scroll!important}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown)>[aria-hidden=true]{display:none}body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown) .swal2-container{position:static!important}}body.swal2-toast-shown .swal2-container{background-color:transparent}body.swal2-toast-shown .swal2-container.swal2-top{top:0;right:auto;bottom:auto;left:50%;transform:translateX(-50%)}body.swal2-toast-shown .swal2-container.swal2-top-end,body.swal2-toast-shown .swal2-container.swal2-top-right{top:0;right:0;bottom:auto;left:auto}body.swal2-toast-shown .swal2-container.swal2-top-left,body.swal2-toast-shown .swal2-container.swal2-top-start{top:0;right:auto;bottom:auto;left:0}body.swal2-toast-shown .swal2-container.swal2-center-left,body.swal2-toast-shown .swal2-container.swal2-center-start{top:50%;right:auto;bottom:auto;left:0;transform:translateY(-50%)}body.swal2-toast-shown .swal2-container.swal2-center{top:50%;right:auto;bottom:auto;left:50%;transform:translate(-50%,-50%)}body.swal2-toast-shown .swal2-container.swal2-center-end,body.swal2-toast-shown .swal2-container.swal2-center-right{top:50%;right:0;bottom:auto;left:auto;transform:translateY(-50%)}body.swal2-toast-shown .swal2-container.swal2-bottom-left,body.swal2-toast-shown .swal2-container.swal2-bottom-start{top:auto;right:auto;bottom:0;left:0}body.swal2-toast-shown .swal2-container.swal2-bottom{top:auto;right:auto;bottom:0;left:50%;transform:translateX(-50%)}body.swal2-toast-shown .swal2-container.swal2-bottom-end,body.swal2-toast-shown .swal2-container.swal2-bottom-right{top:auto;right:0;bottom:0;left:auto}");

/***/ }),

/***/ "../../src/app/models/Usuario.ts":
/*!***********************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/models/Usuario.ts ***!
  \***********************************************************************************************************/
/*! exports provided: Usuario */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Usuario", function() { return Usuario; });
class Usuario {
}


/***/ }),

/***/ "../../src/app/seguros/busca-solicitud/busca-solicitud.component.ts":
/*!**********************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/busca-solicitud/busca-solicitud.component.ts ***!
  \**********************************************************************************************************************************************/
/*! exports provided: BuscaSolicitudComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuscaSolicitudComponent", function() { return BuscaSolicitudComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "../../node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "../../node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
/* harmony import */ var _service_seg_solicitudes_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/seg-solicitudes.service */ "../../src/app/seguros/service/seg-solicitudes.service.ts");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/auth.service */ "../../src/app/service/auth.service.ts");
/* harmony import */ var _service_config_param_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/config-param.service */ "../../src/app/seguros/service/config-param.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

















function BuscaSolicitudComponent_div_15_p_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "* Campo es requerido");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function BuscaSolicitudComponent_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, BuscaSolicitudComponent_div_15_p_1_Template, 2, 0, "p", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.errors.required);
} }
function BuscaSolicitudComponent_li_18_div_25_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " SOLICITUD ASIGNADA A OTRO OFICIAL ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function BuscaSolicitudComponent_li_18_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Solicitud de Cr\u00E9dito : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](6, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "button", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscaSolicitudComponent_li_18_Template_button_click_19_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.onCancelar(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " Cancelar ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscaSolicitudComponent_li_18_Template_button_click_22_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const solicitud_r6 = ctx.$implicit; const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r10.onGestionarSeguro(solicitud_r6.Sol_IdSol); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "i", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, " Gestionar Seguro ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, BuscaSolicitudComponent_li_18_div_25_Template, 2, 0, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const solicitud_r6 = ctx.$implicit;
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", solicitud_r6.Sol_NumSol, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Fecha Solicitud : ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind3"](6, 12, solicitud_r6.Sol_FechaSol, 0, 10), " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate4"](" Asegurado : ", solicitud_r6.Seg_Deudores[0].Deu_Nombre, " ", solicitud_r6.Seg_Deudores[0].Deu_Paterno, " ", solicitud_r6.Seg_Deudores[0].Deu_Materno, ", CI: ", solicitud_r6.Seg_Deudores[0].Deu_NumDoc, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" Oficial de Cr\u00E9ditos : ", solicitud_r6.Sol_Oficial, " ( ", solicitud_r6.Sol_CodOficial, " ) ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Estado de la Solicitud del Seguro : ", ctx_r3.getEstadoSolicitud(solicitud_r6.Sol_EstadoSol), " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Estado de la Operaci\u00F3n del Cr\u00E9dito : ", solicitud_r6.operacion_estado, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Tipo Solicitud de Cr\u00E9dito : ", solicitud_r6.Sol_TipoOpeUni, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.userinfo.usuario_login != solicitud_r6.Sol_CodOficial);
} }
function BuscaSolicitudComponent_div_44_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " SOLICITUD ASIGNADA A OTRO OFICIAL, NO PUEDE CONTINUAR CON LA INSTRUMENTACION DEL SEGURO ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function (a0) { return { "display": a0 }; };
class BuscaSolicitudComponent {
    constructor(authService, segSolicitudesService, configParamService, router, http, msg) {
        this.authService = authService;
        this.segSolicitudesService = segSolicitudesService;
        this.configParamService = configParamService;
        this.router = router;
        this.http = http;
        this.msg = msg;
        this.solicitudes = [];
        this.operacion = {};
        this.deudor = {};
        this.verOperacion = 'none';
        this.url_servicios_ext = '';
        this.estadosSolicitud = [];
    }
    ngOnInit() {
        try {
            // get rol del usuario
            //this.userinfo =  this.authService.getUserInfo() as UserInfo;
            this.userinfo = this.authService.getItemSync('userInfo');
            console.log(`USERINFO-FORM => `, this.userinfo);
        }
        catch (error) {
            this.msg.warning('No se pudo recuperar la session del usuario', 'SOLICITUD');
        }
        this.cargarServiciosExt();
        this.cargarEstadosSolicitud();
    }
    // obtenemos la url del servicio externo
    cargarServiciosExt() {
        this.configParamService.getConfigParamKey("servicios_endpoint").subscribe(resp => {
            const response = resp;
            const endpoints = response.data;
            this.url_servicios_ext = endpoints.find(row => row.nombre == 'servicio_soap_ecofuturo').endpoint;
        });
    }
    cargarEstadosSolicitud() {
        // estado_solicitud
        this.configParamService.getConfigParamKey('estado_solicitud').subscribe(resp => {
            const response = resp;
            if (response.status == 'OK') {
                this.estadosSolicitud = response.data;
            }
        });
    }
    onBuscar() {
        if (this.numero_solicitud && this.numero_solicitud.toString().length < 10) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                allowOutsideClick: false,
                text: 'Recuperando datos ...'
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.showLoading();
            // busqueda de numero de solicitud local
            this.segSolicitudesService.getNroSolicitud(this.numero_solicitud).subscribe(resp => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.close();
                let response = resp;
                this.solicitudes = [];
                this.verOperacion = 'none';
                if (response.status == "OK") {
                    if (response.data.solicitud && response.data.solicitud.length > 0) {
                        this.solicitudes = response.data.solicitud;
                    }
                    else {
                        if (response.data.operacion.Operacion_Solicitud) {
                            this.operacion = response.data.operacion;
                            this.deudor = response.data.deudor;
                            this.solicitudes = [];
                            this.verOperacion = 'block';
                        }
                        else {
                            this.msg.info('Nro.Solicitud no existe', 'Busqueda de Solicitud');
                        }
                    }
                }
                else {
                    this.msg.info(response.message);
                }
            }, ex => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.close();
                this.msg.error('No se pudo establecer comunicación', 'SOLICITUDES');
            });
        }
        else {
            this.msg.error('Numero de Solicitud no valido', 'SOLICITUDES');
        }
    }
    onBuscarEcofuturo(numsol) {
        if (numsol) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                allowOutsideClick: false,
                text: 'Recuperando datos ...'
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.showLoading();
            let response;
            const model = {
                request_body: { solicitud: numsol },
                operation: "getSolicitudPrimeraEtapa"
            };
            this.http.post(this.url_servicios_ext, model).subscribe(resp => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.close();
                response = resp;
                this.solicitudes = [];
                if (response.status == 'OK') {
                    if (response.data.getSolicitudPrimeraEtapaResult) {
                        this.operacion = response.data.getSolicitudPrimeraEtapaResult;
                        this.solicitudes = [];
                        this.verOperacion = 'block';
                    }
                }
            }, ex => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.close();
                this.msg.error('No se pudo establecer comunicación', 'SOLICITUDES');
            });
        }
    }
    ;
    onCrearSuscripcion(numsol) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
            title: `Instrumentar Seguro y Continuar?`,
            showCancelButton: true,
            confirmButtonText: `Aceptar`,
            cancelButtonText: `Cancelar`,
            confirmButtonColor: '#16A085',
            cancelButtonColor: '#F1948A',
        }).then((result) => {
            if (result.isConfirmed) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                    allowOutsideClick: false,
                    text: 'Verificando y creando ...'
                });
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.showLoading();
                //this.datosFolderSeguroService.addDesgravamen(numsol,this.authService.usuario.nombre_usuario)
                this.segSolicitudesService.addDesgravamen(numsol, this.authService.usuario.usuario_login).subscribe(resp => {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.close();
                    const response = resp;
                    console.log('PROCESO DE NUEVA SOLICITUD:', response.messages);
                    console.log('Respuesta addDesgravamen ', resp);
                    if (response.status == 'OK') {
                        const id = response.data.Sol_IdSol;
                        this.router.navigate(['/AppMain/pnetseg/seguros/seguro-form', { id }]);
                        //this.router.navigate(['/seguros/seguro-form',{ id }]);
                    }
                    else {
                        this.msg.error(response.message.toString(), 'Instrumentar Seguro');
                    }
                });
            }
        });
    }
    ;
    onGestionarSeguro(id) {
        this.router.navigate(['/AppMain/pnetseg/seguros/seguro-form', { id }]);
        //  this.router.navigate(['/seguros/seguro-form',{ id }]);
    }
    ;
    onCancelar() {
        this.solicitudes = [];
        this.numero_solicitud;
        this.verOperacion = 'none';
    }
    getEstadoSolicitud(estado) {
        return this.estadosSolicitud.find(row => row.codigo == estado).estado;
    }
}
BuscaSolicitudComponent.ɵfac = function BuscaSolicitudComponent_Factory(t) { return new (t || BuscaSolicitudComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_seg_solicitudes_service__WEBPACK_IMPORTED_MODULE_5__["SegSolicitudesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_config_param_service__WEBPACK_IMPORTED_MODULE_7__["ConfigParamService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"])); };
BuscaSolicitudComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BuscaSolicitudComponent, selectors: [["app-busca-solicitud"]], decls: 45, vars: 22, consts: [[1, "w3-panel", "w3-light-grey"], [1, "w3-row"], [1, "w3-col"], [1, "w3-row", "w3-section"], ["frm", "ngForm"], [1, "w3-col", 2, "width", "50px"], ["type", "button", 1, "w3-button", "w3-round", "w3-indigo", 3, "disabled", "click"], [1, "fa", "fa-search"], [1, "w3-rest"], ["name", "numsol", "type", "number", "placeholder", "N\u00FAmero de Solicitud", "required", "", 1, "w3-input", "w3-border", 3, "ngModel", "ngModelChange"], ["numsol", "ngModel"], ["class", "w3-panel w3-pale-red w3-tiny w3-padding", 4, "ngIf"], [1, "w3-row", "w3-padding-16"], [1, "w3-ul", "w3-border"], [4, "ngFor", "ngForOf"], [1, "w3-row", "w3-padding-16", 3, "ngStyle"], [1, "w3-bar"], [1, "w3-bar-item", "w3-right"], ["type", "button", 1, "w3-button", "w3-border-teal", "w3-white", "w3-border", "w3-round-xlarge", "w3-text-teal", 3, "click"], [1, "w3-button", "w3-round-xlarge", "w3-indigo", "w3-border", 3, "click"], ["title", "Editar solicitud", 1, "fa", "fa-file"], ["class", "w3-panel w3-pale-red", 4, "ngIf"], [1, "w3-panel", "w3-pale-red", "w3-tiny", "w3-padding"], [4, "ngIf"], ["type", "button", 1, "w3-button", "w3-border-blue", "w3-white", "w3-border", "w3-round-xlarge", "w3-text-blue", 3, "click"], [1, "w3-button", "w3-border-blue", "w3-white", "w3-border", "w3-round-xlarge", "w3-text-blue", 3, "click"], ["title", "Editar solicitud", 1, "fa", "fa-edit"], [1, "w3-panel", "w3-pale-red"]], template: function BuscaSolicitudComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " Busqueda Solicitud de Cr\u00E9dito ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "form", null, 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscaSolicitudComponent_Template_button_click_10_listener() { return ctx.onBuscar(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "input", 9, 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function BuscaSolicitudComponent_Template_input_ngModelChange_13_listener($event) { return ctx.numero_solicitud = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, BuscaSolicitudComponent_div_15_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "ul", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, BuscaSolicitudComponent_li_18_Template, 26, 16, "li", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "ul", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, " Solicitud de Cr\u00E9dito: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](27, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscaSolicitudComponent_Template_button_click_38_listener() { return ctx.onCancelar(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Cancelar ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "button", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscaSolicitudComponent_Template_button_click_41_listener() { return ctx.onCrearSuscripcion(ctx.operacion.Operacion_Solicitud); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "i", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, " Instrumentar Seguro ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](44, BuscaSolicitudComponent_div_44_Template, 2, 0, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](8);
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", _r0.invalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.numero_solicitud);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.invalid && (_r1.dirty || _r1.touched));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.solicitudes);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](20, _c0, ctx.verOperacion));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.operacion.Operacion_Solicitud);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Fecha Solicitud : ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind3"](27, 16, ctx.operacion.Operacion_Fecha_Solicitud, 0, 10), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate4"](" Cliente : ", ctx.deudor.Deudor_Nombre, " ", ctx.deudor.Deudor_Paterno, " ", ctx.deudor.Deudor_Materno, ", CI: ", ctx.deudor.Deudor_Numero_Documento, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" Oficial de Cr\u00E9ditos : ", ctx.operacion.Operacion_Oficial_Creditos, " (", ctx.operacion.Operacion_Usuario_Oficial, ")");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Estado de la Operaci\u00F3n del Cr\u00E9dito : ", ctx.operacion.Operacion_Estado, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Tipo Solicitud de Cr\u00E9dito : ", ctx.operacion.Operacion_Tipo_Solicitud, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.userinfo.usuario_login != ctx.operacion.Operacion_Usuario_Oficial);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NumberValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgStyle"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["SlicePipe"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlZ3Vyb3MvYnVzY2Etc29saWNpdHVkL2J1c2NhLXNvbGljaXR1ZC5jb21wb25lbnQuY3NzIn0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BuscaSolicitudComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-busca-solicitud',
                templateUrl: './busca-solicitud.component.html',
                styleUrls: ['./busca-solicitud.component.css']
            }]
    }], function () { return [{ type: _service_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] }, { type: _service_seg_solicitudes_service__WEBPACK_IMPORTED_MODULE_5__["SegSolicitudesService"] }, { type: _service_config_param_service__WEBPACK_IMPORTED_MODULE_7__["ConfigParamService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }, { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }, { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/models/message-email.ts":
/*!*************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/models/message-email.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: MessageEmail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageEmail", function() { return MessageEmail; });
class MessageEmail {
}


/***/ }),

/***/ "../../src/app/seguros/models/seg-adicionales.ts":
/*!***************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/models/seg-adicionales.ts ***!
  \***************************************************************************************************************************/
/*! exports provided: SegAdicionales */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegAdicionales", function() { return SegAdicionales; });
class SegAdicionales {
}


/***/ }),

/***/ "../../src/app/seguros/models/seg-aprobaciones.ts":
/*!****************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/models/seg-aprobaciones.ts ***!
  \****************************************************************************************************************************/
/*! exports provided: SegAprobaciones */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegAprobaciones", function() { return SegAprobaciones; });
class SegAprobaciones {
}


/***/ }),

/***/ "../../src/app/seguros/models/seg-beneficiarios.ts":
/*!*****************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/models/seg-beneficiarios.ts ***!
  \*****************************************************************************************************************************/
/*! exports provided: SegBeneficiarios */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegBeneficiarios", function() { return SegBeneficiarios; });
class SegBeneficiarios {
}


/***/ }),

/***/ "../../src/app/seguros/models/seg-deudores.ts":
/*!************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/models/seg-deudores.ts ***!
  \************************************************************************************************************************/
/*! exports provided: SegDeudores */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegDeudores", function() { return SegDeudores; });
class SegDeudores {
}


/***/ }),

/***/ "../../src/app/seguros/models/seg-observaciones.ts":
/*!*****************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/models/seg-observaciones.ts ***!
  \*****************************************************************************************************************************/
/*! exports provided: SegObservaciones */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegObservaciones", function() { return SegObservaciones; });
class SegObservaciones {
}


/***/ }),

/***/ "../../src/app/seguros/models/seg-operaciones.ts":
/*!***************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/models/seg-operaciones.ts ***!
  \***************************************************************************************************************************/
/*! exports provided: SegOperaciones */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegOperaciones", function() { return SegOperaciones; });
class SegOperaciones {
}


/***/ }),

/***/ "../../src/app/seguros/models/seg-solicitudes.ts":
/*!***************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/models/seg-solicitudes.ts ***!
  \***************************************************************************************************************************/
/*! exports provided: SegSolicitudes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegSolicitudes", function() { return SegSolicitudes; });
class SegSolicitudes {
}


/***/ }),

/***/ "../../src/app/seguros/seguro-form/seguro-form.component.ts":
/*!**************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/seguro-form/seguro-form.component.ts ***!
  \**************************************************************************************************************************************/
/*! exports provided: SeguroFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeguroFormComponent", function() { return SeguroFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "../../node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "../../node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "../../node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/auth.service */ "../../src/app/service/auth.service.ts");
/* harmony import */ var _service_reportes_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/reportes.service */ "../../src/app/seguros/service/reportes.service.ts");
/* harmony import */ var _service_ecofuturo_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../service/ecofuturo.service */ "../../src/app/seguros/service/ecofuturo.service.ts");
/* harmony import */ var _service_send_mail_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../service/send-mail.service */ "../../src/app/seguros/service/send-mail.service.ts");
/* harmony import */ var _service_seg_aprobaciones_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../service/seg-aprobaciones.service */ "../../src/app/seguros/service/seg-aprobaciones.service.ts");
/* harmony import */ var _service_seg_observaciones_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../service/seg-observaciones.service */ "../../src/app/seguros/service/seg-observaciones.service.ts");
/* harmony import */ var _service_seg_beneficiarios_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../service/seg-beneficiarios.service */ "../../src/app/seguros/service/seg-beneficiarios.service.ts");
/* harmony import */ var _service_seg_solicitudes_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../service/seg-solicitudes.service */ "../../src/app/seguros/service/seg-solicitudes.service.ts");
/* harmony import */ var _service_seg_deudores_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../service/seg-deudores.service */ "../../src/app/seguros/service/seg-deudores.service.ts");
/* harmony import */ var _service_seg_adicionales_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../service/seg-adicionales.service */ "../../src/app/seguros/service/seg-adicionales.service.ts");
/* harmony import */ var _service_file_upload_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../service/file-upload.service */ "../../src/app/seguros/service/file-upload.service.ts");
/* harmony import */ var _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../models/seg-solicitudes */ "../../src/app/seguros/models/seg-solicitudes.ts");
/* harmony import */ var _models_seg_beneficiarios__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../models/seg-beneficiarios */ "../../src/app/seguros/models/seg-beneficiarios.ts");
/* harmony import */ var _models_seg_adicionales__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../models/seg-adicionales */ "../../src/app/seguros/models/seg-adicionales.ts");
/* harmony import */ var _models_seg_aprobaciones__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../models/seg-aprobaciones */ "../../src/app/seguros/models/seg-aprobaciones.ts");
/* harmony import */ var _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../models/seg-observaciones */ "../../src/app/seguros/models/seg-observaciones.ts");
/* harmony import */ var _models_seg_deudores__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../models/seg-deudores */ "../../src/app/seguros/models/seg-deudores.ts");
/* harmony import */ var _models_seg_operaciones__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../models/seg-operaciones */ "../../src/app/seguros/models/seg-operaciones.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");
/* harmony import */ var _models_message_email__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../models/message-email */ "../../src/app/seguros/models/message-email.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/forms */ "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");











































function SeguroFormComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 70);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "div", 71);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_span_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("N\u00FAmero de P\u00F3liza : ", ctx_r1.data.seg_solicitudes.Sol_Poliza, " ");
} }
function SeguroFormComponent_button_23_Template(rf, ctx) { if (rf & 1) {
    const _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_23_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r28); const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r27.onDatosEco(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Actualizar");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_button_29_Template(rf, ctx) { if (rf & 1) {
    const _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_29_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r30); const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r29.onImprimirDoc(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Imprimir");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_button_30_Template(rf, ctx) { if (rf & 1) {
    const _r32 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_30_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r32); const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r31.onEliminarSolicitud(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 75);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Eliminar");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_button_31_Template(rf, ctx) { if (rf & 1) {
    const _r34 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_31_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r34); const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r33.onGuardarDatosEdit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "\u00A0\u00A0 Guardar \u00A0\u00A0");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_button_32_Template(rf, ctx) { if (rf & 1) {
    const _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_32_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r36); const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r35.onValidar(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 78);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Validar");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function (a0) { return { "w3-indigo": a0 }; };
function SeguroFormComponent_button_43_Template(rf, ctx) { if (rf & 1) {
    const _r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_43_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r38); const ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r37.tab_active = 4; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Operaci\u00F3n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](1, _c0, ctx_r7.tab_active == 4));
} }
function SeguroFormComponent_button_44_Template(rf, ctx) { if (rf & 1) {
    const _r40 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_44_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r40); const ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r39.tab_active = 5; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Ult.comunicaci\u00F3n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](1, _c0, ctx_r8.tab_active == 5));
} }
function SeguroFormComponent_button_45_Template(rf, ctx) { if (rf & 1) {
    const _r42 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_45_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r42); const ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r41.tab_active = 6; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Resoluci\u00F3n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](1, _c0, ctx_r9.tab_active == 6));
} }
function SeguroFormComponent_button_46_Template(rf, ctx) { if (rf & 1) {
    const _r44 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_46_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r44); const ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r43.tab_active = 1; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "\u00A0");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](1, _c0, ctx_r10.tab_active == 1));
} }
function SeguroFormComponent_div_226_i_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 96);
} }
function SeguroFormComponent_div_226_i_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 97);
} }
function SeguroFormComponent_div_226_span_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "( ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "i", 98);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, " Titular)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_div_226_span_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "( ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "i", 99);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, " Codeudor)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_div_226_span_8_i_2_Template(rf, ctx) { if (rf & 1) {
    const _r70 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "i", 103);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_226_span_8_i_2_Template_i_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r70); const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; return deuObj_r45.Deu_Incluido = "S"; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_div_226_span_8_i_3_Template(rf, ctx) { if (rf & 1) {
    const _r73 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "i", 104);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_226_span_8_i_3_Template_i_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r73); const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; return deuObj_r45.Deu_Incluido = "N"; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_div_226_span_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 100);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Incluido: \u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SeguroFormComponent_div_226_span_8_i_2_Template, 1, 0, "i", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, SeguroFormComponent_div_226_span_8_i_3_Template, 1, 0, "i", 102);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r45.Deu_Incluido == "N");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r45.Deu_Incluido == "S");
} }
function SeguroFormComponent_div_226_tr_117_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Fecha de Resoluci\u00F3n: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](6, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](6, 1, deuObj_r45.Deu_FechaResolucion, 0, 10), " ");
} }
function SeguroFormComponent_div_226_span_126_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_CiudadNac);
} }
function SeguroFormComponent_div_226_input_127_Template(rf, ctx) { if (rf & 1) {
    const _r79 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_226_input_127_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r79); const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r45.Deu_CiudadNac = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r45.Deu_CiudadNac);
} }
function SeguroFormComponent_div_226_span_133_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Peso);
} }
function SeguroFormComponent_div_226_input_134_Template(rf, ctx) { if (rf & 1) {
    const _r84 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 106);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_226_input_134_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r84); const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r45.Deu_Peso = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r45.Deu_Peso);
} }
function SeguroFormComponent_div_226_span_140_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Talla);
} }
function SeguroFormComponent_div_226_input_141_Template(rf, ctx) { if (rf & 1) {
    const _r89 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 107);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_226_input_141_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r89); const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r45.Deu_Talla = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r45.Deu_Talla);
} }
function SeguroFormComponent_div_226_span_147_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Mano);
} }
function SeguroFormComponent_div_226_select_148_Template(rf, ctx) { if (rf & 1) {
    const _r94 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_226_select_148_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r94); const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r45.Deu_Mano = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "option", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "DERECHO(A)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "option", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "ZURDO(A)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r45.Deu_Mano);
} }
function SeguroFormComponent_div_226_span_160_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_DetACtiv);
} }
function SeguroFormComponent_div_226_input_161_Template(rf, ctx) { if (rf & 1) {
    const _r99 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 110);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_226_input_161_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r99); const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r45.Deu_DetACtiv = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r45.Deu_DetACtiv);
} }
function SeguroFormComponent_div_226_tr_187_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_MontoActAcumVerif);
} }
function SeguroFormComponent_div_226_tr_187_input_6_Template(rf, ctx) { if (rf & 1) {
    const _r106 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 112);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_226_tr_187_input_6_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r106); const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; return deuObj_r45.Deu_MontoActAcumVerif = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r45.Deu_MontoActAcumVerif);
} }
function SeguroFormComponent_div_226_tr_187_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "SALDO ACTUAL VERIFICADO: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, SeguroFormComponent_div_226_tr_187_span_5_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, SeguroFormComponent_div_226_tr_187_input_6_Template, 1, 1, "input", 111);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r63.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r63.estado_codigo == "P");
} }
function SeguroFormComponent_div_226_button_209_Template(rf, ctx) { if (rf & 1) {
    const _r109 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_226_button_209_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r109); const ctx_r108 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r108.onGuardarDatosEdit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "\u00A0\u00A0 Guardar \u00A0\u00A0");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_div_226_button_211_Template(rf, ctx) { if (rf & 1) {
    const _r112 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 94);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_226_button_211_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r112); const deuObj_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r110 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r110.onOpenBeneficiarios(deuObj_r45.Deu_Id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 113);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Beneficiarios");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c1 = function (a0) { return { "w3-dark-grey": a0 }; };
const _c2 = function (a0) { return { "display": a0 }; };
const _c3 = function () { return ["A", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8"]; };
function SeguroFormComponent_div_226_Template(rf, ctx) { if (rf & 1) {
    const _r114 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 79);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_226_Template_span_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r114); const i_r46 = ctx.index; const ctx_r113 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r113.deus_[i_r46] == "none" ? (ctx_r113.deus_[i_r46] = "block") : (ctx_r113.deus_[i_r46] = "none"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, SeguroFormComponent_div_226_i_3_Template, 1, 0, "i", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, SeguroFormComponent_div_226_i_4_Template, 1, 0, "i", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, SeguroFormComponent_div_226_span_6_Template, 4, 0, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, SeguroFormComponent_div_226_span_7_Template, 4, 0, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, SeguroFormComponent_div_226_span_8_Template, 4, 2, "span", 84);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 85);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 86);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "table", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Nombre Completo:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "Apellido Paterno: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](29, "Apellido Materno: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](31);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](35, "Apellido de Casada: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](41, "Direcci\u00F3n Domicilio: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](44, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](47, "Direcci\u00F3n Negocio/Oficina: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](51, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](53, "Estado Civil: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](55);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](56, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](57, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](59, "G\u00E9nero: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](60, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](61);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](62, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](63, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](65, "Documento de Identidad: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](66, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](67);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](68, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](69, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](71, "Tipo Documento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](72, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](73);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](74, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](75, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](77, "Extension Documento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](78, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](79);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](80, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](81, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](83, "Complemento Documento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](84, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](85);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](86, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](87, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](88, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](89, "Pa\u00EDs de Nacimiento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](90, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](91);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](92, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](93, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](94, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](95, "Fecha Nacimiento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](96, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](97);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](98, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](99, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](100, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](101, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](102, "Edad (A\u00F1os): ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](103, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](104);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](105, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](106, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](107, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](108, "Tipo de Cobertura del Seguro: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](109, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](110);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](111, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](112, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](113, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](114, "N\u00FAmero de P\u00F3liza: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](115, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](116);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](117, SeguroFormComponent_div_226_tr_117_Template, 7, 5, "tr", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](118, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](119, "table", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](120, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](121, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](122, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](123, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](124, "(*) Cuidad de Nacimiento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](125, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](126, SeguroFormComponent_div_226_span_126_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](127, SeguroFormComponent_div_226_input_127_Template, 1, 1, "input", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](128, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](129, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](130, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](131, "(*) Peso (KGrs): ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](132, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](133, SeguroFormComponent_div_226_span_133_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](134, SeguroFormComponent_div_226_input_134_Template, 1, 1, "input", 88);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](135, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](136, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](137, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](138, "(*) Talla (Cms): ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](139, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](140, SeguroFormComponent_div_226_span_140_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](141, SeguroFormComponent_div_226_input_141_Template, 1, 1, "input", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](142, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](143, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](144, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](145, "(*) Zurd@/Derecho@: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](146, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](147, SeguroFormComponent_div_226_span_147_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](148, SeguroFormComponent_div_226_select_148_Template, 5, 1, "select", 90);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](149, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](150, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](151, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](152, "Actividad: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](153, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](154);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](155, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](156, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](157, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](158, "(*) Detalle de la Actividad: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](159, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](160, SeguroFormComponent_div_226_span_160_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](161, SeguroFormComponent_div_226_input_161_Template, 1, 1, "input", 91);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](162, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](163, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](164, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](165, "Fecha Registro: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](166, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](167);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](168, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](169, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](170, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](171, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](172, "Celular: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](173, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](174);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](175, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](176, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](177, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](178, "Tel\u00E9fono de Contacto: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](179, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](180);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](181, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](182, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](183, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](184, "Tel\u00E9fono: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](185, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](186);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](187, SeguroFormComponent_div_226_tr_187_Template, 7, 2, "tr", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](188, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](189, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](190, "b", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](191, "SALDO ACTUAL + SOLICITUD: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](192, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](193);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](194, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](195, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](196, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](197, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](198, "Incluido: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](199, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](200);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](201, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](202, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](203, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](204, "ID: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](205, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](206);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](207, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](208, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](209, SeguroFormComponent_div_226_button_209_Template, 5, 0, "button", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](210, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](211, SeguroFormComponent_div_226_button_211_Template, 5, 0, "button", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](212, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](213, "button", 94);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_226_Template_button_click_213_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r114); const deuObj_r45 = ctx.$implicit; const ctx_r115 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r115.onDeclaracionDeudor(deuObj_r45.Deu_Id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](214, "i", 95);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](215, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](216, "span", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](217, "Decla.Salud \u00A0");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r45 = ctx.$implicit;
    const i_r46 = ctx.index;
    const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](60, _c1, ctx_r11.deus[i_r46]));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.deus_[i_r46] == "none");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.deus_[i_r46] == "block");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate3"](" \u00A0 ", deuObj_r45.Deu_Nombre, " ", deuObj_r45.Deu_Paterno, " ", deuObj_r45.Deu_Materno, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r45.Deu_NIvel == 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r45.Deu_NIvel == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](62, _c2, ctx_r11.deus_[i_r46]));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Nombre);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Paterno);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Materno);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Casada);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_DirDom);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_DirOfi);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_EstCiv);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Sexo);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_NumDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_TipoDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_ExtDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_CompDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_PaisNac);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](98, 49, deuObj_r45.Deu_FecNac, 0, 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Edad);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_cobertura);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", deuObj_r45.Deu_Poliza, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](64, _c3).includes(ctx_r11.estado_codigo));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Actividad);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](168, 53, deuObj_r45.Deu_FecRegCli, 0, 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_TelCel);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_TelDom);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_TelOfi);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r11.esTarjeta);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](194, 57, deuObj_r45.saldo_actual_solicitud, ".2"), " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Incluido);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r45.Deu_Id);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r11.estado_codigo == "P");
} }
function SeguroFormComponent_button_247_Template(rf, ctx) { if (rf & 1) {
    const _r117 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 114);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_247_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r117); const ctx_r116 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r116.loadBeneficiarios(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 115);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_tr_248_span_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Editar");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_tr_248_span_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Ver");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_tr_248_Template(rf, ctx) { if (rf & 1) {
    const _r122 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "a", 116);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_tr_248_Template_a_click_16_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r122); const ben_r118 = ctx.$implicit; const ctx_r121 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r121.onOpenBeneficiarios(ben_r118.Ben_IdDeu, ben_r118.Ben_Id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, SeguroFormComponent_tr_248_span_17_Template, 2, 0, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, SeguroFormComponent_tr_248_span_18_Template, 2, 0, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ben_r118 = ctx.$implicit;
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ben_r118.Ben_Id);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r13.getDeudor(ben_r118.Ben_IdDeu, "nombres"));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r13.getDeudor(ben_r118.Ben_IdDeu, "tipo"));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate3"]("", ben_r118.Ben_Nombre, " ", ben_r118.Ben_Paterno, " ", ben_r118.Ben_Materno, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ben_r118.Ben_NumDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ben_r118.Ben_Porcentaje, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ben_r118.Ben_Tipo);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r13.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r13.estado_codigo != "P");
} }
function SeguroFormComponent_tr_392_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const row_r123 = ctx.$implicit;
    const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](3, 6, row_r123.Obs_FechaReg, 0, 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r123.Obs_HoraReg);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r123.Obs_Observacion);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r123.Obs_Usuario);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r14.getEifAseguradora(row_r123.Obs_Eeff));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r14.getEifAseguradora(row_r123.Obs_Aseg));
} }
function SeguroFormComponent_div_412_tr_6_input_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "input", 120);
} }
function SeguroFormComponent_div_412_tr_6_input_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "input", 121);
} }
function SeguroFormComponent_div_412_tr_6_input_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "input", 120);
} }
function SeguroFormComponent_div_412_tr_6_input_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "input", 121);
} }
function SeguroFormComponent_div_412_tr_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Aprobado: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, SeguroFormComponent_div_412_tr_6_input_5_Template, 1, 0, "input", 118);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, SeguroFormComponent_div_412_tr_6_input_6_Template, 1, 0, "input", 119);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, " Rechazado: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, SeguroFormComponent_div_412_tr_6_input_8_Template, 1, 0, "input", 118);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, SeguroFormComponent_div_412_tr_6_input_9_Template, 1, 0, "input", 119);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Extra Prima: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "Condiciones/Exclusiones: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const apro_r125 = ctx.$implicit;
    const ctx_r124 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r124.getDeudor(apro_r125.Apr_IdDeu, "nombres"));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", apro_r125.Apr_Aprobado == "S");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", apro_r125.Apr_Aprobado != "S");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", apro_r125.Apr_Aprobado == "N");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", apro_r125.Apr_Aprobado != "N");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](apro_r125.Apr_ExtraPrima);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](apro_r125.Apr_Condiciones);
} }
function SeguroFormComponent_div_412_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "table", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td", 117);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "b", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "PRONUNCIAMIENTO DE LA COMPA\u00D1\u00CDA");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, SeguroFormComponent_div_412_tr_6_Template, 18, 7, "tr", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r15.data.seg_aprobaciones);
} }
function SeguroFormComponent_div_413_tr_2_i_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 133);
} }
function SeguroFormComponent_div_413_tr_2_i_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 133);
} }
function SeguroFormComponent_div_413_tr_2_ng_container_13_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deu_r131.extra_prima);
} }
function SeguroFormComponent_div_413_tr_2_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    const _r141 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 134);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_413_tr_2_ng_container_13_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r141); const deu_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deu_r131.extra_prima = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SeguroFormComponent_div_413_tr_2_ng_container_13_span_2_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const deu_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r134 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r131.extra_prima);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r134.estado_general == "APROBADO");
} }
function SeguroFormComponent_div_413_tr_2_ng_container_16_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deu_r131.condiciones_exclusiones);
} }
function SeguroFormComponent_div_413_tr_2_ng_container_16_Template(rf, ctx) { if (rf & 1) {
    const _r147 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 135);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_413_tr_2_ng_container_16_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r147); const deu_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deu_r131.condiciones_exclusiones = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SeguroFormComponent_div_413_tr_2_ng_container_16_span_2_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const deu_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r135 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r131.condiciones_exclusiones);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r135.estado_general == "APROBADO");
} }
function SeguroFormComponent_div_413_tr_2_ng_container_17_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deu_r131.causal);
} }
function SeguroFormComponent_div_413_tr_2_ng_container_17_Template(rf, ctx) { if (rf & 1) {
    const _r153 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 136);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_413_tr_2_ng_container_17_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r153); const deu_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deu_r131.causal = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SeguroFormComponent_div_413_tr_2_ng_container_17_span_2_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const deu_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r136 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r131.causal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r136.estado_general == "APROBADO");
} }
function SeguroFormComponent_div_413_tr_2_Template(rf, ctx) { if (rf & 1) {
    const _r157 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, " Aprobado ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "input", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SeguroFormComponent_div_413_tr_2_Template_input_change_6_listener() { const deu_r131 = ctx.$implicit; return deu_r131.rechazado = false; })("ngModelChange", function SeguroFormComponent_div_413_tr_2_Template_input_ngModelChange_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r157); const deu_r131 = ctx.$implicit; return deu_r131.aprobado = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, SeguroFormComponent_div_413_tr_2_i_7_Template, 1, 0, "i", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, " Rechazado ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "input", 131);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SeguroFormComponent_div_413_tr_2_Template_input_change_10_listener() { const deu_r131 = ctx.$implicit; return deu_r131.aprobado = false; })("ngModelChange", function SeguroFormComponent_div_413_tr_2_Template_input_ngModelChange_10_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r157); const deu_r131 = ctx.$implicit; return deu_r131.rechazado = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, SeguroFormComponent_div_413_tr_2_i_11_Template, 1, 0, "i", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, SeguroFormComponent_div_413_tr_2_ng_container_13_Template, 3, 2, "ng-container", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, " \u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "td", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, SeguroFormComponent_div_413_tr_2_ng_container_16_Template, 3, 2, "ng-container", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, SeguroFormComponent_div_413_tr_2_ng_container_17_Template, 3, 2, "ng-container", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, " \u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r131 = ctx.$implicit;
    const ctx_r130 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate3"]("", deu_r131.Deu_Nombre, " ", deu_r131.Deu_Paterno, " ", deu_r131.Deu_Materno, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r131.aprobado);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r131.aprobado && ctx_r130.estado_general == "APROBADO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r131.rechazado);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r131.rechazado && ctx_r130.estado_general == "APROBADO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r131.aprobado);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r131.aprobado);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r131.rechazado);
} }
function SeguroFormComponent_div_413_Template(rf, ctx) { if (rf & 1) {
    const _r161 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "table", 122);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SeguroFormComponent_div_413_tr_2_Template, 19, 10, "tr", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "button", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_413_Template_button_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r161); const ctx_r160 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r160.onSaveResolucion(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "i", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, " Aceptar y Guardar ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r16.data.seg_deudores);
} }
function SeguroFormComponent_tr_425_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    const _r171 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " SI ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 143);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SeguroFormComponent_tr_425_ng_container_4_Template_input_change_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r171); const salud_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return salud_r162.valor_no = false; })("ngModelChange", function SeguroFormComponent_tr_425_ng_container_4_Template_input_ngModelChange_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r171); const salud_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return salud_r162.valor_si = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, " NO ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "input", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SeguroFormComponent_tr_425_ng_container_4_Template_input_change_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r171); const salud_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return salud_r162.valor_si = false; })("ngModelChange", function SeguroFormComponent_tr_425_ng_container_4_Template_input_ngModelChange_4_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r171); const salud_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return salud_r162.valor_no = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const salud_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", salud_r162.valor_si);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", salud_r162.valor_no);
} }
function SeguroFormComponent_tr_425_ng_template_5_i_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 133);
} }
function SeguroFormComponent_tr_425_ng_template_5_i_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 133);
} }
function SeguroFormComponent_tr_425_ng_template_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](0, " \u00A0\u00A0 SI ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, SeguroFormComponent_tr_425_ng_template_5_i_1_Template, 1, 0, "i", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " \u00A0\u00A0 NO ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, SeguroFormComponent_tr_425_ng_template_5_i_3_Template, 1, 0, "i", 130);
} if (rf & 2) {
    const salud_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", salud_r162.valor_si);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", salud_r162.valor_no);
} }
function SeguroFormComponent_tr_425_ng_container_8_Template(rf, ctx) { if (rf & 1) {
    const _r184 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 145);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_tr_425_ng_container_8_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r184); const salud_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return salud_r162.Adic_Comment = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const salud_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", salud_r162.Adic_Comment);
} }
function SeguroFormComponent_tr_425_ng_template_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](0);
} if (rf & 2) {
    const salud_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", salud_r162.Adic_Comment, "");
} }
function SeguroFormComponent_tr_425_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td", 138);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, SeguroFormComponent_tr_425_ng_container_4_Template, 5, 2, "ng-container", 139);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, SeguroFormComponent_tr_425_ng_template_5_Template, 4, 2, "ng-template", null, 140, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "td", 141);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, SeguroFormComponent_tr_425_ng_container_8_Template, 2, 1, "ng-container", 139);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, SeguroFormComponent_tr_425_ng_template_9_Template, 1, 1, "ng-template", null, 142, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const salud_r162 = ctx.$implicit;
    const _r164 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](6);
    const _r167 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](10);
    const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"]("", salud_r162.Adic_Pregunta, ". ", salud_r162.Adic_Texto, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r17.estado_codigo == "P")("ngIfElse", _r164);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r17.onAclaracion(salud_r162))("ngIfElse", _r167);
} }
function SeguroFormComponent_div_428_Template(rf, ctx) { if (rf & 1) {
    const _r188 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_428_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r188); const ctx_r187 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r187.onAceptarCuestionario(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "i", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, " Aceptar y Guardar ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_tr_451_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const row_r189 = ctx.$implicit;
    const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](3, 6, row_r189.Obs_FechaReg, 0, 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r189.Obs_HoraReg);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r189.Obs_Observacion);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r189.Obs_Usuario);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r19.getEifAseguradora(row_r189.Obs_Eeff));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r19.getEifAseguradora(row_r189.Obs_Aseg));
} }
function SeguroFormComponent_div_466_i_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 96);
} }
function SeguroFormComponent_div_466_i_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 97);
} }
function SeguroFormComponent_div_466_i_6_Template(rf, ctx) { if (rf & 1) {
    const _r221 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "i", 167);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_466_i_6_Template_i_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r221); const ctx_r220 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); const bene_r190 = ctx_r220.$implicit; const i_r191 = ctx_r220.index; const ctx_r219 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r219.onDelBeneficiario(bene_r190.Ben_Id, i_r191); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_div_466_span_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_Nombre);
} }
function SeguroFormComponent_div_466_input_16_Template(rf, ctx) { if (rf & 1) {
    const _r225 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_input_16_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r225); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_Nombre = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_Nombre);
} }
function SeguroFormComponent_div_466_span_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_Paterno);
} }
function SeguroFormComponent_div_466_input_21_Template(rf, ctx) { if (rf & 1) {
    const _r230 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 169);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_input_21_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r230); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_Paterno = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_Paterno);
} }
function SeguroFormComponent_div_466_span_27_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_Materno);
} }
function SeguroFormComponent_div_466_input_28_Template(rf, ctx) { if (rf & 1) {
    const _r235 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_input_28_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r235); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_Materno = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_Materno);
} }
function SeguroFormComponent_div_466_span_32_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_Casada);
} }
function SeguroFormComponent_div_466_input_33_Template(rf, ctx) { if (rf & 1) {
    const _r240 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 171);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_input_33_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r240); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_Casada = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_Casada);
} }
function SeguroFormComponent_div_466_span_50_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_NumDoc);
} }
function SeguroFormComponent_div_466_input_51_Template(rf, ctx) { if (rf & 1) {
    const _r245 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 172);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_input_51_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r245); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_NumDoc = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_NumDoc);
} }
function SeguroFormComponent_div_466_span_55_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_TipoDoc);
} }
function SeguroFormComponent_div_466_select_56_Template(rf, ctx) { if (rf & 1) {
    const _r250 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 173);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_select_56_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r250); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_TipoDoc = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "option", 174);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "CARNET DE IDENTIDAD");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "option", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "CARNET EXTRANJERO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "option", 176);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "PASAPORTE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_TipoDoc);
} }
function SeguroFormComponent_div_466_span_62_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_ExtDoc);
} }
function SeguroFormComponent_div_466_select_63_Template(rf, ctx) { if (rf & 1) {
    const _r255 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 177);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_select_63_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r255); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_ExtDoc = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "option", 178);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "LA PAZ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "option", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "COCHABAMBA");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "option", 180);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "SANTA CRUZ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "option", 181);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, "ORURO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "option", 182);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "POTOSI");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "option", 183);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, "CHUQUISACA");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "option", 184);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, "TARIJA");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "option", 185);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "BENI");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "option", 186);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, "PANDO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "option", 187);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20, "Sin Extensi\u00F3n ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_ExtDoc);
} }
function SeguroFormComponent_div_466_span_67_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_CompDoc);
} }
function SeguroFormComponent_div_466_input_68_Template(rf, ctx) { if (rf & 1) {
    const _r260 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 188);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_input_68_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r260); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_CompDoc = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_CompDoc);
} }
function SeguroFormComponent_div_466_span_74_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_Relacion);
} }
function SeguroFormComponent_div_466_select_75_Template(rf, ctx) { if (rf & 1) {
    const _r265 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 189);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_select_75_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r265); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_Relacion = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "option", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "CONVIVIENTE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "option", 191);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "CONYUGUE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "option", 192);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "HIJO(A)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "option", 193);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, "HERMANO(A)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "option", 194);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "MADRE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "option", 195);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, "PADRE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "option", 196);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, "OTROS");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_Relacion);
} }
function SeguroFormComponent_div_466_span_79_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_Porcentaje);
} }
function SeguroFormComponent_div_466_input_80_Template(rf, ctx) { if (rf & 1) {
    const _r270 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 197);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_input_80_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r270); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_Porcentaje = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_Porcentaje);
} }
function SeguroFormComponent_div_466_span_86_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_Telefono);
} }
function SeguroFormComponent_div_466_input_87_Template(rf, ctx) { if (rf & 1) {
    const _r275 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 198);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_input_87_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r275); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_Telefono = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_Telefono);
} }
function SeguroFormComponent_div_466_span_91_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](bene_r190.Ben_Tipo);
} }
function SeguroFormComponent_div_466_select_92_Template(rf, ctx) { if (rf & 1) {
    const _r280 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 199);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_select_92_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r280); const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return bene_r190.Ben_Tipo = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "option", 200);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "PRIMARIO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "option", 201);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "CONTINGENTE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r190.Ben_Tipo);
} }
const _c4 = function (a0, a1) { return { "w3-hide": a0, "w3-show": a1 }; };
function SeguroFormComponent_div_466_Template(rf, ctx) { if (rf & 1) {
    const _r283 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 147);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_div_466_Template_span_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r283); const i_r191 = ctx.index; const ctx_r282 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r282.bens[i_r191] ? (ctx_r282.bens[i_r191] = false) : (ctx_r282.bens[i_r191] = true); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, SeguroFormComponent_div_466_i_3_Template, 1, 0, "i", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, SeguroFormComponent_div_466_i_4_Template, 1, 0, "i", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, SeguroFormComponent_div_466_i_6_Template, 1, 0, "i", 148);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 149);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 150);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "table", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "(*) Nombre Completo:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, SeguroFormComponent_div_466_span_15_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, SeguroFormComponent_div_466_input_16_Template, 1, 1, "input", 151);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, "(*) Apellido Paterno:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, SeguroFormComponent_div_466_span_20_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, SeguroFormComponent_div_466_input_21_Template, 1, 1, "input", 152);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](22, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25, "(*) Apellido Materno:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](27, SeguroFormComponent_div_466_span_27_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](28, SeguroFormComponent_div_466_input_28_Template, 1, 1, "input", 153);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30, "Apellido de Casada:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](32, SeguroFormComponent_div_466_span_32_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](33, SeguroFormComponent_div_466_input_33_Template, 1, 1, "input", 154);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](34, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37, "G\u00E9nero (Sexo):");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "select", 155);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_div_466_Template_select_ngModelChange_39_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r283); const bene_r190 = ctx.$implicit; return bene_r190.Ben_Sexo = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "option", 156);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](41, "MASCULINO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "option", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](43, "FEMENINO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](44, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](45, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](48, "(*) Numero documento");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](50, SeguroFormComponent_div_466_span_50_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](51, SeguroFormComponent_div_466_input_51_Template, 1, 1, "input", 158);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](53, "(*) Tipo:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](55, SeguroFormComponent_div_466_span_55_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](56, SeguroFormComponent_div_466_select_56_Template, 7, 1, "select", 159);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](57, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](59, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](60, "Extensi\u00F3n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](61, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](62, SeguroFormComponent_div_466_span_62_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](63, SeguroFormComponent_div_466_select_63_Template, 21, 1, "select", 160);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](65, "Complemento documento");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](66, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](67, SeguroFormComponent_div_466_span_67_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](68, SeguroFormComponent_div_466_input_68_Template, 1, 1, "input", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](69, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](71, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](72, "(*) Relaci\u00F3n:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](73, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](74, SeguroFormComponent_div_466_span_74_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](75, SeguroFormComponent_div_466_select_75_Template, 15, 1, "select", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](77, "(*) Porcentaje cobertura");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](78, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](79, SeguroFormComponent_div_466_span_79_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](80, SeguroFormComponent_div_466_input_80_Template, 1, 1, "input", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](81, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](83, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](84, "Tel\u00E9fonos de Contacto:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](85, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](86, SeguroFormComponent_div_466_span_86_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](87, SeguroFormComponent_div_466_input_87_Template, 1, 1, "input", 164);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](88, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](89, "(*) Tipo Beneficiario");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](90, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](91, SeguroFormComponent_div_466_span_91_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](92, SeguroFormComponent_div_466_select_92_Template, 5, 1, "select", 165);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](93, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](94, "div", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](95, "* Campos requeridos");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r190 = ctx.$implicit;
    const i_r191 = ctx.index;
    const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](36, _c1, ctx_r20.bens[i_r191]));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r20.bens[i_r191]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.bens[i_r191]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate5"](" \u00A0 ", bene_r190.Ben_Nombre, " ", bene_r190.Ben_Paterno, " ", bene_r190.Ben_Materno, " | ", bene_r190.Ben_Tipo, " | ", bene_r190.Ben_Porcentaje, " % ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.bens[i_r191] && ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](38, _c4, !ctx_r20.bens[i_r191], ctx_r20.bens[i_r191]));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r20.estado_codigo != "P")("ngModel", bene_r190.Ben_Sexo);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r20.estado_codigo == "P");
} }
function SeguroFormComponent_button_471_Template(rf, ctx) { if (rf & 1) {
    const _r286 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 202);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_471_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r286); const ctx_r285 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r285.addBeneficiario(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 203);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Adicionar Beneficiarios ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_button_473_Template(rf, ctx) { if (rf & 1) {
    const _r288 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_button_473_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r288); const ctx_r287 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r287.updateBeneficiarios(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Aceptar y Guardar ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r22.segBeneficiariosIdDeu.length == 0);
} }
function SeguroFormComponent_tr_484_td_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r289 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate3"]("Deudor : ", deu_r289.Deu_Nombre, " ", deu_r289.Deu_Paterno, " ", deu_r289.Deu_Materno, "");
} }
function SeguroFormComponent_tr_484_td_2_Template(rf, ctx) { if (rf & 1) {
    const _r296 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 205);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "button", 206);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_tr_484_td_2_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r296); const deu_r289 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r294 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r294.onImprimirV2(deu_r289.Deu_Id, deu_r289.id_documento_version); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Imprimir documentos");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SeguroFormComponent_tr_484_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, SeguroFormComponent_tr_484_td_1_Template, 2, 3, "td", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SeguroFormComponent_tr_484_td_2_Template, 5, 0, "td", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r289 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r289.Deu_Incluido == "S");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r289.Deu_Incluido == "S");
} }
function SeguroFormComponent_li_497_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const obs_r297 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", obs_r297, " ");
} }
function SeguroFormComponent_li_499_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li", 207);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const obs_r298 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", obs_r298, " ");
} }
function SeguroFormComponent_option_520_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option", 208);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const itm_r299 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngValue", itm_r299.autorizado_por);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", itm_r299.autorizado_por, " ");
} }
const _c5 = function () { return ["C1", "C2"]; };
const _c6 = function () { return ["C1", "C2", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8"]; };
const _c7 = function () { return ["A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8"]; };
class SeguroFormComponent {
    constructor(router, activatedRoute, sanitizer, msg, reporteService, segBeneficiariosService, ecofuturoService, segAprobacionesService, sendMailService, segObservacionesService, fileUploadService, authService, segSolicitudesService, segDeudoresService, segAdicionalesService) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.sanitizer = sanitizer;
        this.msg = msg;
        this.reporteService = reporteService;
        this.segBeneficiariosService = segBeneficiariosService;
        this.ecofuturoService = ecofuturoService;
        this.segAprobacionesService = segAprobacionesService;
        this.sendMailService = sendMailService;
        this.segObservacionesService = segObservacionesService;
        this.fileUploadService = fileUploadService;
        this.authService = authService;
        this.segSolicitudesService = segSolicitudesService;
        this.segDeudoresService = segDeudoresService;
        this.segAdicionalesService = segAdicionalesService;
        this.num_solicitud = 0;
        this.nro_solicitud = 0;
        this.id_folder = 0;
        this.id_prestamo = 0;
        this.numero_documento = 0;
        this.id_entidad_seguro = 0;
        this.titular = '';
        this.estado_general = '';
        this.estado_codigo = '';
        this.numero_poliza = '';
        this.tipo_cobertura = '';
        this.tipo_cartera = '';
        this.id_codeudor = 0;
        this.id_deudor = 0;
        this.id_cargo = 0;
        this.id_deudor_Selec = 0;
        this.deudor_nombre_sel = '';
        this.resolucion = { aprobado: false, rechazado: false,
            extra_prima: null, condiciones_exclusiones: null, causal: null };
        this.desistimiento = { causal: '', autorizado_por: '' };
        this.id_tipo_seguro = 0;
        this.file = null;
        this.download_url = src_environments_environment__WEBPACK_IMPORTED_MODULE_24__["environment"].URL_PUBLIC_FILES;
        this.tab_active = 0;
        this.openModal = 'none';
        this.openBen = 'none';
        this.openModalSeguros = 'none';
        this.openModalCodeu = 'none';
        this.openModalBene = 'none';
        this.openModalCuestion = 'none';
        this.openModalMensajes = 'none';
        this.openModalImprimir = 'none';
        this.openModalHistorial = 'none';
        this.openModalValida = 'none';
        this.openModalDesistir = 'none';
        this.display = '';
        this.showDocs = false;
        this.showDocs_1 = false;
        this.showDocs_2 = false;
        this.showImp = [false, false, false, false, false, false, false, false, false, false];
        this.solicitudValid = false;
        this.estadosSolicitud = [];
        this.segBeneficiarios = [];
        this.segBeneficiariosIdDeu = [];
        this.segObservaciones = [];
        this.segDeudores = [];
        this.segAprobaciones = [];
        this.deudorSeleccionado = new _models_seg_deudores__WEBPACK_IMPORTED_MODULE_22__["SegDeudores"]();
        this.autorizacionesSolicitud = [];
        this.benIdDeu = 0;
        this.beneficiarios = [];
        this.beneficiarioDe = '';
        // data:DataSolicitud = {} as DataSolicitud;
        this.data = {
            seg_solicitudes: new _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_17__["SegSolicitudes"](),
            seg_deudores: [new _models_seg_deudores__WEBPACK_IMPORTED_MODULE_22__["SegDeudores"]()],
            seg_operaciones: new _models_seg_operaciones__WEBPACK_IMPORTED_MODULE_23__["SegOperaciones"](),
            seg_beneficiarios: [new _models_seg_beneficiarios__WEBPACK_IMPORTED_MODULE_18__["SegBeneficiarios"]()],
            seg_adicionales: [new _models_seg_adicionales__WEBPACK_IMPORTED_MODULE_19__["SegAdicionales"]()],
            seg_aprobaciones: [new _models_seg_aprobaciones__WEBPACK_IMPORTED_MODULE_20__["SegAprobaciones"]()],
            seg_observaciones: [new _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_21__["SegObservaciones"]()],
            estado_general: '',
            config_param: {}
        };
        this.datosCalculados = {
            tasa_mensual_ref: 0.00,
            prima_mensual_ref: 0.00,
            entidad_seguros_1: "",
            entidad_seguros_2: "",
            saldo_actual_solicitado: 0.00
        };
        this.isEdit = true;
        this.codeudores_def = [];
        this.observaciones = [];
        this.advertencias = [];
        this.deus_ = ['none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none'];
        this.deus = [false, false, false, false, false, false, false, false, false, false];
        this.bens = [false, false, false, false, false, false, false, false, false, false];
        this.btns = [false, false, false, false, false, false, false, false, false, false];
        this.op = [true, false, false, false, false, false, false, false, false, false];
        this.enviarFinanciera = false;
        this.param_buscar = {
            accion: 'add-codeudor',
            numero_documento: '',
            id_folder: 0,
        };
        this.hoy = new Date();
        this.respuestaObser = '';
        this.offGuardarDeudor = true;
        this.offGuardarCoDeudor = true;
        this.viewTable = '';
        this.formData = new FormData();
        this.segObservacionesUlt = [new _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_21__["SegObservaciones"]()];
        this.esTarjeta = false;
        this.isLoading = false;
        this.esObligadosIncluidos = false;
        this.getDatosSolicitud = () => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                allowOutsideClick: false,
                text: 'Recuperando Información  ...'
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
            // recuperamos datos, deudor, codeudor, solicitud
            if (!this.id_solicitud) {
                this.id_solicitud = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
            }
            this.segSolicitudesService.getSolicitudIdSolAll(this.id_solicitud).subscribe(resp => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                const response = resp;
                if (response.status === 'OK') {
                    // init folder
                    this.data = response.data;
                    this.configParam = this.data.config_param;
                    //this.data.seg_solicitudes = response.data.seg_solicitudes;
                    //this.data.seg_deudores = response.data.seg_deudores;
                    //this.data.seg_beneficiarios = response.data.seg_beneficiarios;
                    //this.data.seg_observaciones = response.data.seg_observaciones;
                    //this.data.seg_adicionales = response.data.seg_adicionales;
                    //this.data.seg_aprobaciones = response.data.seg_aprobaciones;
                    // recuperando titulos
                    this.nro_solicitud = Number(this.data.seg_solicitudes.Sol_NumSol);
                    this.tipo_cartera = this.data.seg_solicitudes.Sol_TipoSeg;
                    this.estado_codigo = this.data.seg_solicitudes.Sol_EstadoSol;
                    const estadoSolicitud = this.configParam.estado_solicitud.find(row => row.codigo == this.estado_codigo);
                    if (estadoSolicitud) {
                        this.data.estado_general = estadoSolicitud.estado;
                    }
                    // verificar este campo si es nulo
                    if (!this.data.seg_solicitudes.Sol_SolTipoSol) {
                        this.getSolicitudPrimeraEtapa(this.nro_solicitud);
                    }
                    ;
                    // SEG OPERACIONES
                    if (response.data.seg_operaciones) {
                        this.data.seg_operaciones = response.data.seg_operaciones;
                    }
                    else {
                        this.getSolicitudSegundaEtapa(this.nro_solicitud);
                    }
                    ;
                    //verificamos la poliza
                    //if(!this.data.seg_solicitudes.Sol_Poliza){
                    //  this.segSolicitudesService.getUpdatePoliza(this.data.seg_solicitudes.Sol_IdSol).subscribe(resp=>{
                    //    const response = resp as { status: string; message:string ; messages: []; data: {} };
                    //    if (response.status=='OK'){  
                    //    }
                    //  });
                    //}
                    // BENEFICIARIOS
                    this.segBeneficiarios = [...this.data.seg_beneficiarios];
                    // ADICIONALES - DJS
                    // APROBACIONES
                    this.segAprobaciones = [...this.data.seg_aprobaciones];
                    // OBSERVACIONES
                    this.segObservaciones = [...this.data.seg_observaciones];
                    // CALCULO DE TASAS y Datos Completados 
                    this.datosCompletados();
                    this.sumaSaldoActualSolicitud();
                    this.solicitudValid = (this.data.seg_solicitudes.Sol_EstadoSol != 'P' ? true : false);
                    this.viewTable = this.getViewData(this.data.config_param);
                    //ultima comunicacion
                    if (['C1', 'C2'].includes(this.estado_codigo)) {
                        if (this.usuario_rol == 'of') {
                            this.segObservacionesUlt = this.data.seg_observaciones.filter(row => row.Obs_Eeff != '1');
                        }
                        else {
                            this.segObservacionesUlt = this.data.seg_observaciones.filter(row => row.Obs_Eeff == '1');
                        }
                    }
                    ;
                    //es tarjeta
                    if (this.data.seg_solicitudes.Sol_NumSol.toString().substring(0, 1) == '9') {
                        this.esTarjeta = true;
                    }
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                    this.msg.error(`Servicio principal : ${response.messages.toString()} `, 'Datos del Seguros');
                }
            }, ex => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                this.msg.error('No se pudo establecer comunicación', 'SOLICITUDES');
            });
        };
        // funciion DJS filtra declaraciones del deudor
        this.onDeclaracionDeudor = (deuId) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.declaraciones = [];
            this.openModalCuestion = 'block';
            const declara_codeu = yield this.data.seg_adicionales.filter(row => row.Adic_IdDeu == deuId);
            declara_codeu.sort((a, b) => parseInt(a.Adic_Pregunta) - parseInt(b.Adic_Pregunta));
            //this.declaraciones = JSON.parse(JSON.stringify(declara_codeu));
            this.declaraciones = [...declara_codeu];
        });
        this.onValidar = () => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.onGuardarDatosEdit();
            this.observaciones = [];
            this.advertencias = [];
            // datos requeridos SOLICITUD
            // Sol_MontoSol | Sol_MonedaSol | Sol_TipoSeg | Sol_TipoParam | Sol_Poliza | Sol_EstadoSol
            if (!this.data.seg_solicitudes.Sol_MontoSol) {
                this.observaciones.push(`Solicitud :  Monto solicitado requerido `);
            }
            ;
            if (!this.data.seg_solicitudes.Sol_MonedaSol) {
                this.observaciones.push(`Solicitud :  Moneda requerido `);
            }
            ;
            if (!this.data.seg_solicitudes.Sol_TipoSeg) {
                this.observaciones.push(`Solicitud :  Tipo de Seguro requerido `);
            }
            ;
            //if (!this.data.seg_solicitudes.Sol_TipoParam){this.observaciones.push(`Solicitud :  Tipo de Cobertura requerido `)};
            //if (!this.data.seg_solicitudes.Sol_Poliza){this.observaciones.push(`Solicitud :  Nro.Poliza requerido `)};
            if (!this.data.seg_solicitudes.Sol_EstadoSol) {
                this.observaciones.push(`Solicitud :  Estado Solicitud requerido `);
            }
            ;
            // datos requeridos DEUDORES
            // Deu_DetACtiv | Deu_CiudadNac | Deu_Peso | Deu_Talla | Deu_MontoActAcumVerif | Deu_Mano
            // saldo_actual_solicitud | Hist_TipoSeg
            const cant_obligados = this.data.seg_deudores.length;
            let cant_incluido = 0;
            // 4.CAMPOS REQUERIDOS EN DJS >> Adic_Respuesta: "S||||||||||"
            let djs = true; // DJS esta NORMAL
            this.data.seg_deudores.forEach(deudor => {
                //solo deudores incluidos
                if (deudor.Deu_Incluido == 'S') {
                    let hay_djs = false;
                    let hay_ben = false;
                    if (!deudor.Deu_CiudadNac) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Cuidad de Nacimiento requerido `);
                    }
                    ;
                    if (!deudor.Deu_Peso) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Peso (KGrs) requerido `);
                    }
                    ;
                    if (!deudor.Deu_Talla) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Talla (Cms) requerido `);
                    }
                    ;
                    if (!deudor.Deu_Mano) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Zurd@/Derecho@: requerido `);
                    }
                    ;
                    if (!deudor.Deu_DetACtiv) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Detalle de la Actividad requerido `);
                    }
                    ;
                    if (parseFloat(deudor.Deu_MontoActAcumVerif) < 0) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : SALDO ACTUAL VERIFICADO no valido `);
                    }
                    ;
                    if (!deudor.saldo_actual_solicitud) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : SALDO ACTUAL + SOLICITUD requerido `);
                    }
                    ;
                    const benDeudorObj = this.data.seg_beneficiarios.find(el => el.Ben_IdDeu == deudor.Deu_Id);
                    if (!benDeudorObj) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Beneficiarios requeridos `);
                    }
                    cant_incluido++;
                    // control de DJS
                    this.data.seg_adicionales.forEach(row => {
                        if (row.Adic_IdDeu == deudor.Deu_Id) {
                            hay_djs = true;
                            if (row.valor_si != row.valida_por) {
                                djs = false;
                                if (!row.Adic_Comment) {
                                    this.observaciones.push(`DJS ${this.getDeudor(row.Adic_IdDeu, 'nombres')} : Pregunta ${row.Adic_Pregunta}, requiere SI o NO o Aclaración `);
                                }
                            }
                        }
                    });
                    if (!hay_djs) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')}: DJS es requerido  `);
                    }
                    // validamos BENEFICIARIOS
                    let num_p = 0;
                    let num_c = 0;
                    this.data.seg_beneficiarios.forEach(el => {
                        console.log(el.Ben_IdDeu, deudor.Deu_Id);
                        if (el.Ben_IdDeu == deudor.Deu_Id) {
                            hay_ben = true;
                            if (el.Ben_Tipo == 'CONTINGENTE') {
                                num_c++;
                            }
                            ;
                            if (el.Ben_Tipo == 'PRIMARIO') {
                                num_p++;
                            }
                            ;
                        }
                    });
                    if (this.data.seg_solicitudes.Sol_TipoSeg == 'NO LICITADA') {
                        if (num_c == 0) {
                            this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')}: Debe completar los Beneficiarios CONTINGENTES `);
                        }
                        ;
                    }
                    if (num_p == 0) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')}: Debe completar los Beneficiarios PRIMARIOS  `);
                    }
                    ;
                    if (!hay_ben) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')}: Beneficiarios es requerido  `);
                    }
                }
            });
            // CONTROL DE OBLIGADOS
            console.log(`OBLIGADOS : ${cant_obligados} INCLUIDO : ${cant_incluido} `);
            if (!this.esObligadosIncluidos) {
                if (cant_obligados > 1 && cant_incluido <= 1) {
                    // this.observaciones.push(`Se requiere por lo menos un codeudor `);
                }
                ;
            }
            ;
            // HAY OBSERVACIONES
            if (this.observaciones.length > 0) {
                this.openModalValida = 'block';
                //this.solicitudValid = false;
            }
            else {
                this.msg.success('Validación satisfactoria ', 'VALIDACION');
                this.solicitudValid = true;
                // E S T A D O  D E  L A  S O L I C I T U D
                // FREE COVER O A+B+C
                const poliza = this.data.seg_solicitudes.Sol_Poliza;
                const cartera = this.data.seg_solicitudes.Sol_TipoSeg; // NO LICITADA | LICITADA
                const cobertura = this.data.seg_solicitudes.Sol_TipoParam; // VG | DH
                const monto = this.data.seg_solicitudes.Sol_MontoSol;
                const moneda = this.data.seg_solicitudes.Sol_MonedaSol;
                let nro_poliza; // = this.data.seg_solicitudes.Sol_Poliza;
                let estado_evaluado;
                let respuestasEstadosEvaluados = [];
                if (cartera == 'LICITADA') {
                    this.data.seg_deudores.forEach(deudor => {
                        if (deudor.Deu_Incluido == 'S') {
                            // k_param = (parseFloat(peso)+100)-parseFloat(talla);
                            // imc_param = parseFloat(peso)/((paserFloat(talla)/100)*(paserFloat(talla)/100))
                            const edad = deudor.Deu_Edad;
                            const peso = deudor.Deu_Peso;
                            const talla = deudor.Deu_Talla;
                            const saldo_actual_verificado_titular = deudor.Deu_MontoActAcumVerif;
                            const k_param = (parseFloat(peso) + 100) - parseFloat(talla);
                            const monto_saldo = monto + parseFloat(saldo_actual_verificado_titular);
                            nro_poliza = deudor.Deu_Poliza;
                            // DJS del deudor >> Adic_Respuesta: "S||||||||||"
                            let djs_deu = true; // DJS esta NORMAL
                            this.data.seg_adicionales.forEach(row => {
                                if (row.Adic_IdDeu == deudor.Deu_Id) {
                                    if (row.valor_si != row.valida_por) {
                                        djs_deu = false;
                                    }
                                }
                            });
                            console.log(`1.VALIDACION PARAMETROS :
            deudor : ${deudor.Deu_Nombre}
            cartera: ${cartera},
            cobertura: ${cobertura},
            monto_saldo:${monto_saldo},
            peso:${peso},
            talla:${talla},
            djs:${djs_deu},
            k:${k_param},
            estado_evaludado:${estado_evaluado}`);
                            console.log(deudor.Deu_Nombre, ':', cobertura, monto_saldo, k_param);
                            const estadoDeudor = this.configParam.estado_solicitud_licitada_update.find(row => (edad >= row.edad_min && edad <= row.edad_max) &&
                                (monto_saldo >= row.monto_desde && monto_saldo <= row.monto_hasta) &&
                                (k_param >= row.k_desde && k_param <= row.k_hasta) &&
                                djs_deu == row.djs && row.nro_poliza == nro_poliza);
                            respuestasEstadosEvaluados.push(estadoDeudor);
                            if (estadoDeudor) {
                                if (!estado_evaluado) {
                                    estado_evaluado = estadoDeudor.estado;
                                }
                                if (estadoDeudor.estado != 'A') {
                                    estado_evaluado = estadoDeudor.estado;
                                }
                            }
                            else {
                                estado_evaluado = '?';
                                this.msg.warning('Rangos NO DETERMINADOS para el Estado de la Solicitud ', 'VALIDACION');
                            }
                        }
                        ;
                    });
                }
                // djs, k , monto , imc
                if (cartera == 'NO LICITADA') {
                    this.data.seg_deudores.forEach(deudor => {
                        if (deudor.Deu_Incluido == 'S') {
                            // k_param = (parseFloat(peso)+100)-parseFloat(talla);
                            // imc_param = parseFloat(peso)/((paserFloat(talla)/100)*(paserFloat(talla)/100))
                            const peso = deudor.Deu_Peso;
                            const talla = deudor.Deu_Talla;
                            const edad = deudor.Deu_Edad;
                            const saldo_actual_verificado_titular = deudor.Deu_MontoActAcumVerif;
                            const k_param = (parseFloat(peso) + 100) - parseFloat(talla);
                            const imc_param = parseFloat(peso) / (parseFloat(talla) * (parseFloat(talla) / 10000));
                            const monto_saldo = monto + parseFloat(saldo_actual_verificado_titular);
                            const seguro_anterior = (deudor.Hist_TipoSeg ? deudor.Hist_TipoSeg : cobertura);
                            const seguro_actual = (deudor.Deu_cobertura ? deudor.Deu_cobertura : cobertura);
                            nro_poliza = deudor.Deu_Poliza;
                            // DJS del deudor >> Adic_Respuesta: "S||||||||||"
                            let djs_deu = true; // DJS esta NORMAL
                            this.data.seg_adicionales.forEach(row => {
                                if (row.Adic_IdDeu == deudor.Deu_Id) {
                                    if (row.valor_si != row.valida_por) {
                                        djs_deu = false;
                                    }
                                }
                            });
                            console.log(`1.VALIDACION PARAMETROS :
            deudor : ${deudor.Deu_Nombre}
            cartera: ${cartera},
            cobertura: ${cobertura},
            edad: ${edad},
            monto_saldo:${monto_saldo},
            peso:${peso},
            talla:${talla},
            djs:${djs_deu},
            k:${k_param},
            imc:${imc_param}
            estado_evaludado:${estado_evaluado}`);
                            const estadosSolicitudNoLicitada = this.configParam.estados_solicitud_no_licitada.find(row => row.nro_poliza == nro_poliza);
                            console.log(`2.VALICION RANGOS : `, estadosSolicitudNoLicitada);
                            if (estadosSolicitudNoLicitada) {
                                const factor = estadosSolicitudNoLicitada.factor;
                                if (factor == 'imc') {
                                    const estadosPoliza = estadosSolicitudNoLicitada.estados.find(row => (edad >= row.edad_min && edad <= row.edad_max) &&
                                        (monto_saldo >= row.monto_min && monto_saldo <= row.monto_max) &&
                                        (imc_param >= row.imc_desde && imc_param <= row.imc_hasta) &&
                                        djs_deu == row.djs);
                                    respuestasEstadosEvaluados.push(Object.assign(Object.assign({}, estadosPoliza), { deudor: `${deudor.Deu_Nombre} ${deudor.Deu_Paterno}`, id_deudor: deudor.Deu_Id }));
                                    console.log(`3.VALICION RESPUESTA ESTADO : `, estadosPoliza);
                                    if (estadosPoliza) {
                                        if (!estado_evaluado) {
                                            estado_evaluado = estadosPoliza.estado;
                                            console.log('3.1.set estado_evaludado :', estado_evaluado);
                                        }
                                        if (estadosPoliza.estado != 'A') {
                                            estado_evaluado = estadosPoliza.estado;
                                            console.log('3.2.set estado_evaludado :', estado_evaluado);
                                        }
                                    }
                                    else {
                                        estado_evaluado = '?';
                                        this.msg.error(`${deudor.Deu_Nombre},Rangos NO DETERMINADOS`, 'VALIDACION');
                                    }
                                }
                                else {
                                    const estadosPoliza = estadosSolicitudNoLicitada.estados.find(row => row.seguro_actual == seguro_actual &&
                                        row.seguro_anterior == seguro_anterior &&
                                        (monto_saldo >= row.monto_desde && monto_saldo <= row.monto_hasta) &&
                                        (k_param >= row.k_desde && k_param <= row.k_hasta) &&
                                        djs == row.djs);
                                    console.log(`4.VALICION RESPUESTA ESTADO : `, estadosPoliza);
                                    if (estadosPoliza) {
                                        if (!estado_evaluado) {
                                            estado_evaluado = estadosPoliza.estado;
                                            console.log('4.1.set estado_evaludado :', estado_evaluado);
                                        }
                                        if (estadosPoliza.estado != 'A') {
                                            estado_evaluado = estadosPoliza.estado;
                                            console.log('4.2.set estado_evaludado :', estado_evaluado);
                                        }
                                    }
                                    else {
                                        estado_evaluado = '?';
                                        this.msg.warning(`${deudor.Deu_Nombre},Rangos NO DETERMINADOS`, 'VALIDACION');
                                    }
                                }
                            }
                            else {
                                estado_evaluado = '?';
                                this.msg.warning('Rangos NO DETERMINADOS para la POLIZA ', 'VALIDACION');
                            }
                            ;
                        }
                        ;
                    });
                }
                console.log('Estado final estado_evaludado :', estado_evaluado);
                if (estado_evaluado) {
                    if (estado_evaluado == 'A') {
                        this.aprobacionAutomatica(estado_evaluado);
                    }
                    if (['C1', 'C2'].includes(estado_evaluado)) {
                        let mensajeFinal;
                        // DETERMINAR MENSAJE A LA COMPANIA
                        if (respuestasEstadosEvaluados.length > 0) {
                            mensajeFinal = 'Se esta determinando enviar a la compañia por: <br>';
                            respuestasEstadosEvaluados.forEach(row => {
                                if (['C1', 'C2'].includes(row.estado)) {
                                    if (!row.djs) {
                                        mensajeFinal += `Declaración de Salud tiene Obsevaciones <br>`;
                                    }
                                    if (row.imc_desde) {
                                        mensajeFinal += `Revisar los rangos de IMC <br>`;
                                    }
                                    if (row.k_desde) {
                                        mensajeFinal += `Revisar los los rangos del Factor  K <br>`;
                                    }
                                    if (row.monto_max) {
                                        mensajeFinal += `Excedio el monto de ${row.monto_max}  <br>`;
                                    }
                                    if (row.requisitos) {
                                        mensajeFinal += `Los requisitos son: ${row.requisitos}  <br>`;
                                    }
                                }
                                ;
                            });
                            console.log('6. MENSAJE FINAL', mensajeFinal);
                        }
                        ;
                        this.evaluacionPendiente(estado_evaluado, mensajeFinal);
                    }
                    if (estado_evaluado == 'R') {
                        const rechazo = respuestasEstadosEvaluados.find(el => el.estado == 'R');
                        if (rechazo) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                                icon: 'warning',
                                title: 'Advertencia!',
                                text: `${rechazo.deudor}, excedio los límites de asegurabilidad permitidos, por lo que este será excluido, para fines de la suscripción.`,
                                showCancelButton: true,
                                confirmButtonText: `Aceptar`,
                                cancelButtonText: `Cancelar`,
                                confirmButtonColor: '#16A085',
                                cancelButtonColor: '#F1948A',
                            }).then(result => {
                                if (result.isConfirmed) {
                                    if (rechazo.id_deudor) {
                                        console.log('EXCLUIR A : ', rechazo.id_deudor);
                                        this.data.seg_deudores.find(el => el.Deu_Id == rechazo.id_deudor).Deu_Incluido = 'N';
                                        this.esObligadosIncluidos = true;
                                    }
                                }
                            });
                        }
                    }
                }
                else {
                    this.msg.warning('No se pudo determinar el estado de la EVALUACION', 'EVALUACION');
                }
            }
            ;
        });
        this.calculate_age = (dob) => {
            var diff_ms = Date.now() - dob.getTime();
            var age_dt = new Date(diff_ms);
            return Math.abs(age_dt.getUTCFullYear() - 1970);
        };
        //  GESTION B E N E F I C I A R I O S
        this.loadBeneficiarios = () => {
            this.segBeneficiarios = [];
            //this.segBeneficiariosIdDeu = [];
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                allowOutsideClick: false,
                text: 'Recuperando datos ...'
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
            this.segBeneficiariosService.getIdSol(this.id_solicitud).subscribe(resp => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                const response = resp;
                if (response.status === 'OK') {
                    this.segBeneficiarios = [...response.data];
                }
                else {
                    this.msg.error(response.message);
                }
            }, err => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                this.msg.error('No se pudo establecer comunicacion', 'BENEFICIARIOS');
            });
        };
        // button BENEFICIARIOS (click)
        this.onOpenBeneficiarios = (idDeu, idBen) => {
            this.id_deudor_Selec = idDeu;
            console.log('ID BENEFICIARIO:', idDeu, this.id_deudor);
            this.deudorSeleccionado.Deu_Id = idDeu;
            const deudorBusca = this.data.seg_deudores.find(el => el.Deu_Id == idDeu);
            if (deudorBusca) {
                this.deudor_nombre_sel = `${deudorBusca.Deu_Nombre} ${deudorBusca.Deu_Paterno} ${deudorBusca.Deu_Materno}`;
            }
            this.segBeneficiariosIdDeu = [];
            this.benIdDeu = idDeu;
            const beneficiarios = this.data.seg_beneficiarios.filter(row => row.Ben_IdDeu == idDeu);
            //this.segBeneficiariosIdDeu = [...beneficiarios];
            this.segBeneficiariosIdDeu = JSON.parse(JSON.stringify(beneficiarios));
            this.segBeneficiariosIdDeu.map((el, idx) => {
                if (idBen) {
                    if (el.Ben_Id == idBen) {
                        console.log('idx', idx);
                        this.bens[idx] = true;
                    }
                    else {
                        this.bens[idx] = false;
                    }
                }
                else {
                    this.bens[idx] = false;
                }
            });
            this.openModalBene = 'block';
        };
        this.getDatosEco = () => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.id_solicitud) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                    allowOutsideClick: false,
                    text: 'Actualizando datos ...'
                });
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
                const model = { idsol: this.id_solicitud };
                this.segSolicitudesService.updateServiciosData(model).subscribe(resp => {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                    const response = resp;
                    console.log('update-servicios-data:', response);
                    if (response.status === 'OK') {
                        this.msg.success(`Se actualizo con exito la solicitud nro.${this.data.seg_solicitudes.Sol_NumSol}, por favor vuelva a ingresar `);
                        //this.router.navigate(['/AppMain/pnetseg/seguros/seguro-add']);
                        this.router.navigate(['/seguros/seguro-add']);
                    }
                    else {
                        this.msg.error(`No se pudo actualizar ${response.message} `);
                    }
                });
            }
        });
        this.onSaveResolucion = () => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                title: ` Emitir Resolución  ?`,
                showCancelButton: true,
                confirmButtonText: `Aceptar`,
                cancelButtonText: `Cancelar`,
                confirmButtonColor: '#16A085',
                cancelButtonColor: '#F1948A',
            }).then((result) => {
                if (result.isConfirmed) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                        allowOutsideClick: false,
                        text: 'Guardando datos ...'
                    });
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
                    let aprobaciones = [];
                    let aprobado = false;
                    let rechazado = false;
                    let codeudor = false;
                    let extra_prima = false;
                    let condiciones_exclusiones = false;
                    const resoluciones = this.configParam.estado_solicitud_resolucion;
                    console.log(this.data.seg_deudores);
                    // persiste el la tabla SegAprobaciones
                    this.data.seg_deudores.forEach(deu => {
                        const idDeu = deu.Deu_Id;
                        const idSol = deu.Deu_IdSol;
                        const esCode = deu.Deu_NIvel;
                        (deu.aprobado ? aprobado = true : null);
                        (deu.rechazado ? rechazado = true : null);
                        const extra = deu.extra_prima;
                        const condi = deu.condiciones_exclusiones;
                        const causal = deu.causal;
                        //determinar estado
                        (esCode == 1 ? codeudor = true : null);
                        (extra ? extra_prima = true : null);
                        (condi ? condiciones_exclusiones = true : null);
                        //registro para guardar
                        let aprobacion = new _models_seg_aprobaciones__WEBPACK_IMPORTED_MODULE_20__["SegAprobaciones"]();
                        aprobacion.Apr_IdDeu = deu.Deu_Id;
                        aprobacion.Apr_IdSol = deu.Deu_IdSol;
                        if (aprobado) {
                            aprobacion.Apr_Aprobado = 'S';
                            aprobacion.Apr_ExtraPrima = extra;
                            aprobacion.Apr_Condiciones = condi;
                        }
                        else {
                            if (rechazado) {
                                aprobacion.Apr_Aprobado = 'N';
                                aprobacion.Apr_Condiciones = causal;
                                deu.Deu_Incluido = 'N';
                            }
                            ;
                        }
                        console.log(aprobacion);
                        this.segAprobacionesService.add(aprobacion).subscribe(resp => {
                            const response = resp;
                            if (response.status == 'OK') {
                                this.data.seg_aprobaciones.push(aprobacion);
                            }
                        });
                    });
                    // determinar el estado
                    const estadoResol = resoluciones.find(row => row.aprobado == aprobado && row.rechazado == rechazado &&
                        row.codeudor == codeudor && row.extra_prima == extra_prima &&
                        row.condiciones_exclusiones == condiciones_exclusiones);
                    if (estadoResol) {
                        // guardar el estado de la solicitud
                        const estadoSol = this.configParam.estado_solicitud.find(row => row.codigo == estadoResol.estado);
                        this.estado_general = estadoSol.estado;
                        this.estado_codigo = estadoResol.estado;
                        this.data.seg_solicitudes.Sol_EstadoSol = estadoResol.estado;
                        let segSolicitudes = new _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_17__["SegSolicitudes"]();
                        segSolicitudes.Sol_IdSol = this.id_solicitud;
                        segSolicitudes.Sol_EstadoSol = estadoResol.estado;
                        this.setEstadoGeneral(estadoResol.estado);
                        this.guardaSegSolicitudes(segSolicitudes);
                        this.setFechaResolucion();
                        // crear la plantilla
                        let contenidoHtml = `
            <br><br>
            <table border="1">
              <tr><td rowspan="2" style="text-align:center;">CODEUDOR</td><td colspan="4" style="text-align:center;">RESULTADO DE LA EVALUACION</td></tr>
              <tr><td>APROBADO</td><td>EXTRAPRIMA</td><td>CONDICIONES Y EXCLUSIONES</td><td>CAUSAL RECHAZO</td></tr>
          `;
                        this.data.seg_deudores.forEach(deu => {
                            contenidoHtml += `<tr>
                <td>${deu.Deu_Nombre} ${deu.Deu_Paterno} ${deu.Deu_Materno}</td>
                <td>${deu.aprobado ? 'SI' : ''} ${deu.rechazado ? 'NO' : ''}</td>
                <td>${deu.extra_prima ? deu.extra_prima : '-'}</td>
                <td>${deu.condiciones_exclusiones ? deu.condiciones_exclusiones : '-'}</td>
                <td>${deu.causal ? deu.causal : ''}</td>
              </tr>`;
                        });
                        contenidoHtml += '</table>';
                        this.guardarObservaciones(estadoResol.estado);
                        this.onGuardarDatosEdit();
                        this.onSendMessage(estadoResol.estado, contenidoHtml);
                        this.msg.success('Operación se realizo con exito', 'RESOLUCION');
                    }
                    else {
                        this.msg.warning('No esta definido los estados de la resolucion', 'RESOLUCION');
                    }
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                }
            });
        });
        this.getTable = (content) => {
            const rows = content;
            let html = '<div class="w3-padding-16 w3-responsive"><table class="w3-table-all w3-tiny w3-hoverable">';
            html += '<thead><tr>';
            for (let campo in rows[0]) {
                html += '<th>' + campo + '</th>';
            }
            html += '</tr></thead><tbody>';
            for (let f = 0; f < rows.length; f++) {
                html += '<tr>';
                for (var j in rows[f]) {
                    const rowsObj = rows[f][j];
                    if (Array.isArray(rowsObj)) {
                        html += '<td> ' + this.getTable(rowsObj) + '</td>';
                    }
                    else {
                        html += '<td> ' + rowsObj + '</td>';
                    }
                }
            }
            html += '</tr></tbody></table></div>';
            return html;
        };
    }
    ngOnInit() {
        this.id_solicitud = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
        this.getDatosSolicitud();
        try {
            // get rol del usuario
            //this.userinfo =  this.authService.getUserInfo() as UserInfo;
            this.userinfo = this.authService.getItemSync('userInfo');
            if (this.userinfo) {
                this.usuario_rol = this.userinfo.rol[0].codigo; // of | aseguradora
            }
            else {
                this.msg.warning('Rol del usuario no definido', 'SOLICITUD');
            }
        }
        catch (error) {
            this.msg.warning('No se pudo recuperar la session del usuario', 'SOLICITUD');
        }
    }
    ngAfterViewInit() { }
    getSolicitudSegundaEtapa(num_solicitud) {
        this.ecofuturoService.getSolicitudSegundaEtapa(num_solicitud).subscribe(resp => {
            const response = resp;
            if (response.status == 'OK') {
                const datosOperacion = response.data.getSolicitudSegundaEtapaResult;
                this.data.seg_operaciones = datosOperacion;
            }
        }, ex => {
            console.error('Error en comunicacion, Segunda Etapa : recuperando Servicio del Banco');
        });
    }
    ;
    getSolicitudPrimeraEtapa(num_solicitud) {
        this.ecofuturoService.getSolicitudPrimeraEtapa(num_solicitud).subscribe(resp => {
            const response = resp;
            if (response.status == 'OK') {
                this.data.seg_solicitudes.Sol_SolTipoSol = response.data.getSolicitudPrimeraEtapaResult.Operacion_Tipo_Solicitud;
            }
        }, ex => {
            console.error('Error en comunicacion, Primera Etapa : recuperando Servicio del Banco');
        });
    }
    ;
    setEstadoGeneral(estado) {
        // estado actual de la solicitud
        if (estado) {
            this.data.estado_general = this.configParam.estado_solicitud.find(row => row.codigo == estado).estado;
        }
        else {
            console.error('Config Param : Estados de la Solicitud Vacio');
        }
        ;
    }
    ;
    // Calculo de tasas y completados
    datosCompletados() {
        this.estado_codigo = this.data.seg_solicitudes.Sol_EstadoSol;
        // Tasa Mensual Ref.  | Prima Mensual Ref. var conDecimal = numero.toFixed(2);
        try {
            const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
            if (nro_poliza) {
                const monto_solicitado = this.data.seg_solicitudes.Sol_MontoSol;
                const cant_obligados = this.data.seg_deudores.length;
                const tasas = this.configParam.datos_poliza.find(row => row.nro_poliza == nro_poliza);
                if (tasas) {
                    const tasaTitular = tasas.tasa_anual_ref;
                    const tasaCodeudor = tasas.tasa_anual_ref_2;
                    const tasa_mensual = parseFloat(tasaTitular) + (parseFloat(tasaCodeudor) * (cant_obligados - 1));
                    const prima_mensual = monto_solicitado * (tasa_mensual / 1000);
                    //let tasa_anual = this.data.seg_solicitudes.Sol_TasaSeg?parseFloat(this.data.seg_solicitudes.Sol_TasaSeg.toString()):0.00;
                    //let monto_solicitado = this.data.seg_solicitudes.Sol_MontoSol?this.data.seg_solicitudes.Sol_MontoSol:0.00;
                    //const tasa_mensual = tasa_anual/12;
                    //const prima_mensual = monto_solicitado * (tasa_mensual / 100);
                    this.datosCalculados.tasa_mensual_ref = tasa_mensual;
                    this.datosCalculados.prima_mensual_ref = prima_mensual;
                }
            }
        }
        catch (error) {
            console.error(error);
        }
        // Entidad Financiera
        if (this.data.seg_solicitudes.Sol_Poliza) {
            const nro_poliza = this.data.seg_solicitudes.Sol_Poliza;
            const entidadPoliza = this.configParam.datos_poliza.find(row => row.nro_poliza == nro_poliza);
            if (entidadPoliza) {
                this.datosCalculados.entidad_seguros_1 = entidadPoliza.entidad_seguros;
            }
        }
        //obtener djs, valor para validar djs 
        const tipo_seguro = this.data.seg_solicitudes.Sol_TipoSeg; //NO LICITADA |  LICITADA
        const djsData = this.configParam.djs_salud.filter(row => row.tipo_seguro == tipo_seguro);
        this.data.seg_adicionales.forEach(row => {
            row.valida_por = djsData.find(el => el.nro == row.Adic_Pregunta).valida_por;
            if (row.Adic_Respuesta) {
                if (row.Adic_Respuesta == 'S||||||||||') {
                    row.valor_si = true;
                    row.valor_no = false;
                }
                if (row.Adic_Respuesta == '|S|||||||||') {
                    row.valor_si = false;
                    row.valor_no = true;
                }
            }
        });
        this.autorizacionesSolicitud = this.configParam.autorizaciones_solicitud;
    }
    sumaSaldoActualSolicitud() {
        try {
            // Saldo Actual Solicitado =  this.datosCalculados.saldo_actual_solicitado
            const monto_solicitado = this.data.seg_solicitudes.Sol_MontoSol;
            const moneda = this.data.seg_solicitudes.Sol_MonedaSol;
            const tipo_seguro = this.data.seg_solicitudes.Sol_TipoSeg; // NO LICITADA | LICITADA
            const nro_poliza = this.data.seg_solicitudes.Sol_Poliza;
            let cobertura_sol = '';
            this.data.seg_deudores.forEach((deudor) => {
                const saldo_actual_verificado = parseFloat(deudor.Deu_MontoActAcumVerif);
                const saldo_actual_solicitud = monto_solicitado + saldo_actual_verificado;
                deudor['saldo_actual_solicitud'] = saldo_actual_solicitud;
                // determinar cobertura
                if (tipo_seguro) {
                    //const cobertura = this.configParam.tipos_cobertura.find(row=> 
                    //saldo_actual_solicitud  >= row.monto_desde && saldo_actual_solicitud  <= row.monto_hasta
                    //&& row.moneda == moneda && row.nro_poliza == nro_poliza && row.tipo_seguro==tipo_seguro);
                    console.log(`DETERMINAR COBERTURA: 
              monto_solicitado : ${monto_solicitado}
              saldo_actual_verificado : ${saldo_actual_verificado} 
              saldo_actual_solicitud : ${saldo_actual_solicitud} 
              tipo seguro : ${tipo_seguro} 
              nro_poliza : ${nro_poliza} `);
                    const cobertura = this.configParam.tipos_cobertura.find(row => (monto_solicitado >= row.monto_desde && monto_solicitado <= row.monto_hasta)
                        && (saldo_actual_solicitud >= row.cumulo_desde && saldo_actual_solicitud <= row.cumulo_hasta)
                        && row.moneda == moneda && row.tipo_seguro == tipo_seguro);
                    if (cobertura) {
                        deudor.Deu_cobertura = cobertura.tipo_cobertura;
                        if (this.data.seg_solicitudes.Sol_TipoSeg == 'NO LICITADA') {
                            deudor.id_documento_version = (cobertura.tipo_cobertura == 'VG' ? 13 : 14);
                        }
                        ;
                        if (this.data.seg_solicitudes.Sol_TipoSeg == 'LICITADA') {
                            deudor.id_documento_version = 27;
                        }
                        ;
                        deudor.Deu_Poliza = cobertura.nro_poliza;
                    }
                    else {
                        this.msg.warning(`${deudor.Deu_Nombre} : No se pudo determinar la cobertura `);
                    }
                }
            });
        }
        catch (error) {
            console.error(error);
        }
    }
    onGuardarDatosEdit() {
        this.sumaSaldoActualSolicitud();
        //Datos deudores editados
        let deudores = [];
        this.data.seg_deudores.forEach(deudor => {
            let deudorObj = {};
            deudorObj['Deu_Id'] = deudor.Deu_Id;
            deudorObj['Deu_CiudadNac'] = deudor.Deu_CiudadNac;
            deudorObj['Deu_Peso'] = deudor.Deu_Peso;
            deudorObj['Deu_Talla'] = deudor.Deu_Talla;
            deudorObj['Deu_DetACtiv'] = deudor.Deu_DetACtiv;
            deudorObj['Deu_Mano'] = deudor.Deu_Mano;
            deudorObj['Deu_MontoActAcumVerif'] = deudor.Deu_MontoActAcumVerif;
            deudorObj['Deu_Incluido'] = deudor.Deu_Incluido;
            deudorObj['Deu_cobertura'] = deudor.Deu_cobertura;
            deudorObj['id_documento_version'] = deudor.id_documento_version;
            deudorObj['Deu_Poliza'] = deudor.Deu_Poliza;
            deudorObj['Deu_TasaSeg'] = deudor.Deu_TasaSeg;
            deudorObj['Deu_PrimaSeg'] = deudor.Deu_PrimaSeg;
            deudores.push(deudorObj);
        });
        this.segDeudoresService.updateDeudores(deudores).subscribe(resp => {
            console.log('Respuesta la guardar deudores:', resp);
            const response = resp;
            if (response.status == 'OK') {
                this.msg.success('Datos de los deudores se guardaron');
            }
            else {
                this.msg.error('No se pudeieron guardar los datos de los deudores');
            }
        });
    }
    ;
    onDesistir() {
        if (this.desistimiento.causal && this.desistimiento.autorizado_por) {
            const model = {
                causal: this.desistimiento.causal,
                autorizado_por: this.desistimiento.autorizado_por,
                idsol: this.data.seg_solicitudes.Sol_IdSol,
                usuario: `${this.authService.usuario.usuario_nombre_completo} `
            };
            this.segSolicitudesService.desistimientoSolicitud(model).subscribe(resp => {
                const response = resp;
                if (response.status == 'OK') {
                    this.msg.success('La Solicitud Desistio con exito', 'SOLICITUD');
                    ///seguros/seguro-add
                    this.router.navigate(['/seguros/seguro-add']);
                }
                else {
                    this.msg.error('La Solicitud no se pudo Desistir', 'SOLICITUD');
                }
            });
        }
        else {
            this.msg.error('Causal y Autorizado por , requeridos', 'Desistimiento');
        }
    }
    ;
    onEliminarSolicitud() {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: `¿ESTÁ USTED SEGURO DE ELIMINAR LA SOLICITUD ${this.data.seg_solicitudes.Sol_NumSol} ?`,
            showCancelButton: true,
            confirmButtonText: `Eliminar`,
            cancelButtonText: `Cancelar`,
            confirmButtonColor: '#16A085',
            cancelButtonColor: '#F1948A',
        }).then((result) => {
            if (result.isConfirmed) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                    allowOutsideClick: false,
                    text: 'Eliminando la Solicitud ...'
                });
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
                const id = this.data.seg_solicitudes.Sol_IdSol;
                this.segSolicitudesService.delete(id).subscribe(resp => {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                    const response = resp;
                    if (response.status == 'OK') {
                        this.msg.info('La Solicitud se borro con exito', 'SOLICITUD');
                        ///seguros/seguro-add
                        this.router.navigate(['/AppMain/pnetseg/seguros/seguro-add']);
                    }
                    else {
                        this.msg.error('La Solicitud no se pudo borrar', 'SOLICITUD');
                    }
                });
            }
        });
    }
    // funcion DJS cuestionario
    onAceptarCuestionario() {
        let aceptar = true;
        let messages = [];
        for (const row of this.declaraciones) {
            if (row.valida_por) {
                if (row.valor_si) {
                    row.Adic_Respuesta = 'S||||||||||';
                    row.Adic_Comment = '';
                }
                else {
                    if (row.valor_no) {
                        if (row.Adic_Comment) {
                            row.Adic_Respuesta = '|S|||||||||';
                        }
                        else {
                            messages.push(`Pregunta ${row.Adic_Pregunta}, requiere Aclaración`);
                        }
                    }
                    else {
                        messages.push(`Pregunta ${row.Adic_Pregunta}, requiere SI o NO`);
                    }
                }
            }
            else {
                if (row.valor_no) {
                    row.Adic_Respuesta = '|S|||||||||';
                    row.Adic_Comment = '';
                }
                else {
                    if (row.valor_si) {
                        if (row.Adic_Comment) {
                            row.Adic_Respuesta = 'S||||||||||';
                        }
                        else {
                            messages.push(`Pregunta ${row.Adic_Pregunta}, requiere Aclaración`);
                        }
                    }
                    else {
                        messages.push(`Pregunta ${row.Adic_Pregunta}, requiere SI o NO`);
                    }
                }
            }
        }
        ;
        if (messages.length > 0) {
            this.msg.error(messages.toString(), 'Cuestionario');
        }
        else {
            console.log('Declaraciones', this.declaraciones);
            // guardar adicionales
            this.segAdicionalesService.updateAdiconales(this.declaraciones).subscribe(resp => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                console.log('Response Update declaraciones:', resp);
                const response = resp;
                if (response.status == 'OK') {
                    this.msg.success('Se Guardo con exito', 'DJS');
                    /*
                    for(const row of this.declaraciones){
                      const djsObj = this.data.seg_adicionales.find(el=>el.Adic_Id == row.Adic_Id);
                      
                      djsObj.Adic_Respuesta = row.Adic_Respuesta;
                      djsObj.Adic_Texto = row.Adic_Texto;
                      console.log(djsObj);
                    }
                    */
                }
                else {
                    this.msg.error(response.messages.toString(), 'DJS');
                }
            });
            this.openModalCuestion = 'none';
        }
        ;
    }
    onAclaracion(preg) {
        let esComentario = false;
        if (this.estado_codigo == 'P' && (preg.valida_por != preg.valor_si)) {
            esComentario = true;
        }
        return esComentario;
    }
    getEif(eif) {
        let eif_texto = "";
        if (eif == '1') {
            eif_texto = "Banco PyME Ecofuturo S.A.";
        }
        return eif_texto;
    }
    onChangeInputDeu(valor, campo) {
        //const segDeudor = this.folderSolicitud.seg_deudores.filter((row) => row.Deu_NIvel == 0)[0];
        const segDeudor = this.segDeudores.filter(row => row.Deu_Id == this.id_deudor)[0];
        if (segDeudor[campo]) {
            if (segDeudor[campo] != valor) {
                this.offGuardarDeudor = false;
            }
            else {
                this.offGuardarDeudor = true;
            }
        }
        else {
            if (valor) {
                this.offGuardarDeudor = false;
            }
            else {
                this.offGuardarDeudor = true;
            }
        }
    }
    ;
    onChangeInputCoDeu(valor, campo, origen) {
        //const segDeudor = this.folderSolicitud.seg_deudores.filter((row) => row.Deu_Id == origen.Deu_Id)[0];
        const segDeudor = this.segDeudores.filter(row => row.Deu_Id == origen.Deu_Id)[0];
        if (segDeudor[campo]) {
            if (segDeudor[campo] != valor) {
                this.offGuardarCoDeudor = false;
            }
            else {
                this.offGuardarCoDeudor = true;
            }
        }
        else {
            if (valor) {
                this.offGuardarCoDeudor = false;
            }
            else {
                this.offGuardarCoDeudor = true;
            }
        }
    }
    ;
    dosPasos() {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
            confirmButtonText: 'siguiente',
            confirmButtonColor: '#16A085',
            showCancelButton: true,
            cancelButtonColor: '#F1948A',
            progressSteps: ['1', '2']
        }).queue([
            {
                title: 'Advertencia!',
                text: 'MARIANO ROJAS, excedio los límites de asegurabilidad permitidos, por lo que este será excluido, para fines de la suscripción.'
            },
            'Observaciones'
        ]).then((result) => {
            if (result.value) {
                console.log(result.value);
            }
        });
    }
    aprobacionAutomatica(estado_evaluado) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            icon: 'question',
            html: `LA SOLICITUD SE APROBARA AUTOMATICAMENTE ?`,
            showCancelButton: true,
            confirmButtonText: `Aceptar`,
            cancelButtonText: `Cancelar`,
            confirmButtonColor: '#16A085',
            cancelButtonColor: '#F1948A',
        }).then((result) => {
            if (result.isConfirmed) {
                this.msg.success('Solicitud Aprobada', 'SOLICITUD EN PROCESO');
                // cambiar estado a la solicitud
                this.data.seg_solicitudes.Sol_EstadoSol = estado_evaluado;
                let segSolicitudes = new _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_17__["SegSolicitudes"]();
                segSolicitudes.Sol_IdSol = this.data.seg_solicitudes.Sol_IdSol;
                segSolicitudes.Sol_EstadoSol = estado_evaluado;
                this.estado_codigo = estado_evaluado;
                this.data.seg_solicitudes.Sol_EstadoSol = estado_evaluado;
                // Guardar fecha de aprobacion automatica Deu_FechaResolucion
                this.setFechaResolucion();
                this.guardaSegSolicitudes(segSolicitudes);
                this.setEstadoGeneral(estado_evaluado);
                this.guardarObservaciones(estado_evaluado);
                //this.onSendMessage(estado_evaluado);
                this.onSendMessage_A();
            }
        });
    }
    evaluacionPendiente(estado_evaluado, msg) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            icon: 'question',
            html: `SOLICITUD SERA ENVIADA PREVIAMENTE A LA COMPAÑIA Y DEBE SER APROBADA POR ELLA`,
            showCancelButton: true,
            confirmButtonText: `Aceptar`,
            cancelButtonText: `Cancelar`,
            confirmButtonColor: '#16A085',
            cancelButtonColor: '#F1948A',
        }).then((result) => {
            if (result.isConfirmed) {
                this.msg.warning('Solicitud en evaluación', 'SOLICITUD EN PROCESO');
                // cambiar estado a la solicitud
                this.data.seg_solicitudes.Sol_EstadoSol = estado_evaluado;
                let segSolicitudes = new _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_17__["SegSolicitudes"]();
                segSolicitudes.Sol_IdSol = this.data.seg_solicitudes.Sol_IdSol;
                segSolicitudes.Sol_EstadoSol = estado_evaluado;
                this.estado_codigo = estado_evaluado;
                this.setEstadoGeneral(estado_evaluado);
                this.guardaSegSolicitudes(segSolicitudes);
                this.guardarObservaciones(estado_evaluado, msg);
                //this.onSendMessage(estado_evaluado,msg);
                this.onSendMessage_C1_C2(estado_evaluado, msg);
            }
        });
    }
    ;
    // Deu_FechaResolucion
    setFechaResolucion() {
        const fechaActual = getDateYYYYmmDD();
        let deudores = [];
        this.data.seg_deudores.forEach(row => {
            let deudorObj = {};
            row.Deu_FechaResolucion = fechaActual;
            deudorObj['Deu_Id'] = row.Deu_Id;
            deudorObj['Deu_FechaResolucion'] = fechaActual;
            deudores.push(deudorObj);
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            allowOutsideClick: false,
            text: 'Registrando fecha de resolución  ...'
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
        this.segDeudoresService.updateDeudores(deudores).subscribe(resp => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
            const response = resp;
            if (response.status == 'OK') {
                this.msg.success('Se registro la fecha de resolución con exito ');
            }
            else {
                this.msg.error('No se pudo registrar la fecha de resolución ');
            }
            console.log('Respuesta la guardar deudores:', response);
        });
    }
    onSendMessage_A() {
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        let messageHtml = '';
        const estado_evaluado = 'A';
        if (nro_poliza) {
            this.data.seg_deudores.forEach(row => {
                if (row.Deu_Incluido == 'S') {
                    messageHtml += `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Deudor ${row.Deu_Nombre} ${row.Deu_Paterno} ${row.Deu_Materno} Aprobada Automáticamente <br>`;
                }
            });
            messageHtml += `Enviado por: ${this.data.seg_solicitudes.Sol_Oficial} `;
            //const msgObj = this.configParam.notificaciones_mensajes.find(row=>
            //  row.estado_evaluado == 'A' && row.nro_poliza.split(',').includes(nro_poliza)
            //);
            const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                && row.nro_poliza.split(',').includes(nro_poliza));
            if (correosEstados) {
                //[year, month, day].join('-');
                // envio de correo
                let message = new _models_message_email__WEBPACK_IMPORTED_MODULE_25__["MessageEmail"]();
                message.to = correosEstados.emails;
                message.cc = correosEstados.cc;
                message.subject = `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} `;
                message.html = messageHtml;
                this.sendMailService.sendMessage(message).subscribe(resp => {
                    const response = resp;
                    console.log('envio de mensajes', response);
                });
            }
            else {
                this.msg.warning('No estan definidos las notificaciones');
            }
        }
        ;
    }
    onSendMessage_C1_C2(estado_evaluado, msg) {
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        let messageHtml = '';
        if (nro_poliza) {
            //const msgObj = this.configParam.notificaciones_mensajes.find(row=>
            //  row.estado_evaluado == estado_evaluado && row.nro_poliza.split(',').includes(nro_poliza)
            //);
            const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                && row.nro_poliza.split(',').includes(nro_poliza));
            if (correosEstados) {
                messageHtml += `${this.data.seg_solicitudes.Sol_Oficial} , Solicita su atencion a la Solicitud ${this.data.seg_solicitudes.Sol_NumSol}
        de la Endidad Financiera Banco PyME Ecofuturo S.A. <br> 
        RAZON DE LA SOLICITUD: <br>  ${msg}  <br>`;
                messageHtml += `Enviado por: ${this.data.seg_solicitudes.Sol_Oficial} `;
                // envio de correo
                let message = new _models_message_email__WEBPACK_IMPORTED_MODULE_25__["MessageEmail"]();
                message.to = correosEstados.emails;
                message.cc = correosEstados.cc;
                message.subject = `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} - Banco PyME Ecofuturo S.A. - ${this.data.seg_solicitudes.Sol_Oficial} `;
                message.html = messageHtml;
                this.sendMailService.sendMessage(message).subscribe(resp => {
                    const response = resp;
                    console.log('envio de mensajes', response);
                });
            }
            else {
                this.msg.warning('No estan definidos las notificaciones');
            }
        }
    }
    onSendMessage(estado_evaluado, msg) {
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        let messageHtml = '';
        if (nro_poliza) {
            const msgObj = this.configParam.notificaciones_mensajes.find(row => row.estado_evaluado == estado_evaluado && row.nro_poliza.split(',').includes(nro_poliza));
            const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                && row.nro_poliza.split(',').includes(nro_poliza));
            if (correosEstados) {
                // envio de correo
                let message = new _models_message_email__WEBPACK_IMPORTED_MODULE_25__["MessageEmail"]();
                message.to = correosEstados.emails;
                message.cc = correosEstados.cc;
                message.subject = `Pronunciamiento Compañia sobre la Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A.`;
                message.html = `Pronunciamiento Compañia sobre Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A. <br>
         ${msgObj.asunto} <br>
         Enviado por ${this.userinfo.usuario_nombre_completo}`;
                this.sendMailService.sendMessage(message).subscribe(resp => {
                    const response = resp;
                    console.log('envio de mensajes', response);
                });
            }
            else {
                this.msg.warning('No estan definidos las notificaciones');
            }
        }
        ;
    }
    ;
    getEifAseguradora(cod) {
        // Banco PyME Ecofuturo S.A. = 1 |  Asegurador Alianza = 101 
        // Asegurador La Boliviana Ciacruz = 20001,20002,20006,20007
        //const asegurador = this.configParam.datos_poliza.find(row=>row.nro_poliza==nro_poliza).entidad_seguros;
        let asegurador;
        if (cod) {
            if (cod == '1') {
                asegurador = 'Banco PyME Ecofuturo S.A.';
            }
            ;
            if (cod == '101') {
                asegurador = 'Asegurador Alianza';
            }
            ;
            const datosPoliza = this.configParam.datos_poliza.find(row => row.nro_poliza == cod);
            if (datosPoliza) {
                asegurador = datosPoliza.entidad_seguros;
            }
        }
        return asegurador;
    }
    guardarObservaciones(estado_evaluado, msg) {
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        console.log('USUARIO_ROL:', this.usuario_rol);
        if (nro_poliza) {
            const msgObj = this.configParam.notificaciones_mensajes.find(row => row.estado_evaluado == estado_evaluado && row.nro_poliza.split(',').includes(nro_poliza));
            if (msgObj) {
                let obser = new _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_21__["SegObservaciones"]();
                obser.Obs_IdSol = this.data.seg_solicitudes.Sol_IdSol;
                obser.Obs_Usuario = this.userinfo.usuario_nombre_completo;
                obser.Obs_Observacion = (msg ? `${msgObj.mensaje_historial} ${msg} ` : `${msgObj.mensaje_historial} `);
                if (this.usuario_rol == 'of') {
                    obser.Obs_Eeff = '1';
                    obser.Obs_Aseg = '';
                }
                else {
                    obser.Obs_Eeff = '';
                    obser.Obs_Aseg = nro_poliza;
                }
                ;
                this.segObservacionesService.add(obser).subscribe(resp => {
                    const response = resp;
                    if (response.status == 'OK') {
                        this.data.seg_observaciones.push(response.data);
                    }
                });
            }
            else {
                this.msg.warning('No estan definidos las notificaciones');
            }
        }
    }
    ;
    guardaSegSolicitudes(model) {
        this.segSolicitudesService.update(model).subscribe(resp => {
            const response = resp;
            if (response.status == 'OK') {
                this.msg.success('Se actualizo la solicitud', 'SOLICITUD');
            }
            else {
                this.msg.error('No se pudo guardar datos de la Solicitud', 'SOLICITUD');
            }
        });
    }
    ;
    getSegObservaciones(idSol) {
        this.segObservacionesService.getIdSol(idSol).subscribe(resp => {
            const response = resp;
            if (response.status == 'OK') {
                this.segObservaciones = response.data;
            }
        });
    }
    ;
    onCloseBuscarCodeudor(result) {
        this.openModalCodeu = 'none';
        this.param_buscar.numero_documento = '';
        this.param_buscar.accion = 'add-codeudor';
        this.param_buscar.id_folder = this.id_folder;
        if (result) {
            //this.getDatosFolder();
        }
    }
    addBeneficiario() {
        const newBen = new _models_seg_beneficiarios__WEBPACK_IMPORTED_MODULE_18__["SegBeneficiarios"]();
        newBen.Ben_IdSol = this.data.seg_solicitudes.Sol_IdSol;
        newBen.Ben_IdDeu = this.id_deudor_Selec;
        newBen.Ben_Porcentaje = "100";
        this.segBeneficiariosIdDeu.push(newBen);
        const len = this.segBeneficiariosIdDeu.length;
        for (let i = 0; i < len; i++) {
            this.bens[i] = false;
        }
        this.bens[len - 1] = true;
    }
    ;
    onBeneficiariosValidarCerrar() {
        let valido = true;
        let obser = [];
        let porcentaje_p = 0;
        let porcentaje_c = 0;
        let nro_p = 0;
        let nro_c = 0;
        // validacion del beneficiarios
        this.segBeneficiariosIdDeu.forEach(ben => {
            if (ben.Ben_Tipo == 'PRIMARIO') {
                const num = parseInt(ben.Ben_Porcentaje);
                if (num == 0) {
                    obser.push('Porcentaje no puede ser cero');
                }
                porcentaje_p += num;
                nro_p++;
            }
            if (ben.Ben_Tipo == 'CONTINGENTE') {
                const num = parseInt(ben.Ben_Porcentaje);
                if (num == 0) {
                    obser.push('Porcentaje no puede ser cero');
                }
                porcentaje_c += num;
                nro_c++;
            }
            if (!ben.Ben_Nombre) {
                obser.push('Nombre completo es requerido');
            }
            ;
            if (!ben.Ben_NumDoc) {
                obser.push('Numero de documento es requerido');
            }
            ;
            if (!ben.Ben_TipoDoc) {
                obser.push('Tipo de documento es requerido');
            }
            ;
            if (!ben.Ben_Relacion) {
                obser.push('Relación es requerido');
            }
            ;
            if (!ben.Ben_Porcentaje) {
                obser.push('Porcentaje es requerido');
            }
            ;
            if (!ben.Ben_Tipo) {
                obser.push('Tipo de Beneficiario es requerido');
            }
            ;
            if (!ben.Ben_Paterno) {
                if (!ben.Ben_Materno) {
                    obser.push('Por la menos un Apellido es requerido');
                }
            }
        });
        if (nro_p > 0) {
            if (porcentaje_p != 100) {
                obser.push(`Porcentaje PRIMARIA es ${porcentaje_p} debe ser 100%`);
            }
        }
        if (nro_c > 0) {
            if (porcentaje_c != 100) {
                obser.push(`Porcentaje CONTINGENTE es ${porcentaje_c} debe ser 100%`);
            }
        }
        if (obser.length > 0) {
            valido = false;
        }
        if (valido) {
            this.openModalBene = 'none';
        }
        else {
            this.msg.error(obser.toString(), 'Beneficiarios');
        }
    }
    updateBeneficiarios() {
        let valido = true;
        let obser = [];
        let porcentaje_p = 0;
        let porcentaje_c = 0;
        let nro_p = 0;
        let nro_c = 0;
        // validacion del beneficiarios
        this.segBeneficiariosIdDeu.forEach(ben => {
            if (ben.Ben_Tipo == 'PRIMARIO') {
                const num = parseInt(ben.Ben_Porcentaje);
                if (num == 0) {
                    obser.push('Porcentaje no puede ser cero');
                }
                porcentaje_p += num;
                nro_p++;
            }
            if (ben.Ben_Tipo == 'CONTINGENTE') {
                const num = parseInt(ben.Ben_Porcentaje);
                if (num == 0) {
                    obser.push('Porcentaje no puede ser cero');
                }
                porcentaje_c += num;
                nro_c++;
            }
            if (!ben.Ben_Nombre) {
                obser.push('Nombre completo es requerido');
            }
            ;
            if (!ben.Ben_Relacion) {
                obser.push('Relación es requerido');
            }
            ;
            if (!ben.Ben_Porcentaje) {
                obser.push('Porcentaje es requerido');
            }
            ;
            if (!ben.Ben_Tipo) {
                obser.push('Tipo de Beneficiario es requerido');
            }
            else {
                if (ben.Ben_Tipo == 'PRIMARIO') {
                    if (!ben.Ben_TipoDoc) {
                        obser.push('Tipo de documento es requerido');
                    }
                    ;
                    if (!ben.Ben_NumDoc) {
                        obser.push('Numero de documento es requerido');
                    }
                    ;
                }
            }
            ;
            if (!ben.Ben_Paterno) {
                if (!ben.Ben_Materno) {
                    obser.push('Por la menos un Apellido es requerido');
                }
            }
        });
        if (nro_p > 0) {
            if (porcentaje_p != 100) {
                obser.push(`Porcentaje PRIMARIO es ${porcentaje_p} debe ser 100%`);
            }
        }
        if (nro_c > 0) {
            if (porcentaje_c != 100) {
                obser.push(`Porcentaje CONTINGENTE es ${porcentaje_c} debe ser 100%`);
            }
        }
        if (obser.length > 0) {
            valido = false;
        }
        if (valido) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                allowOutsideClick: false,
                text: 'Guardando datos de Beneficiarios ...'
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
            const idSol = this.data.seg_solicitudes.Sol_IdSol;
            this.segBeneficiariosService.updateAll(this.segBeneficiariosIdDeu, idSol).subscribe(resp => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                const response = resp;
                console.log('updateAll:', response);
                if (response.status == 'OK') {
                    this.openModalBene = 'none';
                    this.msg.success('Beneficiarios se guardaron con exito');
                    this.data.seg_beneficiarios = response.data;
                }
                else {
                    this.msg.error(`Error guardando beneficiarios ${response.messages}`);
                }
            }, ex => { this.msg.error('No se pudo establecer comunicacion', 'Beneficiarios'); });
        }
        else {
            this.msg.error(obser.toString(), 'Beneficiarios');
        }
    }
    ;
    onDelBeneficiario(idBen, index) {
        console.log('onDelBeneficiario:', idBen, index);
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: 'No Incluir Beneficiario',
            showCancelButton: true,
            confirmButtonText: `Aceptar`,
            cancelButtonText: `Cancelar`,
            confirmButtonColor: '#16A085',
            cancelButtonColor: '#F1948A',
        }).then((result) => {
            if (result.isConfirmed) {
                if (idBen) {
                    //borrar en el servicios
                    this.segBeneficiariosService.delete(idBen).subscribe(resp => {
                        const response = resp;
                        if (response.status == 'OK') {
                            // borra filtro y desenfoca la seleccion
                            this.segBeneficiariosIdDeu.map((el, idx) => {
                                if (el.Ben_Id == idBen) {
                                    this.segBeneficiariosIdDeu.splice(index, 1);
                                }
                                this.bens[idx] = false;
                                // actualiza data
                                let benList = this.data.seg_beneficiarios;
                                for (var i = 0; i < benList.length; i++) {
                                    if (benList[i].Ben_Id === idBen) {
                                        benList.splice(i, 1);
                                    }
                                }
                            });
                            this.msg.success('Beneficiario fue excluido con exito', 'BENEFICIARIOS');
                        }
                        else {
                            this.msg.error('Beneficiario no pudo ser excluido con exito', 'BENEFICIARIOS');
                        }
                    });
                }
                else {
                    this.segBeneficiariosIdDeu.map((el, idx) => {
                        this.segBeneficiariosIdDeu.splice(index, 1);
                        this.bens[idx] = false;
                    });
                }
            }
        });
    }
    getDeudor(id, campo) {
        const ben = this.data.seg_deudores.find(el => el.Deu_Id == id);
        if (ben) {
            if (campo == 'nombres') {
                return `${ben.Deu_Nombre} ${ben.Deu_Paterno} ${ben.Deu_Materno}`;
            }
            if (campo == 'tipo') {
                return (ben.Deu_NIvel == 0 ? 'DEUDOR(A)' : 'CODEUDOR(A)');
            }
        }
    }
    ;
    onImprimirV2(iddeu, iddoc) {
        const param = {
            id_deudor: iddeu
        };
        this.msg.info('Generando los reportes para el Codeudor, favor espere...');
        this.isLoading = true;
        this.reporteService.generarReporteV2(param).subscribe(resp => {
            const response = resp;
            console.log('RESPONSE REPORTE:', resp);
            this.isLoading = false;
            if (resp) {
                // PUBLICAR REPORTE 
                const urlPdf = src_environments_environment__WEBPACK_IMPORTED_MODULE_24__["environment"].URL_PUBLIC_PDF_MASIVOS;
                const rutaPdf = urlPdf + `/${response.data.base}`;
                window.open(rutaPdf, '_blank');
            }
            else {
                this.msg.warning('No se pudo generar el reporte', 'REPORTE');
            }
        });
    }
    onImprimirDoc() {
        const tipo_seguro = this.data.seg_solicitudes.Sol_TipoSeg; // NO LICITADA | LICITADA
        let tipo_cobertura = this.data.seg_solicitudes.Sol_TipoParam; // VG | DH
        if (!tipo_cobertura && tipo_seguro == 'LICITADA') {
            tipo_cobertura = 'DH';
        }
        // obtener plantilla de reportes, this.configParam  
        const pts = this.configParam.impresion_documentos.filter(row => row.tipo_seguro == tipo_seguro && row.tipo_cobertura == tipo_cobertura);
        this.openModalImprimir = 'block';
    }
    loadDocumentosDeudor() {
        (this.showDocs_1 == true ? this.showDocs_1 = false : this.showDocs_1 = true);
    }
    loadDocumentosCoDeudor() {
        (this.showDocs_2 == true ? this.showDocs_2 = false : this.showDocs_2 = true);
    }
    onViewHistorial() {
        this.openModalHistorial = 'block';
    }
    onDatosEco() {
        const estado_evaluado = 'C1';
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
            && row.nro_poliza.split(',').includes(nro_poliza));
        console.log('CORREOS:', correosEstados);
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: ' Desea actualizar la información de la Solicitud desde el sistema Central del Banco ?',
            showCancelButton: true,
            confirmButtonText: `SI`,
            cancelButtonText: `NO`,
            confirmButtonColor: '#009688',
            cancelButtonColor: '#f44336',
        }).then((result) => {
            if (result.isConfirmed) {
                this.getDatosEco();
            }
        });
    }
    onChangeUpload(event) {
        this.file = event.target.files[0];
        this.formData.append('file', event.target.files[0], event.target.files[0].name);
    }
    ;
    onResponderObser() {
        if (this.respuestaObser) {
            const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
            const estado_evaluado = this.data.seg_solicitudes.Sol_EstadoSol;
            // Observaciones
            let model = new _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_21__["SegObservaciones"]();
            model.Obs_IdSol = this.id_solicitud;
            model.Obs_Observacion = this.respuestaObser;
            if (this.usuario_rol == 'of') {
                model.Obs_Eeff = '1';
                model.Obs_Aseg = '';
            }
            else {
                model.Obs_Eeff = '';
                model.Obs_Aseg = nro_poliza;
            }
            ;
            model.Obs_Usuario = `${this.userinfo.usuario_nombre_completo} `;
            // registro en las observaciones
            this.segObservacionesService.add(model).subscribe(resp => {
                const response = resp;
                if (response.status == 'OK') {
                    //this.getSegObservaciones(this.id_solicitud);
                    this.data.seg_observaciones.push(response.data);
                    this.respuestaObser = '';
                    this.msg.info('Se registro y envio con exito', 'Ultima Comunicación');
                }
                else {
                    this.msg.error('No se pudo enviar ni ni registrar', 'Ultima Comunicación');
                }
            });
            // ENVIO DE MENSAJE DE CORREO
            console.log(`usuario rol [${this.usuario_rol}]  `);
            // enviar oficial => compañia
            let messageHtml = '';
            // oficial
            if (this.usuario_rol == 'of') {
                if (this.file) {
                    const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                        && row.nro_poliza.split(',').includes(nro_poliza));
                    if (correosEstados) {
                        let mensajeObj = {
                            subject: `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A.`,
                            message: `${this.data.seg_solicitudes.Sol_Oficial}, Solicita su atención : <br><br> ${this.respuestaObser}`,
                            to: correosEstados.emails,
                            cc: correosEstados.cc,
                            file: this.file
                        };
                        this.fileUploadService.upload(mensajeObj).subscribe(resp => {
                            console.log('UPLOAD FILE - SEND MAIL : ', resp);
                        });
                    }
                    else {
                        this.msg.warning('No estan definidos las notificaciones');
                    }
                }
            }
            else {
                if (this.file) {
                    const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                        && row.nro_poliza.split(',').includes(nro_poliza));
                    if (correosEstados) {
                        let mensajeObj = {
                            subject: `Atención de la Compañia sobre la Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A.`,
                            message: `La Compañia solicita su atención :  <br><br> ${this.respuestaObser}`,
                            to: correosEstados.emails,
                            cc: correosEstados.cc,
                            file: this.file
                        };
                        this.fileUploadService.upload(mensajeObj).subscribe(resp => {
                            console.log('UPLOAD FILE - SEND MAIL : ', resp);
                        });
                    }
                    else {
                        this.msg.warning('No estan definidos las notificaciones');
                    }
                }
            }
        }
        else {
            this.msg.warning('Respuesta requerida', 'Ult.Comunicacion');
        }
    }
    ;
    guardarDeudores() {
        //this.codeudores_def
        let deudores = [];
        this.codeudores_def.forEach(deudor_def => {
            let deudor = {};
            deudor_def.forEach((elem) => {
                //if( elem.editable == true){
                if (elem.campo != " ") {
                    deudor[elem.campo] = elem.valor;
                }
                //}
            });
            deudores.push(deudor);
        });
        this.segDeudoresService.updateDeudores(deudores).subscribe(resp => {
            const response = resp;
            console.log('Respuesta la guardar deudores al validar:', response);
        });
    }
    ;
    getViewData(dataObj) {
        let html1 = '';
        const data1 = Object.keys(dataObj);
        data1.forEach((campo, i) => {
            html1 += `<b class='w3-tiny w3-text-teal'>${campo}</b><br>`;
            const valor = dataObj[campo];
            if (Array.isArray(valor)) {
                html1 += this.getTable(valor);
            }
        });
        return html1;
    }
}
SeguroFormComponent.ɵfac = function SeguroFormComponent_Factory(t) { return new (t || SeguroFormComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_reportes_service__WEBPACK_IMPORTED_MODULE_7__["ReportesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_beneficiarios_service__WEBPACK_IMPORTED_MODULE_12__["SegBeneficiariosService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_ecofuturo_service__WEBPACK_IMPORTED_MODULE_8__["EcofuturoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_aprobaciones_service__WEBPACK_IMPORTED_MODULE_10__["SegAprobacionesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_send_mail_service__WEBPACK_IMPORTED_MODULE_9__["SendMailService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_observaciones_service__WEBPACK_IMPORTED_MODULE_11__["SegObservacionesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_file_upload_service__WEBPACK_IMPORTED_MODULE_16__["FileUploadService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_solicitudes_service__WEBPACK_IMPORTED_MODULE_13__["SegSolicitudesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_deudores_service__WEBPACK_IMPORTED_MODULE_14__["SegDeudoresService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_adicionales_service__WEBPACK_IMPORTED_MODULE_15__["SegAdicionalesService"])); };
SeguroFormComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: SeguroFormComponent, selectors: [["app-seguro-form"]], decls: 526, vars: 180, consts: [["class", "overlay", 4, "ngIf"], [1, "w3-bar", "w3-indigo", "w3-block"], [1, "w3-bar-item"], [1, "w3-container", "w3-border", "w3-monospace"], [1, "w3-row"], [1, "w3-col", "w3-small", "s5", "m5", "l5"], [1, "w3-text-teal"], [4, "ngIf"], [1, "w3-col", "s7", "m7", "l7"], [1, "w3-bar"], ["type", "button", "class", "w3-button w3-margin-left w3-indigo w3-round", 3, "click", 4, "ngIf"], ["type", "button", 1, "w3-button", "w3-margin-left", "w3-indigo", "w3-round", 3, "click"], ["title", "Historial de la Solicitud", 1, "fas", "fa-history", "fa-2x"], [1, "w3-tiny"], ["class", "w3-button w3-margin-left w3-indigo w3-round", 3, "click", 4, "ngIf"], [1, "w3-col", "s12", "m12", "l12"], [1, "w3-bar", "w3-light-grey"], [1, "w3-bar-item", "w3-button", "tablink", "w3-border-top", "w3-border-left", "w3-border-right", "w3-round", 3, "ngClass", "click"], [1, "w3-bar-item", "w3-button", "tablink", "w3-border-top", "w3-border-right", "w3-round", 3, "ngClass", "click"], ["class", "w3-bar-item w3-button tablink w3-border-top w3-border-right w3-round", 3, "ngClass", "click", 4, "ngIf"], ["class", "w3-bar-item w3-button tablink  w3-border-top w3-border-right w3-round", 3, "ngClass", "click", 4, "ngIf"], [1, "w3-border-bottom", "w3-border-left", "w3-border-right", "w3-padding", 3, "ngClass"], [1, "w3-col", "s5", "m5", "l5"], [1, "w3-table", "w3-small", 2, "border-collapse", "collapse"], [1, "w3-text-teal", "w3-border-bottom"], [1, "w3-col", "s2", "m2", "l2"], [1, "w3-row-padding", 3, "innerHTML"], [1, "w3-col", "s6", "m6", "l6"], [1, "w3-small"], ["class", "w3-small w3-margin-left", 4, "ngFor", "ngForOf"], [1, "w3-col", "s10", "m10", "l10"], [1, "w3-table", "w3-bordered", "w3-small"], ["type", "button", "class", "w3-button", 3, "click", 4, "ngIf"], [4, "ngFor", "ngForOf"], [1, "w3-col", "m5"], [1, "w3-col", "s8", "m8", "l8"], [1, "w3-table-all", "w3-container"], [1, "w3-center"], [1, "w3-table", "w3-small"], ["name", "mensaje", "cols", "25", "rows", "2", 1, "w3-input", "w3-pale-blue", 3, "ngModel", "ngModelChange"], ["name", "file", "type", "file", 1, "w3-input", 3, "change"], ["type", "button", 1, "w3-button", "w3-round-large", "w3-indigo", 3, "click"], [1, "fa", "fa-paper-plane"], [1, "w3-col", "s4", "m4", "l4"], ["id", "id10", 1, "w3-modal", 3, "ngStyle"], [1, "w3-modal-content"], [1, "w3-container", "w3-indigo"], [1, "w3-button", "w3-display-topright", 3, "click"], [1, "w3-container", "w3-light-grey"], ["class", "w3-bar-item w3-right", 4, "ngIf"], ["id", "id26", 1, "w3-modal", 3, "ngStyle"], [1, "w3-table-all", "w3-small"], ["id", "id06", 1, "w3-modal", 3, "ngStyle"], [1, "w3-tiny", "w3-text-teal"], [1, "w3-bar-item", "w3-left"], ["type", "button", "class", "w3-button w3-round-xlarge w3-border", 3, "click", 4, "ngIf"], [1, "w3-bar-item", "w3-right"], ["type", "button", "class", "w3-button w3-border w3-round-xlarge", 3, "disabled", "click", 4, "ngIf"], ["id", "id13", 1, "w3-modal", 3, "ngStyle"], [1, "w3-table"], [1, "w3-container", "w3-small", "w3-light-grey", 2, "height", "400px", "overflow-y", "scroll"], [1, "w3-ul", "w3-hoverable"], ["class", "w3-text-red", 4, "ngFor", "ngForOf"], ["class", "w3-text-orange", 4, "ngFor", "ngForOf"], [1, "w3-modal", 3, "ngStyle"], [1, "w3-container", "w3-small", "w3-light-grey"], ["name", "causal", "cols", "25", "rows", "2", 1, "w3-input", "w3-pale-blue", 3, "ngModel", "ngModelChange"], [1, "w3-input", "w3-pale-blue", 3, "ngModel", "ngModelChange"], [3, "ngValue", 4, "ngFor", "ngForOf"], ["type", "button", 1, "w3-button", "w3-block", "w3-round-large", "w3-indigo", 3, "click"], [1, "overlay"], [1, "loading"], ["title", "Actualizar datos WS", 1, "fa", "fa-download", "fa-2x"], [1, "fas", "fa-print", "fa-2x"], [2, "font-size", "10px"], [1, "fa", "fa-trash", "fa-2x"], [1, "w3-button", "w3-margin-left", "w3-indigo", "w3-round", 3, "click"], ["title", "Guardar", 1, "fa", "fa-save", "fa-2x"], ["title", "Validar", 1, "fa", "fa-check", "fa-2x"], [1, "w3-small", "w3-margin-left"], [1, "w3-button", "w3-light-grey", "w3-block", "w3-left-align", "w3-round", 3, "ngClass"], [3, "click"], ["class", "fa fa-plus", 4, "ngIf"], ["class", "fa fa-minus", 4, "ngIf"], ["class", "w3-right", 4, "ngIf"], [1, "w3-border", 3, "ngStyle"], [1, "w3-row", "w3-padding"], ["name", "Deu_CiudadNac", "class", "w3-input w3-pale-blue w3-border-0", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "Deu_Peso", "class", "w3-input w3-pale-blue w3-border-0", "type", "number", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "Deu_Talla", "class", "w3-input w3-pale-blue w3-border-0", "type", "number", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "w3-input w3-pale-blue", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "Deu_DetACtiv", "class", "w3-input w3-pale-blue w3-border-0", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], [1, "w3-text-red"], ["class", "w3-button w3-margin-left w3-margin-top w3-indigo w3-round", 3, "click", 4, "ngIf"], [1, "w3-button", "w3-margin-left", "w3-margin-top", "w3-indigo", "w3-round", 3, "click"], ["title", "Declaracion de salud", 1, "fa", "fa-user-md", "fa-2x"], [1, "fa", "fa-plus"], [1, "fa", "fa-minus"], [1, "fa-solid", "fa-user"], [1, "fa-solid", "fa-user-group"], [1, "w3-right"], ["class", "fa fa-square", 3, "click", 4, "ngIf"], ["class", "fa fa-square-check", 3, "click", 4, "ngIf"], [1, "fa", "fa-square", 3, "click"], [1, "fa", "fa-square-check", 3, "click"], ["name", "Deu_CiudadNac", "type", "text", 1, "w3-input", "w3-pale-blue", "w3-border-0", 3, "ngModel", "ngModelChange"], ["name", "Deu_Peso", "type", "number", 1, "w3-input", "w3-pale-blue", "w3-border-0", 3, "ngModel", "ngModelChange"], ["name", "Deu_Talla", "type", "number", 1, "w3-input", "w3-pale-blue", "w3-border-0", 3, "ngModel", "ngModelChange"], ["value", "DERECHO(A)"], ["value", "ZURDO(A)"], ["name", "Deu_DetACtiv", "type", "text", 1, "w3-input", "w3-pale-blue", "w3-border-0", 3, "ngModel", "ngModelChange"], ["name", "Deu_MontoActAcumVerif", "class", "w3-input w3-pale-blue w3-border-0", "type", "number", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "Deu_MontoActAcumVerif", "type", "number", 1, "w3-input", "w3-pale-blue", "w3-border-0", 3, "ngModel", "ngModelChange"], [1, "fas", "fa-users", "fa-2x"], ["type", "button", 1, "w3-button", 3, "click"], [1, "fa", "fa-retweet"], ["href", "javascript:void(0)", 1, "w3-text-blue", 3, "click"], ["colspan", "4", 1, "w3-center"], ["class", "w3-check", "type", "checkbox", "checked", "checked", "disabled", "", 4, "ngIf"], ["class", "w3-check", "type", "checkbox", "disabled", "", 4, "ngIf"], ["type", "checkbox", "checked", "checked", "disabled", "", 1, "w3-check"], ["type", "checkbox", "disabled", "", 1, "w3-check"], [1, "w3-table-all", "w3-small", "w3-margin-top"], ["style", "height: 70px;", 4, "ngFor", "ngForOf"], ["colspan", "5"], ["type", "button", 1, "w3-button", "w3-deep-orange", "w3-border", "w3-round-xlarge", "w3-right", 3, "click"], [1, "fas", "fa-check"], [2, "height", "70px"], [2, "margin-top", "10px"], ["type", "checkbox", "name", "aprobado", 1, "w3-check", 3, "ngModel", "change", "ngModelChange"], ["class", "fas fa-check-square", 4, "ngIf"], ["type", "checkbox", "name", "rechazado", 1, "w3-check", 3, "ngModel", "change", "ngModelChange"], [2, "width", "200px"], [1, "fas", "fa-check-square"], ["type", "number", "name", "extraprima", "placeholder", "Extra prima %", 1, "w3-input", "w3-border", "w3-pale-blue", 3, "ngModel", "ngModelChange"], ["name", "condiciones", "placeholder", "Condiciones/Exclusiones", 1, "w3-input", "w3-border", "w3-pale-blue", 3, "ngModel", "ngModelChange"], ["name", "causal", "placeholder", "Causal", 1, "w3-input", "w3-border", "w3-pale-blue", 3, "ngModel", "ngModelChange"], ["width", "50%"], ["width", "20%"], [4, "ngIf", "ngIfElse"], ["ver", ""], ["width", "30%"], ["ver1", ""], ["type", "checkbox", "name", "valor_si", 1, "w3-check", 3, "ngModel", "change", "ngModelChange"], ["type", "checkbox", "name", "valor_no", 1, "w3-check", 3, "ngModel", "change", "ngModelChange"], ["name", "respuesta", "placeholder", "Aclaraci\u00F3n", 1, "w3-input", 3, "ngModel", "ngModelChange"], ["type", "button", 1, "w3-button", "w3-border", "w3-round-xlarge", 3, "click"], [1, "w3-small", 3, "click"], ["class", "far fa-trash-alt w3-right", 3, "click", 4, "ngIf"], [1, "w3-border", 3, "ngClass"], [1, "w3-col", "m12"], ["class", "w3-input w3-border", "name", "nombre", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "w3-input w3-border", "name", "paterno", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "w3-input w3-border", "name", "materno", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "w3-input w3-border", "name", "casada", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "sexo", 1, "w3-select", 3, "disabled", "ngModel", "ngModelChange"], ["value", "M"], ["value", "F"], ["class", "w3-input w3-border", "name", "numdoc", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "tipo_doc", "class", "w3-select", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "ext_doc", "class", "w3-select", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "w3-input w3-border", "name", "compdoc", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "relacion", "class", "w3-select", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "w3-input w3-border", "name", "porcentaje", "type", "number", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "w3-input w3-border", "name", "casada", "type", "number", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "tipo", "class", "w3-select", 3, "ngModel", "ngModelChange", 4, "ngIf"], [1, "w3-container", "w3-text-red", "w3-opacity"], [1, "far", "fa-trash-alt", "w3-right", 3, "click"], ["name", "nombre", "type", "text", 1, "w3-input", "w3-border", 3, "ngModel", "ngModelChange"], ["name", "paterno", "type", "text", 1, "w3-input", "w3-border", 3, "ngModel", "ngModelChange"], ["name", "materno", "type", "text", 1, "w3-input", "w3-border", 3, "ngModel", "ngModelChange"], ["name", "casada", "type", "text", 1, "w3-input", "w3-border", 3, "ngModel", "ngModelChange"], ["name", "numdoc", "type", "text", 1, "w3-input", "w3-border", 3, "ngModel", "ngModelChange"], ["name", "tipo_doc", 1, "w3-select", 3, "ngModel", "ngModelChange"], ["value", "CARNET DE IDENTIDAD"], ["value", "CARNET EXTRANJERO"], ["value", "PASAPORTE"], ["name", "ext_doc", 1, "w3-select", 3, "ngModel", "ngModelChange"], ["value", "LA PAZ"], ["value", "COCHABAMBA"], ["value", "SANTA CRUZ"], ["value", "ORURO"], ["value", "POTOSI"], ["value", "CHUQUISACA"], ["value", "TARIJA"], ["value", "BENI"], ["value", "PANDO"], ["value", "S/E"], ["name", "compdoc", "type", "text", 1, "w3-input", "w3-border", 3, "ngModel", "ngModelChange"], ["name", "relacion", 1, "w3-select", 3, "ngModel", "ngModelChange"], ["value", "CONVIVIENTE"], ["value", "CONYUGUE"], ["value", "HIJO(A)"], ["value", "HERMANO(A)"], ["value", "MADRE"], ["value", "PADRE"], ["value", "OTROS"], ["name", "porcentaje", "type", "number", 1, "w3-input", "w3-border", 3, "ngModel", "ngModelChange"], ["name", "casada", "type", "number", 1, "w3-input", "w3-border", 3, "ngModel", "ngModelChange"], ["name", "tipo", 1, "w3-select", 3, "ngModel", "ngModelChange"], ["value", "PRIMARIO"], ["value", "CONTINGENTE"], ["type", "button", 1, "w3-button", "w3-round-xlarge", "w3-border", 3, "click"], [1, "fas", "fa-user-plus"], ["type", "button", 1, "w3-button", "w3-border", "w3-round-xlarge", 3, "disabled", "click"], [1, "w3-margin-bottom"], [1, "w3-button", "w3-indigo", "w3-round-large", 3, "click"], [1, "w3-text-orange"], [3, "ngValue"]], template: function SeguroFormComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, SeguroFormComponent_div_0_Template, 2, 0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "SOLICITUD DE SEGURO DE CREDITOS");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](13, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, " Tipo de Cartera: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "span", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, SeguroFormComponent_span_19_Template, 2, 1, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](21, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](23, SeguroFormComponent_button_23_Template, 5, 0, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_button_click_24_listener() { return ctx.onViewHistorial(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](25, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28, " Historial");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](29, SeguroFormComponent_button_29_Template, 5, 0, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](30, SeguroFormComponent_button_30_Template, 5, 0, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](31, SeguroFormComponent_button_31_Template, 5, 0, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](32, SeguroFormComponent_button_32_Template, 5, 0, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](33, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](37, "button", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_button_click_37_listener() { return ctx.tab_active = 0; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](38, "Solicitud");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_button_click_39_listener() { return ctx.tab_active = 2; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](40, "Deudores");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](41, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_button_click_41_listener() { return ctx.tab_active = 3; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](42, "Beneficiarios");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](43, SeguroFormComponent_button_43_Template, 2, 3, "button", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](44, SeguroFormComponent_button_44_Template, 2, 3, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](45, SeguroFormComponent_button_45_Template, 2, 3, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](46, SeguroFormComponent_button_46_Template, 2, 3, "button", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "table", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](51, "tbody");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](55, "Sucursal:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](56, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](57);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](59, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](60, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](61, "Agencia:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](62, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](63);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](65, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](66, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](67, "Ciudad:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](68, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](69);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](71, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](72, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](73, "Oficial de Cr\u00E9ditos: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](74, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](75);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](77, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](78, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](79, "Usuario Of. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](80, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](81);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](83, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](84, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](85, "Digitador del Seguro: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](86, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](87);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](88, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](89, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](90, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](91, "Tipo de Cr\u00E9dito: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](92, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](93);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](94, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](95, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](96, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](97, "Tipo de Cr\u00E9dito sg/ASFI: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](98, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](99);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](100, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](101, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](102, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](103, "Tipo de Cliente: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](104, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](105);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](106, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](107, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](108, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](109, "Tipo de Operaci\u00F3n Bajo L/C: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](110, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](111);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](112, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](113, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](114, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](115, "Tipo de Seguro: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](116, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](117);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](118, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](119, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](120, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](121, "Tipo Solicitud: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](122, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](123);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](124, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](125, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](126, " \u00A0 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](127, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](128, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](129, "table", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](130, "tbody");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](131, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](132, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](133, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](134, "Nro.Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](135, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](136);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](137, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](138, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](139, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](140, "Fecha Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](141, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](142);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](143, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](144, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](145, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](146, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](147, "Monto solicitado:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](148, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](149);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](150, "number");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](151, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](152, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](153, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](154, "Moneda Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](155, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](156);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](157, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](158, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](159, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](160, "Plazo Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](161, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](162);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](163, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](164, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](165, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](166, "Frec.Pag.Sol.:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](167, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](168);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](169, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](170, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](171, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](172, "N\u00FAmero L/C:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](173, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](174);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](175, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](176, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](177, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](178, "Moneda L/C:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](179, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](180);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](181, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](182, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](183, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](184, "Fecha Aprob. L/C:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](185, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](186);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](187, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](188, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](189, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](190, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](191, "Tasa Mensual x Mil Ref.:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](192, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](193);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](194, "number");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](195, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](196, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](197, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](198, "Prima Mensual Ref.:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](199, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](200);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](201, "number");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](202, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](203, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](204, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](205, "Cant.Obligados:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](206, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](207);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](208, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](209, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](210, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](211, "ID Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](212, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](213);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](214, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](215, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](216, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](217, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](218, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](219, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](220, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](221, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](222, "pre", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](223);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](224, "json");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](225, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](226, SeguroFormComponent_div_226_Template, 218, 65, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](227, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](228, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](229, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](230, "table", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](231, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](232, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](233, "Id");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](234, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](235, "Deudor");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](236, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](237, "Tipo Deudor");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](238, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](239, "Nombre Beneficiario");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](240, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](241, "Documento");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](242, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](243, "Porcentaje");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](244, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](245, "Tipo Beneficiarios");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](246, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](247, SeguroFormComponent_button_247_Template, 2, 0, "button", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](248, SeguroFormComponent_tr_248_Template, 19, 11, "tr", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](249, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](250, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](251, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](252, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](253, "table", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](254, "tbody");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](255, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](256, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](257, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](258, "N\u00FAmero de Operaci\u00F3n: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](259, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](260);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](261, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](262, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](263, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](264, "Monto Aprobado: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](265, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](266);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](267, "number");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](268, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](269, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](270, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](271, "Moneda: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](272, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](273);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](274, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](275, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](276, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](277, "Destino del Cr\u00E9dito: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](278, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](279);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](280, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](281, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](282, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](283, "Fecha Desembolsado: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](284, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](285);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](286, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](287, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](288, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](289, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](290, "Monto Desembolsado: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](291, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](292);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](293, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](294, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](295, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](296, "Fecha Aprobaci\u00F3n: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](297, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](298);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](299, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](300, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](301, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](302, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](303, "Plazo: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](304, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](305);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](306, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](307, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](308, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](309, "Frecuencia de Pago: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](310, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](311);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](312, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](313, "table", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](314, "tbody");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](315, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](316, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](317, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](318, "Fecha Reprogr.:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](319, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](320);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](321, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](322, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](323, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](324, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](325, "Fecha Vencimiento:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](326, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](327);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](328, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](329, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](330, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](331, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](332, "Tipo Garantia:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](333, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](334);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](335, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](336, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](337, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](338, "Tipo Garantia Codigo:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](339, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](340);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](341, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](342, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](343, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](344, "Valor Garantia:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](345, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](346);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](347, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](348, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](349, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](350, "Destino Prestamo:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](351, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](352);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](353, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](354, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](355, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](356, "Destino Codigo:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](357, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](358);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](359, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](360, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](361, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](362, "Actual Acumulado:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](363, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](364);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](365, "number");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](366, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](367, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](368, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](369, "td", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](370, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](371, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](372, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](373, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](374, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](375, "table", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](376, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](377, "td", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](378, "table", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](379, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](380, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](381, "Fecha");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](382, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](383, "Hora");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](384, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](385, "observaci\u00F3n");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](386, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](387, "Usuario");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](388, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](389, "EIF");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](390, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](391, "Aseguradora");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](392, SeguroFormComponent_tr_392_Template, 14, 10, "tr", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](393, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](394, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](395, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](396, "td", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](397, "Respuesta");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](398, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](399, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](400, "textarea", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_Template_textarea_ngModelChange_400_listener($event) { return ctx.respuestaObser = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](401, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](402, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](403, " Adjuntar archivo: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](404, "input", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SeguroFormComponent_Template_input_change_404_listener($event) { return ctx.onChangeUpload($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](405, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](406, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](407, "button", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_button_click_407_listener() { return ctx.onResponderObser(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](408, "i", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](409, " \u00A0 Responder y Enviar");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](410, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](411, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](412, SeguroFormComponent_div_412_Template, 7, 1, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](413, SeguroFormComponent_div_413_Template, 8, 1, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](414, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](415, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](416, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](417, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](418, "header", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](419, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_span_click_419_listener() { return ctx.openModalCuestion = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](420, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](421, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](422, "Cuestionario de Salud");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](423, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](424, "table", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](425, SeguroFormComponent_tr_425_Template, 11, 6, "tr", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](426, "footer", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](427, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](428, SeguroFormComponent_div_428_Template, 4, 0, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](429, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](430, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](431, "header", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](432, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](433, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_span_click_433_listener() { return ctx.openModalHistorial = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](434, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](435, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](436, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](437, "table", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](438, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](439, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](440, "Fecha");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](441, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](442, "Hora");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](443, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](444, "observaci\u00F3n");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](445, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](446, "Usuario");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](447, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](448, "EIF");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](449, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](450, "Aseguradora");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](451, SeguroFormComponent_tr_451_Template, 14, 10, "tr", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](452, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](453, "footer", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](454, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](455, "div", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](456, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](457, "header", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](458, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](459, "span", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](460, "BENEFICIARIOS DE : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](461, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](462);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](463, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_span_click_463_listener() { return ctx.openModalBene = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](464, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](465, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](466, SeguroFormComponent_div_466_Template, 96, 41, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](467, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](468, "footer", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](469, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](470, "div", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](471, SeguroFormComponent_button_471_Template, 3, 0, "button", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](472, "div", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](473, SeguroFormComponent_button_473_Template, 3, 1, "button", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](474, "div", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](475, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](476, "header", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](477, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_span_click_477_listener() { return ctx.openModalImprimir = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](478, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](479, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](480, "Imprimir documentos");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](481, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](482, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](483, "table", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](484, SeguroFormComponent_tr_484_Template, 3, 2, "tr", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](485, "footer", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](486, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](487, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](488, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](489, "header", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](490, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_span_click_490_listener() { return ctx.openModalValida = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](491, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](492, "h4", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](493, "Observaciones y Advertencias");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](494, "div", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](495, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](496, "ul", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](497, SeguroFormComponent_li_497_Template, 2, 1, "li", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](498, "ul", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](499, SeguroFormComponent_li_499_Template, 2, 1, "li", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](500, "footer", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](501, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](502, "div", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](503, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](504, "header", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](505, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_span_click_505_listener() { return ctx.openModalDesistir = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](506, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](507, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](508, "Desestimiento de Solicitudes");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](509, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](510, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](511, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](512, "Causa:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](513, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](514, "textarea", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_Template_textarea_ngModelChange_514_listener($event) { return ctx.desistimiento.causal = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](515, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](516, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](517, "Autorizado por:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](518, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](519, "select", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SeguroFormComponent_Template_select_ngModelChange_519_listener($event) { return ctx.desistimiento.autorizado_por = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](520, SeguroFormComponent_option_520_Template, 2, 2, "option", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](521, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](522, "button", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SeguroFormComponent_Template_button_click_522_listener() { return ctx.onDesistir(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](523, " Efectivizar Desestimiento ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](524, "footer", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](525, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" Nro.Solicitud: ", ctx.data.seg_solicitudes.Sol_NumSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" Estado Actual del Seguro: ", ctx.estado_codigo, " - ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.estado_general, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_solicitudes.Sol_TipoSeg);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_general == "APROBADO");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_codigo == "P");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](136, _c3).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_codigo == "P");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_codigo == "P");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_codigo == "P");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](137, _c0, ctx.tab_active == 0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](139, _c0, ctx.tab_active == 2));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](141, _c0, ctx.tab_active == 3));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.usuario_rol == "of");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](143, _c5).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](144, _c6).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](145, _c4, ctx.tab_active != 0, ctx.tab_active == 0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_solicitudes.Sol_Sucursal);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_solicitudes.Sol_Agencia);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.data.seg_solicitudes.Sol_Ciudad, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.data.seg_solicitudes.Sol_Oficial, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_CodOficial, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_Digitador, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.data.seg_solicitudes.Sol_TipoCred, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_TipoAsfi, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_solicitudes.Sol_TipoCli);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.data.seg_solicitudes.Sol_TipoOpeLC, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_TipoSeg, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_SolTipoSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_NumSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](143, 95, ctx.data.seg_solicitudes.Sol_FechaSol, 0, 10), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](150, 99, ctx.data.seg_solicitudes.Sol_MontoSol, ".2"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_MonedaSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_PlazoSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_FrecPagoSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_NumLC, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_MonedaLC, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](187, 102, ctx.data.seg_solicitudes.Sol_FechaLC, 0, 10), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](194, 106, ctx.datosCalculados.tasa_mensual_ref, ".4"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](201, 109, ctx.datosCalculados.prima_mensual_ref, ".2"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_CantObl, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_IdSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](148, _c4, ctx.tab_active != 1, ctx.tab_active == 1));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx.viewTable, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind1"](224, 112, ctx.data), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](151, _c4, ctx.tab_active != 2, ctx.tab_active == 2));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.data.seg_deudores);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](154, _c4, ctx.tab_active != 3, ctx.tab_active == 3));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.data.seg_beneficiarios);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](157, _c4, ctx.tab_active != 4, ctx.tab_active == 4));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Numero_Aprobado);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](267, 114, ctx.data.seg_operaciones.Operacion_Monto_Aprobado, ".2"), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Moneda_Aprobada);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Destino_Prestamo);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](286, 117, ctx.data.seg_operaciones.Operacion_Fecha_Desembolso, 0, 10), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Monto_Desembolsado);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](299, 121, ctx.data.seg_operaciones.Operacion_Fecha_Aprobacion, 0, 10), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Plazo_Aprobado);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Frecuencia_Aprobada);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](321, 125, ctx.data.seg_operaciones.Operacion_Fecha_Reprogramacion, 0, 10), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](328, 129, ctx.data.seg_operaciones.Operacion_Fecha_Vencimiento, 0, 10), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Tipo_Garantia);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Tipo_Grantia_Codigo);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Valor_Garantia);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Destino_Prestamo);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Destino_Codigo);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](365, 133, ctx.data.seg_operaciones.Operacion_Actual_Acumulado, ".2"), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](160, _c4, ctx.tab_active != 5, ctx.tab_active == 5));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.segObservacionesUlt);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.respuestaObser);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](163, _c4, ctx.tab_active != 6, ctx.tab_active == 6));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](166, _c7).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.usuario_rol != "of" && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](167, _c5).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](168, _c2, ctx.openModalCuestion));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.declaraciones);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_codigo == "P");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](170, _c2, ctx.openModalHistorial));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.data.seg_observaciones);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](172, _c2, ctx.openModalBene));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.deudor_nombre_sel);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.segBeneficiariosIdDeu);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_codigo == "P");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_codigo == "P");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](174, _c2, ctx.openModalImprimir));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.data.seg_deudores);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](176, _c2, ctx.openModalValida));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.observaciones);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.advertencias);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](178, _c2, ctx.openModalDesistir));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.desistimiento.causal);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.desistimiento.autorizado_por);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.autorizacionesSolicitud);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_26__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_26__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_26__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_27__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_27__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_27__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_26__["NgStyle"], _angular_forms__WEBPACK_IMPORTED_MODULE_27__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_27__["NumberValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_27__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_27__["ɵangular_packages_forms_forms_x"], _angular_forms__WEBPACK_IMPORTED_MODULE_27__["CheckboxControlValueAccessor"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_26__["SlicePipe"], _angular_common__WEBPACK_IMPORTED_MODULE_26__["DecimalPipe"], _angular_common__WEBPACK_IMPORTED_MODULE_26__["JsonPipe"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlZ3Vyb3Mvc2VndXJvLWZvcm0vc2VndXJvLWZvcm0uY29tcG9uZW50LmNzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](SeguroFormComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
                selector: 'app-seguro-form',
                templateUrl: './seguro-form.component.html',
                styleUrls: ['./seguro-form.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }, { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] }, { type: _service_reportes_service__WEBPACK_IMPORTED_MODULE_7__["ReportesService"] }, { type: _service_seg_beneficiarios_service__WEBPACK_IMPORTED_MODULE_12__["SegBeneficiariosService"] }, { type: _service_ecofuturo_service__WEBPACK_IMPORTED_MODULE_8__["EcofuturoService"] }, { type: _service_seg_aprobaciones_service__WEBPACK_IMPORTED_MODULE_10__["SegAprobacionesService"] }, { type: _service_send_mail_service__WEBPACK_IMPORTED_MODULE_9__["SendMailService"] }, { type: _service_seg_observaciones_service__WEBPACK_IMPORTED_MODULE_11__["SegObservacionesService"] }, { type: _service_file_upload_service__WEBPACK_IMPORTED_MODULE_16__["FileUploadService"] }, { type: _service_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] }, { type: _service_seg_solicitudes_service__WEBPACK_IMPORTED_MODULE_13__["SegSolicitudesService"] }, { type: _service_seg_deudores_service__WEBPACK_IMPORTED_MODULE_14__["SegDeudoresService"] }, { type: _service_seg_adicionales_service__WEBPACK_IMPORTED_MODULE_15__["SegAdicionalesService"] }]; }, null); })();
;
;
;
;
;
;
;
const getDateYYYYmmDD = () => {
    var d = new Date(), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    return [year, month, day].join('-');
};


/***/ }),

/***/ "../../src/app/seguros/seguro-list/seguro-list.component.ts":
/*!**************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/seguro-list/seguro-list.component.ts ***!
  \**************************************************************************************************************************************/
/*! exports provided: SeguroListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeguroListComponent", function() { return SeguroListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class SeguroListComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
SeguroListComponent.ɵfac = function SeguroListComponent_Factory(t) { return new (t || SeguroListComponent)(); };
SeguroListComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SeguroListComponent, selectors: [["app-seguro-list"]], decls: 0, vars: 0, template: function SeguroListComponent_Template(rf, ctx) { }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlZ3Vyb3Mvc2VndXJvLWxpc3Qvc2VndXJvLWxpc3QuY29tcG9uZW50LmNzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SeguroListComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-seguro-list',
                templateUrl: './seguro-list.component.html',
                styleUrls: ['./seguro-list.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/seguros-routing.module.ts":
/*!***************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/seguros-routing.module.ts ***!
  \***************************************************************************************************************************/
/*! exports provided: SegurosRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegurosRoutingModule", function() { return SegurosRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _seguro_form_seguro_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./seguro-form/seguro-form.component */ "../../src/app/seguros/seguro-form/seguro-form.component.ts");
/* harmony import */ var _seguro_list_seguro_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./seguro-list/seguro-list.component */ "../../src/app/seguros/seguro-list/seguro-list.component.ts");
/* harmony import */ var _busca_solicitud_busca_solicitud_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./busca-solicitud/busca-solicitud.component */ "../../src/app/seguros/busca-solicitud/busca-solicitud.component.ts");
/* harmony import */ var _soporte_solicitud_soporte_solicitud_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./soporte-solicitud/soporte-solicitud.component */ "../../src/app/seguros/soporte-solicitud/soporte-solicitud.component.ts");








const routes = [
    {
        path: 'seguro-list',
        component: _seguro_list_seguro_list_component__WEBPACK_IMPORTED_MODULE_3__["SeguroListComponent"]
    },
    {
        path: 'seguro-form',
        component: _seguro_form_seguro_form_component__WEBPACK_IMPORTED_MODULE_2__["SeguroFormComponent"]
    },
    {
        path: 'seguro-add',
        component: _busca_solicitud_busca_solicitud_component__WEBPACK_IMPORTED_MODULE_4__["BuscaSolicitudComponent"]
    },
    {
        path: 'soporte-solicitud',
        component: _soporte_solicitud_soporte_solicitud_component__WEBPACK_IMPORTED_MODULE_5__["SoporteSolicitudComponent"]
    }
];
class SegurosRoutingModule {
}
SegurosRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: SegurosRoutingModule });
SegurosRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function SegurosRoutingModule_Factory(t) { return new (t || SegurosRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SegurosRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SegurosRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "../../src/app/seguros/seguros.module.ts":
/*!*******************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/seguros.module.ts ***!
  \*******************************************************************************************************************/
/*! exports provided: SegurosModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegurosModule", function() { return SegurosModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _seguros_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./seguros-routing.module */ "../../src/app/seguros/seguros-routing.module.ts");
/* harmony import */ var _seguro_list_seguro_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./seguro-list/seguro-list.component */ "../../src/app/seguros/seguro-list/seguro-list.component.ts");
/* harmony import */ var _seguro_form_seguro_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./seguro-form/seguro-form.component */ "../../src/app/seguros/seguro-form/seguro-form.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/shared.module */ "../../src/app/shared/shared.module.ts");
/* harmony import */ var _busca_solicitud_busca_solicitud_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./busca-solicitud/busca-solicitud.component */ "../../src/app/seguros/busca-solicitud/busca-solicitud.component.ts");
/* harmony import */ var _soporte_solicitud_soporte_solicitud_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./soporte-solicitud/soporte-solicitud.component */ "../../src/app/seguros/soporte-solicitud/soporte-solicitud.component.ts");










class SegurosModule {
}
SegurosModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: SegurosModule });
SegurosModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function SegurosModule_Factory(t) { return new (t || SegurosModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _seguros_routing_module__WEBPACK_IMPORTED_MODULE_3__["SegurosRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SegurosModule, { declarations: [_seguro_list_seguro_list_component__WEBPACK_IMPORTED_MODULE_4__["SeguroListComponent"],
        _seguro_form_seguro_form_component__WEBPACK_IMPORTED_MODULE_5__["SeguroFormComponent"],
        _busca_solicitud_busca_solicitud_component__WEBPACK_IMPORTED_MODULE_7__["BuscaSolicitudComponent"],
        _soporte_solicitud_soporte_solicitud_component__WEBPACK_IMPORTED_MODULE_8__["SoporteSolicitudComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _seguros_routing_module__WEBPACK_IMPORTED_MODULE_3__["SegurosRoutingModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
        _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SegurosModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [
                    _seguro_list_seguro_list_component__WEBPACK_IMPORTED_MODULE_4__["SeguroListComponent"],
                    _seguro_form_seguro_form_component__WEBPACK_IMPORTED_MODULE_5__["SeguroFormComponent"],
                    _busca_solicitud_busca_solicitud_component__WEBPACK_IMPORTED_MODULE_7__["BuscaSolicitudComponent"],
                    _soporte_solicitud_soporte_solicitud_component__WEBPACK_IMPORTED_MODULE_8__["SoporteSolicitudComponent"]
                ],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _seguros_routing_module__WEBPACK_IMPORTED_MODULE_3__["SegurosRoutingModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                    _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"]
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/config-param.service.ts":
/*!*********************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/config-param.service.ts ***!
  \*********************************************************************************************************************************/
/*! exports provided: ConfigParamService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigParamService", function() { return ConfigParamService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class ConfigParamService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/config-param`;
    }
    getConfigParam() {
        return this.http.get(this.URL_API);
    }
    ;
    getConfigParamKey(key) {
        return this.http.get(`${this.URL_API}/${key}`);
    }
    ;
    getConfigParamKeys(keys) {
        return this.http.post(`${this.URL_API}/keys`, { keys });
    }
    ;
    update(model) {
        return this.http.post(`${this.URL_API}`, model);
    }
    ;
}
ConfigParamService.ɵfac = function ConfigParamService_Factory(t) { return new (t || ConfigParamService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
ConfigParamService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ConfigParamService, factory: ConfigParamService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ConfigParamService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/ecofuturo.service.ts":
/*!******************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/ecofuturo.service.ts ***!
  \******************************************************************************************************************************/
/*! exports provided: EcofuturoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EcofuturoService", function() { return EcofuturoService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class EcofuturoService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/ecofuturo`;
    }
    // http://localhost:4005/api-seguros/ecofuturo/getsolicitud/172839
    getSolicitud(nro_solicitud) {
        return this.http.get(`${this.URL_API}/getsolicitud/${nro_solicitud}`);
    }
    // http://localhost:4005/api-seguros/ecofuturo/getsolicitudprimera/172839
    getSolicitudPrimeraEtapa(nro_solicitud) {
        return this.http.get(`${this.URL_API}/getsolicitudprimera/${nro_solicitud}`);
    }
    // http://localhost:4005/api-seguros/ecofuturo/getsolicitudsegunda/172839
    getSolicitudSegundaEtapa(nro_solicitud) {
        return this.http.get(`${this.URL_API}/getsolicitudsegunda/${nro_solicitud}`);
    }
    // http://localhost:4005/api-seguros/ecofuturo/getsolicitudtercera/172839
    getSolicitudTerceraEtapa(nro_solicitud) {
        return this.http.get(`${this.URL_API}/getsolicitudtercera/${nro_solicitud}`);
    }
    // http://localhost:4005/api-seguros/ecofuturo/getsolicitudall/172839
    // http://localhost:4005/api-seguros/ecofuturo/getsolicitudall/159322
    getSolicitudAll(nro_solicitud) {
        return this.http.get(`${this.URL_API}/getsolicitudall/${nro_solicitud}`);
    }
}
EcofuturoService.ɵfac = function EcofuturoService_Factory(t) { return new (t || EcofuturoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
EcofuturoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: EcofuturoService, factory: EcofuturoService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EcofuturoService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/file-upload.service.ts":
/*!********************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/file-upload.service.ts ***!
  \********************************************************************************************************************************/
/*! exports provided: FileUploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploadService", function() { return FileUploadService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class FileUploadService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/upload-file`;
    }
    upload(mensajeObj) {
        console.log('file-upload.service:', mensajeObj);
        const formData = new FormData();
        formData.append('file', mensajeObj.file);
        formData.append('subject', mensajeObj.subject);
        formData.append('message', mensajeObj.message);
        formData.append('to', mensajeObj.to);
        formData.append('cc', mensajeObj.cc);
        return this.http.post(`${this.URL_API}/upload-send-email/123`, formData);
    }
    ;
    subirArchivoPdf(formData, param) {
        return this.http.post(`${this.URL_API}/upload-send-email/${param}`, formData);
    }
}
FileUploadService.ɵfac = function FileUploadService_Factory(t) { return new (t || FileUploadService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
FileUploadService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: FileUploadService, factory: FileUploadService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FileUploadService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/parametros-campo.service.ts":
/*!*************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/parametros-campo.service.ts ***!
  \*************************************************************************************************************************************/
/*! exports provided: ParametrosCampoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParametrosCampoService", function() { return ParametrosCampoService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");


//import * as app from '../../app.settings';



class ParametrosCampoService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/parametros-campo`;
    }
    getAll() {
        return this.http.get(this.URL_API);
    }
    ;
    getOne(id) {
        return this.http.get(`${this.URL_API}/${id}`);
    }
    ;
    getCamposConfigParam() {
        return this.http.get(`${this.URL_API}/campos-config-param`);
    }
    ;
    add(model) {
        return this.http.post(this.URL_API, model);
    }
    ;
    update(model) {
        return this.http.put(`${this.URL_API}/${model.id}`, model);
    }
    ;
    delete(id) {
        return this.http.delete(`${this.URL_API}/${id}`);
    }
    ;
    fechaActual(formato) {
        const today = new Date();
        const dd = today.getDate();
        const mm = today.getMonth() + 1; //January is 0!
        const yyyy = today.getFullYear();
        let fechaActual = `${yyyy}-${mm}-${dd}`;
        if (formato) {
            if (formato === 'dd-mm-yyyy') {
                fechaActual = `${dd}-${mm}-${yyyy}`;
            }
        }
        return fechaActual;
    }
    ;
}
ParametrosCampoService.ɵfac = function ParametrosCampoService_Factory(t) { return new (t || ParametrosCampoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
ParametrosCampoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ParametrosCampoService, factory: ParametrosCampoService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ParametrosCampoService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/reportes.service.ts":
/*!*****************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/reportes.service.ts ***!
  \*****************************************************************************************************************************/
/*! exports provided: ReportesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportesService", function() { return ReportesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");


//import * as app from '../../app.settings';



class ReportesService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/reportes`;
        this.URL_API_MASIVOS = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API_MASIVOS}/reporte_query`;
    }
    generarNroSolicitud(param) {
        return this.http.post(this.URL_API, param);
    }
    //generarReporteV2(param:{id_deudor:number}){
    //  return this.http.post(`${environment.URL_PUBLIC_PDF_MASIVOS}/reporte_query/executeQueryCrediticios/`,param);
    //}
    generarReporteV2(param) {
        console.log(`REPORTE RUTA : ${this.URL_API_MASIVOS}/executeQueryCrediticios/`);
        return this.http.post(`${this.URL_API_MASIVOS}/executeQueryCrediticios/`, param, { withCredentials: true });
    }
}
ReportesService.ɵfac = function ReportesService_Factory(t) { return new (t || ReportesService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
ReportesService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ReportesService, factory: ReportesService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ReportesService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/seg-adicionales.service.ts":
/*!************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/seg-adicionales.service.ts ***!
  \************************************************************************************************************************************/
/*! exports provided: SegAdicionalesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegAdicionalesService", function() { return SegAdicionalesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class SegAdicionalesService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/seg-adicionales`;
    }
    getAll() {
        return this.http.get(this.URL_API);
    }
    updateAll(model) {
        return this.http.put(`${this.URL_API}`, model);
    }
    updateAdiconales(model) {
        return this.http.put(`${this.URL_API}/seg-adicionales/1`, { seg_adicionales: model });
    }
}
SegAdicionalesService.ɵfac = function SegAdicionalesService_Factory(t) { return new (t || SegAdicionalesService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
SegAdicionalesService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SegAdicionalesService, factory: SegAdicionalesService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SegAdicionalesService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/seg-aprobaciones.service.ts":
/*!*************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/seg-aprobaciones.service.ts ***!
  \*************************************************************************************************************************************/
/*! exports provided: SegAprobacionesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegAprobacionesService", function() { return SegAprobacionesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class SegAprobacionesService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/seg-aprobaciones`;
    }
    getAll() {
        return this.http.get(this.URL_API);
    }
    getIdSol(idsol) {
        return this.http.get(`${this.URL_API}/${idsol}`);
    }
    add(model) {
        return this.http.post(this.URL_API, model);
    }
    update(model) {
        return this.http.put(`${this.URL_API}/${model.Apr_IdSol}/${model.Apr_IdDeu}`, model);
    }
}
SegAprobacionesService.ɵfac = function SegAprobacionesService_Factory(t) { return new (t || SegAprobacionesService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
SegAprobacionesService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SegAprobacionesService, factory: SegAprobacionesService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SegAprobacionesService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/seg-beneficiarios.service.ts":
/*!**************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/seg-beneficiarios.service.ts ***!
  \**************************************************************************************************************************************/
/*! exports provided: SegBeneficiariosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegBeneficiariosService", function() { return SegBeneficiariosService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class SegBeneficiariosService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/seg-beneficiarios`;
    }
    getAll() {
        return this.http.get(this.URL_API);
    }
    getOne(id) {
        return this.http.get(`${this.URL_API}/${id}`);
    }
    add(model) {
        return this.http.post(this.URL_API, model);
    }
    update(model) {
        return this.http.put(`${this.URL_API}/${model.Ben_Id}`, model);
    }
    delete(id) {
        return this.http.delete(`${this.URL_API}/${id}`);
    }
    getIdSol(Ben_IdSol) {
        return this.http.get(`${this.URL_API}/IdSol/${Ben_IdSol}`);
    }
    updateAll(models, idSol) {
        return this.http.put(`${this.URL_API}/all/${idSol}`, models);
    }
}
SegBeneficiariosService.ɵfac = function SegBeneficiariosService_Factory(t) { return new (t || SegBeneficiariosService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
SegBeneficiariosService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SegBeneficiariosService, factory: SegBeneficiariosService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SegBeneficiariosService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/seg-deudores.service.ts":
/*!*********************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/seg-deudores.service.ts ***!
  \*********************************************************************************************************************************/
/*! exports provided: SegDeudoresService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegDeudoresService", function() { return SegDeudoresService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class SegDeudoresService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/seg-deudores`;
    }
    getAll() {
        return this.http.get(this.URL_API);
    }
    getIdSol(idSol) {
        return this.http.get(`${this.URL_API}/idsol/${idSol}`);
    }
    updateDeudores(models) {
        return this.http.put(`${this.URL_API}/update-deudores/1`, models);
    }
    updateDeudoresSolicitud(model) {
        return this.http.put(`${this.URL_API}/update-deudores-solicitud/1`, model);
    }
}
SegDeudoresService.ɵfac = function SegDeudoresService_Factory(t) { return new (t || SegDeudoresService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
SegDeudoresService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SegDeudoresService, factory: SegDeudoresService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SegDeudoresService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/seg-observaciones.service.ts":
/*!**************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/seg-observaciones.service.ts ***!
  \**************************************************************************************************************************************/
/*! exports provided: SegObservacionesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegObservacionesService", function() { return SegObservacionesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class SegObservacionesService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/seg-observaciones`;
    }
    getAll() {
        return this.http.get(this.URL_API);
    }
    getId(id) {
        return this.http.get(`${this.URL_API}/${id}`);
    }
    getIdSol(id) {
        return this.http.get(`${this.URL_API}/IdSol/${id}`);
    }
    add(model) {
        return this.http.post(this.URL_API, model);
    }
    update(model) {
        return this.http.put(`${this.URL_API}/${model.Obs_Id}`, model);
    }
    delete(id) {
        return this.http.delete(`${this.URL_API}/${id}`);
    }
}
SegObservacionesService.ɵfac = function SegObservacionesService_Factory(t) { return new (t || SegObservacionesService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
SegObservacionesService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SegObservacionesService, factory: SegObservacionesService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SegObservacionesService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/seg-solicitudes.service.ts":
/*!************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/seg-solicitudes.service.ts ***!
  \************************************************************************************************************************************/
/*! exports provided: SegSolicitudesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegSolicitudesService", function() { return SegSolicitudesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class SegSolicitudesService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/seg-solicitudes`;
    }
    getAll() {
        return this.http.get(this.URL_API);
    }
    getAllUsr(nombre_usuario) {
        return this.http.get(`${this.URL_API}/usuario/${nombre_usuario}`);
    }
    getOne(id) {
        return this.http.get(`${this.URL_API}/${id}`);
    }
    getNroSolicitud(nro) {
        return this.http.get(`${this.URL_API}/solicitud/${nro}`);
    }
    getBuscar(param) {
        return this.http.post(`${this.URL_API}/buscar`, param);
    }
    getSolicitud() {
        return this.http.get(`${this.URL_API}/solicitud/1`);
    }
    getUpdatePoliza(idsol) {
        return this.http.get(`${this.URL_API}/update-poliza/${idsol}`);
    }
    getSolicitudesPag(model) {
        return this.http.post(`${this.URL_API}/pag`, model);
    }
    add(model) {
        return this.http.post(this.URL_API, model);
    }
    update(model) {
        return this.http.put(`${this.URL_API}/${model.Sol_IdSol}`, model);
    }
    delete(id) {
        return this.http.delete(`${this.URL_API}/${id}`);
    }
    addDesgravamen(nro_solicitud, nombre_usuario) {
        return this.http.post(`${this.URL_API}/add-desgravamen`, { nro_solicitud, nombre_usuario });
    }
    desistimientoSolicitud(model) {
        return this.http.post(`${this.URL_API}/desistimiento-solicitud`, model);
    }
    getSolicitudIdSolAll(idsol) {
        return this.http.get(`${this.URL_API}/idsol-all/${idsol}`);
    }
    updateServiciosData(model) {
        return this.http.put(`${this.URL_API}/update-servicios-data/${model.idsol}`, model);
    }
    //soporte-solicitud
    updateSolicitudDeudores(request) {
        return this.http.put(`${this.URL_API}/soporte-solicitud/${request.data.seg_solicitudes.Sol_IdSol}`, request);
    }
    sendEmailSoporte(sendObj) {
        return this.http.post(`${this.URL_API}/send-email-soporte`, sendObj);
    }
}
SegSolicitudesService.ɵfac = function SegSolicitudesService_Factory(t) { return new (t || SegSolicitudesService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
SegSolicitudesService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SegSolicitudesService, factory: SegSolicitudesService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SegSolicitudesService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/service/send-mail.service.ts":
/*!******************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/service/send-mail.service.ts ***!
  \******************************************************************************************************************************/
/*! exports provided: SendMailService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendMailService", function() { return SendMailService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class SendMailService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/send-mail`;
    }
    sendMessage(model) {
        return this.http.post(this.URL_API, model);
    }
}
SendMailService.ɵfac = function SendMailService_Factory(t) { return new (t || SendMailService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
SendMailService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SendMailService, factory: SendMailService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SendMailService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/seguros/soporte-solicitud/soporte-solicitud.component.ts":
/*!**************************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/seguros/soporte-solicitud/soporte-solicitud.component.ts ***!
  \**************************************************************************************************************************************************/
/*! exports provided: SoporteSolicitudComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SoporteSolicitudComponent", function() { return SoporteSolicitudComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "../../node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "../../node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "../../node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/auth.service */ "../../src/app/service/auth.service.ts");
/* harmony import */ var _service_reportes_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/reportes.service */ "../../src/app/seguros/service/reportes.service.ts");
/* harmony import */ var _service_ecofuturo_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../service/ecofuturo.service */ "../../src/app/seguros/service/ecofuturo.service.ts");
/* harmony import */ var _service_send_mail_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../service/send-mail.service */ "../../src/app/seguros/service/send-mail.service.ts");
/* harmony import */ var _service_seg_aprobaciones_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../service/seg-aprobaciones.service */ "../../src/app/seguros/service/seg-aprobaciones.service.ts");
/* harmony import */ var _service_seg_observaciones_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../service/seg-observaciones.service */ "../../src/app/seguros/service/seg-observaciones.service.ts");
/* harmony import */ var _service_seg_beneficiarios_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../service/seg-beneficiarios.service */ "../../src/app/seguros/service/seg-beneficiarios.service.ts");
/* harmony import */ var _service_seg_solicitudes_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../service/seg-solicitudes.service */ "../../src/app/seguros/service/seg-solicitudes.service.ts");
/* harmony import */ var _service_seg_deudores_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../service/seg-deudores.service */ "../../src/app/seguros/service/seg-deudores.service.ts");
/* harmony import */ var _service_seg_adicionales_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../service/seg-adicionales.service */ "../../src/app/seguros/service/seg-adicionales.service.ts");
/* harmony import */ var _service_parametros_campo_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../service/parametros-campo.service */ "../../src/app/seguros/service/parametros-campo.service.ts");
/* harmony import */ var _service_file_upload_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../service/file-upload.service */ "../../src/app/seguros/service/file-upload.service.ts");
/* harmony import */ var _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../models/seg-solicitudes */ "../../src/app/seguros/models/seg-solicitudes.ts");
/* harmony import */ var _models_seg_beneficiarios__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../models/seg-beneficiarios */ "../../src/app/seguros/models/seg-beneficiarios.ts");
/* harmony import */ var _models_seg_adicionales__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../models/seg-adicionales */ "../../src/app/seguros/models/seg-adicionales.ts");
/* harmony import */ var _models_seg_aprobaciones__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../models/seg-aprobaciones */ "../../src/app/seguros/models/seg-aprobaciones.ts");
/* harmony import */ var _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../models/seg-observaciones */ "../../src/app/seguros/models/seg-observaciones.ts");
/* harmony import */ var _models_seg_deudores__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../models/seg-deudores */ "../../src/app/seguros/models/seg-deudores.ts");
/* harmony import */ var _models_seg_operaciones__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../models/seg-operaciones */ "../../src/app/seguros/models/seg-operaciones.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");
/* harmony import */ var _models_message_email__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../models/message-email */ "../../src/app/seguros/models/message-email.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/forms */ "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");













































function SoporteSolicitudComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "div", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_span_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("N\u00FAmero de P\u00F3liza : ", ctx_r1.data.seg_solicitudes.Sol_Poliza, " ");
} }
function SoporteSolicitudComponent_button_24_Template(rf, ctx) { if (rf & 1) {
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_button_24_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r24); const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r23.onImprimirDoc(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 84);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Imprimir");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_button_25_Template(rf, ctx) { if (rf & 1) {
    const _r26 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_button_25_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r26); const ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r25.onEliminarSolicitud(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 85);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Eliminar");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_button_31_Template(rf, ctx) { if (rf & 1) {
    const _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_button_31_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r28); const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r27.onValidar(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 86);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Validar");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function (a0) { return { "w3-deep-orange": a0 }; };
function SoporteSolicitudComponent_button_42_Template(rf, ctx) { if (rf & 1) {
    const _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_button_42_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r30); const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r29.tab_active = 4; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Operaci\u00F3n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](1, _c0, ctx_r5.tab_active == 4));
} }
function SoporteSolicitudComponent_button_43_Template(rf, ctx) { if (rf & 1) {
    const _r32 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_button_43_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r32); const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r31.tab_active = 5; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Ult.comunicaci\u00F3n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](1, _c0, ctx_r6.tab_active == 5));
} }
function SoporteSolicitudComponent_button_44_Template(rf, ctx) { if (rf & 1) {
    const _r34 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_button_44_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r34); const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r33.tab_active = 6; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Resoluci\u00F3n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](1, _c0, ctx_r7.tab_active == 6));
} }
function SoporteSolicitudComponent_button_45_Template(rf, ctx) { if (rf & 1) {
    const _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_button_45_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r36); const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r35.tab_active = 1; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "\u00A0");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](1, _c0, ctx_r8.tab_active == 1));
} }
function SoporteSolicitudComponent_option_203_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const itm_r37 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngValue", itm_r37.codigo);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", itm_r37.estado, " ");
} }
function SoporteSolicitudComponent_div_216_i_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 110);
} }
function SoporteSolicitudComponent_div_216_i_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 111);
} }
function SoporteSolicitudComponent_div_216_span_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "( ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "i", 112);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, " Titular)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_div_216_span_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "( ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "i", 113);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, " Codeudor)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_div_216_span_8_i_2_Template(rf, ctx) { if (rf & 1) {
    const _r63 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "i", 117);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_div_216_span_8_i_2_Template_i_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r63); const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; return deuObj_r38.Deu_Incluido = "S"; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_div_216_span_8_i_3_Template(rf, ctx) { if (rf & 1) {
    const _r66 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "i", 118);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_div_216_span_8_i_3_Template_i_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r66); const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; return deuObj_r38.Deu_Incluido = "N"; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_div_216_span_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 114);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Incluido: \u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SoporteSolicitudComponent_div_216_span_8_i_2_Template, 1, 0, "i", 115);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, SoporteSolicitudComponent_div_216_span_8_i_3_Template, 1, 0, "i", 116);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r38.Deu_Incluido == "N");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r38.Deu_Incluido == "S");
} }
function SoporteSolicitudComponent_div_216_span_9_i_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 121);
} }
function SoporteSolicitudComponent_div_216_span_9_i_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 122);
} }
function SoporteSolicitudComponent_div_216_span_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 114);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Incluido: \u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SoporteSolicitudComponent_div_216_span_9_i_2_Template, 1, 0, "i", 119);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, SoporteSolicitudComponent_div_216_span_9_i_3_Template, 1, 0, "i", 120);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r38.Deu_Incluido == "N");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r38.Deu_Incluido == "S");
} }
function SoporteSolicitudComponent_div_216_tr_118_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Fecha de Resoluci\u00F3n: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](6, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](6, 1, deuObj_r38.Deu_FechaResolucion, 0, 10), " ");
} }
function SoporteSolicitudComponent_div_216_span_127_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_CiudadNac);
} }
function SoporteSolicitudComponent_div_216_input_128_Template(rf, ctx) { if (rf & 1) {
    const _r75 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_input_128_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r75); const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r38.Deu_CiudadNac = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_CiudadNac);
} }
function SoporteSolicitudComponent_div_216_span_134_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_Peso);
} }
function SoporteSolicitudComponent_div_216_input_135_Template(rf, ctx) { if (rf & 1) {
    const _r80 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_input_135_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r80); const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r38.Deu_Peso = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_Peso);
} }
function SoporteSolicitudComponent_div_216_span_141_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_Talla);
} }
function SoporteSolicitudComponent_div_216_input_142_Template(rf, ctx) { if (rf & 1) {
    const _r85 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_input_142_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r85); const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r38.Deu_Talla = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_Talla);
} }
function SoporteSolicitudComponent_div_216_span_148_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_Mano);
} }
function SoporteSolicitudComponent_div_216_select_149_Template(rf, ctx) { if (rf & 1) {
    const _r90 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_select_149_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r90); const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r38.Deu_Mano = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "option", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "DERECHO(A)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "option", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "ZURDO(A)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_Mano);
} }
function SoporteSolicitudComponent_div_216_span_161_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_DetACtiv);
} }
function SoporteSolicitudComponent_div_216_input_162_Template(rf, ctx) { if (rf & 1) {
    const _r95 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_input_162_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r95); const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r38.Deu_DetACtiv = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_DetACtiv);
} }
function SoporteSolicitudComponent_div_216_tr_188_Template(rf, ctx) { if (rf & 1) {
    const _r99 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "SALDO ACTUAL VERIFICADO: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "input", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_tr_188_Template_input_ngModelChange_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r99); const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deuObj_r38.Deu_MontoActAcumVerif = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_MontoActAcumVerif);
} }
function SoporteSolicitudComponent_div_216_button_209_Template(rf, ctx) { if (rf & 1) {
    const _r103 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_div_216_button_209_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r103); const deuObj_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r101 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r101.onOpenBeneficiarios(deuObj_r38.Deu_Id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Beneficiarios");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c1 = function (a0) { return { "w3-dark-grey": a0 }; };
const _c2 = function (a0) { return { "display": a0 }; };
const _c3 = function () { return ["A", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8"]; };
function SoporteSolicitudComponent_div_216_Template(rf, ctx) { if (rf & 1) {
    const _r105 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 88);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 90);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_div_216_Template_span_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r105); const i_r39 = ctx.index; const ctx_r104 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r104.deus_[i_r39] == "none" ? (ctx_r104.deus_[i_r39] = "block") : (ctx_r104.deus_[i_r39] = "none"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, SoporteSolicitudComponent_div_216_i_3_Template, 1, 0, "i", 91);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, SoporteSolicitudComponent_div_216_i_4_Template, 1, 0, "i", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, SoporteSolicitudComponent_div_216_span_6_Template, 4, 0, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, SoporteSolicitudComponent_div_216_span_7_Template, 4, 0, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, SoporteSolicitudComponent_div_216_span_8_Template, 4, 2, "span", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, SoporteSolicitudComponent_div_216_span_9_Template, 4, 2, "span", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 94);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 95);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "table", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, "Nombre Completo:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "input", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_Template_input_ngModelChange_20_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r105); const deuObj_r38 = ctx.$implicit; return deuObj_r38.Deu_Nombre = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24, "Apellido Paterno: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "input", 96);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_Template_input_ngModelChange_26_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r105); const deuObj_r38 = ctx.$implicit; return deuObj_r38.Deu_Paterno = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30, "Apellido Materno: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "input", 97);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_Template_input_ngModelChange_32_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r105); const deuObj_r38 = ctx.$implicit; return deuObj_r38.Deu_Materno = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](36, "Apellido de Casada: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](37, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "input", 98);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_Template_input_ngModelChange_38_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r105); const deuObj_r38 = ctx.$implicit; return deuObj_r38.Deu_Casada = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](41, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](42, "Direcci\u00F3n Domicilio: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](43, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](48, "Direcci\u00F3n Negocio/Oficina: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](50);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](51, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](54, "Estado Civil: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](55, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](56);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](57, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](59, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](60, "G\u00E9nero: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](61, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](62);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](63, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](65, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](66, "Documento de Identidad: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](67, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](68);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](69, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](71, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](72, "Tipo Documento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](73, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](74);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](75, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](77, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](78, "Extension Documento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](79, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](80, "input", 99);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_216_Template_input_ngModelChange_80_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r105); const deuObj_r38 = ctx.$implicit; return deuObj_r38.Deu_ExtDoc = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](81, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](83, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](84, "Complemento Documento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](85, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](86);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](87, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](88, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](89, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](90, "Pa\u00EDs de Nacimiento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](91, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](92);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](93, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](94, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](95, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](96, "Fecha Nacimiento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](97, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](98);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](99, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](100, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](101, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](102, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](103, "Edad (A\u00F1os): ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](104, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](105);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](106, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](107, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](108, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](109, "Tipo de Cobertura del Seguro: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](110, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](111);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](112, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](113, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](114, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](115, "N\u00FAmero de P\u00F3liza: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](116, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](117);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](118, SoporteSolicitudComponent_div_216_tr_118_Template, 7, 5, "tr", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](119, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](120, "table", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](121, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](122, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](123, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](124, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](125, "(*) Cuidad de Nacimiento: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](126, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](127, SoporteSolicitudComponent_div_216_span_127_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](128, SoporteSolicitudComponent_div_216_input_128_Template, 1, 1, "input", 100);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](129, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](130, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](131, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](132, "(*) Peso (KGrs): ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](133, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](134, SoporteSolicitudComponent_div_216_span_134_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](135, SoporteSolicitudComponent_div_216_input_135_Template, 1, 1, "input", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](136, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](137, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](138, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](139, "(*) Talla (Cms): ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](140, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](141, SoporteSolicitudComponent_div_216_span_141_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](142, SoporteSolicitudComponent_div_216_input_142_Template, 1, 1, "input", 102);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](143, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](144, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](145, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](146, "(*) Zurd@/Derecho@: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](147, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](148, SoporteSolicitudComponent_div_216_span_148_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](149, SoporteSolicitudComponent_div_216_select_149_Template, 5, 1, "select", 103);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](150, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](151, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](152, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](153, "Actividad: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](154, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](155);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](156, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](157, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](158, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](159, "(*) Detalle de la Actividad: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](160, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](161, SoporteSolicitudComponent_div_216_span_161_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](162, SoporteSolicitudComponent_div_216_input_162_Template, 1, 1, "input", 104);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](163, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](164, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](165, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](166, "Fecha Registro: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](167, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](168);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](169, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](170, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](171, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](172, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](173, "Celular: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](174, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](175);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](176, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](177, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](178, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](179, "Tel\u00E9fono de Contacto: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](180, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](181);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](182, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](183, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](184, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](185, "Tel\u00E9fono: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](186, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](187);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](188, SoporteSolicitudComponent_div_216_tr_188_Template, 6, 1, "tr", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](189, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](190, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](191, "b", 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](192, "SALDO ACTUAL + SOLICITUDA: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](193, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](194);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](195, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](196, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](197, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](198, "Incluido: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](199, "td", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](200);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](201, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](202, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](203, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](204, "ID: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](205, "td", 106);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](206);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](207, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](208, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](209, SoporteSolicitudComponent_div_216_button_209_Template, 5, 0, "button", 107);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](210, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](211, "button", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_div_216_Template_button_click_211_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r105); const deuObj_r38 = ctx.$implicit; const ctx_r111 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r111.onDeclaracionDeudor(deuObj_r38.Deu_Id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](212, "i", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](213, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](214, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](215, "Decla.Salud \u00A0");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deuObj_r38 = ctx.$implicit;
    const i_r39 = ctx.index;
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](57, _c1, ctx_r10.deus[i_r39]));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.deus_[i_r39] == "none");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.deus_[i_r39] == "block");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate3"](" \u00A0 ", deuObj_r38.Deu_Nombre, " ", deuObj_r38.Deu_Paterno, " ", deuObj_r38.Deu_Materno, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r38.Deu_NIvel == 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deuObj_r38.Deu_NIvel == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](59, _c2, ctx_r10.deus_[i_r39]));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_Nombre);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_Paterno);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_Materno);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_Casada);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_DirDom);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_DirOfi);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_EstCiv);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_Sexo);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_NumDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_TipoDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deuObj_r38.Deu_ExtDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_CompDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_PaisNac);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](99, 49, deuObj_r38.Deu_FecNac, 0, 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_Edad);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_cobertura);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", deuObj_r38.Deu_Poliza, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](61, _c3).includes(ctx_r10.estado_codigo));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_Actividad);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo != "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](169, 53, deuObj_r38.Deu_FecRegCli, 0, 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_TelCel);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_TelDom);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_TelOfi);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r10.esTarjeta);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", deuObj_r38.saldo_actual_solicitud, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_Incluido);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deuObj_r38.Deu_Id);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r10.estado_codigo != "X");
} }
function SoporteSolicitudComponent_button_237_Template(rf, ctx) { if (rf & 1) {
    const _r113 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 131);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_button_237_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r113); const ctx_r112 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r112.loadBeneficiarios(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_tr_238_span_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Editar");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_tr_238_span_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Ver");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_tr_238_Template(rf, ctx) { if (rf & 1) {
    const _r118 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "a", 133);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_tr_238_Template_a_click_16_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r118); const ben_r114 = ctx.$implicit; const ctx_r117 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r117.onOpenBeneficiarios(ben_r114.Ben_IdDeu, ben_r114.Ben_Id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, SoporteSolicitudComponent_tr_238_span_17_Template, 2, 0, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, SoporteSolicitudComponent_tr_238_span_18_Template, 2, 0, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ben_r114 = ctx.$implicit;
    const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ben_r114.Ben_Id);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r12.getDeudor(ben_r114.Ben_IdDeu, "nombres"));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r12.getDeudor(ben_r114.Ben_IdDeu, "tipo"));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate3"]("", ben_r114.Ben_Nombre, " ", ben_r114.Ben_Paterno, " ", ben_r114.Ben_Materno, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ben_r114.Ben_NumDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ben_r114.Ben_Porcentaje, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ben_r114.Ben_Tipo);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r12.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r12.estado_codigo != "P");
} }
function SoporteSolicitudComponent_tr_382_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const row_r119 = ctx.$implicit;
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](3, 6, row_r119.Obs_FechaReg, 0, 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r119.Obs_HoraReg);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r119.Obs_Observacion);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r119.Obs_Usuario);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r13.getEifAseguradora(row_r119.Obs_Eeff));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r13.getEifAseguradora(row_r119.Obs_Aseg));
} }
function SoporteSolicitudComponent_div_402_tr_6_input_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "input", 137);
} }
function SoporteSolicitudComponent_div_402_tr_6_input_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "input", 138);
} }
function SoporteSolicitudComponent_div_402_tr_6_input_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "input", 137);
} }
function SoporteSolicitudComponent_div_402_tr_6_input_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "input", 138);
} }
function SoporteSolicitudComponent_div_402_tr_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Aprobado: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, SoporteSolicitudComponent_div_402_tr_6_input_5_Template, 1, 0, "input", 135);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, SoporteSolicitudComponent_div_402_tr_6_input_6_Template, 1, 0, "input", 136);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, " Rechazado: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, SoporteSolicitudComponent_div_402_tr_6_input_8_Template, 1, 0, "input", 135);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, SoporteSolicitudComponent_div_402_tr_6_input_9_Template, 1, 0, "input", 136);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Extra Prima: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "Condiciones/Exclusiones: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const apro_r121 = ctx.$implicit;
    const ctx_r120 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r120.getDeudor(apro_r121.Apr_IdDeu, "nombres"));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", apro_r121.Apr_Aprobado == "S");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", apro_r121.Apr_Aprobado != "S");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", apro_r121.Apr_Aprobado == "N");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", apro_r121.Apr_Aprobado != "N");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](apro_r121.Apr_ExtraPrima);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](apro_r121.Apr_Condiciones);
} }
function SoporteSolicitudComponent_div_402_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "table", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td", 134);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "b", 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "PRONUNCIAMIENTO DE LA COMPA\u00D1\u00CDA");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, SoporteSolicitudComponent_div_402_tr_6_Template, 18, 7, "tr", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r14.data.seg_aprobaciones);
} }
function SoporteSolicitudComponent_div_403_tr_2_i_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 149);
} }
function SoporteSolicitudComponent_div_403_tr_2_i_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 149);
} }
function SoporteSolicitudComponent_div_403_tr_2_ng_container_13_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deu_r127.extra_prima);
} }
function SoporteSolicitudComponent_div_403_tr_2_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    const _r137 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 150);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_403_tr_2_ng_container_13_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r137); const deu_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deu_r127.extra_prima = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SoporteSolicitudComponent_div_403_tr_2_ng_container_13_span_2_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const deu_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r130 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r127.extra_prima);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r130.estado_general == "APROBADO");
} }
function SoporteSolicitudComponent_div_403_tr_2_ng_container_16_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deu_r127.condiciones_exclusiones);
} }
function SoporteSolicitudComponent_div_403_tr_2_ng_container_16_Template(rf, ctx) { if (rf & 1) {
    const _r143 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 151);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_403_tr_2_ng_container_16_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r143); const deu_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deu_r127.condiciones_exclusiones = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SoporteSolicitudComponent_div_403_tr_2_ng_container_16_span_2_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const deu_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r127.condiciones_exclusiones);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r131.estado_general == "APROBADO");
} }
function SoporteSolicitudComponent_div_403_tr_2_ng_container_17_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](deu_r127.causal);
} }
function SoporteSolicitudComponent_div_403_tr_2_ng_container_17_Template(rf, ctx) { if (rf & 1) {
    const _r149 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 152);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_403_tr_2_ng_container_17_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r149); const deu_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return deu_r127.causal = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SoporteSolicitudComponent_div_403_tr_2_ng_container_17_span_2_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const deu_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r132 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r127.causal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r132.estado_general == "APROBADO");
} }
function SoporteSolicitudComponent_div_403_tr_2_Template(rf, ctx) { if (rf & 1) {
    const _r153 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr", 143);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, " Aprobado ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "input", 145);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SoporteSolicitudComponent_div_403_tr_2_Template_input_change_6_listener() { const deu_r127 = ctx.$implicit; return deu_r127.rechazado = false; })("ngModelChange", function SoporteSolicitudComponent_div_403_tr_2_Template_input_ngModelChange_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r153); const deu_r127 = ctx.$implicit; return deu_r127.aprobado = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, SoporteSolicitudComponent_div_403_tr_2_i_7_Template, 1, 0, "i", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, " Rechazado ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "input", 147);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SoporteSolicitudComponent_div_403_tr_2_Template_input_change_10_listener() { const deu_r127 = ctx.$implicit; return deu_r127.aprobado = false; })("ngModelChange", function SoporteSolicitudComponent_div_403_tr_2_Template_input_ngModelChange_10_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r153); const deu_r127 = ctx.$implicit; return deu_r127.rechazado = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, SoporteSolicitudComponent_div_403_tr_2_i_11_Template, 1, 0, "i", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td", 148);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, SoporteSolicitudComponent_div_403_tr_2_ng_container_13_Template, 3, 2, "ng-container", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, " \u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "td", 148);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, SoporteSolicitudComponent_div_403_tr_2_ng_container_16_Template, 3, 2, "ng-container", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, SoporteSolicitudComponent_div_403_tr_2_ng_container_17_Template, 3, 2, "ng-container", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, " \u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r127 = ctx.$implicit;
    const ctx_r126 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate3"]("", deu_r127.Deu_Nombre, " ", deu_r127.Deu_Paterno, " ", deu_r127.Deu_Materno, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r127.aprobado);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r127.aprobado && ctx_r126.estado_general == "APROBADO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", deu_r127.rechazado);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r127.rechazado && ctx_r126.estado_general == "APROBADO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r127.aprobado);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r127.aprobado);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r127.rechazado);
} }
function SoporteSolicitudComponent_div_403_Template(rf, ctx) { if (rf & 1) {
    const _r157 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "table", 139);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SoporteSolicitudComponent_div_403_tr_2_Template, 19, 10, "tr", 140);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td", 141);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "button", 142);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_div_403_Template_button_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r157); const ctx_r156 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r156.onSaveResolucion(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "i", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, " Aceptar y Guardar ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r15.data.seg_deudores);
} }
function SoporteSolicitudComponent_tr_415_ng_container_9_Template(rf, ctx) { if (rf & 1) {
    const _r164 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 160);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_tr_415_ng_container_9_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r164); const salud_r158 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return salud_r158.Adic_Comment = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const salud_r158 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", salud_r158.Adic_Comment);
} }
function SoporteSolicitudComponent_tr_415_ng_template_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](0);
} if (rf & 2) {
    const salud_r158 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", salud_r158.Adic_Comment, "");
} }
function SoporteSolicitudComponent_tr_415_Template(rf, ctx) { if (rf & 1) {
    const _r169 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td", 153);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td", 154);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " SI ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "input", 155);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SoporteSolicitudComponent_tr_415_Template_input_change_5_listener() { const salud_r158 = ctx.$implicit; return salud_r158.valor_no = false; })("ngModelChange", function SoporteSolicitudComponent_tr_415_Template_input_ngModelChange_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r169); const salud_r158 = ctx.$implicit; return salud_r158.valor_si = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, " NO ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "input", 156);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SoporteSolicitudComponent_tr_415_Template_input_change_7_listener() { const salud_r158 = ctx.$implicit; return salud_r158.valor_si = false; })("ngModelChange", function SoporteSolicitudComponent_tr_415_Template_input_ngModelChange_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r169); const salud_r158 = ctx.$implicit; return salud_r158.valor_no = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "td", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, SoporteSolicitudComponent_tr_415_ng_container_9_Template, 2, 1, "ng-container", 158);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, SoporteSolicitudComponent_tr_415_ng_template_10_Template, 1, 1, "ng-template", null, 159, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const salud_r158 = ctx.$implicit;
    const _r160 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](11);
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"]("", salud_r158.Adic_Pregunta, ". ", salud_r158.Adic_Texto, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", salud_r158.valor_si);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", salud_r158.valor_no);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r16.onAclaracion(salud_r158))("ngIfElse", _r160);
} }
function SoporteSolicitudComponent_tr_444_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const row_r172 = ctx.$implicit;
    const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](3, 6, row_r172.Obs_FechaReg, 0, 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r172.Obs_HoraReg);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r172.Obs_Observacion);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](row_r172.Obs_Usuario);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r17.getEifAseguradora(row_r172.Obs_Eeff));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r17.getEifAseguradora(row_r172.Obs_Aseg));
} }
function SoporteSolicitudComponent_div_459_i_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 110);
} }
function SoporteSolicitudComponent_div_459_i_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "i", 111);
} }
function SoporteSolicitudComponent_div_459_i_6_Template(rf, ctx) { if (rf & 1) {
    const _r180 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "i", 203);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_div_459_i_6_Template_i_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r180); const ctx_r179 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); const bene_r173 = ctx_r179.$implicit; const i_r174 = ctx_r179.index; const ctx_r178 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r178.onDelBeneficiario(bene_r173.Ben_Id, i_r174); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c4 = function (a0, a1) { return { "w3-hide": a0, "w3-show": a1 }; };
function SoporteSolicitudComponent_div_459_Template(rf, ctx) { if (rf & 1) {
    const _r182 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_div_459_Template_span_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const i_r174 = ctx.index; const ctx_r181 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r181.bens[i_r174] ? (ctx_r181.bens[i_r174] = false) : (ctx_r181.bens[i_r174] = true); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, SoporteSolicitudComponent_div_459_i_3_Template, 1, 0, "i", 91);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, SoporteSolicitudComponent_div_459_i_4_Template, 1, 0, "i", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, SoporteSolicitudComponent_div_459_i_6_Template, 1, 0, "i", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 164);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "table", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "(*) Nombre Completo:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "input", 165);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_input_ngModelChange_15_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_Nombre = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "(*) Apellido Paterno:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "input", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_input_ngModelChange_19_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_Paterno = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](20, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "(*) Apellido Materno:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "input", 167);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_input_ngModelChange_25_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_Materno = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](27, "Apellido de Casada:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "input", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_input_ngModelChange_29_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_Casada = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](30, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33, "G\u00E9nero (Sexo):");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "select", 169);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_select_ngModelChange_35_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_Sexo = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "option", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37, "MASCULINO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "option", 171);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](39, "FEMENINO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](40, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](41, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](43, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](44, "(*) Numero documento");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "input", 172);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_input_ngModelChange_46_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_NumDoc = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](48, "(*) Tipo:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "select", 173);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_select_ngModelChange_50_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_TipoDoc = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](51, "option", 174);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](52, "CARNET DE IDENTIDAD");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "option", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](54, "CARNET EXTRANJERO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](55, "option", 176);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](56, "PASAPORTE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](57, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](59, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](60, "Extensi\u00F3n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](61, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](62, "select", 177);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_select_ngModelChange_62_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_ExtDoc = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](63, "option", 178);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](64, "LA PAZ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](65, "option", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](66, "COCHABAMBA");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](67, "option", 180);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](68, "SANTA CRUZ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](69, "option", 181);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](70, "ORURO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](71, "option", 182);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](72, "POTOSI");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](73, "option", 183);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](74, "CHUQUISACA");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](75, "option", 184);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](76, "TARIJA");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](77, "option", 185);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](78, "BENI");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](79, "option", 186);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](80, "PANDO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](81, "option", 187);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](82, "Sin Extensi\u00F3n ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](83, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](84, "Complemento documento");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](85, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](86, "input", 188);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_input_ngModelChange_86_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_CompDoc = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](87, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](88, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](89, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](90, "(*) Relaci\u00F3n:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](91, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](92, "select", 189);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_select_ngModelChange_92_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_Relacion = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](93, "option", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](94, "CONVIVIENTE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](95, "option", 191);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](96, "CONYUGUE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](97, "option", 192);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](98, "HIJO(A)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](99, "option", 193);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](100, "HERMANO(A)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](101, "option", 194);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](102, "MADRE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](103, "option", 195);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](104, "PADRE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](105, "option", 196);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](106, "OTROS");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](107, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](108, "(*) Porcentaje cobertura");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](109, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](110, "input", 197);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_input_ngModelChange_110_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_Porcentaje = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](111, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](112, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](113, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](114, "Tel\u00E9fonos de Contacto:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](115, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](116, "input", 198);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_input_ngModelChange_116_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_Telefono = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](117, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](118, "(*) Tipo Beneficiario");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](119, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](120, "select", 199);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_div_459_Template_select_ngModelChange_120_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182); const bene_r173 = ctx.$implicit; return bene_r173.Ben_Tipo = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](121, "option", 200);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](122, "PRIMARIO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](123, "option", 201);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](124, "CONTINGENTE");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](125, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](126, "div", 202);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](127, "* Campos requeridos");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bene_r173 = ctx.$implicit;
    const i_r174 = ctx.index;
    const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](23, _c1, ctx_r18.bens[i_r174]));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r18.bens[i_r174]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r18.bens[i_r174]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate5"](" \u00A0 ", bene_r173.Ben_Nombre, " ", bene_r173.Ben_Paterno, " ", bene_r173.Ben_Materno, " | ", bene_r173.Ben_Tipo, " | ", bene_r173.Ben_Porcentaje, " % ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r18.bens[i_r174] && ctx_r18.estado_codigo == "P");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](25, _c4, !ctx_r18.bens[i_r174], ctx_r18.bens[i_r174]));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_Nombre);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_Paterno);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_Materno);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_Casada);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_Sexo);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_NumDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_TipoDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_ExtDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_CompDoc);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_Relacion);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_Porcentaje);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_Telefono);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", bene_r173.Ben_Tipo);
} }
function SoporteSolicitudComponent_tr_481_td_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r196 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate3"]("Deudor : ", deu_r196.Deu_Nombre, " ", deu_r196.Deu_Paterno, " ", deu_r196.Deu_Materno, "");
} }
function SoporteSolicitudComponent_tr_481_td_2_Template(rf, ctx) { if (rf & 1) {
    const _r203 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "button", 205);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_tr_481_td_2_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r203); const deu_r196 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r201 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r201.onImprimirV2(deu_r196.Deu_Id, deu_r196.id_documento_version); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Imprimir documentos");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function SoporteSolicitudComponent_tr_481_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, SoporteSolicitudComponent_tr_481_td_1_Template, 2, 3, "td", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, SoporteSolicitudComponent_tr_481_td_2_Template, 5, 0, "td", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const deu_r196 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r196.Deu_Incluido == "S");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", deu_r196.Deu_Incluido == "S");
} }
function SoporteSolicitudComponent_li_494_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li", 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const obs_r204 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", obs_r204, " ");
} }
function SoporteSolicitudComponent_li_496_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li", 206);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const obs_r205 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", obs_r205, " ");
} }
function SoporteSolicitudComponent_option_517_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const itm_r206 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngValue", itm_r206.autorizado_por);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", itm_r206.autorizado_por, " ");
} }
const _c5 = function () { return ["P", "A"]; };
const _c6 = function () { return ["C1", "C2"]; };
const _c7 = function () { return ["C1", "C2", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8"]; };
const _c8 = function () { return ["A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8"]; };
class SoporteSolicitudComponent {
    constructor(router, activatedRoute, sanitizer, msg, reporteService, segBeneficiariosService, ecofuturoService, segAprobacionesService, sendMailService, segObservacionesService, fileUploadService, authService, segSolicitudesService, segDeudoresService, segAdicionalesService, parametrosCampoService) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.sanitizer = sanitizer;
        this.msg = msg;
        this.reporteService = reporteService;
        this.segBeneficiariosService = segBeneficiariosService;
        this.ecofuturoService = ecofuturoService;
        this.segAprobacionesService = segAprobacionesService;
        this.sendMailService = sendMailService;
        this.segObservacionesService = segObservacionesService;
        this.fileUploadService = fileUploadService;
        this.authService = authService;
        this.segSolicitudesService = segSolicitudesService;
        this.segDeudoresService = segDeudoresService;
        this.segAdicionalesService = segAdicionalesService;
        this.parametrosCampoService = parametrosCampoService;
        this.num_solicitud = 0;
        this.nro_solicitud = 0;
        this.id_folder = 0;
        this.id_prestamo = 0;
        this.numero_documento = 0;
        this.id_entidad_seguro = 0;
        this.titular = '';
        this.estado_general = '';
        this.estado_codigo = '';
        this.numero_poliza = '';
        this.tipo_cobertura = '';
        this.tipo_cartera = '';
        this.id_codeudor = 0;
        this.id_deudor = 0;
        this.id_cargo = 0;
        this.id_deudor_Selec = 0;
        this.deudor_nombre_sel = '';
        this._estados_solicitud = [];
        this.resolucion = { aprobado: false, rechazado: false,
            extra_prima: null, condiciones_exclusiones: null, causal: null };
        this.desistimiento = { causal: '', autorizado_por: '' };
        this.id_tipo_seguro = 0;
        this.file = null;
        this.download_url = src_environments_environment__WEBPACK_IMPORTED_MODULE_25__["environment"].URL_PUBLIC_FILES;
        this.tab_active = 0;
        this.openModal = 'none';
        this.openBen = 'none';
        this.openModalSeguros = 'none';
        this.openModalCodeu = 'none';
        this.openModalBene = 'none';
        this.openModalCuestion = 'none';
        this.openModalMensajes = 'none';
        this.openModalImprimir = 'none';
        this.openModalHistorial = 'none';
        this.openModalValida = 'none';
        this.openModalDesistir = 'none';
        this.display = '';
        this.showDocs = false;
        this.showDocs_1 = false;
        this.showDocs_2 = false;
        this.showImp = [false, false, false, false, false, false, false, false, false, false];
        this.solicitudValid = false;
        this.estadosSolicitud = [];
        this.segBeneficiarios = [];
        this.segBeneficiariosIdDeu = [];
        this.segObservaciones = [];
        this.segDeudores = [];
        this.segAprobaciones = [];
        this.deudorSeleccionado = new _models_seg_deudores__WEBPACK_IMPORTED_MODULE_23__["SegDeudores"]();
        this.autorizacionesSolicitud = [];
        this.benIdDeu = 0;
        this.beneficiarios = [];
        this.beneficiarioDe = '';
        // data:DataSolicitud = {} as DataSolicitud;
        this.data = {
            seg_solicitudes: new _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_18__["SegSolicitudes"](),
            seg_deudores: [new _models_seg_deudores__WEBPACK_IMPORTED_MODULE_23__["SegDeudores"]()],
            seg_operaciones: new _models_seg_operaciones__WEBPACK_IMPORTED_MODULE_24__["SegOperaciones"](),
            seg_beneficiarios: [new _models_seg_beneficiarios__WEBPACK_IMPORTED_MODULE_19__["SegBeneficiarios"]()],
            seg_adicionales: [new _models_seg_adicionales__WEBPACK_IMPORTED_MODULE_20__["SegAdicionales"]()],
            seg_aprobaciones: [new _models_seg_aprobaciones__WEBPACK_IMPORTED_MODULE_21__["SegAprobaciones"]()],
            seg_observaciones: [new _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_22__["SegObservaciones"]()],
            estado_general: '',
            config_param: {
                datos_poliza: [],
                tipos_cobertura: [],
                estado_solicitud: [],
                estado_solicitud_resolucion: [],
                autorizaciones_solicitud: [],
                impresion_documentos: [],
                djs_salud: [],
                correos_estados: [],
                notificaciones_mensajes: []
            }
        };
        this.dataOri = {};
        this.datosCalculados = {
            tasa_mensual_ref: 0.00,
            prima_mensual_ref: 0.00,
            entidad_seguros_1: "",
            entidad_seguros_2: "",
            saldo_actual_solicitado: 0.00
        };
        this.isEdit = true;
        this.codeudores_def = [];
        this.observaciones = [];
        this.advertencias = [];
        this.deus_ = ['none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none'];
        this.deus = [false, false, false, false, false, false, false, false, false, false];
        this.bens = [false, false, false, false, false, false, false, false, false, false];
        this.btns = [false, false, false, false, false, false, false, false, false, false];
        this.op = [true, false, false, false, false, false, false, false, false, false];
        this.enviarFinanciera = false;
        this.param_buscar = {
            accion: 'add-codeudor',
            numero_documento: '',
            id_folder: 0,
        };
        this.hoy = new Date();
        this.respuestaObser = '';
        this.offGuardarDeudor = true;
        this.offGuardarCoDeudor = true;
        this.viewTable = '';
        this.formData = new FormData();
        this.segObservacionesUlt = [new _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_22__["SegObservaciones"]()];
        this.esTarjeta = false;
        this.isLoading = false;
        this.esObligadosIncluidos = false;
        // funciion DJS filtra declaraciones del deudor
        this.onDeclaracionDeudor = (deuId) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.declaraciones = [];
            this.openModalCuestion = 'block';
            const declara_codeu = yield this.data.seg_adicionales.filter(row => row.Adic_IdDeu == deuId);
            declara_codeu.sort((a, b) => parseInt(a.Adic_Pregunta) - parseInt(b.Adic_Pregunta));
            //this.declaraciones = JSON.parse(JSON.stringify(declara_codeu));
            this.declaraciones = [...declara_codeu];
        });
        this.onValidar = () => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.onGuardarDatosEdit();
            this.observaciones = [];
            this.advertencias = [];
            // datos requeridos SOLICITUD
            // Sol_MontoSol | Sol_MonedaSol | Sol_TipoSeg | Sol_TipoParam | Sol_Poliza | Sol_EstadoSol
            if (!this.data.seg_solicitudes.Sol_MontoSol) {
                this.observaciones.push(`Solicitud :  Monto solicitado requerido `);
            }
            ;
            if (!this.data.seg_solicitudes.Sol_MonedaSol) {
                this.observaciones.push(`Solicitud :  Moneda requerido `);
            }
            ;
            if (!this.data.seg_solicitudes.Sol_TipoSeg) {
                this.observaciones.push(`Solicitud :  Tipo de Seguro requerido `);
            }
            ;
            //if (!this.data.seg_solicitudes.Sol_TipoParam){this.observaciones.push(`Solicitud :  Tipo de Cobertura requerido `)};
            //if (!this.data.seg_solicitudes.Sol_Poliza){this.observaciones.push(`Solicitud :  Nro.Poliza requerido `)};
            if (!this.data.seg_solicitudes.Sol_EstadoSol) {
                this.observaciones.push(`Solicitud :  Estado Solicitud requerido `);
            }
            ;
            // datos requeridos DEUDORES
            // Deu_DetACtiv | Deu_CiudadNac | Deu_Peso | Deu_Talla | Deu_MontoActAcumVerif | Deu_Mano
            // saldo_actual_solicitud | Hist_TipoSeg
            const cant_obligados = this.data.seg_deudores.length;
            let cant_incluido = 0;
            // 4.CAMPOS REQUERIDOS EN DJS >> Adic_Respuesta: "S||||||||||"
            let djs = true; // DJS esta NORMAL
            this.data.seg_deudores.forEach(deudor => {
                //solo deudores incluidos
                if (deudor.Deu_Incluido == 'S') {
                    let hay_djs = false;
                    let hay_ben = false;
                    if (!deudor.Deu_CiudadNac) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Cuidad de Nacimiento requerido `);
                    }
                    ;
                    if (!deudor.Deu_Peso) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Peso (KGrs) requerido `);
                    }
                    ;
                    if (!deudor.Deu_Talla) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Talla (Cms) requerido `);
                    }
                    ;
                    if (!deudor.Deu_Mano) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Zurd@/Derecho@: requerido `);
                    }
                    ;
                    if (!deudor.Deu_DetACtiv) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Detalle de la Actividad requerido `);
                    }
                    ;
                    if (parseFloat(deudor.Deu_MontoActAcumVerif) < 0) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : SALDO ACTUAL VERIFICADO no valido `);
                    }
                    ;
                    if (!deudor.saldo_actual_solicitud) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : SALDO ACTUAL + SOLICITUD requerido `);
                    }
                    ;
                    const benDeudorObj = this.data.seg_beneficiarios.find(el => el.Ben_IdDeu == deudor.Deu_Id);
                    if (!benDeudorObj) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')} : Beneficiarios requeridos `);
                    }
                    cant_incluido++;
                    // control de DJS
                    this.data.seg_adicionales.forEach(row => {
                        if (row.Adic_IdDeu == deudor.Deu_Id) {
                            hay_djs = true;
                            if (row.valor_si != row.valida_por) {
                                djs = false;
                                if (!row.Adic_Comment) {
                                    this.observaciones.push(`DJS ${this.getDeudor(row.Adic_IdDeu, 'nombres')} : Pregunta ${row.Adic_Pregunta}, requiere SI o NO o Aclaración `);
                                }
                            }
                        }
                    });
                    if (!hay_djs) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')}: DJS es requerido  `);
                    }
                    // validamos BENEFICIARIOS
                    let num_p = 0;
                    let num_c = 0;
                    this.data.seg_beneficiarios.forEach(el => {
                        console.log(el.Ben_IdDeu, deudor.Deu_Id);
                        if (el.Ben_IdDeu == deudor.Deu_Id) {
                            hay_ben = true;
                            if (el.Ben_Tipo == 'CONTINGENTE') {
                                num_c++;
                            }
                            ;
                            if (el.Ben_Tipo == 'PRIMARIO') {
                                num_p++;
                            }
                            ;
                        }
                    });
                    if (this.data.seg_solicitudes.Sol_TipoSeg == 'NO LICITADA') {
                        if (num_c == 0) {
                            this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')}: Debe completar los Beneficiarios CONTINGENTES `);
                        }
                        ;
                    }
                    if (num_p == 0) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')}: Debe completar los Beneficiarios PRIMARIOS  `);
                    }
                    ;
                    if (!hay_ben) {
                        this.observaciones.push(`${this.getDeudor(deudor.Deu_Id, 'nombres')}: Beneficiarios es requerido  `);
                    }
                }
            });
            // CONTROL DE OBLIGADOS
            console.log(`OBLIGADOS : ${cant_obligados} INCLUIDO : ${cant_incluido} `);
            if (!this.esObligadosIncluidos) {
                if (cant_obligados > 1 && cant_incluido <= 1) {
                    // this.observaciones.push(`Se requiere por lo menos un codeudor `);
                }
                ;
            }
            ;
            // HAY OBSERVACIONES
            if (this.observaciones.length > 0) {
                this.openModalValida = 'block';
                //this.solicitudValid = false;
            }
            else {
                this.msg.success('Validación satisfactoria ', 'VALIDACION');
                this.solicitudValid = true;
                // E S T A D O  D E  L A  S O L I C I T U D
                // FREE COVER O A+B+C
                const poliza = this.data.seg_solicitudes.Sol_Poliza;
                const cartera = this.data.seg_solicitudes.Sol_TipoSeg; // NO LICITADA | LICITADA
                const cobertura = this.data.seg_solicitudes.Sol_TipoParam; // VG | DH
                const monto = this.data.seg_solicitudes.Sol_MontoSol;
                const moneda = this.data.seg_solicitudes.Sol_MonedaSol;
                let nro_poliza; // = this.data.seg_solicitudes.Sol_Poliza;
                let estado_evaluado;
                let respuestasEstadosEvaluados = [];
                if (cartera == 'LICITADA') {
                    this.data.seg_deudores.forEach(deudor => {
                        if (deudor.Deu_Incluido == 'S') {
                            // k_param = (parseFloat(peso)+100)-parseFloat(talla);
                            // imc_param = parseFloat(peso)/((paserFloat(talla)/100)*(paserFloat(talla)/100))
                            const edad = deudor.Deu_Edad;
                            const peso = deudor.Deu_Peso;
                            const talla = deudor.Deu_Talla;
                            const saldo_actual_verificado_titular = deudor.Deu_MontoActAcumVerif;
                            const k_param = (parseFloat(peso) + 100) - parseFloat(talla);
                            const monto_saldo = monto + parseFloat(saldo_actual_verificado_titular);
                            nro_poliza = deudor.Deu_Poliza;
                            // DJS del deudor >> Adic_Respuesta: "S||||||||||"
                            let djs_deu = true; // DJS esta NORMAL
                            this.data.seg_adicionales.forEach(row => {
                                if (row.Adic_IdDeu == deudor.Deu_Id) {
                                    if (row.valor_si != row.valida_por) {
                                        djs_deu = false;
                                    }
                                }
                            });
                            console.log(`1.VALIDACION PARAMETROS :
            deudor : ${deudor.Deu_Nombre}
            cartera: ${cartera},
            cobertura: ${cobertura},
            monto_saldo:${monto_saldo},
            peso:${peso},
            talla:${talla},
            djs:${djs_deu},
            k:${k_param},
            estado_evaludado:${estado_evaluado}`);
                            console.log(deudor.Deu_Nombre, ':', cobertura, monto_saldo, k_param);
                            const estadoDeudor = this.configParam.estado_solicitud_licitada_update.find(row => (edad >= row.edad_min && edad <= row.edad_max) &&
                                (monto_saldo >= row.monto_desde && monto_saldo <= row.monto_hasta) &&
                                (k_param >= row.k_desde && k_param <= row.k_hasta) &&
                                djs_deu == row.djs && row.nro_poliza == nro_poliza);
                            respuestasEstadosEvaluados.push(estadoDeudor);
                            if (estadoDeudor) {
                                if (!estado_evaluado) {
                                    estado_evaluado = estadoDeudor.estado;
                                }
                                if (estadoDeudor.estado != 'A') {
                                    estado_evaluado = estadoDeudor.estado;
                                }
                            }
                            else {
                                estado_evaluado = '?';
                                this.msg.warning('Rangos NO DETERMINADOS para el Estado de la Solicitud ', 'VALIDACION');
                            }
                        }
                        ;
                    });
                }
                // djs, k , monto , imc
                if (cartera == 'NO LICITADA') {
                    this.data.seg_deudores.forEach(deudor => {
                        if (deudor.Deu_Incluido == 'S') {
                            // k_param = (parseFloat(peso)+100)-parseFloat(talla);
                            // imc_param = parseFloat(peso)/((paserFloat(talla)/100)*(paserFloat(talla)/100))
                            const peso = deudor.Deu_Peso;
                            const talla = deudor.Deu_Talla;
                            const edad = deudor.Deu_Edad;
                            const saldo_actual_verificado_titular = deudor.Deu_MontoActAcumVerif;
                            const k_param = (parseFloat(peso) + 100) - parseFloat(talla);
                            const imc_param = parseFloat(peso) / (parseFloat(talla) * (parseFloat(talla) / 10000));
                            const monto_saldo = monto + parseFloat(saldo_actual_verificado_titular);
                            const seguro_anterior = (deudor.Hist_TipoSeg ? deudor.Hist_TipoSeg : cobertura);
                            const seguro_actual = (deudor.Deu_cobertura ? deudor.Deu_cobertura : cobertura);
                            nro_poliza = deudor.Deu_Poliza;
                            // DJS del deudor >> Adic_Respuesta: "S||||||||||"
                            let djs_deu = true; // DJS esta NORMAL
                            this.data.seg_adicionales.forEach(row => {
                                if (row.Adic_IdDeu == deudor.Deu_Id) {
                                    if (row.valor_si != row.valida_por) {
                                        djs_deu = false;
                                    }
                                }
                            });
                            console.log(`1.VALIDACION PARAMETROS :
            deudor : ${deudor.Deu_Nombre}
            cartera: ${cartera},
            cobertura: ${cobertura},
            edad: ${edad},
            monto_saldo:${monto_saldo},
            peso:${peso},
            talla:${talla},
            djs:${djs_deu},
            k:${k_param},
            imc:${imc_param}
            estado_evaludado:${estado_evaluado}`);
                            const estadosSolicitudNoLicitada = this.configParam.estados_solicitud_no_licitada.find(row => row.nro_poliza == nro_poliza);
                            console.log(`2.VALICION RANGOS : `, estadosSolicitudNoLicitada);
                            if (estadosSolicitudNoLicitada) {
                                const factor = estadosSolicitudNoLicitada.factor;
                                if (factor == 'imc') {
                                    const estadosPoliza = estadosSolicitudNoLicitada.estados.find(row => (edad >= row.edad_min && edad <= row.edad_max) &&
                                        (monto_saldo >= row.monto_min && monto_saldo <= row.monto_max) &&
                                        (imc_param >= row.imc_desde && imc_param <= row.imc_hasta) &&
                                        djs_deu == row.djs);
                                    respuestasEstadosEvaluados.push(Object.assign(Object.assign({}, estadosPoliza), { deudor: `${deudor.Deu_Nombre} ${deudor.Deu_Paterno}`, id_deudor: deudor.Deu_Id }));
                                    console.log(`3.VALICION RESPUESTA ESTADO : `, estadosPoliza);
                                    if (estadosPoliza) {
                                        if (!estado_evaluado) {
                                            estado_evaluado = estadosPoliza.estado;
                                            console.log('3.1.set estado_evaludado :', estado_evaluado);
                                        }
                                        if (estadosPoliza.estado != 'A') {
                                            estado_evaluado = estadosPoliza.estado;
                                            console.log('3.2.set estado_evaludado :', estado_evaluado);
                                        }
                                    }
                                    else {
                                        estado_evaluado = '?';
                                        this.msg.error(`${deudor.Deu_Nombre},Rangos NO DETERMINADOS`, 'VALIDACION');
                                    }
                                }
                                else {
                                    const estadosPoliza = estadosSolicitudNoLicitada.estados.find(row => row.seguro_actual == seguro_actual &&
                                        row.seguro_anterior == seguro_anterior &&
                                        (monto_saldo >= row.monto_desde && monto_saldo <= row.monto_hasta) &&
                                        (k_param >= row.k_desde && k_param <= row.k_hasta) &&
                                        djs == row.djs);
                                    console.log(`4.VALICION RESPUESTA ESTADO : `, estadosPoliza);
                                    if (estadosPoliza) {
                                        if (!estado_evaluado) {
                                            estado_evaluado = estadosPoliza.estado;
                                            console.log('4.1.set estado_evaludado :', estado_evaluado);
                                        }
                                        if (estadosPoliza.estado != 'A') {
                                            estado_evaluado = estadosPoliza.estado;
                                            console.log('4.2.set estado_evaludado :', estado_evaluado);
                                        }
                                    }
                                    else {
                                        estado_evaluado = '?';
                                        this.msg.warning(`${deudor.Deu_Nombre},Rangos NO DETERMINADOS`, 'VALIDACION');
                                    }
                                }
                            }
                            else {
                                estado_evaluado = '?';
                                this.msg.warning('Rangos NO DETERMINADOS para la POLIZA ', 'VALIDACION');
                            }
                            ;
                        }
                        ;
                    });
                }
                console.log('Estado final estado_evaludado :', estado_evaluado);
                if (estado_evaluado) {
                    if (estado_evaluado == 'A') {
                        this.aprobacionAutomatica(estado_evaluado);
                    }
                    if (['C1', 'C2'].includes(estado_evaluado)) {
                        let mensajeFinal;
                        // DETERMINAR MENSAJE A LA COMPANIA
                        if (respuestasEstadosEvaluados.length > 0) {
                            mensajeFinal = 'Se esta determinando enviar a la compañia por: <br>';
                            respuestasEstadosEvaluados.forEach(row => {
                                if (['C1', 'C2'].includes(row.estado)) {
                                    if (!row.djs) {
                                        mensajeFinal += `Declaración de Salud tiene Obsevaciones <br>`;
                                    }
                                    if (row.imc_desde) {
                                        mensajeFinal += `Revisar los rangos de IMC <br>`;
                                    }
                                    if (row.k_desde) {
                                        mensajeFinal += `Revisar los los rangos del Factor  K <br>`;
                                    }
                                    if (row.monto_max) {
                                        mensajeFinal += `Excedio el monto de ${row.monto_max}  <br>`;
                                    }
                                    if (row.requisitos) {
                                        mensajeFinal += `Los requisitos son: ${row.requisitos}  <br>`;
                                    }
                                }
                                ;
                            });
                            console.log('6. MENSAJE FINAL', mensajeFinal);
                        }
                        ;
                        this.evaluacionPendiente(estado_evaluado, mensajeFinal);
                    }
                    if (estado_evaluado == 'R') {
                        const rechazo = respuestasEstadosEvaluados.find(el => el.estado == 'R');
                        if (rechazo) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                                icon: 'warning',
                                title: 'Advertencia!',
                                text: `${rechazo.deudor}, excedio los límites de asegurabilidad permitidos, por lo que este será excluido, para fines de la suscripción.`,
                                showCancelButton: true,
                                confirmButtonText: `Aceptar`,
                                cancelButtonText: `Cancelar`,
                                confirmButtonColor: '#16A085',
                                cancelButtonColor: '#F1948A',
                            }).then(result => {
                                if (result.isConfirmed) {
                                    if (rechazo.id_deudor) {
                                        console.log('EXCLUIR A : ', rechazo.id_deudor);
                                        this.data.seg_deudores.find(el => el.Deu_Id == rechazo.id_deudor).Deu_Incluido = 'N';
                                        this.esObligadosIncluidos = true;
                                    }
                                }
                            });
                        }
                    }
                }
                else {
                    this.msg.warning('No se pudo determinar el estado de la EVALUACION', 'EVALUACION');
                }
            }
            ;
        });
        this.calculate_age = (dob) => {
            var diff_ms = Date.now() - dob.getTime();
            var age_dt = new Date(diff_ms);
            return Math.abs(age_dt.getUTCFullYear() - 1970);
        };
        //  GESTION B E N E F I C I A R I O S
        this.loadBeneficiarios = () => {
            this.segBeneficiarios = [];
            //this.segBeneficiariosIdDeu = [];
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                allowOutsideClick: false,
                text: 'Recuperando datos ...'
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
            this.segBeneficiariosService.getIdSol(this.id_solicitud).subscribe(resp => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                const response = resp;
                if (response.status === 'OK') {
                    this.segBeneficiarios = [...response.data];
                }
                else {
                    this.msg.error(response.message);
                }
            }, err => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                this.msg.error('No se pudo establecer comunicacion', 'BENEFICIARIOS');
            });
        };
        // button BENEFICIARIOS (click)
        this.onOpenBeneficiarios = (idDeu, idBen) => {
            this.id_deudor_Selec = idDeu;
            console.log('ID BENEFICIARIO:', idDeu, this.id_deudor);
            this.deudorSeleccionado.Deu_Id = idDeu;
            const deudorBusca = this.data.seg_deudores.find(el => el.Deu_Id == idDeu);
            if (deudorBusca) {
                this.deudor_nombre_sel = `${deudorBusca.Deu_Nombre} ${deudorBusca.Deu_Paterno} ${deudorBusca.Deu_Materno}`;
            }
            this.segBeneficiariosIdDeu = [];
            this.benIdDeu = idDeu;
            const beneficiarios = this.data.seg_beneficiarios.filter(row => row.Ben_IdDeu == idDeu);
            //this.segBeneficiariosIdDeu = [...beneficiarios];
            this.segBeneficiariosIdDeu = JSON.parse(JSON.stringify(beneficiarios));
            this.segBeneficiariosIdDeu.map((el, idx) => {
                if (idBen) {
                    if (el.Ben_Id == idBen) {
                        console.log('idx', idx);
                        this.bens[idx] = true;
                    }
                    else {
                        this.bens[idx] = false;
                    }
                }
                else {
                    this.bens[idx] = false;
                }
            });
            this.openModalBene = 'block';
        };
        this.getDatosEco = () => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.id_solicitud) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                    allowOutsideClick: false,
                    text: 'Actualizando datos ...'
                });
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
                const model = { idsol: this.id_solicitud };
                this.segSolicitudesService.updateServiciosData(model).subscribe(resp => {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                    const response = resp;
                    console.log('update-servicios-data:', response);
                    if (response.status === 'OK') {
                        this.msg.success(`Se actualizo con exito la solicitud nro.${this.data.seg_solicitudes.Sol_NumSol}, por favor vuelva a ingresar `);
                        //this.router.navigate(['/AppMain/pnetseg/seguros/seguro-add']);
                        this.router.navigate(['/seguros/seguro-add']);
                    }
                    else {
                        this.msg.error(`No se pudo actualizar ${response.message} `);
                    }
                });
            }
        });
        this.onSaveResolucion = () => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                title: ` Emitir Resolución  ?`,
                showCancelButton: true,
                confirmButtonText: `Aceptar`,
                cancelButtonText: `Cancelar`,
                confirmButtonColor: '#16A085',
                cancelButtonColor: '#F1948A',
            }).then((result) => {
                if (result.isConfirmed) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                        allowOutsideClick: false,
                        text: 'Guardando datos ...'
                    });
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
                    let aprobaciones = [];
                    let aprobado = false;
                    let rechazado = false;
                    let codeudor = false;
                    let extra_prima = false;
                    let condiciones_exclusiones = false;
                    const resoluciones = this.configParam.estado_solicitud_resolucion;
                    // persiste el la tabla SegAprobaciones
                    this.data.seg_deudores.forEach(deu => {
                        const idDeu = deu.Deu_Id;
                        const idSol = deu.Deu_IdSol;
                        const esCode = deu.Deu_NIvel;
                        (deu.aprobado ? aprobado = true : null);
                        (deu.rechazado ? rechazado = true : null);
                        const extra = deu.extra_prima;
                        const condi = deu.condiciones_exclusiones;
                        const causal = deu.causal;
                        //determinar estado
                        (esCode == 1 ? codeudor = true : null);
                        (extra ? extra_prima = true : null);
                        (condi ? condiciones_exclusiones = true : null);
                        //registro para guardar
                        let aprobacion = new _models_seg_aprobaciones__WEBPACK_IMPORTED_MODULE_21__["SegAprobaciones"]();
                        aprobacion.Apr_IdDeu = deu.Deu_Id;
                        aprobacion.Apr_IdSol = deu.Deu_IdSol;
                        if (aprobado) {
                            aprobacion.Apr_Aprobado = 'S';
                            aprobacion.Apr_ExtraPrima = extra;
                            aprobacion.Apr_Condiciones = condi;
                        }
                        else {
                            if (rechazado) {
                                aprobacion.Apr_Aprobado = 'N';
                                aprobacion.Apr_Condiciones = causal;
                                deu.Deu_Incluido = 'N';
                            }
                            ;
                        }
                        this.segAprobacionesService.add(aprobacion).subscribe(resp => {
                            const response = resp;
                            if (response.status == 'OK') {
                                this.data.seg_aprobaciones.push(aprobacion);
                            }
                        });
                    });
                    // determinar el estado
                    const estadoResol = resoluciones.find(row => row.aprobado == aprobado && row.rechazado == rechazado &&
                        row.codeudor == codeudor && row.extra_prima == extra_prima &&
                        row.condiciones_exclusiones == condiciones_exclusiones);
                    if (estadoResol) {
                        // guardar el estado de la solicitud
                        const estadoSol = this.configParam.estado_solicitud.find(row => row.codigo == estadoResol.estado);
                        this.estado_general = estadoSol.estado;
                        this.estado_codigo = estadoResol.estado;
                        this.data.seg_solicitudes.Sol_EstadoSol = estadoResol.estado;
                        let segSolicitudes = new _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_18__["SegSolicitudes"]();
                        segSolicitudes.Sol_IdSol = this.id_solicitud;
                        segSolicitudes.Sol_EstadoSol = estadoResol.estado;
                        this.setEstadoGeneral(estadoResol.estado);
                        this.guardaSegSolicitudes(segSolicitudes);
                        this.setFechaResolucion();
                        // crear la plantilla
                        let contenidoHtml = `
            <br><br>
            <table border="1">
              <tr><td rowspan="2" style="text-align:center;">CODEUDOR</td><td colspan="4" style="text-align:center;">RESULTADO DE LA EVALUACION</td></tr>
              <tr><td>APROBADO</td><td>EXTRAPRIMA</td><td>CONDICIONES Y EXCLUSIONES</td><td>CAUSAL RECHAZO</td></tr>
          `;
                        this.data.seg_deudores.forEach(deu => {
                            contenidoHtml += `<tr>
                <td>${deu.Deu_Nombre} ${deu.Deu_Paterno} ${deu.Deu_Materno}</td>
                <td>${deu.aprobado ? 'SI' : ''} ${deu.rechazado ? 'NO' : ''}</td>
                <td>${deu.extra_prima ? deu.extra_prima : '-'}</td>
                <td>${deu.condiciones_exclusiones ? deu.condiciones_exclusiones : '-'}</td>
                <td>${deu.causal ? deu.causal : ''}</td>
              </tr>`;
                        });
                        contenidoHtml += '</table>';
                        this.guardarObservaciones(estadoResol.estado);
                        this.onGuardarDatosEdit();
                        this.onSendMessage(estadoResol.estado, contenidoHtml);
                        this.msg.success('Operación se realizo con exito', 'RESOLUCION');
                    }
                    else {
                        this.msg.warning('No esta definido los estados de la resolucion', 'RESOLUCION');
                    }
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                }
            });
        });
        this.getTable = (content) => {
            const rows = content;
            let html = '<div class="w3-padding-16 w3-responsive"><table class="w3-table-all w3-tiny w3-hoverable">';
            html += '<thead><tr>';
            for (let campo in rows[0]) {
                html += '<th>' + campo + '</th>';
            }
            html += '</tr></thead><tbody>';
            for (let f = 0; f < rows.length; f++) {
                html += '<tr>';
                for (var j in rows[f]) {
                    const rowsObj = rows[f][j];
                    if (Array.isArray(rowsObj)) {
                        html += '<td> ' + this.getTable(rowsObj) + '</td>';
                    }
                    else {
                        html += '<td> ' + rowsObj + '</td>';
                    }
                }
            }
            html += '</tr></tbody></table></div>';
            return html;
        };
        this.controlCambios = [];
        this._parametrosCampos = [];
    }
    ngOnInit() {
        this.id_solicitud = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
        this.getDatosSolicitud();
        this.getParametrosCampos();
        try {
            this.userinfo = this.authService.getItemSync('userInfo');
            //console.log('USUARIO SESSION:',this.userinfo);
        }
        catch (error) {
            this.msg.warning('No se pudo recuperar la session del usuario', 'SOLICITUD');
        }
    }
    ngAfterViewInit() { }
    getParametrosCampos() {
        this.parametrosCampoService.getAll().subscribe(resp => {
            const response = resp;
            console.log('PARAMETROS CAMPOS:', resp);
            if (response.status == 'OK') {
                this._parametrosCampos = response.data;
            }
        });
    }
    getSolicitudSegundaEtapa(num_solicitud) {
        this.ecofuturoService.getSolicitudSegundaEtapa(num_solicitud).subscribe(resp => {
            const response = resp;
            if (response.status == 'OK') {
                const datosOperacion = response.data.getSolicitudSegundaEtapaResult;
                this.data.seg_operaciones = datosOperacion;
            }
        }, ex => {
            console.error('Error en comunicacion, Segunda Etapa : recuperando Servicio del Banco');
        });
    }
    ;
    getSolicitudPrimeraEtapa(num_solicitud) {
        this.ecofuturoService.getSolicitudPrimeraEtapa(num_solicitud).subscribe(resp => {
            const response = resp;
            if (response.status == 'OK') {
                this.data.seg_solicitudes.Sol_SolTipoSol = response.data.getSolicitudPrimeraEtapaResult.Operacion_Tipo_Solicitud;
            }
        }, ex => {
            console.error('Error en comunicacion, Primera Etapa : recuperando Servicio del Banco');
        });
    }
    ;
    setEstadoGeneral(estado) {
        // estado actual de la solicitud
        if (estado) {
            this.data.estado_general = this.configParam.estado_solicitud.find(row => row.codigo == estado).estado;
        }
        else {
            console.error('Config Param : Estados de la Solicitud Vacio');
        }
        ;
    }
    ;
    getDatosSolicitud() {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            allowOutsideClick: false,
            text: 'Recuperando Información  ...'
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
        // recuperamos datos, deudor, codeudor, solicitud
        if (!this.id_solicitud) {
            this.id_solicitud = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
        }
        this.segSolicitudesService.getSolicitudIdSolAll(this.id_solicitud).subscribe(resp => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
            const response = resp;
            if (response.status === 'OK') {
                // init folder
                this.data = response.data;
                this.dataOri = JSON.parse(JSON.stringify(response.data));
                this.configParam = this.data.config_param;
                this._estados_solicitud = this.data.config_param.estado_solicitud;
                //this.data.seg_solicitudes = response.data.seg_solicitudes;
                //this.data.seg_deudores = response.data.seg_deudores;
                //this.data.seg_beneficiarios = response.data.seg_beneficiarios;
                //this.data.seg_observaciones = response.data.seg_observaciones;
                //this.data.seg_adicionales = response.data.seg_adicionales;
                //this.data.seg_aprobaciones = response.data.seg_aprobaciones;
                // recortar fecha
                this.data.seg_solicitudes.Sol_FechaSol = this.data.seg_solicitudes.Sol_FechaSol.substring(0, 10);
                // recuperando titulos
                this.nro_solicitud = Number(this.data.seg_solicitudes.Sol_NumSol);
                this.tipo_cartera = this.data.seg_solicitudes.Sol_TipoSeg;
                this.estado_codigo = this.data.seg_solicitudes.Sol_EstadoSol;
                const estadoSolicitud = this.configParam.estado_solicitud.find(row => row.codigo == this.estado_codigo);
                if (estadoSolicitud) {
                    this.data.estado_general = estadoSolicitud.estado;
                }
                // verificar este campo si es nulo
                if (!this.data.seg_solicitudes.Sol_SolTipoSol) {
                    //this.getSolicitudPrimeraEtapa(this.nro_solicitud);
                }
                ;
                // SEG OPERACIONES
                if (response.data.seg_operaciones) {
                    this.data.seg_operaciones = response.data.seg_operaciones;
                }
                else {
                    this.getSolicitudSegundaEtapa(this.nro_solicitud);
                }
                ;
                //verificamos la poliza
                //if(!this.data.seg_solicitudes.Sol_Poliza){
                //  this.segSolicitudesService.getUpdatePoliza(this.data.seg_solicitudes.Sol_IdSol).subscribe(resp=>{
                //    const response = resp as { status: string; message:string ; messages: []; data: {} };
                //    if (response.status=='OK'){  
                //    }
                //  });
                //}
                // BENEFICIARIOS
                this.segBeneficiarios = [...this.data.seg_beneficiarios];
                // ADICIONALES - DJS
                // APROBACIONES
                this.segAprobaciones = [...this.data.seg_aprobaciones];
                // OBSERVACIONES
                this.segObservaciones = [...this.data.seg_observaciones];
                // CALCULO DE TASAS y Datos Completados 
                this.datosCompletados();
                this.sumaSaldoActualSolicitud();
                this.solicitudValid = (this.data.seg_solicitudes.Sol_EstadoSol != 'P' ? true : false);
                this.viewTable = this.getViewData(this.data.config_param);
                //ultima comunicacion
                if (['C1', 'C2'].includes(this.estado_codigo)) {
                    if (this.usuario_rol == 'of') {
                        this.segObservacionesUlt = this.data.seg_observaciones.filter(row => row.Obs_Eeff != '1');
                    }
                    else {
                        this.segObservacionesUlt = this.data.seg_observaciones.filter(row => row.Obs_Eeff == '1');
                    }
                }
                ;
                //es tarjeta
                if (this.data.seg_solicitudes.Sol_NumSol.toString().substring(0, 1) == '9') {
                    this.esTarjeta = true;
                }
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                this.msg.error(`Servicio principal : ${response.messages.toString()} `, 'Datos del Seguros');
            }
        }, ex => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
            this.msg.error('No se pudo establecer comunicación', 'SOLICITUDES');
        });
    }
    ;
    // Calculo de tasas y completados
    datosCompletados() {
        this.estado_codigo = this.data.seg_solicitudes.Sol_EstadoSol;
        // Tasa Mensual Ref.  | Prima Mensual Ref. var conDecimal = numero.toFixed(2);
        try {
            const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
            if (nro_poliza) {
                const monto_solicitado = this.data.seg_solicitudes.Sol_MontoSol;
                const cant_obligados = this.data.seg_deudores.length;
                const tasas = this.configParam.datos_poliza.find(row => row.nro_poliza == nro_poliza);
                if (tasas) {
                    const tasaTitular = tasas.tasa_anual_ref;
                    const tasaCodeudor = tasas.tasa_anual_ref_2;
                    const tasa_mensual = parseFloat(tasaTitular) + (parseFloat(tasaCodeudor) * (cant_obligados - 1));
                    const prima_mensual = monto_solicitado * (tasa_mensual / 1000);
                    //let tasa_anual = this.data.seg_solicitudes.Sol_TasaSeg?parseFloat(this.data.seg_solicitudes.Sol_TasaSeg.toString()):0.00;
                    //let monto_solicitado = this.data.seg_solicitudes.Sol_MontoSol?this.data.seg_solicitudes.Sol_MontoSol:0.00;
                    //const tasa_mensual = tasa_anual/12;
                    //const prima_mensual = monto_solicitado * (tasa_mensual / 100);
                    this.datosCalculados.tasa_mensual_ref = tasa_mensual;
                    this.datosCalculados.prima_mensual_ref = prima_mensual;
                }
            }
        }
        catch (error) {
            console.error(error);
        }
        // Entidad Financiera
        if (this.data.seg_solicitudes.Sol_Poliza) {
            const nro_poliza = this.data.seg_solicitudes.Sol_Poliza;
            const entidadPoliza = this.configParam.datos_poliza.find(row => row.nro_poliza == nro_poliza);
            if (entidadPoliza) {
                this.datosCalculados.entidad_seguros_1 = entidadPoliza.entidad_seguros;
            }
        }
        //obtener djs, valor para validar djs 
        const tipo_seguro = this.data.seg_solicitudes.Sol_TipoSeg; //NO LICITADA |  LICITADA
        const djsData = this.configParam.djs_salud.filter(row => row.tipo_seguro == tipo_seguro);
        this.data.seg_adicionales.forEach(row => {
            row.valida_por = djsData.find(el => el.nro == row.Adic_Pregunta).valida_por;
            if (row.Adic_Respuesta) {
                if (row.Adic_Respuesta == 'S||||||||||') {
                    row.valor_si = true;
                    row.valor_no = false;
                }
                if (row.Adic_Respuesta == '|S|||||||||') {
                    row.valor_si = false;
                    row.valor_no = true;
                }
            }
        });
        this.autorizacionesSolicitud = this.configParam.autorizaciones_solicitud;
    }
    sumaSaldoActualSolicitud() {
        try {
            // Saldo Actual Solicitado =  this.datosCalculados.saldo_actual_solicitado
            const monto_solicitado = this.data.seg_solicitudes.Sol_MontoSol;
            const moneda = this.data.seg_solicitudes.Sol_MonedaSol;
            const tipo_seguro = this.data.seg_solicitudes.Sol_TipoSeg; // NO LICITADA | LICITADA
            const nro_poliza = this.data.seg_solicitudes.Sol_Poliza;
            let cobertura_sol = '';
            this.data.seg_deudores.forEach((deudor) => {
                let saldo_actual_verificado = 0;
                let saldo_actual_solicitud = 0;
                try {
                    saldo_actual_verificado = parseFloat(deudor.Deu_MontoActAcumVerif);
                    saldo_actual_solicitud = parseFloat(monto_solicitado.toString()) + saldo_actual_verificado;
                    deudor['saldo_actual_solicitud'] = saldo_actual_solicitud;
                }
                catch (error) {
                    alert('Error en tipos de datos numericos');
                }
                // determinar cobertura
                if (tipo_seguro) {
                    //const cobertura = this.configParam.tipos_cobertura.find(row=> 
                    //saldo_actual_solicitud  >= row.monto_desde && saldo_actual_solicitud  <= row.monto_hasta
                    //&& row.moneda == moneda && row.nro_poliza == nro_poliza && row.tipo_seguro==tipo_seguro);
                    console.log(`DETERMINAR COBERTURA: 
              monto_solicitado : ${monto_solicitado}
              saldo_actual_verificado : ${saldo_actual_verificado} 
              saldo_actual_solicitud : ${saldo_actual_solicitud} 
              tipo seguro : ${tipo_seguro} 
              nro_poliza : ${nro_poliza} `);
                    const cobertura = this.configParam.tipos_cobertura.find(row => (monto_solicitado >= row.monto_desde && monto_solicitado <= row.monto_hasta)
                        && (saldo_actual_solicitud >= row.cumulo_desde && saldo_actual_solicitud <= row.cumulo_hasta)
                        && row.moneda == moneda && row.tipo_seguro == tipo_seguro);
                    if (cobertura) {
                        deudor.Deu_cobertura = cobertura.tipo_cobertura;
                        if (this.data.seg_solicitudes.Sol_TipoSeg == 'NO LICITADA') {
                            deudor.id_documento_version = (cobertura.tipo_cobertura == 'VG' ? 13 : 14);
                        }
                        ;
                        if (this.data.seg_solicitudes.Sol_TipoSeg == 'LICITADA') {
                            deudor.id_documento_version = 27;
                        }
                        ;
                        deudor.Deu_Poliza = cobertura.nro_poliza;
                    }
                    else {
                        this.msg.warning(`${deudor.Deu_Nombre} : No se pudo determinar la cobertura `);
                    }
                }
            });
        }
        catch (error) {
            console.error(error);
        }
    }
    objectsEqual(obj1, obj2) {
        //const campo = this._parametrosCampos.find(el=>el.campo=='Sol_SolTipoSol');
        //console.log(`El campo es ${campo.titulo} `);
        Object.keys(obj1).forEach(key => {
            //console.log(key,obj1[key]);
            if (obj1[key] !== obj2[key]) {
                const campo = this._parametrosCampos.find(el => el.campo == key);
                if (campo) {
                    this.controlCambios.push(`${campo.titulo}  ${obj1[key]} por ${obj2[key]} `);
                }
            }
        });
    }
    onGuardarDatosEdit() {
        //this.sumaSaldoActualSolicitud();
        this.controlCambios = [];
        //verificar los datos que se modificaron
        //1.seg_observaciones
        const obj1 = this.data.seg_solicitudes;
        const obj2 = this.dataOri.seg_solicitudes;
        this.objectsEqual(obj1, obj2);
        //2.seg_deudores
        const obj3 = this.data.seg_deudores;
        const obj4 = this.dataOri.seg_deudores;
        obj3.forEach((obj, index) => {
            console.log('DEUDORES obj3:', obj);
            console.log('DEUDORES obj4:', obj4[index]);
            this.objectsEqual(obj, obj4[index]);
        });
        //3.seg_benficiarios
        const obj5 = this.data.seg_beneficiarios;
        const obj6 = this.dataOri.seg_beneficiarios;
        obj5.forEach((obj, index) => {
            this.objectsEqual(obj, obj6[index]);
        });
        if (this.controlCambios.length > 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                confirmButtonText: 'siguiente',
                showCancelButton: true,
                progressSteps: ['1', '2', '3']
            }).queue([
                {
                    title: '',
                    text: '',
                    html: `Control de cambios : <br> ${this.controlCambios.join('<br>')} `
                },
                {
                    title: '',
                    text: 'Ingrese los datos y el motivo de la actualización',
                    input: 'textarea',
                    inputValue: ''
                },
                {
                    title: '',
                    text: 'Ingrese el mensaje que enviara a los usuarios involucrados',
                    input: 'textarea',
                    inputValue: ''
                },
                {
                    title: 'Confirmar actualización',
                    confirmButtonText: 'SI',
                    cancelButtonText: 'NO',
                }
            ]).then((resp) => {
                if (resp.dismiss) {
                }
                else {
                    const _motivos = resp;
                    if (_motivos.value[3]) {
                        console.log('CONTROL DE CAMBIOS:', this.controlCambios);
                        const cambios = this.controlCambios.join(',');
                        const motivo = `${cambios}, ${_motivos.value[1]}`;
                        const mensaje = _motivos.value[2];
                        const dataSol = {
                            observaciones: { motivo, mensaje },
                            data: this.data,
                            usuario: this.userinfo.usuario_login
                        };
                        this.segSolicitudesService.updateSolicitudDeudores(dataSol).subscribe(resp => {
                            console.log('[Soporte] RESPONSE updateSolicitudDeudores:', resp);
                            const response = resp;
                            if (response.status == 'OK') {
                                this.msg.success(`Se actualizo con exito : ${response.message}`);
                                this.dataOri = JSON.parse(JSON.stringify(this.data));
                                this.data.seg_observaciones.push(response.data);
                                this.controlCambios = [];
                                //suma saldos
                                this.sumaSaldoActualSolicitud();
                                // envia correo
                                this.enviarCorreoControlCambios(motivo, mensaje, this.userinfo.usuario_login, this.data.seg_solicitudes.Sol_NumSol);
                            }
                            else {
                                this.msg.error(response.message, 'ERROR');
                            }
                        });
                    }
                }
            });
        }
        else {
            this.msg.warning('No hay cambios');
        }
    }
    ;
    enviarCorreoControlCambios(motivo, mensaje, usuario, nro_sol) {
        try {
            // envio de correo
            // jchiara@kerkuscorredores.com.bo, ymarzana@kerkuscorredores.com.bo,rvictoria@kerkuscorredores.com.bo
            let message = new _models_message_email__WEBPACK_IMPORTED_MODULE_26__["MessageEmail"]();
            message.to = 'jchiara@kerkuscorredores.com.bo, ymarzana@kerkuscorredores.com.bo,rvictoria@kerkuscorredores.com.bo';
            message.subject = `CONTROL DE CAMBIOS Nro.Solicutde ${nro_sol} `;
            message.html = `Se procedio a realizar los siquietes cambios : ${motivo}, ${mensaje} `;
            this.sendMailService.sendMessage(message).subscribe(resp => {
                const response = resp;
                console.log('envio de mensajes', response);
            });
        }
        catch (error) {
            console.log(`ERROR al enviar correo, ${error} `);
        }
    }
    onDesistir() {
        if (this.desistimiento.causal && this.desistimiento.autorizado_por) {
            const model = {
                causal: this.desistimiento.causal,
                autorizado_por: this.desistimiento.autorizado_por,
                idsol: this.data.seg_solicitudes.Sol_IdSol,
                usuario: `${this.authService.usuario.usuario_nombre_completo} `
            };
            this.segSolicitudesService.desistimientoSolicitud(model).subscribe(resp => {
                const response = resp;
                if (response.status == 'OK') {
                    this.msg.success('La Solicitud Desistio con exito', 'SOLICITUD');
                    ///seguros/seguro-add
                    this.router.navigate(['/seguros/seguro-add']);
                }
                else {
                    this.msg.error('La Solicitud no se pudo Desistir', 'SOLICITUD');
                }
            });
        }
        else {
            this.msg.error('Causal y Autorizado por , requeridos', 'Desistimiento');
        }
    }
    ;
    onEliminarSolicitud() {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: `¿ESTÁ USTED SEGURO DE ELIMINAR LA SOLICITUD ${this.data.seg_solicitudes.Sol_NumSol} ?`,
            showCancelButton: true,
            confirmButtonText: `Eliminar`,
            cancelButtonText: `Cancelar`,
            confirmButtonColor: '#16A085',
            cancelButtonColor: '#F1948A',
        }).then((result) => {
            if (result.isConfirmed) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                    allowOutsideClick: false,
                    text: 'Eliminando la Solicitud ...'
                });
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
                const id = this.data.seg_solicitudes.Sol_IdSol;
                this.segSolicitudesService.delete(id).subscribe(resp => {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                    const response = resp;
                    if (response.status == 'OK') {
                        this.msg.info('La Solicitud se borro con exito', 'SOLICITUD');
                        ///seguros/seguro-add
                        this.router.navigate(['/AppMain/pnetseg/seguros/seguro-add']);
                    }
                    else {
                        this.msg.error('La Solicitud no se pudo borrar', 'SOLICITUD');
                    }
                });
            }
        });
    }
    // funcion DJS cuestionario
    onAceptarCuestionario() {
        let aceptar = true;
        let messages = [];
        for (const row of this.declaraciones) {
            if (row.valida_por) {
                if (row.valor_si) {
                    row.Adic_Respuesta = 'S||||||||||';
                    row.Adic_Comment = '';
                }
                else {
                    if (row.valor_no) {
                        if (row.Adic_Comment) {
                            row.Adic_Respuesta = '|S|||||||||';
                        }
                        else {
                            messages.push(`Pregunta ${row.Adic_Pregunta}, requiere Aclaración`);
                        }
                    }
                    else {
                        messages.push(`Pregunta ${row.Adic_Pregunta}, requiere SI o NO`);
                    }
                }
            }
            else {
                if (row.valor_no) {
                    row.Adic_Respuesta = '|S|||||||||';
                    row.Adic_Comment = '';
                }
                else {
                    if (row.valor_si) {
                        if (row.Adic_Comment) {
                            row.Adic_Respuesta = 'S||||||||||';
                        }
                        else {
                            messages.push(`Pregunta ${row.Adic_Pregunta}, requiere Aclaración`);
                        }
                    }
                    else {
                        messages.push(`Pregunta ${row.Adic_Pregunta}, requiere SI o NO`);
                    }
                }
            }
        }
        ;
        if (messages.length > 0) {
            this.msg.error(messages.toString(), 'Cuestionario');
        }
        else {
            console.log('Declaraciones', this.declaraciones);
            // guardar adicionales
            this.segAdicionalesService.updateAdiconales(this.declaraciones).subscribe(resp => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                console.log('Response Update declaraciones:', resp);
                const response = resp;
                if (response.status == 'OK') {
                    this.msg.success('Se Guardo con exito', 'DJS');
                    /*
                    for(const row of this.declaraciones){
                      const djsObj = this.data.seg_adicionales.find(el=>el.Adic_Id == row.Adic_Id);
                      
                      djsObj.Adic_Respuesta = row.Adic_Respuesta;
                      djsObj.Adic_Texto = row.Adic_Texto;
                      console.log(djsObj);
                    }
                    */
                }
                else {
                    this.msg.error(response.messages.toString(), 'DJS');
                }
            });
            this.openModalCuestion = 'none';
        }
        ;
    }
    onAclaracion(preg) {
        let esComentario = false;
        //if(this.estado_codigo == 'P' && (preg.valida_por != preg.valor_si) ){
        if ((preg.valida_por != preg.valor_si)) {
            esComentario = true;
        }
        return esComentario;
    }
    getEif(eif) {
        let eif_texto = "";
        if (eif == '1') {
            eif_texto = "Banco PyME Ecofuturo S.A.";
        }
        return eif_texto;
    }
    onChangeInputDeu(valor, campo) {
        //const segDeudor = this.folderSolicitud.seg_deudores.filter((row) => row.Deu_NIvel == 0)[0];
        const segDeudor = this.segDeudores.filter(row => row.Deu_Id == this.id_deudor)[0];
        if (segDeudor[campo]) {
            if (segDeudor[campo] != valor) {
                this.offGuardarDeudor = false;
            }
            else {
                this.offGuardarDeudor = true;
            }
        }
        else {
            if (valor) {
                this.offGuardarDeudor = false;
            }
            else {
                this.offGuardarDeudor = true;
            }
        }
    }
    ;
    onChangeInputCoDeu(valor, campo, origen) {
        //const segDeudor = this.folderSolicitud.seg_deudores.filter((row) => row.Deu_Id == origen.Deu_Id)[0];
        const segDeudor = this.segDeudores.filter(row => row.Deu_Id == origen.Deu_Id)[0];
        if (segDeudor[campo]) {
            if (segDeudor[campo] != valor) {
                this.offGuardarCoDeudor = false;
            }
            else {
                this.offGuardarCoDeudor = true;
            }
        }
        else {
            if (valor) {
                this.offGuardarCoDeudor = false;
            }
            else {
                this.offGuardarCoDeudor = true;
            }
        }
    }
    ;
    dosPasos() {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
            confirmButtonText: 'siguiente',
            confirmButtonColor: '#16A085',
            showCancelButton: true,
            cancelButtonColor: '#F1948A',
            progressSteps: ['1', '2']
        }).queue([
            {
                title: 'Advertencia!',
                text: 'MARIANO ROJAS, excedio los límites de asegurabilidad permitidos, por lo que este será excluido, para fines de la suscripción.'
            },
            'Observaciones'
        ]).then((result) => {
            if (result.value) {
                console.log(result.value);
            }
        });
    }
    aprobacionAutomatica(estado_evaluado) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            icon: 'question',
            html: `LA SOLICITUD SE APROBARA AUTOMATICAMENTE ?`,
            showCancelButton: true,
            confirmButtonText: `Aceptar`,
            cancelButtonText: `Cancelar`,
            confirmButtonColor: '#16A085',
            cancelButtonColor: '#F1948A',
        }).then((result) => {
            if (result.isConfirmed) {
                this.msg.success('Solicitud Aprobada', 'SOLICITUD EN PROCESO');
                // cambiar estado a la solicitud
                this.data.seg_solicitudes.Sol_EstadoSol = estado_evaluado;
                let segSolicitudes = new _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_18__["SegSolicitudes"]();
                segSolicitudes.Sol_IdSol = this.data.seg_solicitudes.Sol_IdSol;
                segSolicitudes.Sol_EstadoSol = estado_evaluado;
                this.estado_codigo = estado_evaluado;
                this.data.seg_solicitudes.Sol_EstadoSol = estado_evaluado;
                // Guardar fecha de aprobacion automatica Deu_FechaResolucion
                this.setFechaResolucion();
                this.guardaSegSolicitudes(segSolicitudes);
                this.setEstadoGeneral(estado_evaluado);
                this.guardarObservaciones(estado_evaluado);
                //this.onSendMessage(estado_evaluado);
                this.onSendMessage_A();
            }
        });
    }
    evaluacionPendiente(estado_evaluado, msg) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            icon: 'question',
            html: `SOLICITUD SERA ENVIADA PREVIAMENTE A LA COMPAÑIA Y DEBE SER APROBADA POR ELLA`,
            showCancelButton: true,
            confirmButtonText: `Aceptar`,
            cancelButtonText: `Cancelar`,
            confirmButtonColor: '#16A085',
            cancelButtonColor: '#F1948A',
        }).then((result) => {
            if (result.isConfirmed) {
                this.msg.warning('Solicitud en evaluación', 'SOLICITUD EN PROCESO');
                // cambiar estado a la solicitud
                this.data.seg_solicitudes.Sol_EstadoSol = estado_evaluado;
                let segSolicitudes = new _models_seg_solicitudes__WEBPACK_IMPORTED_MODULE_18__["SegSolicitudes"]();
                segSolicitudes.Sol_IdSol = this.data.seg_solicitudes.Sol_IdSol;
                segSolicitudes.Sol_EstadoSol = estado_evaluado;
                this.estado_codigo = estado_evaluado;
                this.setEstadoGeneral(estado_evaluado);
                this.guardaSegSolicitudes(segSolicitudes);
                this.guardarObservaciones(estado_evaluado, msg);
                //this.onSendMessage(estado_evaluado,msg);
                this.onSendMessage_C1_C2(estado_evaluado, msg);
            }
        });
    }
    ;
    // Deu_FechaResolucion
    setFechaResolucion() {
        const fechaActual = getDateYYYYmmDD();
        let deudores = [];
        this.data.seg_deudores.forEach(row => {
            let deudorObj = {};
            row.Deu_FechaResolucion = fechaActual;
            deudorObj['Deu_Id'] = row.Deu_Id;
            deudorObj['Deu_FechaResolucion'] = fechaActual;
            deudores.push(deudorObj);
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            allowOutsideClick: false,
            text: 'Registrando fecha de resolución  ...'
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
        this.segDeudoresService.updateDeudores(deudores).subscribe(resp => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
            const response = resp;
            if (response.status == 'OK') {
                this.msg.success('Se registro la fecha de resolución con exito ');
            }
            else {
                this.msg.error('No se pudo registrar la fecha de resolución ');
            }
            console.log('Respuesta la guardar deudores:', response);
        });
    }
    onSendMessage_A() {
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        let messageHtml = '';
        const estado_evaluado = 'A';
        if (nro_poliza) {
            this.data.seg_deudores.forEach(row => {
                if (row.Deu_Incluido == 'S') {
                    messageHtml += `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Deudor ${row.Deu_Nombre} ${row.Deu_Paterno} ${row.Deu_Materno} Aprobada Automáticamente <br>`;
                }
            });
            messageHtml += `Enviado por: ${this.data.seg_solicitudes.Sol_Oficial} `;
            //const msgObj = this.configParam.notificaciones_mensajes.find(row=>
            //  row.estado_evaluado == 'A' && row.nro_poliza.split(',').includes(nro_poliza)
            //);
            const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                && row.nro_poliza.split(',').includes(nro_poliza));
            if (correosEstados) {
                //[year, month, day].join('-');
                // envio de correo
                let message = new _models_message_email__WEBPACK_IMPORTED_MODULE_26__["MessageEmail"]();
                message.to = correosEstados.emails;
                message.cc = correosEstados.cc;
                message.subject = `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} `;
                message.html = messageHtml;
                this.sendMailService.sendMessage(message).subscribe(resp => {
                    const response = resp;
                    console.log('envio de mensajes', response);
                });
            }
            else {
                this.msg.warning('No estan definidos las notificaciones');
            }
        }
        ;
    }
    onSendMessage_C1_C2(estado_evaluado, msg) {
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        let messageHtml = '';
        if (nro_poliza) {
            //const msgObj = this.configParam.notificaciones_mensajes.find(row=>
            //  row.estado_evaluado == estado_evaluado && row.nro_poliza.split(',').includes(nro_poliza)
            //);
            const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                && row.nro_poliza.split(',').includes(nro_poliza));
            if (correosEstados) {
                messageHtml += `${this.data.seg_solicitudes.Sol_Oficial} , Solicita su atencion a la Solicitud ${this.data.seg_solicitudes.Sol_NumSol}
        de la Endidad Financiera Banco PyME Ecofuturo S.A. <br> 
        RAZON DE LA SOLICITUD: <br>  ${msg}  <br>`;
                messageHtml += `Enviado por: ${this.data.seg_solicitudes.Sol_Oficial} `;
                // envio de correo
                let message = new _models_message_email__WEBPACK_IMPORTED_MODULE_26__["MessageEmail"]();
                message.to = correosEstados.emails;
                message.cc = correosEstados.cc;
                message.subject = `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} - Banco PyME Ecofuturo S.A. - ${this.data.seg_solicitudes.Sol_Oficial} `;
                message.html = messageHtml;
                this.sendMailService.sendMessage(message).subscribe(resp => {
                    const response = resp;
                    console.log('envio de mensajes', response);
                });
            }
            else {
                this.msg.warning('No estan definidos las notificaciones');
            }
        }
    }
    onSendMessage(estado_evaluado, msg) {
        console.log('INICIANDO ENVIO DE CORREO');
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        let messageHtml = '';
        if (nro_poliza) {
            const msgObj = this.configParam.notificaciones_mensajes.find(row => row.estado_evaluado == estado_evaluado && row.nro_poliza.split(',').includes(nro_poliza));
            const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                && row.nro_poliza.split(',').includes(nro_poliza));
            if (correosEstados) {
                // envio de correo
                let message = new _models_message_email__WEBPACK_IMPORTED_MODULE_26__["MessageEmail"]();
                message.to = correosEstados.emails;
                message.cc = correosEstados.cc;
                message.subject = `Pronunciamiento Compañia sobre la Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A.`;
                message.html = `Pronunciamiento Compañia sobre Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A. <br>
         ${msgObj.asunto} <br>
         Enviado por ${this.userinfo.usuario_nombre_completo}`;
                this.sendMailService.sendMessage(message).subscribe(resp => {
                    const response = resp;
                    console.log('envio de mensajes', response);
                });
            }
            else {
                this.msg.warning('No estan definidos las notificaciones');
            }
        }
        ;
    }
    ;
    getEifAseguradora(cod) {
        // Banco PyME Ecofuturo S.A. = 1 |  Asegurador Alianza = 101 
        // Asegurador La Boliviana Ciacruz = 20001,20002,20006,20007
        //const asegurador = this.configParam.datos_poliza.find(row=>row.nro_poliza==nro_poliza).entidad_seguros;
        let asegurador;
        if (cod) {
            if (cod == '1') {
                asegurador = 'Banco PyME Ecofuturo S.A.';
            }
            ;
            if (cod == '101') {
                asegurador = 'Asegurador Alianza';
            }
            ;
            const datosPoliza = this.configParam.datos_poliza.find(row => row.nro_poliza == cod);
            if (datosPoliza) {
                asegurador = datosPoliza.entidad_seguros;
            }
        }
        return asegurador;
    }
    guardarObservaciones(estado_evaluado, msg) {
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        if (nro_poliza) {
            const msgObj = this.configParam.notificaciones_mensajes.find(row => row.estado_evaluado == estado_evaluado && row.nro_poliza.split(',').includes(nro_poliza));
            if (msgObj) {
                let obser = new _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_22__["SegObservaciones"]();
                obser.Obs_IdSol = this.data.seg_solicitudes.Sol_IdSol;
                obser.Obs_Usuario = this.userinfo.usuario_nombre_completo;
                obser.Obs_Observacion = (msg ? `${msgObj.mensaje_historial} ${msg} ` : `${msgObj.mensaje_historial} `);
                if (this.usuario_rol == 'of') {
                    obser.Obs_Eeff = '1';
                    obser.Obs_Aseg = '';
                }
                else {
                    obser.Obs_Eeff = '';
                    obser.Obs_Aseg = nro_poliza;
                }
                ;
                this.segObservacionesService.add(obser).subscribe(resp => {
                    const response = resp;
                    if (response.status == 'OK') {
                        this.data.seg_observaciones.push(response.data);
                    }
                });
            }
            else {
                this.msg.warning('No estan definidos las notificaciones');
            }
        }
    }
    ;
    guardaSegSolicitudes(model) {
        this.segSolicitudesService.update(model).subscribe(resp => {
            const response = resp;
            if (response.status == 'OK') {
                this.msg.success('Se actualizo la solicitud', 'SOLICITUD');
            }
            else {
                this.msg.error('No se pudo guardar datos de la Solicitud', 'SOLICITUD');
            }
        });
    }
    ;
    getSegObservaciones(idSol) {
        this.segObservacionesService.getIdSol(idSol).subscribe(resp => {
            const response = resp;
            if (response.status == 'OK') {
                this.segObservaciones = response.data;
            }
        });
    }
    ;
    onCloseBuscarCodeudor(result) {
        this.openModalCodeu = 'none';
        this.param_buscar.numero_documento = '';
        this.param_buscar.accion = 'add-codeudor';
        this.param_buscar.id_folder = this.id_folder;
        if (result) {
            //this.getDatosFolder();
        }
    }
    addBeneficiario() {
        const newBen = new _models_seg_beneficiarios__WEBPACK_IMPORTED_MODULE_19__["SegBeneficiarios"]();
        newBen.Ben_IdSol = this.data.seg_solicitudes.Sol_IdSol;
        newBen.Ben_IdDeu = this.id_deudor_Selec;
        newBen.Ben_Porcentaje = "100";
        this.segBeneficiariosIdDeu.push(newBen);
        const len = this.segBeneficiariosIdDeu.length;
        for (let i = 0; i < len; i++) {
            this.bens[i] = false;
        }
        this.bens[len - 1] = true;
    }
    ;
    onBeneficiariosValidarCerrar() {
        let valido = true;
        let obser = [];
        let porcentaje_p = 0;
        let porcentaje_c = 0;
        let nro_p = 0;
        let nro_c = 0;
        // validacion del beneficiarios
        this.segBeneficiariosIdDeu.forEach(ben => {
            if (ben.Ben_Tipo == 'PRIMARIO') {
                const num = parseInt(ben.Ben_Porcentaje);
                if (num == 0) {
                    obser.push('Porcentaje no puede ser cero');
                }
                porcentaje_p += num;
                nro_p++;
            }
            if (ben.Ben_Tipo == 'CONTINGENTE') {
                const num = parseInt(ben.Ben_Porcentaje);
                if (num == 0) {
                    obser.push('Porcentaje no puede ser cero');
                }
                porcentaje_c += num;
                nro_c++;
            }
            if (!ben.Ben_Nombre) {
                obser.push('Nombre completo es requerido');
            }
            ;
            if (!ben.Ben_NumDoc) {
                obser.push('Numero de documento es requerido');
            }
            ;
            if (!ben.Ben_TipoDoc) {
                obser.push('Tipo de documento es requerido');
            }
            ;
            if (!ben.Ben_Relacion) {
                obser.push('Relación es requerido');
            }
            ;
            if (!ben.Ben_Porcentaje) {
                obser.push('Porcentaje es requerido');
            }
            ;
            if (!ben.Ben_Tipo) {
                obser.push('Tipo de Beneficiario es requerido');
            }
            ;
            if (!ben.Ben_Paterno) {
                if (!ben.Ben_Materno) {
                    obser.push('Por la menos un Apellido es requerido');
                }
            }
        });
        if (nro_p > 0) {
            if (porcentaje_p != 100) {
                obser.push(`Porcentaje PRIMARIA es ${porcentaje_p} debe ser 100%`);
            }
        }
        if (nro_c > 0) {
            if (porcentaje_c != 100) {
                obser.push(`Porcentaje CONTINGENTE es ${porcentaje_c} debe ser 100%`);
            }
        }
        if (obser.length > 0) {
            valido = false;
        }
        if (valido) {
            this.openModalBene = 'none';
        }
        else {
            this.msg.error(obser.toString(), 'Beneficiarios');
        }
    }
    updateBeneficiarios() {
        let valido = true;
        let obser = [];
        let porcentaje_p = 0;
        let porcentaje_c = 0;
        let nro_p = 0;
        let nro_c = 0;
        // validacion del beneficiarios
        this.segBeneficiariosIdDeu.forEach(ben => {
            if (ben.Ben_Tipo == 'PRIMARIO') {
                const num = parseInt(ben.Ben_Porcentaje);
                if (num == 0) {
                    obser.push('Porcentaje no puede ser cero');
                }
                porcentaje_p += num;
                nro_p++;
            }
            if (ben.Ben_Tipo == 'CONTINGENTE') {
                const num = parseInt(ben.Ben_Porcentaje);
                if (num == 0) {
                    obser.push('Porcentaje no puede ser cero');
                }
                porcentaje_c += num;
                nro_c++;
            }
            if (!ben.Ben_Nombre) {
                obser.push('Nombre completo es requerido');
            }
            ;
            if (!ben.Ben_Relacion) {
                obser.push('Relación es requerido');
            }
            ;
            if (!ben.Ben_Porcentaje) {
                obser.push('Porcentaje es requerido');
            }
            ;
            if (!ben.Ben_Tipo) {
                obser.push('Tipo de Beneficiario es requerido');
            }
            else {
                if (ben.Ben_Tipo == 'PRIMARIO') {
                    if (!ben.Ben_TipoDoc) {
                        obser.push('Tipo de documento es requerido');
                    }
                    ;
                    if (!ben.Ben_NumDoc) {
                        obser.push('Numero de documento es requerido');
                    }
                    ;
                }
            }
            ;
            if (!ben.Ben_Paterno) {
                if (!ben.Ben_Materno) {
                    obser.push('Por la menos un Apellido es requerido');
                }
            }
        });
        if (nro_p > 0) {
            if (porcentaje_p != 100) {
                obser.push(`Porcentaje PRIMARIO es ${porcentaje_p} debe ser 100%`);
            }
        }
        if (nro_c > 0) {
            if (porcentaje_c != 100) {
                obser.push(`Porcentaje CONTINGENTE es ${porcentaje_c} debe ser 100%`);
            }
        }
        if (obser.length > 0) {
            valido = false;
        }
        if (valido) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                allowOutsideClick: false,
                text: 'Guardando datos de Beneficiarios ...'
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
            const idSol = this.data.seg_solicitudes.Sol_IdSol;
            this.segBeneficiariosService.updateAll(this.segBeneficiariosIdDeu, idSol).subscribe(resp => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                const response = resp;
                console.log('updateAll:', response);
                if (response.status == 'OK') {
                    this.openModalBene = 'none';
                    this.msg.success('Beneficiarios se guardaron con exito');
                    this.data.seg_beneficiarios = response.data;
                }
                else {
                    this.msg.error(`Error guardando beneficiarios ${response.messages}`);
                }
            }, ex => { this.msg.error('No se pudo establecer comunicacion', 'Beneficiarios'); });
        }
        else {
            this.msg.error(obser.toString(), 'Beneficiarios');
        }
    }
    ;
    onDelBeneficiario(idBen, index) {
        console.log('onDelBeneficiario:', idBen, index);
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: 'No Incluir Beneficiario',
            showCancelButton: true,
            confirmButtonText: `Aceptar`,
            cancelButtonText: `Cancelar`,
            confirmButtonColor: '#16A085',
            cancelButtonColor: '#F1948A',
        }).then((result) => {
            if (result.isConfirmed) {
                if (idBen) {
                    //borrar en el servicios
                    this.segBeneficiariosService.delete(idBen).subscribe(resp => {
                        const response = resp;
                        if (response.status == 'OK') {
                            // borra filtro y desenfoca la seleccion
                            this.segBeneficiariosIdDeu.map((el, idx) => {
                                if (el.Ben_Id == idBen) {
                                    this.segBeneficiariosIdDeu.splice(index, 1);
                                }
                                this.bens[idx] = false;
                                // actualiza data
                                let benList = this.data.seg_beneficiarios;
                                for (var i = 0; i < benList.length; i++) {
                                    if (benList[i].Ben_Id === idBen) {
                                        benList.splice(i, 1);
                                    }
                                }
                            });
                            this.msg.success('Beneficiario fue excluido con exito', 'BENEFICIARIOS');
                        }
                        else {
                            this.msg.error('Beneficiario no pudo ser excluido con exito', 'BENEFICIARIOS');
                        }
                    });
                }
                else {
                    this.segBeneficiariosIdDeu.map((el, idx) => {
                        this.segBeneficiariosIdDeu.splice(index, 1);
                        this.bens[idx] = false;
                    });
                }
            }
        });
    }
    getDeudor(id, campo) {
        const ben = this.data.seg_deudores.find(el => el.Deu_Id == id);
        if (ben) {
            if (campo == 'nombres') {
                return `${ben.Deu_Nombre} ${ben.Deu_Paterno} ${ben.Deu_Materno}`;
            }
            if (campo == 'tipo') {
                return (ben.Deu_NIvel == 0 ? 'DEUDOR(A)' : 'CODEUDOR(A)');
            }
        }
    }
    ;
    onImprimirV2(iddeu, iddoc) {
        const param = {
            id_deudor: iddeu
        };
        this.msg.info('Generando los reportes para el Codeudor, favor espere...');
        this.isLoading = true;
        this.reporteService.generarReporteV2(param).subscribe(resp => {
            const response = resp;
            console.log('RESPONSE REPORTE:', resp);
            this.isLoading = false;
            if (resp) {
                // PUBLICAR REPORTE 
                //const urlPdf = environment.URL_PUBLIC_PDF_MASIVOS;
                //const rutaPdf = urlPdf+`/${response.data.base}`;
                //window.open(rutaPdf,'_blank');
            }
            else {
                this.msg.warning('No se pudo generar el reporte', 'REPORTE');
            }
        });
    }
    onImprimirDoc() {
        const tipo_seguro = this.data.seg_solicitudes.Sol_TipoSeg; // NO LICITADA | LICITADA
        let tipo_cobertura = this.data.seg_solicitudes.Sol_TipoParam; // VG | DH
        if (!tipo_cobertura && tipo_seguro == 'LICITADA') {
            tipo_cobertura = 'DH';
        }
        // obtener plantilla de reportes, this.configParam  
        const pts = this.configParam.impresion_documentos.filter(row => row.tipo_seguro == tipo_seguro && row.tipo_cobertura == tipo_cobertura);
        this.openModalImprimir = 'block';
    }
    loadDocumentosDeudor() {
        (this.showDocs_1 == true ? this.showDocs_1 = false : this.showDocs_1 = true);
    }
    loadDocumentosCoDeudor() {
        (this.showDocs_2 == true ? this.showDocs_2 = false : this.showDocs_2 = true);
    }
    onViewHistorial() {
        this.openModalHistorial = 'block';
    }
    onDatosEco() {
        const estado_evaluado = 'C1';
        const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
        const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
            && row.nro_poliza.split(',').includes(nro_poliza));
        console.log('CORREOS:', correosEstados);
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: ' Desea actualizar la información de la Solicitud desde el sistema Central del Banco ?',
            showCancelButton: true,
            confirmButtonText: `SI`,
            cancelButtonText: `NO`,
            confirmButtonColor: '#009688',
            cancelButtonColor: '#f44336',
        }).then((result) => {
            if (result.isConfirmed) {
                this.getDatosEco();
            }
        });
    }
    onChangeUpload(event) {
        this.file = event.target.files[0];
        this.formData.append('file', event.target.files[0], event.target.files[0].name);
    }
    ;
    onResponderObser() {
        if (this.respuestaObser) {
            const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
            const estado_evaluado = this.data.seg_solicitudes.Sol_EstadoSol;
            // Observaciones
            let model = new _models_seg_observaciones__WEBPACK_IMPORTED_MODULE_22__["SegObservaciones"]();
            model.Obs_IdSol = this.id_solicitud;
            model.Obs_Observacion = this.respuestaObser;
            if (this.usuario_rol == 'of') {
                model.Obs_Eeff = '1';
                model.Obs_Aseg = '';
            }
            else {
                model.Obs_Eeff = '';
                model.Obs_Aseg = nro_poliza;
            }
            ;
            model.Obs_Usuario = `${this.userinfo.usuario_nombre_completo} `;
            // registro en las observaciones
            this.segObservacionesService.add(model).subscribe(resp => {
                const response = resp;
                if (response.status == 'OK') {
                    //this.getSegObservaciones(this.id_solicitud);
                    this.data.seg_observaciones.push(response.data);
                    this.respuestaObser = '';
                    this.msg.info('Se registro y envio con exito', 'Ultima Comunicación');
                }
                else {
                    this.msg.error('No se pudo enviar ni ni registrar', 'Ultima Comunicación');
                }
            });
            // ENVIO DE MENSAJE DE CORREO
            console.log(`usuario rol [${this.usuario_rol}]  `);
            // enviar oficial => compañia
            let messageHtml = '';
            // oficial
            if (this.usuario_rol == 'of') {
                if (this.file) {
                    const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                        && row.nro_poliza.split(',').includes(nro_poliza));
                    if (correosEstados) {
                        let mensajeObj = {
                            subject: `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A.`,
                            message: `${this.data.seg_solicitudes.Sol_Oficial}, Solicita su atención : <br><br> ${this.respuestaObser}`,
                            to: correosEstados.emails,
                            cc: correosEstados.cc,
                            file: this.file
                        };
                        this.fileUploadService.upload(mensajeObj).subscribe(resp => {
                            console.log('UPLOAD FILE - SEND MAIL : ', resp);
                        });
                    }
                    else {
                        this.msg.warning('No estan definidos las notificaciones');
                    }
                }
            }
            else {
                if (this.file) {
                    const correosEstados = this.configParam.correos_estados.find(row => row.estados_evaluados.split(',').includes(estado_evaluado)
                        && row.nro_poliza.split(',').includes(nro_poliza));
                    if (correosEstados) {
                        let mensajeObj = {
                            subject: `Atención de la Compañia sobre la Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A.`,
                            message: `La Compañia solicita su atención :  <br><br> ${this.respuestaObser}`,
                            to: correosEstados.emails,
                            cc: correosEstados.cc,
                            file: this.file
                        };
                        this.fileUploadService.upload(mensajeObj).subscribe(resp => {
                            console.log('UPLOAD FILE - SEND MAIL : ', resp);
                        });
                    }
                    else {
                        this.msg.warning('No estan definidos las notificaciones');
                    }
                }
            }
        }
        else {
            this.msg.warning('Respuesta requerida', 'Ult.Comunicacion');
        }
    }
    ;
    guardarDeudores() {
        //this.codeudores_def
        let deudores = [];
        this.codeudores_def.forEach(deudor_def => {
            let deudor = {};
            deudor_def.forEach((elem) => {
                //if( elem.editable == true){
                if (elem.campo != " ") {
                    deudor[elem.campo] = elem.valor;
                }
                //}
            });
            deudores.push(deudor);
        });
        this.segDeudoresService.updateDeudores(deudores).subscribe(resp => {
            const response = resp;
            console.log('Respuesta la guardar deudores al validar:', response);
        });
    }
    ;
    getViewData(dataObj) {
        let html1 = '';
        const data1 = Object.keys(dataObj);
        data1.forEach((campo, i) => {
            html1 += `<b class='w3-tiny w3-text-teal'>${campo}</b><br>`;
            const valor = dataObj[campo];
            if (Array.isArray(valor)) {
                html1 += this.getTable(valor);
            }
        });
        return html1;
    }
}
SoporteSolicitudComponent.ɵfac = function SoporteSolicitudComponent_Factory(t) { return new (t || SoporteSolicitudComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_reportes_service__WEBPACK_IMPORTED_MODULE_7__["ReportesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_beneficiarios_service__WEBPACK_IMPORTED_MODULE_12__["SegBeneficiariosService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_ecofuturo_service__WEBPACK_IMPORTED_MODULE_8__["EcofuturoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_aprobaciones_service__WEBPACK_IMPORTED_MODULE_10__["SegAprobacionesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_send_mail_service__WEBPACK_IMPORTED_MODULE_9__["SendMailService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_observaciones_service__WEBPACK_IMPORTED_MODULE_11__["SegObservacionesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_file_upload_service__WEBPACK_IMPORTED_MODULE_17__["FileUploadService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_solicitudes_service__WEBPACK_IMPORTED_MODULE_13__["SegSolicitudesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_deudores_service__WEBPACK_IMPORTED_MODULE_14__["SegDeudoresService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_seg_adicionales_service__WEBPACK_IMPORTED_MODULE_15__["SegAdicionalesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_parametros_campo_service__WEBPACK_IMPORTED_MODULE_16__["ParametrosCampoService"])); };
SoporteSolicitudComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: SoporteSolicitudComponent, selectors: [["app-soporte-solicitud"]], decls: 523, vars: 161, consts: [["class", "overlay", 4, "ngIf"], [1, "w3-bar", "w3-deep-orange", "w3-block"], [1, "w3-bar-item"], [1, "w3-container", "w3-border", "w3-monospace"], [1, "w3-row"], [1, "w3-col", "w3-small", "s5", "m5", "l5"], [1, "w3-text-khaki"], [4, "ngIf"], [1, "w3-col", "s7", "m7", "l7"], [1, "w3-bar"], ["type", "button", 1, "w3-button", "w3-margin-left", "w3-deep-orange", "w3-round", 3, "click"], ["title", "Historial de la Solicitud", 1, "fas", "fa-history", "fa-2x"], [1, "w3-tiny"], ["type", "button", "class", "w3-button w3-margin-left w3-deep-orange w3-round", 3, "click", 4, "ngIf"], [1, "w3-button", "w3-margin-left", "w3-deep-orange", "w3-round", 3, "click"], ["title", "Guardar", 1, "fa", "fa-save", "fa-2x"], [2, "font-size", "10px"], [1, "w3-col", "s12", "m12", "l12"], [1, "w3-bar", "w3-light-grey"], [1, "w3-bar-item", "w3-button", "tablink", "w3-border-top", "w3-border-left", "w3-border-right", "w3-round", 3, "ngClass", "click"], [1, "w3-bar-item", "w3-button", "tablink", "w3-border-top", "w3-border-right", "w3-round", 3, "ngClass", "click"], ["class", "w3-bar-item w3-button tablink w3-border-top w3-border-right w3-round", 3, "ngClass", "click", 4, "ngIf"], ["class", "w3-bar-item w3-button tablink  w3-border-top w3-border-right w3-round", 3, "ngClass", "click", 4, "ngIf"], [1, "w3-border-bottom", "w3-border-left", "w3-border-right", "w3-padding", 3, "ngClass"], [1, "w3-col", "s5", "m5", "l5"], [1, "w3-table", "w3-small", 2, "border-collapse", "collapse"], [1, "w3-text-khaki", "w3-border-bottom"], ["type", "text", "name", "Sol_Oficial", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "text", "name", "Sol_CodOficial", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "text", "name", "Sol_Digitador", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "text", "name", "Sol_FechaSol", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "number", "name", "sol_montosol", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "text", "name", "Sol_MonedaSol", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "text", "name", "Sol_PlazoSol", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "Sol_EstadoSol", 1, "w3-select", "w3-pale-red", 3, "ngModel", "ngModelChange"], [3, "ngValue", 4, "ngFor", "ngForOf"], [1, "w3-col", "s2", "m2", "l2"], [1, "w3-row-padding", 3, "innerHTML"], [1, "w3-col", "s6", "m6", "l6"], [1, "w3-small"], ["class", "w3-small w3-margin-left", 4, "ngFor", "ngForOf"], [1, "w3-col", "s10", "m10", "l10"], [1, "w3-table", "w3-bordered", "w3-small"], ["type", "button", "class", "w3-button", 3, "click", 4, "ngIf"], [4, "ngFor", "ngForOf"], [1, "w3-col", "m5"], [1, "w3-col", "s8", "m8", "l8"], [1, "w3-table-all", "w3-container"], [1, "w3-center"], [1, "w3-table", "w3-small"], ["name", "mensaje", "cols", "25", "rows", "2", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "file", "type", "file", 1, "w3-input", 3, "change"], ["type", "button", 1, "w3-button", "w3-round-large", "w3-deep-orange", 3, "click"], [1, "fa", "fa-paper-plane"], [1, "w3-col", "s4", "m4", "l4"], ["id", "id10", 1, "w3-modal", 3, "ngStyle"], [1, "w3-modal-content"], [1, "w3-container", "w3-deep-orange"], [1, "w3-button", "w3-display-topright", 3, "click"], [1, "w3-container", "w3-light-grey"], [1, "w3-bar-item", "w3-right"], ["type", "button", 1, "w3-button", "w3-border", "w3-round-xlarge", 3, "click"], [1, "fas", "fa-check"], ["id", "id26", 1, "w3-modal", 3, "ngStyle"], [1, "w3-table-all", "w3-small"], ["id", "id06", 1, "w3-modal", 3, "ngStyle"], [1, "w3-tiny", "w3-text-khaki"], [1, "w3-bar-item", "w3-left"], ["type", "button", 1, "w3-button", "w3-round-xlarge", "w3-border", 3, "click"], [1, "fas", "fa-user-plus"], ["type", "button", 1, "w3-button", "w3-border", "w3-round-xlarge", 3, "disabled", "click"], ["id", "id13", 1, "w3-modal", 3, "ngStyle"], [1, "w3-table"], [1, "w3-container", "w3-small", "w3-light-grey", 2, "height", "400px", "overflow-y", "scroll"], [1, "w3-ul", "w3-hoverable"], ["class", "w3-text-red", 4, "ngFor", "ngForOf"], ["class", "w3-text-orange", 4, "ngFor", "ngForOf"], [1, "w3-modal", 3, "ngStyle"], [1, "w3-container", "w3-small", "w3-light-grey"], ["name", "causal", "cols", "25", "rows", "2", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], [1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "button", 1, "w3-button", "w3-block", "w3-round-large", "w3-deep-orange", 3, "click"], [1, "overlay"], [1, "loading"], [1, "fas", "fa-print", "fa-2x"], [1, "fa", "fa-trash", "fa-2x"], ["title", "Validar", 1, "fa", "fa-check", "fa-2x"], [3, "ngValue"], [1, "w3-small", "w3-margin-left"], [1, "w3-button", "w3-light-grey", "w3-block", "w3-left-align", "w3-round", 3, "ngClass"], [3, "click"], ["class", "fa fa-plus", 4, "ngIf"], ["class", "fa fa-minus", 4, "ngIf"], ["class", "w3-right", 4, "ngIf"], [1, "w3-border", 3, "ngStyle"], [1, "w3-row", "w3-padding"], ["type", "text", "name", "Deu_Paterno", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "text", "name", "Deu_Materno", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "text", "name", "Deu_Casada", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["type", "text", "name", "Deu_ExtDoc", 1, "w3-input", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "Deu_CiudadNac", "class", "w3-input w3-pale-red w3-border-0", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "Deu_Peso", "class", "w3-input w3-pale-red w3-border-0", "type", "number", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "Deu_Talla", "class", "w3-input w3-pale-red w3-border-0", "type", "number", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "w3-input w3-pale-red", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["name", "Deu_DetACtiv", "class", "w3-input w3-pale-red w3-border-0", "type", "text", 3, "ngModel", "ngModelChange", 4, "ngIf"], [1, "w3-text-red"], [1, "w3-text-teal", "w3-border-bottom"], ["class", "w3-button w3-margin-left w3-margin-top w3-deep-orange w3-round", 3, "click", 4, "ngIf"], [1, "w3-button", "w3-margin-left", "w3-margin-top", "w3-deep-orange", "w3-round", 3, "click"], ["title", "Declaracion de salud", 1, "fa", "fa-user-md", "fa-2x"], [1, "fa", "fa-plus"], [1, "fa", "fa-minus"], [1, "fa-solid", "fa-user"], [1, "fa-solid", "fa-user-group"], [1, "w3-right"], ["class", "fa fa-square", 3, "click", 4, "ngIf"], ["class", "fa fa-square-check", 3, "click", 4, "ngIf"], [1, "fa", "fa-square", 3, "click"], [1, "fa", "fa-square-check", 3, "click"], ["class", "fa fa-square", 4, "ngIf"], ["class", "fa fa-square-check", 4, "ngIf"], [1, "fa", "fa-square"], [1, "fa", "fa-square-check"], ["name", "Deu_CiudadNac", "type", "text", 1, "w3-input", "w3-pale-red", "w3-border-0", 3, "ngModel", "ngModelChange"], ["name", "Deu_Peso", "type", "number", 1, "w3-input", "w3-pale-red", "w3-border-0", 3, "ngModel", "ngModelChange"], ["name", "Deu_Talla", "type", "number", 1, "w3-input", "w3-pale-red", "w3-border-0", 3, "ngModel", "ngModelChange"], ["value", "DERECHO(A)"], ["value", "ZURDO(A)"], ["name", "Deu_DetACtiv", "type", "text", 1, "w3-input", "w3-pale-red", "w3-border-0", 3, "ngModel", "ngModelChange"], ["name", "Deu_MontoActAcumVerif", "type", "number", 1, "w3-input", "w3-pale-red", "w3-border-0", 3, "ngModel", "ngModelChange"], [1, "fas", "fa-users", "fa-2x"], ["type", "button", 1, "w3-button", 3, "click"], [1, "fa", "fa-retweet"], ["href", "javascript:void(0)", 1, "w3-text-blue", 3, "click"], ["colspan", "4", 1, "w3-center"], ["class", "w3-check", "type", "checkbox", "checked", "checked", "disabled", "", 4, "ngIf"], ["class", "w3-check", "type", "checkbox", "disabled", "", 4, "ngIf"], ["type", "checkbox", "checked", "checked", "disabled", "", 1, "w3-check"], ["type", "checkbox", "disabled", "", 1, "w3-check"], [1, "w3-table-all", "w3-small", "w3-margin-top"], ["style", "height: 70px;", 4, "ngFor", "ngForOf"], ["colspan", "5"], ["type", "button", 1, "w3-button", "w3-deep-orange", "w3-border", "w3-round-xlarge", "w3-right", 3, "click"], [2, "height", "70px"], [2, "margin-top", "10px"], ["type", "checkbox", "name", "aprobado", 1, "w3-check", 3, "ngModel", "change", "ngModelChange"], ["class", "fas fa-check-square", 4, "ngIf"], ["type", "checkbox", "name", "rechazado", 1, "w3-check", 3, "ngModel", "change", "ngModelChange"], [2, "width", "200px"], [1, "fas", "fa-check-square"], ["type", "number", "name", "extraprima", "placeholder", "Extra prima %", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "condiciones", "placeholder", "Condiciones/Exclusiones", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "causal", "placeholder", "Causal", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["width", "50%"], ["width", "20%"], ["type", "checkbox", "name", "valor_si", 1, "w3-check", 3, "ngModel", "change", "ngModelChange"], ["type", "checkbox", "name", "valor_no", 1, "w3-check", 3, "ngModel", "change", "ngModelChange"], ["width", "30%"], [4, "ngIf", "ngIfElse"], ["ver1", ""], ["name", "respuesta", "placeholder", "Aclaraci\u00F3n", 1, "w3-input", 3, "ngModel", "ngModelChange"], [1, "w3-small", 3, "click"], ["class", "far fa-trash-alt w3-right", 3, "click", 4, "ngIf"], [1, "w3-border", 3, "ngClass"], [1, "w3-col", "m12"], ["name", "nombre", "type", "text", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "paterno", "type", "text", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "materno", "type", "text", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "casada", "type", "text", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "sexo", 1, "w3-select", 3, "ngModel", "ngModelChange"], ["value", "M"], ["value", "F"], ["name", "numdoc", "type", "text", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "tipo_doc", 1, "w3-select", 3, "ngModel", "ngModelChange"], ["value", "CARNET DE IDENTIDAD"], ["value", "CARNET EXTRANJERO"], ["value", "PASAPORTE"], ["name", "ext_doc", 1, "w3-select", 3, "ngModel", "ngModelChange"], ["value", "LA PAZ"], ["value", "COCHABAMBA"], ["value", "SANTA CRUZ"], ["value", "ORURO"], ["value", "POTOSI"], ["value", "CHUQUISACA"], ["value", "TARIJA"], ["value", "BENI"], ["value", "PANDO"], ["value", "S/E"], ["name", "compdoc", "type", "text", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "relacion", 1, "w3-select", 3, "ngModel", "ngModelChange"], ["value", "CONVIVIENTE"], ["value", "CONYUGUE"], ["value", "HIJO(A)"], ["value", "HERMANO(A)"], ["value", "MADRE"], ["value", "PADRE"], ["value", "OTROS"], ["name", "porcentaje", "type", "number", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "casada", "type", "number", 1, "w3-input", "w3-border", "w3-pale-red", 3, "ngModel", "ngModelChange"], ["name", "tipo", 1, "w3-select", 3, "ngModel", "ngModelChange"], ["value", "PRIMARIO"], ["value", "CONTINGENTE"], [1, "w3-container", "w3-text-red", "w3-opacity"], [1, "far", "fa-trash-alt", "w3-right", 3, "click"], [1, "w3-margin-bottom"], [1, "w3-button", "w3-deep-orange", "w3-round-large", 3, "click"], [1, "w3-text-orange"]], template: function SoporteSolicitudComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, SoporteSolicitudComponent_div_0_Template, 2, 0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "SOLICITUD DE SEGURO DE CREDITOS");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, " Tipo de Cartera: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "span", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, SoporteSolicitudComponent_span_15_Template, 2, 1, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](17, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_19_listener() { return ctx.onViewHistorial(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](20, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](21, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "span", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, " Historial");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](24, SoporteSolicitudComponent_button_24_Template, 5, 0, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](25, SoporteSolicitudComponent_button_25_Template, 5, 0, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_26_listener() { return ctx.onGuardarDatosEdit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](27, "i", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "span", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30, "\u00A0\u00A0 Guardar \u00A0\u00A0");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](31, SoporteSolicitudComponent_button_31_Template, 5, 0, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](32, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "button", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_36_listener() { return ctx.tab_active = 0; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37, "Solicitud");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_38_listener() { return ctx.tab_active = 2; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](39, "Deudores");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_40_listener() { return ctx.tab_active = 3; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](41, "Beneficiarios");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](42, SoporteSolicitudComponent_button_42_Template, 2, 3, "button", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](43, SoporteSolicitudComponent_button_43_Template, 2, 3, "button", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](44, SoporteSolicitudComponent_button_44_Template, 2, 3, "button", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](45, SoporteSolicitudComponent_button_45_Template, 2, 3, "button", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "table", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "tbody");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](51, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](54, "Sucursal:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](55, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](57, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](59, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](60, "Agencia:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](61, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](62);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](63, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](65, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](66, "Ciudad:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](67, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](68);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](69, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](71, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](72, "Oficial de Cr\u00E9ditos: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](73, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](74, "input", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_input_ngModelChange_74_listener($event) { return ctx.data.seg_solicitudes.Sol_Oficial = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](75, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](77, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](78, "Usuario Of. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](79, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](80, "input", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_input_ngModelChange_80_listener($event) { return ctx.data.seg_solicitudes.Sol_CodOficial = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](81, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](83, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](84, "Digitador del Seguro: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](85, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](86, "input", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_input_ngModelChange_86_listener($event) { return ctx.data.seg_solicitudes.Sol_Digitador = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](87, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](88, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](89, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](90, "Tipo de Cr\u00E9dito: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](91, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](92);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](93, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](94, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](95, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](96, "Tipo de Cr\u00E9dito sg/ASFI: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](97, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](98);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](99, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](100, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](101, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](102, "Tipo de Cliente: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](103, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](104);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](105, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](106, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](107, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](108, "Tipo de Operaci\u00F3n Bajo L/C: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](109, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](110);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](111, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](112, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](113, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](114, "Tipo de Seguro: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](115, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](116);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](117, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](118, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](119, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](120, "Tipo Solicitud: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](121, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](122);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](123, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](124, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](125, " \u00A0 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](126, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](127, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](128, "table", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](129, "tbody");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](130, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](131, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](132, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](133, "Nro.Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](134, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](135);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](136, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](137, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](138, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](139, "Fecha Solicitud (yyyy-mm-dd):");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](140, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](141, "input", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_input_ngModelChange_141_listener($event) { return ctx.data.seg_solicitudes.Sol_FechaSol = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](142, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](143, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](144, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](145, "Monto solicitado:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](146, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](147, "input", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_input_ngModelChange_147_listener($event) { return ctx.data.seg_solicitudes.Sol_MontoSol = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](148, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](149, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](150, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](151, "Moneda Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](152, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](153, "input", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_input_ngModelChange_153_listener($event) { return ctx.data.seg_solicitudes.Sol_MonedaSol = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](154, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](155, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](156, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](157, "Plazo Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](158, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](159, "input", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_input_ngModelChange_159_listener($event) { return ctx.data.seg_solicitudes.Sol_PlazoSol = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](160, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](161, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](162, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](163, "Frec.Pag.Sol.:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](164, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](165);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](166, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](167, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](168, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](169, "N\u00FAmero L/C:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](170, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](171);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](172, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](173, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](174, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](175, "Moneda L/C:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](176, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](177);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](178, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](179, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](180, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](181, "Fecha Aprob. L/C:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](182, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](183);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](184, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](185, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](186, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](187, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](188, "Cant.Obligados:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](189, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](190);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](191, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](192, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](193, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](194, "ID Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](195, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](196);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](197, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](198, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](199, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](200, "Estado Solicitud:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](201, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](202, "select", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_select_ngModelChange_202_listener($event) { return ctx.data.seg_solicitudes.Sol_EstadoSol = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](203, SoporteSolicitudComponent_option_203_Template, 2, 2, "option", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](204, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](205, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](206, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](207, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](208, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](209, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](210, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](211, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](212, "pre", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](213);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](214, "json");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](215, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](216, SoporteSolicitudComponent_div_216_Template, 216, 62, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](217, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](218, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](219, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](220, "table", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](221, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](222, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](223, "Id");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](224, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](225, "Deudor");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](226, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](227, "Tipo Deudor");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](228, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](229, "Nombre Beneficiario");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](230, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](231, "Documento");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](232, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](233, "Porcentaje");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](234, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](235, "Tipo Beneficiarios");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](236, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](237, SoporteSolicitudComponent_button_237_Template, 2, 0, "button", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](238, SoporteSolicitudComponent_tr_238_Template, 19, 11, "tr", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](239, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](240, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](241, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](242, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](243, "table", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](244, "tbody");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](245, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](246, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](247, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](248, "N\u00FAmero de Operaci\u00F3n: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](249, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](250);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](251, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](252, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](253, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](254, "Monto Aprobado: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](255, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](256);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](257, "number");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](258, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](259, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](260, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](261, "Moneda: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](262, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](263);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](264, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](265, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](266, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](267, "Destino del Cr\u00E9dito: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](268, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](269);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](270, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](271, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](272, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](273, "Fecha Desembolsado: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](274, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](275);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](276, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](277, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](278, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](279, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](280, "Monto Desembolsado: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](281, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](282);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](283, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](284, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](285, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](286, "Fecha Aprobaci\u00F3n: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](287, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](288);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](289, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](290, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](291, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](292, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](293, "Plazo: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](294, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](295);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](296, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](297, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](298, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](299, "Frecuencia de Pago: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](300, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](301);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](302, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](303, "table", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](304, "tbody");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](305, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](306, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](307, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](308, "Fecha Reprogr.:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](309, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](310);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](311, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](312, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](313, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](314, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](315, "Fecha Vencimiento:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](316, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](317);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](318, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](319, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](320, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](321, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](322, "Tipo Garantia:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](323, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](324);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](325, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](326, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](327, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](328, "Tipo Garantia Codigo:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](329, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](330);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](331, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](332, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](333, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](334, "Valor Garantia:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](335, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](336);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](337, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](338, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](339, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](340, "Destino Prestamo:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](341, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](342);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](343, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](344, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](345, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](346, "Destino Codigo:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](347, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](348);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](349, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](350, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](351, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](352, "Actual Acumulado:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](353, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](354);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](355, "number");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](356, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](357, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](358, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](359, "td", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](360, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](361, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](362, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](363, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](364, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](365, "table", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](366, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](367, "td", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](368, "table", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](369, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](370, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](371, "Fecha");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](372, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](373, "Hora");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](374, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](375, "observaci\u00F3n");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](376, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](377, "Usuario");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](378, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](379, "EIF");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](380, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](381, "Aseguradora");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](382, SoporteSolicitudComponent_tr_382_Template, 14, 10, "tr", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](383, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](384, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](385, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](386, "td", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](387, "Respuesta");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](388, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](389, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](390, "textarea", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_textarea_ngModelChange_390_listener($event) { return ctx.respuestaObser = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](391, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](392, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](393, " Adjuntar archivo: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](394, "input", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function SoporteSolicitudComponent_Template_input_change_394_listener($event) { return ctx.onChangeUpload($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](395, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](396, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](397, "button", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_397_listener() { return ctx.onResponderObser(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](398, "i", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](399, " \u00A0 Responder y Enviar");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](400, "div", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](401, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](402, SoporteSolicitudComponent_div_402_Template, 7, 1, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](403, SoporteSolicitudComponent_div_403_Template, 8, 1, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](404, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](405, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](406, "div", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](407, "div", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](408, "header", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](409, "span", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_span_click_409_listener() { return ctx.openModalCuestion = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](410, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](411, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](412, "Cuestionario de Salud");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](413, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](414, "table", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](415, SoporteSolicitudComponent_tr_415_Template, 12, 6, "tr", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](416, "footer", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](417, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](418, "div", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](419, "button", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_419_listener() { return ctx.onAceptarCuestionario(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](420, "i", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](421, " Aceptar y Guardar ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](422, "div", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](423, "div", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](424, "header", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](425, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](426, "span", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_span_click_426_listener() { return ctx.openModalHistorial = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](427, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](428, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](429, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](430, "table", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](431, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](432, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](433, "Fecha");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](434, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](435, "Hora");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](436, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](437, "observaci\u00F3n");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](438, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](439, "Usuario");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](440, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](441, "EIF");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](442, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](443, "Aseguradora");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](444, SoporteSolicitudComponent_tr_444_Template, 14, 10, "tr", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](445, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](446, "footer", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](447, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](448, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](449, "div", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](450, "header", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](451, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](452, "span", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](453, "BENEFICIARIOS DE : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](454, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](455);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](456, "span", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_span_click_456_listener() { return ctx.openModalBene = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](457, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](458, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](459, SoporteSolicitudComponent_div_459_Template, 128, 28, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](460, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](461, "footer", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](462, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](463, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](464, "button", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_464_listener() { return ctx.addBeneficiario(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](465, "i", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](466, " Adicionar Beneficiarios ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](467, "div", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](468, "button", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_468_listener() { return ctx.updateBeneficiarios(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](469, "i", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](470, " Aceptar y Guardar ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](471, "div", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](472, "div", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](473, "header", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](474, "span", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_span_click_474_listener() { return ctx.openModalImprimir = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](475, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](476, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](477, "Imprimir documentos");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](478, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](479, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](480, "table", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](481, SoporteSolicitudComponent_tr_481_Template, 3, 2, "tr", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](482, "footer", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](483, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](484, "div", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](485, "div", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](486, "header", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](487, "span", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_span_click_487_listener() { return ctx.openModalValida = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](488, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](489, "h4", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](490, "Observaciones y Advertencias");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](491, "div", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](492, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](493, "ul", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](494, SoporteSolicitudComponent_li_494_Template, 2, 1, "li", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](495, "ul", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](496, SoporteSolicitudComponent_li_496_Template, 2, 1, "li", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](497, "footer", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](498, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](499, "div", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](500, "div", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](501, "header", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](502, "span", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_span_click_502_listener() { return ctx.openModalDesistir = "none"; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](503, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](504, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](505, "Desestimiento de Solicitudes");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](506, "div", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](507, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](508, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](509, "Causa:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](510, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](511, "textarea", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_textarea_ngModelChange_511_listener($event) { return ctx.desistimiento.causal = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](512, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](513, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](514, "Autorizado por:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](515, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](516, "select", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SoporteSolicitudComponent_Template_select_ngModelChange_516_listener($event) { return ctx.desistimiento.autorizado_por = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](517, SoporteSolicitudComponent_option_517_Template, 2, 2, "option", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](518, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](519, "button", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SoporteSolicitudComponent_Template_button_click_519_listener() { return ctx.onDesistir(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](520, " Efectivizar Desestimiento ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](521, "footer", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](522, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" Nro.Solicitud: ", ctx.data.seg_solicitudes.Sol_NumSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_solicitudes.Sol_TipoSeg);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_general == "APROBADO");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_codigo == "X");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](117, _c5).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.estado_codigo == "X");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](118, _c0, ctx.tab_active == 0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](120, _c0, ctx.tab_active == 2));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](122, _c0, ctx.tab_active == 3));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.usuario_rol == "of");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](124, _c6).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](125, _c7).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](126, _c4, ctx.tab_active != 0, ctx.tab_active == 0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_solicitudes.Sol_Sucursal);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_solicitudes.Sol_Agencia);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.data.seg_solicitudes.Sol_Ciudad, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.data.seg_solicitudes.Sol_Oficial);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.data.seg_solicitudes.Sol_CodOficial);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.data.seg_solicitudes.Sol_Digitador);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.data.seg_solicitudes.Sol_TipoCred, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_TipoAsfi, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_solicitudes.Sol_TipoCli);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.data.seg_solicitudes.Sol_TipoOpeLC, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_TipoSeg, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_SolTipoSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_NumSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.data.seg_solicitudes.Sol_FechaSol);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.data.seg_solicitudes.Sol_MontoSol);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.data.seg_solicitudes.Sol_MonedaSol);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.data.seg_solicitudes.Sol_PlazoSol);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_FrecPagoSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_NumLC, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_MonedaLC, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](184, 89, ctx.data.seg_solicitudes.Sol_FechaLC, 0, 10), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_CantObl, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.data.seg_solicitudes.Sol_IdSol, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.data.seg_solicitudes.Sol_EstadoSol);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx._estados_solicitud);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](129, _c4, ctx.tab_active != 1, ctx.tab_active == 1));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx.viewTable, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind1"](214, 93, ctx.data), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](132, _c4, ctx.tab_active != 2, ctx.tab_active == 2));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.data.seg_deudores);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](135, _c4, ctx.tab_active != 3, ctx.tab_active == 3));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.data.seg_beneficiarios);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](138, _c4, ctx.tab_active != 4, ctx.tab_active == 4));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Numero_Aprobado);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](257, 95, ctx.data.seg_operaciones.Operacion_Monto_Aprobado, ".2"), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Moneda_Aprobada);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Destino_Prestamo);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](276, 98, ctx.data.seg_operaciones.Operacion_Fecha_Desembolso, 0, 10), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Monto_Desembolsado);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](289, 102, ctx.data.seg_operaciones.Operacion_Fecha_Aprobacion, 0, 10), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Plazo_Aprobado);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Frecuencia_Aprobada);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](311, 106, ctx.data.seg_operaciones.Operacion_Fecha_Reprogramacion, 0, 10), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind3"](318, 110, ctx.data.seg_operaciones.Operacion_Fecha_Vencimiento, 0, 10), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Tipo_Garantia);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Tipo_Grantia_Codigo);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Valor_Garantia);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Destino_Prestamo);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.data.seg_operaciones.Operacion_Destino_Codigo);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](355, 114, ctx.data.seg_operaciones.Operacion_Actual_Acumulado, ".2"), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](141, _c4, ctx.tab_active != 5, ctx.tab_active == 5));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.segObservacionesUlt);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.respuestaObser);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](144, _c4, ctx.tab_active != 6, ctx.tab_active == 6));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](147, _c8).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.usuario_rol != "of" && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](148, _c6).includes(ctx.estado_codigo));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](149, _c2, ctx.openModalCuestion));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.declaraciones);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](151, _c2, ctx.openModalHistorial));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.data.seg_observaciones);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](153, _c2, ctx.openModalBene));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.deudor_nombre_sel);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.segBeneficiariosIdDeu);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx.segBeneficiariosIdDeu.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](155, _c2, ctx.openModalImprimir));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.data.seg_deudores);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](157, _c2, ctx.openModalValida));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.observaciones);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.advertencias);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](159, _c2, ctx.openModalDesistir));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.desistimiento.causal);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.desistimiento.autorizado_por);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.autorizacionesSolicitud);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_27__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_27__["NgClass"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["NumberValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["SelectControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_27__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_27__["NgStyle"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["ɵangular_packages_forms_forms_x"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["CheckboxControlValueAccessor"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_27__["SlicePipe"], _angular_common__WEBPACK_IMPORTED_MODULE_27__["JsonPipe"], _angular_common__WEBPACK_IMPORTED_MODULE_27__["DecimalPipe"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlZ3Vyb3Mvc29wb3J0ZS1zb2xpY2l0dWQvc29wb3J0ZS1zb2xpY2l0dWQuY29tcG9uZW50LmNzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](SoporteSolicitudComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
                selector: 'app-soporte-solicitud',
                templateUrl: './soporte-solicitud.component.html',
                styleUrls: ['./soporte-solicitud.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }, { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] }, { type: _service_reportes_service__WEBPACK_IMPORTED_MODULE_7__["ReportesService"] }, { type: _service_seg_beneficiarios_service__WEBPACK_IMPORTED_MODULE_12__["SegBeneficiariosService"] }, { type: _service_ecofuturo_service__WEBPACK_IMPORTED_MODULE_8__["EcofuturoService"] }, { type: _service_seg_aprobaciones_service__WEBPACK_IMPORTED_MODULE_10__["SegAprobacionesService"] }, { type: _service_send_mail_service__WEBPACK_IMPORTED_MODULE_9__["SendMailService"] }, { type: _service_seg_observaciones_service__WEBPACK_IMPORTED_MODULE_11__["SegObservacionesService"] }, { type: _service_file_upload_service__WEBPACK_IMPORTED_MODULE_17__["FileUploadService"] }, { type: _service_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] }, { type: _service_seg_solicitudes_service__WEBPACK_IMPORTED_MODULE_13__["SegSolicitudesService"] }, { type: _service_seg_deudores_service__WEBPACK_IMPORTED_MODULE_14__["SegDeudoresService"] }, { type: _service_seg_adicionales_service__WEBPACK_IMPORTED_MODULE_15__["SegAdicionalesService"] }, { type: _service_parametros_campo_service__WEBPACK_IMPORTED_MODULE_16__["ParametrosCampoService"] }]; }, null); })();
;
;
;
;
;
;
;
const getDateYYYYmmDD = () => {
    var d = new Date(), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    return [year, month, day].join('-');
};


/***/ }),

/***/ "../../src/app/service/auth.service.ts":
/*!*****************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/service/auth.service.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _models_Usuario__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/Usuario */ "../../src/app/models/Usuario.ts");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! crypto-js */ "../../node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_3__);






const KEY_ENCRYPT_SECRET = 'pr4bvrw3ghdpbpsxuhnxrhyc4u5q3pjx';
class AuthService {
    constructor(router) {
        this.router = router;
        this.isLogin = false;
        this.usuario = new _models_Usuario__WEBPACK_IMPORTED_MODULE_2__["Usuario"]();
        this.encryptSecretKey = 'pr4bvrw3ghdpbpsxuhnxrhyc4u5q3pjx';
    }
    setupSocketConnection() {
        //environment.SOCKET_ENDPOINT
    }
    ;
    // userInfo
    getUserInfo() {
        try {
            const value = sessionStorage.getItem('userInfo');
            if (value) {
                const valueDes = this.decryptData(value);
                console.log(':) getUerInfo:', valueDes);
                return valueDes;
            }
            else {
                return null;
            }
        }
        catch (error) {
            return null;
        }
    }
    ;
    setItemSync(key, value, encrypted = true) {
        if (encrypted) {
            value = this.encryptData(value);
        }
        localStorage.setItem(key, value);
        sessionStorage.setItem(key, value);
    }
    getItemSync(key, encrypted = true) {
        let localValue = localStorage.getItem(key);
        let sessionValue = sessionStorage.getItem(key);
        if (!localValue && !sessionValue) {
            return null;
        }
        if (localValue) {
            if (encrypted) {
                localValue = this.decryptData(localValue);
            }
            return localValue ? typeof localValue == 'object' ? localValue : typeof localValue == 'string' ? JSON.parse(localValue) : localValue : localValue;
        }
        else {
            return localValue;
        }
    }
    ;
    // userInfo
    setUserInfo(value) {
        const valueEnc = this.encryptData(value);
        sessionStorage.setItem('userInfo', valueEnc);
        console.log('set sessionStore');
    }
    ;
    removeUserInfo() {
        sessionStorage.removeItem('userInfo');
    }
    ;
    encryptData(data) {
        try {
            return crypto_js__WEBPACK_IMPORTED_MODULE_3__["AES"].encrypt(JSON.stringify(data), KEY_ENCRYPT_SECRET).toString();
        }
        catch (e) {
            // console.log(e);
        }
    }
    ;
    decryptData(data) {
        try {
            const bytes = crypto_js__WEBPACK_IMPORTED_MODULE_3__["AES"].decrypt(data, KEY_ENCRYPT_SECRET);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(crypto_js__WEBPACK_IMPORTED_MODULE_3__["enc"].Utf8));
            }
            return data;
        }
        catch (e) {
            // console.log(e);
        }
    }
    ;
}
AuthService.ɵfac = function AuthService_Factory(t) { return new (t || AuthService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"])); };
AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AuthService, factory: AuthService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/shared/buscar-deudor/buscar-deudor.component.ts":
/*!*****************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/shared/buscar-deudor/buscar-deudor.component.ts ***!
  \*****************************************************************************************************************************************/
/*! exports provided: BuscarDeudorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuscarDeudorComponent", function() { return BuscarDeudorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "../../node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
/* harmony import */ var _service_entidad_fin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/entidad-fin.service */ "../../src/app/shared/service/entidad-fin.service.ts");
/* harmony import */ var _service_parametros_campo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/parametros-campo.service */ "../../src/app/shared/service/parametros-campo.service.ts");
/* harmony import */ var _service_datos_folder_seguro_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/datos-folder-seguro.service */ "../../src/app/shared/service/datos-folder-seguro.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "../../node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _pipe_nombre_campo_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../pipe/nombre-campo.pipe */ "../../src/app/shared/pipe/nombre-campo.pipe.ts");














function BuscarDeudorComponent_div_27_tr_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](4, "nombreCampo");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const row_r6 = ctx.$implicit;
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](4, 2, row_r6.key, ctx_r3.parametrosCampo));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r6.value);
} }
function BuscarDeudorComponent_div_27_button_9_Template(rf, ctx) { if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscarDeudorComponent_div_27_button_9_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r7.onContinuarCrear(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "i", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Gestionar seguro");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function BuscarDeudorComponent_div_27_button_10_Template(rf, ctx) { if (rf & 1) {
    const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscarDeudorComponent_div_27_button_10_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11); const cli_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit; const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.onContinuarAdd(cli_r1.numero_documento); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "i", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Incluir como codeudor");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function (a0) { return { "w3-dark-grey": a0 }; };
const _c1 = function (a0, a1) { return { "w3-hide": a0, "w3-show": a1 }; };
function BuscarDeudorComponent_div_27_Template(rf, ctx) { if (rf & 1) {
    const _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscarDeudorComponent_div_27_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13); const i_r2 = ctx.index; const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r12.ops[i_r2] ? (ctx_r12.ops[i_r2] = false) : (ctx_r12.ops[i_r2] = true); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "table", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, BuscarDeudorComponent_div_27_tr_5_Template, 8, 5, "tr", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](6, "keyvalue");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "td", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, BuscarDeudorComponent_div_27_button_9_Template, 3, 0, "button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, BuscarDeudorComponent_div_27_button_10_Template, 3, 0, "button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const cli_r1 = ctx.$implicit;
    const i_r2 = ctx.index;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](10, _c0, ctx_r0.ops[i_r2]));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate3"](" ", cli_r1.nombre, " ", cli_r1.apellido_paterno, " ", cli_r1.apellido_materno, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](12, _c1, !ctx_r0.ops[i_r2], ctx_r0.ops[i_r2]));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](6, 8, cli_r1));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.param.accion == "create_folder");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.param.accion == "add-codeudor");
} }
const _c2 = function (a0) { return { "display": a0 }; };
class BuscarDeudorComponent {
    constructor(ds, msg, parametrosCamposService, datosFolderSeguroService) {
        this.ds = ds;
        this.msg = msg;
        this.parametrosCamposService = parametrosCamposService;
        this.datosFolderSeguroService = datosFolderSeguroService;
        this.result = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.parametrosCampo = [];
        this.clientes = [];
        this.active = false;
        this.tab_active = 0;
        this.openModal = 'none';
        this.ops = [false, false, false, false, false];
    }
    ngOnInit() {
        this.getParametrosCampo();
    }
    getParametrosCampo() {
        this.parametrosCamposService.getAll().subscribe(resp => {
            const response = resp;
            this.parametrosCampo = response.data;
        });
    }
    onBuscarCodeu() {
        if (this.param.numero_documento) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                allowOutsideClick: false,
                text: 'Obteniendo datos del prestamo ...'
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.showLoading();
            //cliente
            this.ds.getCliente(this.param.numero_documento).subscribe(cli => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.close();
                const resp = cli;
                if (resp.status === 'OK') {
                    this.clientes = resp.data;
                }
                else {
                    this.msg.error(resp.messages.toString(), 'Consulta servicio');
                }
            });
        }
    }
    onContinuarCrear() {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: 'Aplicar seguro',
            showCancelButton: true,
            confirmButtonText: `SI`,
            cancelButtonText: `NO`,
            confirmButtonColor: '#ff9933',
            cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.isConfirmed) {
                //this.crearDatosFolderSeguros(documento);
                this.param.numero_documento = '';
                this.clientes = [];
            }
        });
    }
    onContinuarAdd(num_doc) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: 'Incluir como codeudor',
            showCancelButton: true,
            confirmButtonText: `SI`,
            cancelButtonText: `NO`,
            confirmButtonColor: '#ff9933',
            cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.isConfirmed) {
                const request = { numero_documento: num_doc, id_folder: this.param.id_folder, id_prestamo: 0 };
                this.datosFolderSeguroService.addCodeudor(request).subscribe(resp => {
                    const respose = resp;
                    if (respose.status === 'OK') {
                        this.result.emit(true);
                    }
                    else {
                        this.result.emit(false);
                        this.msg.error(respose.messages.toString(), 'Codeudores');
                    }
                });
                this.param.numero_documento = '';
                this.param.id_folder = 0;
                this.param.accion = '';
                this.clientes = [];
            }
        });
    }
}
BuscarDeudorComponent.ɵfac = function BuscarDeudorComponent_Factory(t) { return new (t || BuscarDeudorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_entidad_fin_service__WEBPACK_IMPORTED_MODULE_2__["EntidadFinService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_parametros_campo_service__WEBPACK_IMPORTED_MODULE_3__["ParametrosCampoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_datos_folder_seguro_service__WEBPACK_IMPORTED_MODULE_4__["DatosFolderSeguroService"])); };
BuscarDeudorComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BuscarDeudorComponent, selectors: [["app-buscar-deudor"]], inputs: { param: "param" }, outputs: { result: "result" }, decls: 37, vars: 23, consts: [[1, "w3-row"], [1, "w3-col", "m12"], [1, "w3-bar"], [1, "w3-bar-item"], [1, "w3-row-padding", "w3-bar-item"], [1, "w3-third"], ["required", "", "name", "numero_documento", "type", "text", 1, "w3-input", "w3-bar-item", 3, "ngModel", "ngModelChange"], ["type", "button", 1, "w3-button", "w3-teal", 3, "click"], [1, "fa", "fa-search"], [1, "w3-container"], [1, "w3-bar", "w3-light-grey"], [1, "w3-bar-item", "w3-button", "tablink", 3, "ngClass", "click"], [1, "w3-container", "w3-border", "city", "w3-light-grey", 3, "ngStyle"], [4, "ngFor", "ngForOf"], [1, "w3-container", "w3-border", "w3-light-grey", 3, "ngStyle"], [1, "w3-button", "w3-light-grey", "w3-block", "w3-left-align", 3, "ngClass", "click"], [1, "w3-border", 3, "ngClass"], [1, "w3-table-all"], ["colspan", "2", 1, "w3-center"], ["class", "w3-button w3-teal", 3, "click", 4, "ngIf"], [1, "w3-button", "w3-teal", 3, "click"], [1, "fa", "fa-check"]], template: function BuscarDeudorComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Numero de documento");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "input", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function BuscarDeudorComponent_Template_input_ngModelChange_9_listener($event) { return ctx.param.numero_documento = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscarDeudorComponent_Template_button_click_11_listener() { return ctx.onBuscarCodeu(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscarDeudorComponent_Template_button_click_21_listener() { return ctx.tab_active = 0; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Cliente");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscarDeudorComponent_Template_button_click_23_listener() { return ctx.tab_active = 1; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscarDeudorComponent_Template_button_click_24_listener() { return ctx.tab_active = 2; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, BuscarDeudorComponent_div_27_Template, 11, 15, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "pre");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](35, "json");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.param.numero_documento);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](11, _c0, ctx.tab_active == 0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](13, _c0, ctx.tab_active == 1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](15, _c0, ctx.tab_active == 2));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](17, _c2, ctx.tab_active == 0 ? "block" : "none"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.clientes);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](19, _c2, ctx.tab_active == 1 ? "block" : "none"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](21, _c2, ctx.tab_active == 2 ? "block" : "none"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](35, 9, ctx.clientes));
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgStyle"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["JsonPipe"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["KeyValuePipe"], _pipe_nombre_campo_pipe__WEBPACK_IMPORTED_MODULE_8__["NombreCampoPipe"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9idXNjYXItZGV1ZG9yL2J1c2Nhci1kZXVkb3IuY29tcG9uZW50LmNzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BuscarDeudorComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-buscar-deudor',
                templateUrl: './buscar-deudor.component.html',
                styleUrls: ['./buscar-deudor.component.css']
            }]
    }], function () { return [{ type: _service_entidad_fin_service__WEBPACK_IMPORTED_MODULE_2__["EntidadFinService"] }, { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"] }, { type: _service_parametros_campo_service__WEBPACK_IMPORTED_MODULE_3__["ParametrosCampoService"] }, { type: _service_datos_folder_seguro_service__WEBPACK_IMPORTED_MODULE_4__["DatosFolderSeguroService"] }]; }, { param: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], result: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }] }); })();


/***/ }),

/***/ "../../src/app/shared/pipe/nombre-campo.pipe.ts":
/*!**************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/shared/pipe/nombre-campo.pipe.ts ***!
  \**************************************************************************************************************************/
/*! exports provided: NombreCampoPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NombreCampoPipe", function() { return NombreCampoPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class NombreCampoPipe {
    transform(value, parametrosCampos) {
        const parametrosCampo = parametrosCampos.find(param => param.campo === value);
        if (parametrosCampo) {
            return parametrosCampo.titulo;
        }
        else {
            return null;
        }
    }
}
NombreCampoPipe.ɵfac = function NombreCampoPipe_Factory(t) { return new (t || NombreCampoPipe)(); };
NombreCampoPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({ name: "nombreCampo", type: NombreCampoPipe, pure: true });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NombreCampoPipe, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
        args: [{
                name: 'nombreCampo'
            }]
    }], null, null); })();


/***/ }),

/***/ "../../src/app/shared/service/datos-folder-seguro.service.ts":
/*!***************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/shared/service/datos-folder-seguro.service.ts ***!
  \***************************************************************************************************************************************/
/*! exports provided: DatosFolderSeguroService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatosFolderSeguroService", function() { return DatosFolderSeguroService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class DatosFolderSeguroService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/datos-folder-seguro`;
    }
    getAll() {
        return this.http.get(this.URL_API);
    }
    getId(id) {
        return this.http.get(`${this.URL_API}/${id}`);
    }
    getIdPrestamo(id) {
        return this.http.get(`${this.URL_API}/prestamo/${id}`);
    }
    add(idPrestamo) {
        return this.http.post(this.URL_API, { id_prestamo: idPrestamo });
    }
    update(model) {
        return this.http.put(`${this.URL_API}/${model.folder_seguro.id}`, model);
    }
    delete(id) {
        return this.http.delete(`${this.URL_API}/${id}`);
    }
    addCodeudor(param) {
        return this.http.post(`${this.URL_API}/add-codeudor`, param);
    }
    delCodeudor(param) {
        return this.http.post(`${this.URL_API}/del-codeudor`, param);
    }
}
DatosFolderSeguroService.ɵfac = function DatosFolderSeguroService_Factory(t) { return new (t || DatosFolderSeguroService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
DatosFolderSeguroService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: DatosFolderSeguroService, factory: DatosFolderSeguroService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DatosFolderSeguroService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/shared/service/entidad-fin.service.ts":
/*!*******************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/shared/service/entidad-fin.service.ts ***!
  \*******************************************************************************************************************************/
/*! exports provided: EntidadFinService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntidadFinService", function() { return EntidadFinService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class EntidadFinService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/idepro`;
    }
    // se obtiene participante, solicitud y operacion
    getParticipanteSolicitudOperacion(id) {
        return this.http.get(`${this.URL_API}/${id}`);
    }
    getParticipante(id) {
        return this.http.get(`${this.URL_API}/participante/${id}`);
    }
    getSolicitud(id) {
        return this.http.get(`${this.URL_API}/solicitud/${id}`);
    }
    getOperacion(id) {
        return this.http.get(`${this.URL_API}/operacion/${id}`);
    }
    getPrestamos(id) {
        return this.http.get(`${this.URL_API}/prestamos/${id}`);
    }
    getCliente(id) {
        return this.http.get(`${this.URL_API}/cliente/${id}`);
    }
}
EntidadFinService.ɵfac = function EntidadFinService_Factory(t) { return new (t || EntidadFinService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
EntidadFinService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: EntidadFinService, factory: EntidadFinService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EntidadFinService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/shared/service/parametros-campo.service.ts":
/*!************************************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/shared/service/parametros-campo.service.ts ***!
  \************************************************************************************************************************************/
/*! exports provided: ParametrosCampoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParametrosCampoService", function() { return ParametrosCampoService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "../../src/environments/environment.ts");





class ParametrosCampoService {
    constructor(http) {
        this.http = http;
        this.URL_API = `${src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API}/parametros-campo`;
    }
    getAll() {
        return this.http.get(this.URL_API);
    }
    getOne(id) {
        return this.http.get(`${this.URL_API}/${id}`);
    }
    add(model) {
        return this.http.post(this.URL_API, model);
    }
    update(model) {
        return this.http.put(`${this.URL_API}/${model.id}`, model);
    }
    delete(id) {
        return this.http.delete(`${this.URL_API}/${id}`);
    }
}
ParametrosCampoService.ɵfac = function ParametrosCampoService_Factory(t) { return new (t || ParametrosCampoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
ParametrosCampoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ParametrosCampoService, factory: ParametrosCampoService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ParametrosCampoService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "../../src/app/shared/shared.module.ts":
/*!*****************************************************************************************************************!*\
  !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/app/shared/shared.module.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _buscar_deudor_buscar_deudor_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./buscar-deudor/buscar-deudor.component */ "../../src/app/shared/buscar-deudor/buscar-deudor.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _pipe_nombre_campo_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pipe/nombre-campo.pipe */ "../../src/app/shared/pipe/nombre-campo.pipe.ts");






class SharedModule {
}
SharedModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: SharedModule });
SharedModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SharedModule, { declarations: [_buscar_deudor_buscar_deudor_component__WEBPACK_IMPORTED_MODULE_2__["BuscarDeudorComponent"], _pipe_nombre_campo_pipe__WEBPACK_IMPORTED_MODULE_4__["NombreCampoPipe"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]], exports: [_buscar_deudor_buscar_deudor_component__WEBPACK_IMPORTED_MODULE_2__["BuscarDeudorComponent"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SharedModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [_buscar_deudor_buscar_deudor_component__WEBPACK_IMPORTED_MODULE_2__["BuscarDeudorComponent"], _pipe_nombre_campo_pipe__WEBPACK_IMPORTED_MODULE_4__["NombreCampoPipe"]],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
                ],
                exports: [_buscar_deudor_buscar_deudor_component__WEBPACK_IMPORTED_MODULE_2__["BuscarDeudorComponent"]]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=seguros-seguros-module-es2015.js.map