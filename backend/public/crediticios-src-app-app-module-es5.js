function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return generator._invoke = function (innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; }(innerFn, self, context), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == typeof value && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; this._invoke = function (method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); }; } function maybeInvokeDelegate(delegate, context) { var method = delegate.iterator[context.method]; if (undefined === method) { if (context.delegate = null, "throw" === context.method) { if (delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel; context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method"); } return ContinueSentinel; } var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) { if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; } return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (object) { var keys = []; for (var key in object) { keys.push(key); } return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) { "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); } }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, "catch": function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["crediticios-src-app-app-module"], {
  /***/
  "../../src/core/componentes/archivos/archivos.component.ts":
  /*!*************************************************************************************************************************************!*\
    !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/core/componentes/archivos/archivos.component.ts ***!
    \*************************************************************************************************************************************/

  /*! exports provided: ArchivosComponent */

  /***/
  function srcCoreComponentesArchivosArchivosComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArchivosComponent", function () {
      return ArchivosComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _modelos_upload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../modelos/upload */
    "../../src/core/modelos/upload.ts");
    /* harmony import */


    var _servicios_archivo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../servicios/archivo.service */
    "../../src/core/servicios/archivo.service.ts");
    /* harmony import */


    var _servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../servicios/solicitud.service */
    "../../src/core/servicios/solicitud.service.ts");
    /* harmony import */


    var _servicios_instancia_documento_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../servicios/instancia-documento.service */
    "../../src/core/servicios/instancia-documento.service.ts");
    /* harmony import */


    var primeng__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! primeng */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng.js");
    /* harmony import */


    var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! primeng/api */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-api.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common */
    "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function ArchivosComponent_ng_template_7_tr_1_td_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var i_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().index;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](i_r3 + 1);
      }
    }

    function ArchivosComponent_ng_template_7_tr_1_td_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "img", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var file_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("src", file_r2.ext == "pdf" ? ctx_r5.origin + "/assets/pdf_logo.jpeg" : file_r2.path, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeUrl"]);
      }
    }

    function ArchivosComponent_ng_template_7_tr_1_td_3_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var file_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](file_r2.newName);
      }
    }

    function ArchivosComponent_ng_template_7_tr_1_td_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var file_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](file_r2.tipoDocumento);
      }
    }

    function ArchivosComponent_ng_template_7_tr_1_td_5_Template(rf, ctx) {
      if (rf & 1) {
        var _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ArchivosComponent_ng_template_7_tr_1_td_5_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r15);

          var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r14.deleteFile(ctx_r14.solicitudService.instanciaDocumentoSolicitud.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var file_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", file_r2.idTipoDocumento == ctx_r8.solicitudService.documentoSolicitud.id_tipo_documento && ctx_r8.solicitudService.asegurado.instancia_poliza.id_estado != ctx_r8.solicitudService.estadoSolicitado.id || file_r2.idTipoDocumento == ctx_r8.solicitudService.documentoCertificado.id_tipo_documento && ctx_r8.solicitudService.asegurado.instancia_poliza.id_estado != ctx_r8.solicitudService.estadoEmitido.id);
      }
    }

    function ArchivosComponent_ng_template_7_tr_1_td_6_Template(rf, ctx) {
      if (rf & 1) {
        var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ArchivosComponent_ng_template_7_tr_1_td_6_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r19);

          var file_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r17.solicitudService.ShowUploadedFile(file_r2.newName);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function ArchivosComponent_ng_template_7_tr_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ArchivosComponent_ng_template_7_tr_1_td_1_Template, 2, 1, "td", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ArchivosComponent_ng_template_7_tr_1_td_2_Template, 2, 1, "td", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, ArchivosComponent_ng_template_7_tr_1_td_3_Template, 2, 1, "td", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, ArchivosComponent_ng_template_7_tr_1_td_4_Template, 2, 1, "td", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, ArchivosComponent_ng_template_7_tr_1_td_5_Template, 2, 1, "td", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, ArchivosComponent_ng_template_7_tr_1_td_6_Template, 2, 0, "td", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var file_r2 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", file_r2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", file_r2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", file_r2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", file_r2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", file_r2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", file_r2);
      }
    }

    function ArchivosComponent_ng_template_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "table", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ArchivosComponent_ng_template_7_tr_1_Template, 7, 6, "tr", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r0.uploadedFiles);
      }
    }

    var _c0 = function _c0(a0) {
      return [a0];
    };

    var ArchivosComponent = /*#__PURE__*/function () {
      function ArchivosComponent(archivoService, solicitudService, instanciaDocumentoService) {
        _classCallCheck(this, ArchivosComponent);

        this.archivoService = archivoService;
        this.solicitudService = solicitudService;
        this.instanciaDocumentoService = instanciaDocumentoService;
        this.uploadedFiles = [];
        this.uploadedFile = null; // this.breadcrumbService.setItems([
        //   {label: 'Components'},
        //   {label: 'File Upload', routerLink: ['/file']}
        // ]);
      }

      _createClass(ArchivosComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.origin = window.location.origin;

          if (this.solicitudService.fileDocumentoSolicitud && this.solicitudService.fileDocumentoSolicitud.path) {
            this.uploadedFiles.push(this.solicitudService.fileDocumentoSolicitud);
          }

          if (this.solicitudService.fileDocumentoCertificado && this.solicitudService.fileDocumentoCertificado.path) {
            this.uploadedFiles.push(this.solicitudService.fileDocumentoCertificado);
          }

          for (var i = 0; i < this.uploadedFiles.length; i++) {
            var uploadedFile = this.uploadedFiles[i];

            if (uploadedFile.name && uploadedFile.path) {
              if (uploadedFile.name.indexOf('.pdf') >= 0) {
                uploadedFile.source = "/assets/pdf_logo.jpeg";
              } else {
                uploadedFile.source = uploadedFile.path;
              }
            } else {
              delete this.uploadedFiles[i];
            }
          }
        }
      }, {
        key: "showFile",
        value: function showFile(event) {
          console.log(event);

          if (event.image.path) {
            window.open(event.image.path, '_blank');
          } else {
            window.open("/upload/doc_".concat(this.solicitudService.instanciaDocumentoSolicitud.id, "_").concat(event.image.name), '_blank');
          }
        }
      }, {
        key: "onUpload",
        value: function onUpload(events) {
          var newName;

          if (this.solicitudService.asegurado.instancia_poliza.id) {
            this.msgs = [];

            for (var i = 0; i < events.files.length; i++) {
              var file = events.files[i];

              if (file.name) {
                if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                  newName = "doc_".concat(this.solicitudService.instanciaDocumentoSolicitud.id, "_") + file.name;
                  file.newName = newName;

                  var _newName$split = newName.split('.');

                  var _newName$split2 = _slicedToArray(_newName$split, 2);

                  file.file = _newName$split2[0];
                  file.ext = _newName$split2[1];
                  this.uploadedFile = file;
                  this.uploadedFile.path = this.solicitudService.documentoSolicitud.archivo.ubicacion_local;
                  this.uploadedFile.pathName = this.solicitudService.documentoSolicitud.archivo.ubicacion_local + '/' + this.uploadedFile.newName;
                  this.uploadedFile.tipoDocumento = this.solicitudService.documentoSolicitud.descripcion;
                  this.uploadedFile.idTipoDocumento = this.solicitudService.documentoSolicitud.id_tipo_documento;

                  if (file.name.indexOf('.pdf') >= 0) {
                    this.uploadedFile.source = "/assets/pdf_logo.jpeg";
                  } else {
                    this.uploadedFile.source = this.solicitudService.documentoSolicitud.archivo.ubicacion_local + '/' + this.uploadedFile.newName;
                  }

                  this.solicitudService.instanciaDocumentoSolicitud.nombre_archivo = file.newName;
                  this.solicitudService.fileDocumentoSolicitud = file;
                  this.uploadedFiles[0] = this.uploadedFile;
                  this.msgs.push({
                    severity: 'info',
                    summary: 'Documento Solicitud Subido',
                    detail: ''
                  });
                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
                  newName = "doc_".concat(this.solicitudService.instanciaDocumentoCertificado.id, "_") + file.name;
                  file.newName = newName;

                  var _newName$split3 = newName.split('.');

                  var _newName$split4 = _slicedToArray(_newName$split3, 2);

                  file.file = _newName$split4[0];
                  file.ext = _newName$split4[1];
                  this.uploadedFile = file;
                  this.uploadedFile.path = this.solicitudService.documentoCertificado.archivo.ubicacion_local;
                  this.uploadedFile.pathName = this.solicitudService.documentoCertificado.archivo.ubicacion_local + '/' + this.uploadedFile.newName;
                  this.uploadedFile.tipoDocumento = this.solicitudService.documentoCertificado.descripcion;
                  this.uploadedFile.idTipoDocumento = this.solicitudService.documentoCertificado.id_tipo_documento;

                  if (file.name.indexOf('.pdf') >= 0) {
                    this.uploadedFile.source = "/assets/pdf_logo.jpeg";
                  } else {
                    this.uploadedFile.source = this.solicitudService.documentoCertificado.archivo.ubicacion_local + '/' + this.uploadedFile.newName;
                  }

                  this.solicitudService.instanciaDocumentoCertificado.nombre_archivo = file.newName;
                  this.solicitudService.fileDocumentoCertificado = file;
                  this.uploadedFiles[1] = this.uploadedFile;
                  this.msgs.push({
                    severity: 'info',
                    summary: 'Documento Certificado Subido',
                    detail: ''
                  });
                }
              }
            }

            this.solicitudService.archivoSubido = true;
          }
        }
      }, {
        key: "deleteFile",
        value: function deleteFile(idInstanciaDocumento) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
            var _this = this;

            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.archivoService["delete"](idInstanciaDocumento).subscribe(function (res) {
                      var response = res;

                      var deleteDocument = _this.uploadedFiles.find(function (param) {
                        return param.idTipoDocumento == _this.solicitudService.documentoSolicitud.id_tipo_documento;
                      });

                      var deleteDocumentIndex = _this.uploadedFiles.findIndex(function (param) {
                        return param.idTipoDocumento == _this.solicitudService.documentoSolicitud.id_tipo_documento;
                      });

                      delete _this.uploadedFiles[deleteDocumentIndex];
                      _this.solicitudService.instanciaDocumentoSolicitud = response.data;
                      _this.solicitudService.instanciaDocumentoSolicitud.fecha_emision = new Date(_this.solicitudService.instanciaDocumentoSolicitud.fecha_emision + '');
                      _this.solicitudService.instanciaDocumentoSolicitud.fecha_fin_vigencia = new Date(_this.solicitudService.instanciaDocumentoSolicitud.fecha_fin_vigencia + '');
                      _this.solicitudService.instanciaDocumentoSolicitud.fecha_inicio_vigencia = new Date(_this.solicitudService.instanciaDocumentoSolicitud.fecha_inicio_vigencia + '');
                    });

                  case 2:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "changeFile",
        value: function changeFile(idInstanciaPoliza) {}
      }, {
        key: "validacionGeneralArchivos",
        value: function validacionGeneralArchivos() {}
      }]);

      return ArchivosComponent;
    }();

    ArchivosComponent.ɵfac = function ArchivosComponent_Factory(t) {
      return new (t || ArchivosComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_servicios_archivo_service__WEBPACK_IMPORTED_MODULE_3__["ArchivoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_4__["SolicitudService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_servicios_instancia_documento_service__WEBPACK_IMPORTED_MODULE_5__["InstanciaDocumentoService"]));
    };

    ArchivosComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: ArchivosComponent,
      selectors: [["app-archivos"]],
      inputs: {
        uploadedFiles: "uploadedFiles",
        uploadedFile: "uploadedFile"
      },
      decls: 8,
      vars: 6,
      consts: [["legend", "Documentos Adjuntos", "toggleable", "true", 1, "form-group"], [1, "ui-g"], [1, "ui-g-12"], [3, "value"], ["name", "archivos", "chooseLabel", "Elegir Archivo", "cancelLabel", "Cancelar", "accept", ".pdf", "multiple", "multiple", "maxFileSize", "512000", 3, "uploadLabel", "disabled", "url", "onUpload"], ["pTemplate", "content"], [2, "border", "0px", "width", "100%"], [4, "ngFor", "ngForOf"], [4, "ngIf"], ["alt", "", 2, "width", "25px", "height", "25px", 3, "src"], ["pButton", "", "type", "button", "icon", "ui-icon-delete", 3, "disabled", "click"], ["pButton", "", "type", "button", "icon", "ui-icon-print", 3, "click"]],
      template: function ArchivosComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-fieldset", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "p-growl", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "p-fileUpload", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onUpload", function ArchivosComponent_Template_p_fileUpload_onUpload_4_listener($event) {
            return ctx.onUpload($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Los archivos PDF deben tener maximo 500kb. (medio mega) de tama\xF1o");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, ArchivosComponent_ng_template_7_Template, 2, 1, "ng-template", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("value", ctx.msgs);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("uploadLabel", "Subir Archivo")("disabled", !ctx.solicitudService.asegurado || !ctx.solicitudService.asegurado.id || !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](4, _c0, ctx.solicitudService.estadoSolicitado.id).includes(ctx.solicitudService.asegurado.instancia_poliza.id_estado) || !ctx.solicitudService.hasRolCredito)("url", ctx.archivoService.rutaUpload + "?idInstanciaPoliza=" + ctx.solicitudService.asegurado.instancia_poliza.id);
        }
      },
      directives: [primeng__WEBPACK_IMPORTED_MODULE_6__["Fieldset"], primeng__WEBPACK_IMPORTED_MODULE_6__["FileUpload"], primeng_api__WEBPACK_IMPORTED_MODULE_7__["PrimeTemplate"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], primeng__WEBPACK_IMPORTED_MODULE_6__["ButtonDirective"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvY29yZS9jb21wb25lbnRlcy9hcmNoaXZvcy9hcmNoaXZvcy5jb21wb25lbnQuc2NzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ArchivosComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-archivos',
          templateUrl: './archivos.component.html',
          styleUrls: ['./archivos.component.scss']
        }]
      }], function () {
        return [{
          type: _servicios_archivo_service__WEBPACK_IMPORTED_MODULE_3__["ArchivoService"]
        }, {
          type: _servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_4__["SolicitudService"]
        }, {
          type: _servicios_instancia_documento_service__WEBPACK_IMPORTED_MODULE_5__["InstanciaDocumentoService"]
        }];
      }, {
        uploadedFiles: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        uploadedFile: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      });
    })();
    /***/

  },

  /***/
  "../../src/core/componentes/archivos/archivos.module.ts":
  /*!**********************************************************************************************************************************!*\
    !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/core/componentes/archivos/archivos.module.ts ***!
    \**********************************************************************************************************************************/

  /*! exports provided: ArchivosModule */

  /***/
  function srcCoreComponentesArchivosArchivosModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArchivosModule", function () {
      return ArchivosModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var primeng__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! primeng */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng.js");
    /* harmony import */


    var _archivos_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./archivos.component */
    "../../src/core/componentes/archivos/archivos.component.ts");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ngx-toastr */
    "../../node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");

    var ArchivosModule = /*#__PURE__*/_createClass(function ArchivosModule() {
      _classCallCheck(this, ArchivosModule);
    });

    ArchivosModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: ArchivosModule
    });
    ArchivosModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function ArchivosModule_Factory(t) {
        return new (t || ArchivosModule)();
      },
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], primeng__WEBPACK_IMPORTED_MODULE_2__["FileUploadModule"], primeng__WEBPACK_IMPORTED_MODULE_2__["FieldsetModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrModule"].forRoot()], primeng__WEBPACK_IMPORTED_MODULE_2__["FileUploadModule"], primeng__WEBPACK_IMPORTED_MODULE_2__["FieldsetModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ArchivosModule, {
        declarations: [_archivos_component__WEBPACK_IMPORTED_MODULE_3__["ArchivosComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], primeng__WEBPACK_IMPORTED_MODULE_2__["FileUploadModule"], primeng__WEBPACK_IMPORTED_MODULE_2__["FieldsetModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrModule"]],
        exports: [primeng__WEBPACK_IMPORTED_MODULE_2__["FileUploadModule"], _archivos_component__WEBPACK_IMPORTED_MODULE_3__["ArchivosComponent"], primeng__WEBPACK_IMPORTED_MODULE_2__["FieldsetModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ArchivosModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_archivos_component__WEBPACK_IMPORTED_MODULE_3__["ArchivosComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], primeng__WEBPACK_IMPORTED_MODULE_2__["FileUploadModule"], primeng__WEBPACK_IMPORTED_MODULE_2__["FieldsetModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrModule"].forRoot()],
          exports: [primeng__WEBPACK_IMPORTED_MODULE_2__["FileUploadModule"], _archivos_component__WEBPACK_IMPORTED_MODULE_3__["ArchivosComponent"], primeng__WEBPACK_IMPORTED_MODULE_2__["FieldsetModule"]],
          schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "../../src/core/modelos/desgravamen/seg_deudores.ts":
  /*!******************************************************************************************************************************!*\
    !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/core/modelos/desgravamen/seg_deudores.ts ***!
    \******************************************************************************************************************************/

  /*! exports provided: SegDeudores */

  /***/
  function srcCoreModelosDesgravamenSeg_deudoresTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SegDeudores", function () {
      return SegDeudores;
    });

    var SegDeudores = /*#__PURE__*/_createClass(function SegDeudores() {
      _classCallCheck(this, SegDeudores);
    });
    /***/

  },

  /***/
  "../../src/core/servicios/archivo.service.ts":
  /*!***********************************************************************************************************************!*\
    !*** /Users/rafaelgutierrezgaspar/Sites/colibri2020/colibri2020-prime/frontend/src/core/servicios/archivo.service.ts ***!
    \***********************************************************************************************************************/

  /*! exports provided: ArchivoService */

  /***/
  function srcCoreServiciosArchivoServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArchivoService", function () {
      return ArchivoService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "../../src/environments/environment.ts");

    var ArchivoService = /*#__PURE__*/function () {
      function ArchivoService(http) {
        _classCallCheck(this, ArchivoService);

        this.http = http;
        this.URL_API = "".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].URL_API_MASIVOS, "/archivo"); //URL_API: string = "http://localhost:5001/api-corredores-ecofuturo/usuario";

        this.rutaUpload = this.URL_API + '/upload';
      }

      _createClass(ArchivoService, [{
        key: "getCustomer",
        value: function getCustomer(doc_id, ext) {
          return this.http.get(this.URL_API + '/getCustomer/' + doc_id + '/' + ext, {
            withCredentials: true
          });
        }
      }, {
        key: "delete",
        value: function _delete(idInstanciaDocumento) {
          return this.http.get(this.URL_API + '/delete/' + idInstanciaDocumento, {
            withCredentials: true
          });
        }
      }]);

      return ArchivoService;
    }();

    ArchivoService.ɵfac = function ArchivoService_Factory(t) {
      return new (t || ArchivoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
    };

    ArchivoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ArchivoService,
      factory: ArchivoService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ArchivoService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "../crediticios/src/app/app-routing.module.ts":
  /*!****************************************************!*\
    !*** ../crediticios/src/app/app-routing.module.ts ***!
    \****************************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function crediticiosSrcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _eco_accidentes_alta_eco_accidentes_alta_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./eco-accidentes/alta-eco-accidentes/alta-eco-accidentes.component */
    "../crediticios/src/app/eco-accidentes/alta-eco-accidentes/alta-eco-accidentes.component.ts");
    /* harmony import */


    var _eco_accidentes_gestion_eco_accidentes_gestion_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./eco-accidentes/gestion-eco-accidentes/gestion-eco-accidentes.component */
    "../crediticios/src/app/eco-accidentes/gestion-eco-accidentes/gestion-eco-accidentes.component.ts");
    /* harmony import */


    var _desgravamen_gestion_desgravamen_gestion_desgravamen_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./desgravamen/gestion-desgravamen/gestion-desgravamen.component */
    "../crediticios/src/app/desgravamen/gestion-desgravamen/gestion-desgravamen.component.ts");
    /* harmony import */


    var _src_core_componentes_anulacion_anulacion_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../../src/core/componentes/anulacion/anulacion.component */
    "../../src/core/componentes/anulacion/anulacion.component.ts");

    var routes = [{
      path: 'AltaEcoAccidente',
      component: _eco_accidentes_alta_eco_accidentes_alta_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_2__["AltaEcoAccidentesComponent"]
    }, {
      path: 'AltaEcoResguardo',
      component: _eco_accidentes_alta_eco_accidentes_alta_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_2__["AltaEcoAccidentesComponent"]
    }, {
      path: 'AnularEcoAccidente',
      component: _src_core_componentes_anulacion_anulacion_component__WEBPACK_IMPORTED_MODULE_5__["AnulacionComponent"]
    }, {
      path: 'GestionSolicitudesEcoAccidente',
      component: _eco_accidentes_gestion_eco_accidentes_gestion_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_3__["GestionEcoAccidentesComponent"]
    }, {
      path: 'GestionDesgravamen',
      component: _desgravamen_gestion_desgravamen_gestion_desgravamen_component__WEBPACK_IMPORTED_MODULE_4__["GestionDesgravamenComponent"]
    }, {
      path: 'AnularDesgravamen',
      component: _src_core_componentes_anulacion_anulacion_component__WEBPACK_IMPORTED_MODULE_5__["AnulacionComponent"]
    }];

    var AppRoutingModule = /*#__PURE__*/_createClass(function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    });

    AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: AppRoutingModule
    });
    AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function AppRoutingModule_Factory(t) {
        return new (t || AppRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "../crediticios/src/app/app.component.ts":
  /*!***********************************************!*\
    !*** ../crediticios/src/app/app.component.ts ***!
    \***********************************************/

  /*! exports provided: AppComponent */

  /***/
  function crediticiosSrcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var AppComponent = /*#__PURE__*/_createClass(function AppComponent() {
      _classCallCheck(this, AppComponent);

      this.title = 'crediticios';
    });

    AppComponent.ɵfac = function AppComponent_Factory(t) {
      return new (t || AppComponent)();
    };

    AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AppComponent,
      selectors: [["app-root"]],
      decls: 1,
      vars: 0,
      template: function AppComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9qZWN0cy9jcmVkaXRpY2lvcy9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-root',
          templateUrl: './app.component.html',
          styleUrls: ['./app.component.scss']
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "../crediticios/src/app/app.module.ts":
  /*!********************************************!*\
    !*** ../crediticios/src/app/app.module.ts ***!
    \********************************************/

  /*! exports provided: AppModule */

  /***/
  function crediticiosSrcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./app-routing.module */
    "../crediticios/src/app/app-routing.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app.component */
    "../crediticios/src/app/app.component.ts");
    /* harmony import */


    var _eco_accidentes_alta_eco_accidentes_alta_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./eco-accidentes/alta-eco-accidentes/alta-eco-accidentes.component */
    "../crediticios/src/app/eco-accidentes/alta-eco-accidentes/alta-eco-accidentes.component.ts");
    /* harmony import */


    var _eco_accidentes_gestion_eco_accidentes_gestion_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./eco-accidentes/gestion-eco-accidentes/gestion-eco-accidentes.component */
    "../crediticios/src/app/eco-accidentes/gestion-eco-accidentes/gestion-eco-accidentes.component.ts");
    /* harmony import */


    var _desgravamen_gestion_desgravamen_gestion_desgravamen_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./desgravamen/gestion-desgravamen/gestion-desgravamen.component */
    "../crediticios/src/app/desgravamen/gestion-desgravamen/gestion-desgravamen.component.ts");
    /* harmony import */


    var _src_core_componentes_anulacion_anulacion_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../../src/core/componentes/anulacion/anulacion.module */
    "../../src/core/componentes/anulacion/anulacion.module.ts");
    /* harmony import */


    var primeng__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! primeng */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng.js");
    /* harmony import */


    var _src_core_componentes_archivos_archivos_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../../src/core/componentes/archivos/archivos.module */
    "../../src/core/componentes/archivos/archivos.module.ts");
    /* harmony import */


    var _src_core_componentes_beneficiario_beneficiario_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../../src/core/componentes/beneficiario/beneficiario.module */
    "../../src/core/componentes/beneficiario/beneficiario.module.ts");
    /* harmony import */


    var _src_core_componentes_transiciones_transiciones_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../../src/core/componentes/transiciones/transiciones.module */
    "../../src/core/componentes/transiciones/transiciones.module.ts");
    /* harmony import */


    var _src_core_componentes_actualizar_solicitud_actualizar_solicitud_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.module */
    "../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.module.ts");
    /* harmony import */


    var _src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../src/core/servicios/breadcrumb.service */
    "../../src/core/servicios/breadcrumb.service.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/common/http */
    "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _src_core_servicios_loader_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ../../../../src/core/servicios/loader.service */
    "../../src/core/servicios/loader.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/common */
    "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/forms */
    "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

    var AppModule = /*#__PURE__*/_createClass(function AppModule() {
      _classCallCheck(this, AppModule);
    });

    AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: AppModule,
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
    });
    AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function AppModule_Factory(t) {
        return new (t || AppModule)();
      },
      providers: [_src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_12__["BreadcrumbService"], _src_core_servicios_loader_service__WEBPACK_IMPORTED_MODULE_14__["LoaderService"], primeng__WEBPACK_IMPORTED_MODULE_7__["MessageService"]],
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_15__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_16__["FormsModule"], // BrowserModule,
      _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"], _src_core_componentes_anulacion_anulacion_module__WEBPACK_IMPORTED_MODULE_6__["AnulacionModule"], _src_core_componentes_transiciones_transiciones_module__WEBPACK_IMPORTED_MODULE_10__["TransicionesModule"], _src_core_componentes_beneficiario_beneficiario_module__WEBPACK_IMPORTED_MODULE_9__["BeneficiarioModule"], _src_core_componentes_archivos_archivos_module__WEBPACK_IMPORTED_MODULE_8__["ArchivosModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"], _src_core_componentes_actualizar_solicitud_actualizar_solicitud_module__WEBPACK_IMPORTED_MODULE_11__["ActualizarSolicitudModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["DialogModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["ToastModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["TableModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["PanelModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["MessagesModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["FieldsetModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["DropdownModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["BreadcrumbModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppModule, {
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"], _eco_accidentes_alta_eco_accidentes_alta_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_3__["AltaEcoAccidentesComponent"], _eco_accidentes_gestion_eco_accidentes_gestion_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_4__["GestionEcoAccidentesComponent"], _desgravamen_gestion_desgravamen_gestion_desgravamen_component__WEBPACK_IMPORTED_MODULE_5__["GestionDesgravamenComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_15__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_16__["FormsModule"], // BrowserModule,
        _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"], _src_core_componentes_anulacion_anulacion_module__WEBPACK_IMPORTED_MODULE_6__["AnulacionModule"], _src_core_componentes_transiciones_transiciones_module__WEBPACK_IMPORTED_MODULE_10__["TransicionesModule"], _src_core_componentes_beneficiario_beneficiario_module__WEBPACK_IMPORTED_MODULE_9__["BeneficiarioModule"], _src_core_componentes_archivos_archivos_module__WEBPACK_IMPORTED_MODULE_8__["ArchivosModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"], _src_core_componentes_actualizar_solicitud_actualizar_solicitud_module__WEBPACK_IMPORTED_MODULE_11__["ActualizarSolicitudModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["DialogModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["ToastModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["TableModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["PanelModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["MessagesModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["FieldsetModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["DropdownModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["BreadcrumbModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"], _eco_accidentes_alta_eco_accidentes_alta_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_3__["AltaEcoAccidentesComponent"], _eco_accidentes_gestion_eco_accidentes_gestion_eco_accidentes_component__WEBPACK_IMPORTED_MODULE_4__["GestionEcoAccidentesComponent"], _desgravamen_gestion_desgravamen_gestion_desgravamen_component__WEBPACK_IMPORTED_MODULE_5__["GestionDesgravamenComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_15__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_16__["FormsModule"], // BrowserModule,
          _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"], _src_core_componentes_anulacion_anulacion_module__WEBPACK_IMPORTED_MODULE_6__["AnulacionModule"], _src_core_componentes_transiciones_transiciones_module__WEBPACK_IMPORTED_MODULE_10__["TransicionesModule"], _src_core_componentes_beneficiario_beneficiario_module__WEBPACK_IMPORTED_MODULE_9__["BeneficiarioModule"], _src_core_componentes_archivos_archivos_module__WEBPACK_IMPORTED_MODULE_8__["ArchivosModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"], _src_core_componentes_actualizar_solicitud_actualizar_solicitud_module__WEBPACK_IMPORTED_MODULE_11__["ActualizarSolicitudModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["DialogModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["ToastModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["TableModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["PanelModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["MessagesModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["FieldsetModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["DropdownModule"], primeng__WEBPACK_IMPORTED_MODULE_7__["BreadcrumbModule"]],
          providers: [_src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_12__["BreadcrumbService"], _src_core_servicios_loader_service__WEBPACK_IMPORTED_MODULE_14__["LoaderService"], primeng__WEBPACK_IMPORTED_MODULE_7__["MessageService"]],
          bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "../crediticios/src/app/desgravamen/gestion-desgravamen/gestion-desgravamen.component.ts":
  /*!***********************************************************************************************!*\
    !*** ../crediticios/src/app/desgravamen/gestion-desgravamen/gestion-desgravamen.component.ts ***!
    \***********************************************************************************************/

  /*! exports provided: GestionDesgravamenComponent */

  /***/
  function crediticiosSrcAppDesgravamenGestionDesgravamenGestionDesgravamenComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GestionDesgravamenComponent", function () {
      return GestionDesgravamenComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var primeng__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! primeng */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng.js");
    /* harmony import */


    var _src_helpers_util__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../../../src/helpers/util */
    "../../src/helpers/util.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/breadcrumb.service */
    "../../src/core/servicios/breadcrumb.service.ts");
    /* harmony import */


    var _src_core_servicios_parametro_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/parametro.service */
    "../../src/core/servicios/parametro.service.ts");
    /* harmony import */


    var _src_core_servicios_instancia_poliza_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/instancia-poliza.service */
    "../../src/core/servicios/instancia-poliza.service.ts");
    /* harmony import */


    var _src_core_servicios_menu_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/menu.service */
    "../../src/core/servicios/menu.service.ts");
    /* harmony import */


    var _src_core_servicios_reporte_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/reporte.service */
    "../../src/core/servicios/reporte.service.ts");
    /* harmony import */


    var _src_core_servicios_persona_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/persona.service */
    "../../src/core/servicios/persona.service.ts");
    /* harmony import */


    var _src_core_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/solicitud.service */
    "../../src/core/servicios/solicitud.service.ts");
    /* harmony import */


    var _src_core_servicios_sessionStorage_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/sessionStorage.service */
    "../../src/core/servicios/sessionStorage.service.ts");
    /* harmony import */


    var _src_core_servicios_plan_pago_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/plan-pago.service */
    "../../src/core/servicios/plan-pago.service.ts");
    /* harmony import */


    var primeng_utils__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! primeng/utils */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-utils.js");
    /* harmony import */


    var _src_core_modelos_plan_pago__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/plan_pago */
    "../../src/core/modelos/plan_pago.ts");
    /* harmony import */


    var xlsx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! xlsx */
    "../../node_modules/xlsx/xlsx.js");
    /* harmony import */


    var xlsx__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_16__);
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! file-saver */
    "../../node_modules/file-saver/dist/FileSaver.min.js");
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_17__);
    /* harmony import */


    var _src_core_componentes_actualizar_solicitud_actualizar_solicitud_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ../../../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component */
    "../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component.ts");
    /* harmony import */


    var _src_core_modelos_desgravamen_seg_deudores__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/desgravamen/seg_deudores */
    "../../src/core/modelos/desgravamen/seg_deudores.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! @angular/common */
    "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var primeng_api__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! primeng/api */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-api.js");
    /* harmony import */


    var primeng_panel__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! primeng/panel */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-panel.js");
    /* harmony import */


    var primeng_toast__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! primeng/toast */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-toast.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
    /*! @angular/forms */
    "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var primeng_calendar__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
    /*! primeng/calendar */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-calendar.js");

    var _c0 = ["componenteActualizarSolicitud"];

    function GestionDesgravamenComponent_div_0_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function GestionDesgravamenComponent_p_fieldset_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-fieldset", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "p-dropdown", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_4_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r8.objeto.sucursal = $event;
        })("onChange", function GestionDesgravamenComponent_p_fieldset_9_Template_p_dropdown_onChange_4_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r10.filterAgencies($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Sucursal");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "p-dropdown", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_9_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r11.objeto.agencia = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Agencia");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "input", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_input_ngModelChange_14_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r12.objeto.usuario_login = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Usuario");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_input_ngModelChange_19_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r13.objeto.id_deu = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "Id Deudor");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_input_ngModelChange_24_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r14.objeto.nro_documento = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26, "Nro. Sol.");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_input_ngModelChange_29_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r15.objeto.tipo_credito = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](31, "Tipo Credito");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "p-dropdown", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_34_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r16.objeto.tipo_cobertura = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](36, "Tipo Cobertura");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](37, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "p-dropdown", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_39_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r17.objeto.tipo_seguro = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](41, "Tipo Cartera");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](43, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](44, "p-dropdown", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onChange", function GestionDesgravamenComponent_p_fieldset_9_Template_p_dropdown_onChange_44_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r18.selectAllLikeEstados($event);
        })("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_44_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r19.objeto.estado = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](46, "Estado");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "p-dropdown", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_49_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r20.objeto.poliza = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](51, "Poliza");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_input_ngModelChange_54_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r21.objeto.persona_primer_apellido = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](55, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](56, "Apellido Paterno");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](57, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](59, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_input_ngModelChange_59_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r22.objeto.persona_segundo_apellido = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](60, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](61, "Apellido Materno");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](62, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](63, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_input_ngModelChange_64_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r23.objeto.persona_primer_nombre = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](65, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](66, "Nombre");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](67, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](68, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](69, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_9_Template_input_ngModelChange_69_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r24.objeto.persona_doc_id = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](71, "Carnet de identidad");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.Sucursales)("ngModel", ctx_r1.objeto.sucursal)("disabled", ctx_r1.disableFieldSucursal)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.Agencias)("ngModel", ctx_r1.objeto.agencia)("disabled", ctx_r1.disableFieldAgencia)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.usuario_login)("disabled", ctx_r1.disableFieldUsuario);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.id_deu);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.nro_documento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.tipo_credito);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.TiposCobertura)("ngModel", ctx_r1.objeto.tipo_cobertura)("disabled", ctx_r1.disableTipoCobertura)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.TiposSeguro)("ngModel", ctx_r1.objeto.tipo_seguro)("disabled", ctx_r1.disableTipoSeguro)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.Estados)("ngModel", ctx_r1.objeto.estado)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.Polizas)("ngModel", ctx_r1.objeto.poliza)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.persona_primer_apellido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.persona_segundo_apellido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.persona_primer_nombre);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.persona_doc_id);
      }
    }

    function GestionDesgravamenComponent_p_fieldset_10_Template(rf, ctx) {
      if (rf & 1) {
        var _r26 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-fieldset", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "p-dropdown", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_10_Template_p_dropdown_ngModelChange_4_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r26);

          var ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r25.objeto.campo_fecha = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Fechas");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "p-calendar", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_10_Template_p_calendar_ngModelChange_10_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r26);

          var ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r27.objeto.fecha_min = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "p-calendar", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionDesgravamenComponent_p_fieldset_10_Template_p_calendar_ngModelChange_13_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r26);

          var ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r28.objeto.fecha_max = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r2.FiltrosFechas)("ngModel", ctx_r2.objeto.campo_fecha)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r2.objeto.fecha_min)("showIcon", true)("showButtonBar", true)("locale", ctx_r2.util.calendario_es)("dateFormat", "dd/mm/yy");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r2.objeto.fecha_max)("showIcon", true)("showButtonBar", true)("locale", ctx_r2.util.calendario_es)("dateFormat", "dd/mm/yy");
      }
    }

    function GestionDesgravamenComponent_ng_template_17_span_3_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "(solo se mostrar\xE1n los 300 primeros resultados)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function GestionDesgravamenComponent_ng_template_17_Template(rf, ctx) {
      if (rf & 1) {
        var _r31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Resultados ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, GestionDesgravamenComponent_ng_template_17_span_3_Template, 3, 0, "span", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "button", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_ng_template_17_Template_button_click_5_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r31);

          var ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](16);

          return ctx_r30.exportExcel(_r3);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r4.showMoreResultsMessage);
      }
    }

    function GestionDesgravamenComponent_ng_template_18_th_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "th", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "p-sortIcon", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var col_r35 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", col_r35.width);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("pSortableColumn", col_r35.field);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](col_r35.header);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("field", col_r35.field);
      }
    }

    function GestionDesgravamenComponent_ng_template_18_th_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "th", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("input", function GestionDesgravamenComponent_ng_template_18_th_3_Template_input_input_1_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r38);

          var col_r36 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](16);

          return _r3.filter($event.target.value, col_r36.field, "contains");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var col_r36 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngSwitch", col_r36.field);
      }
    }

    function GestionDesgravamenComponent_ng_template_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, GestionDesgravamenComponent_ng_template_18_th_1_Template, 4, 5, "th", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, GestionDesgravamenComponent_ng_template_18_th_3_Template, 2, 1, "th", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var columns_r32 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", columns_r32);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", columns_r32);
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_0_td_31_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", instancia_poliza_r39["Deu_RespPositivas"] == "0" ? "resp-info" : "resp-danger");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Deu_RespPositivas"] == 0 ? "No" : "SI");
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_0_td_34_button_2_Template(rf, ctx) {
      if (rf & 1) {
        var _r50 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_ng_template_19_tr_0_td_34_button_2_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r50);

          var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;

          var ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r48.solicitudService.ImprimirDocumentosCrediticios(instancia_poliza_r39.id_deu, instancia_poliza_r39.id_documento_version);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_0_td_34_Template(rf, ctx) {
      if (rf & 1) {
        var _r53 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_ng_template_19_tr_0_td_34_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r53);

          var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

          var ctx_r51 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r51.editSolicitudDesgravamen(instancia_poliza_r39.id_sol);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, GestionDesgravamenComponent_ng_template_19_tr_0_td_34_button_2_Template, 1, 0, "button", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

        var ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("icon", "ui-icon-", ctx_r45.getIconEditAdd(instancia_poliza_r39), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("title", ctx_r45.getWordEditAdd(instancia_poliza_r39));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r45.btnEditar);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r45.showBtnImprimir(instancia_poliza_r39));
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_0_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "td", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "td", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](31, GestionDesgravamenComponent_ng_template_19_tr_0_td_31_Template, 2, 2, "td", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](34, GestionDesgravamenComponent_ng_template_19_tr_0_td_34_Template, 3, 4, "td", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        var ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Nro_Sol"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["id_deu"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Fecha_Solicitud"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Fecha_Resolucion"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Poliza"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["CI"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Asegurado"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r41.setEdadClass(instancia_poliza_r39));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Edad"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Sol_MonedaSol"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Sol_MontoSol"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Estado"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Incluido"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", instancia_poliza_r39["Deu_FactorImc"] == "danger" ? "factor-imc-danger" : instancia_poliza_r39["Deu_FactorImc"] == "warning" ? "factor-imc-warning" : instancia_poliza_r39["Deu_FactorImc"] == "good" ? "factor-imc-good" : "factor-imc-info");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Deu_Imc"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Deu_MontoActAcumVerifSum"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Deu_MontoSolAcumVerifSum"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", instancia_poliza_r39.id_deu);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["usuario"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", instancia_poliza_r39.id_deu);
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_1_td_31_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", instancia_poliza_r39["Deu_RespPositivas"] == "0" ? "resp-info" : "resp-danger");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Deu_RespPositivas"] == 0 ? "No" : "SI");
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_1_td_34_button_2_Template(rf, ctx) {
      if (rf & 1) {
        var _r62 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_ng_template_19_tr_1_td_34_button_2_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r62);

          var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;

          var ctx_r60 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r60.solicitudService.ImprimirDocumentosCrediticios(instancia_poliza_r39.id_deu, instancia_poliza_r39.id_documento_version);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_1_td_34_Template(rf, ctx) {
      if (rf & 1) {
        var _r65 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_ng_template_19_tr_1_td_34_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r65);

          var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

          var ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r63.editSolicitudDesgravamen(instancia_poliza_r39.id_sol);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, GestionDesgravamenComponent_ng_template_19_tr_1_td_34_button_2_Template, 1, 0, "button", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

        var ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("icon", "ui-icon-", ctx_r57.getIconEditAdd(instancia_poliza_r39), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("title", ctx_r57.getWordEditAdd(instancia_poliza_r39));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r57.btnEditar);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r57.showBtnImprimir(instancia_poliza_r39));
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "td", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "td", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](31, GestionDesgravamenComponent_ng_template_19_tr_1_td_31_Template, 2, 2, "td", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](34, GestionDesgravamenComponent_ng_template_19_tr_1_td_34_Template, 3, 4, "td", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        var ctx_r42 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Nro_Sol"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["id_deu"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Fecha_Solicitud"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Fecha_Resolucion"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Poliza"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["CI"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Asegurado"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r42.setEdadClass(instancia_poliza_r39));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Edad"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Sol_MonedaSol"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Sol_MontoSol"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Estado"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Incluido"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", instancia_poliza_r39["Deu_FactorK"] == "danger" ? "factor-k-danger" : instancia_poliza_r39["Deu_FactorK"] == "warning" ? "factor-k-warning" : instancia_poliza_r39["Deu_FactorK"] == "good" ? "factor-k-good" : "factor-k-info");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Deu_K"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Deu_MontoActAcumVerifSum"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Deu_MontoSolAcumVerifSum"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", instancia_poliza_r39.id_deu);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["usuario"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", instancia_poliza_r39.id_deu);
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_2_td_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Nro"]);
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_2_td_30_button_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r74 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_ng_template_19_tr_2_td_30_button_3_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r74);

          var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;

          var ctx_r72 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r72.solicitudService.ImprimirDocumentosCrediticios(instancia_poliza_r39.id_deu, instancia_poliza_r39.id_documento_version);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_2_td_30_Template(rf, ctx) {
      if (rf & 1) {
        var _r77 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_ng_template_19_tr_2_td_30_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r77);

          var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

          var ctx_r75 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r75.editSolicitudDesgravamen(instancia_poliza_r39.id_sol);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "button", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_ng_template_19_tr_2_td_30_Template_button_click_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r77);

          var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

          var ctx_r78 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r78.soporteSolicitudDesgravamen(instancia_poliza_r39.id_sol);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, GestionDesgravamenComponent_ng_template_19_tr_2_td_30_button_3_Template, 1, 0, "button", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

        var ctx_r69 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("icon", "ui-icon-", ctx_r69.getIconEditAdd(instancia_poliza_r39), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("title", ctx_r69.getWordEditAdd(instancia_poliza_r39));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r69.btnEditar);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r69.btnSoporte);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r69.showBtnImprimir(instancia_poliza_r39));
      }
    }

    function GestionDesgravamenComponent_ng_template_19_tr_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, GestionDesgravamenComponent_ng_template_19_tr_2_td_1_Template, 2, 1, "td", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "td", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](30, GestionDesgravamenComponent_ng_template_19_tr_2_td_30_Template, 4, 5, "td", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", instancia_poliza_r39.id_deu);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Nro_Sol"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["id_deu"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Poliza"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["CI"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Sucursal"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Agencia"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Tipo_Cobertura"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Tipo_Seguro"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Estado"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Incluido"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Fecha_Solicitud"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Fecha_Resolucion"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["Asegurado"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r39["usuario"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", instancia_poliza_r39.id_deu);
      }
    }

    function GestionDesgravamenComponent_ng_template_19_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, GestionDesgravamenComponent_ng_template_19_tr_0_Template, 35, 20, "tr", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, GestionDesgravamenComponent_ng_template_19_tr_1_Template, 35, 20, "tr", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, GestionDesgravamenComponent_ng_template_19_tr_2_Template, 31, 16, "tr", 41);
      }

      if (rf & 2) {
        var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r6.esAseguradoraLbcSuscriptor);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r6.esAseguradoraAlianza);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r6.esAseguradora);
      }
    }

    function GestionDesgravamenComponent_app_actualizar_solicitud_21_Template(rf, ctx) {
      if (rf & 1) {
        var _r84 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-actualizar-solicitud", 58, 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("setAsegurado", function GestionDesgravamenComponent_app_actualizar_solicitud_21_Template_app_actualizar_solicitud_setAsegurado_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r84);

          var ctx_r83 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r83.solicitudService.asegurado = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    var _c1 = function _c1() {
      return [10, 25, 50];
    };

    var _c2 = function _c2() {
      return {
        marginTop: "70px"
      };
    };

    var GestionDesgravamenComponent = /*#__PURE__*/function () {
      function GestionDesgravamenComponent(params, breadcrumbService, parametrosService, instanciaPolizaService, router, menuService, reporteService, service, personaService, solicitudService, sessionStorageService, planPagoService) {
        _classCallCheck(this, GestionDesgravamenComponent);

        this.params = params;
        this.breadcrumbService = breadcrumbService;
        this.parametrosService = parametrosService;
        this.instanciaPolizaService = instanciaPolizaService;
        this.router = router;
        this.menuService = menuService;
        this.reporteService = reporteService;
        this.service = service;
        this.personaService = personaService;
        this.solicitudService = solicitudService;
        this.sessionStorageService = sessionStorageService;
        this.planPagoService = planPagoService;
        this.color = 'primary';
        this.mode = 'indeterminate';
        this.value = 1;
        this.parametros = [];
        this.instancias_polizas = [];
        this.instancias = []; //poliza = new Poliza();

        this.segSolicitudes = [];
        this.segDeudores = [];
        this.objeto = {
          agencia_required: true,
          sucursal_required: true,
          estado: null,
          poliza: null,
          agencia: null,
          sucursal: null,
          usuario_login: '',
          persona_primer_apellido: '',
          persona_segundo_apellido: '',
          persona_primer_nombre: '',
          persona_doc_id: '',
          persona_doc_id_ext: '',
          id_poliza: null,
          campo_fecha: null,
          fecha_min: null,
          fecha_max: null,
          tipo_cobertura: '',
          tipo_seguro: '',
          id_deu: '',
          nro_documento: '',
          tipo_credito: '',
          usuarioLogin: null,
          first: 0,
          rows: 10
        };
        this.showFiltros = true;
        this.FiltrosFechas = [];
        this.ProcedenciaCI = [];
        this.Agencias = [];
        this.paramAgencias = [];
        this.paramSucursales = [];
        this.Sucursales = [];
        this.Estados = [];
        this.EstadosAll = [];
        this.Polizas = [];
        this.EstadosFiltered = [];
        this.TiposCobertura = [];
        this.TiposSeguro = [];
        this.util = new _src_helpers_util__WEBPACK_IMPORTED_MODULE_3__["Util"]();
        this.btnEditar = false;
        this.btnSoporte = false;
        this.btnVer1 = false;
        this.btnVer2 = false;
        this.btnEmitir = false;
        this.disableFieldUsuario = false;
        this.disableFieldAgencia = false;
        this.disableFieldSucursal = false;
        this.disableTipoCobertura = false;
        this.disableTipoSeguro = false;
        this.displayExedioResultados = false;
        this.PanelFiltrosColapsed = false;
        this.showMoreResultsMessage = false;
        this.esAseguradora = false;
        this.esAseguradoraAlianza = false;
        this.esAseguradoraLbcSuscriptor = false;
        this.rowsPerPage = 0;
        this.numRecords = 0;
        this.totalRows = 0;
        var userInfo = this.sessionStorageService.getItemSync('userInfo');
        var parametros = this.sessionStorageService.getItemSync('parametros');
        this.usuarioLogin = userInfo;
        this.parametrosRuteo = parametros;
        this.componentesInvisibles = this.parametrosRuteo.ComponentesInvisibles ? JSON.parse(this.parametrosRuteo.ComponentesInvisibles) : this.parametrosRuteo.ComponentesInvisibles;
        this.ruta = this.parametrosRuteo.ruta;
        this.breadcrumbService.setItems([{
          label: this.ruta
        }]);
        this.cols = [{
          field: 'Nro',
          header: 'Nro',
          width: '5%'
        }, {
          field: 'Nro_Sol',
          header: 'Nro. Sol.',
          width: '5%'
        }, {
          field: 'id_deu',
          header: 'Id Deudor',
          width: '5%'
        }, {
          field: 'Deu_Poliza',
          header: 'Poliza',
          width: '5%'
        }, {
          field: 'ci',
          header: 'CI',
          width: '7%'
        }, {
          field: 'Sucursal',
          header: 'Sucursal',
          width: '8%'
        }, {
          field: 'Agencia',
          header: 'Agencia',
          width: '10%'
        }, {
          field: 'Tipo_Cobertura',
          header: 'Tipo Cobertura',
          width: '6%'
        }, {
          field: 'Tipo_Seguro',
          header: 'Tipo Cartera',
          width: '6%'
        }, {
          field: 'Estado',
          header: 'Estado Sol.',
          width: '6%'
        }, {
          field: 'Incluido',
          header: 'Incluido',
          width: '5%'
        }, {
          field: 'Fecha_Solicitud',
          header: 'Fecha Solicitud',
          width: '9%'
        }, {
          field: 'Fecha_Resolucion',
          header: 'Fecha Resolucion',
          width: '9%'
        }, {
          field: 'Asegurado',
          header: 'Asegurado',
          width: '15%'
        }, {
          field: 'usuario',
          header: 'usuario',
          width: '8%'
        }, {
          field: null,
          header: 'Acciones',
          width: '10%'
        }];
        this.colsAseguradoraLbcSuscriptor = [{
          field: 'Nro_Sol',
          header: 'Nro. Sol.',
          width: '5%'
        }, {
          field: 'id_deu',
          header: 'Id Deudor',
          width: '5%'
        }, {
          field: 'Fecha_Solicitud',
          header: 'Fecha Solicitud',
          width: '6%'
        }, {
          field: 'Fecha_Resolucion',
          header: 'Fecha Resolucion',
          width: '6%'
        }, {
          field: 'Deu_Poliza',
          header: 'Poliza',
          width: '5%'
        }, {
          field: 'ci',
          header: 'CI',
          width: '7%'
        }, {
          field: 'Asegurado',
          header: 'Asegurado',
          width: '6%'
        }, {
          field: 'Edad',
          header: 'Edad',
          width: '4%'
        }, {
          field: 'Sol_MonedaSol',
          header: 'Moneda',
          width: '5%'
        }, {
          field: 'Sol_MontoSol',
          header: 'Monto Solicitado',
          width: '5%'
        }, {
          field: 'Estado',
          header: 'Estado Sol.',
          width: '6%'
        }, {
          field: 'Incluido',
          header: 'Incluido',
          width: '4%'
        }, {
          field: 'Deu_Imc',
          header: 'IMC',
          width: '7%'
        }, {
          field: 'Deu_MontoActAcumVerifSum',
          header: 'Saldo',
          width: '6%'
        }, {
          field: 'Deu_MontoSolAcumVerifSum',
          header: 'Monto a Evaluar',
          width: '8%'
        }, {
          field: 'Deu_RespPositivas',
          header: 'Resp. Positivas',
          width: '6%'
        }, {
          field: 'usuario',
          header: 'usuario',
          width: '6%'
        }, {
          field: null,
          header: 'Acciones',
          width: '6%'
        }];
        this.colsAseguradoraAlianza = [{
          field: 'Nro_Sol',
          header: 'Nro. Sol.',
          width: '5%'
        }, {
          field: 'id_deu',
          header: 'Id Deudor',
          width: '5%'
        }, {
          field: 'Fecha_Solicitud',
          header: 'Fecha Solicitud',
          width: '6%'
        }, {
          field: 'Fecha_Resolucion',
          header: 'Fecha Resolucion',
          width: '6%'
        }, {
          field: 'Deu_Poliza',
          header: 'Poliza',
          width: '5%'
        }, {
          field: 'ci',
          header: 'CI',
          width: '7%'
        }, {
          field: 'Asegurado',
          header: 'Asegurado',
          width: '6%'
        }, {
          field: 'Edad',
          header: 'Edad',
          width: '4%'
        }, {
          field: 'Sol_MonedaSol',
          header: 'Moneda',
          width: '5%'
        }, {
          field: 'Sol_MontoSol',
          header: 'Monto Solicitado',
          width: '5%'
        }, {
          field: 'Estado',
          header: 'Estado Sol.',
          width: '6%'
        }, {
          field: 'Incluido',
          header: 'Incluido',
          width: '4%'
        }, {
          field: 'Deu_FactorK',
          header: 'Factor K',
          width: '7%'
        }, {
          field: 'Deu_MontoActAcumVerifSum',
          header: 'Saldo',
          width: '6%'
        }, {
          field: 'Deu_MontoSolAcumVerifSum',
          header: 'Monto a Evaluar',
          width: '8%'
        }, {
          field: 'Deu_RespPositivas',
          header: 'Resp. Positivas',
          width: '6%'
        }, {
          field: 'usuario',
          header: 'usuario',
          width: '6%'
        }, {
          field: null,
          header: 'Acciones',
          width: '6%'
        }];

        primeng_utils__WEBPACK_IMPORTED_MODULE_14__["FilterUtils"]['custom'] = function (value, filter) {
          if (filter === undefined || filter === null || filter.trim() === '') {
            return true;
          }

          if (value === undefined || value === null) {
            return false;
          }

          return parseInt(filter) > value;
        };
      }

      _createClass(GestionDesgravamenComponent, [{
        key: "ValidarComponentesInvisible",
        value: function ValidarComponentesInvisible(id) {
          if (this.componentesInvisibles.find(function (params) {
            return params.codigo === id && params.estado === 'I';
          })) {
            return false;
          } else {
            return true;
          }
        }
      }, {
        key: "getIconEditAdd",
        value: function getIconEditAdd(instancia) {
          if (instancia.Estado) {
            if (this.usuarioLogin.usuarioRoles.find(function (param) {
              return [20, 21, 22, 23, 24, 25].includes(parseInt(param.id + ''));
            })) {
              return 'remove-red-eye';
            } else {
              return ['A', 'C'].includes(instancia.Estado.substring(0, 1)) ? 'remove-red-eye' : 'edit';
            }
          }
        }
      }, {
        key: "getWordEditAdd",
        value: function getWordEditAdd(instancia) {
          if (instancia.Estado) {
            if (this.usuarioLogin.usuarioRoles.find(function (param) {
              return [20, 21, 22, 23, 24, 25].includes(parseInt(param.id + ''));
            })) {
              return 'Ver';
            } else {
              return ['A', 'C'].includes(instancia.Estado.substring(0, 1)) ? 'Ver' : 'Editar';
            }
          }
        }
      }, {
        key: "showBtnImprimir",
        value: function showBtnImprimir(seg_deudor) {
          var estadosAprobados = this.EstadosAll.filter(function (param) {
            return ['A'].includes(param.Est_Codigo.substring(0, 1));
          });

          if (seg_deudor.id_documento_version && estadosAprobados.find(function (param) {
            return param.Est_Codigo.substring(0, 1) == seg_deudor.deu_solicitud.Sol_EstadoSol.substring(0, 1);
          }) && seg_deudor.Deu_Incluido.toUpperCase() == 'S') {
            return true;
          } else {
            return false;
          }
        }
      }, {
        key: "setEdadClass",
        value: function setEdadClass(instancia) {
          return parseInt(instancia['Edad']) < 71 ? 'resp-info' : 'resp-danger';
        }
      }, {
        key: "ngAfterViewChecked",
        value: function ngAfterViewChecked() {
          /*if (this.solicitudService.asegurado.instancia_poliza.id_estado + '' === '59') {
            if (this.poliza.instancia_polizas.length > 0 && (this.index_instancia_poliza || this.index_instancia_poliza===0)) {
              this.poliza.instancia_polizas[this.index_instancia_poliza].id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
              this.instancias[this.index_instancia_poliza].id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
              this.instancias[this.index_instancia_poliza]['Estado'] = this.devuelveValorParametro(this.instancias[this.index_instancia_poliza].id_estado);
            }
          }*/
        }
      }, {
        key: "ValidarComponentesEditable",
        value: function ValidarComponentesEditable(id) {
          if (this.componentesEditables.find(function (params) {
            return params.codigo === id && params.estado === 'NE';
          })) {
            return true;
          } else {
            return false;
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
            var _this2 = this;

            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    this.solicitudService.constructComponent(function () {
                      _this2.breadcrumbService.setItems([{
                        label: _this2.solicitudService.ruta
                      }]);

                      _this2.objeto = _this2.solicitudService.parametrosRuteo.parametro_ruteo && _this2.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? _this2.solicitudService.parametrosRuteo.parametro_ruteo.filtros : _this2.objeto;
                      _this2.objeto.id_poliza = parseInt(_this2.solicitudService.parametrosRuteo.parametro_vista + '') ? parseInt(_this2.solicitudService.parametrosRuteo.parametro_vista + '') : _this2.solicitudService.id_poliza;
                      _this2.solicitudService.product = _this2.solicitudService.ruta;
                      _this2.solicitudService.isInAltaSolicitud = false;
                      _this2.solicitudService.isLoadingAgain = false;

                      _this2.GetAllParametrosByIdDiccionarios(function () {
                        if (_this2.parametrosRuteo.parametro_ruteo !== null) {
                          _this2.solicituddes = _this2.parametrosRuteo.parametro_ruteo;

                          _this2.listaAllByIds();

                          _this2.showFiltros = false;
                        } else {
                          _this2.solicitudService.isLoadingAgain = false;
                        }

                        if (_this2.parametrosRuteo.parametro_vista !== null) {
                          _this2.id_poliza = _this2.parametrosRuteo.parametro_vista;
                        }

                        if (_this2.usuarioLogin) {
                          //this.disableFieldUsuario = true;
                          if (_this2.usuarioLogin.usuario_banco) {
                            _this2.disableFieldSucursal = true;
                            _this2.disableFieldAgencia = true;
                          } // 5   Oficial de Credito


                          if (_this2.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 5;
                          })) {
                            _this2.objeto.usuario_login = _this2.usuarioLogin.usuario_login;
                            _this2.objeto.sucursal = _this2.usuarioLogin.usuario_banco ? _this2.usuarioLogin.usuario_banco.us_sucursal + '' : ''; //this.objeto.agencia = this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_oficina+'' : '';

                            _this2.disableFieldUsuario = true;
                            _this2.disableFieldSucursal = true;
                            _this2.disableFieldAgencia = true;
                            _this2.esAseguradora = false;
                            _this2.esAseguradoraLbcSuscriptor = false;
                            _this2.esAseguradoraAlianza = false;
                          } // 20	aseguradora	Aseguradora Alianza


                          if (_this2.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 20;
                          })) {
                            _this2.disableFieldUsuario = false;
                            _this2.disableFieldSucursal = false;
                            _this2.disableFieldAgencia = false;
                            _this2.disableTipoSeguro = false;
                            _this2.disableTipoCobertura = false;
                            _this2.esAseguradora = true;
                            _this2.esAseguradoraAlianza = true;
                            _this2.esAseguradoraLbcSuscriptor = false; // this.objeto.tipo_seguro = this.TiposSeguro[1].value;
                            // this.objeto.tipo_cobertura = this.TiposCobertura[2].value;
                          } // 21	aseguradora_no_licitada	LBC


                          if (_this2.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 21;
                          })) {
                            _this2.disableFieldUsuario = false;
                            _this2.disableFieldSucursal = false;
                            _this2.disableFieldAgencia = false;
                            _this2.disableTipoSeguro = false;
                            _this2.disableTipoCobertura = false;
                            _this2.esAseguradora = true;
                            _this2.esAseguradoraLbcSuscriptor = true;
                            _this2.esAseguradoraAlianza = false;
                          } // 22	corredora_desgravamen	Corredora Desgravamen


                          if (_this2.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 22;
                          })) {
                            _this2.disableFieldUsuario = false;
                            _this2.disableFieldSucursal = false;
                            _this2.disableFieldAgencia = false;
                            _this2.disableTipoSeguro = false;
                            _this2.disableTipoCobertura = false;
                            _this2.esAseguradora = false;
                            _this2.esAseguradoraLbcSuscriptor = false;
                            _this2.esAseguradoraAlianza = false;
                          } // 23	supervisor_ofcredito	Supervisor General


                          if (_this2.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 23;
                          })) {
                            _this2.disableFieldUsuario = false;
                            _this2.disableFieldSucursal = false;
                            _this2.disableFieldAgencia = false;
                            _this2.esAseguradora = false;
                            _this2.esAseguradoraLbcSuscriptor = false;
                            _this2.esAseguradoraAlianza = false;
                          } // 24	supervisor_ofcredito_sucursal	Supervisor Sucursal


                          if (_this2.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 24;
                          })) {
                            _this2.objeto.sucursal = _this2.usuarioLogin.usuario_banco ? _this2.usuarioLogin.usuario_banco.us_sucursal + '' : '';
                            _this2.disableFieldUsuario = false;
                            _this2.disableFieldSucursal = true;
                            _this2.disableFieldAgencia = false;
                            _this2.esAseguradora = false;
                            _this2.esAseguradoraLbcSuscriptor = false;
                            _this2.esAseguradoraAlianza = false;
                          } // 25	supervisor_ofcredito_agencia	Supervisor Agencia


                          if (_this2.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 25;
                          })) {
                            _this2.objeto.sucursal = _this2.usuarioLogin.usuario_banco ? _this2.usuarioLogin.usuario_banco.us_sucursal + '' : '';
                            _this2.objeto.agencia = _this2.usuarioLogin.usuario_banco ? _this2.usuarioLogin.usuario_banco.us_oficina + '' : '';
                            _this2.disableFieldUsuario = false;
                            _this2.disableFieldSucursal = true;
                            _this2.disableFieldAgencia = true;
                            _this2.esAseguradora = false;
                            _this2.esAseguradoraLbcSuscriptor = false;
                            _this2.esAseguradoraAlianza = false;
                          } // 26	aseguradora LBC Aseguradora LBC Suscriptor


                          if (_this2.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 26;
                          })) {
                            _this2.disableFieldUsuario = false;
                            _this2.disableFieldSucursal = false;
                            _this2.disableFieldAgencia = false;
                            _this2.disableTipoSeguro = false;
                            _this2.disableTipoCobertura = false;
                            _this2.esAseguradora = true;
                            _this2.esAseguradoraLbcSuscriptor = true;
                            _this2.esAseguradoraAlianza = false;
                          }
                        }
                      });
                    });

                  case 1:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "listaAll",
        value: function listaAll(dt) {
          var _this3 = this;

          var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

          if (dt) {
            dt.reset();
          }

          setTimeout(function () {
            if ($(window).width() < 720) {
              $('.ui-table').css('overflow', 'scroll');
              $('.ui-table table').css('width', '400%');
            }
          }, 2000);
          this.instancias = [];

          if (!this.objeto.agencia) {
            this.objeto.agencia_required = false;
          }

          if (!this.objeto.sucursal_required) {
            this.objeto.sucursal_required = false;
          }

          this.solicitudService.isLoadingAgain = true;

          if (this.objeto.campo_fecha && this.objeto.fecha_max) {
            this.objeto.fecha_max.setDate(this.objeto.fecha_max.getDate() + 1);
            this.objeto.fecha_max.setMinutes(this.objeto.fecha_max.getMinutes() - 1);
          }

          this.objeto.usuarioLogin = this.usuarioLogin;

          if (this.totalRows && this.totalRows == this.objeto.first) {
            this.objeto.first = this.objeto.first - this.objeto.rows;
          }

          this.instanciaPolizaService.GetAllByParametrosDesgravamen(this.objeto).subscribe(function (res) {
            var response = res;
            _this3.totalRows = response.recordsTotal;
            _this3.objeto.first = response.recordsOffset;
            _this3.objeto.rows = response.recordsFiltered;
            _this3.segDeudores = response.data;

            if (_this3.segDeudores.length >= 300) {
              _this3.showMoreResultsMessage = true;
            } else {
              _this3.showMoreResultsMessage = false;
            }

            _this3.PanelFiltrosColapsed = false;

            if (_this3.segDeudores && _this3.segDeudores.length) {
              if (_this3.segDeudores.length === 0) {
                _this3.service.add({
                  key: 'tst',
                  severity: 'warn',
                  summary: 'Advertencia',
                  detail: 'No se encontro ningun registro'
                });
              } else if (_this3.segDeudores.length >= 300 && !_this3.usuarioLogin.usuarioRoles.find(function (param) {
                return param.id == 5;
              })) {
                _this3.displayExedioResultados = true;
              }

              _this3.segDeudores = _this3.getCars(_this3.segDeudores);
              var lastEmptyRows = _this3.totalRows % _this3.objeto.rows;
              var lastFilledRows = _this3.objeto.rows - lastEmptyRows;
              var rowsAfter = _this3.totalRows - lastEmptyRows;

              for (var i = 0; i < rowsAfter; i++) {
                _this3.segDeudores.push(new _src_core_modelos_desgravamen_seg_deudores__WEBPACK_IMPORTED_MODULE_19__["SegDeudores"]());

                _this3.segDeudores[i].Nro = i + (_this3.objeto.first + 1);
              }

              _this3.rowsPerPage = _this3.objeto.first + _this3.objeto.rows;

              if (_this3.totalRows - _this3.objeto.first == lastFilledRows) {
                _this3.numRecords = lastFilledRows;
              } else {
                _this3.numRecords = _this3.objeto.rows;
              }
            } else {
              _this3.service.add({
                key: 'tst',
                severity: 'warn',
                summary: 'Advertencia',
                detail: 'No se encontro ningun registro'
              });
            }

            _this3.solicitudService.isLoadingAgain = false;

            if (typeof callback == 'function') {
              callback();
            }
          }, function (err) {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
              _this3.router.navigate(['login']);
            } else {
              console.log(err);
            }
          });
        }
      }, {
        key: "listaAllByIds",
        value: function listaAllByIds() {
          var _this4 = this;

          this.instanciaPolizaService.GetAllByIdsDesgravamen({
            ids: this.solicituddes
          }).subscribe(function (res) {
            var response = res;
            _this4.segDeudores = response.data;

            if (_this4.segDeudores !== undefined) {
              if (_this4.segDeudores.length === 0) {
                _this4.service.add({
                  key: 'tst',
                  severity: 'warn',
                  summary: 'Advertencia',
                  detail: 'No se encontro ningun registro'
                });
              }

              _this4.segDeudores = _this4.getCars(_this4.segDeudores);
            } else {
              _this4.service.add({
                key: 'tst',
                severity: 'warn',
                summary: 'Advertencia',
                detail: 'No se encontro ningun registro'
              });
            }
          }, function (err) {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
              _this4.router.navigate(['login']);
            } else {
              console.log(err);
            }
          });
        }
      }, {
        key: "devuelveValorParametro",
        value: function devuelveValorParametro(id) {
          var parametro = this.parametros.find(function (parametro) {
            return parametro.id === id;
          });

          if (parametro) {
            return parametro.parametro_descripcion;
          } else {
            return '';
          }
        }
      }, {
        key: "devuelveValorParametroByCod",
        value: function devuelveValorParametroByCod(id) {
          var parametro = this.parametros.find(function (parametro) {
            return parametro.parametro_cod === id;
          });

          if (parametro) {
            return parametro.parametro_descripcion;
          } else {
            return '';
          }
        }
      }, {
        key: "devuelveValorAgenciaSucursal",
        value: function devuelveValorAgenciaSucursal(agencia) {
          if (agencia) {
            var parametro = this.parametros.find(function (parametro) {
              return parametro.parametro_cod === agencia.valor;
            });

            if (parametro) {
              return parametro.parametro_descripcion;
            } else {
              return '';
            }
          } else {
            return '';
          }
        }
      }, {
        key: "GetAllParametrosByIdDiccionarios",
        value: function GetAllParametrosByIdDiccionarios(callback) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee3() {
            var _this5 = this;

            var ids;
            return _regeneratorRuntime().wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.parametrosService.GetAllEstadosDesgravamen().subscribe(function (res) {
                      var response = res;

                      _this5.Estados.push({
                        label: "Seleccione Estado",
                        value: null
                      });

                      _this5.EstadosFiltered.push({
                        label: "Seleccione Estado",
                        value: null
                      });

                      var estados = response.data.estados;
                      _this5.EstadosAll = response.data.estadosAll;

                      if (estados) {
                        for (var i = 0; i < estados.length; i++) {
                          var estado = estados[i];

                          if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 5;
                          })) {
                            // 5    Oficial de Credito
                            _this5.Estados.push({
                              label: estado.Est_Grupo,
                              value: estado.Est_Codigo
                            });
                          } else if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 20;
                          })) {
                            // 20	aseguradora_no_licitada	Aseguradora No Licitada Alianza
                            if (!['P'].includes(estado.Est_Codigo && estado.Est_Codigo.toUpperCase())) {
                              _this5.Estados.push({
                                label: estado.Est_Grupo,
                                value: estado.Est_Codigo
                              });
                            }
                          } else if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 21;
                          })) {
                            // 21	aseguradora_no_licitada	Aseguradora No Licitada Boliviana Ciacruz
                            if (!['P'].includes(estado.Est_Codigo && estado.Est_Codigo.toUpperCase())) {
                              _this5.Estados.push({
                                label: estado.Est_Grupo,
                                value: estado.Est_Codigo
                              });
                            }
                          } else if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 22;
                          })) {
                            // 22	corredora_desgravamen	Corredora Desgravamen
                            _this5.Estados.push({
                              label: estado.Est_Grupo,
                              value: estado.Est_Codigo
                            });
                          } else if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 23;
                          })) {
                            // 23	supervisor_ofcredito	Supervisor General
                            _this5.Estados.push({
                              label: estado.Est_Grupo,
                              value: estado.Est_Codigo
                            });
                          } else if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 24;
                          })) {
                            // 24	supervisor_ofcredito_sucursal	Supervisor Sucursal
                            _this5.Estados.push({
                              label: estado.Est_Grupo,
                              value: estado.Est_Codigo
                            });
                          } else if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 25;
                          })) {
                            // 25	supervisor_ofcredito_agencia	Supervisor Agencia
                            _this5.Estados.push({
                              label: estado.Est_Grupo,
                              value: estado.Est_Codigo
                            });
                          } else if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 26;
                          })) {
                            // 26	supervisor_ofcredito	Supervisor General Suscriptor
                            if (['A', 'C', 'D', 'O', 'R'].includes(estado.Est_Codigo && estado.Est_Codigo.toUpperCase().substring(0, 1))) {
                              _this5.Estados.push({
                                label: estado.Est_Grupo,
                                value: estado.Est_Codigo
                              });
                            }
                          }
                        }
                      }
                    });

                  case 2:
                    _context3.next = 4;
                    return this.parametrosService.GetAllPolizasDesgravamen().subscribe(function (res) {
                      var response = res;

                      _this5.Polizas.push({
                        label: "Seleccione Poliza",
                        value: null
                      });

                      if (response.data) {
                        for (var i = 0; i < response.data.length; i++) {
                          var polizaDesgravamen = response.data[i]; // 5    Oficial de Credito

                          if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 5;
                          })) {
                            // if (['1/2018','2/2018','80268'].includes(polizaDesgravamen.Deu_Poliza)){
                            _this5.Polizas.push({
                              label: polizaDesgravamen.Deu_Poliza,
                              value: polizaDesgravamen.Deu_Poliza
                            }); // }

                          } // 20	aseguradora_no_licitada	Aseguradora No Licitada Alianza


                          if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 20;
                          })) {
                            if (['1/2018', '2/2018', '80268'].includes(polizaDesgravamen.Deu_Poliza)) {
                              _this5.Polizas.push({
                                label: polizaDesgravamen.Deu_Poliza,
                                value: polizaDesgravamen.Deu_Poliza
                              });
                            }
                          } // 21	aseguradora_no_licitada	Aseguradora No Licitada Boliviana Ciacruz


                          if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 21;
                          })) {
                            if (['20002', '20007', '20001', '20006'].includes(polizaDesgravamen.Deu_Poliza)) {
                              _this5.Polizas.push({
                                label: polizaDesgravamen.Deu_Poliza,
                                value: polizaDesgravamen.Deu_Poliza
                              });
                            }
                          } // 22	corredora_desgravamen	Corredora Desgravamen


                          if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 22;
                          })) {
                            if (['1/2018', '2/2018', '80268', '20002', '20007', '20001', '20006'].includes(polizaDesgravamen.Deu_Poliza)) {
                              _this5.Polizas.push({
                                label: polizaDesgravamen.Deu_Poliza,
                                value: polizaDesgravamen.Deu_Poliza
                              });
                            }
                          } // 23	supervisor_ofcredito	Supervisor General


                          if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 23;
                          })) {
                            // if (['1/2018','2/2018','3/2018'].includes(polizaDesgravamen.Deu_Poliza)){
                            _this5.Polizas.push({
                              label: polizaDesgravamen.Deu_Poliza,
                              value: polizaDesgravamen.Deu_Poliza
                            }); // }

                          } // 24	supervisor_ofcredito_sucursal	Supervisor Sucursal


                          if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 24;
                          })) {
                            // if (['1/2018','2/2018','3/2018'].includes(polizaDesgravamen.Deu_Poliza)){
                            _this5.Polizas.push({
                              label: polizaDesgravamen.Deu_Poliza,
                              value: polizaDesgravamen.Deu_Poliza
                            }); // }

                          } // 25	supervisor_ofcredito_agencia	Supervisor Agencia


                          if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 25;
                          })) {
                            // if (['1/2018','2/2018','3/2018'].includes(polizaDesgravamen.Deu_Poliza)){
                            _this5.Polizas.push({
                              label: polizaDesgravamen.Deu_Poliza,
                              value: polizaDesgravamen.Deu_Poliza
                            }); // }

                          } // 26	aseguradora_no_licitada	Aseguradora No Licitada Boliviana Ciacruz Suscriptor


                          if (_this5.usuarioLogin.usuarioRoles.find(function (param) {
                            return param.id == 26;
                          })) {
                            // if (['1/2018','2/2018','3/2018'].includes(polizaDesgravamen.Deu_Poliza)){
                            _this5.Polizas.push({
                              label: polizaDesgravamen.Deu_Poliza,
                              value: polizaDesgravamen.Deu_Poliza
                            }); // }

                          }
                        }
                      }
                    });

                  case 4:
                    ids = [11, 1, 18, 38, 40, 54, 62, 63, 64];
                    _context3.next = 7;
                    return this.parametrosService.GetAllParametrosByIdDiccionarios(ids).subscribe(function (res) {
                      var response = res;
                      _this5.parametros = response.data;

                      _this5.ProcedenciaCI.push({
                        label: "Seleccione Procedencia",
                        value: null
                      });

                      _this5.parametros.filter(function (param) {
                        return param.diccionario_id + '' === "18";
                      }).forEach(function (element) {
                        _this5.ProcedenciaCI.push({
                          label: element.parametro_descripcion,
                          value: element.parametro_abreviacion
                        });
                      });

                      _this5.FiltrosFechas.push({
                        label: "Seleccione un tipo de fecha",
                        value: null
                      });

                      _this5.parametros.filter(function (param) {
                        return param.diccionario_id + '' === "64";
                      }).forEach(function (element) {
                        _this5.FiltrosFechas.push({
                          label: element.parametro_descripcion,
                          value: element.parametro_cod
                        });
                      });

                      _this5.Agencias.push({
                        label: "Seleccione Agencia",
                        value: null
                      });

                      _this5.parametros.filter(function (param) {
                        return param.diccionario_id + '' === "40";
                      }).forEach(function (element) {
                        _this5.Agencias.push({
                          label: element.parametro_descripcion,
                          value: element.parametro_cod
                        });

                        _this5.paramAgencias.push(element);
                      });

                      _this5.Sucursales.push({
                        label: "Seleccione Sucursal",
                        value: null
                      });

                      _this5.parametros.filter(function (param) {
                        return param.diccionario_id + '' === "38";
                      }).forEach(function (element) {
                        _this5.Sucursales.push({
                          label: element.parametro_descripcion,
                          value: element.parametro_cod
                        });

                        _this5.paramSucursales.push(element);
                      });

                      _this5.TiposCobertura.push({
                        label: "Seleccione Tipo Cobertura",
                        value: null
                      });

                      _this5.parametros.filter(function (param) {
                        return param.diccionario_id + '' === "62";
                      }).forEach(function (element) {
                        _this5.TiposCobertura.push({
                          label: element.parametro_descripcion,
                          value: element.parametro_cod
                        });
                      });

                      _this5.TiposSeguro.push({
                        label: "Seleccione Tipo Cartera",
                        value: null
                      });

                      _this5.parametros.filter(function (param) {
                        return param.diccionario_id + '' === "63";
                      }).forEach(function (element) {
                        _this5.TiposSeguro.push({
                          label: element.parametro_descripcion,
                          value: element.parametro_cod
                        });
                      });

                      if (_this5.usuarioLogin) {
                        if (_this5.usuarioLogin.usuario_banco) {//   if (this.usuarioLogin.usuario_banco.us_sucursal) {
                          //     this.objeto.sucursal = this.usuarioLogin.usuario_banco.us_sucursal + '';
                          //   }
                          //   if (this.usuarioLogin.usuario_banco.us_oficina) {
                          //     this.objeto.agencia = this.usuarioLogin.usuario_banco.us_oficina + '';
                          //   }
                        }
                      } else {
                        _this5.router.navigate(['login']);
                      }

                      if (typeof callback == 'function') {
                        callback();
                      }
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this5.router.navigate(['login']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 7:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "listaAllById",
        value: function listaAllById() {}
      }, {
        key: "EditarPoliza",
        value: function EditarPoliza(id) {
          this.btnEditar = true;
          this.btnSoporte = true;
          this.btnVer1 = true;
          sessionStorage.setItem("paramsDeleted", '');

          if (this.id_poliza === '6') {
            this.menuService.activaRuteoMenu('46', 6, {
              id_instancia_poliza: id
            }, 'AltaEcoVida', this.solicitudService.componentesInvisibles);
          }

          if (this.id_poliza === '7') {
            this.menuService.activaRuteoMenu('49', 7, {
              id_instancia_poliza: id
            }, 'AltaEcoProteccion', this.solicitudService.componentesInvisibles);
          } //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);

        }
      }, {
        key: "VerPoliza",
        value: function VerPoliza(id) {
          sessionStorage.setItem("paramsDeleted", '');
          this.btnVer2 = true;

          if (this.id_poliza + '' === '6') {
            this.menuService.activaRuteoMenu('52', 6, {
              id_instancia_poliza: id
            }, 'AltaEcoVida', this.solicitudService.componentesInvisibles);
          }

          if (this.id_poliza + '' === '7') {
            this.menuService.activaRuteoMenu('53', 7, {
              id_instancia_poliza: id
            }, 'AltaEcoProteccion', this.solicitudService.componentesInvisibles);
          } //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);

        }
      }, {
        key: "ValidarEstados",
        value: function ValidarEstados(id_estado, estados) {
          if (estados.find(function (param) {
            return param == '*';
          })) {
            return true;
          } else {
            if (estados.includes(parseInt(id_estado + ''))) {
              return true;
            } else {
              return false;
            }
          }
        }
      }, {
        key: "verifyRol",
        value: function verifyRol(estadosAvailable, rolsAvailable, instanciaPoliza) {
          var includesRol = false;

          if (!this.usuarioLogin) {
            this.usuarioLogin = this.sessionStorageService.getItemSync('userInfo');
          }

          if (this.usuarioLogin) {
            for (var i = 0; i < this.usuarioLogin.usuarioRoles.length; i++) {
              var rol = this.usuarioLogin.usuarioRoles[i];

              if (rolsAvailable.find(function (param) {
                return param == '*';
              })) {
                includesRol = true;
                break;
              } else {
                if (rolsAvailable.includes(parseInt(rol.id + ''))) {
                  includesRol = true;
                }
              }
            }

            if (this.ValidarEstados(instanciaPoliza.id_estado, estadosAvailable) && includesRol) {
              return true;
            }

            return false;
          } else {
            this.router.navigate(['login']);
          }
        }
      }, {
        key: "ValidarPlanPago",
        value: function ValidarPlanPago() {
          if (this.solicitudService.asegurado.instancia_poliza.planPago && Object.keys(this.solicitudService.asegurado.instancia_poliza.planPago)) {
            return true;
          } else {
            return false;
          }
        }
      }, {
        key: "editSolicitudDesgravamen",
        value: function editSolicitudDesgravamen(id) {
          this.router.navigate(['/AppMain/pnetseg/seguros/seguro-form', {
            id: id
          }]);
        }
      }, {
        key: "soporteSolicitudDesgravamen",
        value: function soporteSolicitudDesgravamen(id) {
          this.router.navigate(['/AppMain/pnetseg/seguros/soporte-solicitud', {
            id: id
          }]);
        }
      }, {
        key: "buscarObjetoAsegurado",
        value: function buscarObjetoAsegurado(id_instancia_poliza, index) {
          var _this6 = this;

          this.index_instancia_poliza = index;
          var id_objeto = 0;

          if (this.id_poliza + '' === '6') {
            id_objeto = 18;
          }

          if (this.id_poliza + '' === '7') {
            id_objeto = 20;
          }

          this.btnEmitir = true;
          this.solicitudService.isLoadingAgain = true;
          this.personaService.findPersonaSolicitudByIdInstanciaPoliza(id_objeto, id_instancia_poliza).subscribe(function (res) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this6, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee4() {
              var _this7 = this;

              var response;
              return _regeneratorRuntime().wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      response = res;
                      this.solicitudService.asegurado = response.data;
                      _context4.next = 4;
                      return this.componenteActualizarSolicitud.cambiarSolicitudEstado().then(function (respo) {
                        /*let ins = this.instancias_polizas.find(param => param.id + '' === id_instancia_poliza + '');
                        if (ins) {
                            ins.id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
                        }*/
                        _this7.solicitudService.isLoadingAgain = false;
                      });

                    case 4:
                      this.instancias_polizas = [];
                      this.btnEmitir = false;
                      this.instancias = [];

                    case 7:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }, function (err) {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
              _this6.router.navigate(['login']);
            } else {
              console.log(err);
            }
          });
        }
      }, {
        key: "limpiar",
        value: function limpiar() {
          this.segDeudores = [];
          var rolOficialCredito = this.usuarioLogin.usuarioRoles.find(function (param) {
            return param.id == 5;
          });
          this.objeto = {
            agencia_required: true,
            sucursal_required: true,
            estado: null,
            poliza: null,
            agencia: this.usuarioLogin ? this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_oficina + '' : '' : '',
            sucursal: this.usuarioLogin ? this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_sucursal + '' : '' : '',
            usuario_login: this.usuarioLogin ? rolOficialCredito ? this.usuarioLogin.usuario_login : '' : '',
            persona_primer_apellido: '',
            persona_segundo_apellido: '',
            persona_primer_nombre: '',
            persona_doc_id: '',
            persona_doc_id_ext: '',
            id_poliza: null,
            campo_fecha: null,
            fecha_min: null,
            fecha_max: null,
            tipo_cobertura: '',
            tipo_seguro: '',
            id_deu: '',
            nro_documento: '',
            tipo_credito: '',
            usuarioLogin: this.usuarioLogin ? this.usuarioLogin : null,
            first: 0,
            rows: 10
          };
        }
      }, {
        key: "onPageAction",
        value: function onPageAction(event, dt) {
          console.log(event);
          this.objeto.first = event.first;
          this.objeto.rows = event.rows;
          this.listaAll(dt);
        }
      }, {
        key: "filterAgencies",
        value: function filterAgencies(event) {
          if (event.value) {
            var value = event.value;
            var sucursalPadre = this.paramSucursales.find(function (param) {
              return param.parametro_cod == value;
            });

            if (sucursalPadre) {
              var filteredAgencias = this.paramAgencias.filter(function (param) {
                return param.id_padre + '' == sucursalPadre.id + '';
              });
              this.Agencias = filteredAgencias.map(function (param) {
                return {
                  value: param.parametro_cod,
                  label: param.parametro_descripcion
                };
              });
              this.Agencias.unshift({
                label: "Seleccione Agencia",
                value: null
              });
            }
          }
        }
      }, {
        key: "selectAllLikeEstados",
        value: function selectAllLikeEstados(event) {
          if (event.value) {
            var value = event.value; // if (value.indexOf(',') >= 0) {
            //     let filteredEstados = this.Estados.filter(param => value.split(',').includes(param.value));
            //     let strFilterEstado = filteredEstados.map(param => {return param.value});
            //     this.objeto.estado = strFilterEstado.join(',');
            // } else {
            // }

            this.objeto.estado = value;
          }
        }
      }, {
        key: "obtenerUltimoDocumento",
        value: function obtenerUltimoDocumento(documentos) {
          var nro_solicitud = "";
          documentos.forEach(function (documento) {
            nro_solicitud = documento.nro_documento;
          });
          return nro_solicitud;
        }
      }, {
        key: "ImprimirSolicitud",
        value: function ImprimirSolicitud(id) {
          var _this8 = this;

          var nombre_archivo = 'SoliEcoAgui' + id;
          this.solicitudService.isLoadingAgain = true;
          this.reporteService.ReporteSolicitud(id, nombre_archivo).subscribe(function (res) {
            var response = res;
            _this8.solicitudService.isLoadingAgain = false;

            if (response.status == 'ERROR') {
              _this8.service.add({
                key: 'tst',
                severity: 'warn',
                summary: 'Advertencia',
                detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
              });
            } else {
              _this8.reporteService.cargarPagina(nombre_archivo);
            }
          }, function (err) {
            _this8.solicitudService.isLoadingAgain = false;

            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
              _this8.router.navigate(['login']);
            } else {
              console.log(err);
            }
          });
        }
      }, {
        key: "ImprimirCertificado",
        value: function ImprimirCertificado(id) {
          var _this9 = this;

          var nombre_archivo = 'CertEcoAgui' + id;
          this.solicitudService.isLoadingAgain = true;
          this.reporteService.certificado_ecoaguinaldo(id, nombre_archivo).subscribe(function (res) {
            var response = res;
            _this9.solicitudService.isLoadingAgain = false;

            if (response.status == 'ERROR') {
              _this9.service.add({
                key: 'tst',
                severity: 'warn',
                summary: 'Advertencia',
                detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
              });
            } else {
              _this9.reporteService.cargarPagina(nombre_archivo);
            }
          }, function (err) {
            _this9.solicitudService.isLoadingAgain = false;

            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
              _this9.router.navigate(['login']);
            } else {
              console.log(err);
            }
          });
        }
      }, {
        key: "exportExcel",
        value: function exportExcel(dt) {
          var _this10 = this;

          this.objeto.rows = this.totalRows;
          this.objeto.first = 0;
          this.listaAll(dt, function () {
            var segDudores = null;

            if (dt.filteredValue !== null) {
              segDudores = dt.filteredValue;
            } else {
              segDudores = _this10.segDeudores;
            }

            var excelDataSegDudores = _this10.getCarsExcel(segDudores);

            var worksheet = xlsx__WEBPACK_IMPORTED_MODULE_16__["utils"].json_to_sheet(excelDataSegDudores);
            var workbook = {
              Sheets: {
                'data': worksheet
              },
              SheetNames: ['data']
            };
            var excelBuffer = xlsx__WEBPACK_IMPORTED_MODULE_16__["write"](workbook, {
              bookType: 'xlsx',
              type: 'array'
            });

            _this10.saveAsExcelFile(excelBuffer, "primengTable");
          });
        }
      }, {
        key: "saveAsExcelFile",
        value: function saveAsExcelFile(buffer, fileName) {
          var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
          var EXCEL_EXTENSION = '.xlsx';
          var data = new Blob([buffer], {
            type: EXCEL_TYPE
          });
          file_saver__WEBPACK_IMPORTED_MODULE_17__["saveAs"](data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
        }
      }, {
        key: "getCars",
        value: function getCars(aData) {
          var _this11 = this;

          var segDeudores = [];
          var c = 0,
              d = 0;

          var _loop = function _loop(i) {
            var data = aData[i];
            c++;

            var solicitudEstado = _this11.EstadosAll.find(function (param) {
              return param.Est_Codigo && param.Est_Codigo == data.deu_solicitud.Sol_EstadoSol;
            });

            var objeto = data;
            objeto['Nro'] = c;
            objeto['Nro_Sol'] = data.deu_solicitud.Sol_NumSol ? data.deu_solicitud.Sol_NumSol : '';
            objeto['id_sol'] = data.deu_solicitud.Sol_IdSol;
            objeto['id_deu'] = data.Deu_Id;
            objeto['Edad'] = data.Deu_Edad;
            objeto['Talla'] = data.Deu_Talla;
            objeto['Peso'] = data.Deu_Peso;
            objeto['id_documento_version'] = data.id_documento_version;
            objeto['Estado_Cod'] = data.deu_solicitud.Sol_EstadoSol;
            objeto['Sol_MontoSolSum'] = data.deu_solicitud.Sol_MontoSolSum;
            objeto['Sol_MontoSol'] = data.deu_solicitud.Sol_MontoSol;
            objeto['Sol_Deu_MontoSolAcumSum'] = data.deu_solicitud.Sol_Deu_MontoSolAcumSum;
            objeto['Deu_Imc'] = data.Deu_Imc;
            objeto['Deu_MontoActAcumVerifSum'] = data.Deu_MontoActAcumVerifSum;
            objeto['Deu_MontoSolAcumVerifSum'] = data.Deu_MontoSolAcumVerifSum;
            objeto['Deu_FactorK'] = data.Deu_FactorK;
            objeto['Deu_RespPositivas'] = data.Deu_RespPositivas;
            objeto['Deu_K'] = data.Deu_K;
            objeto['Deu_MontoSolAcumVerifSum'] = data.Deu_MontoSolAcumVerifSum;
            objeto['Deu_FactorImc'] = data.Deu_FactorImc;
            objeto['Sol_MonedaSol'] = data.deu_solicitud.Sol_MonedaSol;
            objeto['Poliza'] = data.Deu_Poliza ? data.Deu_Poliza : '';
            objeto['CI'] = data ? data.Deu_NumDoc ? data.Deu_NumDoc + (data.Deu_ExtDoc ? ' ' + data.Deu_ExtDoc : '') : '' : '';
            objeto['Sucursal'] = data.deu_solicitud.Sol_Sucursal;
            objeto['Agencia'] = data.deu_solicitud.Sol_Agencia;
            objeto['Tipo_Cobertura'] = data.Deu_cobertura;
            objeto['Tipo_Seguro'] = data.deu_solicitud.Sol_TipoSeg;
            objeto['Estado'] = solicitudEstado ? solicitudEstado.Est_Descripcion : '';
            objeto['Incluido'] = data.Deu_Incluido;
            objeto['Fecha_Solicitud'] = data.deu_solicitud.Sol_FechaSol ? _this11.util.formatoFecha(data.deu_solicitud.Sol_FechaSol + '') : null;
            objeto['Fecha_Resolucion'] = data ? data.Deu_FechaResolucion ? _this11.util.formatoFecha(data.Deu_FechaResolucion + '') : null : null;
            objeto['Asegurado'] = data ? data.Deu_Nombre + ' ' + data.Deu_Paterno + ' ' + data.Deu_Materno : '';
            objeto['usuario'] = data.deu_solicitud.Sol_CodOficial;
            segDeudores.push(objeto);
          };

          for (var i = 0; i < aData.length; i++) {
            _loop(i);
          }

          return segDeudores;
        }
      }, {
        key: "getCarsExcel",
        value: function getCarsExcel(ins_poli) {
          var segDeudores = [];
          var c = 0;

          var _iterator = _createForOfIteratorHelper(ins_poli),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var data = _step.value;
              c++;
              var objeto = {};
              objeto['Nro'] = data.Nro; // objeto['id_sol'] = instancia.id_sol;

              objeto['id_deu'] = data.Deu_Id; // objeto['id_documento_version'] = instancia.id_documento_version;

              objeto['Nro_Sol'] = data.Nro_Sol;
              objeto['Poliza'] = data.Poliza;
              objeto['CI'] = data.CI;
              objeto['Sucursal'] = data.Sucursal;
              objeto['Agencia'] = data.Agencia;
              objeto['Tipo_Cobertura'] = data.Tipo_Cobertura;
              objeto['Tipo_Seguro'] = data.Tipo_Seguro;
              objeto['Estado'] = data.Estado;
              objeto['Fecha_Solicitud'] = data.Fecha_Solicitud;
              objeto['Fecha_Resolucion'] = data.Fecha_Resolucion;
              objeto['Asegurado'] = data.Asegurado;
              objeto['usuario'] = data.usuario;
              segDeudores.push(objeto);
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          return segDeudores;
        }
      }, {
        key: "customSort",
        value: function customSort(event) {
          event.data.sort(function (data1, data2) {
            var value1 = data1[event.field];
            var value2 = data2[event.field];
            var result = null;
            if (value1 == null && value2 != null) result = -1;else if (value1 != null && value2 == null) result = 1;else if (value1 == null && value2 == null) result = 0;else if (typeof value1 === 'string' && typeof value2 === 'string') result = value1.localeCompare(value2);else result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;
            return event.order * result;
          });
        } // exportPdf() {
        //       const doc = new jsPDF.default(0, 0);
        //       doc.autoTable(this.exportColumns, this.poliza.instancia_polizas);
        //       doc.save('primengTable.pdf');
        // }

      }, {
        key: "ImprimirSolicitudEcovida",
        value: function ImprimirSolicitudEcovida(id) {
          var _this12 = this;

          var nombre_archivo = 'SoliEcoVida' + id;
          this.solicitudService.isLoadingAgain = true;

          if (this.id_poliza + '' === '6') {
            this.reporteService.ReporteSolicitudEcoVida(id, nombre_archivo).subscribe(function (res) {
              var response = res;
              _this12.solicitudService.isLoadingAgain = false;

              if (response.status == 'ERROR') {
                _this12.service.add({
                  key: 'tst',
                  severity: 'warn',
                  summary: 'Advertencia',
                  detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
              } else {
                _this12.reporteService.cargarPagina(nombre_archivo);
              }
            }, function (err) {
              _this12.solicitudService.isLoadingAgain = false;
              console.error("ERROR llamando servicio ImprimirSolicitud:", err);
            });
          }

          if (this.id_poliza + '' === '7') {
            this.reporteService.ReporteSolicitudEcoProteccion(id, nombre_archivo).subscribe(function (res) {
              var response = res;
              _this12.solicitudService.isLoadingAgain = false;

              if (response.status == 'ERROR') {
                _this12.service.add({
                  key: 'tst',
                  severity: 'warn',
                  summary: 'Advertencia',
                  detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
              } else {
                _this12.reporteService.cargarPagina(nombre_archivo);
              }
            }, function (err) {
              _this12.solicitudService.isLoadingAgain = false;
              console.error("ERROR llamando servicio ImprimirSolicitud:", err);
            });
          }
        }
      }, {
        key: "ImprimirCertificadoEcoVida",
        value: function ImprimirCertificadoEcoVida(id) {
          var _this13 = this;

          var nombre_archivo = 'CertEcoVida' + id;
          this.solicitudService.isLoadingAgain = true;

          if (this.id_poliza + '' === '6') {
            this.reporteService.ReporteCertificadoEcoVida(id, nombre_archivo).subscribe(function (res) {
              var response = res;
              _this13.solicitudService.isLoadingAgain = false;

              if (response.status == 'ERROR') {
                _this13.service.add({
                  key: 'tst',
                  severity: 'warn',
                  summary: 'Advertencia',
                  detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
              } else {
                _this13.reporteService.cargarPagina(nombre_archivo);
              }
            }, function (err) {
              _this13.solicitudService.isLoadingAgain = false;
              console.error("ERROR llamando servicio ImprimirCertificadoEcoVida:", err);
            });
          }

          if (this.id_poliza + '' === '7') {
            this.reporteService.ReporteCertificadoEcoProteccion(id, nombre_archivo).subscribe(function (res) {
              var response = res;
              _this13.solicitudService.isLoadingAgain = false;

              if (response.status == 'ERROR') {
                _this13.service.add({
                  key: 'tst',
                  severity: 'warn',
                  summary: 'Advertencia',
                  detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
              } else {
                _this13.reporteService.cargarPagina(nombre_archivo);
              }
            }, function (err) {
              _this13.solicitudService.isLoadingAgain = false;
              console.error("ERROR llamando servicio ImprimirCertificadoEcoVida:", err);
            });
          }
        }
      }, {
        key: "generarplanpago",
        value: function generarplanpago(id) {
          var pp = new _src_core_modelos_plan_pago__WEBPACK_IMPORTED_MODULE_15__["Plan_pago"]();
          pp.id_instancia_poliza = id;
          pp.total_prima = 192;
          pp.interes = 0;
          pp.plazo_anos = 1;
          pp.periodicidad_anual = 12;
          pp.prepagable_postpagable = 1;
          pp.fecha_inicio = new Date();
          pp.adicionado_por = '2';
          pp.modificado_por = '2';
          this.planPagoService.GenerarPlanPagos(pp).subscribe(function (res) {
            var response = res;

            if (response.status == 'ERROR') {} else {}
          }, function (err) {
            return console.error("ERROR llamando servicio plan de pagos:", err);
          });
        }
      }, {
        key: "GeneraAllPlanPagos",
        value: function GeneraAllPlanPagos(id) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee6() {
            var _this14 = this;

            return _regeneratorRuntime().wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.planPagoService.listaTodosSinPlanPagos().subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this14, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee5() {
                        var response, i, pp;
                        return _regeneratorRuntime().wrap(function _callee5$(_context5) {
                          while (1) {
                            switch (_context5.prev = _context5.next) {
                              case 0:
                                response = res;
                                i = 0;

                              case 2:
                                if (!(i < response.data.length)) {
                                  _context5.next = 18;
                                  break;
                                }

                                pp = new _src_core_modelos_plan_pago__WEBPACK_IMPORTED_MODULE_15__["Plan_pago"]();
                                pp.id_instancia_poliza = response.data[i].id;
                                pp.total_prima = parseInt(response.data[i].monto) * parseInt(response.data[i].plazo);
                                pp.interes = 0;
                                pp.plazo_anos = 1;
                                pp.periodicidad_anual = parseInt(response.data[i].monto);
                                pp.prepagable_postpagable = 1;
                                pp.fecha_inicio = response.data[i].fecha_emision;
                                pp.adicionado_por = '4';
                                pp.modificado_por = '4';
                                _context5.next = 15;
                                return this.planPagoService.GenerarPlanPagos2(pp).then(function (res) {
                                  var response = res;

                                  if (response.status == 'ERROR') {} else {}
                                }, function (err) {
                                  return console.error("ERROR llamando servicio plan de pagos:", err);
                                });

                              case 15:
                                i++;
                                _context5.next = 2;
                                break;

                              case 18:
                              case "end":
                                return _context5.stop();
                            }
                          }
                        }, _callee5, this);
                      }));
                    }, function (err) {
                      return console.error("ERROR llamando servicio plan de pagos:", err);
                    });

                  case 2:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "verPlanPagos",
        value: function verPlanPagos(ins_pol) {
          this.menuService.activaRuteoMenu('66', null, {
            id_instancia_poliza: ins_pol.id
          });
        }
      }]);

      return GestionDesgravamenComponent;
    }();

    GestionDesgravamenComponent.ɵfac = function GestionDesgravamenComponent_Factory(t) {
      return new (t || GestionDesgravamenComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_5__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_parametro_service__WEBPACK_IMPORTED_MODULE_6__["ParametrosService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_instancia_poliza_service__WEBPACK_IMPORTED_MODULE_7__["InstanciaPolizaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_menu_service__WEBPACK_IMPORTED_MODULE_8__["MenuService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_reporte_service__WEBPACK_IMPORTED_MODULE_9__["ReporteService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_persona_service__WEBPACK_IMPORTED_MODULE_10__["PersonaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_11__["SolicitudService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_sessionStorage_service__WEBPACK_IMPORTED_MODULE_12__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_plan_pago_service__WEBPACK_IMPORTED_MODULE_13__["PlanPagoService"]));
    };

    GestionDesgravamenComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: GestionDesgravamenComponent,
      selectors: [["app-gestion-desgravamen"]],
      viewQuery: function GestionDesgravamenComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.componenteActualizarSolicitud = _t.first);
        }
      },
      decls: 22,
      vars: 23,
      consts: [["class", "overlay", 4, "ngIf"], ["header", "Se Actualizo Exitosamente", 3, "visible", "modal", "responsive", "minY", "visibleChange"], [1, "ui-g-12", "text-center"], [1, "ui-icon-priority-high", "ui-icon-warn-colibri", 2, "border", "1px solid #4CAF50", "border-radius", "20px", "font-size", "50px", "color", "#4CAF50"], [3, "outerHTML"], [1, "ui-dialog-buttonpane", "ui-helper-clearfix"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "Aceptar", 3, "click"], ["header", "Filtros", "toggleable", "true", "collapseIcon", "fa fa-minus", 3, "collapsed", "collapsedChange"], ["class", "form-group", "legend", "Buscar polizas por", "toggleable", "true", 4, "ngIf"], ["class", "form-group", "legend", "Buscar por fecha", "toggleable", "true", 4, "ngIf"], [1, "ui-g", "form-group"], [1, "ui-g-12", "ui-md-12", 2, "text-align", "right"], ["type", "button", "pButton", "", "pTooltip", "Borrar Filtros", "tooltipPosition", "top", "icon", "ui-icon-refresh", 3, "click"], ["type", "button", "pButton", "", "pTooltip", "Buscar", "tooltipPosition", "top", "icon", "pi pi-search", 3, "click"], ["responsiveLayout", "scroll", "resetPageOnSort", "false", "dataKey", "id", 3, "columns", "value", "paginator", "rows", "lazyLoadOnInit", "totalRecords", "showCurrentPageReport", "currentPageReportTemplate", "rowsPerPageOptions", "onPage"], ["dt", ""], ["pTemplate", "caption"], ["pTemplate", "header"], ["pTemplate", "body"], ["key", "tst"], [3, "setAsegurado", 4, "ngIf"], [1, "overlay"], [1, "loading"], ["legend", "Buscar polizas por", "toggleable", "true", 1, "form-group"], [1, "ui-g-12", "ui-md-2"], [1, "md-inputfield"], [3, "options", "ngModel", "disabled", "showClear", "ngModelChange", "onChange"], [3, "options", "ngModel", "disabled", "showClear", "ngModelChange"], ["type", "text", "pInputText", "", 3, "ngModel", "disabled", "ngModelChange"], ["type", "text", "pInputText", "", 3, "ngModel", "ngModelChange"], [3, "options", "ngModel", "showClear", "onChange", "ngModelChange"], [3, "options", "ngModel", "showClear", "ngModelChange"], ["legend", "Buscar por fecha", "toggleable", "true", 1, "form-group"], ["selectId", "1", 3, "options", "ngModel", "showClear", "ngModelChange"], [1, "ui-g-12", "ui-md-4"], [1, "ui-g-12", "ui-md-6"], [1, "ui-g-12"], ["placeholder", "Desde", 3, "ngModel", "showIcon", "showButtonBar", "locale", "dateFormat", "ngModelChange"], ["placeholder", "Hasta", 3, "ngModel", "showIcon", "showButtonBar", "locale", "dateFormat", "ngModelChange"], [1, "ui-g"], [1, "ui-g-6", 2, "text-align", "left"], [4, "ngIf"], [1, "ui-g-6", 2, "text-align", "right"], ["type", "button", "pButton", "", "icon", "pi pi-file-excel", "iconPos", "left", "label", "EXCEL", 1, "ui-button-success", 2, "margin-right", "0.5em", 3, "click"], [3, "width", "pSortableColumn", 4, "ngFor", "ngForOf"], [3, "ngSwitch", 4, "ngFor", "ngForOf"], [3, "pSortableColumn"], ["ariaLabel", "Activate to sort", "ariaLabelDesc", "Activate to sort in descending order", "ariaLabelAsc", "Activate to sort in ascending order", 3, "field"], [3, "ngSwitch"], ["pInputText", "", "type", "text", 3, "input"], [2, "font-size", "12px"], [2, "font-size", "12px", 3, "classList"], ["style", "font-size: 12px;", 3, "classList", 4, "ngIf"], ["type", "button", "pButton", "", 3, "disabled", "icon", "title", "click"], ["type", "button", "pButton", "", "icon", "ui-icon-print", "title", "Imprimir Documentos", 3, "click", 4, "ngIf"], ["type", "button", "pButton", "", "icon", "ui-icon-print", "title", "Imprimir Documentos", 3, "click"], ["style", "font-size: 12px;", 4, "ngIf"], ["type", "button", "pButton", "", "icon", "ui-icon-supervisor-account", "title", "Soporte", 3, "disabled", "click"], [3, "setAsegurado"], ["componenteActualizarSolicitud", ""]],
      template: function GestionDesgravamenComponent_Template(rf, ctx) {
        if (rf & 1) {
          var _r85 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, GestionDesgravamenComponent_div_0_Template, 2, 0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-dialog", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function GestionDesgravamenComponent_Template_p_dialog_visibleChange_1_listener($event) {
            return ctx.displayExedioResultados = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "i", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "p", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "button", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_Template_button_click_7_listener() {
            return ctx.displayExedioResultados = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "p-panel", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("collapsedChange", function GestionDesgravamenComponent_Template_p_panel_collapsedChange_8_listener($event) {
            return ctx.PanelFiltrosColapsed = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, GestionDesgravamenComponent_p_fieldset_9_Template, 72, 31, "p-fieldset", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, GestionDesgravamenComponent_p_fieldset_10_Template, 14, 13, "p-fieldset", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "button", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_Template_button_click_13_listener() {
            return ctx.limpiar();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "button", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionDesgravamenComponent_Template_button_click_14_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r85);

            var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](16);

            ctx.objeto.first = 0;
            ctx.objeto.rows = 10;
            return ctx.listaAll(_r3);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "p-table", 14, 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onPage", function GestionDesgravamenComponent_Template_p_table_onPage_15_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r85);

            var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](16);

            return ctx.onPageAction($event, _r3);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, GestionDesgravamenComponent_ng_template_17_Template, 6, 1, "ng-template", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, GestionDesgravamenComponent_ng_template_18_Template, 4, 2, "ng-template", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, GestionDesgravamenComponent_ng_template_19_Template, 3, 3, "ng-template", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](20, "p-toast", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, GestionDesgravamenComponent_app_actualizar_solicitud_21_Template, 2, 0, "app-actualizar-solicitud", 20);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.isLoadingAgain);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayExedioResultados)("modal", true)("responsive", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("outerHTML", "<br><br>Advertencia: El criterio de b\xFAsqueda introducido ha generado como resultado gran cantidad de registros, <br>por lo que se mostrar\xE1n solo los primeros 300 cuyas fechas de solicitud son las m\xE1s recientes.", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("collapsed", ctx.PanelFiltrosColapsed);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.showFiltros);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.showFiltros);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("columns", ctx.esAseguradoraAlianza ? ctx.colsAseguradoraAlianza : ctx.esAseguradoraLbcSuscriptor ? ctx.colsAseguradoraLbcSuscriptor : ctx.cols)("value", ctx.segDeudores)("paginator", true)("rows", ctx.objeto.rows)("lazyLoadOnInit", true)("totalRecords", ctx.totalRows)("showCurrentPageReport", true)("currentPageReportTemplate", "Mostrando los \xFAltimos " + ctx.numRecords + " de " + ctx.rowsPerPage + " de " + ctx.totalRows + " registros")("rowsPerPageOptions", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](21, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](22, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_20__["NgIf"], primeng__WEBPACK_IMPORTED_MODULE_2__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_21__["Footer"], primeng__WEBPACK_IMPORTED_MODULE_2__["ButtonDirective"], primeng_panel__WEBPACK_IMPORTED_MODULE_22__["Panel"], primeng__WEBPACK_IMPORTED_MODULE_2__["Table"], primeng_api__WEBPACK_IMPORTED_MODULE_21__["PrimeTemplate"], primeng_toast__WEBPACK_IMPORTED_MODULE_23__["Toast"], primeng__WEBPACK_IMPORTED_MODULE_2__["Fieldset"], primeng__WEBPACK_IMPORTED_MODULE_2__["Dropdown"], _angular_forms__WEBPACK_IMPORTED_MODULE_24__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_24__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_24__["DefaultValueAccessor"], primeng__WEBPACK_IMPORTED_MODULE_2__["InputText"], primeng_calendar__WEBPACK_IMPORTED_MODULE_25__["Calendar"], _angular_common__WEBPACK_IMPORTED_MODULE_20__["NgForOf"], primeng__WEBPACK_IMPORTED_MODULE_2__["SortableColumn"], primeng__WEBPACK_IMPORTED_MODULE_2__["SortIcon"], _angular_common__WEBPACK_IMPORTED_MODULE_20__["NgSwitch"], _src_core_componentes_actualizar_solicitud_actualizar_solicitud_component__WEBPACK_IMPORTED_MODULE_18__["ActualizarSolicitudComponent"]],
      styles: [".factor-imc-danger[_ngcontent-%COMP%]{\n    color:red;\n}\n.factor-imc-warning[_ngcontent-%COMP%]{\n    color:yellow;\n}\n.factor-imc-good[_ngcontent-%COMP%]{\n    color:blue;\n}\n.factor-imc-info[_ngcontent-%COMP%]{\n    color:deepskyblue;\n}\n.factor-k-danger[_ngcontent-%COMP%]{\n    color:red;\n}\n.factor-k-warning[_ngcontent-%COMP%]{\n    color:yellow;\n}\n.factor-k-good[_ngcontent-%COMP%]{\n    color:blue;\n}\n.factor-k-info[_ngcontent-%COMP%]{\n    color:blue;\n}\n.resp-warning[_ngcontent-%COMP%] {\n    color: yellow;\n}\n.resp-danger[_ngcontent-%COMP%] {\n    color: red;\n}\n.resp-info[_ngcontent-%COMP%] {\n    color: blue;\n}\n.edad-warning[_ngcontent-%COMP%] {\n    color: yellow;\n}\n.edad-danger[_ngcontent-%COMP%] {\n    color: red;\n}\n.edad-info[_ngcontent-%COMP%] {\n    color: blue;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2NyZWRpdGljaW9zL3NyYy9hcHAvZGVzZ3JhdmFtZW4vZ2VzdGlvbi1kZXNncmF2YW1lbi9nZXN0aW9uLWRlc2dyYXZhbWVuLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxTQUFTO0FBQ2I7QUFDQTtJQUNJLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFVBQVU7QUFDZDtBQUNBO0lBQ0ksaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxTQUFTO0FBQ2I7QUFDQTtJQUNJLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFVBQVU7QUFDZDtBQUNBO0lBQ0ksVUFBVTtBQUNkO0FBRUE7SUFDSSxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxVQUFVO0FBQ2Q7QUFDQTtJQUNJLFdBQVc7QUFDZjtBQUNBO0lBQ0ksYUFBYTtBQUNqQjtBQUNBO0lBQ0ksVUFBVTtBQUNkO0FBQ0E7SUFDSSxXQUFXO0FBQ2YiLCJmaWxlIjoicHJvamVjdHMvY3JlZGl0aWNpb3Mvc3JjL2FwcC9kZXNncmF2YW1lbi9nZXN0aW9uLWRlc2dyYXZhbWVuL2dlc3Rpb24tZGVzZ3JhdmFtZW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mYWN0b3ItaW1jLWRhbmdlcntcbiAgICBjb2xvcjpyZWQ7XG59XG4uZmFjdG9yLWltYy13YXJuaW5ne1xuICAgIGNvbG9yOnllbGxvdztcbn1cbi5mYWN0b3ItaW1jLWdvb2R7XG4gICAgY29sb3I6Ymx1ZTtcbn1cbi5mYWN0b3ItaW1jLWluZm97XG4gICAgY29sb3I6ZGVlcHNreWJsdWU7XG59XG4uZmFjdG9yLWstZGFuZ2Vye1xuICAgIGNvbG9yOnJlZDtcbn1cbi5mYWN0b3Itay13YXJuaW5ne1xuICAgIGNvbG9yOnllbGxvdztcbn1cbi5mYWN0b3Itay1nb29ke1xuICAgIGNvbG9yOmJsdWU7XG59XG4uZmFjdG9yLWstaW5mb3tcbiAgICBjb2xvcjpibHVlO1xufVxuXG4ucmVzcC13YXJuaW5nIHtcbiAgICBjb2xvcjogeWVsbG93O1xufVxuLnJlc3AtZGFuZ2VyIHtcbiAgICBjb2xvcjogcmVkO1xufVxuLnJlc3AtaW5mbyB7XG4gICAgY29sb3I6IGJsdWU7XG59XG4uZWRhZC13YXJuaW5nIHtcbiAgICBjb2xvcjogeWVsbG93O1xufVxuLmVkYWQtZGFuZ2VyIHtcbiAgICBjb2xvcjogcmVkO1xufVxuLmVkYWQtaW5mbyB7XG4gICAgY29sb3I6IGJsdWU7XG59Il19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](GestionDesgravamenComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-gestion-desgravamen',
          templateUrl: './gestion-desgravamen.component.html',
          styleUrls: ['./gestion-desgravamen.component.css']
        }]
      }], function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_5__["BreadcrumbService"]
        }, {
          type: _src_core_servicios_parametro_service__WEBPACK_IMPORTED_MODULE_6__["ParametrosService"]
        }, {
          type: _src_core_servicios_instancia_poliza_service__WEBPACK_IMPORTED_MODULE_7__["InstanciaPolizaService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _src_core_servicios_menu_service__WEBPACK_IMPORTED_MODULE_8__["MenuService"]
        }, {
          type: _src_core_servicios_reporte_service__WEBPACK_IMPORTED_MODULE_9__["ReporteService"]
        }, {
          type: primeng__WEBPACK_IMPORTED_MODULE_2__["MessageService"]
        }, {
          type: _src_core_servicios_persona_service__WEBPACK_IMPORTED_MODULE_10__["PersonaService"]
        }, {
          type: _src_core_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_11__["SolicitudService"]
        }, {
          type: _src_core_servicios_sessionStorage_service__WEBPACK_IMPORTED_MODULE_12__["SessionStorageService"]
        }, {
          type: _src_core_servicios_plan_pago_service__WEBPACK_IMPORTED_MODULE_13__["PlanPagoService"]
        }];
      }, {
        componenteActualizarSolicitud: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['componenteActualizarSolicitud', {
            "static": false
          }]
        }]
      });
    })();
    /***/

  },

  /***/
  "../crediticios/src/app/eco-accidentes/alta-eco-accidentes/alta-eco-accidentes.component.ts":
  /*!**************************************************************************************************!*\
    !*** ../crediticios/src/app/eco-accidentes/alta-eco-accidentes/alta-eco-accidentes.component.ts ***!
    \**************************************************************************************************/

  /*! exports provided: AltaEcoAccidentesComponent */

  /***/
  function crediticiosSrcAppEcoAccidentesAltaEcoAccidentesAltaEcoAccidentesComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AltaEcoAccidentesComponent", function () {
      return AltaEcoAccidentesComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! primeng/api */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-api.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/platform-browser */
    "../../node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
    /* harmony import */


    var util__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! util */
    "../../node_modules/util/util.js");
    /* harmony import */


    var util__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_6__);
    /* harmony import */


    var _src_helpers_prototypes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../../../../src/helpers/prototypes */
    "../../src/helpers/prototypes.ts");
    /* harmony import */


    var _src_helpers_prototypes__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_src_helpers_prototypes__WEBPACK_IMPORTED_MODULE_7__);
    /* harmony import */


    var _src_core_modelos_anexo_poliza__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/anexo_poliza */
    "../../src/core/modelos/anexo_poliza.ts");
    /* harmony import */


    var _src_core_modelos_persona__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/persona */
    "../../src/core/modelos/persona.ts");
    /* harmony import */


    var _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/persona_banco */
    "../../src/core/modelos/persona_banco.ts");
    /* harmony import */


    var _src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/breadcrumb.service */
    "../../src/core/servicios/breadcrumb.service.ts");
    /* harmony import */


    var _src_core_servicios_parametro_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/parametro.service */
    "../../src/core/servicios/parametro.service.ts");
    /* harmony import */


    var _src_core_servicios_anexo_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/anexo.service */
    "../../src/core/servicios/anexo.service.ts");
    /* harmony import */


    var _src_core_servicios_persona_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/persona.service */
    "../../src/core/servicios/persona.service.ts");
    /* harmony import */


    var _src_core_servicios_beneficiario_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/beneficiario.service */
    "../../src/core/servicios/beneficiario.service.ts");
    /* harmony import */


    var _src_core_servicios_soapui_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/soapui.service */
    "../../src/core/servicios/soapui.service.ts");
    /* harmony import */


    var _src_core_servicios_atributo_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/atributo.service */
    "../../src/core/servicios/atributo.service.ts");
    /* harmony import */


    var _src_core_servicios_objetoAtributo_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/objetoAtributo.service */
    "../../src/core/servicios/objetoAtributo.service.ts");
    /* harmony import */


    var _src_core_servicios_documento_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/documento.service */
    "../../src/core/servicios/documento.service.ts");
    /* harmony import */


    var _src_core_servicios_poliza_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/poliza.service */
    "../../src/core/servicios/poliza.service.ts");
    /* harmony import */


    var _src_core_servicios_instancia_poliza_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/instancia-poliza.service */
    "../../src/core/servicios/instancia-poliza.service.ts");
    /* harmony import */


    var _src_core_servicios_reporte_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/reporte.service */
    "../../src/core/servicios/reporte.service.ts");
    /* harmony import */


    var _src_core_servicios_rol_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/rol.service */
    "../../src/core/servicios/rol.service.ts");
    /* harmony import */


    var _src_core_servicios_usuarios_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/usuarios.service */
    "../../src/core/servicios/usuarios.service.ts");
    /* harmony import */


    var _src_core_servicios_contexto_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/contexto.service */
    "../../src/core/servicios/contexto.service.ts");
    /* harmony import */


    var _src_core_servicios_instancia_poliza_trans_service__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/instancia-poliza-trans.service */
    "../../src/core/servicios/instancia-poliza-trans.service.ts");
    /* harmony import */


    var _src_core_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/solicitud.service */
    "../../src/core/servicios/solicitud.service.ts");
    /* harmony import */


    var _src_core_servicios_plan_pago_service__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/plan-pago.service */
    "../../src/core/servicios/plan-pago.service.ts");
    /* harmony import */


    var _src_core_servicios_sessionStorage_service__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/sessionStorage.service */
    "../../src/core/servicios/sessionStorage.service.ts");
    /* harmony import */


    var _src_core_servicios_administracion_de_permisos_service__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/administracion-de-permisos.service */
    "../../src/core/servicios/administracion-de-permisos.service.ts");
    /* harmony import */


    var _src_core_modelos_asegurado__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/asegurado */
    "../../src/core/modelos/asegurado.ts");
    /* harmony import */


    var _src_core_modelos_instancia_documento__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/instancia_documento */
    "../../src/core/modelos/instancia_documento.ts");
    /* harmony import */


    var _src_core_modelos_instancia_poliza_transicion__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/instancia_poliza_transicion */
    "../../src/core/modelos/instancia_poliza_transicion.ts");
    /* harmony import */


    var _src_core_modelos_persona_banco_operacion__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/persona_banco_operacion */
    "../../src/core/modelos/persona_banco_operacion.ts");
    /* harmony import */


    var _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/persona_banco_account */
    "../../src/core/modelos/persona_banco_account.ts");
    /* harmony import */


    var _src_core_modelos_persona_banco_pep__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/persona_banco_pep */
    "../../src/core/modelos/persona_banco_pep.ts");
    /* harmony import */


    var _src_core_modelos_persona_banco_datos__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/persona_banco_datos */
    "../../src/core/modelos/persona_banco_datos.ts");
    /* harmony import */


    var _src_core_modelos_instancia_poliza__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/instancia_poliza */
    "../../src/core/modelos/instancia_poliza.ts");
    /* harmony import */


    var _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/atributo_instancia_poliza */
    "../../src/core/modelos/atributo_instancia_poliza.ts");
    /* harmony import */


    var _src_core_modelos_plan_pago__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/plan_pago */
    "../../src/core/modelos/plan_pago.ts");
    /* harmony import */


    var _src_core_modelos_upload__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/upload */
    "../../src/core/modelos/upload.ts");
    /* harmony import */


    var _src_core_servicios_archivo_service__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/archivo.service */
    "../../src/core/servicios/archivo.service.ts");
    /* harmony import */


    var _src_core_componentes_transiciones_transiciones_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(
    /*! ../../../../../../src/core/componentes/transiciones/transiciones.component */
    "../../src/core/componentes/transiciones/transiciones.component.ts");
    /* harmony import */


    var _src_core_componentes_beneficiario_beneficiario_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(
    /*! ../../../../../../src/core/componentes/beneficiario/beneficiario.component */
    "../../src/core/componentes/beneficiario/beneficiario.component.ts");
    /* harmony import */


    var _src_core_componentes_archivos_archivos_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(
    /*! ../../../../../../src/core/componentes/archivos/archivos.component */
    "../../src/core/componentes/archivos/archivos.component.ts");
    /* harmony import */


    var _src_helpers_util__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(
    /*! ../../../../../../src/helpers/util */
    "../../src/helpers/util.ts");
    /* harmony import */


    var _src_core_componentes_actualizar_solicitud_actualizar_solicitud_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(
    /*! ../../../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component */
    "../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(
    /*! @angular/common */
    "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var primeng__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(
    /*! primeng */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng.js");
    /* harmony import */


    var primeng_messages__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(
    /*! primeng/messages */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-messages.js");
    /* harmony import */


    var primeng_toast__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(
    /*! primeng/toast */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-toast.js");
    /* harmony import */


    var primeng_panel__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(
    /*! primeng/panel */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-panel.js");
    /* harmony import */


    var primeng_calendar__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(
    /*! primeng/calendar */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-calendar.js");
    /* harmony import */


    var primeng_message__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(
    /*! primeng/message */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-message.js");
    /* harmony import */


    var primeng_tabview__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(
    /*! primeng/tabview */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-tabview.js");

    var _c0 = ["componentTransiciones"];
    var _c1 = ["componenteBeneficiario"];
    var _c2 = ["componenteArchivo"];
    var _c3 = ["componenteActualizarSolicitud"];

    function AltaEcoAccidentesComponent_div_0_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "div", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_app_transiciones_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "app-transiciones", 65, 66);
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("parametros_atributos", ctx_r1.solicitudService.parametrosYAtributos);
      }
    }

    function AltaEcoAccidentesComponent_app_actualizar_solicitud_2_Template(rf, ctx) {
      if (rf & 1) {
        var _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-actualizar-solicitud", 67, 68);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("setAsegurado", function AltaEcoAccidentesComponent_app_actualizar_solicitud_2_Template_app_actualizar_solicitud_setAsegurado_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r23);

          var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r22.solicitudService.asegurado = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_p_button_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r68 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-button", 98);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_panel_3_p_button_9_Template_p_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r68);

          var ctx_r67 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r67.solicitudService.btnOrdenPagoEnabled ? ctx_r67.solicitudService.ImprimirOrden(ctx_r67.solicitudService.asegurado.instancia_poliza.id, ctx_r67.solicitudService.asegurado.instancia_poliza.id_poliza) : "";
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r24.solicitudService.btnOrdenPagoEnabled);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_p_button_10_Template(rf, ctx) {
      if (rf & 1) {
        var _r70 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-button", 99);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_panel_3_p_button_10_Template_p_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r70);

          var ctx_r69 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r69.solicitudService.btnCartaEnabled ? ctx_r69.solicitudService.ImprimirCarta(ctx_r69.solicitudService.asegurado.instancia_poliza.id, ctx_r69.solicitudService.asegurado.instancia_poliza.id_poliza) : "";
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r25.solicitudService.btnCartaEnabled);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_p_button_11_Template(rf, ctx) {
      if (rf & 1) {
        var _r72 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-button", 100);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_panel_3_p_button_11_Template_p_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r72);

          var ctx_r71 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r71.solicitudService.btnCartaDesistimiento ? ctx_r71.solicitudService.ImprimirAnulacion(ctx_r71.solicitudService.asegurado.instancia_poliza.id, ctx_r71.solicitudService.asegurado.instancia_poliza.id_poliza) : "";
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r26.solicitudService.btnCartaDesistimiento);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_p_button_13_Template(rf, ctx) {
      if (rf & 1) {
        var _r74 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-button", 101);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_panel_3_p_button_13_Template_p_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r74);

          var ctx_r73 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r73.solicitudService.btnRefrescarInformacionEnabled ? ctx_r73.refrescarInformacion() : "";
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("title", "Refrescar Informaci\xF3n")("disabled", !ctx_r27.solicitudService.btnRefrescarInformacionEnabled);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_36_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "N\xFAmero de Comprobante de Pago:");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_37_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "N\xFAmero de Certificado de Cobertura: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_42_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Vigencia del certificado de cobertura: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_43_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Sucursal: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_44_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Agencia: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_53_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r33.solicitudService.instanciaDocumentoSolicitud.fecha_emision.getDate().pad(2) + "/" + (ctx_r33.solicitudService.instanciaDocumentoSolicitud.fecha_emision.getMonth() + 1).pad(2) + "/" + ctx_r33.solicitudService.instanciaDocumentoSolicitud.fecha_emision.getFullYear());
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_55_span_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r75 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r75.solicitudService.instanciaDocumentoComprobante.fecha_emision.getDate().pad(2) + "/" + (ctx_r75.solicitudService.instanciaDocumentoComprobante.fecha_emision.getMonth() + 1).pad(2) + "/" + ctx_r75.solicitudService.instanciaDocumentoComprobante.fecha_emision.getFullYear());
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_55_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AltaEcoAccidentesComponent_p_panel_3_span_55_span_2_Template, 2, 1, "span", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx_r34.solicitudService.instanciaDocumentoComprobante.nro_documento, " de ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r34.solicitudService.instanciaDocumentoComprobante.fecha_emision);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_56_span_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r76 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r76.solicitudService.instanciaDocumentoCertificado.fecha_emision.getDate().pad(2) + "/" + (ctx_r76.solicitudService.instanciaDocumentoCertificado.fecha_emision.getMonth() + 1).pad(2) + "/" + ctx_r76.solicitudService.instanciaDocumentoCertificado.fecha_emision.getFullYear());
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_56_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AltaEcoAccidentesComponent_p_panel_3_span_56_span_2_Template, 2, 1, "span", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx_r35.solicitudService.instanciaDocumentoCertificado.nro_documento, " de ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r35.solicitudService.instanciaDocumentoCertificado.fecha_emision);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_60_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate6"](" De ", ctx_r36.solicitudService.instanciaDocumentoCertificado.fecha_inicio_vigencia.getDate().pad(2), "/", (ctx_r36.solicitudService.instanciaDocumentoCertificado.fecha_inicio_vigencia.getMonth() + 1).pad(2), "/", ctx_r36.solicitudService.instanciaDocumentoCertificado.fecha_inicio_vigencia.getFullYear(), " a", ctx_r36.solicitudService.instanciaDocumentoCertificado.fecha_fin_vigencia.getDate().pad(2), "/", (ctx_r36.solicitudService.instanciaDocumentoCertificado.fecha_fin_vigencia.getMonth() + 1).pad(2), "/", ctx_r36.solicitudService.instanciaDocumentoCertificado.fecha_fin_vigencia.getFullYear(), "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_61_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r37.solicitudService.SucursalesParametroCod[ctx_r37.solicitudService.atributoSucursal.valor]);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_62_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r38.solicitudService.AgenciasParametroCod[ctx_r38.solicitudService.atributoAgencia.valor]);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_64_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "M\u0117todo de Pago: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_65_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Nro Solicitud de Cr\xE9dito: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_66_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Tipo Cr\xE9dito: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_67_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Estado solicitud de Cr\xE9dito: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_68_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Prima: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_70_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r44 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r44.solicitudService.CondicionesIdDesc[ctx_r44.solicitudService.atributoDebitoAutomatico.valor]);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_71_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r45.solicitudService.atributoSolicitudSci.valor);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_72_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r46 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r46.solicitudService.atributoTipoCredito.valor);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_73_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r47.solicitudService.atributoSolicitudSciEstado.valor);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_74_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r48.solicitudService.asegurado.instancia_poliza.anexo_poliza.monto_prima);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_83_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Nombre: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_84_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Apellido Paterno: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_85_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Apellido Materno:");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_91_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r52 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r52.solicitudService.asegurado.entidad.persona.persona_primer_nombre);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_92_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r53.solicitudService.asegurado.entidad.persona.persona_primer_apellido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_93_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r54.solicitudService.asegurado.entidad.persona.persona_segundo_apellido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_98_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Fecha Nacimiento: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_99_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Celular: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_100_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Nro Cuenta: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_101_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Plan:");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_102_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Plazo:");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_104_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r60 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r60.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getDate() + "/" + (ctx_r60.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getMonth() + 1) + "/" + ctx_r60.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getFullYear());
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_105_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r61.solicitudService.asegurado.entidad.persona.persona_celular);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_106_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r62 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r62.solicitudService.persona_banco_account.nrocuenta + " " + ctx_r62.solicitudService.MonedasParametroCod[ctx_r62.solicitudService.persona_banco_account.moneda + ""]);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_107_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r63.solicitudService.PlanesParametroCod[ctx_r63.solicitudService.persona_banco_datos.plan + ""]);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_span_108_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r64 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx_r64.solicitudService.persona_banco_datos.plazo, " (meses)");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_3_app_archivos_113_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "app-archivos", null, 102);
      }
    }

    var _c4 = function _c4(a0) {
      return [a0];
    };

    function AltaEcoAccidentesComponent_p_panel_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r79 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-panel", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onBeforeToggle", function AltaEcoAccidentesComponent_p_panel_3_Template_p_panel_onBeforeToggle_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r79);

          var ctx_r78 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          ctx_r78.solicitudService.displayModalDatosTitular = false;
          return ctx_r78.beforeToggleDatosTitular(false);
        })("collapsedChange", function AltaEcoAccidentesComponent_p_panel_3_Template_p_panel_collapsedChange_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r79);

          var ctx_r80 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r80.collapsedDatosTitular = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 72);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 73);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 74);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "p-button", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_panel_3_Template_p_button_click_8_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r79);

          var ctx_r81 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r81.solicitudService.btnImprimirSolicitudEnabled ? ctx_r81.solicitudService.ImprimirSolicitud(ctx_r81.solicitudService.asegurado.instancia_poliza.id, ctx_r81.solicitudService.asegurado.instancia_poliza.id_poliza, null, ctx_r81.solicitudService.persona_banco_solicitud.solicitud_sci_selected) : "";
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, AltaEcoAccidentesComponent_p_panel_3_p_button_9_Template, 1, 1, "p-button", 76);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, AltaEcoAccidentesComponent_p_panel_3_p_button_10_Template, 1, 1, "p-button", 77);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, AltaEcoAccidentesComponent_p_panel_3_p_button_11_Template, 1, 1, "p-button", 78);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "p-button", 79);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_panel_3_Template_p_button_click_12_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r79);

          var ctx_r82 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r82.solicitudService.btnEmitirCertificadoEnabled ? ctx_r82.solicitudService.ImprimirCertificado(ctx_r82.solicitudService.asegurado.instancia_poliza.id, ctx_r82.solicitudService.asegurado.instancia_poliza.id_poliza) : "";
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, AltaEcoAccidentesComponent_p_panel_3_p_button_13_Template, 1, 2, "p-button", 80);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "p-button", 81);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_panel_3_Template_p_button_click_14_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r79);

          var ctx_r83 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r83.solicitudService.btnValidarYContinuarEnabled ? ctx_r83.solicitudService.hasRolCajero || ctx_r83.solicitudService.hasRolPlataforma && ctx_r83.solicitudService.atributoSolicitudSci.valor.substring(0, 1) == "9" ? ctx_r83.componenteActualizarSolicitud.cambiarSolicitudEstado() : ctx_r83.validarYContinuar() : "";
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 82);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "p-fieldset", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "div", 85);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](20, "img", 86);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "div", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "div", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26, "N\xFAmero de solicitud del seguro: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](27, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30, "N\xFAmero de Poliza: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](31, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](34, "Fecha de Solicitud: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](35, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](36, AltaEcoAccidentesComponent_p_panel_3_span_36_Template, 4, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](37, AltaEcoAccidentesComponent_p_panel_3_span_37_Template, 4, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](40, "Estado Solicitud Seguro: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](41, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](42, AltaEcoAccidentesComponent_p_panel_3_span_42_Template, 4, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](43, AltaEcoAccidentesComponent_p_panel_3_span_43_Template, 4, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](44, AltaEcoAccidentesComponent_p_panel_3_span_44_Template, 4, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "div", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](48, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](51, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](53, AltaEcoAccidentesComponent_p_panel_3_span_53_Template, 2, 1, "span", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](54, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](55, AltaEcoAccidentesComponent_p_panel_3_span_55_Template, 4, 2, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](56, AltaEcoAccidentesComponent_p_panel_3_span_56_Template, 4, 2, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](57, "span", 91);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](58);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](59, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](60, AltaEcoAccidentesComponent_p_panel_3_span_60_Template, 3, 6, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](61, AltaEcoAccidentesComponent_p_panel_3_span_61_Template, 3, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](62, AltaEcoAccidentesComponent_p_panel_3_span_62_Template, 3, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](63, "div", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](64, AltaEcoAccidentesComponent_p_panel_3_span_64_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](65, AltaEcoAccidentesComponent_p_panel_3_span_65_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](66, AltaEcoAccidentesComponent_p_panel_3_span_66_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](67, AltaEcoAccidentesComponent_p_panel_3_span_67_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](68, AltaEcoAccidentesComponent_p_panel_3_span_68_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](69, "div", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](70, AltaEcoAccidentesComponent_p_panel_3_span_70_Template, 3, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](71, AltaEcoAccidentesComponent_p_panel_3_span_71_Template, 3, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](72, AltaEcoAccidentesComponent_p_panel_3_span_72_Template, 3, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](73, AltaEcoAccidentesComponent_p_panel_3_span_73_Template, 3, 1, "span", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](74, AltaEcoAccidentesComponent_p_panel_3_span_74_Template, 3, 1, "span", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](75, "div", 92);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "p-fieldset", 93);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](77, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](78, "div", 85);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](79, "img", 94);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](80, "div", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](81, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "div", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](83, AltaEcoAccidentesComponent_p_panel_3_span_83_Template, 4, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](84, AltaEcoAccidentesComponent_p_panel_3_span_84_Template, 4, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](85, AltaEcoAccidentesComponent_p_panel_3_span_85_Template, 4, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](86, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](87, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](88, "Nro Documento Identidad: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](89, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](90, "div", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](91, AltaEcoAccidentesComponent_p_panel_3_span_91_Template, 3, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](92, AltaEcoAccidentesComponent_p_panel_3_span_92_Template, 3, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](93, AltaEcoAccidentesComponent_p_panel_3_span_93_Template, 3, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](94, "span", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](95);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](96, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](97, "div", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](98, AltaEcoAccidentesComponent_p_panel_3_span_98_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](99, AltaEcoAccidentesComponent_p_panel_3_span_99_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](100, AltaEcoAccidentesComponent_p_panel_3_span_100_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](101, AltaEcoAccidentesComponent_p_panel_3_span_101_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](102, AltaEcoAccidentesComponent_p_panel_3_span_102_Template, 5, 0, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](103, "div", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](104, AltaEcoAccidentesComponent_p_panel_3_span_104_Template, 4, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](105, AltaEcoAccidentesComponent_p_panel_3_span_105_Template, 4, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](106, AltaEcoAccidentesComponent_p_panel_3_span_106_Template, 4, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](107, AltaEcoAccidentesComponent_p_panel_3_span_107_Template, 4, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](108, AltaEcoAccidentesComponent_p_panel_3_span_108_Template, 4, 1, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](109, "div", 92);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](110, "button", 95);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_panel_3_Template_button_click_110_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r79);

          var ctx_r84 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r84.editarTitular();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](111, "app-beneficiario", 96, 97);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("setBeneficiarios", function AltaEcoAccidentesComponent_p_panel_3_Template_app_beneficiario_setBeneficiarios_111_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r79);

          var ctx_r85 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r85.solicitudService.asegurado.beneficiarios = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](113, AltaEcoAccidentesComponent_p_panel_3_app_archivos_113_Template, 2, 0, "app-archivos", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("header", "SOLICITUD ", ctx_r3.solicitudService.poliza.descripcion, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("collapsed", ctx_r3.collapsedDatosTitular);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r3.solicitudService.btnImprimirSolicitudEnabled);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.showOrdenPago);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.showBtnImprimirCarta);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.showBtnImprimirDesistimiento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r3.solicitudService.btnEmitirCertificadoEnabled);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.btnRefrescarInformacion);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("title", "Validar y Continuar")("disabled", !ctx_r3.solicitudService.btnValidarYContinuarEnabled);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r3.solicitudService.asegurado.instancia_poliza.id_estado == ctx_r3.solicitudService.estadoIniciado.id ? "task-box task-box-1" : ctx_r3.solicitudService.asegurado.instancia_poliza.id_estado == ctx_r3.solicitudService.estadoSolicitado.id ? "task-box task-box-2" : ctx_r3.solicitudService.asegurado.instancia_poliza.id_estado == ctx_r3.solicitudService.estadoPorPagar.id ? "task-box task-box-3" : ctx_r3.solicitudService.asegurado.instancia_poliza.id_estado == ctx_r3.solicitudService.estadoEmitido.id ? "task-box task-box-4" : "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](19);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.instanciaDocumentoComprobante.nro_documento && ctx_r3.solicitudService.showOrdenPago);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.instanciaDocumentoCertificado.nro_documento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.instanciaDocumentoCertificado.fecha_inicio_vigencia && ctx_r3.solicitudService.instanciaDocumentoCertificado.fecha_fin_vigencia);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoSucursal);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoAgencia);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r3.solicitudService.instanciaDocumentoSolicitud.nro_documento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r3.solicitudService.poliza.numero);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.instanciaDocumentoSolicitud.fecha_emision);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.instanciaDocumentoComprobante.nro_documento && ctx_r3.solicitudService.showOrdenPago);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.instanciaDocumentoCertificado.nro_documento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.EstadosInstaciaPolizaId[ctx_r3.solicitudService.asegurado.instancia_poliza.id_estado]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.instanciaDocumentoCertificado.fecha_inicio_vigencia && ctx_r3.solicitudService.instanciaDocumentoCertificado.fecha_fin_vigencia);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoSucursal);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoAgencia);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoDebitoAutomatico);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoSolicitudSci);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoTipoCredito);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoSolicitudSciEstado);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.asegurado.instancia_poliza.anexo_poliza);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoDebitoAutomatico);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoSolicitudSci);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoTipoCredito);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.atributoSolicitudSciEstado);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado && ctx_r3.solicitudService.asegurado.instancia_poliza.anexo_poliza);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_primer_nombre);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_primer_apellido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_segundo_apellido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_primer_nombre);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_primer_apellido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_segundo_apellido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"]("", ctx_r3.solicitudService.asegurado.entidad.persona.persona_doc_id, " ", ctx_r3.solicitudService.asegurado.entidad.persona.persona_doc_id_ext != 12 ? ctx_r3.solicitudService.ProcedenciaCIParametroCod[ctx_r3.solicitudService.asegurado.entidad.persona.persona_doc_id_ext] : "", "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_celular);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.showNroCuenta && ctx_r3.solicitudService.persona_banco_account.nrocuenta != "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.id_poliza == 5 && ctx_r3.solicitudService.showPlan);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.id_poliza == 5 && ctx_r3.solicitudService.showPlazo);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.asegurado.entidad.persona.persona_celular);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.showNroCuenta && ctx_r3.solicitudService.persona_banco_account.nrocuenta != "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.id_poliza == 5 && ctx_r3.solicitudService.showPlan);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.solicitudService.id_poliza == 5 && ctx_r3.solicitudService.showPlazo);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("icon", ctx_r3.solicitudService.hasRolConsultaTarjetas || ctx_r3.solicitudService.hasRolConsultaCajero ? "ui-icon-remove-red-eye" : ctx_r3.solicitudService.estadoIniciado.id == ctx_r3.solicitudService.asegurado.instancia_poliza.id_estado + "" ? "ui-icon-edit" : "ui-icon-remove-red-eye");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("parametros_beneficiario", ctx_r3.solicitudService.parametrosYAtributos);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](56, _c4, ctx_r3.solicitudService.estadoIniciado.id + "").includes(ctx_r3.solicitudService.asegurado.instancia_poliza.id_estado + ""));
      }
    }

    function AltaEcoAccidentesComponent_p_dialog_4_p_message_10_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r86 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r86.fechaEmisionForm.controls["fecha_registro"].errors != null ? ctx_r86.fechaEmisionForm.controls["fecha_registro"].errors["required"] ? "La fecha es requerida" : ctx_r86.fechaEmisionForm.controls["fecha_registro"].errors["minlength"] ? "Debe tener por lo menos 6 caracteres" : "" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_dialog_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r88 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-dialog", 103);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_p_dialog_4_Template_p_dialog_visibleChange_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r88);

          var ctx_r87 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r87.solicitudService.displayEmitirSolicitud = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "form", 104);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function AltaEcoAccidentesComponent_p_dialog_4_Template_form_ngSubmit_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r88);

          var ctx_r89 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r89.updateInstanciaPoliza();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "i", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 72);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "La fecha de emisi\xF3n del certificado de seguro sera la siguiente:");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 105);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "span", 106);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "p-calendar", 107);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_dialog_4_Template_p_calendar_ngModelChange_9_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r88);

          var ctx_r90 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r90.solicitudService.instanciaDocumentoCertificado.fecha_emision = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, AltaEcoAccidentesComponent_p_dialog_4_p_message_10_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](11, "button", 109);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx_r4.solicitudService.displayEmitirSolicitud)("modal", true)("minY", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx_r4.fechaEmisionForm);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r4.solicitudService.instanciaDocumentoCertificado.fecha_emision)("minDate", ctx_r4.yesterday)("disabled", false);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r4.fechaEmisionForm.controls["fecha_registro"].valid && ctx_r4.fechaEmisionForm.controls["fecha_registro"].dirty);
      }
    }

    function AltaEcoAccidentesComponent_p_34_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p", 19);
      }

      if (rf & 2) {
        var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx_r5.solicitudService.msgCambioExitoso, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);
      }
    }

    function AltaEcoAccidentesComponent_p_35_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p", 19);
      }

      if (rf & 2) {
        var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx_r6.solicitudService.msgCambioExitoso, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);
      }
    }

    function AltaEcoAccidentesComponent_p_36_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p", 19);
      }

      if (rf & 2) {
        var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx_r7.solicitudService.msgCambioExitoso, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);
      }
    }

    function AltaEcoAccidentesComponent_p_45_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "\xBFDesea imprimir la solicitud de seguro?");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_46_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("\xBFDesea imprimir el comprobante de pago Nro: ", ctx_r9.solicitudService.instanciaDocumentoComprobante.nro_documento + "", "?");
      }
    }

    function AltaEcoAccidentesComponent_p_47_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("\xBFDesea imprimir el certificado de cobertura Nro: ", ctx_r10.solicitudService.instanciaDocumentoCertificado.nro_documento + "", "?");
      }
    }

    function AltaEcoAccidentesComponent_button_51_Template(rf, ctx) {
      if (rf & 1) {
        var _r92 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 111);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_button_51_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r92);

          var ctx_r91 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r91.solicitudService.displayCambioEstadoExitoso = false;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_div_61_Template(rf, ctx) {
      if (rf & 1) {
        var _r94 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 112);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_div_61_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r94);

          var ctx_r93 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r93.crearEcoResguardo();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_div_62_Template(rf, ctx) {
      if (rf & 1) {
        var _r96 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 113);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_div_62_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r96);

          var ctx_r95 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r95.reiniciarEcoAccidentes();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_messages_73_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-messages", 40);
      }

      if (rf & 2) {
        var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("closable", false)("value", ctx_r14.solicitudService.msgs_warn);
      }
    }

    function AltaEcoAccidentesComponent_div_81_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 114);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "p-messages", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("closable", false)("value", ctx_r15.solicitudService.msgs_warn);
      }
    }

    function AltaEcoAccidentesComponent_div_90_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 114);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "p-messages", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("closable", false)("value", ctx_r16.solicitudService.msgs_info_warn);
      }
    }

    function AltaEcoAccidentesComponent_p_dialog_103_Template(rf, ctx) {
      if (rf & 1) {
        var _r98 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-dialog", 115);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_p_dialog_103_Template_p_dialog_visibleChange_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r98);

          var ctx_r97 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r97.solicitudService.displayActualizacionPersona = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "i", 116);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, " Nota: Esto afectara a las anteriores solicitudes que el cliente haya realizado en los distintos productos del Colibri. ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "p-footer");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "button", 117);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_dialog_103_Template_button_click_11_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r98);

          var ctx_r99 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          ctx_r99.solicitudService.displayActualizacionPersona = false;
          return ctx_r99.afterSearch();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "button", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_p_dialog_103_Template_button_click_12_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r98);

          var ctx_r100 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r100.cambiarPersonaExtension(ctx_r100.aseguradoWithDiferentDocIdExt.entidad.persona.id, ctx_r100.aseguradoWithDiferentDocIdExt.id_instancia_poliza, ctx_r100.solicitudService.doc_id, ctx_r100.solicitudService.extension);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx_r17.solicitudService.displayActualizacionPersona)("modal", true)("minY", 70)("position", "center");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate7"]("La informacion proveniente de la busqueda del CI ", ctx_r17.solicitudService.doc_id, " con extensi\xF3n ", ctx_r17.solicitudService.ProcedenciaCIParametroCodDescripcion[ctx_r17.solicitudService.extension + ""], " del cliente ", ctx_r17.aseguradoWithDiferentDocIdExt.entidad.persona.persona_primer_apellido, " ", ctx_r17.aseguradoWithDiferentDocIdExt.entidad.persona.persona_segundo_apellido, " ", ctx_r17.aseguradoWithDiferentDocIdExt.entidad.persona.persona_primer_nombre, ", tiene actualmente el CI ", ctx_r17.aseguradoWithDiferentDocIdExt.entidad.persona.persona_doc_id, " con extensi\xF3n ", ctx_r17.solicitudService.ProcedenciaCIParametroCodDescripcion[ctx_r17.aseguradoWithDiferentDocIdExt.entidad.persona.persona_doc_id_ext], ",");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" Desea Cambiar la extensi\xF3n del cliente a ", ctx_r17.solicitudService.ProcedenciaCIParametroCodDescripcion[ctx_r17.solicitudService.extension + ""], "? ");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_3_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_3_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r126 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r126.userform.controls["persona_primer_apellido"].errors != undefined ? ctx_r126.userform.controls["persona_primer_apellido"].errors["required"] ? "El apellido paterno es requerido" : ctx_r126.userform.controls["persona_primer_apellido"].errors["pattern"] ? "Debe contener solo letras" : "Apellido invalido" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r128 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 130);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_3_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r128);

          var ctx_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r127.solicitudService.persona_banco.paterno = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_3_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r128);

          var ctx_r129 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          ctx_r129.solicitudService.showFormValidation(ctx_r129.userform);
          return ctx_r129.onApellidoInput();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Apellido Paterno ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_3_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_3_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r105 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r105.solicitudService.styledFields("paterno"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r105.solicitudService.persona_banco.paterno)("readonly", !ctx_r105.solicitudService.editPaterno);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r105.solicitudService.atributoApellidoPaterno.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r105.userform.controls["persona_primer_apellido"].errors && !ctx_r105.userform.controls["persona_primer_apellido"].valid && ctx_r105.userform.controls["persona_primer_apellido"].dirty);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r131.userform.controls["persona_segundo_apellido"].errors != undefined ? ctx_r131.userform.controls["persona_segundo_apellido"].errors["required"] ? "El apellido materno es requerido" : ctx_r131.userform.controls["persona_segundo_apellido"].errors["pattern"] ? "Debe contener solo letras" : "apellido inv\xE1lido" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r132 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r132.solicitudService.atributoApellidoMaterno.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r134 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 133);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r134);

          var ctx_r133 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r133.solicitudService.persona_banco.materno = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r134);

          var ctx_r135 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          ctx_r135.solicitudService.showFormValidation(ctx_r135.userform);
          return ctx_r135.onApellidoInput();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Apellido Materno ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r106 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r106.solicitudService.styledFields("materno"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r106.solicitudService.persona_banco.materno)("readonly", !ctx_r106.solicitudService.editMaterno);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r106.solicitudService.atributoApellidoMaterno.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r106.userform.controls["persona_segundo_apellido"].errors && !ctx_r106.userform.controls["persona_segundo_apellido"].valid && ctx_r106.userform.controls["persona_segundo_apellido"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r106.solicitudService.atributoApellidoMaterno.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r137 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r137.userform.controls["persona_primer_nombre"].errors != undefined ? ctx_r137.userform.controls["persona_primer_nombre"].errors["required"] ? "El primer nombre es requerido" : ctx_r137.userform.controls["persona_primer_nombre"].errors["pattern"] ? "Debe contener solo letras" : "nombre inv\xE1lido" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r138 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r138.solicitudService.atributoPrimerNombre.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_Template(rf, ctx) {
      if (rf & 1) {
        var _r140 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 135);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r140);

          var ctx_r139 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r139.solicitudService.persona_banco.nombre = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r140);

          var ctx_r141 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r141.solicitudService.showFormValidation(ctx_r141.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Nombres ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r107 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r107.solicitudService.styledFields("nombre"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r107.solicitudService.persona_banco.nombre)("readonly", !ctx_r107.solicitudService.editPrimerNombre);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r107.solicitudService.atributoPrimerNombre.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r107.userform.controls["persona_primer_nombre"].errors && !ctx_r107.userform.controls["persona_primer_nombre"].valid && ctx_r107.userform.controls["persona_primer_nombre"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r107.solicitudService.atributoPrimerNombre.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r143 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r143.userform.controls["persona_apellido_casada"].errors != undefined ? ctx_r143.userform.controls["persona_apellido_casada"].errors["required"] ? "El apellido de casada es requerido" : ctx_r143.userform.controls["persona_apellido_casada"].errors["pattern"] ? "Debe contener solo letras" : "apellido inv\xE1lido" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r144 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r144.solicitudService.atributoApellidoCasada.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_Template(rf, ctx) {
      if (rf & 1) {
        var _r146 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 136);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r146);

          var ctx_r145 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r145.solicitudService.persona_banco.apcasada = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r146);

          var ctx_r147 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r147.solicitudService.showFormValidation(ctx_r147.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Apellido de casada ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r108 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r108.solicitudService.styledFields("apcasada"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r108.solicitudService.persona_banco.apcasada)("readonly", !ctx_r108.solicitudService.editApCasada);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r108.solicitudService.atributoApellidoCasada.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r108.userform.controls["persona_apellido_casada"].errors && !ctx_r108.userform.controls["persona_apellido_casada"].valid && ctx_r108.userform.controls["persona_apellido_casada"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r108.solicitudService.atributoApellidoCasada.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 140);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r150 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r150.solicitudService.atributoTipoDoc.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_Template(rf, ctx) {
      if (rf & 1) {
        var _r152 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 137);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p-dropdown", 138);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_Template_p_dropdown_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r152);

          var ctx_r151 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r151.solicitudService.TiposDocumentosCodId[ctx_r151.solicitudService.persona_banco_datos.tipo_doc] = $event;
        })("onChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_Template_p_dropdown_onChange_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r152);

          var ctx_r153 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r153.solicitudService.showFormValidation(ctx_r153.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Tipo de Documento ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_p_message_6_Template, 1, 0, "p-message", 139);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r109 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r109.solicitudService.TiposDocumentos)("ngModel", ctx_r109.solicitudService.TiposDocumentosCodId[ctx_r109.solicitudService.persona_banco_datos.tipo_doc])("showClear", true)("disabled", !ctx_r109.solicitudService.editTipoDocumento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r109.solicitudService.atributoTipoDoc.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r109.userform.controls["par_tipo_documento_id"].valid && ctx_r109.userform.controls["par_tipo_documento_id"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r109.solicitudService.atributoTipoDoc.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r155 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r155.userform.controls["persona_doc_id"].errors["required"] ? "Password is required" : ctx_r155.userform.controls["persona_doc_id"].errors["minlength"] ? "Debe tener por lo menos 6 caracteres" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r156 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r156.solicitudService.atributoDocId.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_Template(rf, ctx) {
      if (rf & 1) {
        var _r158 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 141);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r158);

          var ctx_r157 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r157.solicitudService.persona_banco.doc_id = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r158);

          var ctx_r159 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r159.solicitudService.showFormValidation(ctx_r159.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Numero de documento ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r110 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r110.solicitudService.styledFields("doc_id"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r110.solicitudService.persona_banco.doc_id)("readonly", !ctx_r110.solicitudService.editDocId);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r110.solicitudService.atributoDocId.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r110.userform.controls["persona_doc_id"].valid && ctx_r110.userform.controls["persona_doc_id"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r110.solicitudService.atributoDocId.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 140);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r162.solicitudService.atributoDocIdExt.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r164 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p-dropdown", 142);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_Template_p_dropdown_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r164);

          var ctx_r163 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r163.solicitudService.persona_banco.extension = $event;
        })("onChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_Template_p_dropdown_onChange_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r164);

          var ctx_r165 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r165.solicitudService.showFormValidation(ctx_r165.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Extensi\xF3n ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_p_message_6_Template, 1, 0, "p-message", 139);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r111 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r111.solicitudService.styledFields("extension"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r111.solicitudService.ProcedenciaCI)("ngModel", ctx_r111.solicitudService.persona_banco.extension)("showClear", true)("disabled", !ctx_r111.solicitudService.editDocIdExt);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.solicitudService.atributoDocIdExt.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r111.userform.controls["persona_doc_id_ext"].valid && ctx_r111.userform.controls["persona_doc_id_ext"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.solicitudService.atributoDocIdExt.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 145);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r168 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r168.solicitudService.atributoSexo.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_Template(rf, ctx) {
      if (rf & 1) {
        var _r170 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p-dropdown", 143);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_Template_p_dropdown_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r170);

          var ctx_r169 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r169.solicitudService.persona_banco.sexo = $event;
        })("onChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_Template_p_dropdown_onChange_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r170);

          var ctx_r171 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r171.solicitudService.showFormValidation(ctx_r171.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Sexo ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_p_message_6_Template, 1, 0, "p-message", 144);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r112 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r112.solicitudService.styledFields("sexo"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r112.solicitudService.SexosCod)("ngModel", ctx_r112.solicitudService.persona_banco.sexo)("showClear", true)("disabled", !ctx_r112.solicitudService.editSexoId);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r112.solicitudService.atributoSexo.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r112.userform.controls["par_sexo_id"].valid && ctx_r112.userform.controls["par_sexo_id"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r112.solicitudService.atributoSexo.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r173 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r173.userform.controls["persona_fecha_nacimiento"].errors != undefined ? ctx_r173.userform.controls["persona_fecha_nacimiento"].errors["required"] ? "Fecha de nacimiento requerida" : ctx_r173.userform.controls["persona_fecha_nacimiento"].errors["max"] ? "La persona es mayor de 65 a\xF1os" : ctx_r173.userform.controls["persona_fecha_nacimiento"].errors["min"] ? "La persona no es mayor de 18 a\xF1os" : ctx_r173.userform.controls["persona_fecha_nacimiento"].errors["pattern"] ? "Ejemplo de fecha dd/mm/aaaa" : "" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r174 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r174.solicitudService.atributoFechaNacimiento.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_Template(rf, ctx) {
      if (rf & 1) {
        var _r176 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 146);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r176);

          var ctx_r175 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r175.solicitudService.persona_banco.fecha_nacimiento_str = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r176);

          var ctx_r177 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r177.solicitudService.showFormValidation(ctx_r177.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Fecha de Nacimiento ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r113 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r113.solicitudService.styledFields("dia_fechanac"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r113.solicitudService.persona_banco.fecha_nacimiento_str)("readonly", !ctx_r113.solicitudService.editFechaNac)("disabled", !ctx_r113.solicitudService.editFechaNac);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r113.solicitudService.atributoFechaNacimiento.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r113.userform.controls["persona_fecha_nacimiento"].errors && !ctx_r113.userform.controls["persona_fecha_nacimiento"].valid && ctx_r113.userform.controls["persona_fecha_nacimiento"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r113.solicitudService.atributoFechaNacimiento.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 140);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r180 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r180.solicitudService.atributoLugarNacimiento.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_Template(rf, ctx) {
      if (rf & 1) {
        var _r182 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 147);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 148);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182);

          var ctx_r181 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r181.solicitudService.persona_banco_datos.lugar_nacimiento = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r182);

          var ctx_r183 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r183.solicitudService.showFormValidation(ctx_r183.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Lugar de Nacimiento ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_p_message_6_Template, 1, 0, "p-message", 139);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r114 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r114.solicitudService.styledFields("lugar_nacimiento"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r114.solicitudService.persona_banco_datos.lugar_nacimiento)("disabled", !ctx_r114.solicitudService.editLugarNacimiento)("readonly", !ctx_r114.solicitudService.editLugarNacimiento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r114.solicitudService.atributoLugarNacimiento.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r114.userform.controls["par_lugar_nacimiento_id"].errors && !ctx_r114.userform.controls["par_lugar_nacimiento_id"].valid && ctx_r114.userform.controls["par_lugar_nacimiento_id"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r114.solicitudService.atributoLugarNacimiento.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_span_14_span_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_span_14_Template(rf, ctx) {
      if (rf & 1) {
        var _r186 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 149);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 150);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_span_14_Template_input_ngModelChange_1_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r186);

          var ctx_r185 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r185.solicitudService.persona_banco_datos.ocupacion = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_span_14_Template_input_change_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r186);

          var ctx_r187 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r187.solicitudService.showFormValidation(ctx_r187.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Ocupacion ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_span_14_span_4_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r115 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r115.solicitudService.persona_banco_datos.ocupacion)("readonly", !ctx_r115.solicitudService.editOcupacion);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r115.solicitudService.atributoOcupacion.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_p_message_15_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r116 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r116.solicitudService.atributoOcupacion.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r189 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r189.userform.controls["persona_telefono_domicilio"].errors != undefined ? ctx_r189.userform.controls["persona_telefono_domicilio"].errors["required"] ? "El tel\xE9fono de domicilio es requerido" : ctx_r189.userform.controls["persona_telefono_domicilio"].errors["minlength"] ? ctx_r189.minLengthMsgCelular : ctx_r189.userform.controls["persona_telefono_domicilio"].errors["maxlength"] ? ctx_r189.minLengthMsgCelular : ctx_r189.userform.controls["persona_telefono_domicilio"].errors["pattern"] ? "Debe contener solo n\xFAmeros" : "N\xFAmero de telefono inv\xE1lido" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r190 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r190.solicitudService.atributoTelefonoDomicilio.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_Template(rf, ctx) {
      if (rf & 1) {
        var _r192 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 151);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r192);

          var ctx_r191 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r191.solicitudService.persona_banco.fono_domicilio = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r192);

          var ctx_r193 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r193.solicitudService.showFormValidation(ctx_r193.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Tel\xE9fono de domicilio ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r117 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r117.solicitudService.styledFields("fono_domicilio"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r117.solicitudService.persona_banco.fono_domicilio)("readonly", !ctx_r117.solicitudService.editTelefonoDomicilio);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r117.solicitudService.atributoTelefonoDomicilio.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r117.userform.controls["persona_telefono_domicilio"].valid && ctx_r117.userform.controls["persona_telefono_domicilio"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r117.solicitudService.atributoTelefonoDomicilio.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r195 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r195.userform.controls["persona_telefono_celular"].errors != undefined ? ctx_r195.userform.controls["persona_telefono_celular"].errors["required"] ? "El tel\xE9fono de celular es requerido" : ctx_r195.userform.controls["persona_telefono_celular"].errors["minlength"] ? ctx_r195.minLengthMsgCelular : ctx_r195.userform.controls["persona_telefono_celular"].errors["maxlength"] ? ctx_r195.minLengthMsgCelular : ctx_r195.userform.controls["persona_telefono_celular"].errors["pattern"] ? "Debe contener solo n\xFAmeros" : "N\xFAmero de telefono inv\xE1lido" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r196 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r196.solicitudService.atributoTelefonoCelular.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_Template(rf, ctx) {
      if (rf & 1) {
        var _r198 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 152);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r198);

          var ctx_r197 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r197.solicitudService.persona_banco.nro_celular = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r198);

          var ctx_r199 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r199.solicitudService.showFormValidation(ctx_r199.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Tel\xE9fono celular ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r118 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r118.solicitudService.styledFields("nro_celular"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r118.solicitudService.persona_banco.nro_celular)("readonly", !ctx_r118.solicitudService.editTelefonoCelular);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r118.solicitudService.atributoTelefonoCelular.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r118.userform.controls["persona_telefono_celular"].valid && ctx_r118.userform.controls["persona_telefono_celular"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r118.solicitudService.atributoTelefonoCelular.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_18_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_18_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r201 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r201.userform.controls["par_mail_id"].errors != undefined ? ctx_r201.userform.controls["par_mail_id"].errors["required"] ? "El correo electronico es requerido" : ctx_r201.userform.controls["par_mail_id"].errors["mail"] ? "Debe introducir un correo valido" : "Correo invalido" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_18_Template(rf, ctx) {
      if (rf & 1) {
        var _r203 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 153);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_18_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r203);

          var ctx_r202 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r202.solicitudService.persona_banco.e_mail = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_18_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r203);

          var ctx_r204 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r204.solicitudService.showFormValidation(ctx_r204.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Correo Electronico ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_18_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_18_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r119 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r119.solicitudService.styledFields("e_mail"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r119.solicitudService.persona_banco.e_mail)("readonly", !ctx_r119.solicitudService.editEmail);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r119.solicitudService.atributoEmail.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r119.userform.controls["par_mail_id"].valid && ctx_r119.userform.controls["par_mail_id"].dirty);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 140);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r207 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r207.solicitudService.atributoDepartamento.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_Template(rf, ctx) {
      if (rf & 1) {
        var _r209 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 147);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 154);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r209);

          var ctx_r208 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r208.solicitudService.persona_banco.departamento = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r209);

          var ctx_r210 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r210.solicitudService.showFormValidation(ctx_r210.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Departamento");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_p_message_6_Template, 1, 0, "p-message", 139);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r120 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r120.solicitudService.styledFields("departamento"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r120.solicitudService.persona_banco.departamento)("disabled", !ctx_r120.solicitudService.editDepartamento)("readonly", !ctx_r120.solicitudService.editDepartamento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r120.solicitudService.atributoDepartamento.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r120.userform.controls["par_departamento_id"].errors && !ctx_r120.userform.controls["par_departamento_id"].valid && ctx_r120.userform.controls["par_departamento_id"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r120.solicitudService.atributoDepartamento.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 140);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r213 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r213.solicitudService.atributoLocalidad.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_Template(rf, ctx) {
      if (rf & 1) {
        var _r215 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 147);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 155);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r215);

          var ctx_r214 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r214.solicitudService.persona_banco.localidad = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r215);

          var ctx_r216 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r216.solicitudService.showFormValidation(ctx_r216.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Ciudad ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_p_message_6_Template, 1, 0, "p-message", 139);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r121 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r121.solicitudService.styledFields("localidad"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r121.solicitudService.persona_banco.localidad)("disabled", !ctx_r121.solicitudService.editLocalidad)("readonly", !ctx_r121.solicitudService.editLocalidad);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r121.solicitudService.atributoLocalidad.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r121.userform.controls["par_localidad_id"].errors && !ctx_r121.userform.controls["par_localidad_id"].valid && ctx_r121.userform.controls["par_localidad_id"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r121.solicitudService.atributoLocalidad.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 140);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r219 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r219.solicitudService.atributoProvincia.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_Template(rf, ctx) {
      if (rf & 1) {
        var _r221 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 147);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 156);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r221);

          var ctx_r220 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r220.solicitudService.persona_banco_datos.provincia = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r221);

          var ctx_r222 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r222.solicitudService.showFormValidation(ctx_r222.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Provincia ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_p_message_6_Template, 1, 0, "p-message", 139);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r122 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r122.solicitudService.styledFields("provincia"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r122.solicitudService.persona_banco_datos.provincia)("disabled", !ctx_r122.solicitudService.editProvincia)("readonly", !ctx_r122.solicitudService.editProvincia);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r122.solicitudService.atributoProvincia.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r122.userform.controls["par_provincia_id"].errors && !ctx_r122.userform.controls["par_provincia_id"].valid && ctx_r122.userform.controls["par_provincia_id"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r122.solicitudService.atributoProvincia.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r224 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r224.userform.controls["persona_direccion_domicilio"].errors != undefined ? ctx_r224.userform.controls["persona_direccion_domicilio"].errors["required"] ? "Direccion de domicilio requerido" : ctx_r224.userform.controls["persona_direccion_domicilio"].errors["minlength"] ? "Debe tener por lo menos 6 caracteres" : "" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r225 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r225.solicitudService.atributoDireccionDomicilio.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_Template(rf, ctx) {
      if (rf & 1) {
        var _r227 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 157);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r227);

          var ctx_r226 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r226.solicitudService.persona_banco.direccion = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r227);

          var ctx_r228 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r228.solicitudService.showFormValidation(ctx_r228.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Dirrecci\xF3n domicilio ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r123 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("classList", ctx_r123.solicitudService.styledFields("direccion"));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r123.solicitudService.persona_banco.direccion)("readonly", !ctx_r123.solicitudService.editDireccionDomicilio);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r123.solicitudService.atributoDireccionDomicilio.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r123.userform.controls["persona_direccion_domicilio"].valid && ctx_r123.userform.controls["persona_direccion_domicilio"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r123.solicitudService.atributoDireccionDomicilio.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_p_message_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r230 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r230.userform.controls["persona_direccion_domicilio"].errors != undefined ? ctx_r230.userform.controls["persona_direccion_domicilio"].errors["required"] ? "Direccion de domicilio requerido" : ctx_r230.userform.controls["persona_direccion_domicilio"].errors["minlength"] ? "Debe tener por lo menos 6 caracteres" : "" : "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r231 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r231.solicitudService.atributoDireccionLaboral.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_Template(rf, ctx) {
      if (rf & 1) {
        var _r233 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 158);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "input", 159);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_Template_input_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r233);

          var ctx_r232 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r232.solicitudService.persona_banco_datos.direccion_laboral = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_Template_input_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r233);

          var ctx_r234 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r234.solicitudService.showFormValidation(ctx_r234.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Dirrecci\xF3n Laboral ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_p_message_6_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r124 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r124.solicitudService.persona_banco_datos.direccion_laboral)("readonly", !ctx_r124.solicitudService.editDireccionLaboral);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r124.solicitudService.atributoDireccionLaboral.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r124.userform.controls["persona_direccion_domicilio"].valid && ctx_r124.userform.controls["persona_direccion_domicilio"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r124.solicitudService.atributoDireccionLaboral.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-fieldset", 126);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 127);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_3_Template, 7, 5, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_4_Template, 8, 6, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_5_Template, 8, 6, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_6_Template, 8, 6, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_7_Template, 8, 7, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_8_Template, 8, 6, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_9_Template, 8, 8, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_10_Template, 8, 8, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_11_Template, 8, 7, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_12_Template, 8, 7, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 123);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_span_14_Template, 5, 3, "span", 128);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_p_message_15_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_16_Template, 8, 6, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_17_Template, 8, 6, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_18_Template, 7, 5, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_19_Template, 8, 7, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_20_Template, 8, 7, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_21_Template, 8, 7, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](22, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_22_Template, 8, 6, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](23, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_div_23_Template, 8, 5, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r101 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showPaterno);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showMaterno);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showPrimerNombre);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showApCasada);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showTipoDocumento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showDocId);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showDocIdExt);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showSexoId);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showFechaNac);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showLugarNacimiento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showOcupacion);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.atributoOcupacion.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showTelefonoDomicilio);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showTelefonoCelular);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showEmail);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showDepartamento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showLocalidad);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showProvincia);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showDireccionDomicilio);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r101.solicitudService.showDireccionLaboral);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_3_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r245 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 158);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p-dropdown", 161);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_3_Template_p_dropdown_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r245);

          var ctx_r244 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r244.solicitudService.poliza.id = $event;
        })("onChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_3_Template_p_dropdown_onChange_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r245);

          var ctx_r246 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r246.solicitudService.showFormValidation(ctx_r246.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Tipo Seguro ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_3_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r235 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r235.solicitudService.TipoSeguros)("ngModel", ctx_r235.solicitudService.poliza.id)("showClear", true)("disabled", !ctx_r235.solicitudService.editTipoSeguro);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r235.solicitudService.atributoTipoSeguro.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_4_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r249 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 158);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p-dropdown", 162);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_4_Template_p_dropdown_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r249);

          var ctx_r248 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r248.solicitudService.persona_banco_solicitud.solicitud_sci_selected = $event;
        })("onChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_4_Template_p_dropdown_onChange_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r249);

          var ctx_r250 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          ctx_r250.solicitudService.showFormValidation(ctx_r250.userform);
          return ctx_r250.getDatosFromSolicitudPrimeraEtapaService(ctx_r250.solicitudService.persona_banco_solicitud.solicitud_sci_selected);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Nro Solicitud de Credito ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_4_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r236 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r236.solicitudService.SolicitudesSci)("ngModel", ctx_r236.solicitudService.persona_banco_solicitud.solicitud_sci_selected)("showClear", true)("disabled", !ctx_r236.solicitudService.editNroSolicitudSci);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r236.solicitudService.atributoSolicitudSci.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_6_span_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_6_Template(rf, ctx) {
      if (rf & 1) {
        var _r253 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 149);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 163);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_6_Template_input_ngModelChange_1_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r253);

          var ctx_r252 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r252.solicitudService.persona_banco_operacion.Operacion_Tipo_Credito = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_6_Template_input_change_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r253);

          var ctx_r254 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r254.solicitudService.showFormValidation(ctx_r254.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Tipo de Cr\xE9dito");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_6_span_4_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r237 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r237.solicitudService.persona_banco_operacion.Operacion_Tipo_Credito)("readonly", !ctx_r237.solicitudService.editOperacionTipoCredito);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r237.solicitudService.atributoTipoCredito.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_8_span_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_8_Template(rf, ctx) {
      if (rf & 1) {
        var _r257 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 149);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 164);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_8_Template_input_ngModelChange_1_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r257);

          var ctx_r256 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r256.solicitudService.persona_banco_operacion.Operacion_Plazo = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_8_Template_input_change_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r257);

          var ctx_r258 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r258.solicitudService.showFormValidation(ctx_r258.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Plazo del cr\xE9dito en meses");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_8_span_4_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r238 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r238.solicitudService.persona_banco_operacion.Operacion_Plazo)("readonly", !ctx_r238.solicitudService.editSolicitudPlazoCredito);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r238.solicitudService.atributoSolicitudPlazoCredito.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_10_span_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_10_Template(rf, ctx) {
      if (rf & 1) {
        var _r261 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 149);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 165);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_10_Template_input_ngModelChange_1_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r261);

          var ctx_r260 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r260.solicitudService.persona_banco_solicitud.solicitud_prima_total = $event;
        })("change", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_10_Template_input_change_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r261);

          var ctx_r262 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r262.solicitudService.showFormValidation(ctx_r262.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_10_span_4_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r239 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r239.solicitudService.persona_banco_solicitud.solicitud_prima_total)("readonly", !ctx_r239.solicitudService.editSolicitudPrimaTotal);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Monto Total de la Prima en ", ctx_r239.solicitudService.persona_banco_operacion ? ctx_r239.solicitudService.MonedasDescripcion[ctx_r239.solicitudService.persona_banco_operacion.Operacion_Moneda] : "", "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r239.solicitudService.atributoSolicitudPrimaTotal.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_p_message_11_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r240 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r240.solicitudService.atributoSolicitudPrimaTotal.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_12_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_12_Template(rf, ctx) {
      if (rf & 1) {
        var _r265 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 158);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p-dropdown", 166);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_12_Template_p_dropdown_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r265);

          var ctx_r264 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r264.solicitudService.persona_banco_solicitud.solicitud_forma_pago = $event;
        })("onChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_12_Template_p_dropdown_onChange_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r265);

          var ctx_r266 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r266.solicitudService.showFormValidation(ctx_r266.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Forma de Pago");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_12_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "span", 167);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r241 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r241.solicitudService.FormasPago)("ngModel", ctx_r241.solicitudService.persona_banco_solicitud.solicitud_forma_pago)("showClear", true)("disabled", !ctx_r241.solicitudService.editFormaPago);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r241.solicitudService.atributoFormaPago.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_13_span_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 132);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(*)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_13_p_message_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 134);
      }

      if (rf & 2) {
        var ctx_r268 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r268.solicitudService.atributoDebitoAutomatico.tipo_error);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_13_Template(rf, ctx) {
      if (rf & 1) {
        var _r270 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 158);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p-dropdown", 168);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_13_Template_p_dropdown_ngModelChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r270);

          var ctx_r269 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r269.solicitudService.persona_banco.debito_automatico = $event;
        })("onChange", function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_13_Template_p_dropdown_onChange_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r270);

          var ctx_r271 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          ctx_r271.selectPagoEfectivo($event);
          return ctx_r271.solicitudService.showFormValidation(ctx_r271.userform);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "M\xE9todo de Pago");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_13_span_5_Template, 2, 0, "span", 131);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "span", 167);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_13_p_message_7_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r242 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r242.solicitudService.Condiciones)("ngModel", ctx_r242.solicitudService.persona_banco.debito_automatico)("showClear", true)("disabled", !ctx_r242.solicitudService.editPagoEfectivo);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r242.solicitudService.atributoDebitoAutomatico.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r242.solicitudService.atributoDebitoAutomatico.requerido);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-fieldset", 160);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 127);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_3_Template, 6, 5, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_4_Template, 6, 5, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_6_Template, 5, 3, "span", 128);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_8_Template, 5, 3, "span", 128);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_span_10_Template, 5, 4, "span", 128);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_p_message_11_Template, 1, 1, "p-message", 129);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_12_Template, 7, 5, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_div_13_Template, 8, 6, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r102 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r102.solicitudService.showTipoSeguro);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r102.solicitudService.showNroSolicitudSci);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r102.solicitudService.showTipoCredito);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r102.solicitudService.showSolicitudPlazoCredito);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r102.solicitudService.showSolicitudPrimaTotal);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r102.solicitudService.atributoSolicitudPrimaTotal.requerido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r102.solicitudService.showFormaPago);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r102.solicitudService.showPagoEfectivo);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_div_10_label_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r272 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"]("Prima ", ctx_r272.solicitudService.poliza.anexo_poliza.par_moneda.parametro_abreviacion, " ", ctx_r272.solicitudService.atributoSolicitudPrimaTotal.valor, "");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_div_10_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 149);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AltaEcoAccidentesComponent_p_panel_244_div_10_label_2_Template, 2, 2, "label", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r103 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r103.solicitudService.atributoSolicitudPrimaTotal);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_ng_template_5_Template(rf, ctx) {
      if (rf & 1) {
        var _r278 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 176);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Dato ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 177);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "i", 178);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "input", 179);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("input", function AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_ng_template_5_Template_input_input_5_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r278);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          var _r273 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](4);

          return _r273.filterGlobal($event.target.value, "contains");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_ng_template_6_th_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var col_r281 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", col_r281.header, " ");
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_ng_template_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_ng_template_6_th_1_Template, 2, 1, "th", 180);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var columns_r279 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", columns_r279);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_ng_template_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr", 181);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var atributo_instancia_poliza_r282 = ctx.$implicit;

        var ctx_r276 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("pSelectableRow", ctx_r276.atributo_instancia_poliza_selected);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](atributo_instancia_poliza_r282.objeto_x_atributo.atributo.descripcion);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](atributo_instancia_poliza_r282.valor);
      }
    }

    var _c5 = function _c5() {
      return ["valor"];
    };

    function AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_Template(rf, ctx) {
      if (rf & 1) {
        var _r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-tabPanel", 169);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-fieldset", 170);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "p-table", 171, 172);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("selectionChange", function AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_Template_p_table_selectionChange_3_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r284);

          var ctx_r283 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r283.atributo_instancia_poliza_selected = $event;
        })("onRowSelect", function AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_Template_p_table_onRowSelect_3_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r284);

          var ctx_r285 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r285.onRowSelectDato_complementario($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_ng_template_5_Template, 6, 0, "ng-template", 173);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_ng_template_6_Template, 2, 1, "ng-template", 174);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_ng_template_7_Template, 5, 3, "ng-template", 175);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r104 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("columns", ctx_r104.cols)("value", ctx_r104.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter)("paginator", false)("rows", 10)("globalFilterFields", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](6, _c5))("selection", ctx_r104.atributo_instancia_poliza_selected);
      }
    }

    function AltaEcoAccidentesComponent_p_panel_244_Template(rf, ctx) {
      if (rf & 1) {
        var _r287 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-panel", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onBeforeToggle", function AltaEcoAccidentesComponent_p_panel_244_Template_p_panel_onBeforeToggle_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r287);

          var ctx_r286 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          ctx_r286.solicitudService.displayModalFormTitular = false;
          return ctx_r286.beforeToggleFormTitular();
        })("collapsedChange", function AltaEcoAccidentesComponent_p_panel_244_Template_p_panel_collapsedChange_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r287);

          var ctx_r288 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r288.collapsedFormTitular = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-tabView", 118);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "p-tabPanel", 119);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "form", 120);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function AltaEcoAccidentesComponent_p_panel_244_Template_form_ngSubmit_3_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r287);

          var ctx_r289 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r289.guardarSolicitudValidando();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_4_Template, 24, 20, "p-fieldset", 121);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AltaEcoAccidentesComponent_p_panel_244_p_fieldset_5_Template, 14, 8, "p-fieldset", 122);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "p-fieldset");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 123);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](8, "button", 124);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 123);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, AltaEcoAccidentesComponent_p_panel_244_div_10_Template, 3, 1, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, AltaEcoAccidentesComponent_p_panel_244_p_tabPanel_11_Template, 8, 7, "p-tabPanel", 125);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("header", "SOLICITUD ", ctx_r18.solicitudService.poliza.descripcion, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("collapsed", ctx_r18.collapsedFormTitular);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx_r18.userform);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r18.solicitudService.showSecRegistroTitular);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r18.solicitudService.showSecFormaPago);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r18.userform.valid || ctx_r18.solicitudService.disableToSave);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r18.solicitudService.asegurado.instancia_poliza.anexo_poliza && ctx_r18.solicitudService.showPrima);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r18.displayFormTitular);
      }
    }

    function AltaEcoAccidentesComponent_div_245_p_message_11_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-message", 110);
      }

      if (rf & 2) {
        var ctx_r290 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("text", ctx_r290.formBusqueda.controls["persona_doc_id"].errors != undefined ? ctx_r290.formBusqueda.controls["persona_doc_id"].errors["required"] ? "El documento de identidad es requerido" : ctx_r290.formBusqueda.controls["persona_doc_id"].errors["pattern"] ? "Debe introducir solo n\xFAmeros" : "" : "");
      }
    }

    function AltaEcoAccidentesComponent_div_245_Template(rf, ctx) {
      if (rf & 1) {
        var _r292 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 182);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "form", 104);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function AltaEcoAccidentesComponent_div_245_Template_form_ngSubmit_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r292);

          var ctx_r291 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r291.BuscarCliente();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 127);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "p-fieldset", 183);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 127);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 184);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "span", 149);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "input", 185);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_div_245_Template_input_ngModelChange_8_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r292);

          var ctx_r293 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r293.solicitudService.doc_id = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "Cedula de identidad");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, AltaEcoAccidentesComponent_div_245_p_message_11_Template, 1, 1, "p-message", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 184);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "span", 149);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "p-dropdown", 186);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AltaEcoAccidentesComponent_div_245_Template_p_dropdown_ngModelChange_14_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r292);

          var ctx_r294 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r294.solicitudService.extension = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Extensi\xF3n");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 184);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "button", 187);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx_r19.formBusqueda);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r19.solicitudService.doc_id);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r19.formBusqueda.controls["persona_doc_id"].valid && ctx_r19.formBusqueda.controls["persona_doc_id"].dirty);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r19.solicitudService.ProcedenciaCI)("ngModel", ctx_r19.solicitudService.extension)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r19.formBusqueda.valid && !ctx_r19.formBusqueda.valid === true);
      }
    }

    var _c6 = function _c6() {
      return {
        marginTop: "70px"
      };
    };

    var AltaEcoAccidentesComponent = /*#__PURE__*/function () {
      function AltaEcoAccidentesComponent(params, breadcrumbService, changeDetection, parametroService, anexoService, fb, personaService, beneficiarioService, soapuiService, atributoService, objetoAtributoService, documentoService, polizaService, router, instanciaPolizaService, messageService, sanitizer, reporteService, rolesService, service, usuarioService, contextoService, instanciaPolizaTransService, cdRef, solicitudService, planPagoService, archivoService, sessionStorageService, adminPermisosService) {
        _classCallCheck(this, AltaEcoAccidentesComponent);

        this.params = params;
        this.breadcrumbService = breadcrumbService;
        this.changeDetection = changeDetection;
        this.parametroService = parametroService;
        this.anexoService = anexoService;
        this.fb = fb;
        this.personaService = personaService;
        this.beneficiarioService = beneficiarioService;
        this.soapuiService = soapuiService;
        this.atributoService = atributoService;
        this.objetoAtributoService = objetoAtributoService;
        this.documentoService = documentoService;
        this.polizaService = polizaService;
        this.router = router;
        this.instanciaPolizaService = instanciaPolizaService;
        this.messageService = messageService;
        this.sanitizer = sanitizer;
        this.reporteService = reporteService;
        this.rolesService = rolesService;
        this.service = service;
        this.usuarioService = usuarioService;
        this.contextoService = contextoService;
        this.instanciaPolizaTransService = instanciaPolizaTransService;
        this.cdRef = cdRef;
        this.solicitudService = solicitudService;
        this.planPagoService = planPagoService;
        this.archivoService = archivoService;
        this.sessionStorageService = sessionStorageService;
        this.adminPermisosService = adminPermisosService; // @ViewChild("componenteAnexo", {static:false}) private componenteAnexo:AnexoComponent;

        this.color = 'primary';
        this.mode = 'indeterminate';
        this.value = 1;
        this.show = false;
        this.yesterday = new Date();
        this.displayFormTitular = true;
        this.displayDatosTitular = false;
        this.displayErrorMontoCuota = false;
        this.displayErrorPlanPago = false;
        this.displayTarjetaInvalida = false;
        this.displayTarjetaInvalidaContinuar = false;
        this.displayBeneficiariosExcedidos = false;
        this.displayBeneficiariosTitular = false;
        this.displayBeneficiariosNotFound = false;
        this.displayBeneficiarioFound = false;
        this.collapsedFormTitular = false;
        this.collapsedDatosTitular = false;
        this.displayClienteNoExiste = false;
        this.displayEdadIncorrecta = false;
        this.displaySolicitudDuplicado = false;
        this.displayActualizacionExitoso = false;
        this.displayIntroduscaNroTarjeta = false;
        this.displayNroTarjetaSinCuentas = false;
        this.displayNroCuentasActualizados = false; // beneficiario: Persona = new Persona();

        this.beneficiario = new _src_core_modelos_persona__WEBPACK_IMPORTED_MODULE_9__["Persona"]();
        this.anexosPlanesPoliza = [];
        this.Beneficiarios = [];
        this.BeneficiariosAux = [];
        this.numeroBeneficiarios = 2;
        this.docId = null;
        this.minLengthMsg = '';
        this.minLengthMsgCelular = '';
        this.minlength = 0;
        this.minlengthCelular = 0;
        this.docIdExt = null;
        this.display = false;
        this.displayBeneficiario = false;
        this.displayBeneficiarioDouble = false;
        this.displayBeneficiariosCambio = false;
        this.showEnviarDocumentos = true;
        this.displayEliminarBeneficiario = false;
        this.displayEliminarBeneficiarioOK = false;
        this.SucursalesId = [];
        this.AgenciasId = [];
        this.MonedasParametroDescripcion = [];
        this.parametros = [];
        this.nuevo = true; // atribut_instancia_poliza:Atributo_instancia_poliza[];
        // dato_complementario: [];
        //selected_atributo_instancia_poliza:Atributo_instancia_poliza;

        this.atributo_instancia_poliza_selected = [];
        this.doc_id = 0;
        this.ext = 0; //  titular: Persona = new Persona;
        // atributo_instancia_poliza_cuenta: Atributo_instancia_poliza = new Atributo_instancia_poliza();

        this.persona_banco_tarjetas_debito = [];
        this.plan = new _src_core_modelos_anexo_poliza__WEBPACK_IMPORTED_MODULE_8__["Anexo_poliza"]();
        this.events = [];
        this.messages = [];
        this.maxLengthTarjeta = 1;
        this.guardandoTitular = false;
        this.editandoBeneficiario = false;
        this.fromSearch = false;
        this.totalPorcentaje = 0;
        this.showErrors = false;
        this.messaje = "";
        this.withErrorsOrWarnings = false; // benificiarioAux: Beneficiario[] = [];
        // btnValidarYContinuarEnabled = false;
        // btnOrdenPagoEnabled = false;

        this.btnEnviarDocumentosEnabled = false;
        this.btnTarjetaDebitoCuentasEnabled = true;
        this.showValidarYContinuar = true;
        this.showEmitirCertificado = true; // msgText: string;

        this.validarTarjeta = false;
        this.reloadBeneficiarioComponent = true;
        this.util = new _src_helpers_util__WEBPACK_IMPORTED_MODULE_46__["Util"]();
        this.apellidos = []; // edadMinimaYears: number = 18 // 18 Años;
        // edadMaximaYears: number = 65// 65 años;

        this.idsPerfiles = [];
        this.stopSaving = false;
        this.stopSending = false;
        this.fechaEdadMinima = new Date();
        this.stopSavingByEdad = false;
        this.stopSavingByLastName = false;
        this.stopSavingBySolicitud = false;
        this.stopSavingByMontoCuota = false;
        this.validandoContinuando = false;
        this.splitButtonItems = [{
          label: 'Update',
          icon: 'ui-icon-update'
        }, {
          label: 'Delete',
          icon: 'ui-icon-close'
        }, {
          label: 'Home',
          icon: 'ui-icon-home',
          url: 'http://www.primefaces.org/primeng'
        }];
        this.search = false;
        this.solicitudesObservaciones = [];
        this.reloadData = false;
      }

      _createClass(AltaEcoAccidentesComponent, [{
        key: "beforeToggleDatosTitular",
        value: function beforeToggleDatosTitular() {
          var reload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
          this.sessionStorageService.setItemSync("paramsDeleted", 'true');
          this.solicitudService.displayModalDatosTitular = false;
          this.solicitudService.parametrosRuteo.openForm = false;
          this.solicitudService.changeView(this.userform, this.solicitudService.asegurado);
          this.onInit(10, null, null, false);
        }
      }, {
        key: "beforeToggleFormTitular",
        value: function beforeToggleFormTitular() {
          this.solicitudService.displayModalFormTitular = false;
          this.solicitudService.parametrosRuteo.openForm = false;
          this.solicitudService.changeView(this.userform, this.solicitudService.asegurado);
          this.onInit(10, this.solicitudService.doc_id, parseInt(this.solicitudService.extension + ''), false);
        }
      }, {
        key: "setFeaturesBeneficiarios",
        value: function setFeaturesBeneficiarios(beneficiarioResp) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee8() {
            var _this15 = this;

            return _regeneratorRuntime().wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    if (beneficiarioResp != undefined) {
                      if (beneficiarioResp.length) {
                        beneficiarioResp.forEach(function (beneficiario) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this15, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee7() {
                            return _regeneratorRuntime().wrap(function _callee7$(_context7) {
                              while (1) {
                                switch (_context7.prev = _context7.next) {
                                  case 0:
                                    beneficiario.persona_doc_id_ext = beneficiario.entidad != undefined ? beneficiario.entidad.persona.persona_doc_id_ext : beneficiario.persona_doc_id_ext != undefined ? beneficiario.persona_doc_id_ext : '';
                                    beneficiario.persona_primer_apellido = beneficiario.entidad != undefined ? beneficiario.entidad.persona.persona_primer_apellido : beneficiario.persona_primer_apellido != undefined ? beneficiario.persona_primer_apellido : '';
                                    beneficiario.persona_segundo_apellido = beneficiario.entidad != undefined ? beneficiario.entidad.persona.persona_segundo_apellido : beneficiario.persona_segundo_apellido != undefined ? beneficiario.persona_segundo_apellido : '';

                                  case 3:
                                  case "end":
                                    return _context7.stop();
                                }
                              }
                            }, _callee7);
                          }));
                        });
                      }
                    }

                  case 1:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.onInit();
          this.reloadData = true;
          this.solicitudService.parametrosRuteo.openForm = false;
        }
      }, {
        key: "onInit",
        value: function onInit() {
          var _this16 = this;

          var id_poliza = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          var docId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
          var docIdExt = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
          var search = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
          var updatePersona = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;
          this.solicitudService.product = this.solicitudService.ruta;
          this.solicitudService.persona_banco_datos = new _src_core_modelos_persona_banco_datos__WEBPACK_IMPORTED_MODULE_37__["persona_banco_datos"]();
          this.solicitudService.persona_banco = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco"]();
          this.solicitudService.persona_banco_account = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();
          this.solicitudService.persona_banco_solicitud = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco_solicitud"]();
          this.solicitudService.persona_banco_operacion = new _src_core_modelos_persona_banco_operacion__WEBPACK_IMPORTED_MODULE_34__["persona_banco_operacion"]();
          this.solicitudService.persona_banco_pep = new _src_core_modelos_persona_banco_pep__WEBPACK_IMPORTED_MODULE_36__["persona_banco_pep"]();
          this.solicitudService.fileDocumentoSolicitud = new _src_core_modelos_upload__WEBPACK_IMPORTED_MODULE_41__["Upload"]();
          this.solicitudService.fileDocumentoCertificado = new _src_core_modelos_upload__WEBPACK_IMPORTED_MODULE_41__["Upload"]();
          this.solicitudService.isInAltaSolicitud = true;
          this.solicitudService.solicitudAprobada = false;
          this.solicitudService.paramsLoaded = false;
          this.solicitudService.isLoadingAgain = true;
          this.solicitudService.displayBusquedaCI = true;
          this.solicitudService.destinatariosCorreos = '';
          this.solicitudService.paramsLoaded = false;
          this.solicitudService.displayModalDatosTitular = false;
          this.solicitudService.parametrosRuteo.parametro_vista = id_poliza + '';
          this.solicitudService.constructComponent(function () {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this16, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee12() {
              var _this17 = this;

              return _regeneratorRuntime().wrap(function _callee12$(_context12) {
                while (1) {
                  switch (_context12.prev = _context12.next) {
                    case 0:
                      this.getPoliza(function () {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this17, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee11() {
                          var _this18 = this;

                          var paramsDeleted;
                          return _regeneratorRuntime().wrap(function _callee11$(_context11) {
                            while (1) {
                              switch (_context11.prev = _context11.next) {
                                case 0:
                                  paramsDeleted = this.sessionStorageService.getItemSync("paramsDeleted");
                                  this.solicitudService.TipoSeguros = [{
                                    label: this.solicitudService.poliza.descripcion,
                                    value: this.solicitudService.poliza.id
                                  }];
                                  this.setUserForm();

                                  if (!paramsDeleted && this.solicitudService.parametrosRuteo.parametro_ruteo !== null && this.solicitudService.parametrosRuteo.parametro_ruteo !== "undefined" && this.solicitudService.parametrosRuteo.parametro_ruteo !== "{}" && Object.keys(this.solicitudService.parametrosRuteo.parametro_ruteo).length) {
                                    this.solicitudService.isLoadingAgain = true;
                                    this.solicitudService.editandoTitular = false;
                                    this.solicitudService.displayBusquedaCI = false;
                                    this.solicitudService.displayModalFormTitular = false;
                                    this.id_instancia_poliza = this.solicitudService.parametrosRuteo.parametro_ruteo ? this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza ? this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza : null : null; // this.iniciarSolicitud();

                                    this.stateButtonsFlow();
                                    this.personaService.findPersonaSolicitudByIdInstanciaPoliza(this.solicitudService.objetoAseguradoDatosComplementarios.id, this.id_instancia_poliza).subscribe(function (res) {
                                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this18, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee10() {
                                        var _this19 = this;

                                        var resp;
                                        return _regeneratorRuntime().wrap(function _callee10$(_context10) {
                                          while (1) {
                                            switch (_context10.prev = _context10.next) {
                                              case 0:
                                                resp = res;

                                                if (!Object.keys(resp.data)) {
                                                  _context10.next = 8;
                                                  break;
                                                }

                                                this.solicitudService.asegurado = resp.data; // await this.solicitudService.setDatesOfAsegurado();

                                                this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = []; // this.solicitudService.setAtributosToPersonaBanco(this.userform, async () => {
                                                //this.solicitudService.setAtributosToAsegurado();

                                                _context10.next = 6;
                                                return this.initInstanciaPoliza(function () {
                                                  return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this19, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee9() {
                                                    return _regeneratorRuntime().wrap(function _callee9$(_context9) {
                                                      while (1) {
                                                        switch (_context9.prev = _context9.next) {
                                                          case 0:
                                                            if (!(this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id)) {
                                                              _context9.next = 5;
                                                              break;
                                                            }

                                                            if (!(this.reloadData && this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id)) {
                                                              _context9.next = 5;
                                                              break;
                                                            }

                                                            this.reloadData = false;
                                                            _context9.next = 5;
                                                            return this.getDatosFromCustomerService(this.solicitudService.asegurado.entidad.persona.persona_doc_id, this.solicitudService.asegurado.entidad.persona.persona_doc_id_ext + '', null);

                                                          case 5:
                                                          case "end":
                                                            return _context9.stop();
                                                        }
                                                      }
                                                    }, _callee9, this);
                                                  }));
                                                });

                                              case 6:
                                                _context10.next = 10;
                                                break;

                                              case 8:
                                                this.displayClienteNoExiste = true;
                                                this.stateButtonsFlow();

                                              case 10:
                                              case "end":
                                                return _context10.stop();
                                            }
                                          }
                                        }, _callee10, this);
                                      }));
                                    }, function (err) {
                                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                                        _this18.router.navigate(['login']);
                                      } else {
                                        console.log(err);
                                      }
                                    });
                                  } else {
                                    if (this.solicitudService.parametrosRuteo.openForm) {
                                      this.BuscarCliente(docId, docIdExt, search, updatePersona);
                                    } else {
                                      this.solicitudService.isLoadingAgain = false;
                                      this.solicitudService.displayModalFormTitular = false;
                                      this.solicitudService.displayBusquedaCI = true;
                                    }
                                  }

                                case 4:
                                case "end":
                                  return _context11.stop();
                              }
                            }
                          }, _callee11, this);
                        }));
                      });

                    case 1:
                    case "end":
                      return _context12.stop();
                  }
                }
              }, _callee12, this);
            }));
          });
        }
      }, {
        key: "initInstanciaPoliza",
        value: function initInstanciaPoliza() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee15() {
            var _this20 = this;

            return _regeneratorRuntime().wrap(function _callee15$(_context15) {
              while (1) {
                switch (_context15.prev = _context15.next) {
                  case 0:
                    this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
                    this.solicitudService.product = this.solicitudService.asegurado.instancia_poliza.poliza.descripcion;
                    this.solicitudService.id_poliza = this.solicitudService.asegurado.instancia_poliza.poliza.id; // await this.solicitudService.setDatesOfAsegurado();

                    this.solicitudService.setAtributosToPersonaBanco(this.userform, function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this20, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee14() {
                        var _this21 = this;

                        return _regeneratorRuntime().wrap(function _callee14$(_context14) {
                          while (1) {
                            switch (_context14.prev = _context14.next) {
                              case 0:
                                this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach(function (instancia_documento) {
                                  if (instancia_documento.id_documento == _this21.solicitudService.documentoSolicitud.id) {
                                    _this21.solicitudService.instanciaDocumentoSolicitud = instancia_documento;
                                  } else if (instancia_documento.id_documento == _this21.solicitudService.documentoComprobante.id) {
                                    _this21.solicitudService.instanciaDocumentoComprobante = instancia_documento;
                                  } else if (instancia_documento.id_documento == _this21.solicitudService.documentoCertificado.id) {
                                    _this21.solicitudService.instanciaDocumentoCertificado = instancia_documento;
                                  }
                                });
                                _context14.next = 3;
                                return this.solicitudService.setAtributosToPersonaBanco(this.userform, function () {
                                  return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this21, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee13() {
                                    return _regeneratorRuntime().wrap(function _callee13$(_context13) {
                                      while (1) {
                                        switch (_context13.prev = _context13.next) {
                                          case 0:
                                            this.solicitudService.persona_banco.extension = this.solicitudService.asegurado.entidad.persona.persona_doc_id_ext + '';
                                            _context13.next = 3;
                                            return this.stateButtonsFlow();

                                          case 3:
                                          case "end":
                                            return _context13.stop();
                                        }
                                      }
                                    }, _callee13, this);
                                  }));
                                });

                              case 3:
                                this.setPago(this.solicitudService.atributoDebitoAutomatico.valor); //this.setMonto(this.solicitudService.persona_banco_datos.modalidad_pago);

                                this.setNroCuotas(this.solicitudService.persona_banco_datos.nro_cuotas);
                                this.solicitudService.setBeneficiarios();
                                this.solicitudService.displayModalDatosTitular = true;
                                this.displayDatosTitular = true;
                                this.solicitudService.isLoadingAgain = false;

                                if (this.componenteBeneficiario != undefined) {
                                  this.componenteBeneficiario.ngOnInit();
                                }

                                if (this.transicionesComponent != undefined) {
                                  this.transicionesComponent.ngOnInit();
                                }

                                if (!(this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id)) {
                                  _context14.next = 14;
                                  break;
                                }

                                _context14.next = 14;
                                return this.getDatosFromAccountService();

                              case 14:
                                this.stateButtonsFlow();

                                if (!(typeof callback == 'function')) {
                                  _context14.next = 18;
                                  break;
                                }

                                _context14.next = 18;
                                return callback();

                              case 18:
                              case "end":
                                return _context14.stop();
                            }
                          }
                        }, _callee14, this);
                      }));
                    });

                  case 4:
                  case "end":
                    return _context15.stop();
                }
              }
            }, _callee15, this);
          }));
        }
      }, {
        key: "setUserForm",
        value: function setUserForm() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee17() {
            var _this22 = this;

            return _regeneratorRuntime().wrap(function _callee17$(_context17) {
              while (1) {
                switch (_context17.prev = _context17.next) {
                  case 0:
                    this.solicitudService.isLoadingAgain = true;

                    if (this.solicitudService.id_poliza == 10) {
                      this.idsPerfiles = [27, 33];
                    } else if (this.solicitudService.id_poliza == 12) {
                      this.idsPerfiles = [34, 35];
                    }

                    this.solicitudService.setCurrentPerfil(this.idsPerfiles);
                    this.adminPermisosService.getComponentes(this.solicitudService.parametrosRuteo.id_vista, this.solicitudService.currentPerfil.id).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this22, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee16() {
                        var _this23 = this;

                        var response;
                        return _regeneratorRuntime().wrap(function _callee16$(_context16) {
                          while (1) {
                            switch (_context16.prev = _context16.next) {
                              case 0:
                                response = res;
                                this.solicitudService.isLoadingAgain = false;
                                this.solicitudService.componentesInvisibles = response.data;
                                _context16.next = 5;
                                return this.solicitudService.componentsBehavior(function (res) {
                                  _this23.minlengthCelular = 8;
                                  _this23.minLengthMsg = "Debe contener " + _this23.minlength + " digitos";
                                  _this23.minLengthMsgCelular = "Debe contener " + _this23.minlengthCelular + " digitos";

                                  _this23.breadcrumbService.setItems([{
                                    label: 'Productos'
                                  }, {
                                    label: 'Eco Accidentes',
                                    routerLink: ['/file']
                                  }]);

                                  _this23.formBusqueda = _this23.fb.group({
                                    'persona_doc_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
                                    'persona_doc_id_ext': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
                                  });
                                  _this23.envioDocumentosForm = _this23.fb.group({
                                    'destinatarios_correos': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
                                    'envio_documentos': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
                                  });
                                  _this23.fechaEmisionForm = _this23.fb.group({
                                    'fecha_registro': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                                    'nro_transaccion': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
                                  });
                                  _this23.userform = _this23.fb.group({
                                    'persona_doc_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
                                    'persona_doc_id_ext': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                                    //'persona_doc_id_comp': new FormControl(''),
                                    'persona_primer_apellido': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showPrimerApellido ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                                    'persona_segundo_apellido': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showSegundoApellido ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                                    'persona_primer_nombre': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showPrimerNombre ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                                    'persona_segundo_nombre': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showSegundoNombre ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                                    'persona_apellido_casada': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")),
                                    'persona_direccion_domicilio': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
                                    'persona_direcion_trabajo': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
                                    'persona_fecha_nacimiento': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
                                    // 'persona_celular': new FormControl(''),
                                    'persona_telefono_domicilio': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](_this23.solicitudService.showTelefonoDomicilio ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(8), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[0-9]*$")] : []),
                                    'persona_telefono_trabajo': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](_this23.solicitudService.showTelefonoTrabajo ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(8), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[0-9]*$")] : []),
                                    'persona_telefono_celular': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showTelefonoCelular ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(8), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[0-9]*$")] : []),
                                    //'persona_profesion': new FormControl(''),
                                    'par_tipo_documento_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showTipoDocumento ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required] : []),
                                    // 'par_nacionalidad_id': new FormControl(4),
                                    'par_pais_nacimiento_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', []),
                                    'par_ciudad_nacimiento_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showCiudadNacimiento ? [] : []),
                                    'par_provincia_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showProvincia ? [] : []),
                                    'par_departamento_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showDepartamento ? [] : []),
                                    'par_lugar_nacimiento_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showLugarNacimiento ? [] : []),
                                    'par_sexo_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
                                    //'persona_origen': new FormControl(''),
                                    'par_numero_cuenta': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showNroCuenta ? [] : []),
                                    'par_cuenta_expiracion': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showCuentaFechaExpiracion ? [] : []),
                                    'par_ocupacion': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showOcupacion ? [] : []),
                                    // 'par_plan': new FormControl('', this.solicitudService.showPlan ? [Validators.required] : []),
                                    // 'par_plazo': new FormControl('', this.solicitudService.showPlazo ? [Validators.required, Validators.min(1)] : []),
                                    'par_codigo_agenda_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
                                    //'par_estado_civil_id': new FormControl(''),
                                    'par_mail_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showEmail ? [] : []),
                                    //'par_caedec_id': new FormControl(''),
                                    'par_localidad_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showLocalidad ? [] : []),
                                    'par_sucursal': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showSucursal ? [] : []),
                                    'par_agencia': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showAgencia ? [] : []),
                                    //'par_prima': new FormControl('', this.solicitudService.showPrima ? [this.solicitudService.atributoPrima.requerido = Validators.required] : []),
                                    //'par_nro_sci': new FormControl(''),
                                    'par_modalidad_pago': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
                                    'par_zona': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
                                    'par_nro_direccion': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
                                    'par_razon_social': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
                                    'par_nit_carnet': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
                                    'par_moneda': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showMoneda ? [] : []),
                                    'par_debito_automatico_id': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showPagoEfectivo ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required] : []),
                                    //'par_condicion_pep': new FormControl('', this.solicitudService.showCondicionPep ? [] : []),
                                    //'par_cargo_pep': new FormControl('', this.solicitudService.showCargoEntidadPep ? [] : []),
                                    //'par_periodo_pep': new FormControl('', this.solicitudService.showPeriodoCargoPep ? [] : []),
                                    'par_direccion_laboral': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showDireccionLaboral ? [] : []),
                                    //'par_tipo_cuenta': new FormControl('', this.solicitudService.showTipoCuenta ? [] : []),
                                    //'par_nro_cuotas': new FormControl('', this.solicitudService.showNroCuotas? [] : []),
                                    //'par_monto': new FormControl('', this.solicitudService.showMonto? [] : []),
                                    'par_forma_pago': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showFormaPago ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required] : []),
                                    'par_desc_ocupacion': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showDescOcupacion ? [] : []),
                                    'par_producto_asociado': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showProductoAsociado ? [] : []),
                                    'par_tipo_seguro': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showTipoSeguro ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required] : []),
                                    'par_nro_solicitud_sci': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showNroSolicitudSci ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required] : []),
                                    'par_operacion_plazo': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showSolicitudPlazoCredito ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required] : []),
                                    'par_operacion_tipo_credito': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showTipoCredito ? [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required] : []),
                                    'par_prima': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showPrima ? [] : []),
                                    'par_solicitud_prima_total': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _this23.solicitudService.showSolicitudPrimaTotal ? [] : [])
                                  });

                                  _this23.solicitudService.getFormValidationErrors(_this23.userform);

                                  _this23.solicitudService.showFormValidation(_this23.userform);

                                  _this23.solicitudService.setFeatureValidacionAlInicioPersonaBanco();
                                });

                              case 5:
                              case "end":
                                return _context16.stop();
                            }
                          }
                        }, _callee16, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this22.router.navigate(['login']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 4:
                  case "end":
                    return _context17.stop();
                }
              }
            }, _callee17, this);
          }));
        }
      }, {
        key: "featureApPaternoMaternoCasada",
        value: function featureApPaternoMaternoCasada() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee18() {
            return _regeneratorRuntime().wrap(function _callee18$(_context18) {
              while (1) {
                switch (_context18.prev = _context18.next) {
                  case 0:
                    if (!(this.solicitudService.persona_banco.paterno == '' && this.solicitudService.persona_banco.materno == '' && this.solicitudService.persona_banco.apcasada == '')) {
                      _context18.next = 3;
                      break;
                    }

                    this.userform.controls['persona_primer_apellido'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[a-zA-Z\xC0-\xFF\xF1\xD1]+(\\s*[a-zA-Z\xC0-\xFF\xF1\xD1]*)*[a-zA-Z\xC0-\xFF\xF1\xD1]+$")]);
                    return _context18.abrupt("return", false);

                  case 3:
                    return _context18.abrupt("return", true);

                  case 4:
                  case "end":
                    return _context18.stop();
                }
              }
            }, _callee18, this);
          }));
        }
      }, {
        key: "componentsBehaviorOnInit",
        value: function componentsBehaviorOnInit() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee19() {
            return _regeneratorRuntime().wrap(function _callee19$(_context19) {
              while (1) {
                switch (_context19.prev = _context19.next) {
                  case 0:
                    this.solicitudService.showTarjetaNombre = false;
                    this.solicitudService.showTarjetaValida = false;
                    this.solicitudService.showModalidadPago = false;
                    this.solicitudService.showPagoEfectivo = false;
                    this.solicitudService.showRazonSocial = false;
                    this.solicitudService.showTarjetaNro = false;
                    this.solicitudService.showNitCarnet = false;
                    this.solicitudService.showTarjetaUltimosCuatroDigitos = false;
                    this.solicitudService.showNroCuenta = false;
                    this.solicitudService.showCuentaFechaExpiracion = false;
                    this.solicitudService.showOcupacion = false;
                    this.solicitudService.showPlan = false;
                    this.solicitudService.showPlazo = false;
                    this.solicitudService.showAgencia = false;
                    this.solicitudService.showUsuarioCargo = false;
                    this.solicitudService.showPrima = true;
                    this.solicitudService.showSucursal = false;
                    this.solicitudService.showMoneda = false;
                    this.solicitudService.showCaedec = true;
                    this.solicitudService.showLocalidad = true;
                    this.solicitudService.showDepartamento = true;
                    this.solicitudService.showCodSucursal = true;
                    this.solicitudService.showTipoDocumento = true;
                    this.solicitudService.showEmail = true;
                    this.solicitudService.showEstadoCivil = true;
                    this.solicitudService.showManejo = true;
                    this.solicitudService.showCodAgenda = true;
                    this.solicitudService.showDescCaedec = true;
                    this.solicitudService.showZona = false;
                    this.solicitudService.showNroDireccion = false;

                    if (typeof callback == 'function') {
                      callback();
                    }

                  case 31:
                  case "end":
                    return _context19.stop();
                }
              }
            }, _callee19, this);
          }));
        }
      }, {
        key: "getPoliza",
        value: function getPoliza() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee22() {
            var _this24 = this;

            return _regeneratorRuntime().wrap(function _callee22$(_context22) {
              while (1) {
                switch (_context22.prev = _context22.next) {
                  case 0:
                    _context22.next = 2;
                    return this.solicitudService.setIdPoliza(this.solicitudService.parametrosRuteo.parametro_vista);

                  case 2:
                    _context22.next = 4;
                    return this.polizaService.getPolizaById(this.solicitudService.id_poliza).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this24, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee21() {
                        var _this25 = this;

                        var response;
                        return _regeneratorRuntime().wrap(function _callee21$(_context21) {
                          while (1) {
                            switch (_context21.prev = _context21.next) {
                              case 0:
                                response = res;
                                this.solicitudService.poliza = response.data;
                                _context21.next = 4;
                                return this.solicitudService.getAllParametrosByIdDiccionario([32, 33], this.solicitudService.poliza.id, [1, 2, 3, 4, 11, 17, 18, 20, 21, 22, 25, 29, 30, 31, 32, 36, 38, 40, 52, 34, 58, 41, 56, 59, 66, 67], function () {
                                  return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this25, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee20() {
                                    var _this26 = this;

                                    return _regeneratorRuntime().wrap(function _callee20$(_context20) {
                                      while (1) {
                                        switch (_context20.prev = _context20.next) {
                                          case 0:
                                            if (!(this.solicitudService.poliza.anexo_polizas && this.solicitudService.poliza.anexo_polizas.length)) {
                                              _context20.next = 18;
                                              break;
                                            }

                                            this.solicitudService.poliza.anexo_poliza = this.solicitudService.poliza.anexo_polizas.find(function (param) {
                                              return param.id_tipo == 288;
                                            });
                                            this.solicitudService.anexoPoliza = this.solicitudService.poliza.anexo_polizas.find(function (param) {
                                              return param.id_tipo == 288;
                                            });
                                            this.solicitudService.anexoAsegurado = this.solicitudService.anexoPoliza.anexo_asegurados.find(function (param) {
                                              return param.id_tipo == _this26.solicitudService.parametroAsegurado.id;
                                            });

                                            if (!this.solicitudService.anexoAsegurado) {
                                              _context20.next = 18;
                                              break;
                                            }

                                            this.solicitudService.isRenovated = this.solicitudService.asegurado.instancia_poliza.id == this.solicitudService.asegurado.instancia_poliza.id_instancia_renovada ? false : true;
                                            _context20.t0 = this.solicitudService.anexoAsegurado.id_edad_unidad;
                                            _context20.next = _context20.t0 === this.solicitudService.parametroYear.id ? 9 : _context20.t0 === this.solicitudService.parametroMonth.id ? 12 : _context20.t0 === this.solicitudService.parametroDay.id ? 15 : 18;
                                            break;

                                          case 9:
                                            this.solicitudService.edadMinimaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                            this.solicitudService.edadMaximaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                            return _context20.abrupt("break", 18);

                                          case 12:
                                            this.solicitudService.edadMinimaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                            this.solicitudService.edadMaximaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                            return _context20.abrupt("break", 18);

                                          case 15:
                                            this.solicitudService.edadMinimaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                            this.solicitudService.edadMaximaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                            return _context20.abrupt("break", 18);

                                          case 18:
                                            if (!(typeof callback == 'function')) {
                                              _context20.next = 21;
                                              break;
                                            }

                                            _context20.next = 21;
                                            return callback();

                                          case 21:
                                          case "end":
                                            return _context20.stop();
                                        }
                                      }
                                    }, _callee20, this);
                                  }));
                                });

                              case 4:
                              case "end":
                                return _context21.stop();
                            }
                          }
                        }, _callee21, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this24.router.navigate(['']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 4:
                  case "end":
                    return _context22.stop();
                }
              }
            }, _callee22, this);
          }));
        }
      }, {
        key: "cambiarSolicitudEstado",
        value: function cambiarSolicitudEstado() {
          var _this27 = this;

          this.solicitudService.displayValidacionSinObservaciones = false;

          if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
            this.solicitudService.emitirInstanciaPoliza = false;
          } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
            this.solicitudService.instanciaDocumentoComprobante = new _src_core_modelos_instancia_documento__WEBPACK_IMPORTED_MODULE_32__["Instancia_documento"]();
            this.solicitudService.instanciaDocumentoComprobante.documento = this.solicitudService.documentoComprobante;

            if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
              this.solicitudService.instanciaDocumentoComprobante.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
            }

            if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(function (param) {
              return param.id_documento == _this27.solicitudService.instanciaDocumentoComprobante.id_documento;
            })) {
              this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoComprobante);
            }

            this.solicitudService.emitirInstanciaPoliza = false;
          } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
            if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoDebitAutomatico.id + '') {
              this.solicitudService.instanciaDocumentoCertificado = new _src_core_modelos_instancia_documento__WEBPACK_IMPORTED_MODULE_32__["Instancia_documento"]();

              if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
              }

              this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
              this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
              this.solicitudService.emitirInstanciaPoliza = true;
            } else if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0, 1) == '9') {
              this.solicitudService.instanciaDocumentoCertificado = new _src_core_modelos_instancia_documento__WEBPACK_IMPORTED_MODULE_32__["Instancia_documento"]();

              if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
              }

              this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
              this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
              this.solicitudService.emitirInstanciaPoliza = true;
            } else {
              this.solicitudService.emitirInstanciaPoliza = false;
            }

            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
            var transicionSolicitado = new _src_core_modelos_instancia_poliza_transicion__WEBPACK_IMPORTED_MODULE_33__["Instancia_poliza_transicion"]();
            transicionSolicitado.observacion = this.solicitudService.msgText;
            transicionSolicitado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            transicionSolicitado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            transicionSolicitado.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionSolicitado);
          } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
            this.solicitudService.instanciaDocumentoCertificado = new _src_core_modelos_instancia_documento__WEBPACK_IMPORTED_MODULE_32__["Instancia_documento"](); // if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
            //     this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
            // }
            // this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
            // this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;

            this.solicitudService.emitirInstanciaPoliza = true;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];

            var _transicionSolicitado = new _src_core_modelos_instancia_poliza_transicion__WEBPACK_IMPORTED_MODULE_33__["Instancia_poliza_transicion"]();

            _transicionSolicitado.observacion = this.solicitudService.msgText;
            _transicionSolicitado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            _transicionSolicitado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            _transicionSolicitado.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(_transicionSolicitado);
          } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
            this.solicitudService.instanciaDocumentoCertificado = new _src_core_modelos_instancia_documento__WEBPACK_IMPORTED_MODULE_32__["Instancia_documento"](); // if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
            //     this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
            // }
            // this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
            // this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;

            this.solicitudService.emitirInstanciaPoliza = true;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];

            var _transicionSolicitado2 = new _src_core_modelos_instancia_poliza_transicion__WEBPACK_IMPORTED_MODULE_33__["Instancia_poliza_transicion"]();

            _transicionSolicitado2.observacion = this.solicitudService.msgText;
            _transicionSolicitado2.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            _transicionSolicitado2.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            _transicionSolicitado2.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(_transicionSolicitado2);
          } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
            this.solicitudService.emitirInstanciaPoliza = true;
          }

          if (!this.solicitudService.userFormHasErrors && !this.solicitudService.emitirInstanciaPoliza) {
            this.updateInstanciaPoliza();
          } else if (!this.solicitudService.userFormHasErrors && this.solicitudService.emitirInstanciaPoliza) {
            if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id + '') {
              this.solicitudService.displayEmitirSolicitud = false;
              this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
            } else {
              if (this.solicitudService.atributoSolicitudSci && this.solicitudService.TiposCreditoConsumo.find(function (param) {
                return _this27.solicitudService.atributoTipoCredito.valor.includes(param.value);
              }) || this.solicitudService.TiposCreditoDisponible.find(function (param) {
                return _this27.solicitudService.atributoTipoCredito.valor.includes(param.value);
              }) || this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0, 1) == '9') {
                this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
              } else {
                this.solicitudService.displayEmitirSolicitud = true;
              }
            }
          }
        }
      }, {
        key: "updateInstanciaPoliza",
        value: function updateInstanciaPoliza() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee24() {
            var _this28 = this;

            return _regeneratorRuntime().wrap(function _callee24$(_context24) {
              while (1) {
                switch (_context24.prev = _context24.next) {
                  case 0:
                    if (parseInt(this.solicitudService.atributoDebitoAutomatico.valor) == this.solicitudService.condicionNo.id && this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id && !this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(function (param) {
                      return param.id_documento == _this28.solicitudService.instanciaDocumentoCertificado.id_documento;
                    })) {
                      this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoCertificado);
                    }

                    this.solicitudService.isLoadingAgain = true;

                    if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoEfectivo.id + '') {
                      // antiguamente solo EcoResguardo 12
                      if ([10, 12].includes(parseInt(this.solicitudService.asegurado.instancia_poliza.id_poliza + ''))) {
                        if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0, 1) == '9') {
                          this.solicitudService.estadosUpdateSolicitud = [this.solicitudService.estadoIniciado.id, this.solicitudService.estadoSolicitado.id, this.solicitudService.estadoPorEmitir.id, this.solicitudService.estadoEmitido.id];
                        } else {
                          this.solicitudService.estadosUpdateSolicitud = [this.solicitudService.estadoIniciado.id, this.solicitudService.estadoSolicitado.id, this.solicitudService.estadoPorEmitir.id, this.solicitudService.estadoPorPagar.id, this.solicitudService.estadoEmitido.id];
                        }
                      } // else {
                      //     if (this.solicitudService.TiposCreditoConsumo.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                      //         this.solicitudService.TiposCreditoDisponible.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                      //         this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9'
                      //     ) {
                      //         this.solicitudService.estadosUpdateSolicitud = [
                      //             this.solicitudService.estadoIniciado.id,
                      //             this.solicitudService.estadoSolicitado.id,
                      //             this.solicitudService.estadoPorEmitir.id,
                      //             this.solicitudService.estadoEmitido.id
                      //         ]
                      //     } else {
                      //         this.solicitudService.estadosUpdateSolicitud = [
                      //             this.solicitudService.estadoIniciado.id,
                      //             this.solicitudService.estadoSolicitado.id,
                      //             this.solicitudService.estadoPorEmitir.id,
                      //             this.solicitudService.estadoPorPagar.id,
                      //             this.solicitudService.estadoEmitido.id
                      //         ]
                      //     }
                      // }

                    } else {
                      this.solicitudService.estadosUpdateSolicitud = [this.solicitudService.estadoIniciado.id, this.solicitudService.estadoSolicitado.id, this.solicitudService.estadoPorEmitir.id, this.solicitudService.estadoEmitido.id];
                    }

                    this.instanciaPolizaService.updateInstanciaToNextStatus(this.solicitudService.estadosUpdateSolicitud, this.solicitudService.asegurado).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this28, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee23() {
                        var _this29 = this;

                        var response;
                        return _regeneratorRuntime().wrap(function _callee23$(_context23) {
                          while (1) {
                            switch (_context23.prev = _context23.next) {
                              case 0:
                                response = res;
                                this.solicitudService.asegurado = response.data;
                                this.stateButtonsFlow();
                                this.solicitudService.setDatesOfAsegurado();
                                this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach(function (instancia_documento) {
                                  if (instancia_documento.id_documento == _this29.solicitudService.documentoSolicitud.id) {
                                    _this29.solicitudService.instanciaDocumentoSolicitud = instancia_documento;
                                  } else if (instancia_documento.id_documento == _this29.solicitudService.documentoComprobante.id) {
                                    _this29.solicitudService.instanciaDocumentoComprobante = instancia_documento;
                                  } else if (instancia_documento.id_documento == _this29.solicitudService.documentoCertificado.id) {
                                    _this29.solicitudService.instanciaDocumentoCertificado = instancia_documento;
                                  }
                                });
                                this.solicitudService.setBeneficiarios();
                                this.transicionesComponent.ngOnInit();
                                this.componenteBeneficiario.ngOnInit();

                                if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                                  if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoEfectivo.id + '') {
                                    this.solicitudService.msgCambioExitoso = "La solicitud fue actualizada exitosamente a la siguiente etapa ".concat(this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado], " <br><br> En esta etapa debe imprimir la Solicitud del Seguro y hacerla firmar por el cliente, este documento debe digitalizarlo y subirlo al sistema, en la secci\xF3n \"documentos Adjuntos\"");
                                  } else {
                                    this.solicitudService.msgCambioExitoso = "La solicitud fue actualizada exitosamente a la siguiente etapa ".concat(this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado], " <br><br> En esta etapa debe imprimir la Solicitud del Seguro y hacerla firmar por el cliente, este documento debe digitalizarlo y subirlo al sistema, en la secci\xF3n \"documentos Adjuntos\"");
                                  }
                                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                                  this.solicitudService.msgCambioExitoso = 'El cliente debe aproximarse a caja para hacer el pago de la prima y habilitar la emision del certificado de cobertura.';
                                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                                  this.solicitudService.msgCambioExitoso = 'El cliente puede aproximarse a plataforma para habilitar la emision del certificado de cobertura.';
                                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
                                  this.solicitudService.msgCambioExitoso = "La solicitud fue actualizada exit\xF3samente al siguiente estado ".concat(this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado], ". \xBFDesea imprimir el certificado de cobertura Nro: ").concat(this.solicitudService.instanciaDocumentoCertificado.nro_documento + '', ".");
                                } else {
                                  this.solicitudService.msgCambioExitoso = 'La solicitud fue actualizada exitósamente al siguiente estado. ' + this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado];
                                } //await this.setPlanPago(() => {


                                this.solicitudService.displayEmitirSolicitud = false;
                                this.solicitudService.displayEmitirSolicitudDesdeCaja = false;
                                this.solicitudService.displayCambioEstadoExitoso = true; //});

                                this.solicitudService.isLoadingAgain = false;
                                this.validandoContinuando = false;

                              case 14:
                              case "end":
                                return _context23.stop();
                            }
                          }
                        }, _callee23, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this28.router.navigate(['login']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 4:
                  case "end":
                    return _context24.stop();
                }
              }
            }, _callee24, this);
          }));
        }
      }, {
        key: "setPlanPago",
        value: function setPlanPago() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee25() {
            var _this30 = this;

            var documentoCertificado, atributoCuota, atributoMonto, atributoModalidad, atributoMoneda, planPago;
            return _regeneratorRuntime().wrap(function _callee25$(_context25) {
              while (1) {
                switch (_context25.prev = _context25.next) {
                  case 0:
                    if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
                      this.solicitudService.isLoadingAgain = true;
                      documentoCertificado = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(function (param) {
                        return param.id_documento == _this30.solicitudService.documentoCertificado.id;
                      });
                      atributoCuota = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(function (param) {
                        return param.objeto_x_atributo.id_atributo == _this30.solicitudService.atributoNroCuotas.objeto_x_atributo.id_atributo;
                      });
                      atributoMonto = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(function (param) {
                        return param.objeto_x_atributo.id_atributo == _this30.solicitudService.atributoMonto.objeto_x_atributo.id_atributo;
                      });
                      atributoModalidad = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(function (param) {
                        return param.objeto_x_atributo.id_atributo == _this30.solicitudService.atributoModalidadPago.objeto_x_atributo.id_atributo;
                      });
                      atributoMoneda = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(function (param) {
                        return param.objeto_x_atributo.id_atributo == _this30.solicitudService.atributoMoneda.objeto_x_atributo.id_atributo;
                      });
                      planPago = new _src_core_modelos_plan_pago__WEBPACK_IMPORTED_MODULE_40__["Plan_pago"]();

                      if (documentoCertificado && documentoCertificado.fecha_emision) {
                        planPago.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
                        planPago.total_prima = atributoCuota && atributoMonto ? parseInt(atributoCuota.valor) * parseInt(atributoMonto.valor) : 0;
                        planPago.interes = 0;
                        planPago.id_moneda = parseInt(atributoMoneda.valor);
                        planPago.plazo_anos = 1;
                        planPago.periodicidad_anual = atributoModalidad && atributoModalidad.valor == this.solicitudService.parMensual.id + '' ? 12 : atributoModalidad.valor == this.solicitudService.parAnual.id + '' ? 1 : 0;
                        planPago.prepagable_postpagable = 1;
                        planPago.fecha_inicio = documentoCertificado.fecha_emision;
                        planPago.adicionado_por = this.solicitudService.asegurado.instancia_poliza.adicionada_por;
                        planPago.modificado_por = this.solicitudService.asegurado.instancia_poliza.modificada_por;
                        this.planPagoService.GenerarPlanPagos(planPago).subscribe(function (res) {
                          var response = res;

                          if (response.status == 'ERROR') {
                            _this30.displayErrorPlanPago = true;
                          }

                          if (typeof callback == 'function') {
                            callback();
                          }

                          _this30.solicitudService.isLoadingAgain = false;
                        });
                      } else {
                        this.displayErrorPlanPago = true;
                      }
                    }

                  case 1:
                  case "end":
                    return _context25.stop();
                }
              }
            }, _callee25, this);
          }));
        }
      }, {
        key: "onUpload",
        value: function onUpload(event) {
          var _iterator2 = _createForOfIteratorHelper(event.files),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var file = _step2.value;

              if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                this.solicitudService.fileDocumentoSolicitud = file;
              }

              this.solicitudService.uploadedFiles = [this.solicitudService.fileDocumentoSolicitud];
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }

          this.msgs = [];
          this.msgs.push({
            severity: 'info',
            summary: 'File Uploaded',
            detail: ''
          });
        }
      }, {
        key: "validarYContinuar",
        value: function validarYContinuar() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee27() {
            var _this31 = this;

            return _regeneratorRuntime().wrap(function _callee27$(_context27) {
              while (1) {
                switch (_context27.prev = _context27.next) {
                  case 0:
                    this.solicitudService.msgs_warn = [];
                    this.solicitudService.msgs_error = [];
                    this.solicitudService.msgs_info_warn = [];
                    this.solicitudService.userFormHasErrors = false;
                    this.solicitudService.userFormHasWarnings = false;
                    this.validandoContinuando = true;
                    _context27.next = 8;
                    return this.solicitudService.setAtributosToAsegurado(function () {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this31, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee26() {
                        return _regeneratorRuntime().wrap(function _callee26$(_context26) {
                          while (1) {
                            switch (_context26.prev = _context26.next) {
                              case 0:
                                _context26.next = 2;
                                return this.solicitudService.setBeneficiarios();

                              case 2:
                                _context26.next = 4;
                                return this.stateButtonsFlow();

                              case 4:
                                if (this.transicionesComponent != undefined) {
                                  this.transicionesComponent.ngOnInit();
                                }

                                if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                                  this.solicitudService.mostrarObservaciones = false;
                                  this.solicitudService.mostrarAdvertencias = true;

                                  if (this.solicitudService.msgs_warn.length || this.solicitudService.msgs_info_warn.length) {
                                    this.solicitudService.msgText = "No tiene observaciones, puede continuar con la instrumentaci\xF3n del seguro. <br><br> Tenga en cuenta que la solicitud de cr\xE9dito seleccionada para la instrumentacion del seguro es ".concat(this.solicitudService.atributoSolicitudSci.valor, ", cuyo plazo es: ").concat(this.solicitudService.atributoSolicitudPlazoCredito.valor, " meses");
                                  } else {
                                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.';
                                  }
                                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                                  this.solicitudService.mostrarObservaciones = false;

                                  if (this.solicitudService.msgs_warn.length) {
                                    this.solicitudService.msgText = "No tiene observaciones, puede continuar con la instrumentaci\xF3n del seguro.";
                                  } else {
                                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.';
                                  }
                                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                                  this.solicitudService.mostrarObservaciones = false;

                                  if (this.solicitudService.msgs_warn.length) {
                                    this.solicitudService.msgText = "No tiene observaciones, puede continuar con la instrumentaci\xF3n del seguro.";
                                  } else {
                                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.';
                                  }
                                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                                  this.solicitudService.mostrarObservaciones = false;

                                  if (this.solicitudService.msgs_warn.length) {
                                    this.solicitudService.msgText = "No tiene observaciones, puede continuar con la instrumentaci\xF3n del seguro. <br><br> Tenga en cuenta que a partir de la siguiente etapa ya no podr\xE1 modificar los datos que fueron introducidos.";
                                  } else {
                                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.';
                                  }
                                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                                  this.solicitudService.mostrarObservaciones = false;

                                  if (this.solicitudService.msgs_warn.length) {
                                    this.solicitudService.msgText = "No tiene observaciones, puede continuar con la instrumentaci\xF3n del seguro. <br><br> Tenga en cuenta que a partir de la siguiente etapa ya no podr\xE1 modificar los datos que fueron introducidos.";
                                  } else {
                                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.';
                                  }
                                } else {
                                  this.solicitudService.mostrarObservaciones = false;
                                  this.solicitudService.msgText = 'No tiene observaciones ni advertencias,  por lo que se la pasará a la siguiente etapa.';
                                }

                                if (this.solicitudService.userFormHasErrors) {
                                  this.withErrorsOrWarnings = true;
                                } else if (this.solicitudService.userFormHasWarnings) {
                                  this.withErrorsOrWarnings = false;
                                }

                                if (this.solicitudService.userFormHasErrors) {
                                  this.solicitudService.displayValidacionConObservaciones = true;
                                  this.solicitudService.displayValidacionSinObservaciones = false;
                                } else if (this.solicitudService.userFormHasWarnings) {
                                  this.solicitudService.displayValidacionConObservaciones = false;
                                  this.solicitudService.displayValidacionSinObservaciones = true;
                                } else if (this.solicitudService.userFormHasInfoWarnings) {
                                  this.solicitudService.displayValidacionConObservaciones = false;
                                  this.solicitudService.displayValidacionInfoWarnings = true;
                                } else {
                                  this.solicitudService.displayValidacionConObservaciones = false;
                                  this.solicitudService.displayValidacionSinObservaciones = true;
                                }

                              case 8:
                              case "end":
                                return _context26.stop();
                            }
                          }
                        }, _callee26, this);
                      }));
                    });

                  case 8:
                  case "end":
                    return _context27.stop();
                }
              }
            }, _callee27, this);
          }));
        }
      }, {
        key: "validateForm",
        value: function validateForm() {
          console.log(this.solicitudService.getFormValidationErrors(this.userform));
          console.log('atributoCondicionPep', this.solicitudService.atributoCondicionPep.requerido);
        }
      }, {
        key: "disableForm",
        value: function disableForm(form) {
          for (var control in form.controls) {
            if (form.controls[control]) {
              form.controls[control].disable();
            }
          }

          setTimeout(function () {
            $('.ui-message').css('display', 'none');
          }, 200);
        }
      }, {
        key: "enableUserform",
        value: function enableUserform() {
          for (var control in this.userform.controls) {
            if (control == 'par_tipo_documento_id' || control == 'persona_direccion_domicilio' || control == 'persona_doc_id_comp' || control == 'par_pais_nacimiento_id' || control == 'persona_telefono_trabajo' || control == 'persona_telefono_domicilio' || control == 'par_debito_automatico_id' || control == 'submit' || control == 'par_moneda' || control == 'par_numero_cuenta') {
              if (this.userform.controls[control]) {
                this.userform.controls[control].enable();
              }
            }
          }
        } // setSolicitudAsIniciado() {
        //     if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
        //         this.solicitudService.btnEmitirCertificadoEnabled = false;
        //         this.btnEnviarDocumentosEnabled = false;
        //         this.solicitudService.btnImprimirSolicitudEnabled = false;
        //         if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
        //             this.solicitudService.showOrdenPago = false;
        //         } else {
        //             this.solicitudService.showOrdenPago = false;
        //         }
        //         this.solicitudService.btnOrdenPago(false);
        //         if (this.solicitudService.hasRolConsultaTarjetas || this.solicitudService.hasRolConsultaCajero) {
        //             this.btnRefrescarInformacion = false;
        //         } else {
        //             this.btnRefrescarInformacion = true;
        //         }
        //     }
        // }

      }, {
        key: "stateButtonsFlow",
        value: function stateButtonsFlow() {
          var _this32 = this;

          this.solicitudService.showFormValidation(this.userform);
          this.solicitudService.validarBeneficiarios(this.componenteBeneficiario);
          this.solicitudService.validarArchivos();
          this.solicitudService.validarWarnsOrErrors(); // this.solicitudService.setFeatureValidacionAlInicioPersonaBanco();

          this.solicitudService.btnValidarYContinuarEnabled = true;
          this.solicitudService.btnRefrescarInformacion = true;
          this.btnTarjetaDebitoCuentasEnabled = true;
          this.solicitudService.showOrdenPago = false;
          this.solicitudService.btnOrdenPago(false);

          if (this.solicitudService.asegurado.instancia_poliza != null && this.solicitudService.atributoDebitoAutomatico != undefined && this.solicitudService.condicionSi != undefined && this.solicitudService.condicionNo != undefined) {
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
              this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
              this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
              this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? true : false;

              if (this.componenteBeneficiario) {
                this.componenteBeneficiario.enableAdicionarBeneficiario = false;
              }

              this.solicitudService.showBtnImprimirCarta = false;
              this.solicitudService.btnCartaEnabled = false;
              this.solicitudService.btnEmitirCertificadoEnabled = false;
              this.solicitudService.btnRefrescarInformacionEnabled = true;
              this.solicitudService.btnRefrescarInformacion = true;
              this.solicitudService.btnImprimirSolicitudEnabled = false;
              this.btnEnviarDocumentosEnabled = false;

              if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id + '') {
                this.solicitudService.showOrdenPago = false;
              } else {
                this.solicitudService.showOrdenPago = false;
              }

              this.solicitudService.btnOrdenPago(false); // if (this.solicitudService.hasRolConsultaTarjetas || this.solicitudService.hasRolConsultaCajero) {
              //     this.btnRefrescarInformacion = false;
              // } else {
              // }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
              if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor) {
                if (this.solicitudService.TiposCreditoConsumo.find(function (param) {
                  return _this32.solicitudService.atributoTipoCredito.valor.includes(param.value);
                }) || this.solicitudService.TiposCreditoDisponible.find(function (param) {
                  return _this32.solicitudService.atributoTipoCredito.valor.includes(param.value);
                }) || this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0, 1) == '9') {
                  this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                  this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
                  this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? true : false;
                } else {
                  this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                  this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
                  this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? true : false;
                }
              } else {
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? true : false;
              }

              this.solicitudService.showBtnImprimirCarta = false;
              this.solicitudService.btnCartaEnabled = false;

              if (this.componenteBeneficiario) {
                this.componenteBeneficiario.enableAdicionarBeneficiario = true;
              }

              this.solicitudService.btnEmitirCertificadoEnabled = false;
              this.solicitudService.btnRefrescarInformacionEnabled = false;
              this.btnEnviarDocumentosEnabled = true;
              this.solicitudService.btnRefrescarInformacion = false;
              this.solicitudService.btnImprimirSolicitudEnabled = true;

              if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id + '') {
                this.solicitudService.showOrdenPago = false;
              } else {
                this.solicitudService.showOrdenPago = false;
              }

              this.solicitudService.btnOrdenPago(false);
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
              if (this.solicitudService.hasRolPlataforma) {
                this.solicitudService.btnOrdenPago(true);

                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoEfectivo.id + '') {
                  this.solicitudService.showOrdenPago = true;
                } else {
                  this.solicitudService.showOrdenPago = false;
                }
              }

              this.solicitudService.btnOrdenPagoEnabled = false;

              if (this.solicitudService.TiposCreditoConsumo.find(function (param) {
                return _this32.solicitudService.atributoTipoCredito.valor.includes(param.value);
              }) || this.solicitudService.TiposCreditoDisponible.find(function (param) {
                return _this32.solicitudService.atributoTipoCredito.valor.includes(param.value);
              }) || this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0, 1) == '9') {
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? true : false;

                if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor.substring(0, 1) == '9') {
                  this.solicitudService.btnOrdenPago(false);
                  this.solicitudService.showOrdenPago = false;
                }

                this.solicitudService.showBtnImprimirCarta = this.solicitudService.hasRolPlataforma ? true : false;
                this.solicitudService.btnCartaEnabled = this.solicitudService.hasRolPlataforma ? true : false;
              } else {
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? true : false;
                this.solicitudService.showBtnImprimirCarta = false;
                this.solicitudService.btnCartaEnabled = false;
              }

              if (this.componenteBeneficiario) {
                this.componenteBeneficiario.enableAdicionarBeneficiario = true;
              } // this.solicitudService.btnCartaEnabled = true;


              this.solicitudService.btnEmitirCertificadoEnabled = false;
              this.solicitudService.btnImprimirSolicitudEnabled = false;
              this.solicitudService.btnRefrescarInformacionEnabled = false;
              this.btnEnviarDocumentosEnabled = true;
              this.solicitudService.btnRefrescarInformacion = false;
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
              this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? false : true;
              this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
              this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? true : false;

              if (this.componenteBeneficiario) {
                this.componenteBeneficiario.enableAdicionarBeneficiario = true;
              }

              this.solicitudService.btnEmitirCertificadoEnabled = false;
              this.solicitudService.btnRefrescarInformacionEnabled = false;
              this.solicitudService.btnRefrescarInformacion = false;
              this.btnEnviarDocumentosEnabled = true;
              this.solicitudService.btnImprimirSolicitudEnabled = false;

              if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id + '') {
                this.solicitudService.showOrdenPago = false;
              } else {
                this.solicitudService.showOrdenPago = false;
              }

              this.solicitudService.showOrdenPago = this.solicitudService.hasRolPlataforma ? true : false;
              this.solicitudService.btnOrdenPagoEnabled = this.solicitudService.hasRolPlataforma ? true : false;
              this.solicitudService.btnOrdenPago(true);
            }
            /*else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? true : false;
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                this.solicitudService.btnRefrescarInformacion = false;
                this.btnEnviarDocumentosEnabled = true;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                this.solicitudService.btnOrdenPago(false);
            }*/
            else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
              if (this.componenteBeneficiario) {
                this.componenteBeneficiario.enableAdicionarBeneficiario = true;
              }

              this.solicitudService.btnEmitirCertificadoEnabled = true;
              this.solicitudService.btnImprimirSolicitudEnabled = false;
              this.solicitudService.btnRefrescarInformacionEnabled = false;

              if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor) {
                if (this.solicitudService.TiposCreditoConsumo.find(function (param) {
                  return _this32.solicitudService.atributoTipoCredito.valor.includes(param.value);
                }) || this.solicitudService.TiposCreditoDisponible.find(function (param) {
                  return _this32.solicitudService.atributoTipoCredito.valor.includes(param.value);
                }) || this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0, 1) == '9') {
                  this.solicitudService.showBtnImprimirCarta = this.solicitudService.hasRolPlataforma || this.solicitudService.hasRolCredito ? true : false;
                  this.solicitudService.btnCartaEnabled = this.solicitudService.hasRolPlataforma || this.solicitudService.hasRolCredito ? true : false;
                } else {
                  this.solicitudService.showBtnImprimirCarta = false;
                  this.solicitudService.btnCartaEnabled = false;
                }
              } else {
                this.solicitudService.showBtnImprimirCarta = false;
                this.solicitudService.btnCartaEnabled = false;
              }

              this.btnEnviarDocumentosEnabled = true;
              this.solicitudService.btnOrdenPago(true);
              this.solicitudService.btnValidarYContinuarEnabled = false;
              this.solicitudService.btnRefrescarInformacion = false;
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoAnulado.id) {
              if (this.componenteBeneficiario) {
                this.componenteBeneficiario.enableAdicionarBeneficiario = true;
              }

              this.solicitudService.btnEmitirCertificadoEnabled = false;
              this.solicitudService.btnImprimirSolicitudEnabled = false;
              this.solicitudService.showBtnImprimirDesistimiento = false;
              this.solicitudService.btnCartaDesistimiento = false;
              this.solicitudService.btnRefrescarInformacionEnabled = false;
              this.btnEnviarDocumentosEnabled = false;
              this.solicitudService.btnOrdenPago(false);
              this.solicitudService.btnValidarYContinuarEnabled = false;
              this.solicitudService.btnRefrescarInformacion = false;
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoDesistido.id) {
              if (this.componenteBeneficiario) {
                this.componenteBeneficiario.enableAdicionarBeneficiario = true;
              }

              this.solicitudService.showBtnImprimirDesistimiento = true;
              this.solicitudService.btnCartaDesistimiento = true;
              this.solicitudService.btnEmitirCertificadoEnabled = false;
              this.solicitudService.btnImprimirSolicitudEnabled = false;
              this.solicitudService.btnRefrescarInformacionEnabled = false;
              this.btnEnviarDocumentosEnabled = false;
              this.solicitudService.btnOrdenPago(false);
              this.solicitudService.btnValidarYContinuarEnabled = false;
              this.solicitudService.btnRefrescarInformacion = false;
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSinVigencia.id) {
              if (this.componenteBeneficiario) {
                this.componenteBeneficiario.enableAdicionarBeneficiario = true;
              }

              this.solicitudService.btnEmitirCertificadoEnabled = false;
              this.solicitudService.showBtnImprimirDesistimiento = false;
              this.solicitudService.btnCartaDesistimiento = false;
              this.solicitudService.btnImprimirSolicitudEnabled = false;
              this.solicitudService.btnRefrescarInformacionEnabled = false;
              this.btnEnviarDocumentosEnabled = false;
              this.solicitudService.btnOrdenPago(false);
              this.solicitudService.btnValidarYContinuarEnabled = false;
              this.solicitudService.btnRefrescarInformacion = false;
            } else {
              this.solicitudService.btnOrdenPago(false);
              this.solicitudService.btnEmitirCertificadoEnabled = false;
              this.solicitudService.btnRefrescarInformacionEnabled = false;
              this.solicitudService.btnImprimirSolicitudEnabled = false;
            }
          }
        }
      }, {
        key: "refrescarInformacion",
        value: function refrescarInformacion() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee29() {
            var _this33 = this;

            return _regeneratorRuntime().wrap(function _callee29$(_context29) {
              while (1) {
                switch (_context29.prev = _context29.next) {
                  case 0:
                    this.solicitudService.isLoadingAgain = true;
                    this.solicitudService.editandoTitular = true;
                    this.getDatosFromCustomerService(this.solicitudService.persona_banco.doc_id, this.solicitudService.persona_banco.extension, function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this33, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee28() {
                        var _this34 = this;

                        var i, persona_banco_solicitud_sci;
                        return _regeneratorRuntime().wrap(function _callee28$(_context28) {
                          while (1) {
                            switch (_context28.prev = _context28.next) {
                              case 0:
                                if (!(this.solicitudService.persona_banco_solicitud && this.solicitudService.persona_banco_solicitud.solicitudes_sci.length)) {
                                  _context28.next = 13;
                                  break;
                                }

                                this.solicitudService.SolicitudesSci = [];
                                this.solicitudService.SolicitudesSci.push({
                                  label: "Seleccione una solicitud",
                                  value: null
                                });
                                i = 0;

                              case 4:
                                if (!(i < this.solicitudService.persona_banco_solicitud.solicitudes_sci.length)) {
                                  _context28.next = 11;
                                  break;
                                }

                                persona_banco_solicitud_sci = this.solicitudService.persona_banco_solicitud.solicitudes_sci[i];
                                _context28.next = 8;
                                return this.solicitudService.SolicitudesSci.push({
                                  label: persona_banco_solicitud_sci,
                                  value: persona_banco_solicitud_sci
                                });

                              case 8:
                                i++;
                                _context28.next = 4;
                                break;

                              case 11:
                                _context28.next = 14;
                                break;

                              case 13:
                                this.solicitudService.persona_banco_solicitud = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco_solicitud"]();

                              case 14:
                                _context28.next = 16;
                                return this.solicitudService.setAtributosToAsegurado(function (async) {
                                  _this34.stateButtonsFlow();

                                  _this34.solicitudService.persona_banco.debito_automatico = _this34.solicitudService.atributoDebitoAutomatico.valor;
                                  _this34.solicitudService.persona_banco_account.moneda = _this34.solicitudService.atributoMoneda.valor;
                                  _this34.solicitudService.persona_banco_datos.nro_tarjeta = _this34.solicitudService.atributoNroTarjeta.valor;
                                  _this34.solicitudService.persona_banco_account.nrocuenta = _this34.solicitudService.atributoNroCuenta.valor;
                                  _this34.solicitudService.persona_banco_datos.zona = _this34.solicitudService.atributoZona.valor;
                                  _this34.solicitudService.persona_banco_datos.nro_direccion = _this34.solicitudService.atributoNroDireccion.valor;
                                  _this34.solicitudService.persona_banco.e_mail = _this34.solicitudService.atributoEmail.valor;
                                  _this34.solicitudService.persona_banco_tarjeta_debito.tarjeta_ultimos_cuatro_digitos = _this34.solicitudService.atributoUltimosCuatroDigitos.valor;
                                  _this34.solicitudService.persona_banco_datos.nit_carnet = _this34.solicitudService.atributoNitCarnet.valor;
                                  _this34.solicitudService.persona_banco_datos.razon_social = _this34.solicitudService.atributoRazonSocial.valor;
                                  _this34.solicitudService.persona_banco_datos.ocupacion = _this34.solicitudService.atributoOcupacion.valor;
                                  _this34.solicitudService.persona_banco_datos.tipo_doc = _this34.solicitudService.atributoTipoDoc.valor;
                                  _this34.solicitudService.persona_banco_datos.amp_con_amb_med_general = _this34.solicitudService.atributoAmpConAmbMedGeneral.valor;
                                  _this34.solicitudService.persona_banco_datos.amp_con_amb_med_especializada = _this34.solicitudService.atributoAmpConAmbMedEspecializada.valor;
                                  _this34.solicitudService.persona_banco_datos.amp_sum_amb_medicamentos = _this34.solicitudService.atributoAmpSumAmbMedicamentos.valor;
                                  _this34.solicitudService.persona_banco_datos.amp_sum_exa_laboratorio = _this34.solicitudService.atributoAmpSumExaLaboratorio.valor;
                                  _this34.solicitudService.persona_banco_datos.plan = parseInt(_this34.solicitudService.atributoPlan.valor);
                                  _this34.solicitudService.persona_banco_transaccion.moneda = _this34.solicitudService.atributoTransaccionMoneda.valor;
                                  _this34.solicitudService.persona_banco_transaccion.detalle = _this34.solicitudService.atributoTransaccionDetalle.valor;
                                  _this34.solicitudService.persona_banco_transaccion.importe = _this34.solicitudService.atributoTransaccionImporte.valor;
                                  _this34.solicitudService.persona_banco_transaccion.fechatran = _this34.solicitudService.atributoFechaTransaccion.valor;
                                  _this34.solicitudService.persona_banco_solicitud.solicitud_sci_selected = _this34.solicitudService.atributoSolicitudSci.valor;
                                  _this34.solicitudService.persona_banco_operacion.Operacion_Tipo_Credito = _this34.solicitudService.atributoTipoCredito.valor;
                                  _this34.solicitudService.persona_banco_operacion.Operacion_Plazo = parseInt(_this34.solicitudService.atributoSolicitudPlazoCredito.valor + '');
                                  _this34.solicitudService.persona_banco_solicitud.solicitud_prima_total = _this34.solicitudService.atributoSolicitudPrimaTotal.valor;
                                  _this34.solicitudService.persona_banco_solicitud.solicitud_forma_pago = _this34.solicitudService.atributoFormaPago.valor;
                                  _this34.solicitudService.persona_banco_datos.plazo = parseInt(_this34.solicitudService.atributoPlazo.valor);

                                  if (_this34.solicitudService.usuario_banco) {
                                    _this34.solicitudService.usuario_banco.us_sucursal = parseInt(_this34.solicitudService.atributoSucursal.valor);
                                    _this34.solicitudService.usuario_banco.us_oficina = parseInt(_this34.solicitudService.atributoAgencia.valor);
                                    _this34.solicitudService.usuario_banco.us_cargo = _this34.solicitudService.atributoUsuarioCargo.valor;
                                  }

                                  _this34.solicitudService.persona_banco_datos.modalidad_pago = _this34.solicitudService.atributoModalidadPago.valor;
                                  _this34.solicitudService.persona_banco_datos.prima = parseInt(_this34.solicitudService.atributoPrima.valor);
                                  _this34.solicitudService.persona_banco_datos.telefono_celular = _this34.solicitudService.atributoTelefonoCelular.valor;
                                  _this34.solicitudService.persona_banco_datos.ciudad_nacimiento = _this34.solicitudService.atributoCiudadNacimiento.valor;
                                  _this34.solicitudService.persona_banco_datos.direccion_laboral = _this34.solicitudService.atributoDireccionLaboral.valor;
                                  _this34.solicitudService.persona_banco_pep.condicion = _this34.solicitudService.atributoCondicionPep.valor;
                                  _this34.solicitudService.persona_banco_pep.cargo_entidad = _this34.solicitudService.atributoCargoEntidadPep.valor;
                                  _this34.solicitudService.persona_banco_pep.periodo_cargo_publico = _this34.solicitudService.atributoPeriodoCargoPublico.valor;
                                  _this34.solicitudService.persona_banco_tarjeta_debito.fecha_expiracion = new Date(_this34.solicitudService.atributoCtaFechaExpiracion.valor);
                                  _this34.solicitudService.aseguradoBeneficiarios = _this34.solicitudService.asegurado.beneficiarios; // this.solicitudService.setBeneficiarios();
                                  // this.validarTarjetaYGuardarSolicitud(() => {

                                  _this34.solicitudService.asegurado.beneficiarios = _this34.solicitudService.aseguradoBeneficiarios;

                                  _this34.solicitudService.setBeneficiarios();

                                  _this34.displayActualizacionExitoso = true;
                                  _this34.solicitudService.isLoadingAgain = false; // });

                                  _this34.guardarSolicitudValidando();
                                });

                              case 16:
                              case "end":
                                return _context28.stop();
                            }
                          }
                        }, _callee28, this);
                      }));
                    });

                  case 3:
                  case "end":
                    return _context29.stop();
                }
              }
            }, _callee29, this);
          }));
        }
      }, {
        key: "getDatosFromSolicitudPrimeraEtapaService",
        value: function getDatosFromSolicitudPrimeraEtapaService(solicitud_sci_selected) {
          var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee31() {
            var _this35 = this;

            return _regeneratorRuntime().wrap(function _callee31$(_context31) {
              while (1) {
                switch (_context31.prev = _context31.next) {
                  case 0:
                    this.solicitudService.isLoadingAgain = true;

                    if (solicitud_sci_selected && solicitud_sci_selected.substring(0, 1) == '9') {
                      this.solicitudService.persona_banco.debito_automatico = this.solicitudService.pagoDebitAutomatico.id + '';
                      this.solicitudService.pagoACredito = true;
                      this.solicitudService.editPagoEfectivo = false;
                    } else {
                      this.solicitudService.pagoACredito = false;
                      this.solicitudService.persona_banco.debito_automatico = this.solicitudService.pagoEfectivo.id + '';
                    }

                    this.solicitudService.persona_banco_solicitud.solicitud_forma_pago = this.solicitudService.parametroPagoContado.id + '';

                    if (!solicitud_sci_selected) {
                      _context31.next = 8;
                      break;
                    }

                    _context31.next = 6;
                    return this.soapuiService.getSolicitudPrimeraEtapa(solicitud_sci_selected).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this35, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee30() {
                        var response, validacion, _validacion;

                        return _regeneratorRuntime().wrap(function _callee30$(_context30) {
                          while (1) {
                            switch (_context30.prev = _context30.next) {
                              case 0:
                                this.solicitudService.isLoadingAgain = false;
                                response = res;

                                if (!(Object(util__WEBPACK_IMPORTED_MODULE_6__["isObject"])(response.data) && Object.keys(response.data).length)) {
                                  _context30.next = 15;
                                  break;
                                }

                                this.solicitudService.persona_banco_operacion = response.data;
                                this.solicitudService.persona_banco_operaciones.push(this.solicitudService.persona_banco_operacion);
                                this.solicitudService.persona_banco_solicitud.solicitud_prima_total = this.solicitudService.poliza.anexo_poliza.monto_prima * (this.solicitudService.persona_banco_operacion.Operacion_Plazo > 60 ? 60 : this.solicitudService.persona_banco_operacion.Operacion_Plazo) + '';
                                this.solicitudService.setAtributosToAsegurado();

                                if (this.solicitudService.persona_banco_operacion.Operacion_Estado && this.solicitudService.persona_banco_operacion.Operacion_Estado.includes('APROBAD')) {
                                  this.solicitudService.isLoadingAgain = false;
                                  this.solicitudService.stopSavingBySolicitud = false;
                                  this.solicitudService.solicitudAprobada = true;
                                  this.solicitudService.solicitudAjena = false;
                                } else {
                                  validacion = this.solicitudService.msgSolicitudSciInvalida = "Advertencia: El Nro de solicitud de cr\xE9dito seleccionado: ".concat(solicitud_sci_selected, ", no fue aprobado");
                                  this.solicitudService.solicitudAjena = false;

                                  if (!this.solicitudService.msgs_error.find(function (param) {
                                    return param.detail == validacion;
                                  }) && this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                                    this.solicitudService.setMsgsWarnsOrErrors(this.solicitudService.asegurado, this.solicitudService.parametroError, validacion);
                                  }

                                  this.solicitudService.solicitudAprobada = false;
                                  this.solicitudService.stopSavingBySolicitud = true;
                                  this.solicitudService.displaySolicitudSciInvalida = false;
                                }

                                if (this.solicitudService.persona_banco_operacion.Operacion_Usuario_Oficial && this.solicitudService.persona_banco_operacion.Operacion_Usuario_Oficial.includes(this.solicitudService.usuarioLogin.usuario_login)) {
                                  this.solicitudService.solicitudAjena = false;
                                  this.solicitudService.displaySolicitudSciInvalida = false;
                                } else {
                                  this.solicitudService.solicitudAjena = true;
                                  _validacion = this.solicitudService.msgSolicitudSciInvalida = "Advertencia: El Nro de solicitud de cr\xE9dito seleccionado: ".concat(solicitud_sci_selected, ", pertenece al usuario ").concat(this.solicitudService.persona_banco_operacion.Operacion_Usuario_Oficial, ", su usuario actual es: ").concat(this.solicitudService.usuarioLogin.usuario_login);

                                  if (!this.solicitudService.msgs_error.find(function (param) {
                                    return param.detail == _validacion;
                                  }) && this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                                    this.solicitudService.setMsgsWarnsOrErrors(this.solicitudService.asegurado, this.solicitudService.parametroError, _validacion);
                                  }

                                  this.solicitudService.displaySolicitudSciInvalida = true;
                                }

                                this.solicitudService.disableToSave = false;

                                if (!(typeof callback == 'function')) {
                                  _context30.next = 13;
                                  break;
                                }

                                _context30.next = 13;
                                return callback(this.solicitudService.solicitudAprobada, this.solicitudService.solicitudAjena, solicitud_sci_selected);

                              case 13:
                                _context30.next = 23;
                                break;

                              case 15:
                                this.solicitudService.solicitudAprobada = false;
                                this.solicitudService.stopSavingBySolicitud = true;
                                this.solicitudService.displaySolicitudSciInvalida = true;
                                this.solicitudService.persona_banco_operacion = new _src_core_modelos_persona_banco_operacion__WEBPACK_IMPORTED_MODULE_34__["persona_banco_operacion"]();
                                this.solicitudService.msgSolicitudSciInvalida = 'Existen problemas de comunicacion con el sistema del banco, por favor comunicate con el administrador del sistema';

                                if (!(typeof callback == 'function')) {
                                  _context30.next = 23;
                                  break;
                                }

                                _context30.next = 23;
                                return callback(this.solicitudService.solicitudAprobada);

                              case 23:
                              case "end":
                                return _context30.stop();
                            }
                          }
                        }, _callee30, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this35.router.navigate(['']);
                      } else {
                        _this35.solicitudService.nombreServicioBanco = 'getSolicitudPrimeraEtapa';
                        _this35.solicitudService.displayErrorRespuestaBanco = true;
                      }
                    });

                  case 6:
                    _context31.next = 12;
                    break;

                  case 8:
                    this.solicitudService.isLoadingAgain = false;

                    if (!(typeof callback == 'function')) {
                      _context31.next = 12;
                      break;
                    }

                    _context31.next = 12;
                    return callback(this.solicitudService.solicitudAprobada);

                  case 12:
                  case "end":
                    return _context31.stop();
                }
              }
            }, _callee31, this);
          }));
        }
      }, {
        key: "validaDatosFromSolicitudPrimeraEtapaServiceSelected",
        value: function validaDatosFromSolicitudPrimeraEtapaServiceSelected() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee32() {
            var toContinue, i, solicitudSci;
            return _regeneratorRuntime().wrap(function _callee32$(_context32) {
              while (1) {
                switch (_context32.prev = _context32.next) {
                  case 0:
                    toContinue = false;
                    this.solicitudesObservaciones = [];
                    this.solicitudService.SolicitudesSci = [];
                    this.solicitudService.SolicitudesSci.push({
                      label: "Seleccione una solicitud",
                      value: null
                    });
                    i = 0;

                  case 5:
                    if (!(i < this.solicitudService.persona_banco_solicitud.solicitudes_sci.length)) {
                      _context32.next = 16;
                      break;
                    }

                    solicitudSci = this.solicitudService.persona_banco_solicitud.solicitudes_sci[i];
                    this.solicitudService.SolicitudesSci.push({
                      label: solicitudSci,
                      value: solicitudSci
                    });

                    if (this.solicitudService.persona_banco_solicitud.solicitud_sci_selected && this.solicitudService.persona_banco_solicitud.solicitud_sci_selected.substring(0, 1) == '9') {
                      this.solicitudService.persona_banco.debito_automatico = this.solicitudService.pagoDebitAutomatico.id + '';
                      this.solicitudService.pagoACredito = true;
                      this.solicitudService.editPagoEfectivo = false;
                    } else {
                      this.solicitudService.persona_banco_solicitud.solicitud_sci_selected = this.solicitudService.persona_banco_solicitud.solicitud_sci_selected ? this.solicitudService.persona_banco_solicitud.solicitud_sci_selected : solicitudSci;
                      this.solicitudService.pagoACredito = false;
                      this.solicitudService.persona_banco.debito_automatico = this.solicitudService.pagoEfectivo.id + '';
                    }

                    this.solicitudService.persona_banco_solicitud.solicitud_forma_pago = this.solicitudService.parametroPagoContado.id + ''; // await this.getDatosFromSolicitudPrimeraEtapaService(solicitudSci, async (solicitudAprobada, solicitudAjena, solicitudSci) => {
                    //     this.solicitudesObservaciones.push({solicitudAprobada, solicitudAjena, solicitudSelected:this.solicitudService.persona_banco_solicitud.solicitud_sci_selected});
                    //     if (typeof callback == 'function') {
                    //         await callback(toContinue, this.solicitudesObservaciones, i == this.solicitudService.persona_banco_solicitud.solicitudes_sci.length-1);
                    //     }
                    // });

                    if (!(typeof callback == 'function')) {
                      _context32.next = 13;
                      break;
                    }

                    _context32.next = 13;
                    return callback(toContinue, this.solicitudesObservaciones, i == this.solicitudService.persona_banco_solicitud.solicitudes_sci.length - 1);

                  case 13:
                    i++;
                    _context32.next = 5;
                    break;

                  case 16:
                  case "end":
                    return _context32.stop();
                }
              }
            }, _callee32, this);
          }));
        }
      }, {
        key: "getDatosFromCustomerService",
        value: function getDatosFromCustomerService() {
          var docId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          var extension = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
          var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee36() {
            var _this36 = this;

            return _regeneratorRuntime().wrap(function _callee36$(_context36) {
              while (1) {
                switch (_context36.prev = _context36.next) {
                  case 0:
                    this.solicitudService.esClienteBanco = false;
                    _context36.next = 3;
                    return this.soapuiService.getCustomerSol(this.solicitudService.doc_id ? this.solicitudService.doc_id : docId, this.solicitudService.extension ? parseInt(this.solicitudService.extension) : parseInt(extension)).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this36, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee35() {
                        var _this37 = this;

                        var response;
                        return _regeneratorRuntime().wrap(function _callee35$(_context35) {
                          while (1) {
                            switch (_context35.prev = _context35.next) {
                              case 0:
                                response = res;

                                if (response.data && Object.keys(response.data).length) {
                                  this.solicitudService.persona_banco = this.solicitudService.persona_banco_bkp = response.data.general;
                                  this.solicitudService.persona_banco.extension = this.solicitudService.persona_banco.extension == '' ? '12' : this.solicitudService.persona_banco.extension;
                                  this.solicitudService.persona_banco_datos = new _src_core_modelos_persona_banco_datos__WEBPACK_IMPORTED_MODULE_37__["persona_banco_datos"]();
                                  this.solicitudService.persona_banco_solicitud = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco_solicitud"]();
                                  this.solicitudService.persona_banco_operacion = new _src_core_modelos_persona_banco_operacion__WEBPACK_IMPORTED_MODULE_34__["persona_banco_operacion"]();
                                  this.solicitudService.persona_banco_solicitud = response.data.solicitud;
                                  this.solicitudService.setAtributosToAsegurado(function () {
                                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this37, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee34() {
                                      var _this38 = this;

                                      return _regeneratorRuntime().wrap(function _callee34$(_context34) {
                                        while (1) {
                                          switch (_context34.prev = _context34.next) {
                                            case 0:
                                              if (!(this.solicitudService.persona_banco && Object(util__WEBPACK_IMPORTED_MODULE_6__["isObject"])(this.solicitudService.persona_banco) && Object.keys(this.solicitudService.persona_banco).length)) {
                                                _context34.next = 17;
                                                break;
                                              }

                                              this.solicitudService.showMoneda = false;
                                              this.solicitudService.showNroCuenta = false;
                                              this.solicitudService.showTipoCuenta = false;
                                              this.solicitudService.esClienteBanco = true;

                                              if (!(this.solicitudService.persona_banco_solicitud && this.solicitudService.persona_banco_solicitud.solicitudes_sci.length)) {
                                                _context34.next = 10;
                                                break;
                                              }

                                              _context34.next = 8;
                                              return this.validaDatosFromSolicitudPrimeraEtapaServiceSelected(function (toContinue, solicitudesObservaciones, loopFinish) {
                                                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this38, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee33() {
                                                  var _this39 = this;

                                                  var personaBancoOperacion;
                                                  return _regeneratorRuntime().wrap(function _callee33$(_context33) {
                                                    while (1) {
                                                      switch (_context33.prev = _context33.next) {
                                                        case 0:
                                                          if (!loopFinish) {
                                                            _context33.next = 19;
                                                            break;
                                                          }

                                                          if (solicitudesObservaciones.find(function (param) {
                                                            return param.solicitudAjena == true;
                                                          })) {
                                                            toContinue = false;
                                                          } else {
                                                            toContinue = true;
                                                          }

                                                          if (this.solicitudService.persona_banco_operaciones.find(function (param) {
                                                            return param.Operacion_Usuario_Oficial != _this39.solicitudService.userInfo.usuario_login;
                                                          })) {
                                                            toContinue = false;
                                                          } else {
                                                            toContinue = true;
                                                          }

                                                          personaBancoOperacion = this.solicitudService.persona_banco_operaciones.find(function (param) {
                                                            return param.Operacion_Solicitud == _this39.solicitudService.persona_banco_solicitud.solicitud_sci_selected;
                                                          });
                                                          this.solicitudService.persona_banco_operacion = personaBancoOperacion ? personaBancoOperacion : this.solicitudService.persona_banco_operacion;
                                                          this.solicitudService.conCreditoAsociado = true;
                                                          this.solicitudService.persona_banco.debito_automatico = '';

                                                          if (![10, 12].includes(parseInt(this.solicitudService.id_poliza + ''))) {
                                                            _context33.next = 16;
                                                            break;
                                                          }

                                                          this.solicitudService.persona_banco_accounts = [];
                                                          this.solicitudService.Cuentas = [];
                                                          this.solicitudService.persona_banco_account = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();

                                                          if (!(typeof callback == 'function')) {
                                                            _context33.next = 14;
                                                            break;
                                                          }

                                                          _context33.next = 14;
                                                          return callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado);

                                                        case 14:
                                                          _context33.next = 19;
                                                          break;

                                                        case 16:
                                                          if (!(typeof callback == 'function')) {
                                                            _context33.next = 19;
                                                            break;
                                                          }

                                                          _context33.next = 19;
                                                          return callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado);

                                                        case 19:
                                                        case "end":
                                                          return _context33.stop();
                                                      }
                                                    }
                                                  }, _callee33, this);
                                                }));
                                              });

                                            case 8:
                                              _context34.next = 15;
                                              break;

                                            case 10:
                                              this.solicitudService.conCreditoAsociado = false;

                                              if (!(typeof callback == 'function')) {
                                                _context34.next = 14;
                                                break;
                                              }

                                              _context34.next = 14;
                                              return callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado);

                                            case 14:
                                              this.solicitudService.isLoadingAgain = false;

                                            case 15:
                                              _context34.next = 21;
                                              break;

                                            case 17:
                                              if (!(typeof callback == 'function')) {
                                                _context34.next = 20;
                                                break;
                                              }

                                              _context34.next = 20;
                                              return callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado);

                                            case 20:
                                              this.solicitudService.isLoadingAgain = false;

                                            case 21:
                                            case "end":
                                              return _context34.stop();
                                          }
                                        }
                                      }, _callee34, this);
                                    }));
                                  });
                                } else {
                                  this.solicitudService.nombreServicioBanco = 'getCustomerSol';
                                  this.solicitudService.displayErrorRespuestaBanco = true;
                                  this.solicitudService.isLoadingAgain = false;
                                }

                              case 2:
                              case "end":
                                return _context35.stop();
                            }
                          }
                        }, _callee35, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this36.router.navigate(['']);
                      } else {
                        _this36.solicitudService.nombreServicioBanco = 'getCustomerSol';
                        _this36.solicitudService.displayErrorRespuestaBanco = true;
                      }
                    });

                  case 3:
                  case "end":
                    return _context36.stop();
                }
              }
            }, _callee36, this);
          }));
        }
      }, {
        key: "getDatosFromAccountService",
        value: function getDatosFromAccountService() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee38() {
            var _this40 = this;

            return _regeneratorRuntime().wrap(function _callee38$(_context38) {
              while (1) {
                switch (_context38.prev = _context38.next) {
                  case 0:
                    this.solicitudService.isLoadingAgain = true;
                    _context38.next = 3;
                    return this.soapuiService.getAccount(this.solicitudService.persona_banco.cod_agenda).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this40, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee37() {
                        var _this41 = this;

                        var response, keys, values;
                        return _regeneratorRuntime().wrap(function _callee37$(_context37) {
                          while (1) {
                            switch (_context37.prev = _context37.next) {
                              case 0:
                                this.solicitudService.Cuentas = [{
                                  label: "Seleccione Nro de Cuenta",
                                  value: null
                                }];
                                response = res;
                                this.solicitudService.isLoadingAgain = false;

                                if (!(Object(util__WEBPACK_IMPORTED_MODULE_6__["isObject"])(response.data) && Object.keys(response.data).length)) {
                                  _context37.next = 17;
                                  break;
                                }

                                this.solicitudService.persona_banco_accounts = [];

                                if (!(response.data.nrocuenta != undefined)) {
                                  _context37.next = 10;
                                  break;
                                }

                                //this.solicitudService.persona_banco_account = response.data;
                                this.solicitudService.persona_banco_accounts.push(response.data);
                                this.solicitudService.Cuentas.push({
                                  label: response.data.nrocuenta + ' ' + this.solicitudService.MonedasParametroCod[response.data.moneda + ''],
                                  value: response.data.nrocuenta
                                });
                                _context37.next = 14;
                                break;

                              case 10:
                                keys = Object.keys(response.data);
                                values = Object.values(response.data);
                                _context37.next = 14;
                                return keys.forEach(function (key) {
                                  _this41.solicitudService.Cuentas.push({
                                    label: values[key].nrocuenta + ' ' + _this41.solicitudService.MonedasParametroCod[values[key].moneda],
                                    value: values[key].nrocuenta
                                  });

                                  _this41.solicitudService.persona_banco_accounts.push(values[key]);
                                });

                              case 14:
                                if (this.solicitudService.Cuentas.length) {
                                  this.solicitudService.esClienteBanco = true;
                                  this.solicitudService.tieneCuentaBanco = true;
                                } else {
                                  this.solicitudService.esClienteBanco = false;
                                  this.solicitudService.tieneCuentaBanco = false;
                                  this.solicitudService.displayClienteSinCuenta = true;
                                }

                                _context37.next = 19;
                                break;

                              case 17:
                                this.solicitudService.persona_banco_accounts = [];
                                this.solicitudService.Cuentas = [];

                              case 19:
                                this.solicitudService.persona_banco_account = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();

                                if (!(typeof callback == 'function')) {
                                  _context37.next = 23;
                                  break;
                                }

                                _context37.next = 23;
                                return callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado, this.solicitudService.tieneCuentaBanco);

                              case 23:
                              case "end":
                                return _context37.stop();
                            }
                          }
                        }, _callee37, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this40.router.navigate(['']);
                      } else {
                        _this40.solicitudService.nombreServicioBanco = 'getAccount';
                        _this40.solicitudService.displayErrorRespuestaBanco = true;
                      }
                    });

                  case 3:
                  case "end":
                    return _context38.stop();
                }
              }
            }, _callee38, this);
          }));
        }
      }, {
        key: "getDatosFromTarjetaDebitoCuentaService",
        value: function getDatosFromTarjetaDebitoCuentaService() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee40() {
            var _this42 = this;

            return _regeneratorRuntime().wrap(function _callee40$(_context40) {
              while (1) {
                switch (_context40.prev = _context40.next) {
                  case 0:
                    if (!this.solicitudService.persona_banco_datos.nro_tarjeta) {
                      _context40.next = 6;
                      break;
                    }

                    this.solicitudService.isLoadingAgain = true;
                    _context40.next = 4;
                    return this.soapuiService.getTarjetaDebitoCuentas(this.solicitudService.persona_banco_datos.nro_tarjeta + '').subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this42, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee39() {
                        var _this43 = this;

                        var response, personaBancoAccount, keys, values;
                        return _regeneratorRuntime().wrap(function _callee39$(_context39) {
                          while (1) {
                            switch (_context39.prev = _context39.next) {
                              case 0:
                                this.solicitudService.isLoadingAgain = false;
                                this.persona_banco_tarjetas_debito = [];
                                response = res;

                                if (!(Object(util__WEBPACK_IMPORTED_MODULE_6__["isObject"])(response.data) && Object.keys(response.data).length)) {
                                  _context39.next = 7;
                                  break;
                                }

                                if (response.data.Cuentas != undefined) {
                                  this.solicitudService.persona_banco_accounts = [];

                                  if (!this.guardandoTitular && !this.solicitudService.editandoTitular) {
                                    this.displayNroCuentasActualizados = true;
                                  }

                                  if (response.data.Cuentas.nro_cuenta != undefined) {
                                    this.solicitudService.Cuentas = [{
                                      label: "Seleccione Nro de Cuenta",
                                      value: null
                                    }];
                                    this.persona_banco_tarjetas_debito.push(response.data.Cuentas);
                                    this.parametroMoneda = this.solicitudService.parametrosMonedas.find(function (params) {
                                      return params.parametro_descripcion == response.data.Cuentas.moneda.trim();
                                    });
                                    personaBancoAccount = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();
                                    personaBancoAccount.nrocuenta = response.data.Cuentas.nro_cuenta;
                                    personaBancoAccount.moneda = this.parametroMoneda.parametro_cod;
                                    personaBancoAccount.fecha_expiracion = new Date(response.data.Cuentas.fecha_expiracion);
                                    personaBancoAccount.cod_agenda = this.solicitudService.persona_banco.cod_agenda;
                                    this.solicitudService.persona_banco_accounts.push(personaBancoAccount);
                                    this.solicitudService.persona_banco_account.fecha_expiracion = new Date(response.data.Cuentas.fecha_expiracion);
                                    this.solicitudService.Cuentas.push({
                                      label: personaBancoAccount.nrocuenta + ' ' + this.solicitudService.MonedasParametroCod[personaBancoAccount.moneda + ''],
                                      value: personaBancoAccount.nrocuenta
                                    });
                                  } else {
                                    this.solicitudService.Cuentas = [{
                                      label: "Seleccione Nro de Cuenta",
                                      value: null
                                    }];
                                    keys = Object.keys(response.data.Cuentas);
                                    values = Object.values(response.data.Cuentas);
                                    values.forEach(function (value) {
                                      _this43.persona_banco_tarjetas_debito.push(value);

                                      _this43.parametroMoneda = _this43.solicitudService.parametrosMonedas.find(function (params) {
                                        return params.parametro_descripcion == value.moneda.trim();
                                      });
                                      var personaBancoAccount = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();
                                      personaBancoAccount.nrocuenta = value.nro_cuenta;
                                      personaBancoAccount.moneda = _this43.parametroMoneda.parametro_cod;
                                      personaBancoAccount.fecha_expiracion = new Date(value.fecha_expiracion + '');
                                      personaBancoAccount.cod_agenda = _this43.solicitudService.persona_banco.cod_agenda;

                                      _this43.solicitudService.persona_banco_accounts.push(personaBancoAccount);

                                      _this43.solicitudService.persona_banco_account.fecha_expiracion = new Date(value.fecha_expiracion + '');

                                      _this43.solicitudService.Cuentas.push({
                                        label: personaBancoAccount.nrocuenta + ' ' + _this43.solicitudService.MonedasParametroCod[personaBancoAccount.moneda + ''],
                                        value: personaBancoAccount.nrocuenta
                                      });
                                    });
                                  }
                                }

                                _context39.next = 18;
                                break;

                              case 7:
                                if (!(this.solicitudService.persona_banco.cod_agenda != '' && this.solicitudService.persona_banco.cod_agenda != undefined && this.solicitudService.persona_banco.cod_agenda != "undefined")) {
                                  _context39.next = 12;
                                  break;
                                }

                                _context39.next = 10;
                                return this.getDatosFromAccountService(function (res) {
                                  _this43.persona_banco_tarjetas_debito = [];
                                  _this43.displayNroTarjetaSinCuentas = true;

                                  if (typeof callback == 'function') {
                                    callback();
                                  }
                                });

                              case 10:
                                _context39.next = 18;
                                break;

                              case 12:
                                this.solicitudService.persona_banco_accounts = [];
                                this.solicitudService.Cuentas = [];
                                this.solicitudService.persona_banco_account = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();

                                if (!(typeof callback == 'function')) {
                                  _context39.next = 18;
                                  break;
                                }

                                _context39.next = 18;
                                return callback();

                              case 18:
                                if (!(typeof callback == 'function')) {
                                  _context39.next = 21;
                                  break;
                                }

                                _context39.next = 21;
                                return callback();

                              case 21:
                              case "end":
                                return _context39.stop();
                            }
                          }
                        }, _callee39, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this42.router.navigate(['']);
                      } else {
                        _this42.solicitudService.nombreServicioBanco = 'getTarjetaDebitoCuentas';
                        _this42.solicitudService.displayErrorRespuestaBanco = true;
                      }
                    });

                  case 4:
                    _context40.next = 7;
                    break;

                  case 6:
                    this.displayIntroduscaNroTarjeta = true;

                  case 7:
                  case "end":
                    return _context40.stop();
                }
              }
            }, _callee40, this);
          }));
        }
      }, {
        key: "setAnexoPoliza",
        value: function setAnexoPoliza() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee42() {
            var _this44 = this;

            return _regeneratorRuntime().wrap(function _callee42$(_context42) {
              while (1) {
                switch (_context42.prev = _context42.next) {
                  case 0:
                    _context42.next = 2;
                    return this.anexoService.findAnexoPoliza(this.solicitudService.poliza.id).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this44, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee41() {
                        var _this45 = this;

                        var response;
                        return _regeneratorRuntime().wrap(function _callee41$(_context41) {
                          while (1) {
                            switch (_context41.prev = _context41.next) {
                              case 0:
                                response = res;
                                this.solicitudService.anexosPoliza = response.data;
                                this.solicitudService.anexosPoliza.forEach(function (anexoPoliza) {
                                  _this45.solicitudService.Planes.push({
                                    label: anexoPoliza.descripcion,
                                    value: parseInt(anexoPoliza.id + '')
                                  });

                                  _this45.solicitudService.planes.push(anexoPoliza);

                                  _this45.solicitudService.PlanesParametroCod[anexoPoliza.id + ''] = anexoPoliza.descripcion;
                                });

                              case 3:
                              case "end":
                                return _context41.stop();
                            }
                          }
                        }, _callee41, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this44.router.navigate(['login']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 2:
                  case "end":
                    return _context42.stop();
                }
              }
            }, _callee42, this);
          }));
        }
      }, {
        key: "setAnexoPolizaPlanes",
        value: function setAnexoPolizaPlanes() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee44() {
            var _this46 = this;

            return _regeneratorRuntime().wrap(function _callee44$(_context44) {
              while (1) {
                switch (_context44.prev = _context44.next) {
                  case 0:
                    _context44.next = 2;
                    return this.anexoService.findPlanesAnexoPoliza(this.solicitudService.poliza.id).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this46, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee43() {
                        var _this47 = this;

                        var response;
                        return _regeneratorRuntime().wrap(function _callee43$(_context43) {
                          while (1) {
                            switch (_context43.prev = _context43.next) {
                              case 0:
                                response = res;
                                this.solicitudService.anexosPoliza = response.data;
                                this.anexosPlanesPoliza = this.solicitudService.anexosPoliza.filter(function (params) {
                                  return params.id_tipo == 110;
                                });
                                this.anexosPlanesPoliza.forEach(function (anexoPoliza) {
                                  _this47.solicitudService.Planes.push({
                                    label: anexoPoliza.descripcion,
                                    value: anexoPoliza.id
                                  });

                                  _this47.solicitudService.planes.push(anexoPoliza);

                                  _this47.solicitudService.PlanesParametroCod[anexoPoliza.id + ''] = anexoPoliza.descripcion;
                                });

                              case 4:
                              case "end":
                                return _context43.stop();
                            }
                          }
                        }, _callee43, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this46.router.navigate(['login']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 2:
                  case "end":
                    return _context44.stop();
                }
              }
            }, _callee44, this);
          }));
        }
      }, {
        key: "setAnexoPolizaWithAsegurado",
        value: function setAnexoPolizaWithAsegurado() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee46() {
            var _this48 = this;

            return _regeneratorRuntime().wrap(function _callee46$(_context46) {
              while (1) {
                switch (_context46.prev = _context46.next) {
                  case 0:
                    _context46.next = 2;
                    return this.anexoService.findAnexoPolizaWithAsegurado(this.solicitudService.poliza.id, this.solicitudService.asegurado.id).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this48, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee45() {
                        var _this49 = this;

                        var response;
                        return _regeneratorRuntime().wrap(function _callee45$(_context45) {
                          while (1) {
                            switch (_context45.prev = _context45.next) {
                              case 0:
                                response = res;
                                this.solicitudService.anexosPoliza = response.data;

                                if (this.solicitudService.anexosPoliza.length) {
                                  this.anexosPlanesPoliza = this.solicitudService.anexosPoliza.filter(function (params) {
                                    return params.id_tipo == 110;
                                  });
                                  this.anexosPlanesPoliza.forEach(function (anexoPoliza) {
                                    _this49.solicitudService.Planes.push({
                                      label: anexoPoliza.descripcion,
                                      value: anexoPoliza.id
                                    });

                                    _this49.solicitudService.planes.push(anexoPoliza);

                                    _this49.solicitudService.PlanesParametroCod[anexoPoliza.id + ''] = anexoPoliza.descripcion;
                                  });
                                }

                              case 3:
                              case "end":
                                return _context45.stop();
                            }
                          }
                        }, _callee45, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this48.router.navigate(['login']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 2:
                  case "end":
                    return _context46.stop();
                }
              }
            }, _callee46, this);
          }));
        }
      }, {
        key: "setAnexoPolizaWithAseguradoAnexo",
        value: function setAnexoPolizaWithAseguradoAnexo() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee49() {
            var _this50 = this;

            return _regeneratorRuntime().wrap(function _callee49$(_context49) {
              while (1) {
                switch (_context49.prev = _context49.next) {
                  case 0:
                    if (!(this.solicitudService.persona_banco_datos.plan != 0)) {
                      _context49.next = 3;
                      break;
                    }

                    _context49.next = 3;
                    return this.anexoService.listaAtributosByIdPoliza(this.solicitudService.poliza.id, this.solicitudService.asegurado.id, this.solicitudService.persona_banco_datos.plan).then(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this50, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee48() {
                        var _this51 = this;

                        var response;
                        return _regeneratorRuntime().wrap(function _callee48$(_context48) {
                          while (1) {
                            switch (_context48.prev = _context48.next) {
                              case 0:
                                response = res;
                                this.solicitudService.anexosPoliza = response.data;

                                if (this.solicitudService.anexosPoliza.length) {
                                  this.anexosPlanesPoliza = this.solicitudService.anexosPoliza.filter(function (params) {
                                    return params.id_tipo === 110;
                                  });
                                  this.anexosPlanesPoliza.forEach(function (anexoPoliza) {
                                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this51, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee47() {
                                      return _regeneratorRuntime().wrap(function _callee47$(_context47) {
                                        while (1) {
                                          switch (_context47.prev = _context47.next) {
                                            case 0:
                                              _context47.next = 2;
                                              return this.solicitudService.Planes.push({
                                                label: anexoPoliza.descripcion,
                                                value: anexoPoliza.id
                                              });

                                            case 2:
                                            case "end":
                                              return _context47.stop();
                                          }
                                        }
                                      }, _callee47, this);
                                    }));
                                  });
                                }

                              case 3:
                              case "end":
                                return _context48.stop();
                            }
                          }
                        }, _callee48, this);
                      }));
                    });

                  case 3:
                  case "end":
                    return _context49.stop();
                }
              }
            }, _callee49, this);
          }));
        }
      }, {
        key: "BuscarCliente",
        value: function BuscarCliente() {
          var docId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          var docIdExt = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
          var search = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
          var updatePersona = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee57() {
            var _this52 = this;

            return _regeneratorRuntime().wrap(function _callee57$(_context57) {
              while (1) {
                switch (_context57.prev = _context57.next) {
                  case 0:
                    this.search = search;
                    this.solicitudService.buscando = true;
                    this.solicitudService.persona_banco = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco"]();
                    this.solicitudService.persona_banco_datos = new _src_core_modelos_persona_banco_datos__WEBPACK_IMPORTED_MODULE_37__["persona_banco_datos"]();
                    this.solicitudService.persona_banco_account = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();
                    this.solicitudService.persona_banco_solicitud = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco_solicitud"]();
                    this.solicitudService.persona_banco_operacion = new _src_core_modelos_persona_banco_operacion__WEBPACK_IMPORTED_MODULE_34__["persona_banco_operacion"]();
                    this.solicitudService.persona_banco_pep = new _src_core_modelos_persona_banco_pep__WEBPACK_IMPORTED_MODULE_36__["persona_banco_pep"]();
                    this.solicitudService.asegurado = new _src_core_modelos_asegurado__WEBPACK_IMPORTED_MODULE_31__["Asegurado"]();
                    this.solicitudService.setUserLogin();
                    this.solicitudService.editandoTitular = false;
                    this.solicitudService.isLoadingAgain = true;

                    if (docId && docIdExt) {
                      this.solicitudService.extension = docIdExt + '';
                      this.solicitudService.doc_id = docId;
                    }

                    if (!(this.solicitudService.doc_id && this.solicitudService.extension)) {
                      _context57.next = 18;
                      break;
                    }

                    _context57.next = 16;
                    return this.getDatosFromCustomerService(this.solicitudService.doc_id, this.solicitudService.extension, function (isBankClient, conCreditoAsociado) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this52, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee56() {
                        var _this53 = this;

                        var persona_doc_id, persona_doc_id_ext;
                        return _regeneratorRuntime().wrap(function _callee56$(_context56) {
                          while (1) {
                            switch (_context56.prev = _context56.next) {
                              case 0:
                                persona_doc_id = this.solicitudService.doc_id ? this.solicitudService.doc_id.trim() : this.solicitudService.doc_id;
                                persona_doc_id_ext = this.solicitudService.extension;

                                if (!(this.solicitudService.persona_banco && Object(util__WEBPACK_IMPORTED_MODULE_6__["isObject"])(this.solicitudService.persona_banco) && Object.keys(this.solicitudService.persona_banco).length)) {
                                  _context56.next = 7;
                                  break;
                                }

                                _context56.next = 5;
                                return this.personaService.findPersonasAseguradasConAtributosByDocIdYPoliza(persona_doc_id, this.solicitudService.objetoAseguradoDatosComplementarios.id, [10, 12]).subscribe(function (res) {
                                  return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this53, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee55() {
                                    var _this54 = this;

                                    var response;
                                    return _regeneratorRuntime().wrap(function _callee55$(_context55) {
                                      while (1) {
                                        switch (_context55.prev = _context55.next) {
                                          case 0:
                                            // await this.setAnexoPolizaPlanes();
                                            response = res;
                                            this.solicitudService.asegurados = response.data; // if (this.solicitudService.asegurados.length) {

                                            if (!this.solicitudService.asegurados.length) {
                                              _context55.next = 19;
                                              break;
                                            }

                                            this.aseguradoWithDiferentDocIdExt = this.solicitudService.asegurados.find(function (param) {
                                              return param.entidad.persona.persona_doc_id_ext + '' != persona_doc_id_ext + '';
                                            });

                                            if (!this.aseguradoWithDiferentDocIdExt) {
                                              _context55.next = 15;
                                              break;
                                            }

                                            if (!updatePersona) {
                                              _context55.next = 10;
                                              break;
                                            }

                                            _context55.next = 8;
                                            return this.cambiarPersonaExtension(this.aseguradoWithDiferentDocIdExt.entidad.persona.id, this.aseguradoWithDiferentDocIdExt.id_instancia_poliza, this.solicitudService.doc_id, this.solicitudService.extension);

                                          case 8:
                                            _context55.next = 12;
                                            break;

                                          case 10:
                                            _context55.next = 12;
                                            return this.afterSearch();

                                          case 12:
                                            this.solicitudService.isLoadingAgain = false;
                                            _context55.next = 17;
                                            break;

                                          case 15:
                                            _context55.next = 17;
                                            return this.solicitudService.afterSearchWithLimits(this.search, this.userform, function () {
                                              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this54, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee50() {
                                                return _regeneratorRuntime().wrap(function _callee50$(_context50) {
                                                  while (1) {
                                                    switch (_context50.prev = _context50.next) {
                                                      case 0:
                                                        _context50.next = 2;
                                                        return this.newAsegurado(persona_doc_id, persona_doc_id_ext);

                                                      case 2:
                                                      case "end":
                                                        return _context50.stop();
                                                    }
                                                  }
                                                }, _callee50, this);
                                              }));
                                            }, function () {
                                              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this54, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee51() {
                                                return _regeneratorRuntime().wrap(function _callee51$(_context51) {
                                                  while (1) {
                                                    switch (_context51.prev = _context51.next) {
                                                      case 0:
                                                        this.abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext);

                                                      case 1:
                                                      case "end":
                                                        return _context51.stop();
                                                    }
                                                  }
                                                }, _callee51, this);
                                              }));
                                            }, function () {
                                              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this54, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee52() {
                                                return _regeneratorRuntime().wrap(function _callee52$(_context52) {
                                                  while (1) {
                                                    switch (_context52.prev = _context52.next) {
                                                      case 0:
                                                        this.reiniciarEcoAccidentes();

                                                      case 1:
                                                      case "end":
                                                        return _context52.stop();
                                                    }
                                                  }
                                                }, _callee52, this);
                                              }));
                                            }, function () {
                                              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this54, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee53() {
                                                return _regeneratorRuntime().wrap(function _callee53$(_context53) {
                                                  while (1) {
                                                    switch (_context53.prev = _context53.next) {
                                                      case 0:
                                                        this.solicitudService.isLoadingAgain = false;
                                                        _context53.next = 3;
                                                        return this.newAsegurado(persona_doc_id, persona_doc_id_ext);

                                                      case 3:
                                                        this.stateButtonsFlow();

                                                      case 4:
                                                      case "end":
                                                        return _context53.stop();
                                                    }
                                                  }
                                                }, _callee53, this);
                                              }));
                                            });

                                          case 17:
                                            _context55.next = 32;
                                            break;

                                          case 19:
                                            if (isBankClient) {
                                              _context55.next = 23;
                                              break;
                                            }

                                            this.solicitudService.displayNuevoCliente = true;
                                            _context55.next = 30;
                                            break;

                                          case 23:
                                            if (conCreditoAsociado) {
                                              _context55.next = 27;
                                              break;
                                            }

                                            this.solicitudService.displayClienteSinCredito = true;
                                            _context55.next = 30;
                                            break;

                                          case 27:
                                            _context55.next = 29;
                                            return this.newAsegurado(persona_doc_id, persona_doc_id_ext);

                                          case 29:
                                            this.abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext);

                                          case 30:
                                            this.solicitudService.isLoadingAgain = false;
                                            this.stateButtonsFlow();

                                          case 32:
                                            _context55.next = 34;
                                            return this.solicitudService.setAtributosToAsegurado(function () {
                                              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this54, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee54() {
                                                return _regeneratorRuntime().wrap(function _callee54$(_context54) {
                                                  while (1) {
                                                    switch (_context54.prev = _context54.next) {
                                                      case 0:
                                                        this.stateButtonsFlow();

                                                      case 1:
                                                      case "end":
                                                        return _context54.stop();
                                                    }
                                                  }
                                                }, _callee54, this);
                                              }));
                                            });

                                          case 34:
                                          case "end":
                                            return _context55.stop();
                                        }
                                      }
                                    }, _callee55, this);
                                  }));
                                }, function (err) {
                                  if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                                    _this53.router.navigate(['login']);
                                  } else {
                                    console.log(err);
                                  }
                                });

                              case 5:
                                _context56.next = 14;
                                break;

                              case 7:
                                this.solicitudService.persona_banco_account = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();
                                this.solicitudService.persona_banco_datos = new _src_core_modelos_persona_banco_datos__WEBPACK_IMPORTED_MODULE_37__["persona_banco_datos"]();
                                this.solicitudService.persona_banco = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco"]();
                                this.solicitudService.displayNuevoCliente = true;
                                this.solicitudService.isLoadingAgain = false; // await this.newAsegurado(persona_doc_id, persona_doc_id_ext);

                                this.solicitudService.displayClienteSinCredito = true;
                                this.stateButtonsFlow();

                              case 14:
                              case "end":
                                return _context56.stop();
                            }
                          }
                        }, _callee56, this);
                      }));
                    });

                  case 16:
                    _context57.next = 20;
                    break;

                  case 18:
                    this.solicitudService.displayNuevoCliente = true;
                    this.solicitudService.isLoadingAgain = false;

                  case 20:
                    this.cols = [];

                  case 21:
                  case "end":
                    return _context57.stop();
                }
              }
            }, _callee57, this);
          }));
        }
      }, {
        key: "newAsegurado",
        value: function newAsegurado(persona_doc_id, persona_doc_id_ext) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee61() {
            var _this55 = this;

            var oldAsegurado;
            return _regeneratorRuntime().wrap(function _callee61$(_context61) {
              while (1) {
                switch (_context61.prev = _context61.next) {
                  case 0:
                    oldAsegurado = new _src_core_modelos_asegurado__WEBPACK_IMPORTED_MODULE_31__["Asegurado"]();

                    if (Object(util__WEBPACK_IMPORTED_MODULE_6__["isObject"])(this.solicitudService.asegurado)) {
                      oldAsegurado = this.solicitudService.asegurado;

                      if (oldAsegurado.instancia_poliza.atributo_instancia_polizas && oldAsegurado.instancia_poliza.atributo_instancia_polizas.length) {
                        oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter = oldAsegurado.instancia_poliza.atributo_instancia_polizas.filter(function (param) {
                          return param.objeto_x_atributo.par_comportamiento_interfaz_id == _this55.solicitudService.parametroVisible.id;
                        });
                      }

                      if (oldAsegurado.instancia_poliza.atributo_instancia_polizas) {
                        oldAsegurado.instancia_poliza.atributo_instancia_polizas.forEach(function (atributo_instancia_poliza) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this55, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee58() {
                            return _regeneratorRuntime().wrap(function _callee58$(_context58) {
                              while (1) {
                                switch (_context58.prev = _context58.next) {
                                  case 0:
                                    atributo_instancia_poliza.valor = '';

                                  case 1:
                                  case "end":
                                    return _context58.stop();
                                }
                              }
                            }, _callee58);
                          }));
                        });
                      }

                      if (oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter) {
                        oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter.forEach(function (atributo_instancia_poliza) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this55, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee59() {
                            return _regeneratorRuntime().wrap(function _callee59$(_context59) {
                              while (1) {
                                switch (_context59.prev = _context59.next) {
                                  case 0:
                                    atributo_instancia_poliza.valor = '';

                                  case 1:
                                  case "end":
                                    return _context59.stop();
                                }
                              }
                            }, _callee59);
                          }));
                        });
                      }
                    }

                    oldAsegurado.entidad.persona.persona_doc_id = persona_doc_id;
                    oldAsegurado.entidad.persona.persona_doc_id_ext = persona_doc_id_ext;
                    this.solicitudService.asegurado.instancia_poliza = new _src_core_modelos_instancia_poliza__WEBPACK_IMPORTED_MODULE_38__["Instancia_poliza"]();
                    this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
                    _context61.next = 8;
                    return this.solicitudService.setDatesOfAsegurado();

                  case 8:
                    this.solicitudService.persona_banco.doc_id = this.solicitudService.doc_id ? this.solicitudService.doc_id.trim() : this.solicitudService.doc_id;
                    this.solicitudService.persona_banco.extension = this.solicitudService.extension;
                    this.solicitudService.asegurado = new _src_core_modelos_asegurado__WEBPACK_IMPORTED_MODULE_31__["Asegurado"]();
                    this.BeneficiariosAux = []; //this.solicitudService.usuario_banco = this.solicitudService.usuarioLogin.usuario_banco;

                    this.solicitudService.asegurado.instancia_poliza.id_anexo_poliza = this.solicitudService.poliza.anexo_poliza.id;
                    this.solicitudService.asegurado.instancia_poliza.estado = this.solicitudService.estadoIniciado;
                    this.solicitudService.asegurado.instancia_poliza.poliza = this.solicitudService.poliza;
                    this.solicitudService.asegurado.instancia_poliza.id_estado = this.solicitudService.estadoIniciado.id;
                    this.solicitudService.asegurado.instancia_poliza.id_poliza = this.solicitudService.poliza.id;
                    this.solicitudService.instanciaDocumentoSolicitud = new _src_core_modelos_instancia_documento__WEBPACK_IMPORTED_MODULE_32__["Instancia_documento"]();
                    this.solicitudService.instanciaDocumentoSolicitud.documento = this.solicitudService.documentoSolicitud;

                    if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(function (param) {
                      return param.id_documento == _this55.solicitudService.instanciaDocumentoSolicitud.id_documento;
                    })) {
                      this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoSolicitud);
                    }

                    this.solicitudService.asegurado.entidad = oldAsegurado.entidad;
                    this.solicitudService.persona_banco_datos.ocupacion = this.solicitudService.persona_banco.caedec + ' - ' + this.solicitudService.persona_banco.desc_caedec;
                    this.solicitudService.persona_banco_datos.modalidad_pago = this.solicitudService.Periodicidad[2].value + '';
                    this.solicitudService.persona_banco_datos.plazo = 12;
                    this.solicitudService.persona_banco_datos.ciudad_nacimiento = '';
                    this.solicitudService.persona_banco_datos.lugar_nacimiento = '';
                    this.solicitudService.persona_banco_datos.provincia = '';
                    this.solicitudService.persona_banco_datos.desc_ocupacion = '';
                    this.solicitudService.asegurado.entidad.persona.par_pais_nacimiento_id = "5";
                    this.solicitudService.persona_banco_datos.tipo_doc = 'CI';
                    this.solicitudService.persona_banco_solicitud.solicitud_sci_selected = null;
                    this.solicitudService.persona_banco_solicitud.solicitud_forma_pago = null;
                    this.solicitudService.persona_banco_solicitud.solicitud_prima_total = '';
                    this.solicitudService.persona_banco_operacion.Operacion_Plazo = 0;
                    this.solicitudService.persona_banco_operacion.Operacion_Tipo_Credito = '';
                    this.solicitudService.persona_banco.debito_automatico = null;
                    this.solicitudService.persona_banco.debito_automatico = this.solicitudService.Condiciones[1].value;
                    this.solicitudService.persona_banco_datos.razon_social = this.solicitudService.persona_banco.paterno + ' ' + this.solicitudService.persona_banco.nombre;
                    this.solicitudService.persona_banco_datos.nit_carnet = this.solicitudService.persona_banco.doc_id + '';
                    this.solicitudService.atributoNroCuenta = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoNroDireccion = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoEmail = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoUltimosCuatroDigitos = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoRazonSocial = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoOcupacion = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoAmpConAmbMedGeneral = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoAmpConAmbMedEspecializada = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoAmpSumAmbMedicamentos = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoAmpSumExaLaboratorio = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoNitCarnet = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoCtaFechaExpiracion = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoPlan = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoNroTransaccion = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoTransaccionImporte = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoTransaccionDetalle = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoFechaTransaccion = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoTransaccionMoneda = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoPlazo = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoAgencia = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoPrima = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoTelefonoCelular = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoCiudadNacimiento = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoSucursal = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoUsuarioCargo = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoZona = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoDebitoAutomatico = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoFormaPago = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.aseguradoBeneficiarios = [];
                    this.solicitudService.atributoMoneda = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoNroTarjeta = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoSolicitudPrimaTotal = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoSolicitudFrecuenciaPlazo = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoSolicitudMoneda = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoSolicitudSciEstado = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoSolicitudSci = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoTipoSeguro = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.atributoSolicitudPlazoCredito = new _src_core_modelos_atributo_instancia_poliza__WEBPACK_IMPORTED_MODULE_39__["Atributo_instancia_poliza"]();
                    this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas = oldAsegurado.instancia_poliza.atributo_instancia_polizas;
                    this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter = oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter;
                    _context61.next = 81;
                    return this.componentsBehaviorOnInit();

                  case 81:
                    _context61.next = 83;
                    return this.solicitudService.setAtributosToAsegurado(function () {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this55, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee60() {
                        return _regeneratorRuntime().wrap(function _callee60$(_context60) {
                          while (1) {
                            switch (_context60.prev = _context60.next) {
                              case 0:
                                this.stateButtonsFlow(); // await this.solicitudService.componentsBehavior();

                                _context60.next = 3;
                                return this.solicitudService.setBeneficiarios();

                              case 3:
                                this.setPago(this.solicitudService.persona_banco.debito_automatico); //this.setMonto(this.solicitudService.persona_banco_datos.modalidad_pago);

                                this.setNroCuotas(this.solicitudService.persona_banco_datos.nro_cuotas);

                                if (this.transicionesComponent != undefined) {
                                  this.transicionesComponent.ngOnInit();
                                }

                              case 6:
                              case "end":
                                return _context60.stop();
                            }
                          }
                        }, _callee60, this);
                      }));
                    });

                  case 83:
                  case "end":
                    return _context61.stop();
                }
              }
            }, _callee61, this);
          }));
        }
      }, {
        key: "selectNroCuenta",
        value: function selectNroCuenta(event) {
          if (event.value) {
            var value = event.value;

            if ([10, 12].includes(parseInt(this.solicitudService.poliza.id + ''))) {
              this.solicitudService.showNroCuenta = false;
              this.solicitudService.showMoneda = false;
            } else {
              if (this.solicitudService.persona_banco_accounts.length) {
                this.solicitudService.showNroCuenta = true;
                this.solicitudService.showMoneda = true;
                this.solicitudService.persona_banco_account = this.solicitudService.persona_banco_accounts.find(function (params) {
                  return params.nrocuenta === value;
                });

                if (this.solicitudService.persona_banco_account) {
                  this.userform.controls['par_moneda'].setValue(this.solicitudService.persona_banco_account.moneda);
                  this.userform.controls['par_numero_cuenta'].setValue(this.solicitudService.persona_banco_account.nrocuenta);
                }
              } // this.solicitudService.setAtributosToAsegurado();

            }
          }
        }
      }, {
        key: "afterSearch",
        value: function afterSearch() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee65() {
            var _this56 = this;

            return _regeneratorRuntime().wrap(function _callee65$(_context65) {
              while (1) {
                switch (_context65.prev = _context65.next) {
                  case 0:
                    _context65.next = 2;
                    return this.solicitudService.afterSearchWithLimits(this.search, this.userform, function () {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this56, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee62() {
                        return _regeneratorRuntime().wrap(function _callee62$(_context62) {
                          while (1) {
                            switch (_context62.prev = _context62.next) {
                              case 0:
                                _context62.next = 2;
                                return this.newAsegurado(this.solicitudService.doc_id, this.solicitudService.extension);

                              case 2:
                              case "end":
                                return _context62.stop();
                            }
                          }
                        }, _callee62, this);
                      }));
                    }, function () {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this56, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee63() {
                        return _regeneratorRuntime().wrap(function _callee63$(_context63) {
                          while (1) {
                            switch (_context63.prev = _context63.next) {
                              case 0:
                                this.abrirModalRegistroCliente(this.solicitudService.doc_id, this.solicitudService.extension);

                              case 1:
                              case "end":
                                return _context63.stop();
                            }
                          }
                        }, _callee63, this);
                      }));
                    }, function () {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this56, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee64() {
                        return _regeneratorRuntime().wrap(function _callee64$(_context64) {
                          while (1) {
                            switch (_context64.prev = _context64.next) {
                              case 0:
                                this.solicitudService.isLoadingAgain = false;
                                _context64.next = 3;
                                return this.newAsegurado(this.solicitudService.doc_id, this.solicitudService.extension);

                              case 3:
                                this.stateButtonsFlow();

                              case 4:
                              case "end":
                                return _context64.stop();
                            }
                          }
                        }, _callee64, this);
                      }));
                    });

                  case 2:
                  case "end":
                    return _context65.stop();
                }
              }
            }, _callee65, this);
          }));
        }
      }, {
        key: "cambiarPersonaExtension",
        value: function cambiarPersonaExtension(idPerson, idInstanciaPoliza, docId, ext) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee66() {
            var _this57 = this;

            return _regeneratorRuntime().wrap(function _callee66$(_context66) {
              while (1) {
                switch (_context66.prev = _context66.next) {
                  case 0:
                    this.solicitudService.isLoadingAgain = true;
                    this.personaService.actualizarPersonaExt(idPerson, idInstanciaPoliza, docId, ext).subscribe(function (resp) {
                      var response = resp;

                      if (Object(util__WEBPACK_IMPORTED_MODULE_6__["isObject"])(response.data) && Object.keys(response.data).length) {
                        _this57.solicitudService.isLoadingAgain = false;
                        _this57.solicitudService.asegurado.entidad.persona = response.data.persona;
                        _this57.solicitudService.displayActualizacionPersona = false;

                        _this57.afterSearch();
                      }
                    });

                  case 2:
                  case "end":
                    return _context66.stop();
                }
              }
            }, _callee66, this);
          }));
        }
      }, {
        key: "selectPagoEfectivo",
        value: function selectPagoEfectivo(event) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee68() {
            var _this58 = this;

            var value;
            return _regeneratorRuntime().wrap(function _callee68$(_context68) {
              while (1) {
                switch (_context68.prev = _context68.next) {
                  case 0:
                    if (!event.value) {
                      _context68.next = 4;
                      break;
                    }

                    value = event.value;
                    _context68.next = 4;
                    return this.getDatosFromAccountService(function () {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this58, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee67() {
                        return _regeneratorRuntime().wrap(function _callee67$(_context67) {
                          while (1) {
                            switch (_context67.prev = _context67.next) {
                              case 0:
                                _context67.next = 2;
                                return this.setPago(value);

                              case 2:
                              case "end":
                                return _context67.stop();
                            }
                          }
                        }, _callee67, this);
                      }));
                    });

                  case 4:
                  case "end":
                    return _context68.stop();
                }
              }
            }, _callee68, this);
          }));
        }
      }, {
        key: "setPago",
        value: function setPago(value) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee69() {
            return _regeneratorRuntime().wrap(function _callee69$(_context69) {
              while (1) {
                switch (_context69.prev = _context69.next) {
                  case 0:
                    if (this.userform) {
                      if (this.solicitudService.pagoEfectivo.id == value) {
                        this.solicitudService.showTipoCuenta = false;
                        this.solicitudService.showNroCuenta = false;
                        this.solicitudService.showMoneda = false;
                        this.userform.controls['par_numero_cuenta'].clearValidators();
                        this.userform.controls['par_moneda'].clearValidators();
                        this.userform.controls['par_numero_cuenta'].setValue('');
                        this.userform.controls['par_moneda'].setValue('');
                      } else if ([10, 12].includes(parseInt(this.solicitudService.poliza.id + ''))) {
                        this.userform.controls['par_numero_cuenta'].clearValidators();
                        this.userform.controls['par_moneda'].clearValidators();
                        this.solicitudService.showMoneda = false;
                        this.solicitudService.showNroCuenta = false;
                        this.solicitudService.showTipoCuenta = false;
                      } else {
                        this.userform.controls['par_numero_cuenta'].setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required);
                        this.userform.controls['par_moneda'].setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required);
                        this.solicitudService.showMoneda = true;
                        this.solicitudService.showNroCuenta = true;
                        this.solicitudService.showTipoCuenta = false;
                      }
                    }

                  case 1:
                  case "end":
                    return _context69.stop();
                }
              }
            }, _callee69, this);
          }));
        }
      }, {
        key: "setNroCuotas",
        value: function setNroCuotas(event) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee70() {
            var value;
            return _regeneratorRuntime().wrap(function _callee70$(_context70) {
              while (1) {
                switch (_context70.prev = _context70.next) {
                  case 0:
                    if (event.value) {
                      value = event.value;
                    } else {
                      value = event;
                    }

                    if (parseInt(value)) {
                      if (this.solicitudService.persona_banco_datos.nro_cuotas == 1) {
                        this.solicitudService.persona_banco_datos.monto = parseInt('176') / parseInt(value + '');
                      } else {
                        this.solicitudService.persona_banco_datos.monto = parseInt('192') / parseInt(value + '');
                      }
                    } else {
                      this.solicitudService.persona_banco_datos.monto = 0;
                    }

                  case 2:
                  case "end":
                    return _context70.stop();
                }
              }
            }, _callee70, this);
          }));
        }
      }, {
        key: "selectPlan",
        value: function selectPlan(event) {
          if (event.value) {
            var value = event.value;

            if (this.solicitudService.Planes.length) {
              this.plan = this.solicitudService.planes.find(function (params) {
                return params.id === value;
              });

              if (this.plan) {
                this.solicitudService.persona_banco_datos.plan = this.plan.id;
                this.userform.controls['par_prima'].setValue(this.plan.monto_prima);
              }
            }
          }
        }
      }, {
        key: "abrirModalRegistroCliente",
        value: function abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee71() {
            var _this59 = this;

            return _regeneratorRuntime().wrap(function _callee71$(_context71) {
              while (1) {
                switch (_context71.prev = _context71.next) {
                  case 0:
                    // await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                    this.stateButtonsFlow();
                    this.solicitudService.displayModalFormTitular = true;
                    this.solicitudService.displayBusquedaCI = false;
                    this.collapsedFormTitular = false;
                    this.solicitudService.setFromObjetoAtributoToAtributoInstanciaPoliza(function () {
                      _this59.solicitudService.setFeatureValidacionAlInicioPersonaBanco();
                    });
                    _context71.next = 7;
                    return this.solicitudService.validarWarnsOrErrors(function () {
                      if (_this59.solicitudService.userFormHasErrors && _this59.solicitudService.userFormHasWarnings) {
                        _this59.solicitudService.msgText = 'La información proporcionada por el sistema del banco tiene las siguientes advertencias y observaciones que deben ser resueltas,';
                      } else if (_this59.solicitudService.userFormHasWarnings) {
                        _this59.solicitudService.msgText = 'La información proporcionada por el sistema del banco tiene las siguientes advertencias,';
                      }
                    });

                  case 7:
                    this.enableUserform();

                  case 8:
                  case "end":
                    return _context71.stop();
                }
              }
            }, _callee71, this);
          }));
        }
      }, {
        key: "iniciarSolicitud",
        value: function iniciarSolicitud() {
          this.solicitudService.msgs_warn = [];
          this.solicitudService.msgs_error = [];
        }
      }, {
        key: "cancelarSolicitud",
        value: function cancelarSolicitud() {
          this.iniciarSolicitud();
          this.solicitudService.displayModalSolicitudExistente = false;
          this.solicitudService.asegurado = new _src_core_modelos_asegurado__WEBPACK_IMPORTED_MODULE_31__["Asegurado"]();
          this.solicitudService.persona_banco = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco"]();
          this.solicitudService.persona_banco_datos = new _src_core_modelos_persona_banco_datos__WEBPACK_IMPORTED_MODULE_37__["persona_banco_datos"]();
          this.solicitudService.persona_banco_account = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();
          this.solicitudService.persona_banco_accounts = [];
          this.solicitudService.persona_banco_pep = new _src_core_modelos_persona_banco_pep__WEBPACK_IMPORTED_MODULE_36__["persona_banco_pep"]();
        }
      }, {
        key: "crearEcoResguardo",
        value: function crearEcoResguardo() {
          this.solicitudService.parametrosRuteo = this.sessionStorageService.getItemSync('parametros');
          this.solicitudService.id_poliza = 12;
          this.solicitudService.parametrosRuteo.id_vista = 76;
          this.solicitudService.parametrosRuteo.parametro_vista = this.solicitudService.id_poliza + '';
          this.solicitudService.parametrosRuteo.openForm = true;
          this.sessionStorageService.setItemSync('parametros', this.solicitudService.parametrosRuteo);
          var docId = this.solicitudService.doc_id ? this.solicitudService.doc_id.trim() : this.solicitudService.doc_id;
          var docIdExt = parseInt(this.solicitudService.extension + '');
          this.solicitudService.displayModalSolicitudExistente = false;
          this.solicitudService.isLoadingAgain = true;
          this.onInit(this.solicitudService.id_poliza, docId, docIdExt, false, false);
        }
      }, {
        key: "reiniciarEcoAccidentes",
        value: function reiniciarEcoAccidentes() {
          this.solicitudService.parametrosRuteo = this.sessionStorageService.getItemSync('parametros');
          this.solicitudService.id_poliza = 10;
          this.solicitudService.parametrosRuteo.id = 67;
          this.solicitudService.parametrosRuteo.parametro_vista = this.solicitudService.id_poliza + '';
          this.solicitudService.parametrosRuteo.openForm = true;
          this.sessionStorageService.setItemSync('parametros', this.solicitudService.parametrosRuteo);
          var docId = this.solicitudService.doc_id ? this.solicitudService.doc_id.trim() : this.solicitudService.doc_id;
          var docIdExt = parseInt(this.solicitudService.extension + '');
          this.solicitudService.displayModalSolicitudExistente = false;
          this.onInit(this.solicitudService.id_poliza, docId, docIdExt, false, false);
        }
      }, {
        key: "abrirVentanaTitular",
        value: function abrirVentanaTitular() {
          this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = "";
          this.solicitudService.asegurado.entidad.persona.persona_segundo_nombre = "";
          this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = "";
          this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = "";
          this.solicitudService.asegurado.entidad.persona.persona_apellido_casada = "";
          this.stateButtonsFlow();
          this.solicitudService.displayModalFormTitular = true;
          this.displayFormTitular = true;
        }
      }, {
        key: "abrirVentanaRegistroNuevoTitular",
        value: function abrirVentanaRegistroNuevoTitular() {
          this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = "";
          this.solicitudService.asegurado.entidad.persona.persona_segundo_nombre = "";
          this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = "";
          this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = "";
          this.solicitudService.asegurado.entidad.persona.persona_apellido_casada = "";
          this.displayFormTitular = true;
          this.displayDatosTitular = true;
          this.solicitudService.isLoadingAgain = false;
          this.stateButtonsFlow();
          this.solicitudService.displayModalFormTitular = true;
        }
      }, {
        key: "onRowSelectDato_complementario",
        value: function onRowSelectDato_complementario(even) {}
      }, {
        key: "onApellidoInput",
        value: function onApellidoInput() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee72() {
            return _regeneratorRuntime().wrap(function _callee72$(_context72) {
              while (1) {
                switch (_context72.prev = _context72.next) {
                  case 0:
                    this.userform.controls['persona_primer_apellido'].clearAsyncValidators();
                    this.userform.controls['persona_primer_apellido'].setErrors(null);
                    this.userform.controls['persona_segundo_apellido'].clearAsyncValidators();
                    this.userform.controls['persona_segundo_apellido'].setErrors(null);

                  case 4:
                  case "end":
                    return _context72.stop();
                }
              }
            }, _callee72, this);
          }));
        }
      }, {
        key: "validarApellidos",
        value: function validarApellidos() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee73() {
            return _regeneratorRuntime().wrap(function _callee73$(_context73) {
              while (1) {
                switch (_context73.prev = _context73.next) {
                  case 0:
                    if (this.solicitudService.persona_banco.paterno == '' && this.solicitudService.persona_banco.materno == '' && this.solicitudService.persona_banco.apcasada == '') {
                      this.userform.controls['persona_primer_apellido'].setErrors([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[a-zA-Z\xC0-\xFF\xF1\xD1]+(\\s*[a-zA-Z\xC0-\xFF\xF1\xD1]*)*[a-zA-Z\xC0-\xFF\xF1\xD1]+$")]);
                      this.userform.controls['persona_segundo_apellido'].setErrors([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[a-zA-Z\xC0-\xFF\xF1\xD1]+(\\s*[a-zA-Z\xC0-\xFF\xF1\xD1]*)*[a-zA-Z\xC0-\xFF\xF1\xD1]+$")]);
                      this.solicitudService.atributoApellidoPaterno.requerido = true;
                      this.solicitudService.isLoadingAgain = false;
                      this.stopSavingByLastName = true;
                    } else {
                      //this.onApellidoInput();
                      this.stopSavingByLastName = false;
                    }

                    if (!(typeof callback == 'function')) {
                      _context73.next = 4;
                      break;
                    }

                    _context73.next = 4;
                    return callback();

                  case 4:
                  case "end":
                    return _context73.stop();
                }
              }
            }, _callee73, this);
          }));
        }
      }, {
        key: "EnviarDocumentosADestinatariosEcoProteccion",
        value: function EnviarDocumentosADestinatariosEcoProteccion() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee74() {
            return _regeneratorRuntime().wrap(function _callee74$(_context74) {
              while (1) {
                switch (_context74.prev = _context74.next) {
                  case 0:
                    _context74.next = 2;
                    return this.validarCorreos();

                  case 2:
                    if (!this.stopSending) {
                      this.solicitudService.envioDestinatariosExitoso = true;
                    }

                  case 3:
                  case "end":
                    return _context74.stop();
                }
              }
            }, _callee74, this);
          }));
        }
      }, {
        key: "validarCorreos",
        value: function validarCorreos() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee75() {
            var _this60 = this;

            var correos, correosValidos;
            return _regeneratorRuntime().wrap(function _callee75$(_context75) {
              while (1) {
                switch (_context75.prev = _context75.next) {
                  case 0:
                    if (this.solicitudService.destinatariosCorreos != '') {
                      correos = this.solicitudService.destinatariosCorreos.split(',');
                      correosValidos = true;
                      correos.forEach(function (correo) {
                        if (!_this60.solicitudService.validarEmail(correo)) {
                          correosValidos = false;
                        }
                      });

                      if (!correosValidos) {
                        this.envioDocumentosForm.controls['destinatarios_correos'].setErrors([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]);
                        this.solicitudService.atributoDestinatarios.requerido = true;
                        this.solicitudService.atributoFechaNacimiento.tipo_error = 'Uno de los correos no es valido, por favor revisalos';
                        this.solicitudService.isLoadingAgain = false;
                        this.stopSending = true;
                      } else {
                        this.stopSending = false;
                      }
                    } else {
                      this.envioDocumentosForm.controls['destinatarios_correos'].clearAsyncValidators();
                      this.envioDocumentosForm.controls['destinatarios_correos'].setErrors(null);
                      this.stopSending = false;
                    }

                  case 1:
                  case "end":
                    return _context75.stop();
                }
              }
            }, _callee75, this);
          }));
        }
      }, {
        key: "afterAlertEdad",
        value: function afterAlertEdad() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee76() {
            return _regeneratorRuntime().wrap(function _callee76$(_context76) {
              while (1) {
                switch (_context76.prev = _context76.next) {
                  case 0:
                    this.userform.controls['persona_fecha_nacimiento'].setErrors(null);
                    this.stopSavingByEdad = false;

                  case 2:
                  case "end":
                    return _context76.stop();
                }
              }
            }, _callee76, this);
          }));
        }
      }, {
        key: "validarFechaNacimiento",
        value: function validarFechaNacimiento() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee77() {
            var fechaMin, fechaMax, edad, edadMinimaMiliseconds, edadMaximaMiliseconds;
            return _regeneratorRuntime().wrap(function _callee77$(_context77) {
              while (1) {
                switch (_context77.prev = _context77.next) {
                  case 0:
                    fechaMin = this.solicitudService.getFechaMinimaSegunEdad();
                    fechaMax = this.solicitudService.getFechaMaximaSegunEdad();

                    if (!this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento) {
                      _context77.next = 28;
                      break;
                    }

                    edad = new Date().getTime() - this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();

                    if (!(fechaMin && fechaMax)) {
                      _context77.next = 28;
                      break;
                    }

                    //let minTime = fechaMin.getTime();
                    // 567648000000 = 18 años
                    edadMinimaMiliseconds = this.solicitudService.edadMinimaYears * 31539999999.9988899;
                    edadMaximaMiliseconds = this.solicitudService.edadMaximaYears * 31539999999.9988899;

                    if (!(edad < edadMinimaMiliseconds)) {
                      _context77.next = 16;
                      break;
                    }

                    this.solicitudService.atributoFechaNacimiento.tipo_error = 'La persona no es mayor de 18 años';
                    this.solicitudService.isLoadingAgain = false;
                    this.userform.controls['persona_fecha_nacimiento'].setErrors([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(fechaMin.getTime())]);
                    this.stopSavingByEdad = true;

                    if (!this.validandoContinuando) {
                      this.displayEdadIncorrecta = true;
                    }

                    return _context77.abrupt("return", '');

                  case 16:
                    if (!(edad > edadMaximaMiliseconds)) {
                      _context77.next = 25;
                      break;
                    }

                    this.solicitudService.atributoFechaNacimiento.tipo_error = 'La persona es mayor de 65 años';
                    this.solicitudService.isLoadingAgain = false;
                    this.userform.controls['persona_fecha_nacimiento'].setErrors([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].max(fechaMax.getTime())]);
                    this.stopSavingByEdad = true;

                    if (!this.validandoContinuando) {
                      this.displayEdadIncorrecta = true;
                    }

                    return _context77.abrupt("return", '');

                  case 25:
                    this.userform.controls['persona_fecha_nacimiento'].clearAsyncValidators();
                    this.userform.controls['persona_fecha_nacimiento'].setErrors(null);
                    this.stopSavingByEdad = false;

                  case 28:
                    if (!(typeof callback == 'function')) {
                      _context77.next = 31;
                      break;
                    }

                    _context77.next = 31;
                    return callback();

                  case 31:
                  case "end":
                    return _context77.stop();
                }
              }
            }, _callee77, this);
          }));
        }
      }, {
        key: "validarFechaMinima",
        value: function validarFechaMinima() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee78() {
            var fechaMin, diff, yearsMiliseconds;
            return _regeneratorRuntime().wrap(function _callee78$(_context78) {
              while (1) {
                switch (_context78.prev = _context78.next) {
                  case 0:
                    fechaMin = this.solicitudService.getFechaMinimaSegunEdad();
                    diff = new Date().getTime() - this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
                    yearsMiliseconds = 5.676e+11; // 18 años;

                    if (diff < fechaMin.getTime()) {
                      this.userform.controls['persona_fecha_nacimiento'].setErrors([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(fechaMin.getTime())]);
                      this.solicitudService.atributoFechaNacimiento.tipo_error = 'La persona no es mayor de 18 años';
                      this.solicitudService.atributoApellidoPaterno.requerido = true;
                      this.solicitudService.isLoadingAgain = false;
                      this.stopSaving = true;
                    } else {
                      this.userform.controls['persona_fecha_nacimiento'].clearAsyncValidators();
                      this.userform.controls['persona_fecha_nacimiento'].setErrors(null);
                      this.stopSaving = false;
                    }

                  case 4:
                  case "end":
                    return _context78.stop();
                }
              }
            }, _callee78, this);
          }));
        }
      }, {
        key: "validarFechaMaxima",
        value: function validarFechaMaxima() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee79() {
            var fechaMax, diff, yearsMiliseconds;
            return _regeneratorRuntime().wrap(function _callee79$(_context79) {
              while (1) {
                switch (_context79.prev = _context79.next) {
                  case 0:
                    fechaMax = this.solicitudService.getFechaMaximaSegunEdad();
                    diff = new Date().getTime() - this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
                    yearsMiliseconds = 2.05e+12; // 65 años;

                    if (diff > fechaMax.getTime()) {
                      this.userform.controls['persona_fecha_nacimiento'].setErrors([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].max(fechaMax.getTime())]);
                      this.solicitudService.atributoFechaNacimiento.tipo_error = 'La persona es mayor de 65 años';
                      this.solicitudService.atributoApellidoPaterno.requerido = true;
                      this.solicitudService.isLoadingAgain = false;
                      this.stopSaving = true;
                    } else {
                      this.userform.controls['persona_fecha_nacimiento'].clearAsyncValidators();
                      this.userform.controls['persona_fecha_nacimiento'].setErrors(null);
                      this.stopSaving = false;
                    }

                  case 4:
                  case "end":
                    return _context79.stop();
                }
              }
            }, _callee79, this);
          }));
        }
      }, {
        key: "guardarSolicitudValidando",
        value: function guardarSolicitudValidando() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee81() {
            var _this61 = this;

            return _regeneratorRuntime().wrap(function _callee81$(_context81) {
              while (1) {
                switch (_context81.prev = _context81.next) {
                  case 0:
                    _context81.next = 2;
                    return this.solicitudService.setAtributosToAsegurado(function () {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this61, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee80() {
                        var _this62 = this;

                        return _regeneratorRuntime().wrap(function _callee80$(_context80) {
                          while (1) {
                            switch (_context80.prev = _context80.next) {
                              case 0:
                                this.stateButtonsFlow();
                                _context80.next = 3;
                                return this.validarApellidos(function () {
                                  _this62.validarFechaNacimiento(function () {
                                    _this62.guardarSolicitudPersona(callback);
                                  });
                                });

                              case 3:
                              case "end":
                                return _context80.stop();
                            }
                          }
                        }, _callee80, this);
                      }));
                    });

                  case 2:
                  case "end":
                    return _context81.stop();
                }
              }
            }, _callee81, this);
          }));
        }
      }, {
        key: "guardarSolicitudPersona",
        value: function guardarSolicitudPersona() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee84() {
            var _this63 = this;

            return _regeneratorRuntime().wrap(function _callee84$(_context84) {
              while (1) {
                switch (_context84.prev = _context84.next) {
                  case 0:
                    if (!this.stopSaving && !this.stopSavingByEdad && !this.stopSavingByLastName && !this.stopSavingByMontoCuota) {
                      this.solicitudService.isLoadingAgain = true;
                      this.stateButtonsFlow();

                      if (this.solicitudService.editandoTitular) {
                        this.solicitudService.editandoTitular = false;
                        this.personaService.actualizarSolicitud(this.solicitudService.asegurado).subscribe(function (res) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this63, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee82() {
                            var _this64 = this;

                            var response;
                            return _regeneratorRuntime().wrap(function _callee82$(_context82) {
                              while (1) {
                                switch (_context82.prev = _context82.next) {
                                  case 0:
                                    response = res;
                                    this.solicitudService.buscando = false;
                                    this.solicitudService.asegurado = response.data; // this.setFeaturesBeneficiarios(this.solicitudService.asegurado.beneficiarios);

                                    this.solicitudService.persona_banco_account = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();
                                    this.solicitudService.persona_banco = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco"]();
                                    this.solicitudService.persona_banco_datos = new _src_core_modelos_persona_banco_datos__WEBPACK_IMPORTED_MODULE_37__["persona_banco_datos"]();
                                    _context82.next = 8;
                                    return this.stateButtonsFlow();

                                  case 8:
                                    _context82.next = 10;
                                    return this.solicitudService.setDatesOfAsegurado();

                                  case 10:
                                    _context82.next = 12;
                                    return this.solicitudService.setAtributosToPersonaBanco(this.userform, function () {
                                      _this64.solicitudService.persona_banco.extension = _this64.solicitudService.asegurado.entidad.persona.persona_doc_id_ext + '';
                                    });

                                  case 12:
                                    _context82.next = 14;
                                    return this.solicitudService.setBeneficiarios();

                                  case 14:
                                    this.componenteBeneficiario.ngOnInit();
                                    this.solicitudService.isLoadingAgain = false;

                                    if (!(typeof callback == 'function')) {
                                      _context82.next = 19;
                                      break;
                                    }

                                    _context82.next = 19;
                                    return callback();

                                  case 19:
                                  case "end":
                                    return _context82.stop();
                                }
                              }
                            }, _callee82, this);
                          }));
                        }, function (err) {
                          if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                            _this63.router.navigate(['login']);
                          } else {
                            console.log(err);
                          }
                        });
                      } else {
                        this.personaService.crearNuevaSolicitud(this.solicitudService.asegurado).subscribe(function (res) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this63, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee83() {
                            var _this65 = this;

                            var response;
                            return _regeneratorRuntime().wrap(function _callee83$(_context83) {
                              while (1) {
                                switch (_context83.prev = _context83.next) {
                                  case 0:
                                    response = res;
                                    this.solicitudService.buscando = false;
                                    this.solicitudService.asegurado = response.data; // this.setFeaturesBeneficiarios(this.solicitudService.asegurado.beneficiarios);

                                    this.solicitudService.persona_banco_account = new _src_core_modelos_persona_banco_account__WEBPACK_IMPORTED_MODULE_35__["persona_banco_account"]();
                                    this.solicitudService.persona_banco = new _src_core_modelos_persona_banco__WEBPACK_IMPORTED_MODULE_10__["persona_banco"]();
                                    this.solicitudService.persona_banco_datos = new _src_core_modelos_persona_banco_datos__WEBPACK_IMPORTED_MODULE_37__["persona_banco_datos"]();
                                    _context83.next = 8;
                                    return this.stateButtonsFlow();

                                  case 8:
                                    _context83.next = 10;
                                    return this.solicitudService.setDatesOfAsegurado();

                                  case 10:
                                    _context83.next = 12;
                                    return this.solicitudService.setAtributosToPersonaBanco(this.userform, function () {
                                      _this65.solicitudService.persona_banco.extension = _this65.solicitudService.asegurado.entidad.persona.persona_doc_id_ext + '';
                                    });

                                  case 12:
                                    _context83.next = 14;
                                    return this.solicitudService.setBeneficiarios();

                                  case 14:
                                    // await this.setSolicitudAsIniciado();
                                    this.componenteBeneficiario.ngOnInit();
                                    this.solicitudService.isLoadingAgain = false;

                                    if (!(typeof callback == 'function')) {
                                      _context83.next = 19;
                                      break;
                                    }

                                    _context83.next = 19;
                                    return callback();

                                  case 19:
                                  case "end":
                                    return _context83.stop();
                                }
                              }
                            }, _callee83, this);
                          }));
                        }, function (err) {
                          if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                            _this63.router.navigate(['login']);
                          } else {
                            console.log(err);
                          }
                        });
                      }

                      this.displayDatosTitular = true;
                      this.collapsedDatosTitular = false;
                      this.solicitudService.displayModalDatosTitular = true;
                      this.solicitudService.displayModalFormTitular = false;
                    }

                  case 1:
                  case "end":
                    return _context84.stop();
                }
              }
            }, _callee84, this);
          }));
        }
      }, {
        key: "onAcceptValidationInit",
        value: function onAcceptValidationInit() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee85() {
            return _regeneratorRuntime().wrap(function _callee85$(_context85) {
              while (1) {
                switch (_context85.prev = _context85.next) {
                  case 0:
                    this.solicitudService.displayValidacionAlInicio = false;

                    if (this.userform.controls['par_debito_automatico_id'].value == null || this.userform.controls['par_debito_automatico_id'].value == '') {
                      this.solicitudService.showNroCuenta = false;
                      this.solicitudService.showMoneda = false;
                      this.solicitudService.showTipoCuenta = false;
                    }

                    if (this.solicitudService.persona_banco_accounts.length == 0) {
                      //this.cancelarSolicitud();
                      this.stateButtonsFlow();
                      this.solicitudService.displayModalFormTitular = true; //this.solicitudService.displayBusquedaCI = true;
                    }

                  case 3:
                  case "end":
                    return _context85.stop();
                }
              }
            }, _callee85, this);
          }));
        }
      }, {
        key: "editarTitular",
        value: function editarTitular() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee86() {
            var _this66 = this;

            return _regeneratorRuntime().wrap(function _callee86$(_context86) {
              while (1) {
                switch (_context86.prev = _context86.next) {
                  case 0:
                    if ((this.solicitudService.asegurado.instancia_poliza.id_estado != this.solicitudService.estadoIniciado.id || this.solicitudService.hasRolCajero) && this.solicitudService.asegurado.instancia_poliza.id_estado != null) {
                      this.solicitudService.disableForm(this.userform);
                      this.solicitudService.disableToSave = true;
                      this.stateButtonsFlow();
                    }

                    _context86.next = 3;
                    return this.solicitudService.setAtributosToPersonaBanco(this.userform, function () {
                      _this66.solicitudService.persona_banco.extension = _this66.solicitudService.asegurado.entidad.persona.persona_doc_id_ext + '';
                    });

                  case 3:
                    this.stateButtonsFlow();
                    this.solicitudService.setDatesOfAsegurado();
                    this.solicitudService.editandoTitular = true;
                    this.solicitudService.displayModalFormTitular = true;
                    this.solicitudService.displayModalDatosTitular = false;
                    this.collapsedFormTitular = false;

                  case 9:
                  case "end":
                    return _context86.stop();
                }
              }
            }, _callee86, this);
          }));
        }
      }]);

      return AltaEcoAccidentesComponent;
    }();

    AltaEcoAccidentesComponent.ɵfac = function AltaEcoAccidentesComponent_Factory(t) {
      return new (t || AltaEcoAccidentesComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_11__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_parametro_service__WEBPACK_IMPORTED_MODULE_12__["ParametrosService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_anexo_service__WEBPACK_IMPORTED_MODULE_13__["AnexoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_persona_service__WEBPACK_IMPORTED_MODULE_14__["PersonaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_beneficiario_service__WEBPACK_IMPORTED_MODULE_15__["BeneficiarioService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_soapui_service__WEBPACK_IMPORTED_MODULE_16__["SoapuiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_atributo_service__WEBPACK_IMPORTED_MODULE_17__["AtributoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_objetoAtributo_service__WEBPACK_IMPORTED_MODULE_18__["ObjetoAtributoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_documento_service__WEBPACK_IMPORTED_MODULE_19__["DocumentoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_poliza_service__WEBPACK_IMPORTED_MODULE_20__["PolizaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_instancia_poliza_service__WEBPACK_IMPORTED_MODULE_21__["InstanciaPolizaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_reporte_service__WEBPACK_IMPORTED_MODULE_22__["ReporteService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_rol_service__WEBPACK_IMPORTED_MODULE_23__["RolesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_usuarios_service__WEBPACK_IMPORTED_MODULE_24__["UsuariosService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_contexto_service__WEBPACK_IMPORTED_MODULE_25__["ContextoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_instancia_poliza_trans_service__WEBPACK_IMPORTED_MODULE_26__["InstanciaPolizaTransService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_27__["SolicitudService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_plan_pago_service__WEBPACK_IMPORTED_MODULE_28__["PlanPagoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_archivo_service__WEBPACK_IMPORTED_MODULE_42__["ArchivoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_sessionStorage_service__WEBPACK_IMPORTED_MODULE_29__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_administracion_de_permisos_service__WEBPACK_IMPORTED_MODULE_30__["AdministracionDePermisosService"]));
    };

    AltaEcoAccidentesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: AltaEcoAccidentesComponent,
      selectors: [["app-alta-eco-accidentes"]],
      viewQuery: function AltaEcoAccidentesComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, true);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c1, true);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c2, true);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c3, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.transicionesComponent = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.componenteBeneficiario = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.componenteArchivo = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.componenteActualizarSolicitud = _t.first);
        }
      },
      decls: 247,
      vars: 140,
      consts: [["class", "overlay", 4, "ngIf"], [3, "parametros_atributos", 4, "ngIf"], [3, "setAsegurado", 4, "ngIf"], ["toggleable", "true", "collapseIcon", "ui-icon-close", 3, "header", "collapsed", "onBeforeToggle", "collapsedChange", 4, "ngIf"], ["class", "dialog-emitir-solicitud", "header", "Emitir Solicitud", 3, "visible", "modal", "minY", "visibleChange", 4, "ngIf"], ["header", "Se Actualizo Exitosamente", 3, "visible", "modal", "responsive", "minY", "visibleChange"], [1, "ui-g-12", "text-center"], [1, "ui-icon-check", 2, "border", "1px solid #4CAF50", "border-radius", "20px", "font-size", "50px", "color", "#4CAF50"], [1, "ui-dialog-buttonpane", "ui-helper-clearfix"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "Aceptar", 3, "click"], ["header", "No puede instrumentar Ecoresguardo", 3, "visible", "modal", "responsive", "minY", "visibleChange"], [1, "ui-icon-warn", 2, "border", "1px solid #4CAF50", "border-radius", "20px", "font-size", "50px", "color", "#4CAF50"], ["header", "El cliente no existe", 3, "visible", "modal", "minY", "visibleChange"], [1, "ui-icon-priority-high", "ui-icon-warn-colibri"], [1, "text-center"], ["header", "Se Actualiz\xF3 Exit\xF3samente", 3, "visible", "modal", "responsive", "minY", "visibleChange"], [3, "innerHTML", 4, "ngIf"], ["type", "button", "pButton", "", "icon", "pi pi-times", "label", "Cancelar", 3, "click"], ["type", "button", "pButton", "", "icon", "pi pi-checks", "label", "Aceptar", 3, "click"], [3, "innerHTML"], [4, "ngIf"], ["type", "button", "pButton", "", "icon", "pi pi-check", 3, "label", "click"], ["type", "button", "pButton", "", "icon", "pi pi-times", "label", "No", 3, "click", 4, "ngIf"], ["header", "Solicitud Existente", 3, "visible", "modal", "closable", "minY", "visibleChange"], [1, "ui-icon-priority-high", 2, "border", "1px solid #f1e017", "border-radius", "20px", "font-size", "50px", "color", "#e6e102"], [1, "ui-g-12"], [1, "ui-g", "ui-dialog-buttonpane", "ui-helper-clearfix"], [1, "ui-g-12", "ui-md-3"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "VER Solicitudes Anteriores", 1, "full-width", 3, "click"], ["class", "ui-g-12 ui-md-3", 4, "ngIf"], [1, "ui-g-12", "ui-md-2"], ["type", "button", "pButton", "", "icon", "pi pi-times", "label", "Cancelar", 1, "full-width", 3, "click"], ["header", "Validaci\xF3n de la solicitud", 1, "ui-messages-eco", 3, "visible", "modal", "minY", "visibleChange"], [1, "ui-icon-error-outline", "ui-icon-error-colibri"], [1, "ui-g-offset-2", "ui-g-9", "ui-g-offset-1", "msg_error"], ["severity", "error", "text", "Fields are required", 3, "closable", "value"], ["severity", "warn", "text", "Are you sure?", 3, "closable", "value", 4, "ngIf"], [1, "ui-icon-check", "ui-icon-check-colibri"], ["class", "ui-g-offset-2 ui-g-9 ui-g-offset-1 msg_warn", 4, "ngIf"], ["header", "Actualizar Datos Persona", "style", "background: #ffffff !important;", 3, "visible", "modal", "minY", "position", "visibleChange", 4, "ngIf"], ["severity", "warn", "text", "Are you sure?", 3, "closable", "value"], ["header", "Error de validacion", 3, "visible", "modal", "minY", "visibleChange"], [1, "ui-icon-priority-high", "ui-icon-error-colibri"], ["type", "button", "pButton", "", "icon", "pi pi-times", "label", "Aceptar", 3, "click"], ["header", "Edad incorrecta", 3, "visible", "modal", "minY", "visibleChange"], ["header", "No Existe", 3, "visible", "modal", "minY", "visibleChange"], ["header", "Sin Credito Asociado", 3, "visible", "modal", "minY", "visibleChange"], ["header", "Tarjeta invalida", 3, "visible", "modal", "minY", "visibleChange"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "Verificar", 3, "click"], ["header", "\xBFEsta seguro?", 3, "visible", "modal", "minY", "visibleChange"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "Si", 3, "click"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "No", 3, "click"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "SI", 3, "click"], ["type", "button", "pButton", "", "icon", "pi pi-times", "label", "NO", 3, "click"], ["header", "Nro de Tarjeta Vacia", 3, "visible", "modal", "minY", "visibleChange"], ["header", "Solicitud de credito invalida", 3, "visible", "modal", "minY", "visibleChange"], [1, "text-center", 3, "innerHTML"], ["header", "Informaci\xF3n actualizada", 3, "visible", "modal", "minY", "visibleChange"], ["header", "Tarjeta de D\xE9bito sin Cuentas", 3, "visible", "modal", "minY", "visibleChange"], ["header", "Error Interno", 3, "visible", "modal", "minY", "visibleChange"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "Nuevo Cliente", 3, "click"], ["class", "card form-group", 4, "ngIf"], ["key", "tst"], [1, "overlay"], [1, "loading"], [3, "parametros_atributos"], ["componentTransiciones", ""], [3, "setAsegurado"], ["componenteActualizarSolicitud", ""], ["toggleable", "true", "collapseIcon", "ui-icon-close", 3, "header", "collapsed", "onBeforeToggle", "collapsedChange"], [1, "ui-fluid"], [1, "ui-g"], [1, "ui-g-12", "ui-md-12"], [1, "ui-g", "ui-fileupload", "ui-widget"], [1, "ui-g-12", "ui-fileupload-buttonbar", "ui-abrirModalRegistroClientewidget-header", 2, "text-align", "right"], ["type", "button", "icon", "ui-icon-print", "title", "Imprimir Solicitud de Seguro", "label", "Imprimir Solicitud de Seguro", 1, "green-btn", 3, "disabled", "click"], ["type", "button", "icon", "ui-icon-attach-money", "title", "Orden de Cobro", "class", "blue-grey-btn", "label", "Imprimir Orden de Cobro", 3, "disabled", "click", 4, "ngIf"], ["type", "button", "icon", "ui-icon-attach-money", "title", "Imprimir Carta Autorizacion", "class", "blue-grey-btn", "label", "Imprimir Carta Autorizacion", 3, "disabled", "click", 4, "ngIf"], ["type", "button", "icon", "ui-icon-attach-money", "title", "Imprimir Carta Desistimiento", "class", "blue-grey-btn", "label", "Imprimir Carta Desistimiento", 3, "disabled", "click", 4, "ngIf"], ["type", "button", "icon", "ui-icon-import-contacts", "title", ">Imprimir Certificado de Cobertura", "label", "Imprimir Certificado de Cobertura", 1, "blue-grey-btn", 3, "disabled", "click"], ["type", "button", "icon", "ui-icon-check", "class", "blue-grey-btn", "label", "Refrescar Informaci\xF3n>", 3, "title", "disabled", "click", 4, "ngIf"], ["type", "button", "icon", "ui-icon-check", "label", "Continuar... >", 1, "blue-grey-btn", "ui-toolbar-group-right", 3, "title", "disabled", "click"], [1, "ui-fileupload-content", "ui-widget-content", "ui-corner-bottom"], ["legend", "Datos de la solicitud", "toggleable", "true", 1, "form-group"], [3, "classList"], [1, "ui-g-12", "ui-md-1", 2, "text-align", "center"], ["src", "/assets/form-icon.jpg", "alt", "form-icon"], [1, "ui-g-12", "ui-md-10", "car-details"], [1, "ui-g-3", "ui-sm-3"], [2, "white-space", "nowrap"], ["style", "white-space: nowrap", 4, "ngIf"], [1, "task-status", 2, "white-space", "nowrap"], [1, "ui-g-12", "ui-md-1", "search-icon"], ["legend", "Datos del Titular", "toggleable", "true", 1, "form-group"], ["src", "assets/layout/images/avatar5.png", "width", "30px"], ["pButton", "", "type", "button", 1, "ui-button-secondary", 2, "width", "35px", "height", "35px", 3, "icon", "click"], [3, "parametros_beneficiario", "setBeneficiarios"], ["componenteBeneficiario", ""], ["type", "button", "icon", "ui-icon-attach-money", "title", "Orden de Cobro", "label", "Imprimir Orden de Cobro", 1, "blue-grey-btn", 3, "disabled", "click"], ["type", "button", "icon", "ui-icon-attach-money", "title", "Imprimir Carta Autorizacion", "label", "Imprimir Carta Autorizacion", 1, "blue-grey-btn", 3, "disabled", "click"], ["type", "button", "icon", "ui-icon-attach-money", "title", "Imprimir Carta Desistimiento", "label", "Imprimir Carta Desistimiento", 1, "blue-grey-btn", 3, "disabled", "click"], ["type", "button", "icon", "ui-icon-check", "label", "Refrescar Informaci\xF3n>", 1, "blue-grey-btn", 3, "title", "disabled", "click"], ["componenteArchivo", ""], ["header", "Emitir Solicitud", 1, "dialog-emitir-solicitud", 3, "visible", "modal", "minY", "visibleChange"], [3, "formGroup", "ngSubmit"], [1, "ui-g-12", "ui-md-offset-3", "ui-md-6", "ui-md-offset-3"], [1, "md-inputfield", 2, "height", "260px"], ["dateFormat", "dd/mm/yy", "pInputText", "", "formControlName", "fecha_registro", 3, "ngModel", "minDate", "disabled", "ngModelChange"], ["severity", "error", 3, "text", 4, "ngIf"], ["type", "submit", "pButton", "", "icon", "fa fa-check", "label", "Guardar", 2, "bottom", "115px"], ["severity", "error", 3, "text"], ["type", "button", "pButton", "", "icon", "pi pi-times", "label", "No", 3, "click"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "Crear EcoResguardo", 1, "full-width", 3, "click"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "Crear EcoAccidente", 1, "full-width", 3, "click"], [1, "ui-g-offset-2", "ui-g-9", "ui-g-offset-1", "msg_warn"], ["header", "Actualizar Datos Persona", 2, "background", "#ffffff !important", 3, "visible", "modal", "minY", "position", "visibleChange"], [1, "ui-icon-priority-high", 2, "border", "1px solid #4CAF50", "border-radius", "20px", "font-size", "50px", "color", "#4CAF50"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "NO", 3, "click"], [1, "form-group"], ["header", "Datos Personales", "leftIcon", "ui-icon-person"], ["id", "formSolicitud", 1, "eco-form-titular", 3, "formGroup", "ngSubmit"], ["id", "EA_SecRegistroTitular", "legend", "Registro del Titular", "toggleable", "true", 4, "ngIf"], ["id", "EA_SecFormaPago", "legend", "Datos para el seguro", "toggleable", "true", 4, "ngIf"], [1, "ui-g-12", "ui-md-6"], ["type", "submit", "pButton", "", "icon", "fa fa-check", "label", "Guardar", 3, "disabled"], ["header", "Datos Complementarios", "leftIcon", "ui-icon-description", 4, "ngIf"], ["id", "EA_SecRegistroTitular", "legend", "Registro del Titular", "toggleable", "true"], [1, "ui-g", "form-group"], ["class", "md-inputfield", 4, "ngIf"], ["severity", "warn", 3, "text", 4, "ngIf"], ["type", "text", "pInputText", "", "id", "persona_primer_apellido", "formControlName", "persona_primer_apellido", 3, "ngModel", "readonly", "ngModelChange", "change"], ["style", "color: red", 4, "ngIf"], [2, "color", "red"], ["type", "text", "pInputText", "", "id", "persona_segundo_apellido", "formControlName", "persona_segundo_apellido", 3, "ngModel", "readonly", "ngModelChange", "change"], ["severity", "warn", 3, "text"], ["type", "text", "pInputText", "", "id", "persona_primer_nombre", "formControlName", "persona_primer_nombre", 3, "ngModel", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "persona_apellido_casada", "formControlName", "persona_apellido_casada", 3, "ngModel", "readonly", "ngModelChange", "change"], [1, "md-inputfield", "md-inputfield-fill-dropdown"], ["formControlName", "par_tipo_documento_id", 3, "options", "ngModel", "showClear", "disabled", "ngModelChange", "onChange"], ["severity", "error", "text", "Contexto es requerido", 4, "ngIf"], ["severity", "error", "text", "Contexto es requerido"], ["type", "text", "pInputText", "", "id", "persona_doc_id", "formControlName", "persona_doc_id", 3, "ngModel", "readonly", "ngModelChange", "change"], ["formControlName", "persona_doc_id_ext", 3, "options", "ngModel", "showClear", "disabled", "ngModelChange", "onChange"], ["formControlName", "par_sexo_id", 3, "options", "ngModel", "showClear", "disabled", "ngModelChange", "onChange"], ["severity", "error", "text", "Sexo es requerido", 4, "ngIf"], ["severity", "error", "text", "Sexo es requerido"], ["type", "text", "pInputText", "", "formControlName", "persona_fecha_nacimiento", 3, "ngModel", "readonly", "disabled", "ngModelChange", "change"], [1, "md-inputfield", "md-inputfield-fill-dropdown", 3, "classList"], ["type", "text", "pInputText", "", "id", "par_lugar_nacimiento_id", "formControlName", "par_lugar_nacimiento_id", 3, "ngModel", "disabled", "readonly", "ngModelChange", "change"], [1, "md-inputfield"], ["type", "text", "pInputText", "", "id", "par_ocupacion", "formControlName", "par_ocupacion", 3, "ngModel", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "persona_telefono_domicilio", "formControlName", "persona_telefono_domicilio", 3, "ngModel", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "persona_telefono_celular", "formControlName", "persona_telefono_celular", 3, "ngModel", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "par_mail_id", "formControlName", "par_mail_id", 3, "ngModel", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "par_departamento_id", "formControlName", "par_departamento_id", 3, "ngModel", "disabled", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "par_localidad_id", "formControlName", "par_localidad_id", 3, "ngModel", "disabled", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "par_provincia_id", "formControlName", "par_provincia_id", 3, "ngModel", "disabled", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "persona_direccion_domicilio", "formControlName", "persona_direccion_domicilio", 3, "ngModel", "readonly", "ngModelChange", "change"], [1, "md-inputfield", "md-inputfield-fill"], ["type", "text", "pInputText", "", "id", "par_direccion_laboral", "formControlName", "par_direccion_laboral", 3, "ngModel", "readonly", "ngModelChange", "change"], ["id", "EA_SecFormaPago", "legend", "Datos para el seguro", "toggleable", "true"], ["id", "par_tipo_seguro", "formControlName", "par_tipo_seguro", 3, "options", "ngModel", "showClear", "disabled", "ngModelChange", "onChange"], ["id", "par_nro_solicitud_sci", "formControlName", "par_nro_solicitud_sci", 3, "options", "ngModel", "showClear", "disabled", "ngModelChange", "onChange"], ["type", "text", "pInputText", "", "id", "par_operacion_tipo_credito", "formControlName", "par_operacion_tipo_credito", 3, "ngModel", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "par_operacion_plazo", "formControlName", "par_operacion_plazo", 3, "ngModel", "readonly", "ngModelChange", "change"], ["type", "text", "pInputText", "", "id", "par_solicitud_prima_total", "formControlName", "par_solicitud_prima_total", 3, "ngModel", "readonly", "ngModelChange", "change"], ["selectId", "410", "formControlName", "par_forma_pago", 3, "options", "ngModel", "showClear", "disabled", "ngModelChange", "onChange"], [1, "md-inputfield-important"], ["selectId", "410", "formControlName", "par_debito_automatico_id", 3, "options", "ngModel", "showClear", "disabled", "ngModelChange", "onChange"], ["header", "Datos Complementarios", "leftIcon", "ui-icon-description"], ["legend", "Datos Adicionales del Titular", "toggleable", "true"], ["selectionMode", "single", "dataKey", "id", 3, "columns", "value", "paginator", "rows", "globalFilterFields", "selection", "selectionChange", "onRowSelect"], ["dt", ""], ["pTemplate", "caption"], ["pTemplate", "header"], ["pTemplate", "body"], [1, "ui-g-6", 2, "text-align", "left"], [1, "ui-g-6", 2, "text-align", "right"], [1, "pi", "pi-search", 2, "margin", "4px 4px 0 0"], ["type", "text", "pInputText", "", "size", "15", "placeholder", "Global Filter", 2, "width", "auto", 3, "input"], [4, "ngFor", "ngForOf"], [3, "pSelectableRow"], [1, "card", "form-group"], ["legend", "Busqueda del cliente", "toggleable", "false", 1, "ui-g-12"], [1, "ui-g-12", "ui-md-4"], ["type", "text", "pInputText", "", "formControlName", "persona_doc_id", 3, "ngModel", "ngModelChange"], ["formControlName", "persona_doc_id_ext", 3, "options", "ngModel", "showClear", "ngModelChange"], ["type", "submit", "pButton", "", "icon", "pi pi-search", 3, "disabled"]],
      template: function AltaEcoAccidentesComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, AltaEcoAccidentesComponent_div_0_Template, 2, 0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AltaEcoAccidentesComponent_app_transiciones_1_Template, 2, 1, "app-transiciones", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AltaEcoAccidentesComponent_app_actualizar_solicitud_2_Template, 2, 0, "app-actualizar-solicitud", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AltaEcoAccidentesComponent_p_panel_3_Template, 114, 58, "p-panel", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AltaEcoAccidentesComponent_p_dialog_4_Template, 12, 8, "p-dialog", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "p-dialog", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_5_listener($event) {
            return ctx.displayActualizacionExitoso = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "i", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "La informacion proveniente del banco sobre el cliente fue actualizada exitosamente");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_12_listener() {
            return ctx.displayActualizacionExitoso = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "p-dialog", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_13_listener($event) {
            return ctx.solicitudService.displayNoPuedeInstrumentarProducto = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "i", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_20_listener() {
            return ctx.solicitudService.displayNoPuedeInstrumentarProducto = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "p-dialog", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_21_listener($event) {
            return ctx.solicitudService.displayNuevoClienteSinNroCuenta = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](23, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25, "No se encontr\xF3 una cuenta para realizar el d\xE9bito de la prima");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_28_listener() {
            ctx.cancelarSolicitud();
            return ctx.solicitudService.displayNuevoClienteSinNroCuenta = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "p-dialog", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_29_listener($event) {
            return ctx.solicitudService.displayCambioEstadoExitosoSinOpciones = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](31, "i", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](34, AltaEcoAccidentesComponent_p_34_Template, 1, 1, "p", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](35, AltaEcoAccidentesComponent_p_35_Template, 1, 1, "p", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](36, AltaEcoAccidentesComponent_p_36_Template, 1, 1, "p", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](37, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "button", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_39_listener() {
            return ctx.solicitudService.displayCambioEstadoExitosoSinOpciones = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "button", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_40_listener() {
            return ctx.solicitudService.displayCambioEstadoExitosoSinOpciones = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](41, "p-dialog", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_41_listener($event) {
            return ctx.solicitudService.displayCambioEstadoExitoso = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](43, "i", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](44, "p", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](45, AltaEcoAccidentesComponent_p_45_Template, 2, 0, "p", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](46, AltaEcoAccidentesComponent_p_46_Template, 2, 1, "p", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](47, AltaEcoAccidentesComponent_p_47_Template, 2, 1, "p", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "button", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_50_listener() {
            ctx.solicitudService.displayCambioEstadoExitoso = false;
            return ctx.solicitudService.asegurado.instancia_poliza.id_estado == ctx.solicitudService.estadoSolicitado.id ? ctx.solicitudService.ImprimirSolicitud(ctx.solicitudService.asegurado.instancia_poliza.id, ctx.solicitudService.asegurado.instancia_poliza.id_poliza, null, ctx.solicitudService.persona_banco_solicitud.solicitud_sci_selected) : ctx.solicitudService.asegurado.instancia_poliza.id_estado == ctx.solicitudService.estadoPorPagar.id ? ctx.solicitudService.ImprimirOrden(ctx.solicitudService.asegurado.instancia_poliza.id, ctx.solicitudService.asegurado.instancia_poliza.id_poliza) : ctx.solicitudService.asegurado.instancia_poliza.id_estado == ctx.solicitudService.estadoEmitido.id ? ctx.solicitudService.ImprimirCertificado(ctx.solicitudService.asegurado.instancia_poliza.id, ctx.solicitudService.asegurado.instancia_poliza.id_poliza) : "";
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](51, AltaEcoAccidentesComponent_button_51_Template, 1, 0, "button", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "p-dialog", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_52_listener($event) {
            return ctx.solicitudService.displayModalSolicitudExistente = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](54, "i", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](55, "p", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](56, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](57, "div", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](59, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](60, "button", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_60_listener() {
            return ctx.solicitudService.verGestionSolicitudes(ctx.solicitudService.asegurados, null);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](61, AltaEcoAccidentesComponent_div_61_Template, 2, 0, "div", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](62, AltaEcoAccidentesComponent_div_62_Template, 2, 0, "div", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](63, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "button", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_64_listener() {
            return ctx.cancelarSolicitud();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](65, "p-dialog", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_65_listener($event) {
            return ctx.solicitudService.displayValidacionConObservaciones = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](66, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](67, "i", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](68, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](69, "Antes de continuar debe resolver las siguientes observaciones:");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](71, "p-messages", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](72, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](73, AltaEcoAccidentesComponent_p_messages_73_Template, 1, 2, "p-messages", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](74, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](75, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_76_listener() {
            return ctx.solicitudService.displayValidacionConObservaciones = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](77, "p-dialog", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_77_listener($event) {
            return ctx.solicitudService.displayValidacionSinObservaciones = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](78, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](79, "i", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](80, "p", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](81, AltaEcoAccidentesComponent_div_81_Template, 2, 2, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](83, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](84, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_84_listener() {
            return ctx.cambiarSolicitudEstado();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](85, "button", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_85_listener() {
            return ctx.solicitudService.displayValidacionSinObservaciones = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](86, "p-dialog", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_86_listener($event) {
            return ctx.solicitudService.displayValidacionInfoWarnings = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](87, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](88, "i", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](89, "p", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](90, AltaEcoAccidentesComponent_div_90_Template, 2, 2, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](91, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](92, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](93, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_93_listener() {
            return ctx.cambiarSolicitudEstado();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](94, "button", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_94_listener() {
            return ctx.solicitudService.displayValidacionInfoWarnings = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](95, "p-dialog", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_95_listener($event) {
            return ctx.solicitudService.displayValidacionMessages = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](96, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](97, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](98, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](99);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](100, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](101, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](102, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_102_listener() {
            return ctx.solicitudService.displayValidacionMessages = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](103, AltaEcoAccidentesComponent_p_dialog_103_Template, 13, 12, "p-dialog", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](104, "p-dialog", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_104_listener($event) {
            return ctx.solicitudService.displayValidacionAlInicio = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](105, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](106, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](107, "p", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](108, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](109, "p-messages", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](110, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](111, "p-messages", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](112, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](113, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](114, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_114_listener() {
            return ctx.onAcceptValidationInit();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](115, "p-dialog", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_115_listener($event) {
            return ctx.solicitudService.displayNuevoCliente = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](116, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](117, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](118, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](119, "El n\xFAmero de carnet digitado no fue encontrado en la base de PERSONAS del banco");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](120, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](121, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](122, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_122_listener() {
            ctx.cancelarSolicitud();
            return ctx.solicitudService.displayNuevoCliente = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](123, "p-dialog", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_123_listener($event) {
            return ctx.displayErrorMontoCuota = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](124, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](125, "i", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](126, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](127, "No se logro validar el monto y la cuota respecto a la modalidad de pago, por favor contactese con el area de TI");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](128, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](129, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](130, "button", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_130_listener() {
            return ctx.displayErrorMontoCuota = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](131, "p-dialog", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_131_listener($event) {
            return ctx.displayErrorPlanPago = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](132, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](133, "i", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](134, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](135, "No se logro iniciar el plan de pago, por favor contactese con el area de TI");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](136, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](137, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](138, "button", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_138_listener() {
            return ctx.displayErrorPlanPago = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](139, "p-dialog", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_139_listener($event) {
            return ctx.solicitudService.displayVerificaArchivo = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](140, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](141, "i", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](142, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](143, "No se subi\xF3 ningun archivo de solicitud, \xBFDesea continuar? ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](144, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](145, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](146, "button", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_146_listener() {
            return ctx.solicitudService.displayVerificaArchivo = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](147, "p-dialog", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_147_listener($event) {
            return ctx.displayEdadIncorrecta = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](148, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](149, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](150, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](151);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](152, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](153, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](154, "button", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_154_listener() {
            return ctx.displayEdadIncorrecta = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](155, "p-dialog", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_155_listener($event) {
            return ctx.displayClienteNoExiste = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](156, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](157, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](158, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](159, "El cliente no existe");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](160, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](161, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](162, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_162_listener() {
            ctx.cancelarSolicitud();
            return ctx.displayClienteNoExiste = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](163, "p-dialog", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_163_listener($event) {
            return ctx.solicitudService.displayClienteSinCredito = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](164, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](165, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](166, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](167, "El cliente no tiene asociado ningun cr\xE9dito");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](168, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](169, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](170, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_170_listener() {
            ctx.cancelarSolicitud();
            return ctx.solicitudService.displayClienteSinCredito = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](171, "p-dialog", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_171_listener($event) {
            return ctx.displayTarjetaInvalida = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](172, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](173, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](174, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](175, "La tarjeta es invalida, por favor, verifica que los datos sean correctos.");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](176, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](177, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](178, "button", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_178_listener() {
            return ctx.displayTarjetaInvalida = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](179, "p-dialog", 49);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_179_listener($event) {
            return ctx.solicitudService.displaySeguroDeRefrescar = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](180, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](181, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](182, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](183, "Confirme esta operacion solamente si realiz\xF3 cambios recientes en el sistema del banco que no se encuentren reflejados en el sistema Colibri y desea actualizarlos, \xBFDesea continuar?");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](184, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](185, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](186, "button", 50);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_186_listener() {
            ctx.solicitudService.displaySeguroDeRefrescar = false;
            return ctx.refrescarInformacion();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](187, "button", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_187_listener() {
            return ctx.solicitudService.displaySeguroDeRefrescar = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](188, "p-dialog", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_188_listener($event) {
            return ctx.displayTarjetaInvalidaContinuar = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](189, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](190, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](191, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](192, "La tarjeta es invalida, desea continuar?.");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](193, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](194, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](195, "button", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_195_listener() {
            ctx.displayTarjetaInvalidaContinuar = false;
            return ctx.guardarSolicitudPersona();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](196, "button", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_196_listener() {
            return ctx.displayTarjetaInvalidaContinuar = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](197, "p-dialog", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_197_listener($event) {
            return ctx.displayIntroduscaNroTarjeta = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](198, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](199, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](200, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](201, "Debe introducir un Nro de Tarjeta");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](202, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](203, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](204, "button", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_204_listener() {
            return ctx.displayIntroduscaNroTarjeta = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](205, "p-dialog", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_205_listener($event) {
            return ctx.solicitudService.displaySolicitudSciInvalida = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](206, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](207, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](208, "p", 56);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](209, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](210, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](211, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_211_listener() {
            return ctx.solicitudService.displaySolicitudSciInvalida = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](212, "p-dialog", 57);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_212_listener($event) {
            return ctx.displayNroCuentasActualizados = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](213, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](214, "i", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](215, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](216, "La informaci\xF3n relacionada al Nro. de tarjeta fue recuperada exitosamente.");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](217, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](218, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](219, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_219_listener() {
            return ctx.displayNroCuentasActualizados = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](220, "p-dialog", 58);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_220_listener($event) {
            return ctx.displayNroTarjetaSinCuentas = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](221, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](222, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](223, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](224);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](225, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](226, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](227, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_227_listener() {
            return ctx.displayNroTarjetaSinCuentas = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](228, "p-dialog", 59);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_228_listener($event) {
            return ctx.displaySolicitudDuplicado = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](229, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](230, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](231, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](232);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](233, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](234, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](235, "button", 60);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_235_listener() {
            ctx.abrirVentanaRegistroNuevoTitular();
            return ctx.displaySolicitudDuplicado = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](236, "p-dialog", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function AltaEcoAccidentesComponent_Template_p_dialog_visibleChange_236_listener($event) {
            return ctx.solicitudService.displayErrorRespuestaBanco = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](237, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](238, "i", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](239, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](240);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](241, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](242, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](243, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AltaEcoAccidentesComponent_Template_button_click_243_listener() {
            return ctx.solicitudService.displayErrorRespuestaBanco = false;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](244, AltaEcoAccidentesComponent_p_panel_244_Template, 12, 8, "p-panel", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](245, AltaEcoAccidentesComponent_div_245_Template, 19, 7, "div", 61);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](246, "p-toast", 62);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.isLoadingAgain);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado && ctx.solicitudService.asegurado.instancia_poliza && ctx.solicitudService.asegurado.instancia_poliza.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.displayModalDatosTitular && ctx.solicitudService.asegurado);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.fechaEmisionForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayActualizacionExitoso)("modal", true)("responsive", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayNoPuedeInstrumentarProducto)("modal", true)("responsive", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("No cuenta con permisos para instrumentar ", ctx.solicitudService.poliza.descripcion, ", consulte a soporte Colibri");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayNuevoClienteSinNroCuenta)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayCambioEstadoExitosoSinOpciones)("modal", true)("responsive", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.solicitudService.msgCambioExitoso, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado.instancia_poliza.id_estado == ctx.solicitudService.estadoSolicitado.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado.instancia_poliza.id_estado == ctx.solicitudService.estadoPorPagar.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado.instancia_poliza.id_estado == ctx.solicitudService.estadoEmitido.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayCambioEstadoExitoso)("modal", true)("responsive", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx.solicitudService.msgCambioExitoso, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado.instancia_poliza.id_estado == ctx.solicitudService.estadoSolicitado.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado.instancia_poliza.id_estado == ctx.solicitudService.estadoPorPagar.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado.instancia_poliza.id_estado == ctx.solicitudService.estadoEmitido.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("label", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](133, _c4, ctx.solicitudService.estadoPorPagar.id).includes(ctx.solicitudService.asegurado.instancia_poliza.id_estado) ? "SI" : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](135, _c4, ctx.solicitudService.estadoPorEmitir.id).includes(ctx.solicitudService.asegurado.instancia_poliza.id_estado) ? "Aceptar" : "Aceptar");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](137, _c4, ctx.solicitudService.estadoPorPagar.id).includes(ctx.solicitudService.asegurado.instancia_poliza.id_estado));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayModalSolicitudExistente)("modal", true)("closable", false)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx.solicitudService.msgSolicitudExistente, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.btnCrearEcoResguardoEnabled);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.btnCrearEcoAccidenteEnabled);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayValidacionConObservaciones)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("closable", false)("value", ctx.solicitudService.msgs_error);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.mostrarObservaciones);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayValidacionSinObservaciones)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx.solicitudService.msgText + ", \xBFDesea continuar?", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.mostrarObservaciones);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayValidacionInfoWarnings)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx.solicitudService.msgText + ", \xBFDesea continuar?", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.mostrarAdvertencias);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayValidacionMessages)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.solicitudService.msgsFromSolicitud);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.aseguradoWithDiferentDocIdExt);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayValidacionAlInicio)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx.solicitudService.msgText + ", \xBFDesea continuar?", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("closable", false)("value", ctx.solicitudService.msgs_error);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("closable", false)("value", ctx.solicitudService.msgs_warn);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayNuevoCliente)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayErrorMontoCuota)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayErrorPlanPago)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayVerificaArchivo)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayEdadIncorrecta)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"]("La edad debe estar entre ", ctx.solicitudService.edadMinimaYears, " a ", ctx.solicitudService.edadMaximaYears, ", \xBFDesea continuar?");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayClienteNoExiste)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayClienteSinCredito)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayTarjetaInvalida)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displaySeguroDeRefrescar)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayTarjetaInvalidaContinuar)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayIntroduscaNroTarjeta)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displaySolicitudSciInvalida)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx.solicitudService.msgSolicitudSciInvalida, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayNroCuentasActualizados)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayNroTarjetaSinCuentas)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("El Nro de tarjeta de d\xE9bito introducido no tiene cuentas asignadas a la misma ", ctx.solicitudService.Cuentas.length ? ", actualmente se muestran solo las cuentas relacionadas a NetBank." : ".", "");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displaySolicitudDuplicado)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate5"]("Se encontro (", ctx.solicitudService.asegurados.length, ") solicitudes referentes al cliente: ", ctx.solicitudService.asegurado.entidad.persona.persona_segundo_apellido, " ", ctx.solicitudService.asegurado.entidad.persona.persona_primer_nombre, " con CI: ", ctx.solicitudService.asegurado.entidad.persona.persona_doc_id, " ", ctx.solicitudService.ProcedenciaCIParametroCod[ctx.solicitudService.asegurado.entidad.persona.persona_doc_id_ext], "");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.solicitudService.displayErrorRespuestaBanco)("modal", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Hubo un problema relacionado con la respuesta de los servicios del banco, el servicio: ", ctx.solicitudService.nombreServicioBanco, " no genero ningun dato, por favor contactese con el area de sistemas del Banco, o intente mas tarde.");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.displayModalFormTitular);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.formBusqueda && ctx.solicitudService.displayBusquedaCI);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](139, _c6));
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_48__["NgIf"], primeng__WEBPACK_IMPORTED_MODULE_49__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["Footer"], primeng__WEBPACK_IMPORTED_MODULE_49__["ButtonDirective"], primeng_messages__WEBPACK_IMPORTED_MODULE_50__["Messages"], primeng_toast__WEBPACK_IMPORTED_MODULE_51__["Toast"], _src_core_componentes_transiciones_transiciones_component__WEBPACK_IMPORTED_MODULE_43__["TransicionesComponent"], _src_core_componentes_actualizar_solicitud_actualizar_solicitud_component__WEBPACK_IMPORTED_MODULE_47__["ActualizarSolicitudComponent"], primeng_panel__WEBPACK_IMPORTED_MODULE_52__["Panel"], primeng__WEBPACK_IMPORTED_MODULE_49__["Button"], primeng__WEBPACK_IMPORTED_MODULE_49__["Fieldset"], _src_core_componentes_beneficiario_beneficiario_component__WEBPACK_IMPORTED_MODULE_44__["BeneficiarioComponent"], _src_core_componentes_archivos_archivos_component__WEBPACK_IMPORTED_MODULE_45__["ArchivosComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"], primeng_calendar__WEBPACK_IMPORTED_MODULE_53__["Calendar"], primeng__WEBPACK_IMPORTED_MODULE_49__["InputText"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"], primeng_message__WEBPACK_IMPORTED_MODULE_54__["UIMessage"], primeng_tabview__WEBPACK_IMPORTED_MODULE_55__["TabView"], primeng_tabview__WEBPACK_IMPORTED_MODULE_55__["TabPanel"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], primeng__WEBPACK_IMPORTED_MODULE_49__["Dropdown"], primeng__WEBPACK_IMPORTED_MODULE_49__["Table"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], _angular_common__WEBPACK_IMPORTED_MODULE_48__["NgForOf"], primeng__WEBPACK_IMPORTED_MODULE_49__["SelectableRow"]],
      styles: ["body[_ngcontent-%COMP%]   .table-box[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{\n  padding:0px 10px\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2NyZWRpdGljaW9zL3NyYy9hcHAvZWNvLWFjY2lkZW50ZXMvYWx0YS1lY28tYWNjaWRlbnRlcy9hbHRhLWVjby1hY2NpZGVudGVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRTtBQUNGIiwiZmlsZSI6InByb2plY3RzL2NyZWRpdGljaW9zL3NyYy9hcHAvZWNvLWFjY2lkZW50ZXMvYWx0YS1lY28tYWNjaWRlbnRlcy9hbHRhLWVjby1hY2NpZGVudGVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJib2R5IC50YWJsZS1ib3ggdGR7XG4gIHBhZGRpbmc6MHB4IDEwcHhcbn1cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AltaEcoAccidentesComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-alta-eco-accidentes',
          templateUrl: './alta-eco-accidentes.component.html',
          styleUrls: ['./alta-eco-accidentes.component.css']
        }]
      }], function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_11__["BreadcrumbService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]
        }, {
          type: _src_core_servicios_parametro_service__WEBPACK_IMPORTED_MODULE_12__["ParametrosService"]
        }, {
          type: _src_core_servicios_anexo_service__WEBPACK_IMPORTED_MODULE_13__["AnexoService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
        }, {
          type: _src_core_servicios_persona_service__WEBPACK_IMPORTED_MODULE_14__["PersonaService"]
        }, {
          type: _src_core_servicios_beneficiario_service__WEBPACK_IMPORTED_MODULE_15__["BeneficiarioService"]
        }, {
          type: _src_core_servicios_soapui_service__WEBPACK_IMPORTED_MODULE_16__["SoapuiService"]
        }, {
          type: _src_core_servicios_atributo_service__WEBPACK_IMPORTED_MODULE_17__["AtributoService"]
        }, {
          type: _src_core_servicios_objetoAtributo_service__WEBPACK_IMPORTED_MODULE_18__["ObjetoAtributoService"]
        }, {
          type: _src_core_servicios_documento_service__WEBPACK_IMPORTED_MODULE_19__["DocumentoService"]
        }, {
          type: _src_core_servicios_poliza_service__WEBPACK_IMPORTED_MODULE_20__["PolizaService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _src_core_servicios_instancia_poliza_service__WEBPACK_IMPORTED_MODULE_21__["InstanciaPolizaService"]
        }, {
          type: primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]
        }, {
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"]
        }, {
          type: _src_core_servicios_reporte_service__WEBPACK_IMPORTED_MODULE_22__["ReporteService"]
        }, {
          type: _src_core_servicios_rol_service__WEBPACK_IMPORTED_MODULE_23__["RolesService"]
        }, {
          type: primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]
        }, {
          type: _src_core_servicios_usuarios_service__WEBPACK_IMPORTED_MODULE_24__["UsuariosService"]
        }, {
          type: _src_core_servicios_contexto_service__WEBPACK_IMPORTED_MODULE_25__["ContextoService"]
        }, {
          type: _src_core_servicios_instancia_poliza_trans_service__WEBPACK_IMPORTED_MODULE_26__["InstanciaPolizaTransService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]
        }, {
          type: _src_core_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_27__["SolicitudService"]
        }, {
          type: _src_core_servicios_plan_pago_service__WEBPACK_IMPORTED_MODULE_28__["PlanPagoService"]
        }, {
          type: _src_core_servicios_archivo_service__WEBPACK_IMPORTED_MODULE_42__["ArchivoService"]
        }, {
          type: _src_core_servicios_sessionStorage_service__WEBPACK_IMPORTED_MODULE_29__["SessionStorageService"]
        }, {
          type: _src_core_servicios_administracion_de_permisos_service__WEBPACK_IMPORTED_MODULE_30__["AdministracionDePermisosService"]
        }];
      }, {
        transicionesComponent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['componentTransiciones', {
            "static": false
          }]
        }],
        componenteBeneficiario: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ["componenteBeneficiario", {
            "static": false
          }]
        }],
        componenteArchivo: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ["componenteArchivo", {
            "static": false
          }]
        }],
        componenteActualizarSolicitud: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ["componenteActualizarSolicitud", {
            "static": false
          }]
        }]
      });
    })();
    /***/

  },

  /***/
  "../crediticios/src/app/eco-accidentes/gestion-eco-accidentes/gestion-eco-accidentes.component.ts":
  /*!********************************************************************************************************!*\
    !*** ../crediticios/src/app/eco-accidentes/gestion-eco-accidentes/gestion-eco-accidentes.component.ts ***!
    \********************************************************************************************************/

  /*! exports provided: GestionEcoAccidentesComponent */

  /***/
  function crediticiosSrcAppEcoAccidentesGestionEcoAccidentesGestionEcoAccidentesComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GestionEcoAccidentesComponent", function () {
      return GestionEcoAccidentesComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "../../node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! primeng/api */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-api.js");
    /* harmony import */


    var _src_helpers_prototypes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../../../src/helpers/prototypes */
    "../../src/helpers/prototypes.ts");
    /* harmony import */


    var _src_helpers_prototypes__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_src_helpers_prototypes__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! file-saver */
    "../../node_modules/file-saver/dist/FileSaver.min.js");
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_5__);
    /* harmony import */


    var xlsx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! xlsx */
    "../../node_modules/xlsx/xlsx.js");
    /* harmony import */


    var xlsx__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_6__);
    /* harmony import */


    var _src_core_modelos_poliza__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/poliza */
    "../../src/core/modelos/poliza.ts");
    /* harmony import */


    var _src_core_modelos_plan_pago__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/plan_pago */
    "../../src/core/modelos/plan_pago.ts");
    /* harmony import */


    var _src_core_modelos_instancia_poliza__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/instancia_poliza */
    "../../src/core/modelos/instancia_poliza.ts");
    /* harmony import */


    var _src_core_modelos_componente__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../../../../src/core/modelos/componente */
    "../../src/core/modelos/componente.ts");
    /* harmony import */


    var _src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/breadcrumb.service */
    "../../src/core/servicios/breadcrumb.service.ts");
    /* harmony import */


    var _src_core_servicios_parametro_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/parametro.service */
    "../../src/core/servicios/parametro.service.ts");
    /* harmony import */


    var _src_core_servicios_instancia_poliza_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/instancia-poliza.service */
    "../../src/core/servicios/instancia-poliza.service.ts");
    /* harmony import */


    var _src_core_servicios_reporte_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/reporte.service */
    "../../src/core/servicios/reporte.service.ts");
    /* harmony import */


    var _src_core_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/solicitud.service */
    "../../src/core/servicios/solicitud.service.ts");
    /* harmony import */


    var _src_core_servicios_sessionStorage_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/sessionStorage.service */
    "../../src/core/servicios/sessionStorage.service.ts");
    /* harmony import */


    var _src_core_servicios_plan_pago_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/plan-pago.service */
    "../../src/core/servicios/plan-pago.service.ts");
    /* harmony import */


    var _src_core_servicios_poliza_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/poliza.service */
    "../../src/core/servicios/poliza.service.ts");
    /* harmony import */


    var _src_core_servicios_persona_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/persona.service */
    "../../src/core/servicios/persona.service.ts");
    /* harmony import */


    var _src_core_servicios_menu_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ../../../../../../src/core/servicios/menu.service */
    "../../src/core/servicios/menu.service.ts");
    /* harmony import */


    var primeng_utils__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! primeng/utils */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-utils.js");
    /* harmony import */


    var _src_core_componentes_actualizar_solicitud_actualizar_solicitud_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! ../../../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component */
    "../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component.ts");
    /* harmony import */


    var _src_helpers_util__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! ../../../../../../src/helpers/util */
    "../../src/helpers/util.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
    /*! @angular/common */
    "../../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var primeng__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
    /*! primeng */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng.js");
    /* harmony import */


    var primeng_panel__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
    /*! primeng/panel */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-panel.js");
    /* harmony import */


    var primeng_toast__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
    /*! primeng/toast */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-toast.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(
    /*! @angular/forms */
    "../../node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var primeng_calendar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(
    /*! primeng/calendar */
    "../../node_modules/primeng/__ivy_ngcc__/fesm2015/primeng-calendar.js");

    var _c0 = ["componenteActualizarSolicitud"];

    function GestionEcoAccidentesComponent_div_0_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function GestionEcoAccidentesComponent_p_fieldset_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-fieldset", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "p-dropdown", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_4_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r8.objeto.sucursal = $event;
        })("onChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_p_dropdown_onChange_4_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r10.filterAgencies($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Sucursal");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "p-dropdown", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_9_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r11.objeto.agencia = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Agencia");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_input_ngModelChange_14_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r12.objeto.usuario_login = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Usuario");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "input", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_input_ngModelChange_19_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r13.objeto.nro_documento = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "Nro Solicitud");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_input_ngModelChange_24_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r14.objeto.id_certificado = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26, "Nro. Certificado");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "p-dropdown", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_29_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r15.objeto.estado = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](31, "Estado");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_input_ngModelChange_34_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r16.objeto.persona_primer_apellido = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](36, "Apellido Paterno");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](37, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_input_ngModelChange_39_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r17.objeto.persona_segundo_apellido = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](41, "Apellido Materno");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](43, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](44, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_input_ngModelChange_44_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r18.objeto.persona_primer_nombre = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](46, "Nombre");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "input", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_input_ngModelChange_49_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r19.objeto.persona_doc_id = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](51, "Carnet de identidad");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "p-dropdown", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_9_Template_p_dropdown_ngModelChange_54_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r20.objeto.id_poliza = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](55, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](56, "Poliza");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.Sucursales)("ngModel", ctx_r1.objeto.sucursal)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.Agencias)("ngModel", ctx_r1.objeto.agencia)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.usuario_login);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.nro_documento);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.id_certificado);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.Estados)("ngModel", ctx_r1.objeto.estado)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.persona_primer_apellido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.persona_segundo_apellido);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.persona_primer_nombre);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.objeto.persona_doc_id);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.Polizas)("ngModel", ctx_r1.objeto.id_poliza)("showClear", true);
      }
    }

    function GestionEcoAccidentesComponent_p_fieldset_10_div_7_Template(rf, ctx) {
      if (rf & 1) {
        var _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "p-calendar", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_10_div_7_Template_p_calendar_ngModelChange_3_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r23);

          var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r22.objeto.fecha_min = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "p-calendar", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_10_div_7_Template_p_calendar_ngModelChange_6_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r23);

          var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r24.objeto.fecha_max = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r21.objeto.fecha_min)("showIcon", true)("showButtonBar", true)("locale", ctx_r21.util.calendario_es);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r21.objeto.fecha_max)("showIcon", true)("showButtonBar", true)("locale", ctx_r21.util.calendario_es);
      }
    }

    function GestionEcoAccidentesComponent_p_fieldset_10_Template(rf, ctx) {
      if (rf & 1) {
        var _r26 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-fieldset", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "p-dropdown", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function GestionEcoAccidentesComponent_p_fieldset_10_Template_p_dropdown_ngModelChange_4_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r26);

          var ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r25.objeto.campo_fecha = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Fechas");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, GestionEcoAccidentesComponent_p_fieldset_10_div_7_Template, 7, 8, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r2.FiltrosFechas)("ngModel", ctx_r2.objeto.campo_fecha)("showClear", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.objeto.campo_fecha);
      }
    }

    function GestionEcoAccidentesComponent_ng_template_17_span_3_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "(solo se mostrar\xE1n los 300 primeros resultados)");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function GestionEcoAccidentesComponent_ng_template_17_Template(rf, ctx) {
      if (rf & 1) {
        var _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Resultados ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, GestionEcoAccidentesComponent_ng_template_17_span_3_Template, 3, 0, "span", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "button", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionEcoAccidentesComponent_ng_template_17_Template_button_click_5_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r29);

          var ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](16);

          return ctx_r28.exportExcel(_r3);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r4.showMoreResultsMessage);
      }
    }

    function GestionEcoAccidentesComponent_ng_template_18_th_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "th", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "b", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "p-sortIcon", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var col_r33 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", col_r33.width);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("pSortableColumn", col_r33.field);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](col_r33.header);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("field", col_r33.field);
      }
    }

    function GestionEcoAccidentesComponent_ng_template_18_th_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "th", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("input", function GestionEcoAccidentesComponent_ng_template_18_th_3_Template_input_input_1_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r36);

          var col_r34 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](16);

          return _r3.filter($event.target.value, col_r34.field, "contains");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var col_r34 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngSwitch", col_r34.field);
      }
    }

    function GestionEcoAccidentesComponent_ng_template_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, GestionEcoAccidentesComponent_ng_template_18_th_1_Template, 4, 5, "th", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, GestionEcoAccidentesComponent_ng_template_18_th_3_Template, 2, 1, "th", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var columns_r30 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", columns_r30);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", columns_r30);
      }
    }

    var _c1 = function _c1() {
      return [24, 81, 302];
    };

    function GestionEcoAccidentesComponent_ng_template_19_button_28_Template(rf, ctx) {
      if (rf & 1) {
        var _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionEcoAccidentesComponent_ng_template_19_button_28_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r46);

          var instancia_poliza_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          var ctx_r44 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r44.EditarPoliza(instancia_poliza_r37.id_poliza, instancia_poliza_r37.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        var ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("icon", "ui-icon-", ctx_r39.ValidarEstados(instancia_poliza_r37.id_estado, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](3, _c1)) ? "edit" : "remove-red-eye", "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("title", ctx_r39.ValidarEstados(instancia_poliza_r37.id_estado, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](4, _c1)) ? "Editar" : "Ver");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r39.btnEditar);
      }
    }

    var _c2 = function _c2() {
      return [238];
    };

    function GestionEcoAccidentesComponent_ng_template_19_button_29_Template(rf, ctx) {
      if (rf & 1) {
        var _r50 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionEcoAccidentesComponent_ng_template_19_button_29_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r50);

          var instancia_poliza_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          var ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r48.ValidarEstados(instancia_poliza_r37.id_estado, [238]) ? ctx_r48.EditarPoliza(instancia_poliza_r37.id_poliza, instancia_poliza_r37.id) : ctx_r48.VerPoliza(instancia_poliza_r37.id_poliza, instancia_poliza_r37.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        var ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("icon", "ui-icon-", ctx_r40.ValidarEstados(instancia_poliza_r37.id_estado, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](3, _c2)) ? "edit" : "remove-red-eye", "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("title", ctx_r40.ValidarEstados(instancia_poliza_r37.id_estado, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](4, _c2)) ? "Editar" : "Ver");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r40.btnVer1);
      }
    }

    function GestionEcoAccidentesComponent_ng_template_19_button_30_Template(rf, ctx) {
      if (rf & 1) {
        var _r54 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionEcoAccidentesComponent_ng_template_19_button_30_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r54);

          var ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          var instancia_poliza_r37 = ctx_r53.$implicit;
          var i_r38 = ctx_r53.rowIndex;

          var ctx_r52 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r52.buscarObjetoAsegurado(instancia_poliza_r37.id, i_r38);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r41.btnEmitir);
      }
    }

    function GestionEcoAccidentesComponent_ng_template_19_button_31_Template(rf, ctx) {
      if (rf & 1) {
        var _r57 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionEcoAccidentesComponent_ng_template_19_button_31_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r57);

          var instancia_poliza_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          var ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r55.solicitudService.ImprimirCertificado(instancia_poliza_r37.id, instancia_poliza_r37.id_poliza, null);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function GestionEcoAccidentesComponent_ng_template_19_button_32_Template(rf, ctx) {
      if (rf & 1) {
        var _r60 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionEcoAccidentesComponent_ng_template_19_button_32_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r60);

          var instancia_poliza_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          var ctx_r58 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r58.solicitudService.ImprimirSolicitud(instancia_poliza_r37.id, instancia_poliza_r37.id_poliza, null, instancia_poliza_r37.Nro_Sol_Sci);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    var _c3 = function _c3() {
      return [24, 59, 60, 62, 63, 65, 81, 238, 282, 302];
    };

    var _c4 = function _c4() {
      return [5, 15, 9, 16];
    };

    var _c5 = function _c5() {
      return [13];
    };

    var _c6 = function _c6() {
      return [59, 60, 62, 63, 65, 282];
    };

    var _c7 = function _c7() {
      return [5, 9, 13, 15, 16];
    };

    var _c8 = function _c8() {
      return [59, 60, 62, 63, 65, 81, 238, 282];
    };

    function GestionEcoAccidentesComponent_ng_template_19_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "td", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](28, GestionEcoAccidentesComponent_ng_template_19_button_28_Template, 1, 5, "button", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](29, GestionEcoAccidentesComponent_ng_template_19_button_29_Template, 1, 5, "button", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](30, GestionEcoAccidentesComponent_ng_template_19_button_30_Template, 1, 1, "button", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](31, GestionEcoAccidentesComponent_ng_template_19_button_31_Template, 1, 0, "button", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](32, GestionEcoAccidentesComponent_ng_template_19_button_32_Template, 1, 0, "button", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var instancia_poliza_r37 = ctx.$implicit;

        var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Nro"] ? instancia_poliza_r37["Nro"] : "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Nro_Sol"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["CI"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Sucursal"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Agencia"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Nro_Certificado"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Estado"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Fecha_Registro"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Fecha_Inicio_Cert"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Fecha_Fin_Cert"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["Asegurado"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["oficial"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](instancia_poliza_r37["producto"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r6.verifyRol(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](18, _c3), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](19, _c4), instancia_poliza_r37));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r6.verifyRol(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](20, _c3), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](21, _c5), instancia_poliza_r37));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r6.verifyRol(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](22, _c2), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](23, _c5), instancia_poliza_r37));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r6.verifyRol(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](24, _c6), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](25, _c7), instancia_poliza_r37));

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r6.verifyRol(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](26, _c8), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](27, _c7), instancia_poliza_r37));
      }
    }

    function GestionEcoAccidentesComponent_app_actualizar_solicitud_21_Template(rf, ctx) {
      if (rf & 1) {
        var _r63 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-actualizar-solicitud", 59, 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("setAsegurado", function GestionEcoAccidentesComponent_app_actualizar_solicitud_21_Template_app_actualizar_solicitud_setAsegurado_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r63);

          var ctx_r62 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r62.solicitudService.asegurado = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    var _c9 = function _c9() {
      return [10, 25, 50];
    };

    var _c10 = function _c10() {
      return {
        marginTop: "70px"
      };
    };

    var GestionEcoAccidentesComponent = /*#__PURE__*/function () {
      function GestionEcoAccidentesComponent(params, breadcrumbService, parametrosService, instanciaPolizaService, router, menuService, reporteService, service, personaService, polizaService, solicitudService, sessionStorageService, planPagoService) {
        _classCallCheck(this, GestionEcoAccidentesComponent);

        this.params = params;
        this.breadcrumbService = breadcrumbService;
        this.parametrosService = parametrosService;
        this.instanciaPolizaService = instanciaPolizaService;
        this.router = router;
        this.menuService = menuService;
        this.reporteService = reporteService;
        this.service = service;
        this.personaService = personaService;
        this.polizaService = polizaService;
        this.solicitudService = solicitudService;
        this.sessionStorageService = sessionStorageService;
        this.planPagoService = planPagoService;
        this.color = 'primary';
        this.mode = 'indeterminate';
        this.value = 1;
        this.parametros = [];
        this.instancias_polizas = [];
        this.instancias = [];
        this.poliza = new _src_core_modelos_poliza__WEBPACK_IMPORTED_MODULE_7__["Poliza"]();
        this.objeto = new _src_core_modelos_componente__WEBPACK_IMPORTED_MODULE_10__["Filtro"]();
        this.paramAgencias = [];
        this.paramSucursales = [];
        this.showFiltros = true;
        this.FiltrosFechas = [];
        this.ProcedenciaCI = [];
        this.Polizas = [];
        this.Agencias = [];
        this.Sucursales = [];
        this.Estados = [];
        this.polizas = [];
        this.util = new _src_helpers_util__WEBPACK_IMPORTED_MODULE_23__["Util"]();
        this.displayExedioResultados = false;
        this.showMoreResultsMessage = false;
        this.PanelFiltrosColapsed = false;
        this.btnEmitir = false;
        this.btnEditar = false;
        this.btnVer1 = false;
        this.btnVer2 = false;
        this.rowsPerPage = 0;
        this.numRecords = 0;
        this.totalRows = 0;
        this.cols = [{
          field: 'Nro',
          header: 'Nro',
          width: '5%'
        }, // {field: 'Id', header: 'Id', width: '8%'},
        {
          field: 'Nro_Sol',
          header: 'Nro. Sol.',
          width: '8%'
        }, {
          field: 'CI',
          header: 'CI',
          width: '8%'
        }, {
          field: 'Sucursal',
          header: 'Sucursal',
          width: '8%'
        }, {
          field: 'Agencia',
          header: 'Agencia',
          width: '10%'
        }, {
          field: 'Nro_Certificado',
          header: 'Nro. Certificado',
          width: '7%'
        }, {
          field: 'Estado',
          header: 'Estado',
          width: '7%'
        }, {
          field: 'Fecha_Registro',
          header: 'Fecha Registro',
          width: '8%'
        }, {
          field: 'Fecha_Inicio_Cert',
          header: 'Fecha Inicio Cert',
          width: '8%'
        }, {
          field: 'Fecha_Fin_Cert',
          header: 'Fecha Fin Cert',
          width: '8%'
        }, // {field: 'Fecha_Solicitud', header: 'Fecha Solicitud', width: '8%'},
        // {field: 'Fecha_Emision', header: 'Fecha Emisión', width: '8%'},
        {
          field: 'Asegurado',
          header: 'Asegurado',
          width: '8%'
        }, {
          field: 'oficial',
          header: 'Usuario',
          width: '8%'
        }, {
          field: 'producto',
          header: 'Poliza',
          width: '10%'
        }, {
          field: null,
          header: 'Acciones',
          width: '6%'
        }];

        primeng_utils__WEBPACK_IMPORTED_MODULE_21__["FilterUtils"]['custom'] = function (value, filter) {
          if (filter === undefined || filter === null || filter.trim() === '') {
            return true;
          }

          if (value === undefined || value === null) {
            return false;
          }

          return parseInt(filter) > value;
        };
      }

      _createClass(GestionEcoAccidentesComponent, [{
        key: "ValidarComponentesInvisible",
        value: function ValidarComponentesInvisible(id) {
          if (this.solicitudService.componentesInvisibles.find(function (params) {
            return params.codigo === id && params.estado === 'I';
          })) {
            return false;
          } else {
            return true;
          }
        }
      }, {
        key: "ngAfterViewChecked",
        value: function ngAfterViewChecked() {
          /*if (this.solicitudService.asegurado.instancia_poliza.id_estado + '' === '59') {
            if (this.poliza.instancia_polizas.length > 0 && (this.index_instancia_poliza || this.index_instancia_poliza===0)) {
              this.poliza.instancia_polizas[this.index_instancia_poliza].id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
              this.instancias[this.index_instancia_poliza].id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
              this.instancias[this.index_instancia_poliza]['Estado'] = this.devuelveValorParametro(this.instancias[this.index_instancia_poliza].id_estado);
            }
          }*/
        }
      }, {
        key: "ValidarComponentesEditable",
        value: function ValidarComponentesEditable(id) {
          if (this.componentesEditables.find(function (params) {
            return params.codigo === id && params.estado === 'NE';
          })) {
            return true;
          } else {
            return false;
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this67 = this;

          this.solicitudService.constructComponent(function () {
            _this67.breadcrumbService.setItems([{
              label: _this67.solicitudService.ruta
            }]);

            _this67.objeto = _this67.solicitudService.parametrosRuteo.parametro_ruteo && _this67.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? _this67.solicitudService.parametrosRuteo.parametro_ruteo.filtros : _this67.objeto;
            _this67.objeto.id_poliza = _this67.id_poliza = _this67.solicitudService.parametrosRuteo.parametro_vista ? _this67.solicitudService.parametrosRuteo.parametro_vista : _this67.id_poliza;
            _this67.solicitudService.product = _this67.solicitudService.ruta;
            _this67.solicitudService.isInAltaSolicitud = false;
            _this67.solicitudService.isLoadingAgain = false;

            _this67.GetAllParametrosByIdDiccionarios(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this67, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee87() {
                return _regeneratorRuntime().wrap(function _callee87$(_context87) {
                  while (1) {
                    switch (_context87.prev = _context87.next) {
                      case 0:
                        if (this.solicitudService.parametrosRuteo.parametro_ruteo && Object.keys(this.solicitudService.parametrosRuteo.parametro_ruteo).length) {
                          this.id_instancias = this.solicitudService.parametrosRuteo.parametro_ruteo;
                          this.listaAll();
                        } else {
                          this.PanelFiltrosColapsed = false;
                        }

                      case 1:
                      case "end":
                        return _context87.stop();
                    }
                  }
                }, _callee87, this);
              }));
            });
          });
        }
      }, {
        key: "getPoliza",
        value: function getPoliza() {
          var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee89() {
            var _this68 = this;

            return _regeneratorRuntime().wrap(function _callee89$(_context89) {
              while (1) {
                switch (_context89.prev = _context89.next) {
                  case 0:
                    _context89.next = 2;
                    return this.polizaService.getPolizaById(this.solicitudService.id_poliza).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this68, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee88() {
                        var response;
                        return _regeneratorRuntime().wrap(function _callee88$(_context88) {
                          while (1) {
                            switch (_context88.prev = _context88.next) {
                              case 0:
                                response = res;
                                this.solicitudService.poliza = response.data;
                                this.solicitudService.poliza.anexo_poliza = this.solicitudService.poliza.anexo_polizas.find(function (param) {
                                  return param.id_tipo == 288;
                                });

                              case 3:
                              case "end":
                                return _context88.stop();
                            }
                          }
                        }, _callee88, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this68.router.navigate(['login']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 2:
                  case "end":
                    return _context89.stop();
                }
              }
            }, _callee89, this);
          }));
        }
      }, {
        key: "listaAll",
        value: function listaAll() {
          var _this69 = this;

          var dt = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
          setTimeout(function () {
            if ($(window).width() < 720) {
              $('.ui-table').css('overflow', 'scroll');
              $('.ui-table table').css('width', '400%');
            }
          }, 2000);

          if (dt) {
            dt.reset();
          }

          this.instancias = [];

          if (this.objeto && this.objeto.id_instancia_poliza) {
            this.objeto = new _src_core_modelos_componente__WEBPACK_IMPORTED_MODULE_10__["Filtro"]();
            this.objeto.id_instancia_poliza = this.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? this.solicitudService.parametrosRuteo.parametro_ruteo.filtros.id_instancia_poliza : '';
          } else {
            if (this.solicitudService.parametrosRuteo.parametro_ruteo) {
              this.objeto = this.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? this.solicitudService.parametrosRuteo.parametro_ruteo.filtros : this.objeto;

              if (this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza) {
                this.objeto = new _src_core_modelos_componente__WEBPACK_IMPORTED_MODULE_10__["Filtro"]();
                this.objeto.id_instancia_poliza = this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza;
              } else {
                this.objeto.id_instancia_poliza = null;
              }
            }
          }

          if (!this.objeto.agencia) {
            this.objeto.agencia_required = false;
          }

          if (!this.objeto.sucursal_required) {
            this.objeto.sucursal_required = false;
          }

          this.objeto.id_poliza = this.objeto.id_poliza ? this.objeto.id_poliza : this.id_poliza;
          this.solicitudService.isLoadingAgain = true;
          this.instanciaPolizaService.GetAllByParametrosEcoAccidentes(this.objeto).subscribe(function (res) {
            var response = res;
            _this69.totalRows = response.recordsTotal ? response.recordsTotal : 0;
            _this69.objeto.first = response.recordsOffset ? response.recordsOffset : 0;
            _this69.objeto.rows = response.recordsFiltered ? _this69.totalRows >= response.recordsFiltered ? response.recordsFiltered : _this69.totalRows : _this69.totalRows > 10 ? 10 : _this69.totalRows;
            _this69.instancias_polizas = response.data;
            _this69.instancias_polizas = response.data;

            if (_this69.instancias_polizas.length >= 300) {
              _this69.showMoreResultsMessage = true;
            } else {
              _this69.showMoreResultsMessage = false;
            }

            _this69.PanelFiltrosColapsed = true;
            var lastEmptyRows = _this69.totalRows % _this69.objeto.rows - _this69.objeto.rows;
            lastEmptyRows = lastEmptyRows < 0 ? lastEmptyRows * -1 : lastEmptyRows;
            var lastFilledRows = _this69.objeto.rows - lastEmptyRows;
            var rowsAfter = _this69.totalRows - lastFilledRows;
            rowsAfter = rowsAfter == _this69.totalRows ? rowsAfter - _this69.objeto.rows : rowsAfter;
            _this69.instancias_polizas = _this69.getCars(_this69.instancias_polizas);

            for (var i = 0; i < rowsAfter; i++) {
              _this69.instancias_polizas.push(new _src_core_modelos_instancia_poliza__WEBPACK_IMPORTED_MODULE_9__["Instancia_poliza"]());

              var rowNumber = i + (_this69.objeto.first + 1);
              _this69.instancias_polizas[i].Nro = rowNumber > _this69.totalRows ? null : rowNumber;
            }

            if (_this69.totalRows - _this69.rowsPerPage == lastFilledRows) {
              _this69.rowsPerPage = _this69.objeto.first + lastFilledRows;
            } else {
              _this69.rowsPerPage = _this69.objeto.first + _this69.objeto.rows;
            }

            if (_this69.totalRows - _this69.objeto.first == lastFilledRows) {
              _this69.numRecords = lastFilledRows;
            } else {
              _this69.numRecords = _this69.objeto.rows;
            }

            if (_this69.instancias_polizas && _this69.instancias_polizas.length) {// if (this.instancias_polizas.length >= 300 && !this.usuarioLogin.rol.find(param => param.id == 5)) {
              //     this.displayExedioResultados = true;
              // }
            } else {
              _this69.service.add({
                key: 'tst',
                severity: 'warn',
                summary: 'Advertencia',
                detail: 'No se encontro ningun registro'
              });
            }

            _this69.solicitudService.isLoadingAgain = false;

            if (typeof callback == 'function') {
              callback();
            }
          }, function (err) {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
              _this69.router.navigate(['login']);
            } else {
              console.log(err);
            }
          });
        }
      }, {
        key: "onPageAction",
        value: function onPageAction(event, dt) {
          console.log(event);
          this.objeto.first = event.first;
          this.objeto.rows = event.rows;
          this.listaAll(dt);
        }
      }, {
        key: "onSearchAction",
        value: function onSearchAction(event, dt) {
          this.objeto.first = 0;
          this.objeto.rows = event.rows;
          this.listaAll(dt);
        }
      }, {
        key: "filterAgencies",
        value: function filterAgencies(event) {
          if (event.value) {
            var value = event.value;
            var sucursalPadre = this.paramSucursales.find(function (param) {
              return param.parametro_cod == value;
            });

            if (sucursalPadre) {
              var filteredAgencias = this.paramAgencias.filter(function (param) {
                return param.id_padre + '' == sucursalPadre.id + '';
              });
              this.Agencias = filteredAgencias.map(function (param) {
                return {
                  value: param.parametro_cod,
                  label: param.parametro_descripcion
                };
              });
              this.Agencias.unshift({
                label: "Seleccione Agencia",
                value: null
              });
            }
          }
        }
      }, {
        key: "listaAllByIds",
        value: function listaAllByIds() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee90() {
            var _this70 = this;

            return _regeneratorRuntime().wrap(function _callee90$(_context90) {
              while (1) {
                switch (_context90.prev = _context90.next) {
                  case 0:
                    this.instanciaPolizaService.GetAllByIdsEcoAccidentes(this.id_instancias).subscribe(function (res) {
                      var response = res;
                      _this70.PanelFiltrosColapsed = true;
                      _this70.instancias_polizas = response.data;
                      _this70.totalRows = response.recordsTotal ? response.recordsTotal : 0;
                      _this70.objeto.first = response.recordsOffset ? response.recordsOffset : 0;
                      _this70.objeto.rows = response.recordsFiltered ? _this70.totalRows >= response.recordsFiltered ? response.recordsFiltered : _this70.totalRows : _this70.totalRows > 10 ? 10 : _this70.totalRows;
                      _this70.instancias_polizas = response.data;

                      if (_this70.instancias_polizas.length >= 300) {
                        _this70.showMoreResultsMessage = true;
                      } else {
                        _this70.showMoreResultsMessage = false;
                      }

                      _this70.instancias_polizas = _this70.getCars(_this70.instancias_polizas);
                      var lastEmptyRows = _this70.totalRows % _this70.objeto.rows - _this70.objeto.rows;
                      lastEmptyRows = lastEmptyRows < 0 ? lastEmptyRows * -1 : lastEmptyRows;
                      var lastFilledRows = _this70.objeto.rows - lastEmptyRows;
                      var rowsAfter = _this70.totalRows - lastFilledRows;
                      rowsAfter = rowsAfter == _this70.totalRows ? rowsAfter - _this70.objeto.rows : rowsAfter;

                      for (var i = 0; i < rowsAfter; i++) {
                        _this70.instancias_polizas.push(new _src_core_modelos_instancia_poliza__WEBPACK_IMPORTED_MODULE_9__["Instancia_poliza"]());

                        var rowNumber = i + (_this70.objeto.first + 1);
                        _this70.instancias_polizas[i].Nro = rowNumber > _this70.totalRows ? null : rowNumber;
                      }

                      if (_this70.totalRows - _this70.rowsPerPage == lastFilledRows) {
                        _this70.rowsPerPage = _this70.objeto.first + lastFilledRows;
                      } else {
                        _this70.rowsPerPage = _this70.objeto.first + _this70.objeto.rows;
                      }

                      if (_this70.totalRows - _this70.objeto.first == lastFilledRows) {
                        _this70.numRecords = lastFilledRows;
                      } else {
                        _this70.numRecords = _this70.objeto.rows;
                      }

                      if (_this70.instancias_polizas && _this70.instancias_polizas.length) {
                        if (_this70.instancias_polizas.length >= 300 && !_this70.solicitudService.userInfo.usuarioRoles.find(function (param) {
                          return param.id == 5;
                        })) {
                          _this70.displayExedioResultados = true;
                        }
                      } else {
                        _this70.service.add({
                          key: 'tst',
                          severity: 'warn',
                          summary: 'Advertencia',
                          detail: 'No se encontro ningun registro'
                        });

                        _this70.poliza = new _src_core_modelos_poliza__WEBPACK_IMPORTED_MODULE_7__["Poliza"]();
                      }
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this70.router.navigate(['login']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 1:
                  case "end":
                    return _context90.stop();
                }
              }
            }, _callee90, this);
          }));
        }
      }, {
        key: "devuelveValorParametro",
        value: function devuelveValorParametro(id) {
          var parametro = this.parametros.find(function (parametro) {
            return parametro.id === id;
          });

          if (parametro) {
            return parametro.parametro_descripcion;
          } else {
            return '';
          }
        }
      }, {
        key: "devuelveValorParametroByCodAbreviacion",
        value: function devuelveValorParametroByCodAbreviacion(id, idDiccionario) {
          var parametro = this.parametros.find(function (parametro) {
            return parametro.parametro_cod == id && parametro.diccionario_id == idDiccionario;
          });

          if (parametro) {
            return parametro.parametro_abreviacion;
          } else {
            return '';
          }
        }
      }, {
        key: "devuelveValorParametroByCod",
        value: function devuelveValorParametroByCod(id) {
          var parametro = this.parametros.find(function (parametro) {
            return parametro.parametro_cod === id;
          });

          if (parametro) {
            return parametro.parametro_descripcion;
          } else {
            return '';
          }
        }
      }, {
        key: "devuelveValorAgenciaSucursal",
        value: function devuelveValorAgenciaSucursal(agencia) {
          if (agencia) {
            var parametro = this.parametros.find(function (parametro) {
              return parametro.parametro_cod === agencia.valor;
            });

            if (parametro) {
              return parametro.parametro_descripcion;
            } else {
              return '';
            }
          } else {
            return '';
          }
        }
      }, {
        key: "GetAllParametrosByIdDiccionarios",
        value: function GetAllParametrosByIdDiccionarios(callback) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee92() {
            var _this71 = this;

            var ids, idPolizas;
            return _regeneratorRuntime().wrap(function _callee92$(_context92) {
              while (1) {
                switch (_context92.prev = _context92.next) {
                  case 0:
                    //ids ponemos los id de los diccionarios que necesitemos
                    ids = [11, 17, 1, 18, 38, 40, 54]; // this.solicitudService.isLoadingAgain = true;

                    idPolizas = [10, 12];
                    _context92.next = 4;
                    return this.parametrosService.GetAllParametrosByIdDiccionarios(ids).subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this71, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee91() {
                        var _this72 = this;

                        var response;
                        return _regeneratorRuntime().wrap(function _callee91$(_context91) {
                          while (1) {
                            switch (_context91.prev = _context91.next) {
                              case 0:
                                response = res;
                                this.parametros = response.data;
                                this.ProcedenciaCI.push({
                                  label: "Seleccione Procedencia",
                                  value: null
                                });
                                this.parametros.filter(function (param) {
                                  return param.diccionario_id + '' === "18";
                                }).forEach(function (element) {
                                  _this72.ProcedenciaCI.push({
                                    label: element.parametro_descripcion,
                                    value: element.parametro_cod
                                  });
                                });
                                this.FiltrosFechas.push({
                                  label: "Seleccione un tipo de fecha",
                                  value: null
                                });
                                this.parametros.filter(function (param) {
                                  return param.diccionario_id + '' == "54" && param.id != 287;
                                }).forEach(function (element) {
                                  _this72.FiltrosFechas.push({
                                    label: element.parametro_descripcion,
                                    value: element.parametro_cod
                                  });
                                });
                                this.FiltrosFechas.push({
                                  label: 'Fecha Inicio Vigencia Certificado',
                                  value: 'fecha_inicio_cert'
                                });
                                this.FiltrosFechas.push({
                                  label: 'Fecha Fin Vigencia Certificado',
                                  value: 'fecha_fin_cert'
                                });
                                this.Agencias.push({
                                  label: "Seleccione Agencia",
                                  value: null
                                });
                                this.parametros.filter(function (param) {
                                  return param.diccionario_id + '' === "40";
                                }).forEach(function (element) {
                                  _this72.Agencias.push({
                                    label: element.parametro_descripcion,
                                    value: element.parametro_cod
                                  });

                                  _this72.paramAgencias.push(element);
                                });
                                this.Sucursales.push({
                                  label: "Seleccione Sucursal",
                                  value: null
                                });
                                this.parametros.filter(function (param) {
                                  return param.diccionario_id + '' === "38";
                                }).forEach(function (element) {
                                  _this72.Sucursales.push({
                                    label: element.parametro_descripcion,
                                    value: element.parametro_cod
                                  });

                                  _this72.paramSucursales.push(element);
                                });
                                this.Estados.push({
                                  label: "Seleccione Estado",
                                  value: null
                                });
                                this.parametros.filter(function (param) {
                                  return param.diccionario_id + '' === "11";
                                }).forEach(function (element) {
                                  _this72.Estados.push({
                                    label: element.parametro_descripcion,
                                    value: element.id
                                  });
                                });
                                this.Polizas.push({
                                  label: "Seleccione una Póliza",
                                  value: null
                                });
                                this.polizaService.getPolizaByIds(idPolizas).subscribe(function (res) {
                                  var response = res;

                                  if (Array.isArray(response.data)) {
                                    _this72.polizas = response.data;

                                    for (var i = 0; i < _this72.polizas.length; i++) {
                                      var poliza = _this72.polizas[i];

                                      _this72.Polizas.push({
                                        label: poliza.descripcion,
                                        value: poliza.id
                                      });
                                    }
                                  }
                                });

                                if (this.solicitudService.userInfo) {
                                  if (this.solicitudService.userInfo.usuario_banco) {
                                    if (this.solicitudService.userInfo.usuario_banco.us_sucursal) {
                                      this.objeto.sucursal = this.solicitudService.userInfo.usuario_banco.us_sucursal + '';
                                    }

                                    if (this.solicitudService.userInfo.usuario_banco.us_oficina) {
                                      this.objeto.agencia = this.solicitudService.userInfo.usuario_banco.us_oficina + '';
                                    }

                                    if (this.solicitudService.userInfo.usuarioRoles.find(function (param) {
                                      return param.id == 5;
                                    })) {
                                      this.objeto.usuario_login = this.solicitudService.userInfo.usuario_login + '';
                                    }
                                  }
                                } else {
                                  this.router.navigate(['login']);
                                }

                                if (!(typeof callback == 'function')) {
                                  _context91.next = 20;
                                  break;
                                }

                                _context91.next = 20;
                                return callback();

                              case 20:
                              case "end":
                                return _context91.stop();
                            }
                          }
                        }, _callee91, this);
                      }));
                    }, function (err) {
                      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        _this71.router.navigate(['login']);
                      } else {
                        console.log(err);
                      }
                    });

                  case 4:
                  case "end":
                    return _context92.stop();
                }
              }
            }, _callee92, this);
          }));
        }
      }, {
        key: "listaAllById",
        value: function listaAllById() {}
      }, {
        key: "EditarPoliza",
        value: function EditarPoliza(id_poliza, id) {
          this.btnEditar = true;
          this.btnVer1 = true;
          this.objeto.id_poliza = id_poliza;

          if (parseInt(id_poliza + '') == 10) {
            this.menuService.activaRuteoMenu(67, 10, {
              id_instancia_poliza: id,
              filtros: this.objeto
            }, 'crediticios/AltaEcoAccidente', this.solicitudService.componentesInvisibles);
          } else if (parseInt(id_poliza + '') == 12) {
            this.menuService.activaRuteoMenu(76, 12, {
              id_instancia_poliza: id,
              filtros: this.objeto
            }, 'crediticios/AltaEcoResguardo', this.solicitudService.componentesInvisibles);
          } //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);

        }
      }, {
        key: "VerPoliza",
        value: function VerPoliza(id_poliza, id) {
          this.btnVer2 = true;
          this.objeto.id_poliza = id_poliza;

          if (parseInt(id_poliza + '') == 10) {
            this.menuService.activaRuteoMenu(67, 10, {
              id_instancia_poliza: id,
              filtros: this.objeto
            }, 'crediticios/AltaEcoAccidente', this.solicitudService.componentesInvisibles);
          } else if (parseInt(id_poliza + '') == 12) {
            this.menuService.activaRuteoMenu(76, 12, {
              id_instancia_poliza: id,
              filtros: this.objeto
            }, 'crediticios/AltaEcoResguardo', this.solicitudService.componentesInvisibles);
          } //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);

        }
      }, {
        key: "ValidarEstados",
        value: function ValidarEstados(id_estado, estados) {
          if (estados.find(function (param) {
            return param == '*';
          })) {
            return true;
          } else {
            if (estados.includes(parseInt(id_estado + ''))) {
              return true;
            } else {
              return false;
            }
          }
        }
      }, {
        key: "verifyRol",
        value: function verifyRol(estadosAvailable, rolsAvailable, instanciaPoliza) {
          var includesRol = false;

          if (!this.solicitudService.userInfo) {
            this.solicitudService.userInfo = this.sessionStorageService.getItemSync('userInfo');
          }

          if (this.solicitudService.userInfo) {
            for (var i = 0; i < this.solicitudService.userInfo.usuarioRoles.length; i++) {
              var rol = this.solicitudService.userInfo.usuarioRoles[i];

              if (rolsAvailable.find(function (param) {
                return param == '*';
              })) {
                includesRol = true;
                break;
              } else {
                if (rolsAvailable.includes(parseInt(rol.id + ''))) {
                  includesRol = true;
                }
              }
            }

            if (this.ValidarEstados(instanciaPoliza.id_estado, estadosAvailable) && includesRol) {
              return true;
            }

            return false;
          } else {
            this.router.navigate(['login']);
          }
        }
      }, {
        key: "ValidarPlanPago",
        value: function ValidarPlanPago() {
          if (this.solicitudService.asegurado.instancia_poliza.planPago && Object.keys(this.solicitudService.asegurado.instancia_poliza.planPago)) {
            return true;
          } else {
            return false;
          }
        }
      }, {
        key: "buscarObjetoAsegurado",
        value: function buscarObjetoAsegurado(id_instancia_poliza, index) {
          var _this73 = this;

          this.index_instancia_poliza = index;
          var id_objeto = 0;

          if (this.id_poliza + '' === '6') {
            id_objeto = 18;
          }

          if (this.id_poliza + '' === '7') {
            id_objeto = 20;
          }

          if (this.id_poliza + '' === '10') {
            id_objeto = 26;
          }

          if (this.id_poliza + '' === '12') {
            id_objeto = 28;
          }

          this.btnEmitir = true;
          this.solicitudService.isLoadingAgain = true;
          this.personaService.findPersonaSolicitudByIdInstanciaPoliza(id_objeto, id_instancia_poliza).subscribe(function (res) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this73, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee93() {
              var _this74 = this;

              var response;
              return _regeneratorRuntime().wrap(function _callee93$(_context93) {
                while (1) {
                  switch (_context93.prev = _context93.next) {
                    case 0:
                      response = res;
                      this.solicitudService.asegurado = response.data;
                      _context93.next = 4;
                      return this.componenteActualizarSolicitud.cambiarSolicitudEstado().then(function (respo) {
                        /*let ins = this.instancias_polizas.find(param => param.id + '' === id_instancia_poliza + '');
                        if (ins) {
                            ins.id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
                        }*/
                        _this74.solicitudService.isLoadingAgain = false;
                      });

                    case 4:
                      this.instancias_polizas = [];
                      this.btnEmitir = false;
                      this.instancias = [];

                    case 7:
                    case "end":
                      return _context93.stop();
                  }
                }
              }, _callee93, this);
            }));
          }, function (err) {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
              _this73.router.navigate(['login']);
            } else {
              console.log(err);
            }
          });
        }
      }, {
        key: "limpiar",
        value: function limpiar() {
          this.instancias_polizas = [];
          this.instancias = [];
          this.poliza = new _src_core_modelos_poliza__WEBPACK_IMPORTED_MODULE_7__["Poliza"]();
          this.objeto = new _src_core_modelos_componente__WEBPACK_IMPORTED_MODULE_10__["Filtro"]();
          this.solicitudService.parametrosRuteo.parametro_ruteo = null;
        }
      }, {
        key: "obtenerUltimoDocumento",
        value: function obtenerUltimoDocumento(documentos) {
          var nro_solicitud = "";
          documentos.forEach(function (documento) {
            nro_solicitud = documento.nro_documento;
          });
          return nro_solicitud;
        }
      }, {
        key: "ImprimirSolicitud",
        value: function ImprimirSolicitud(id) {
          var _this75 = this;

          var nombre_archivo = 'SoliEcoAgui' + id;
          this.solicitudService.isLoadingAgain = true;
          this.reporteService.ReporteSolicitud(id, nombre_archivo).subscribe(function (res) {
            var response = res;
            _this75.solicitudService.isLoadingAgain = false;

            if (response.status == 'ERROR') {
              _this75.service.add({
                key: 'tst',
                severity: 'warn',
                summary: 'Advertencia',
                detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
              });
            } else {
              _this75.reporteService.cargarPagina(nombre_archivo);
            }
          }, function (err) {
            _this75.solicitudService.isLoadingAgain = false;

            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
              _this75.router.navigate(['login']);
            } else {
              console.log(err);
            }
          });
        }
      }, {
        key: "ImprimirCertificado",
        value: function ImprimirCertificado(id) {
          var _this76 = this;

          var nombre_archivo = 'CertEcoAgui' + id;
          this.solicitudService.isLoadingAgain = true;
          this.reporteService.certificado_ecoaguinaldo(id, nombre_archivo).subscribe(function (res) {
            var response = res;
            _this76.solicitudService.isLoadingAgain = false;

            if (response.status == 'ERROR') {
              _this76.service.add({
                key: 'tst',
                severity: 'warn',
                summary: 'Advertencia',
                detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
              });
            } else {
              _this76.reporteService.cargarPagina(nombre_archivo);
            }
          }, function (err) {
            _this76.solicitudService.isLoadingAgain = false;

            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
              _this76.router.navigate(['login']);
            } else {
              console.log(err);
            }
          });
        }
      }, {
        key: "exportExcel",
        value: function exportExcel(dt) {
          var _this77 = this;

          var accept = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

          if (!accept && this.totalRows > 300) {
            this.displayExedioResultados = true;
          }

          if (accept || this.totalRows <= 300) {
            this.objeto.rows = 300;
            this.objeto.first = 0;
            this.listaAll(dt, function () {
              var instan = null;

              if (dt.filteredValue !== null) {
                instan = dt.filteredValue;
              } else {
                instan = _this77.instancias_polizas;
              }

              var instancia_poliza_excel = _this77.getCarsExcel(instan);

              var worksheet = xlsx__WEBPACK_IMPORTED_MODULE_6__["utils"].json_to_sheet(instancia_poliza_excel);
              var workbook = {
                Sheets: {
                  'data': worksheet
                },
                SheetNames: ['data']
              };
              var excelBuffer = xlsx__WEBPACK_IMPORTED_MODULE_6__["write"](workbook, {
                bookType: 'xlsx',
                type: 'array'
              });
              _this77.solicitudService.isLoadingAgain = false;

              _this77.saveAsExcelFile(excelBuffer, "primengTable");
            });
          }
        }
      }, {
        key: "saveAsExcelFile",
        value: function saveAsExcelFile(buffer, fileName) {
          var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
          var EXCEL_EXTENSION = '.xlsx';
          var data = new Blob([buffer], {
            type: EXCEL_TYPE
          });
          file_saver__WEBPACK_IMPORTED_MODULE_5__["saveAs"](data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
        }
      }, {
        key: "getCars",
        value: function getCars(ins_poli) {
          var _this78 = this;

          var instancias = [];
          var c = 0;

          var _loop2 = function _loop2(i) {
            c++;
            var data = ins_poli[i];
            var objeto = data;
            var instancia = data;
            var docSolicitud = void 0,
                docCertificado = void 0;

            switch (data.id_poliza + '') {
              case '6':
                if (instancia.solicitudes && instancia.solicitudes.length) {
                  docSolicitud = instancia.solicitudes.find(function (param) {
                    return param.id_documento == 16;
                  });
                }

                if (instancia.certificados && instancia.certificados.length) {
                  docCertificado = instancia.certificados.find(function (param) {
                    return param.id_documento == 18;
                  });
                }

                break;

              case '7':
                if (instancia.solicitudes && instancia.solicitudes.length) {
                  docSolicitud = instancia.solicitudes.find(function (param) {
                    return param.id_documento == 19;
                  });
                }

                if (instancia.certificados && instancia.certificados.length) {
                  docCertificado = instancia.certificados.find(function (param) {
                    return param.id_documento == 21;
                  });
                }

                break;

              case '10':
                if (instancia.solicitudes && instancia.solicitudes.length) {
                  docSolicitud = instancia.solicitudes.find(function (param) {
                    return param.id_documento == 28;
                  });
                }

                if (instancia.certificados && instancia.certificados.length) {
                  docCertificado = instancia.certificados.find(function (param) {
                    return param.id_documento == 30;
                  });
                }

                break;

              case '12':
                if (instancia.solicitudes && instancia.solicitudes.length) {
                  docSolicitud = instancia.solicitudes.find(function (param) {
                    return param.id_documento == 31;
                  });
                }

                if (instancia.certificados && instancia.certificados.length) {
                  docCertificado = instancia.certificados.find(function (param) {
                    return param.id_documento == 33;
                  });
                }

                break;
            }

            var poliza = _this78.polizas.find(function (param) {
              return param.id == instancia.id_poliza;
            });

            objeto['Nro'] = c;
            objeto['id'] = instancia.id;
            objeto['id_estado'] = instancia.id_estado;
            objeto['id_poliza'] = instancia.id_poliza;
            objeto['Nro_Sol_Sci'] = instancia.nro_solicitud_sci ? instancia.nro_solicitud_sci.valor ? instancia.nro_solicitud_sci.valor : '' : '';
            objeto['Sucursal'] = _this78.devuelveValorAgenciaSucursal(instancia.sucursal);
            objeto['Agencia'] = _this78.devuelveValorAgenciaSucursal(instancia.agencia);
            objeto['Nro_Sol'] = instancia.solicitudes && instancia.solicitudes.length ? instancia.solicitudes[0].nro_documento : '';
            objeto['Id'] = instancia.id;
            objeto['Nro_Certificado'] = instancia.certificados && instancia.certificados.length ? _this78.obtenerUltimoDocumento(instancia.certificados) : '';
            objeto['Estado'] = _this78.devuelveValorParametro(instancia.id_estado);
            objeto['Fecha_Registro'] = _this78.util.formatoFecha(instancia.fecha_registro + '');
            objeto['Fecha_Inicio_Cert'] = docCertificado ? _this78.util.formatoFecha(docCertificado.fecha_inicio_vigencia + '') : '';
            objeto['Fecha_Fin_Cert'] = docCertificado ? _this78.util.formatoFecha(docCertificado.fecha_fin_vigencia + '') : ''; // objeto['Fecha_Solicitud'] = docSolicitud ? this.util.formatoFecha(docSolicitud.fecha_emision + '') : null;
            // objeto['Fecha_Emision'] = docCertificado ? this.util.formatoFecha(docCertificado.fecha_emision + '') : null;

            objeto['Asegurado'] = instancia.asegurados[0].entidad.persona.persona_primer_nombre + ' ' + instancia.asegurados[0].entidad.persona.persona_primer_apellido + ' ' + instancia.asegurados[0].entidad.persona.persona_segundo_apellido;
            objeto['CI'] = instancia.asegurados[0].entidad.persona.persona_doc_id + ' ' + _this78.devuelveValorParametroByCodAbreviacion(instancia.asegurados[0].entidad.persona.persona_doc_id_ext, 18);
            objeto['oficial'] = instancia.usuario.usuario_login;
            objeto['producto'] = instancia.poliza ? instancia.poliza.descripcion : '';
            instancias.push(objeto);
          };

          for (var i = 0; i < ins_poli.length; i++) {
            _loop2(i);
          }

          return instancias;
        }
      }, {
        key: "getCarsExcel",
        value: function getCarsExcel(ins_poli) {
          var instancias = [];
          var c = 0;

          var _iterator3 = _createForOfIteratorHelper(ins_poli),
              _step3;

          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var instancia = _step3.value;
              c++;
              var objeto = {};
              var docSolicitud = void 0,
                  docCertificado = void 0;

              switch (this.id_poliza + '') {
                case '6':
                  docSolicitud = instancia.Fecha_Solicitud;
                  docCertificado = instancia.Fecha_Emision;
                  break;

                case '7':
                  docSolicitud = instancia.Fecha_Solicitud;
                  docCertificado = instancia.Fecha_Emision;
                  break;
              }

              objeto['NRO'] = instancia['Nro'];
              objeto['ID'] = instancia['Id'];
              objeto['SUCURSAL'] = instancia['Sucursal'];
              objeto['AGENCIA'] = instancia['Agencia'];
              objeto['NRO_CERTIFICADO'] = instancia['Nro_Certificado'];
              objeto['ESTADO'] = instancia['Estado'];
              objeto['FECHA REGISTRO'] = instancia['Fecha_Registro'];
              objeto['FECHA INICIO CERTIFICADO'] = instancia['Fecha_Inicio_Cert'];
              objeto['FECHA FIN CERTIFICADO'] = instancia['Fecha_Fin_Cert']; // objeto['FECHA SOLICITUD'] = instancia['Fecha_Solicitud'];
              // objeto['FECHA EMISION'] = instancia['Fecha_Emision'];

              objeto['ASEGURADO'] = instancia['Asegurado'];
              objeto['USUARIO'] = instancia['oficial'];
              objeto['POLIZA'] = instancia['producto'];
              instancias.push(objeto);
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }

          return instancias;
        }
      }, {
        key: "customSort",
        value: function customSort(event) {
          event.data.sort(function (data1, data2) {
            var value1 = data1[event.field];
            var value2 = data2[event.field];
            var result = null;
            if (value1 == null && value2 != null) result = -1;else if (value1 != null && value2 == null) result = 1;else if (value1 == null && value2 == null) result = 0;else if (typeof value1 === 'string' && typeof value2 === 'string') result = value1.localeCompare(value2);else result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;
            return event.order * result;
          });
        } // exportPdf() {
        //       const doc = new jsPDF.default(0, 0);
        //       doc.autoTable(this.exportColumns, this.poliza.instancia_polizas);
        //       doc.save('primengTable.pdf');
        // }

      }, {
        key: "ImprimirSolicitudEcoAccidentes",
        value: function ImprimirSolicitudEcoAccidentes(id) {
          var _this79 = this;

          var nombre_archivo = 'SoliEcoAccidentes' + id;
          this.solicitudService.isLoadingAgain = true;

          if (this.id_poliza + '' === '10') {
            this.reporteService.ReporteSolicitudEcoVida(id, nombre_archivo).subscribe(function (res) {
              var response = res;
              _this79.solicitudService.isLoadingAgain = false;

              if (response.status == 'ERROR') {
                _this79.service.add({
                  key: 'tst',
                  severity: 'warn',
                  summary: 'Advertencia',
                  detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
              } else {
                _this79.reporteService.cargarPagina(nombre_archivo);
              }
            }, function (err) {
              _this79.solicitudService.isLoadingAgain = false;
              console.error("ERROR llamando servicio ImprimirSolicitud:", err);
            });
          }
        }
      }, {
        key: "ImprimirCertificadoEcoAccidentes",
        value: function ImprimirCertificadoEcoAccidentes(id) {
          var _this80 = this;

          var nombre_archivo = 'CertEcoAccidentes' + id;
          this.solicitudService.isLoadingAgain = true;

          if (this.id_poliza + '' === '10') {
            this.reporteService.ReporteCertificadoEcoVida(id, nombre_archivo).subscribe(function (res) {
              var response = res;
              _this80.solicitudService.isLoadingAgain = false;

              if (response.status == 'ERROR') {
                _this80.service.add({
                  key: 'tst',
                  severity: 'warn',
                  summary: 'Advertencia',
                  detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
              } else {
                _this80.reporteService.cargarPagina(nombre_archivo);
              }
            }, function (err) {
              _this80.solicitudService.isLoadingAgain = false;
              console.error("ERROR llamando servicio ImprimirCertificadoEcoAccidentes:", err);
            });
          }
        }
      }, {
        key: "generarplanpago",
        value: function generarplanpago(id) {
          var pp = new _src_core_modelos_plan_pago__WEBPACK_IMPORTED_MODULE_8__["Plan_pago"]();
          pp.id_instancia_poliza = id;
          pp.total_prima = 192;
          pp.interes = 0;
          pp.plazo_anos = 1;
          pp.periodicidad_anual = 12;
          pp.prepagable_postpagable = 1;
          pp.fecha_inicio = new Date();
          pp.adicionado_por = '2';
          pp.modificado_por = '2';
          this.planPagoService.GenerarPlanPagos(pp).subscribe(function (res) {
            var response = res;

            if (response.status == 'ERROR') {} else {}
          }, function (err) {
            return console.error("ERROR llamando servicio plan de pagos:", err);
          });
        }
      }, {
        key: "GeneraAllPlanPagos",
        value: function GeneraAllPlanPagos(id) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee95() {
            var _this81 = this;

            return _regeneratorRuntime().wrap(function _callee95$(_context95) {
              while (1) {
                switch (_context95.prev = _context95.next) {
                  case 0:
                    _context95.next = 2;
                    return this.planPagoService.listaTodosSinPlanPagos().subscribe(function (res) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this81, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee94() {
                        var response, i, pp;
                        return _regeneratorRuntime().wrap(function _callee94$(_context94) {
                          while (1) {
                            switch (_context94.prev = _context94.next) {
                              case 0:
                                response = res;
                                i = 0;

                              case 2:
                                if (!(i < response.data.length)) {
                                  _context94.next = 18;
                                  break;
                                }

                                pp = new _src_core_modelos_plan_pago__WEBPACK_IMPORTED_MODULE_8__["Plan_pago"]();
                                pp.id_instancia_poliza = response.data[i].id;
                                pp.total_prima = parseInt(response.data[i].monto) * parseInt(response.data[i].plazo);
                                pp.interes = 0;
                                pp.plazo_anos = 1;
                                pp.periodicidad_anual = parseInt(response.data[i].monto);
                                pp.prepagable_postpagable = 1;
                                pp.fecha_inicio = response.data[i].fecha_emision;
                                pp.adicionado_por = '4';
                                pp.modificado_por = '4';
                                _context94.next = 15;
                                return this.planPagoService.GenerarPlanPagos2(pp).then(function (res) {
                                  var response = res;

                                  if (response.status == 'ERROR') {} else {}
                                }, function (err) {
                                  return console.error("ERROR llamando servicio plan de pagos:", err);
                                });

                              case 15:
                                i++;
                                _context94.next = 2;
                                break;

                              case 18:
                              case "end":
                                return _context94.stop();
                            }
                          }
                        }, _callee94, this);
                      }));
                    }, function (err) {
                      return console.error("ERROR llamando servicio plan de pagos:", err);
                    });

                  case 2:
                  case "end":
                    return _context95.stop();
                }
              }
            }, _callee95, this);
          }));
        }
      }, {
        key: "verPlanPagos",
        value: function verPlanPagos(ins_pol) {
          this.menuService.activaRuteoMenu('66', null, {
            id_instancia_poliza: ins_pol.id
          });
        }
      }]);

      return GestionEcoAccidentesComponent;
    }();

    GestionEcoAccidentesComponent.ɵfac = function GestionEcoAccidentesComponent_Factory(t) {
      return new (t || GestionEcoAccidentesComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_11__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_parametro_service__WEBPACK_IMPORTED_MODULE_12__["ParametrosService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_instancia_poliza_service__WEBPACK_IMPORTED_MODULE_13__["InstanciaPolizaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_menu_service__WEBPACK_IMPORTED_MODULE_20__["MenuService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_reporte_service__WEBPACK_IMPORTED_MODULE_14__["ReporteService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_persona_service__WEBPACK_IMPORTED_MODULE_19__["PersonaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_poliza_service__WEBPACK_IMPORTED_MODULE_18__["PolizaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_15__["SolicitudService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_sessionStorage_service__WEBPACK_IMPORTED_MODULE_16__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_src_core_servicios_plan_pago_service__WEBPACK_IMPORTED_MODULE_17__["PlanPagoService"]));
    };

    GestionEcoAccidentesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: GestionEcoAccidentesComponent,
      selectors: [["app-gestion-eco-accidentes"]],
      viewQuery: function GestionEcoAccidentesComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.componenteActualizarSolicitud = _t.first);
        }
      },
      features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]])],
      decls: 22,
      vars: 24,
      consts: [["class", "overlay", 4, "ngIf"], ["header", "Se Actualizo Exitosamente", 3, "visible", "modal", "responsive", "minY", "visibleChange"], [1, "ui-g-12", "text-center"], [1, "ui-icon-priority-high", "ui-icon-warn-colibri", 2, "border", "1px solid #4CAF50", "border-radius", "20px", "font-size", "50px", "color", "#4CAF50"], [3, "outerHTML"], [1, "ui-dialog-buttonpane", "ui-helper-clearfix"], ["type", "button", "pButton", "", "icon", "pi pi-check", "label", "Aceptar", 3, "click"], ["toggleable", "true", "collapseIcon", "fa fa-minus", 3, "header", "collapsed", "collapsedChange"], ["class", "form-group", "legend", "Buscar polizas por", "toggleable", "true", 4, "ngIf"], ["class", "form-group", "legend", "Buscar por fecha", "toggleable", "true", 4, "ngIf"], [1, "ui-g", "form-group"], [1, "ui-g-12", "ui-md-12", 2, "text-align", "right"], ["type", "button", "pButton", "", "pTooltip", "Borrar Filtros", "tooltipPosition", "top", "icon", "ui-icon-refresh", 3, "click"], ["type", "button", "pButton", "", "pTooltip", "Buscar", "tooltipPosition", "top", "icon", "pi pi-search", 3, "click"], ["responsiveLayout", "scroll", "resetPageOnSort", "false", "dataKey", "id", 3, "columns", "value", "paginator", "rows", "lazyLoadOnInit", "totalRecords", "showCurrentPageReport", "currentPageReportTemplate", "rowsPerPageOptions", "onPage"], ["dt", ""], ["pTemplate", "caption"], ["pTemplate", "header"], ["pTemplate", "body"], ["key", "tst"], [3, "setAsegurado", 4, "ngIf"], [1, "overlay"], [1, "loading"], ["legend", "Buscar polizas por", "toggleable", "true", 1, "form-group"], [1, "ui-g-12", "ui-md-2"], [1, "md-inputfield", "ui-float-label"], [3, "options", "ngModel", "showClear", "ngModelChange", "onChange"], [1, "md-inputfield"], [3, "options", "ngModel", "showClear", "ngModelChange"], ["type", "text", "pInputText", "", 3, "ngModel", "ngModelChange"], ["type", "number", "pInputText", "", 3, "ngModel", "ngModelChange"], ["legend", "Buscar por fecha", "toggleable", "true", 1, "form-group"], ["selectId", "1", 3, "options", "ngModel", "showClear", "ngModelChange"], ["class", "ui-g-12 ui-md-4", 4, "ngIf"], [1, "ui-g-12", "ui-md-4"], [1, "ui-g-12", "ui-md-6"], [1, "ui-g-12"], ["placeholder", "Desde", 3, "ngModel", "showIcon", "showButtonBar", "locale", "ngModelChange"], ["placeholder", "Hasta", 3, "ngModel", "showIcon", "showButtonBar", "locale", "ngModelChange"], [1, "ui-g"], [1, "ui-g-6", 2, "text-align", "left"], [4, "ngIf"], [1, "ui-g-6", 2, "text-align", "right"], ["type", "button", "pButton", "", "icon", "pi pi-file-excel", "iconPos", "left", "label", "EXCEL", 1, "ui-button-success", 2, "margin-right", "0.5em", 3, "click"], [3, "width", "pSortableColumn", 4, "ngFor", "ngForOf"], [3, "ngSwitch", 4, "ngFor", "ngForOf"], [3, "pSortableColumn"], [2, "font-size", "10px"], ["ariaLabel", "Activate to sort", "ariaLabelDesc", "Activate to sort in descending order", "ariaLabelAsc", "Activate to sort in ascending order", 3, "field"], [3, "ngSwitch"], ["pInputText", "", "type", "text", 3, "input"], ["style", "font-size:10px;", "type", "button", "pButton", "", 3, "disabled", "icon", "title", "click", 4, "ngIf"], ["style", "font-size:10px;", "type", "button", "pButton", "", "icon", "ui-icon-content-copy", "title", "Emitir Certificado", 3, "disabled", "click", 4, "ngIf"], ["style", "font-size:10px;", "type", "button", "pButton", "", "icon", "ui-icon-print", "title", "Imprimir Certificado", 3, "click", 4, "ngIf"], ["style", "font-size:10px;", "type", "button", "pButton", "", "icon", "ui-icon-print", "title", "Imprimir Solicitud", 3, "click", 4, "ngIf"], ["type", "button", "pButton", "", 2, "font-size", "10px", 3, "disabled", "icon", "title", "click"], ["type", "button", "pButton", "", "icon", "ui-icon-content-copy", "title", "Emitir Certificado", 2, "font-size", "10px", 3, "disabled", "click"], ["type", "button", "pButton", "", "icon", "ui-icon-print", "title", "Imprimir Certificado", 2, "font-size", "10px", 3, "click"], ["type", "button", "pButton", "", "icon", "ui-icon-print", "title", "Imprimir Solicitud", 2, "font-size", "10px", 3, "click"], [3, "setAsegurado"], ["componenteActualizarSolicitud", ""]],
      template: function GestionEcoAccidentesComponent_Template(rf, ctx) {
        if (rf & 1) {
          var _r64 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, GestionEcoAccidentesComponent_div_0_Template, 2, 0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-dialog", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function GestionEcoAccidentesComponent_Template_p_dialog_visibleChange_1_listener($event) {
            return ctx.displayExedioResultados = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "i", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "p", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "p-footer");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "button", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionEcoAccidentesComponent_Template_button_click_7_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r64);

            var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](16);

            ctx.displayExedioResultados = false;
            return ctx.exportExcel(_r3, true);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "p-panel", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("collapsedChange", function GestionEcoAccidentesComponent_Template_p_panel_collapsedChange_8_listener($event) {
            return ctx.PanelFiltrosColapsed = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, GestionEcoAccidentesComponent_p_fieldset_9_Template, 57, 19, "p-fieldset", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, GestionEcoAccidentesComponent_p_fieldset_10_Template, 8, 4, "p-fieldset", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "button", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionEcoAccidentesComponent_Template_button_click_13_listener() {
            return ctx.limpiar();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "button", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GestionEcoAccidentesComponent_Template_button_click_14_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r64);

            var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](16);

            return ctx.onSearchAction(ctx.objeto, _r3);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "p-table", 14, 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onPage", function GestionEcoAccidentesComponent_Template_p_table_onPage_15_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r64);

            var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](16);

            return ctx.onPageAction($event, _r3);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, GestionEcoAccidentesComponent_ng_template_17_Template, 6, 1, "ng-template", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, GestionEcoAccidentesComponent_ng_template_18_Template, 4, 2, "ng-template", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, GestionEcoAccidentesComponent_ng_template_19_Template, 33, 28, "ng-template", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](20, "p-toast", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, GestionEcoAccidentesComponent_app_actualizar_solicitud_21_Template, 2, 0, "app-actualizar-solicitud", 20);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.isLoadingAgain);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.displayExedioResultados)("modal", true)("responsive", true)("minY", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("outerHTML", "<br><br>Advertencia: El criterio de b\xFAsqueda introducido ha generado como resultado gran cantidad de registros, <br>por lo que se mostrar\xE1n solo los primeros 300 cuyas fechas de solicitud son las m\xE1s recientes.", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("header", ctx.solicitudService.product);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("collapsed", ctx.PanelFiltrosColapsed);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.showFiltros);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.showFiltros);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("columns", ctx.cols)("value", ctx.instancias_polizas)("paginator", true)("rows", ctx.objeto.rows)("lazyLoadOnInit", true)("totalRecords", ctx.totalRows)("showCurrentPageReport", true)("currentPageReportTemplate", "Mostrando " + ctx.rowsPerPage + " de " + ctx.totalRows + " registros")("rowsPerPageOptions", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](22, _c9));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](23, _c10));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.solicitudService.asegurado);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_24__["NgIf"], primeng__WEBPACK_IMPORTED_MODULE_25__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["Footer"], primeng__WEBPACK_IMPORTED_MODULE_25__["ButtonDirective"], primeng_panel__WEBPACK_IMPORTED_MODULE_26__["Panel"], primeng__WEBPACK_IMPORTED_MODULE_25__["Table"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_toast__WEBPACK_IMPORTED_MODULE_27__["Toast"], primeng__WEBPACK_IMPORTED_MODULE_25__["Fieldset"], primeng__WEBPACK_IMPORTED_MODULE_25__["Dropdown"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["DefaultValueAccessor"], primeng__WEBPACK_IMPORTED_MODULE_25__["InputText"], _angular_forms__WEBPACK_IMPORTED_MODULE_28__["NumberValueAccessor"], primeng_calendar__WEBPACK_IMPORTED_MODULE_29__["Calendar"], _angular_common__WEBPACK_IMPORTED_MODULE_24__["NgForOf"], primeng__WEBPACK_IMPORTED_MODULE_25__["SortableColumn"], primeng__WEBPACK_IMPORTED_MODULE_25__["SortIcon"], _angular_common__WEBPACK_IMPORTED_MODULE_24__["NgSwitch"], _src_core_componentes_actualizar_solicitud_actualizar_solicitud_component__WEBPACK_IMPORTED_MODULE_22__["ActualizarSolicitudComponent"]],
      styles: ["[_nghost-%COMP%]     {\n    .p-datatable-responsive-demo .p-datatable-tbody > tr > td .p-column-title {\n        display: none;\n    }\n}\n\n.ui-table[_ngcontent-%COMP%] {\n    overflow: scroll;\n}\n\n@media screen and (max-width: 40rem) {\n    .ui-table[_ngcontent-%COMP%]   .ui-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]{\n        width: 400%;\n    }\n    [_nghost-%COMP%]     {\n        .p-datatable {\n            &.p-datatable-responsive-demo {\n                .p-datatable-thead > tr > th,\n                .p-datatable-tfoot > tr > td {\n                    display: none !important;\n                }\n\n                .p-datatable-tbody > tr > td {\n                    text-align: left;\n                    display: block;\n                    width: 100%;\n                    float: left;\n                    clear: left;\n                    border: 0 none;\n\n                    .p-column-title {\n                        padding: .4rem;\n                        min-width: 30%;\n                        display: inline-block;\n                        margin: -.4em 1em -.4em -.4rem;\n                        font-weight: bold;\n                    }\n\n                    &:last-child {\n                         border-bottom: 1px solid var(--surface-d);\n                    }\n                }\n            }\n        }\n    }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2NyZWRpdGljaW9zL3NyYy9hcHAvZWNvLWFjY2lkZW50ZXMvZ2VzdGlvbi1lY28tYWNjaWRlbnRlcy9nZXN0aW9uLWVjby1hY2NpZGVudGVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSTtRQUNJLGFBQWE7SUFDakI7QUFDSjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJO1FBQ0ksV0FBVztJQUNmO0lBQ0E7UUFDSTtZQUNJO2dCQUNJOztvQkFFSSx3QkFBd0I7Z0JBQzVCOztnQkFFQTtvQkFDSSxnQkFBZ0I7b0JBQ2hCLGNBQWM7b0JBQ2QsV0FBVztvQkFDWCxXQUFXO29CQUNYLFdBQVc7b0JBQ1gsY0FBYzs7b0JBRWQ7d0JBQ0ksY0FBYzt3QkFDZCxjQUFjO3dCQUNkLHFCQUFxQjt3QkFDckIsOEJBQThCO3dCQUM5QixpQkFBaUI7b0JBQ3JCOztvQkFFQTt5QkFDSyx5Q0FBeUM7b0JBQzlDO2dCQUNKO1lBQ0o7UUFDSjtJQUNKO0FBQ0oiLCJmaWxlIjoicHJvamVjdHMvY3JlZGl0aWNpb3Mvc3JjL2FwcC9lY28tYWNjaWRlbnRlcy9nZXN0aW9uLWVjby1hY2NpZGVudGVzL2dlc3Rpb24tZWNvLWFjY2lkZW50ZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IDo6bmctZGVlcCB7XG4gICAgLnAtZGF0YXRhYmxlLXJlc3BvbnNpdmUtZGVtbyAucC1kYXRhdGFibGUtdGJvZHkgPiB0ciA+IHRkIC5wLWNvbHVtbi10aXRsZSB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxufVxuXG4udWktdGFibGUge1xuICAgIG92ZXJmbG93OiBzY3JvbGw7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQwcmVtKSB7XG4gICAgLnVpLXRhYmxlIC51aS10YWJsZS13cmFwcGVyIHRhYmxle1xuICAgICAgICB3aWR0aDogNDAwJTtcbiAgICB9XG4gICAgOmhvc3QgOjpuZy1kZWVwIHtcbiAgICAgICAgLnAtZGF0YXRhYmxlIHtcbiAgICAgICAgICAgICYucC1kYXRhdGFibGUtcmVzcG9uc2l2ZS1kZW1vIHtcbiAgICAgICAgICAgICAgICAucC1kYXRhdGFibGUtdGhlYWQgPiB0ciA+IHRoLFxuICAgICAgICAgICAgICAgIC5wLWRhdGF0YWJsZS10Zm9vdCA+IHRyID4gdGQge1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLnAtZGF0YXRhYmxlLXRib2R5ID4gdHIgPiB0ZCB7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGNsZWFyOiBsZWZ0O1xuICAgICAgICAgICAgICAgICAgICBib3JkZXI6IDAgbm9uZTtcblxuICAgICAgICAgICAgICAgICAgICAucC1jb2x1bW4tdGl0bGUge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogLjRyZW07XG4gICAgICAgICAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDMwJTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogLS40ZW0gMWVtIC0uNGVtIC0uNHJlbTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgdmFyKC0tc3VyZmFjZS1kKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cblxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](GestionEcoAccidentesComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-gestion-eco-accidentes',
          templateUrl: './gestion-eco-accidentes.component.html',
          styleUrls: ['./gestion-eco-accidentes.component.css'],
          providers: [primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]]
        }]
      }], function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _src_core_servicios_breadcrumb_service__WEBPACK_IMPORTED_MODULE_11__["BreadcrumbService"]
        }, {
          type: _src_core_servicios_parametro_service__WEBPACK_IMPORTED_MODULE_12__["ParametrosService"]
        }, {
          type: _src_core_servicios_instancia_poliza_service__WEBPACK_IMPORTED_MODULE_13__["InstanciaPolizaService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _src_core_servicios_menu_service__WEBPACK_IMPORTED_MODULE_20__["MenuService"]
        }, {
          type: _src_core_servicios_reporte_service__WEBPACK_IMPORTED_MODULE_14__["ReporteService"]
        }, {
          type: primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]
        }, {
          type: _src_core_servicios_persona_service__WEBPACK_IMPORTED_MODULE_19__["PersonaService"]
        }, {
          type: _src_core_servicios_poliza_service__WEBPACK_IMPORTED_MODULE_18__["PolizaService"]
        }, {
          type: _src_core_servicios_solicitud_service__WEBPACK_IMPORTED_MODULE_15__["SolicitudService"]
        }, {
          type: _src_core_servicios_sessionStorage_service__WEBPACK_IMPORTED_MODULE_16__["SessionStorageService"]
        }, {
          type: _src_core_servicios_plan_pago_service__WEBPACK_IMPORTED_MODULE_17__["PlanPagoService"]
        }];
      }, {
        componenteActualizarSolicitud: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['componenteActualizarSolicitud', {
            "static": false
          }]
        }]
      });
    })();
    /***/

  }
}]);
//# sourceMappingURL=crediticios-src-app-app-module-es5.js.map