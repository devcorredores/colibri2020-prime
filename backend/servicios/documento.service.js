const modelos = require("../modelos");

class DocumentoService {

    static async getAll() {
        try {
            return await modelos.documento.findAll();
        } catch (e) {
            console.log(e)
        }
    }

    static async byid(id) {
        try {
            return await modelos.documento.findAll({ where: { diccionario_id: id } })
        } catch (e) {
            console.log(e)
        }
    }

    static async byidparam(id) {
        try {
            return await modelos.documento.findAll({ where: { id } })
        } catch (e) {
            console.log(e)
        }
    }

    static async listaDocumentosByIdPoliza(idPoliza, callback) {
        try {
            return await modelos.documento.findAll({
                where: {id_poliza: idPoliza},
                include: [{
                    model:modelos.poliza,
                    include: [{
                        model:modelos.entidad,
                        as: 'aseguradora'
                    }, {
                        model:modelos.parametro, as: 'ramo',
                    }, {
                        model:modelos.objeto,
                    }]
                }, {
                    model:modelos.archivo,
                }]
            });
            if (typeof callback == 'function') {
                await callback(response.data);
            }
        } catch (e) {
            console.log(e)
        }
    }

}

module.exports = DocumentoService;
