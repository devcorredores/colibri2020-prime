const modelos = require('../modelos');

class AdminPermisosService {

    static async newModulo(data, callback = null) {
        return await modelos.modulo.create(data);
    }

    static async newVista(data, callback = null) {
        return await modelos.vista.create(data);
    }

    static async newComponente(data, callback = null) {
        return await modelos.componente.create(data)
    }

    static async getAllModulos(callback = null) {
        return await modelos.modulo.findAll()
    }

    static async getComponentes(idVista,idPerfil) {
        return await modelos.perfil_x_componente.findAll({
            where:{id_perfil:idPerfil},
            include:{
                model:modelos.componente,
                where: {id_vista:idVista}
            }
        });
    }

    static async listarMenu(idUsuario) {
        let [usuarioRoles] = await modelos.sequelize.query(`select id_perfil from usuario_x_rol A inner join rol_x_perfil B on A.id_rol=B.id_rol where A.id_usuario=${idUsuario}`);
        let idsUser = usuarioRoles.map(param => param.id_perfil);
        let idsPerfil = idsUser.join(',');
        let [perfilModulos] = await modelos.sequelize.query(`select A.* from (select distinct B.* from perfil_x_modulo A inner join modulo B on A.id_modulo=B.id and A.estado!='I' and A.id_perfil in (${idsUser})) A order by CONVERT(int,A.codigo)`);
        return await this.generarMenu(perfilModulos,idsPerfil);
    }

    static async listarMenu2(idUsuario) {
        return await modelos.usuario.find({
            where: {id:idUsuario},
            include:{
                model: modelos.rol, as:'usuarioRoles',
                include: {
                    model:modelos.perfil, as:'rolPerfiles',
                    include: {
                        model: modelos.modulo, as:'perfilModulos',
                        include: {
                           model: modelos.vista,
                           include: {
                               model: modelos.componente
                           }
                        }
                    }
                }
            }
        });
    }

    static async generarMenu2 (usuario) {
        try {
            let ArrayMenu = [];
            for (let i = 0; i < usuario.usuarioRoles.length; i++) {
                let usuarioRol = usuario.usuarioRoles[i];
                let menuPadre;
                for (let j = 0; j < usuarioRol.rolPerfiles.length; j++) {
                    let rolPerfil = usuarioRol.rolPerfiles[j];
                    let perfilModuloPadre = rolPerfil.perfilModulos.find(param => param.id_modulo_super == null);
                    rolPerfil.perfilModulos = rolPerfil.perfilModulos.filter(param => param.id_modulo_super != null);
                    if (menuPadre) {
                        if (perfilModuloPadre && menuPadre.label != perfilModuloPadre.descripcion) {
                            menuPadre = {
                                label:perfilModuloPadre.descripcion,
                                visible:perfilModuloPadre.visible,
                                icon:perfilModuloPadre.icon,
                                items:[]
                            }
                        }
                    } else {
                        menuPadre = {
                            label:perfilModuloPadre.descripcion,
                            visible:perfilModuloPadre.visible,
                            icon:perfilModuloPadre.icon,
                            items:[]
                        }
                    }
                    for (let k = 0; k < rolPerfil.perfilModulos.length; k++) {
                        let perfilModulo = rolPerfil.perfilModulos[k];
                        let subMenu = {
                            label:perfilModulo.descripcion,
                            visible:perfilModulo.visible,
                            icon:perfilModulo.icon,
                            items:[]
                        }
                        for (let l = 0; l < perfilModulo.vista.length; l++) {
                            let vista = perfilModulo.vista[l];
                            let subSubMenu = {
                                label:vista.descripcion,
                                visible:vista.visible,
                                icon:vista.icon,
                                items:[]
                            }
                            subMenu.items.push(subSubMenu);
                        }
                        if (perfilModulo.vista.length) {
                            menuPadre.items.push(subMenu);
                        }
                    }
                    if(!ArrayMenu.find(param => param.label == menuPadre.label)) {
                        ArrayMenu.push(menuPadre)
                    }
                }
            }
            return ArrayMenu;
        } catch (e) {
            console.log(e)
        }
    }

    static async generarMenu (perfilModulos,idsPerfil) {
        return new Promise(async resolve => {
            let ArrayMuenu=[];
            let menus_padres=perfilModulos.filter(menu => menu.id_modulo_super === null );
            for(let i=0;menus_padres.length>i;i++) {
                let itm=await this.BuscarMenusHijos(menus_padres[i].id,perfilModulos,menus_padres[i].descripcion,idsPerfil);
                let menu= {
                    label:menus_padres[i].descripcion,
                    visible:menus_padres[i].visible,
                    icon:menus_padres[i].icon,
                    items:itm
                };
                ArrayMuenu.push(menu);
                if (i === menus_padres.length-1){
                    resolve(ArrayMuenu);
                }
            }
        });
    }

    static async BuscarMenusHijos (idSuper,modulos,localizacion,idsPerfil) {
        return new Promise(async resolve => {
            let ArrayMuenu=[];
            ArrayMuenu = await this.BuscarVistas(idSuper,localizacion,idsPerfil);
            let menus_hijos=modulos.filter(menu => menu.id_modulo_super === idSuper );
            for(let j=0; j<menus_hijos.length;j++){
                let menu={
                    label:menus_hijos[j].descripcion,
                    icon:menus_hijos[j].icon,
                    visible:menus_hijos[j].visible,
                    items:await this.BuscarMenusHijos(menus_hijos[j].id,modulos,localizacion+'/'+menus_hijos[j].descripcion,idsPerfil),localizacion:localizacion+'/'+menus_hijos[j].descripcion
                };
                ArrayMuenu.push(menu);
                if (j===menus_hijos.length-1){
                    resolve (ArrayMuenu);
                }
            }
            if (menus_hijos.length===0){
                resolve (ArrayMuenu);
            }
            //resolve (ArrayMuenu);
        });
    }

    static async BuscarVistas (idSuper,localizacion,idsPerfil) {
        return new Promise(async resolve => {
            await modelos.sequelize.query(`select distinct B.*,A.estado from perfil_x_vista A inner join vista B on A.id_vista=B.id and B.id_modulo=${idSuper} and A.estado!='I' and id_perfil in (${idsPerfil})`)
                .then(async ([results, metadata]) => {
                    let ArrayMuenu=[];
                    if(results.length===0){
                        resolve(ArrayMuenu);
                    }
                    for(let i=0;i<results.length;i++){
                        let menu={
                            id:results[i].id,
                            visible:results[i].visible,
                            label:results[i].descripcion,
                            icon:results[i].icon,
                            ruta:results[i].ruta,
                            codigo:results[i].codigo,
                            componentes:await this.BuscarComponentes(results[i].id,idsPerfil),
                            localizacion:localizacion+'/'+results[i].descripcion,
                            estado:results[i].estado
                        };
                        ArrayMuenu.push(menu);
                        if (i===results.length-1){
                            resolve(ArrayMuenu);
                        }
                    }
                });
        });
    }

    static async BuscarComponentes (id_vista,idsPerfil) {
        return new Promise(async resolve => {
            await modelos.sequelize.query(`select B.*,A.estado,A.observacion from perfil_x_componente A inner join componente B on A.id_componente=B.id and B.id_vista=${id_vista} and A.id_perfil in (${idsPerfil})`)
                .then(([results, metadata]) => {
                    let ArrayComponentes=[];
                    results.forEach(element => {
                        let componente={
                            codigo:element.codigo,
                            descripcion:element.descripcion,
                            tipo_compoonente:element.tipo_compoonente,
                            estado:element.estado,
                            observacion:element.observacion
                        };
                        ArrayComponentes.push(componente);
                    });
                    resolve(ArrayComponentes);
                });
        });
    }
}

module.exports = AdminPermisosService;
