const modelos = require('../modelos');


class InstanciaPolizaService {

    static async deleteInstanciaPolizas(idInstancias = []) {
        try {
            let aData = [];
            for (let i = 0; i < idInstancias.length; i++) {
                let idInstancia = idInstancias[i];
                let resp = await modelos.sequelize.query(`
                    DECLARE @idInstanciaPoliza int;
                    SET @idInstanciaPoliza = ${idInstancia};
                    DELETE FROM instancia_anexo_asegurado WHERE id_asegurado in (select id from asegurado where id_instancia_poliza = @idInstanciaPoliza);
                    DELETE FROM atributo_instancia_poliza where id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM beneficiario where id_asegurado in (select id from asegurado where id_instancia_poliza = @idInstanciaPoliza);
                    DELETE FROM atributo_instancia_documento WHERE id_instancia_documento in (select id from instancia_documento where id_instancia_poliza = @idInstanciaPoliza);
                    DELETE FROM instancia_poliza_transicion WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM asegurado WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM instancia_documento WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM plan_pago_detalle WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM plan_pago WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM instancia_poliza WHERE id = @idInstanciaPoliza;
                `);
                aData.push(resp);
            }
            return aData;
        } catch (e) {
            console.log(e)
        }
    }
}

module.exports = InstanciaPolizaService;
