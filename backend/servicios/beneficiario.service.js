require('../utils/Prototipes');
const bcrypt = require('bcrypt-nodejs');
const modelos = require("../modelos");
const modelosDesgravamen = require("../modelos/desgravamen");

class BeneficiarioService {

    static async usuarios () {
        try {
            return await modelos.sequelize.query("SELECT * FROM usuario U INNER JOIN autenticacion_usuario A ON U.id =  a.usuario_id");
        } catch (e) {
            console.log(e);
        }
    }

    static async register () {
        try {
            return await modelos.persona.create()
        } catch (e) {
            console.log(e);
        }
    }

    static async getById (id) {
        try {
            let beneficiario = await modelos.beneficiario.findById(id);
            let id_entidad = parseInt(beneficiario.dataValues.id_beneficiario, 10);
            let entidad = await modelos.entidad.findById(id_entidad)
            let id_persona = parseInt(entidad.dataValues.id_persona, 10);
            let persona = await modelos.persona.findById(id_persona);
            return persona;
        } catch (e) {
            console.log(e);
        }
    }

    static async getBeneficiarios(id) {
        try {
            let beneficiario = await modelos.beneficiario.findById(id);
            let id_entidad = parseInt(beneficiario.dataValues.id_beneficiario, 10);
            let entidad = await modelos.entidad.findById(id_entidad);
            let id_persona = parseInt(entidad.dataValues.id_persona, 10);
            return await modelos.persona.findById(id_persona);
        } catch (e) {
            console.log(e);
        }
    }

    static async byId_beneficiario (id_beneficiario) {
        try {
            let beneficiario = await modelos.beneficiario.findById(id_beneficiario);
            let id_entidad = parseInt(beneficiario.dataValues.id_beneficiario, 10);
            let entidad = await modelos.entidad.findById(id_entidad);
            let id_persona = parseInt(entidad.dataValues.id_persona, 10);
            return await modelos.persona.findById(id_persona);
        } catch (e) {
            console.log(e);
        }
    }

    static async eliminar (id) {
        try {
            let beneficiario = await modelos.beneficiario.findById(id);
            let persona;
            let id_persona;
            let entidad;
            let id_entidad;
            if (beneficiario) {
                id_entidad = parseInt(beneficiario.dataValues.id_beneficiario, 10);
                beneficiario.destroy();
                entidad = await modelos.entidad.findById(id_entidad);
                id_persona = parseInt(entidad.dataValues.id_persona, 10);
                entidad.destroy();
                persona = await modelos.persona.findById(id_persona)
                persona.destroy();
            }
            return persona;
        } catch (e) {
            console.log(e);
        }
    }

    static async modificar (data) {
        try {

            let personaBeneficiario = data.personaCompleto;
            if (personaBeneficiario.par_tipo_documento_id == '') {
                personaBeneficiario.par_tipo_documento_id = null
            }
            if (personaBeneficiario.par_nacionalidad_id == '') {
                personaBeneficiario.par_nacionalidad_id = null
            }
            if (personaBeneficiario.par_pais_nacimiento_id == '') {
                personaBeneficiario.par_pais_nacimiento_id = null
            }
            if (personaBeneficiario.par_sexo_id == '') {
                personaBeneficiario.par_sexo_id = null
            }
            if (personaBeneficiario.persona_celular == '') {
                personaBeneficiario.persona_celular = null
            }
            const id_parentesco = data.id_parentesco ? data.id_parentesco : null;
            const porcentaje = data.porcentaje ? data.porcentaje : null;
            const tipo = data.tipo ? data.tipo : null;
            const estado = data.estado ? data.estado : null;
            const condicion = data.condicion ? data.condicion : null;
            var descripcion_otro = '';
            if (id_parentesco == 56) {
                descripcion_otro = data.descripcion_otro;
            } else {
                descripcion_otro = '';
            }
            var id = data.id_beneficiario;
            let beneficiario = await modelos.beneficiario.findById(id);
            let id_entidad = parseInt(beneficiario.dataValues.id_beneficiario, 10);
            await beneficiario.update({id_parentesco, descripcion_otro, porcentaje, tipo, estado, condicion});
            let entidad = await modelos.entidad.findById(id_entidad)
            const id_persona = parseInt(entidad.dataValues.id_persona, 10);
            let persona = await modelos.persona.findById(id_persona);
            return await persona.update(personaBeneficiario);
        } catch (e) {
            console.log(e);
        }
    }

    static async restore (username,password,password2) {
        try {
            if (password != password2) {
                return res.status(301).send({ message: 'La contraseña nueva y su repeticion no coinciden.' });
                // return done(null, false, req.flash('restoreMessage', 'La contraseña nueva y su repeticion no coinciden.'));
            } else {
                let usuarioPwd = await modelos.usuario.find({ where: { usuario_login: username } });
                if (usuarioPwd) {
                    let salt = bcrypt.hashSync(password, null, null);
                    return await usuariosPwd.update({ login: salt });
                } else {
                    return null
                }
                return null
            }
        } catch (e) {
            console.log(e);
        }
    }

    static async getDatosAdicionalesPersona (id_persona) {
        try {
            let [data] = await modelos.sequelize.query(`
                SELECT atributo.descripcion AS id_objeto_x_atributo,
                datos_adicionales_persona.valor
                FROM atributo 
                INNER JOIN objeto_x_atributo 
                ON atributo.id = objeto_x_atributo.id_atributo 
                INNER JOIN objeto 
                ON objeto.id = objeto_x_atributo.id_objeto
                INNER JOIN datos_adicionales_persona
                ON datos_adicionales_persona.id_objeto_x_atributo = objeto_x_atributo.id
                WHERE datos_adicionales_persona.id_persona = ${id_persona};`);
            return data;
        } catch (e) {
            console.log(e);
        }
    }

    static async adicionarPersonaCompleto (data) {
        try {
            let datos_adicionales = [{ id_persona: 1 }];
            let persona = { id: 1, persona_datos_adicionales: datos_adicionales };
            persona = data;
            let persona1 = await modelos.persona.create(persona);
            persona.persona_datos_adicionales.forEach(async element => {
                element.id_persona = persona1.id;
                await modelos.datos_adicionales_persona.create(element)
            });
            return persona;
        } catch (e) {
            console.log(e);
        }
    }

    static async getDatosDeudor (nroSolicitudSci, docId, docIdExt) {
        try {
            let parDocIdExt = await modelos.parametro.findOne({where:{diccionario_id:18,parametro_cod:docIdExt}});
            let solicitud = await modelosDesgravamen.seg_solicitudes.findOne({
                where: {
                    Sol_NumSol: {$like: nroSolicitudSci.trim()}
                },
                include: [{
                    model: modelosDesgravamen.seg_deudores,
                    as: 'sol_deudor',
                    where: {
                        Deu_NumDoc: {$like:docId.trim()},
                        // Deu_ExtDoc: docIdExt != 12 ? {$like: parDocIdExt.parametro_abreviacion} : ['',null],
                    },
                    include:{
                        model: modelosDesgravamen.seg_beneficiarios,
                        as:'deu_beneficiarios'
                    }
                }]
            });
            console.log(solicitud);
            return solicitud;
        } catch (e) {
            console.log(e);
        }
    }

    static async createBeneficiariosFromSolicitud(objSolicitud,idInstanciaPoliza) {
        try {
            let asegurado = await modelos.asegurado.findOne({
                where: {id_instancia_poliza: idInstanciaPoliza},
                include: {
                    model: modelos.beneficiario,
                    include: [
                        {
                            model: modelos.entidad,
                            include: [
                                {
                                    model: modelos.persona,
                                },
                            ],
                        },
                        {
                            model: modelos.parametro,
                            as: "estado_obj",
                        },
                        {
                            model: modelos.parametro,
                            as: "tipo_obj",
                        },
                        {
                            model: modelos.parametro,
                            as: "parentesco",
                        },
                    ]
                }
            });
            let parametros = await modelos.parametro.findAll({
                where: {diccionario_id:[1,18,4,20,58]}
            });
            if (objSolicitud.sol_deudor && objSolicitud.sol_deudor.length && objSolicitud.sol_deudor.find(param => param.deu_beneficiarios && param.deu_beneficiarios.length)) {
                for (let i = 0; i < objSolicitud.sol_deudor.length; i++) {
                    let solDeudor = objSolicitud.sol_deudor[i];
                    for (let i = 0; i < solDeudor.deu_beneficiarios.length; i++) {
                        let deuBeneficiario = solDeudor.deu_beneficiarios[i];
                        let parPersonaBenExt = deuBeneficiario.Deu_ExtDoc ? parametros.find(param => param.diccionario_id == 18 && param.parametro_abreviacion.toLowerCase() == deuBeneficiario.Deu_ExtDoc.toLowerCase()) : null;
                        let parPersonaBenSexo = deuBeneficiario.Ben_Sexo ? parametros.find(param => param.diccionario_id == 4 && param.parametro_descripcion.toLowerCase() == deuBeneficiario.Ben_Sexo.toLowerCase()) : null;
                        let parTipoDocumento = deuBeneficiario.Ben_TipoDoc ? parametros.find(param => param.diccionario_id == 1 && param.parametro_descripcion.toLowerCase() == deuBeneficiario.Ben_TipoDoc.toLowerCase()) : null;
                        let parBeneficiarioParentesco = deuBeneficiario.Ben_Relacion ? parametros.find(param => param.diccionario_id == 20 && param.parametro_descripcion.toLowerCase().substring(0,4) == deuBeneficiario.Ben_Relacion.toLowerCase().substring(0,4)) : null;
                        let parCondicionBeneficiario = deuBeneficiario.Ben_Tipo ? parametros.find(param => param.diccionario_id == 58 && param.parametro_abreviacion.toLowerCase() == deuBeneficiario.Ben_Tipo.toLowerCase()) : null;
                        let newPersonaBeneficiario = {
                            persona_doc_id:deuBeneficiario.Ben_NumDoc,
                            persona_doc_id_ext:parPersonaBenExt ? parPersonaBenExt.parametro_cod : parPersonaBenExt,
                            persona_doc_id_comp:deuBeneficiario.Ben_CompDoc,
                            persona_primer_apellido:deuBeneficiario.Ben_Paterno,
                            persona_segundo_apellido:deuBeneficiario.Ben_Materno,
                            persona_primer_nombre:deuBeneficiario.Ben_Nombre,
                            persona_segundo_nombre:'',
                            persona_apellido_casada:deuBeneficiario.Ben_Casada,
                            persona_direccion_domicilio:'',
                            persona_direcion_trabajo:'',
                            persona_fecha_nacimiento:null,
                            persona_celular:deuBeneficiario.Ben_Telefono,
                            persona_telefono_domicilio:'',
                            persona_telefono_trabajo:'',
                            persona_profesion:'',
                            par_tipo_documento_id:parTipoDocumento ? parTipoDocumento.id : null,
                            par_nacionalidad_id:null,
                            par_pais_nacimiento_id:null,
                            par_sexo_id:parPersonaBenSexo ? parPersonaBenSexo.parametro_cod : parPersonaBenSexo,
                            adicionada_por:asegurado.adicionado_por,
                            modificada_por:asegurado.modificado_por
                        };
                        let personaBeneficiario = await modelos.persona.create(newPersonaBeneficiario);
                        let newEntidad = {
                            id_persona:personaBeneficiario.id,
                            tipo_entidad:12,
                            adicionada_por:asegurado.adicionado_por,
                            modificada_por:asegurado.modificado_por
                        };
                        let entidadBeneficiario = await modelos.entidad.create(newEntidad);
                        let newBeneficiario = {
                            id_asegurado:asegurado.id,
                            id_beneficiario:entidadBeneficiario.id,
                            id_parentesco:parBeneficiarioParentesco ? parBeneficiarioParentesco.id : 56,
                            porcentaje:deuBeneficiario.Ben_Porcentaje,
                            adicionado_por:asegurado.modificado_por,
                            modificado_por:asegurado.modificado_por,
                            descripcion_otro:!parBeneficiarioParentesco ? deuBeneficiario.Ben_Relacion : '',
                            estado:83,
                            tipo:86,
                            condicion:parCondicionBeneficiario ? parCondicionBeneficiario.id : 300,
                            fecha_declaracion:asegurado.createdAt
                        };
                        let beneficiario = await modelos.beneficiario.create(newBeneficiario);
                        entidadBeneficiario.dataValues.persona = personaBeneficiario;
                        beneficiario.dataValues.entidad = entidadBeneficiario;
                        asegurado.dataValues.beneficiarios.push(beneficiario);
                    }
                }
            }
            return asegurado;
        } catch (e) {
            console.log(e);
        }
    }
}

module.exports = BeneficiarioService;
