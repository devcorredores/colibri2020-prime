const modelos = require('../modelos');
const {loggerSolicitud} = require("../modulos/winston");
const {loggerCatch} = require("../modulos/winston");
const request = require('request');
const specialRequest = request.defaults({strictSSL: false});
const soap = require('soap');
const webconfig = require('../config/web-config');
const to_json = require('xmljson').to_json;
const soapHeaderXml = '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';
const invoke = require("../modulos/soap-request");
const fechas = require('fechas');
const moment = require('moment');
const solicitudService = require('./solicitud.service');
const util = require('../utils/util');
const Op = require('sequelize');
const { GenerarPlanPagos } = require('../modulos/funciones_plan_pago');
const multer = require('multer');
const path = require("path");

class FileService {

	static async upload(req,res = null) {
		try {
			let storage = multer.diskStorage({
				destination: (req, file, cb) => {
					cb(null, path.join(__dirname, "../../../public/upload"))
				},
				filename: (req, file, cb) => {
					cb(null, file.originalname);
				}
			});

			const upload = multer({ storage });

			let multerMiddleware = upload.single('file');

			multerMiddleware(req, res, () => {
				if (typeof callback == 'function') {
					callback(null,req.file);
				}
			});
		} catch (e) {
			console.log(e);
		}
	}
}

module.exports = FileService;
