const modelos = require("../modelos");

class AnexoService {

    static async anexoAndDantoAnexoByIdPoliza (id_poliza, id_asegurado, id_anexo_poliza) {
        try {
            return modelos.poliza.find({
                where: { id : id_poliza },
                include: [{
                    model: modelos.anexo_poliza,
                    where: { id_tipo : 110, id : id_anexo_poliza},
                    include: [{
                        model: modelos.anexo_asegurado,
                        include: [{
                            model: modelos.dato_anexo_asegurado
                        }, {
                            model: modelos.parametro
                        }, {
                            model: modelos.instancia_anexo_asegurado,
                            where: { id_asegurado },
                            required: false,
                            include:  [{
                                model:modelos.atributo_instancia_anexo_asegurado,
                                required: false
                            }, {
                                model:modelos.entidad,
                                include:modelos.persona
                            }, {
                                model:modelos.parametro,
                                as:'par_parentesco'
                            }, {
                                model:modelos.parametro,
                                as:'par_relacion'
                            }],
                        }]
                    }]
                }, {
                    model: modelos.objeto,
                    where: { id_tipo: 103 },
                    required: false,
                    include: [{
                        model: modelos.objeto_x_atributo,
                        include: [{
                            model: modelos.atributo,
                            include: modelos.diccionario
                        }]
                    }]
                }]
            });
        } catch (e) {
            console.log(e)
        }
    }

    static async findPlanesAnexoPoliza (idPoliza) {
        try {
            const idTipo = 110;
            return await modelos.anexo_poliza.findAll({
                where:{ id_poliza:idPoliza, id_tipo: idTipo },
                include: { model: modelos.anexo_asegurado}
            })
        } catch (e) {
            console.log(e)
        }
    }

    static async findAnexoPoliza (idPoliza) {
        try {
            return await modelos.anexo_poliza.findAll({
                where: {id_poliza: idPoliza, vigente: true},
                order: [['version', 'ASC']],
                include: [{
                    model: modelos.anexo_asegurado,
                    include: [{
                        model: modelos.dato_anexo_asegurado
                    }, {
                        model: modelos.parametro,
                    }, {
                        model: modelos.parametro, as:'edad_unidad'
                    }]
                }]
            });
        } catch (e) {
            console.log(e)
        }
    }

     static async findAnexoPolizaWithAsegurado (idPoliza, idAsegurado) {
        try {
            return await modelos.anexo_poliza.findAll({
                where: {id_poliza: idPoliza, vigente:true},
                include: [{
                    model: modelos.anexo_asegurado,
                    include: [{
                        model: modelos.dato_anexo_asegurado
                    }, {
                        model: modelos.parametro
                    }, {
                        model: modelos.instancia_anexo_asegurado,
                        where: {idAsegurado},
                        required: false,
                        include: [{
                            model: modelos.atributo_instancia_anexo_asegurado,
                            required: false
                        }],
                    }]
                }]
            });
        } catch (e) {
            console.log(e)
        }
     }

    static async byid (id) {
        try {
            return await modelos.rol.findAll({ where: { id } })
        } catch (e) {
            console.log(e)
        }
    }

    static async guardar_usuarioxrol (id_usuario, id_rol) {
        try {
            return await modelos.usuario_x_rol.create({ id_usuario, id_rol })
        } catch (e) {
            console.log(e)
        }
    }

    static async resgistrarEmparejamiento (usuarios) {
        try {
            for (let usuario of usuarios) {
                for (let rol of usuario.roles_adicionar) {
                    let usuario_x_rol = {
                        id_rol: rol.id,
                        id_usuario: usuario.id,
                        adicionado_por: usuario.adicionado_por,
                        modificado_por: usuario.modificado_por,
                    };
                    await modelos.usuario_x_rol.create(usuario_x_rol);
                }
            }
        } catch (e) {
            console.log(e);
        }
    }

    static async eliminarRol_x_Perfil (id_usuario, id_rol) {
        try {
            return await modelos.usuario_x_rol.destroy({ where: { id_usuario, id_rol } })
        } catch (e) {
            console.log(e)
        }
    }

    static async findAllAnexoPolizaByIdPoliza (id_poliza) {
        try {
            return await modelos.anexo_poliza.findAll({ where: { id_poliza, vigente:true } })
        } catch (e) {
            console.log(e)
        }
    }
}

module.exports = AnexoService;
