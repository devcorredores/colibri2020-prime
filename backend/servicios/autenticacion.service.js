const modelos = require("../modelos");

class AutenticacionService {

    static async autenticacionUsuario () {
        try {
            return await modelos.autenticacion_usuario.findAll()
        } catch (e) {
            console.log(e)
        }
    }

    static async autenticacionusuario (id_usuario) {
        try {
            return await modelos.autenticacion_usuario.findAll({ where: { id_usuario } });
        } catch (e) {
            console.log(e)
        }
    }

    static async byid (id) {
        try {
            return await modelos.autenticacion_usuario.findAll({ where: { id } });
        } catch (e) {
            console.log(e)
        }
    }

    static async guardar_autenticacionxusuario (usuario_id,par_autenticacion_id) {
        try {
            return await modelos.autenticacion_usuario.create({ usuario_id, par_autenticacion_id})
        } catch (e) {
            console.log(e)
        }
    }
}

module.exports = AutenticacionService;
