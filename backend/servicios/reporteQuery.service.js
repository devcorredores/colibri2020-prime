require('../utils/Prototipes');
const modelos = require('../modelos');
const modelosDesgravamen = require('../modelos/desgravamen');
const funciones = require('../modulos/funciones');
const soapService = require('../servicios/soap.service');
const generaPdf = require('../modulos/genera-pdf');
const env2 = require("../config/env2");

class ReporteQueryService {

	static async GetWithParametrosByRol(id) {
		try {
			return await modelos.reporte_query.findAll({
				where: { id_rol: id },
				include: [{
					model:modelos.reporte_query_parametro,
					required:false,
					include:{
						model:modelos.diccionario,
						requiered:false
					}

				},{
					model:modelos.poliza,
					requiered:false
				}],
				order: [
					['id_poliza', 'ASC']
				]
			}).then(resp => {
                response.data = resp;
            }).catch((err) => {
                response.status = 'ERROR';
                response.message = err;
            });
		} catch (e) {
			console.log(e);
		}
	}

	static async EjecutarQuery(id,reporte_query_parametros) {
		try {
			let reporteQuery = await modelos.reporte_query.findOne({where: { id: id }});
			let query = reporteQuery.query;
            reporte_query_parametros.forEach(element => {
				if (element.tipo === 'date') {
					element.valor = funciones.formatoFechaQuery(element.valor);
				}
				query=query.replaceAll(element.nombre, element.valor);
            });
			let data;
			if (reporteQuery.id_tipo == 328) {
				let aCodigo = reporteQuery.codigo.split(',');
				let reporteQueryFunction = aCodigo.shift();
				let reporteQueryFunctionParam1 = reporte_query_parametros.find(param => param.nombre == aCodigo[0]);
				let reporteQueryFunctionParam2 = reporte_query_parametros.find(param => param.nombre == aCodigo[1]);
				if (typeof soapService[reporteQueryFunction] == 'function') {
					data = await soapService[reporteQueryFunction](reporteQueryFunctionParam1 ? reporteQueryFunctionParam1.valor : null,reporteQueryFunctionParam2 ? reporteQueryFunctionParam2.valor : null);
				} else {
					data = await modelosDesgravamen.sequelize.query(query);
				}
			} else {
				if (reporteQuery.codigo ) {
					if (reporteQuery.codigo.includes(env2.database)){
						data = await modelosDesgravamen.sequelize.query(query);
					} else {
						data = await modelos.sequelize.query(query);
					}
				} else {
					data = await modelos.sequelize.query(query);
				}
			}
            if(Array.isArray(data)){
            	data = data[0]
            } else {
            	data = data
            }
            return data;
		} catch (e) {
			console.log(e);
		}
	}

	static async EjecutarQueryCertificado(id_poliza,nombre_archivo,reporte_query_parametros) {
		try {
			let documentoVersion = await modelos.documento_version.findOne({
				include: {
					model: modelos.reporte_query,
					where: { id_poliza: id_poliza, id_tipo:281 },
					include: {
						model:modelos.reporte_query_parametro
					}
				}
			});
			let query = documentoVersion.reporte_query.query;
            reporte_query_parametros.forEach(element => {
                if (element.tipo === 'date') {
                	element.valor = funciones.formatoFechaQuery(element.valor);
                }
                query = query.replaceAll(element.nombre, element.valor);
            });
            let data = await modelos.sequelize.query(query);
			if (data) {
                data = data[0];
                if (data.length > 0) {
                    await generaPdf.generarCertificado(data, nombre_archivo, documentoVersion);
                }
            }
            return data;
		} catch (e) {
			console.log(e);
		}
	}

	static async EjecutarQuerySolicitud(id_poliza,nombre_archivo,reporte_query_parametros) {
		try {
			let documentoVersion = await modelos.documento_version.findOne({
				include: {
					model: modelos.reporte_query,
					where: { id_poliza: id_poliza, id_tipo:279 },
					include: {
						model:modelos.reporte_query_parametro
					}
				}
			});
			let query = documentoVersion.reporte_query.query;
            reporte_query_parametros.forEach(element => {
                if (element.tipo === 'date') {
                	element.valor = funciones.formatoFechaQuery(element.valor);
                }
                query = query.replaceAll(element.nombre, element.valor);
            });
            let data = await modelos.sequelize.query(query);
			if (data) {
                data = data[0];
                if (data.length > 0) {
                     switch (parseInt(id_poliza)) {
                         case 1:
                             await generaPdf.generarReporteSolicitud(datos, nombre_archivo);
                             break;
                         case 2:
                             await generaPdf.generarReporteSolicitud(datos, nombre_archivo);
                             break;
                         case 3:
                             await generaPdf.generarReporteSolicitudTarjetas(datos, nombre_archivo);
                             break;
                         case 4:
                             await generaPdf.generarReporteSolicitudTarjetas(datos, nombre_archivo);
                             break;
                         case 6:
                             await generaPdf.generarReporteSolicitudEcoVida(datos, nombre_archivo);
                             break;
                         case 7:
                             await generaPdf.generarReporteSolicitudEcoProteccion(datos, nombre_archivo);
                             break;
                         case 8:
                             await generaPdf.GenerarReporteSolicitudEcoMedic(datos, nombre_archivo);
                             break;
                         case 10:
                             await generaPdf.generarReporteSolicitudEcoAccidentes(datos, nombre_archivo);
                             break;
                         case 11:
                             await generaPdf.generarReporteSolicitudEcoRiesgo(datos, nombre_archivo);
                             break;
                         case 12:
                             await generaPdf.generarReporteSolicitudEcoResguardo(datos, nombre_archivo);
                             break;
                         case 13:
                             await generaPdf.generarReporteSolicitudTarjetas(datos, nombre_archivo);
                             break;
                     }
                }
            }
            return data;
		} catch (e) {
			console.log(e);
		}
	}

	static async EjecutarQueryOrdenPago(id_poliza,nombre_archivo,reporte_query_parametros) {
		try {
			let documentoVersion = await modelos.documento_version.findOne({
				include: {
					model: modelos.reporte_query,
					where: { id_poliza: id_poliza, id_tipo:280 },
					include: {
						model:modelos.reporte_query_parametro
					}
				}
			});
			let query = documentoVersion.reporte_query.query;
            reporte_query_parametros.forEach(element => {
                if (element.tipo === 'date') {
                	element.valor = funciones.formatoFechaQuery(element.valor);
                }
                query = query.replaceAll(element.nombre, element.valor);
            });
            let data = await modelos.sequelize.query(query);
			if (data) {
                data = data[0];
                if (data.length > 0) {
                     switch (parseInt(id_poliza)) {
                         case 1:
                             await generaPdf.generarOrdenCobroEcoaguinaldo(data, nombre_archivo);
                             break;
                         case 2:
                             await generaPdf.generarOrdenCobroEcoaguinaldo(data, nombre_archivo);
                             break;
                         case 6:
                             await generaPdf.generarOrdenCobroEcoVida(data, nombre_archivo);
                             break;
                         case 8:
                             await generaPdf.generarOrdenCobroEcoMedic(data, nombre_archivo);
                             break;
                         case 10:
                             await generaPdf.generarOrdenCobroEcoAccidentes(data, nombre_archivo);
                             break;
                         case 11:
                             await generaPdf.generarOrdenCobroEcoriesgo(data, nombre_archivo);
                             break;
                         case 12:
                             await generaPdf.generarOrdenCobroEcoResguardo(data, nombre_archivo);
                             break;
                     }
                }
            }
            return data;
		} catch (e) {
			console.log(e);
		}
	}

	static async EjecutarQueryPlanPago(id_poliza,nombre_archivo,reporte_query_parametros) {
		try {
			let documentoVersion = await modelos.documento_version.findOne({
				include: {
					model: modelos.reporte_query,
					where: { id_poliza: id_poliza, id_tipo:280 },
					include: {
						model:modelos.reporte_query_parametro
					}
				}
			});
			let query = documentoVersion.reporte_query.query;
            reporte_query_parametros.forEach(element => {
                if (element.tipo === 'date') {
                	element.valor = funciones.formatoFechaQuery(element.valor);
                }
                query = query.replaceAll(element.nombre, element.valor);
            });
            let data = await modelos.sequelize.query(query);
			if (data) {
                data = data[0];
                if (data.length > 0) {
                     await generaPdf.generarOrdenCobroEcoaguinaldo(data, nombre_archivo);
                }
            }
            return data;
		} catch (e) {
			console.log(e);
		}
	}

	static async ReporteCobertura(objeto,fecha_ini,fecha_fin) {
		try {
			return await modelos.sequelize.query(`select D1.nro_documento,format(D1.fecha_emision,'dd-MM-yyyy') fecha_emision
				,format(D1.fecha_fin_vigencia,'dd-MM-yyyy') fecha_fin_vigencia
				,format(D1.fecha_inicio_vigencia,'dd-MM-yyyy') fecha_inicio_vigencia
				,isnull(D.persona_primer_apellido,'')+' '+isnull(D.persona_segundo_apellido,'')+' '+isnull(D.persona_primer_nombre,'')+' '+isnull(D.persona_segundo_nombre,'') asegurado
				,RIGHT('00' + Ltrim(Rtrim(datediff(day,format(getdate(),'yyyy-MM-dd'),format(D1.fecha_fin_vigencia,'yyyy-MM-dd')))),2) dias_vigencia
				,K.parametro_descripcion agencia
				,L.parametro_descripcion sucursal
				,K.parametro_abreviacion tipo_agencia	
				,M.usuario_nombre_completo
				,M.usuario_login
				from instancia_poliza A 
				inner join poliza A1 on A.id_poliza=A1.id and A.id_estado=59
				inner join asegurado B on A.id=B.id_instancia_poliza
				inner join entidad C on B.id_entidad=C.id
				inner join persona D on D.id=C.id_persona
				inner join instancia_documento D1 on D1.id_instancia_poliza=A.id and D1.id_documento in (3,6,9,12,15,18,21,24) and D1.fecha_fin_vigencia between '${fecha_ini}' and '${fecha_fin}'
				inner join objeto E on E.id_poliza=A.id_poliza and E.id_poliza=${objeto.id_poliza}
				inner join objeto_x_atributo F on F.id_objeto=E.id and F.id_atributo=5 --cuenta
				inner join objeto_x_atributo G on G.id_objeto=E.id and G.id_atributo=4 --correo
				inner join objeto_x_atributo I on I.id_objeto=E.id and I.id_atributo=59 --agencia
				inner join atributo_instancia_poliza I1 on I1.id_objeto_x_atributo=I.id and I1.id_instancia_poliza=A.id
				inner join objeto_x_atributo J on J.id_objeto=E.id and J.id_atributo=60 --sucursal
				inner join atributo_instancia_poliza J1 on J1.id_objeto_x_atributo=J.id and J1.id_instancia_poliza=A.id
				left join parametro K on K.parametro_cod=I1.valor and K.diccionario_id=40
				left join parametro L on L.parametro_cod=J1.valor and L.diccionario_id=38
				left join usuario M on A.adicionada_por=M.id
			`);
		} catch (e) {
			console.log(e);
		}
	}
}

module.exports = ReporteQueryService;
