const modelos = require('../modelos');
const {loggerSolicitud} = require("../modulos/winston");
const {loggerCatch} = require("../modulos/winston");
const fechas = require('fechas');
const moment = require('moment');
const solicitudService = require('./solicitud.service');
const util = require('../utils/util');
const soap = require('./soap.service');
const { GenerarPlanPagos } = require('../modulos/funciones_plan_pago');

class VigenciaService {

	static async verificaPersonaDatos(req,res = null) {
		try {
			let response = {status: 'OK', message: '', data: [], errors: []};
			let {idPoliza} = req.params;
			let asegurados = await modelos.asegurado.findAll({
				include: [{
					model: modelos.entidad,
					include: {
						model: modelos.persona,
						where: {persona_doc_id:{$ne:null},persona_doc_id_ext:{$ne:null}}
					}
				}, {
					model:modelos.instancia_poliza,
					where:{id_poliza: idPoliza}
				}]
			});
			for (let i = 0; i < asegurados.length; i++) {
				let asegurado = asegurados[i];
				let soapCustomer;
				if(asegurado.entidad.persona.persona_doc_id && asegurado.entidad.persona.persona_doc_id_ext) {
					soapCustomer = await soap.getCustomer(asegurado.entidad.persona.persona_doc_id,asegurado.entidad.persona.persona_doc_id_ext)
					if (soapCustomer) {
						if (soapCustomer.dia_fechanac && soapCustomer.mes_fechanac && soapCustomer.anio_fechanac) {
							let newFechaNacimiento;
							if (asegurado.entidad.persona.persona_fecha_nacimiento) {
								let pFechaNacDay = moment(asegurado.entidad.persona.persona_fecha_nacimiento).date();
								let pFechaNacMonth = moment(asegurado.entidad.persona.persona_fecha_nacimiento).month()+1;
								let pFechaNacYear = moment(asegurado.entidad.persona.persona_fecha_nacimiento).year();
								if (pFechaNacDay != parseInt(soapCustomer.dia_fechanac) ||
									pFechaNacMonth != parseInt(soapCustomer.mes_fechanac) ||
									pFechaNacYear != parseInt(soapCustomer.anio_fechanac)) {
									newFechaNacimiento = moment(soapCustomer.dia_fechanac+'/'+soapCustomer.mes_fechanac+'/'+soapCustomer.anio_fechanac,"DD/MM/YYYY").add('hours',4).toDate();
									await modelos.persona.update({persona_fecha_nacimiento:newFechaNacimiento},{where:{id:asegurado.entidad.persona.id}});
									await modelos.instancia_poliza_transicion.create({
										id_instancia_poliza:asegurado.id_instancia_poliza,
										par_estado_id:asegurado.instancia_poliza.id_estado,
										par_observacion_id:'326',
										observacion:`Se corrigió la fecha de nacimiento del asegurado, fecha anterior: ${moment(asegurado.entidad.persona.persona_fecha_nacimiento).format('DD/MM/YYYY h:mm:ss')}, fecha actual: ${moment(newFechaNacimiento).format('DD/MM/YYYY h:mm:ss')}`,
										adicionado_por:'583',
										modificado_por:'583',
									})
								}
							} else {
								newFechaNacimiento = moment(soapCustomer.dia_fechanac+'/'+soapCustomer.mes_fechanac+'/'+soapCustomer.anio_fechanac,"DD/MM/YYYY").toDate();
								await modelos.persona.update({persona_fecha_nacimiento:newFechaNacimiento},{where:{id:persona.id}});
							}
						}
					}
				}
			}

		} catch (e) {
			console.log(e)
		}
	}

	static async verificaVigencia(req,res = null) {
		try {
			let response = { status: 'OK', message: '', data: [], errors:[] };
			let { idPoliza } = req.params;
			let { idInstanciaPoliza } = req.params;
			let date = new Date();
			let parametros = await modelos.parametro.findAll({where:{diccionario_id:[11,52]}});
			let documentos = await modelos.documento.findAll({where:{id_poliza:idPoliza}});

			let parEmitido = parametros.find(param => param.id == 59);
			let parSolicitado = parametros.find(param => param.id == 81);
			let parDesistido = parametros.find(param => param.id == 62);
			let parSinVigencia = parametros.find(param => param.id == 282);

			let docSolicitud = documentos.find(param => param.id == 279);
			let docComprobante = documentos.find(param => param.id == 280);
			let docCertificado = documentos.find(param => param.id == 281);


			let documentoSolicitud = documentos.find(param => param.i)
			let respSolicitudes;
			let query = req.query;
			let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;

			if (idPoliza) {
				let yesterday =moment().subtract(1,'days').toDate();
				respSolicitudes = await modelos.asegurado.findAll({
					// limit: query.limit ? parseInt(query.limit) : null,
					// offset: offset ? parseInt(offset) : 0,
					where: idInstanciaPoliza  ? {id_instancia_poliza: idInstanciaPoliza} : null,
					include:{
						model: modelos.instancia_poliza,
						where:{id_estado:parEmitido.id, id_poliza:idPoliza},
						include:[
							{model: modelos.poliza},
							{
								model:modelos.instancia_documento,
					 			where: idInstanciaPoliza === undefined ? { fecha_fin_vigencia: {$lte:yesterday} } : null,
								include: {
									model:modelos.documento,
									where:{id:docCertificado.id},
									include: {
										model: modelos.parametro, as:'tipo_documento'
									}
								}
							}
						]
					}
				});

				for (let i = 0 ; i < (query.limit ? parseInt(query.limit) : respSolicitudes.length) ; i++) {
					let respSolicitud = respSolicitudes[i];
					let solicitud = respSolicitud.dataValues;
					let solicitudInstanciaPoliza = solicitud.instancia_poliza && solicitud.instancia_poliza.dataValues ? solicitud.instancia_poliza.dataValues : null;
					if (solicitud.instancia_poliza.instancia_documentos.length) {
						for (let j = 0 ; j < solicitudInstanciaPoliza.instancia_documentos.length ; j++) {
							let respInstanciaDocumento = solicitudInstanciaPoliza.instancia_documentos[j];
							let instanciaDocumento = respInstanciaDocumento && respInstanciaDocumento.dataValues ? respInstanciaDocumento.dataValues : null;
							if (instanciaDocumento) {
								if (instanciaDocumento.documento.id == docCertificado.id) {
									let todayStr = `${util.minTwoDigits(date.getDate())}/${util.minTwoDigits(date.getMonth()+1)}/${date.getFullYear()}`;
									let fechaInicioVigencia = instanciaDocumento.fecha_inicio_vigencia;
									let fechaInicioVigenciaStr = `${util.minTwoDigits(fechaInicioVigencia.getDate())}/${util.minTwoDigits(fechaInicioVigencia.getMonth()+1)}/${fechaInicioVigencia.getFullYear()}`;
									let momFechaInicioVigencia = moment(fechaInicioVigencia);
									let momToday = moment(date);
									let idInstanciaDocumento = instanciaDocumento.id;
									if (instanciaDocumento.fecha_fin_vigencia === null || instanciaDocumento.fecha_fin_vigencia === undefined) {
										let newFechaFinVigenciaStr = fechas.addMeses(fechaInicioVigenciaStr,12);
										let [dayFin,monthFin,yearFin] = newFechaFinVigenciaStr.split('/');
										instanciaDocumento.fecha_fin_vigencia = new Date(yearFin,monthFin,dayFin);
										delete instanciaDocumento.id;
										let respInstanciaDocumento = await modelos.instancia_documento.update(instanciaDocumento, {where:{id:idInstanciaDocumento}});
										respInstanciaDocumento = await modelos.instancia_documento.findOne({where:{id:idInstanciaDocumento}});
										instanciaDocumento = respInstanciaDocumento.dataValues;
										loggerSolicitud.info(`Fecha Fin vigencia actualizada: (${newFechaFinVigenciaStr}), Instancia poliza: (${solicitudInstanciaPoliza.id}), Documento actualizado: (${instanciaDocumento.id}),`);
									}

									let fechaFinVigencia = instanciaDocumento.fecha_fin_vigencia;
									let fechaFinVigenciaStr = `${util.minTwoDigits(fechaFinVigencia.getDate())}/${util.minTwoDigits(fechaFinVigencia.getMonth()+1)}/${fechaFinVigencia.getFullYear()}`;
									let momFechaFinVigencia = moment(fechaFinVigencia);
									let diffMesesVigencia = momFechaFinVigencia.diff(momFechaInicioVigencia, 'months')+1;
									let vigenciaActual = momToday.diff(momFechaInicioVigencia, 'months')+1;
									let instanciaPoliza, respInstanciaPoliza, idInstanciaPoliza;
									loggerSolicitud.info(`La instancia poliza Nro ${solicitudInstanciaPoliza.id} del producto ${solicitudInstanciaPoliza.poliza.descripcion}, tiene ${vigenciaActual} meses actualimente, su vigencia es del ${fechaInicioVigenciaStr} hasta el ${fechaFinVigenciaStr}`);
									let respInstanciaPolizaTransicions, respOldInstanciaPolizaTransicion, oldInstanciaPolizaTransicion, objInstanciaPolizaTransicions;
									if (solicitudInstanciaPoliza.poliza.vigencia_meses < vigenciaActual) {
										solicitudInstanciaPoliza.id_estado = parSinVigencia.dataValues.id;
										idInstanciaPoliza = solicitudInstanciaPoliza.id;
										delete solicitudInstanciaPoliza.id;
										respInstanciaPoliza = await modelos.instancia_poliza.update(solicitudInstanciaPoliza, {where:{id:idInstanciaPoliza}});
										respInstanciaPoliza = await modelos.instancia_poliza.findOne({where:{id:idInstanciaPoliza}});

										// transicion Poliza
										respOldInstanciaPolizaTransicion = await modelos.instancia_poliza_transicion.findOne({where:{id_instancia_poliza:idInstanciaPoliza}});
										oldInstanciaPolizaTransicion = respOldInstanciaPolizaTransicion && respOldInstanciaPolizaTransicion.dataValues ? respOldInstanciaPolizaTransicion.dataValues : null;
										if (oldInstanciaPolizaTransicion) {
											let oldInstanciaPolizaTransicionId = oldInstanciaPolizaTransicion.id;
											oldInstanciaPolizaTransicion.id_instancia_poliza = idInstanciaPoliza;
											oldInstanciaPolizaTransicion.par_estado_id = solicitudInstanciaPoliza.id_estado;
											oldInstanciaPolizaTransicion.observacio = `La vigencia de la solicitud expiro (${fechaInicioVigenciaStr} - ${fechaFinVigenciaStr}), la solicitud fue actualizada al estado: ${parSinVigencia.dataValues.parametro_descripcion}.`;
											delete oldInstanciaPolizaTransicion.id;
											respOldInstanciaPolizaTransicion = await modelos.instancia_poliza_transicion.update(oldInstanciaPolizaTransicion,{where:{id:oldInstanciaPolizaTransicionId}});
											respInstanciaPolizaTransicions = await modelos.instancia_poliza_transicion.findOne({where:{id:oldInstanciaPolizaTransicionId}});
										} else {
											let newInstanciaPolizaTrancision = {
												id_instancia_poliza: idInstanciaPoliza,
												par_estado_id: solicitudInstanciaPoliza.id_estado,
												par_observacion_id: '96',
												observacion: `La vigencia de la solicitud expiro (${fechaInicioVigenciaStr} - ${fechaFinVigenciaStr}), la solicitud fue actualizada al estado: ${parSinVigencia.dataValues.parametro_descripcion}.`,
												adicionado_por: solicitudInstanciaPoliza.adicionado_por,
												modificado_por: solicitudInstanciaPoliza.adicionado_por,
												updatedAt: new Date(),
												createdAt:new Date()
											};
											respInstanciaPolizaTransicions = await modelos.instancia_poliza_transicion.create(newInstanciaPolizaTrancision);
										}

										instanciaPoliza = respInstanciaPoliza.dataValues;
										loggerSolicitud.info(`Producto ${solicitudInstanciaPoliza.poliza.descripcion}, La instancia_poliza (${solicitud.instancia_poliza.id}) vencio, vigencia: (${fechaInicioVigenciaStr} - ${fechaFinVigenciaStr})'`, instanciaPoliza);
										response.data.push({info:'Solicitud Caduca', data:instanciaPoliza.id});
										console.log(`Verificado solicitud ID: ${solicitud.id}, instancia poliza ID: ${solicitud.instancia_poliza.id}, instancia Documento: ${instanciaDocumento.id}, documento ${instanciaDocumento.documento.descripcion}`, instanciaDocumento);
									} else {
										response.data.push({info:'Solicitud Caduca', data:solicitud.instancia_poliza.id});
										console.log(`Verificado solicitud ID: ${solicitud.id}, instancia poliza ID: ${solicitud.instancia_poliza.id}, instancia Documento: ${instanciaDocumento.id}, documento ${instanciaDocumento.documento.descripcion}`, instanciaDocumento);
									}
								}
							}
						}
					} else {
						loggerSolicitud.info(`Error: Producto ${solicitudInstanciaPoliza.poliza.descripcion}, la instancia poliza Nro ${solicitud.instancia_poliza.id} tiene (${solicitudInstanciaPoliza.instancia_documentos.length} documentos)`, solicitudInstanciaPoliza.instancia_documentos);
					}
				}
			}
			return response;
		} catch (e) {
			console.log(e);
			loggerCatch.info(new Date(),e);
		}
	}

	static async verificaVigenciaRenovando(req,res = null) {
		try {
			let response = [];
			let { idEstado } = req.params;
			let { idPoliza } = req.params;
			let { idUsuario } = req.params;
			let { idSucursal } = req.params;
			let { idAgencia } = req.params;
			let { idInstanciaPoliza } = req.params;
			let { gracia } = req.params;
			let { bForceSinVigencia } = req.params;
			let { manual } = req.params;
			let date = new Date();
			let parametros = await modelos.parametro.findAll({where:{diccionario_id:[11,52,31]}});
			let documentos = await modelos.documento.findAll({where:{id_poliza:idPoliza}});

			let parEmitido = parametros.find(param => param.id == 59);
			let parSolicitado = parametros.find(param => param.id == 81);
			let parDesistido = parametros.find(param => param.id == 62);
			let parSinVigencia = parametros.find(param => param.id == 282);
			let parModalidadMensual = parametros.find(param => param.id == 90);
			let parModalidadAnual = parametros.find(param => param.id == 93);

			let docSolicitud = documentos.find(param => param.id_tipo_documento == 279);
			let docComprobante = documentos.find(param => param.id_tipo_documento == 280);
			let docCertificado = documentos.find(param => param.id_tipo_documento == 281);

			let objetoAtributoModalidadEcoProteccion = 28;
			let parModPagoAnual = 93;

			idUsuario = req.session && req.session.passport ? req.session.passport.user : idUsuario;
			let respUsuario,usuario;
			if (idUsuario) {
				respUsuario = await modelos.usuario.findOne({where: {id: idUsuario}});
				usuario = respUsuario && respUsuario.dataValues ? respUsuario.dataValues : null;
			}

			let respPoliza = await modelos.poliza.findOne({where:{id:idPoliza}});
			let poliza = respPoliza.dataValues;
			//let documentoSolicitud = documentos.find(param => param.i);
			let respSolicitudes;
			let query = {};
			if (req.query) {
				query = req.query;
			}
			if (Object.keys(query).length) {
				query.offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
				query.limit = Object.keys(query).length ? query.limit ? query.limit : query.start ? query.start : query.limit ? 0 : null : null;
			} else {
				query.offset = null;
				query.limit = null;
			}

			if (idPoliza) {
				let dateToFind, dateWaitAfterDays, dateWaitBeforeDays;
				let numDaysWaitAfter = poliza.dias_espera_renovacion;
				let numDaysWaitBefore = poliza.dias_anticipado_renovacion;
				let date = new Date();
				dateToFind = moment(new Date()).subtract(1,"days").toDate();
				dateWaitBeforeDays = moment(new Date()).subtract(numDaysWaitBefore + 1,"days").toDate();
				dateWaitAfterDays = moment(new Date()).add(numDaysWaitAfter,'days').toDate();

				if (gracia) {
					dateToFind = moment(new Date()).add(gracia,'days').toDate();
				}

				respSolicitudes = await modelos.asegurado.findAll({
					// limit: query.limit ? parseInt(query.limit) : null,
					// offset: query.offset ? parseInt(query.offset) : 0,
					where: idInstanciaPoliza  ? {id_instancia_poliza: idInstanciaPoliza} : null,
					include:[{
						model: modelos.instancia_poliza,
						where:{id_estado: idPoliza == 7 ? [parEmitido.id] : [parEmitido.id, parSinVigencia.id],
							id_poliza:idPoliza},
						include:[
							{
								model: modelos.atributo_instancia_poliza,
								include: {model:modelos.objeto_x_atributo}
							},
							{model: modelos.poliza},
							{
								model:modelos.instancia_documento,
								where: idInstanciaPoliza === undefined ? { $and: [
					 				  {fecha_fin_vigencia: {$lte:dateToFind}},
									  {fecha_fin_vigencia:{$gte:dateWaitBeforeDays}},
									  {fecha_fin_vigencia:{$lte:dateWaitAfterDays}},
									]} : { $and: [
										{id_instancia_poliza: idInstanciaPoliza}
								  ]},
								include: {
									model:modelos.documento,
									where:{id:docCertificado.id},
									include: {
										model: modelos.parametro, as:'tipo_documento'
									}
								}
							}
						]
					}, {
						model: modelos.entidad,
						include: [{
							model: modelos.persona,
						}]
					}]
				});
				loggerSolicitud.info(`Info: Se encontraron ${respSolicitudes.length} instancias polizas (${respSolicitudes.map(param => param.id_instancia_poliza)}) en el rango de fechas: ${dateWaitBeforeDays} - ${dateToFind}`);
				let anexoAsegurado, edadMinimaYears, edadMaximaYears;
				let anexosPolizas = await modelos.anexo_poliza.findAll({
					where:{id_poliza:idPoliza, vigente:true},
					include: [{
						model: modelos.anexo_asegurado,
						include: [
							{ model: modelos.dato_anexo_asegurado },
							{ model: modelos.parametro },
							{ model: modelos.parametro, as:'edad_unidad' }
						]
					}]
				}).catch(err => {
					console.log(err);
					loggerCatch.info(new Date(),err);
				});

				for (let i = 0 ; i < anexosPolizas.length ; i++) {
					let anexoPoliza = anexosPolizas[i];
					anexoAsegurado = anexoPoliza.anexo_asegurados.find(param => param.id_tipo == 289);
					if(anexoAsegurado) {
						edadMinimaYears = anexoAsegurado.edad_min_permanencia;
						edadMaximaYears = anexoAsegurado.edad_max_permanencia;
					}
				}

				for (let i = 0 ; i < (query.limit ? parseInt(query.limit) : respSolicitudes.length) ; i++) {
					let isModoPagoAnual = false;
					let respSolicitud = respSolicitudes[i];
					let solicitud = respSolicitud.dataValues;
					let solDocCertificad = solicitud.instancia_poliza.instancia_documentos.find(param => param.id_documento == docCertificado.id);
					let atributoModalidadPago;
					if (poliza.id == 7) {
						atributoModalidadPago = solicitud.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.dataValues.id_atributo == objetoAtributoModalidadEcoProteccion);
						if (atributoModalidadPago && atributoModalidadPago.valor == parModPagoAnual) {
							isModoPagoAnual = true;
						}
					}
					if (isModoPagoAnual) {
						response.push({info:'Solicitud ecoproteccion con modalidad de pago anual', id_instancia_poliza:solicitud.instancia_poliza.id, data:{}});
						loggerSolicitud.warn(`4. No se renovó la instancia poliza ${solicitud.instancia_poliza.id} del prooducto ${poliza.descripcion} con estado ${parametros.find(param => param.id == solicitud.instancia_poliza.id_estado).parametro_descripcion} ya que tiene (${solicitud.instancia_poliza.instancia_documentos.length} documentos: ${solicitud.instancia_poliza.instancia_documentos.map(param => param.nro_documento)}) y su modalidad de pago es ${atributoModalidadPago ? parametros.find(param => param.id == atributoModalidadPago.valor).parametro_descripcion : ''} ${solDocCertificad ? ', el rango de vigencia es '+ solDocCertificad.fecha_inicio_vigencia+' - '+solDocCertificad.fecha_fin_vigencia : ''}`);
					}
						let solicitudInstanciaPoliza = solicitud.instancia_poliza && solicitud.instancia_poliza.dataValues ? solicitud.instancia_poliza.dataValues : null;
						if (solicitud.instancia_poliza.instancia_documentos.length) {
							for (let j = 0 ; j < solicitudInstanciaPoliza.instancia_documentos.length ; j++) {
								let respInstanciaDocumento = solicitudInstanciaPoliza.instancia_documentos[j];
								let instanciaDocumento = respInstanciaDocumento && respInstanciaDocumento.dataValues ? respInstanciaDocumento.dataValues : null;
								if (instanciaDocumento) {
									if (instanciaDocumento.documento.id == docCertificado.id) {
										let todayStr = `${util.minTwoDigits(date.getDate())}/${util.minTwoDigits(date.getMonth()+1)}/${date.getFullYear()}`;
										let fechaInicioVigencia = instanciaDocumento.fecha_inicio_vigencia;
										let fechaInicioVigenciaStr = `${util.minTwoDigits(fechaInicioVigencia.getDate())}/${util.minTwoDigits(fechaInicioVigencia.getMonth()+1)}/${fechaInicioVigencia.getFullYear()}`;
										let momFechaInicioVigencia = moment(fechaInicioVigencia);
										let momToday = moment(date);
										let idInstanciaDocumento = instanciaDocumento.id;
										if (!instanciaDocumento.fecha_fin_vigencia) {
											let newFechaFinVigenciaStr = fechas.addMeses(fechaInicioVigenciaStr,12);
											let [dayFin,monthFin,yearFin] = newFechaFinVigenciaStr.split('/');
											instanciaDocumento.fecha_fin_vigencia = new Date(yearFin,monthFin,dayFin);
											delete instanciaDocumento.id;
											let respInstanciaDocumento = await modelos.instancia_documento.update(instanciaDocumento, {where:{id:idInstanciaDocumento}});
											respInstanciaDocumento = await modelos.instancia_documento.findOne({where:{id:idInstanciaDocumento}});
											instanciaDocumento = respInstanciaDocumento.dataValues;
											loggerSolicitud.info(`La fecha de finalizacion de la instancia poliza ${solicitudInstanciaPoliza.id} fue actualizada con fecha de finalizacion ${newFechaFinVigenciaStr}, actualmente tiene el documento ${instanciaDocumento.nro_documento} `);
										}

										let fechaFinVigencia = instanciaDocumento.fecha_fin_vigencia;
										let fechaFinVigenciaStr = `${util.minTwoDigits(fechaFinVigencia.getDate())}/${util.minTwoDigits(fechaFinVigencia.getMonth()+1)}/${fechaFinVigencia.getFullYear()}`;
										let momFechaFinVigencia = moment(fechaFinVigencia);
										let diffMesesVigencia = momFechaFinVigencia.diff(momFechaInicioVigencia, 'months')+1;
										let vigenciaActual, vigenciaConGracia;
										if (gracia) {
											vigenciaConGracia = momToday.add(gracia,'days').diff(momFechaInicioVigencia, 'months')+1;
											momToday = moment(date);
											vigenciaActual = momToday.diff(momFechaInicioVigencia, 'months')+1;
										} else {
											vigenciaActual = momToday.diff(momFechaInicioVigencia, 'months')+1;
											momToday = moment(date);
											vigenciaConGracia = momToday.diff(momFechaInicioVigencia, 'months')+1;
										}
										let instanciaPoliza, respInstanciaPoliza, idInstanciaPoliza;
										loggerSolicitud.info(`La instancia poliza (${solicitud.instancia_poliza.id}) del producto ${poliza.descripcion} con estado ${parametros.find(param => param.id == solicitud.instancia_poliza.id_estado).parametro_descripcion}, con documentos ${solicitud.instancia_poliza.instancia_documentos.map(param => param.nro_documento)} tiene una vigencia de ${vigenciaActual} meses actualimente, cuyo inicio fue el ${fechaInicioVigenciaStr} y concluye el ${fechaFinVigenciaStr}`);
										let respInstanciaPolizaTransicions, respOldInstanciaPolizaTransicion, oldInstanciaPolizaTransicion, objInstanciaPolizaTransicions;

										if (solicitudInstanciaPoliza.poliza.vigencia_meses < vigenciaConGracia || bForceSinVigencia) {
											if (solicitudInstanciaPoliza.poliza.vigencia_meses < vigenciaActual || bForceSinVigencia) {
												solicitudInstanciaPoliza.id_estado = parSinVigencia.dataValues.id;
												idInstanciaPoliza = solicitudInstanciaPoliza.id;
												delete solicitudInstanciaPoliza.id;
												respInstanciaPoliza = await modelos.instancia_poliza.update(solicitudInstanciaPoliza, {where:{id:idInstanciaPoliza}});
												respInstanciaPoliza = await modelos.instancia_poliza.findOne({where:{id:idInstanciaPoliza}});

												// transicion Poliza
												respOldInstanciaPolizaTransicion = await modelos.instancia_poliza_transicion.findOne({where:{id_instancia_poliza:idInstanciaPoliza,par_estado_id:282}});
												oldInstanciaPolizaTransicion = respOldInstanciaPolizaTransicion && respOldInstanciaPolizaTransicion.dataValues ? respOldInstanciaPolizaTransicion.dataValues : null;
												if (oldInstanciaPolizaTransicion) {
													let oldInstanciaPolizaTransicionId = oldInstanciaPolizaTransicion.id;
													oldInstanciaPolizaTransicion.id_instancia_poliza = idInstanciaPoliza;
													oldInstanciaPolizaTransicion.par_estado_id = solicitudInstanciaPoliza.id_estado;
													oldInstanciaPolizaTransicion.observacio = `La vigencia del certificado expiró el: ${fechaFinVigenciaStr}, la solicitud fue actualizada al estado: ${parSinVigencia.dataValues.parametro_descripcion}.`;
													oldInstanciaPolizaTransicion.createdAt = moment(fechaFinVigencia).add(1,'days').toDate();
													oldInstanciaPolizaTransicion.updatedAt = moment(fechaFinVigencia).add(1,'days').toDate();
													delete oldInstanciaPolizaTransicion.id;
													respOldInstanciaPolizaTransicion = await modelos.instancia_poliza_transicion.update(oldInstanciaPolizaTransicion,{where:{id:oldInstanciaPolizaTransicionId}});
													respInstanciaPolizaTransicions = await modelos.instancia_poliza_transicion.findOne({where:{id:oldInstanciaPolizaTransicionId}});
												} else {
													let newInstanciaPolizaTrancision = {
														id_instancia_poliza: idInstanciaPoliza,
														par_estado_id: solicitudInstanciaPoliza.id_estado,
														par_observacion_id: '96',
														observacion: `La vigencia del certificado expiró el: ${fechaFinVigenciaStr}, la solicitud fue actualizada al estado: ${parSinVigencia.dataValues.parametro_descripcion}.`,
														adicionado_por: '454',
														modificado_por: '454',
														updatedAt: moment(fechaFinVigencia).add(1,'days').toDate(),
														createdAt: moment(fechaFinVigencia).add(1,'days').toDate()
													};
													respInstanciaPolizaTransicions = await modelos.instancia_poliza_transicion.create(newInstanciaPolizaTrancision);
												}
												instanciaPoliza = respInstanciaPoliza.dataValues;
												loggerSolicitud.info(`La instancia_poliza Nro ${instanciaPoliza.id} del producto ${poliza.descripcion} con estado ${parametros.find(param => param.id == solicitud.instancia_poliza.id_estado).parametro_descripcion},  con documentos ${solicitud.instancia_poliza.instancia_documentos.map(param => param.nro_documento)}, expiró, su vigencia fue desde el ${fechaInicioVigenciaStr} hasta el ${fechaFinVigenciaStr} '`);
											}

											let solicitudRenovada, newAsegurado, edadMinimaMiliseconds, edadMaximaMiliseconds;
											let edadAseguradoMilliseconds = new Date().getTime() - solicitud.entidad.persona.persona_fecha_nacimiento.getTime();
											let edadAseguradoYears;
											let tieneEdad = false;
											if (anexoAsegurado && edadMinimaYears && edadMaximaYears) {
												switch (anexoAsegurado.id_edad_unidad) {
													case '241':
														edadAseguradoYears = edadAseguradoMilliseconds / 31556900000;
														edadMinimaMiliseconds = edadMinimaYears * 31556900000;
														edadMaximaMiliseconds = edadMaximaYears * 31556900000;
														break;
													case '240':
														edadAseguradoYears = edadAseguradoMilliseconds / 2629750000;
														edadMinimaMiliseconds = edadMinimaYears * 2629750000;
														edadMaximaMiliseconds = edadMaximaYears * 2629750000;
														break;
													case '239':
														edadAseguradoYears = edadAseguradoMilliseconds / 86400000;
														edadMinimaMiliseconds = edadMinimaYears * 86400000;
														edadMaximaMiliseconds = edadMaximaYears * 86400000;
														break;
												}
												tieneEdad = edadAseguradoMilliseconds >= edadMinimaMiliseconds && edadAseguradoMilliseconds < edadMaximaMiliseconds ;
											}

											if ((poliza && poliza.renovacion_automatica && tieneEdad && !isModoPagoAnual) || manual) {
												let respAsegurado = await solicitudService.findPersonaSolicitudByIdInstanciaPoliza(solicitud.id_instancia_poliza);
												let asegurado = respAsegurado && respAsegurado.dataValues ? respAsegurado.dataValues : null;
												let respInstanciaDocumentoCertificado = asegurado.instancia_poliza.instancia_documentos.find(param => param.documento.id == docCertificado.id);
												if (respInstanciaDocumentoCertificado) {
													let instanciaDocumentoCertificado = respInstanciaDocumentoCertificado.dataValues;
													let momFechaInicioVigencia = moment(instanciaDocumentoCertificado.fecha_inicio_vigencia);
													let momFechaFinVigencia = moment(instanciaDocumentoCertificado.fecha_fin_vigencia);
													let momToday = moment(new Date());
													let instanciasRenovadas = await modelos.instancia_poliza.findAll({
														where:{id_instancia_renovada:solicitud.instancia_poliza.id},
														include:{
															model:modelos.instancia_documento,
															include: {
																model:modelos.documento,
																where:{id:docCertificado.id},
																include: {
																	model: modelos.parametro, as:'tipo_documento'
																}
															}
														}
													});
													for (let j = 0 ; j < instanciasRenovadas.length ; j++) {
														let instanciaRenovada = instanciasRenovadas[j];
														let documentoCertificado = instanciaRenovada.instancia_documentos.find(param => param.documento.id == docCertificado.id);
														let momFechaInicioVigencia = moment(documentoCertificado.fecha_inicio_vigencia);
														let momFechaFinVigencia = moment(documentoCertificado.fecha_fin_vigencia);
														let today = moment(new Date());
														let diffMonths = today.diff(momFechaInicioVigencia, 'months');
														if (solicitudInstanciaPoliza.poliza.vigencia_meses < diffMonths) {
															let req = {params:{}};
															req.params.idPoliza = idPoliza;
															req.params.idInstanciaPoliza = solicitudInstanciaPoliza.id;
															let respVigencia = await this.verificaVigencia(req);
															let respRenuevaSolicitud = await this.renuevaSolicitudes(req);
														}
													}
													if(solicitudInstanciaPoliza.poliza.certificado_correlativo) {
														solicitudRenovada = await solicitudService.createSolicitud(asegurado,idPoliza,idEstado, usuario, true, gracia, idSucursal, idAgencia);
													} else {
														solicitudRenovada = await solicitudService.createSolicitud(asegurado,idPoliza,idEstado, usuario, false, gracia, idSucursal, idAgencia);
													}
													if (solicitudRenovada && poliza.plan_pago) {
														let respPlanPago = await this.createPlanPago(solicitudRenovada)
													}

													// if (!instanciasRenovadas.length) {
													// 	if(solicitudInstanciaPoliza.poliza.certificado_correlativo) {
													// 		solicitudRenovada = await solicitudService.createSolicitud(asegurado,idPoliza,idEstado, usuario, true, gracia);
													// 	} else {
													// 		solicitudRenovada = await solicitudService.createSolicitud(asegurado,idPoliza,idEstado, usuario, false, gracia);
													// 	}
													// 	if (solicitudRenovada && poliza.plan_pago) {
													// 		let respPlanPago = await this.createPlanPago(solicitudRenovada)
													// 	}
													// } else {
													// 	for (let k = 0 ; k < instanciasRenovadas.length ; k++) {
													// 		let instanciasRenovada = instanciasRenovadas[k];
													// 		let respSolicitudRenovada = await solicitudService.findPersonaSolicitudByIdInstanciaPoliza(instanciasRenovada.id);
													// 		solicitudRenovada = respSolicitudRenovada && respSolicitudRenovada.dataValues ? respSolicitudRenovada.dataValues : null;
													//
													// 		if (solicitudRenovada && poliza.plan_pago) {
													// 			//let respPlanPago = await this.createPlanPago(solicitudRenovada)
													// 		}
													// 	}
													// 	newAsegurado = respAsegurado && respAsegurado.dataValues ? respAsegurado.dataValues : null;
													// }


													// response.push({id_instancia_poliza: solicitudRenovada.instancia_poliza.id});
												}
												response.push({info:`La solicitud fue renovada exitosamente, id_instancia_renovada: ${solicitudRenovada.instancia_poliza.id}`, id_instancia_poliza:solicitudRenovada.instancia_poliza.id, data:solicitudRenovada});
												loggerSolicitud.info(`Se renovó exitosamente la instancia poliza ${solicitud.instancia_poliza.id}  del producto ${poliza.descripcion} con estado ${parametros.find(param => param.id == solicitud.instancia_poliza.id_estado).parametro_descripcion}, con documentos ${solicitud.instancia_poliza.instancia_documentos.map(param => param.nro_documento)}`);
												console.log(`Se verificó la solicitud cuyo ID: ${solicitud.id}, instancia poliza ${solicitud.instancia_poliza.id}, instancia Documento: ${instanciaDocumento.id}, documento ${instanciaDocumento.documento.descripcion}`, instanciaDocumento);
											} else {
												response.push({info:`No se renovo la instancia poliza: ${solicitud.id_instancia_poliza} de la poliza ${poliza.descripcion}`});
												loggerSolicitud.warn(`1. No se renovó la instancia poliza ${solicitud.id_instancia_poliza}  del producto ${poliza.descripcion} con estado ${parametros.find(param => param.id == solicitud.instancia_poliza.id_estado).parametro_descripcion}, con documentos ${solicitud.instancia_poliza.instancia_documentos.map(param => param.nro_documento)}, la poliza ${poliza.renovacion_automatica?'tiene':'no tiene'} renovacion automatica y el cliente ${tieneEdad?'tiene':'no tiene'} la edad correspondiente, la edad del asegurado :${edadAseguradoYears} no es menor a ${edadMinimaYears} o mayor igual a ${edadMaximaYears}`);
												console.log(`Se verifico la solicitud cuyo ID: ${solicitud.id}, instancia poliza ID: ${solicitud.id_instancia_poliza}, instancia Documento: ${instanciaDocumento.id}, documento ${instanciaDocumento.documento.descripcion}`, instanciaDocumento);
											}
										} else {
											response.push({info:'Solicitud No Caduca', id_instancia_poliza:solicitud.instancia_poliza.id, data:{}});
											loggerSolicitud.warn(`2. No se renovó la instancia poliza ${solicitud.instancia_poliza.id} del producto ${poliza.descripcion} con estado ${parametros.find(param => param.id == solicitud.instancia_poliza.id_estado).parametro_descripcion}, con documentos ${solicitud.instancia_poliza.instancia_documentos.map(param => param.nro_documento)}, no caducó, su vigencia en meses ${solicitudInstanciaPoliza.poliza.vigencia_meses} no es menor a los dias de vigencia con gracia ${vigenciaConGracia}`);
											console.log(`Info: La instancia poliza ${solicitud.instancia_poliza.id}, instancia Documento: ${instanciaDocumento.id}, documento ${instanciaDocumento.documento.descripcion}`, instanciaDocumento);
										}
									}
								}
							}
						} else {
							response.push({info:'Solicitud sin documentos', id_instancia_poliza:solicitud.instancia_poliza.id, data:{}});
							loggerSolicitud.warn(`3. No se renovó la instancia poliza ${solicitudInstanciaPoliza.id} del producto ${solicitudInstanciaPoliza.poliza.descripcion} con estado ${parametros.find(param => param.id == solicitud.instancia_poliza.id_estado).parametro_descripcion} ya que tiene (${solicitudInstanciaPoliza.instancia_documentos.length} documentos ${solicitud.instancia_poliza.instancia_documentos.map(param => param.nro_documento)} su modalidad de pago es ${parametros.find(param => param.id == atributoModalidadPago.valor).parametro_descripcion} ${solDocCertificad ? ', el rango de vigencia es '+ solDocCertificad.fecha_inicio_vigencia+' - '+solDocCertificad.fecha_fin_vigencia : ''}`);
						}
				}

			}
			return response;
		} catch (e) {
			console.log(e);
			loggerCatch.info(new Date(),e);
		}
	}

	static async createPlanPago(asegurado) {
		try {
			let parametros = await modelos.parametro.findAll({where:{diccionario_id:[31]}});
			let parMensual = parametros.find(param => param.id == 90);
			let parAnual = parametros.find(param => param.id == 93);

			let atributoMonto = asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.atributo.id == 61);
			let atributoCuota = asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.atributo.id == 76);
			let atributoPlazoAnos = asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.atributo.id == 58);
			let atributoModalidad = asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.atributo.id == 28);
			let atributoMoneda = asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.atributo.id == 21);

			let respGeneraPlanPago;
			let respInstanciaDocumentoCertificado = asegurado.instancia_poliza.instancia_documentos.find(param => param.documento.tipo_documento.id == 281);
			let instanciaDocumentoCertificado = respInstanciaDocumentoCertificado.dataValues ? respInstanciaDocumentoCertificado.dataValues : respInstanciaDocumentoCertificado;
			if (instanciaDocumentoCertificado) {
				let planPago = {
					id_instancia_poliza : asegurado.instancia_poliza.id,
					total_prima : atributoCuota && atributoMonto ? parseInt(atributoCuota.valor) * parseInt(atributoMonto.valor) : 0,
					interes: 0,
					plazo_anos: 1,
					periodicidad_anual: atributoModalidad && atributoModalidad.valor == parMensual.id+'' ? 12 : atributoModalidad.valor == parAnual.id+'' ? 1 : 0,
					prepagable_postpagable: 1,
					id_moneda: parseInt(atributoMoneda.valor),
					fecha_inicio: instanciaDocumentoCertificado.fecha_inicio_vigencia,
					adicionado_por : asegurado.adicionado_por,
					modificado_por : asegurado.modificado_por
				}
				respGeneraPlanPago = await GenerarPlanPagos(planPago);
			}
			return respGeneraPlanPago;
		} catch (e) {
			console.log(e);
			loggerCatch.info(new Date(),e)
		}
	}

	static async renuevaSolicitudes(req,res = null) {
		try {
			let solicitudRenovada;
			let { idPoliza } = req.params;
			let { idEstado } = req.params;
			let query = req.query;
			let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;

			let { idInstanciaPoliza } = req.params;
            let respObjetoAtributoDebito = await modelos.objeto_x_atributo.findOne({
				include:[{
                    model:modelos.objeto,
                    where:{id_poliza:idPoliza}
                }, {
					model: modelos.atributo,
					where:{id:12}
				}]
			});

      let objetoAtributoDebito = respObjetoAtributoDebito && respObjetoAtributoDebito.dataValues ? respObjetoAtributoDebito.dataValues : null;
			let idUsuario = req.session && req.session.passport ? req.session.passport.user : null;
			let respUsuario,usuario;
			if (idUsuario) {
				respUsuario = await modelos.usuario.findOne({where: {id: idUsuario}});
				usuario = respUsuario && respUsuario.dataValues ? respUsuario.dataValues : null;
			}

			let date = new Date();
			let response = { status: 'OK', message: '', data: [], errors:[] };
			let parametros = await modelos.parametro.findAll({where:{diccionario_id:[11,52,31]}});
			let parSinVigencia = parametros.find(param => param.id == 282);
			let parTipoDocumentoSolicitud = parametros.find(param => param.id == 279);
			let parTipoDocumentoComprobante = parametros.find(param => param.id == 280);
			let parTipoDocumentoCertificado = parametros.find(param => param.id == 281);
			let newAsegurado;

			if (idPoliza) {
				let respPoliza = await modelos.poliza.findOne({where:{id:idPoliza}});
				let poliza = respPoliza && respPoliza.dataValues ? respPoliza.dataValues : null;
        let yesterday =moment().subtract(1,'days').toDate();
        let where = idInstanciaPoliza ? {id_estado:parSinVigencia.id, id_poliza:idPoliza, id: idInstanciaPoliza} : {id_estado:parSinVigencia.id, id_poliza:idPoliza};
        let respSolicitudes = await modelos.asegurado.findAll({
	        // limit: query.limit ? parseInt(query.limit) : null,
	        // offset: offset ? parseInt(offset) : 0,
					include:{
						model: modelos.instancia_poliza,
						where:where,
						include:[
							{
								model: modelos.poliza,
								where: {renovacion_automatica:true}
							},
							{
								model:modelos.instancia_documento,
								where: idInstanciaPoliza === undefined ? {fecha_fin_vigencia:{$lte:yesterday}} : null,
								include: {
									model:modelos.documento,
									where:{id_tipo_documento:parTipoDocumentoCertificado.id},
									include: {
										model: modelos.parametro, as:'tipo_documento'
									}
								}
							},{
								model: modelos.atributo_instancia_poliza,
								// where: {id_objeto_x_atributo:objetoAtributoDebito.id, valor:58}
							}
						]
					}
				});

				let date = new Date();
				for (let i = 0 ; i < (query.limit ? parseInt(query.limit) : respSolicitudes.length) ; i++) {
					let respSolicitud = respSolicitudes[i];
					let solicitud = respSolicitud.dataValues;
					let solicitudInstanciaPoliza = solicitud.instancia_poliza && solicitud.instancia_poliza.dataValues ? solicitud.instancia_poliza.dataValues : null;
					if (solicitudInstanciaPoliza.id_instancia_renovada){
						let respAsegurado = await solicitudService.findPersonaSolicitudByIdInstanciaPoliza(solicitud.instancia_poliza.id_instancia_renovada);
						// let asegurado = respAsegurado && respAsegurado.dataValues ? respAsegurado.dataValues : null;
						// let instanciaDocumentoCertificado = asegurado.instancia_poliza.instancia_documentos.find(param => param.documento.tipo_documento.id == parTipoDocumentoCertificado.dataValues.id);
						// if (instanciaDocumentoCertificado) {
						// 	let momFechaInicioVigencia = moment(instanciaDocumentoCertificado.fecha_inicio_vigencia);
						// 	let momFechaFinVigencia = moment(instanciaDocumentoCertificado.fecha_fin_vigencia);
						// 	let momToday = moment(new Date());
						// 	let diffFromToday = momToday.diff(momFechaFinVigencia,'days');
						// 	if (diffFromToday <= solicitudInstanciaPoliza.poliza.dias_espera_renovacion) {
						// 		if(solicitudInstanciaPoliza.poliza.certificado_correlativo) {
						// 			let respNewAsegurado = await solicitudService.createSolicitud(asegurado,idPoliza,idEstado,usuario);
						// 		} else {
						// 			let respNewAsegurado = await solicitudService.createSolicitud(asegurado,idPoliza,idEstado,usuario,false);
						// 		}
						// 	}
						// }
					} else {
						let respAsegurado = await solicitudService.findPersonaSolicitudByIdInstanciaPoliza(solicitud.instancia_poliza.id);
						let asegurado = respAsegurado && respAsegurado.dataValues ? respAsegurado.dataValues : null;
						let respInstanciaDocumentoCertificado = asegurado.instancia_poliza.instancia_documentos.find(param => param.documento.tipo_documento.id == parTipoDocumentoCertificado.id);
						if (respInstanciaDocumentoCertificado) {
							let instanciaDocumentoCertificado = respInstanciaDocumentoCertificado.dataValues;
							let momFechaInicioVigencia = moment(instanciaDocumentoCertificado.fecha_inicio_vigencia);
							let momFechaFinVigencia = moment(instanciaDocumentoCertificado.fecha_fin_vigencia);
							let momToday = moment(new Date());
							let instanciasRenovadas = await modelos.instancia_poliza.findAll({
								where:{id_instancia_renovada:solicitud.instancia_poliza.id},
								include:{
									model:modelos.instancia_documento,
									include: {
										model:modelos.documento,
										where:{id_tipo_documento:parTipoDocumentoCertificado.id},
										include: {
											model: modelos.parametro, as:'tipo_documento'
										}
									}
								}
							});
							for (let j = 0 ; j < instanciasRenovadas.length ; j++) {
								let instanciaRenovada = instanciasRenovadas[j];
								let documentoCertificado = instanciaRenovada.instancia_documentos.find(param => param.documento.tipo_documento.id == parTipoDocumentoCertificado.id);
								let momFechaInicioVigencia = moment(documentoCertificado.fecha_inicio_vigencia);
								let momFechaFinVigencia = moment(documentoCertificado.fecha_fin_vigencia);
								let today = moment(new Date());
								let diffMonths = today.diff(momFechaInicioVigencia, 'months');
								if (solicitudInstanciaPoliza.poliza.vigencia_meses < diffMonths) {
									let req = {params:{}};
									req.params.idPoliza = idPoliza;
									req.params.idInstanciaPoliza = solicitudInstanciaPoliza.id;
									let respVigencia = await this.verificaVigencia(req);
									let respRenuevaSolicitud = await this.renuevaSolicitudes(req);
								}
							}
							if (!instanciasRenovadas.length) {
								if(solicitudInstanciaPoliza.poliza.certificado_correlativo) {
									solicitudRenovada = await solicitudService.createSolicitud(asegurado,idPoliza,idEstado,usuario);
								} else {
									solicitudRenovada = await solicitudService.createSolicitud(asegurado,idPoliza,idEstado,usuario,false);
								}
								if (solicitudRenovada) {
									let respPlanPago = await this.createPlanPago(solicitudRenovada)
								}
							} else {
								for (let k = 0 ; k < instanciasRenovadas.length ; k++) {
									let instanciasRenovada = instanciasRenovadas[k];
									let respSolicitudRenovada = await solicitudService.findPersonaSolicitudByIdInstanciaPoliza(instanciasRenovada.id);
									solicitudRenovada = respSolicitudRenovada && respSolicitudRenovada.dataValues ? respSolicitudRenovada.dataValues : null;

									if (solicitudRenovada && solicitudRenovada.instancia_poliza.id_poliza == 7) {
										let respPlanPago = await this.createPlanPago(solicitudRenovada)
									}
								}
								newAsegurado = respAsegurado && respAsegurado.dataValues ? respAsegurado.dataValues : null;
							}
							response.data.push({id_instancia_poliza: solicitudRenovada.instancia_poliza.id});
						}
					}
				}
			}
			return response;
		} catch (e) {
			console.log(e)
			loggerCatch.info(new Date(),e);
		}
	}
}

module.exports = VigenciaService;
