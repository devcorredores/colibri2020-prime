const modelos = require("../modelos");
const moment = require("moment");
const util = require("../utils/util");
const {loggerCatch} = require("../modulos/winston");

class SolicitudService {

  static async isLoggedIn(req, res, next) {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
    }
    return next()
    //return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
  }

  static async setInstanciaDocumentos(instanciadocumentos = []) {
    try {
      let toReturn = {};
      if (instanciadocumentos && instanciadocumentos.length) {
        for (let i = 0; i < instanciadocumentos.length; i++) {
          let instanciadocumento = instanciadocumentos[i].dataValues;
          if (instanciadocumento.documento) {
            switch (parseInt(instanciadocumento.documento.id_tipo_documento)) {
              case 279:
                toReturn.documentoSolicitud = instanciadocumento;
                break; // Diccionary ID¡: 1
              case 280:
                toReturn.documentoComprobante = instanciadocumento;
                break; // Diccionary ID¡: 1
              case 281:
                toReturn.documentoCertificado = instanciadocumento;
                break; // Diccionary ID¡: 1
            }
          }
        }
      }
      return toReturn;
    } catch (e) {
      console.log(e);
      loggerCatch.info(new Date(),e);
    }
  }

  static async setAtributoInstanciaPoliza(atributosInstanciaPoliza = []) {
    try {
      let toReturn = {};
      if (atributosInstanciaPoliza && atributosInstanciaPoliza.length) {
        for (let i = 0; i < atributosInstanciaPoliza.length; i++) {
          let atributoInstanciaPoliza = atributosInstanciaPoliza[i].dataValues;
          if (
            atributoInstanciaPoliza.objeto_x_atributo &&
            atributoInstanciaPoliza.objeto_x_atributo.atributo
          ) {
            switch (
              parseInt(atributoInstanciaPoliza.objeto_x_atributo.atributo.id)
            ) {
              case 1:
                toReturn.atributoPrima = atributoInstanciaPoliza;
                break;
              case 2:
                toReturn.atributoValorAsegurado = atributoInstanciaPoliza;
                break;
              case 3:
                toReturn.atributoEstadoCivil = atributoInstanciaPoliza;
                break;
              case 4:
                toReturn.atributoCorreoElectronico = atributoInstanciaPoliza;
                break;
              case 5:
                toReturn.atributoCuentaBancaria = atributoInstanciaPoliza;
                break;
              case 9:
                toReturn.atributoTipoDebito = atributoInstanciaPoliza;
                break;
              case 10:
                toReturn.atributoCodigoAgenda = atributoInstanciaPoliza;
                break;
              case 12:
                toReturn.atributoDebitoAutomatico = atributoInstanciaPoliza;
                break;
              case 14:
                toReturn.atributoCodigoCAEDEC = atributoInstanciaPoliza;
                break;
              case 16:
                toReturn.atributoLocalidad = atributoInstanciaPoliza;
                break;
              case 17:
                toReturn.atributoDepartamento = atributoInstanciaPoliza;
                break;
              case 18:
                toReturn.atributoCodigoSucursal = atributoInstanciaPoliza;
                break;
              case 19:
                toReturn.atributoTipoDocumento = atributoInstanciaPoliza;
                break;
              case 20:
                toReturn.atributoManejo = atributoInstanciaPoliza;
                break;
              case 21:
                toReturn.atributoMoneda = atributoInstanciaPoliza;
                break;
              case 22:
                toReturn.atributoCAEDEC = atributoInstanciaPoliza;
                break;
              case 23:
                toReturn.atributoNrotarjeta = atributoInstanciaPoliza;
                break;
              case 24:
                toReturn.atributoNombreTarjeta = atributoInstanciaPoliza;
                break;
              case 25:
                toReturn.atributoTarjetaValida = atributoInstanciaPoliza;
                break;
              case 26:
                toReturn.atributoTarjetaTipo = atributoInstanciaPoliza;
                break;
              case 28:
                toReturn.atributoModalidadPago = atributoInstanciaPoliza;
                break;
              case 29:
                toReturn.atributoZona = atributoInstanciaPoliza;
                break;
              case 30:
                toReturn.atributoNroDireccion = atributoInstanciaPoliza;
                break;
              case 32:
                toReturn.atributoNombreRazonSocial = atributoInstanciaPoliza;
                break;
              case 33:
                toReturn.atributoNitCarnet = atributoInstanciaPoliza;
                break;
              case 34:
                toReturn.atributoUltimosCuatroDigitosNumeroTarjeta =
                  atributoInstanciaPoliza;
                break;
              case 35:
                toReturn.atributoFechaExpiracionCuenta = atributoInstanciaPoliza;
                break;
              case 36:
                toReturn.atributoTipoTarjeta = atributoInstanciaPoliza;
                break;
              case 37:
                toReturn.atributoOcupacion = atributoInstanciaPoliza;
                break;
              case 38:
                toReturn.atributoPlan = atributoInstanciaPoliza;
                break;
              case 43:
                toReturn.atributoPorcentaje = atributoInstanciaPoliza;
                break;
              case 44:
                toReturn.atributoRelacion = atributoInstanciaPoliza;
                break;
              case 46:
                toReturn.atributoDescripcion = atributoInstanciaPoliza;
                break;
              case 48:
                toReturn.atributoEdadMaxAdmisionTitular = atributoInstanciaPoliza;
                break;
              case 50:
                toReturn.atributoEdadLimiteAdmisionDependientes = atributoInstanciaPoliza;
                break;
              case 53:
                toReturn.atributoEdadMinAdmisionTitular = atributoInstanciaPoliza;
                break;
              case 54:
                toReturn.atributoEdadLimiteAdmisionDependiente = atributoInstanciaPoliza;
                break;
              case 55:
                toReturn.atributoNroMaximoDependientes = atributoInstanciaPoliza;
                break;
              case 57:
                toReturn.atributoDeclaracionObligatoriaDependientes = atributoInstanciaPoliza;
                break;
              case 58:
                toReturn.atributoPlazo = atributoInstanciaPoliza;
                break;
              case 59:
                toReturn.atributoOficina = atributoInstanciaPoliza;
                break;
              case 60:
                toReturn.atributoSucursal = atributoInstanciaPoliza;
                break;
              case 61:
                toReturn.atributoMonto = atributoInstanciaPoliza;
                break;
              case 62:
                toReturn.atributoTelefonoCelular = atributoInstanciaPoliza;
                break;
              case 63:
                toReturn.atributoCiudadNacimiento = atributoInstanciaPoliza;
                break;
              case 64:
                toReturn.atributoNumerotransaccion = atributoInstanciaPoliza;
                break;
              case 65:
                toReturn.atributoFechaTransaccion = atributoInstanciaPoliza;
                break;
              case 66:
                toReturn.atributoImporteTransaccion = atributoInstanciaPoliza;
                break;
              case 67:
                toReturn.atributoMonedaTransaccion = atributoInstanciaPoliza;
                break;
              case 69:
                toReturn.atributoDetalleTransaccion = atributoInstanciaPoliza;
                break;
              case 70:
                toReturn.atributoCargoOficial = atributoInstanciaPoliza;
                break;
              case 71:
                toReturn.atributoCondicionCargoPublicoPoliticoJerarquico = atributoInstanciaPoliza;
                break;
              case 72:
                toReturn.atributoCargoEntidadPublicoPoliticoJerarquico = atributoInstanciaPoliza;
                break;
              case 73:
                toReturn.atributoPeriodoCargoPublicoPoliticoJerarquico = atributoInstanciaPoliza;
                break;
              case 74:
                toReturn.atributoDireccionLaboral = atributoInstanciaPoliza;
                break;
              case 75:
                toReturn.atributoTipoCuenta = atributoInstanciaPoliza;
                break;
              case 76:
                toReturn.atributoNroCuotas = atributoInstanciaPoliza;
                break;
              case 77:
                toReturn.atributoDescripcionOcupacion = atributoInstanciaPoliza;
                break;
            }
          }
        }
      }
      return toReturn;
    } catch (e) {
      console.log(e);
      loggerCatch.info(new Date(),e);
    }
  }

  static async setParametros(diccionarios = []) {
    try {
      let toReturn = {};
      if (diccionarios.length) {
        let parametros = await modelos.parametro.findAll({
          where: { diccionario_id: diccionarios },
        });
        if (parametros && parametros.length) {
          toReturn.parObservaciones = [];

          toReturn.tipoDocumentos = [];
          toReturn.nacionalidad = [];
          toReturn.pais = [];
          toReturn.sexo = [];
          toReturn.estadosUsuario = [];
          toReturn.tipoAutenticacion = [];
          toReturn.tipoPersona = [];
          toReturn.subEstadoPlanPago = [];
          toReturn.tipoAtributo = [];
          toReturn.estadosInstanciaPoliza = [];
          toReturn.gestionClaves = [];
          toReturn.ramo = [];
          toReturn.estadoCertificadoCobertura = [];
          toReturn.estadosComponente = [];
          toReturn.lugarExtencionDocId = [];
          toReturn.parentesco = [];
          toReturn.condicion = [];
          toReturn.moneda = [];
          toReturn.documentoPoliza = [];
          toReturn.estadosBeneficiario = [];
          toReturn.tipoBeneficiario = [];
          toReturn.tipoNumeriacionDocumentos = [];
          toReturn.periodicidaTemporal = [];
          toReturn.tiposObservaciones = [];
          toReturn.tipoTarjeta = [];
          toReturn.tipoAnexo = [];
          toReturn.tipoObjeto = [];
          toReturn.sucursalesEcofuturo = [];
          toReturn.tipoAnexo = [];
          toReturn.oficinasEcofuturo = [];
          toReturn.unidadTiempo = [];
          toReturn.tipoArchivo = [];
          toReturn.tipoComkparacion = [];
          toReturn.tipoCuentas = [];
          toReturn.estadoFtp = [];
          toReturn.estadoPlanPago = [];
          toReturn.estadoEnvio = [];
          toReturn.formaEnvio = [];
          toReturn.reporteTipos = [];
          toReturn.estadoPlan = [];
          toReturn.tiposDocumentos = [];
          toReturn.calendarioFestivo = [];
          toReturn.filtroFechas = [];
          toReturn.parametros = [];

          for (let i = 0; i < parametros.length; i++) {
            let parametro = parametros[i].dataValues;
            toReturn.parametros.push(parametro);
            switch (parseInt(parametro.id)) {
              case 1:
                toReturn.parCedulaIdentidad = parametro;
                toReturn.tipoDocumentos[parametro.parametro_cod] = parametro.parametro_descripcion;
                break;
              case 2:
                toReturn.parCedulaExtranjero = parametro;
                toReturn.tipoDocumentos[parametro.parametro_cod] = parametro.parametro_descripcion;
                break;
              case 3:
                toReturn.parPasaporte = parametro;
                toReturn.tipoDocumentos[parametro.parametro_cod] = parametro.parametro_descripcion;
                break;
              case 4:
                toReturn.parBoliviana = parametro;
                toReturn.nacionalidad[parametro.parametro_cod] = parametro.parametro_descripcion;
                break;
              case 5:
                toReturn.parBolivia = parametro;
                break; // Diccionary ID¡: 3 descripcion: NACIONALIDAD
              case 6:
                toReturn.parMasculino = parametro;
                break; // Diccionary ID¡: 4 descripcion: PAIS
              case 7:
                toReturn.parFemenino = parametro;
                break; // Diccionary ID¡: 4 descripcion: SEXO
              case 8:
                toReturn.parActivo = parametro;
                break; // Diccionary ID¡: 5 descripcion: SEXO
              case 9:
                toReturn.parInactivo = parametro;
                break; // Diccionary ID¡: 5 descripcion: ESTADOS DEL USUARIO
              case 10:
                toReturn.parLocal = parametro;
                break; // Diccionary ID¡: 6 descripcion: ESTADOS DEL USUARIO
              case 11:
                toReturn.parExternaEco = parametro;
                break; // Diccionary ID¡: 6 descripcion: TIPO DE AUTENTICACION
              case 12:
                toReturn.parPersonaNatural = parametro;
                break; // Diccionary ID¡: 7 descripcion: TIPO DE AUTENTICACION
              case 13:
                toReturn.parPersonaJuridica = parametro;
                break; // Diccionary ID¡: 7 descripcion: TIPO PERSONA
              case 14:
                toReturn.parActivo = parametro;
                break; // Diccionary ID¡: 8 descripcion: TIPO PERSONA
              case 17:
                toReturn.parInactivo = parametro;
                break; // Diccionary ID¡: 8 descripcion: SUB ESTADO PLAN DE PAGO
              case 272:
                toReturn.parCuentaSinSaldoParaDebito = parametro;
                break; // Diccionary ID¡: 8 descripcion: SUB ESTADO PLAN DE PAGO
              case 273:
                toReturn.parCobradoSegunMigracion = parametro;
                break; // Diccionary ID¡: 8 descripcion: SUB ESTADO PLAN DE PAGO
              case 274:
                toReturn.parPreguntarBanco = parametro;
                break; // Diccionary ID¡: 8 descripcion: SUB ESTADO PLAN DE PAGO
              case 275:
                toReturn.parCuentaCerrada = parametro;
                break; // Diccionary ID¡: 8 descripcion: SUB ESTADO PLAN DE PAGO
              case 18:
                toReturn.parLocal = parametro;
                break; // Diccionary ID¡: 9 descripcion: SUB ESTADO PLAN DE PAGO
              case 19:
                toReturn.parEXTERNO = parametro;
                break; // Diccionary ID¡: 9 descripcion: .
              case 20:
                toReturn.parCADENA = parametro;
                break; // Diccionary ID¡: 10 descripcion: .
              case 21:
                toReturn.parNUMERICO = parametro;
                break; // Diccionary ID¡: 10 descripcion: TIPO DE ATRIBUTO
              case 22:
                toReturn.parFECHA = parametro;
                break; // Diccionary ID¡: 10 descripcion: TIPO DE ATRIBUTO
              case 23:
                toReturn.parImagenJPG = parametro;
                break; // Diccionary ID¡: 10 descripcion: TIPO DE ATRIBUTO
              case 24:
                toReturn.parIniciado = parametro;
                break; // Diccionary ID¡: 11 descripcion: TIPO DE ATRIBUTO
              case 81:
                toReturn.parSolicitado = parametro;
                break; // Diccionary ID¡: 11 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 238:
                toReturn.parPorPagar = parametro;
                break; // Diccionary ID¡: 11 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 282:
                toReturn.parSinVigencia = parametro;
                toReturn.parEstadoSinVigencia = parametro;
                break; // Diccionary ID¡: 11 descripcion: CALENDARIO FESTIVObreak;
              case 59:
                toReturn.parEMITIDO = parametro;
                break; // Diccionary ID¡: 11 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 60:
                toReturn.parANULADO = parametro;
                break; // Diccionary ID¡: 11 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 62:
                toReturn.parDESISTIDO = parametro;
                break; // Diccionary ID¡: 11 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 63:
                toReturn.parCOBRADO = parametro;
                break; // Diccionary ID¡: 11 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 65:
                toReturn.parPLATAFORMA = parametro;
                break; // Diccionary ID¡: 11 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 25:
                toReturn.parASISTIDA = parametro;
                break; // Diccionary ID¡: 13 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 26:
                toReturn.parAUTONONOMA = parametro;
                break; // Diccionary ID¡: 13 descripcion: GESTION DE CLAVES
              case 27:
                toReturn.parAccidentesPersonales = parametro;
                break; // Diccionary ID¡: 14 descripcion: GESTION DE CLAVES
              case 28:
                toReturn.parVidaGrupoCortoplazo = parametro;
                break; // Diccionary ID¡: 14 descripcion: RAMO
              case 29:
                toReturn.parVidaGrupoLargoPlazo = parametro;
                break; // Diccionary ID¡: 14 descripcion: RAMO
              case 109:
                toReturn.parSALUD = parametro;
                break; // Diccionary ID¡: 14 descripcion: RAMO
              case 30:
                toReturn.parEnSolicitud = parametro;
                break; // Diccionary ID¡: 15 descripcion: RAMO
              case 31:
                toReturn.parEMITIDO = parametro;
                break; // Diccionary ID¡: 15 descripcion: ESTADO DE CERTIFICADO DE COBERTURA
              case 32:
                toReturn.parINVISIBLE = parametro;
                break; // Diccionary ID¡: 17 descripcion: ESTADO DE CERTIFICADO DE COBERTURA
              case 33:
                toReturn.parNoEditable = parametro;
                break; // Diccionary ID¡: 17 descripcion: ESTADOS DE COMPONENTE
              case 70:
                toReturn.parVISIBLE = parametro;
                break; // Diccionary ID¡: 17 descripcion: ESTADOS DE COMPONENTE
              case 71:
                toReturn.parEDITABLE = parametro;
                break; // Diccionary ID¡: 17 descripcion: ESTADOS DE COMPONENTE
              case 34:
                toReturn.parCHUQUISACA = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 37:
                toReturn.parLaPaz = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 38:
                toReturn.parCOCHABAMBA = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 39:
                toReturn.parORURO = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 40:
                toReturn.parPOTOSI = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 41:
                toReturn.parTARIJA = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 42:
                toReturn.parSantaCruz = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 43:
                toReturn.parBENI = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 44:
                toReturn.parPANDO = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 45:
                toReturn.parPersonaExtranjera = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 46:
                toReturn.parPersonaJuridica = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] = parametro.parametro_abreviacion;
                break;
              case 48:
                toReturn.parCONVIVIENTE = parametro;
                break; // Diccionary ID¡: 20 descripcion: LUGAR DE EXTENCION DOC. ID.
              case 49:
                toReturn.parCONYUGE = parametro;
                break; // Diccionary ID¡: 20 descripcion: LUGAR DE EXTENCION DOC. ID.
              case 50:
                toReturn.parHijo = parametro;
                break; // Diccionary ID¡: 20 descripcion: PARENTESCO
              case 52:
                toReturn.parHERMANO = parametro;
                break; // Diccionary ID¡: 20 descripcion: PARENTESCO
              case 53:
                toReturn.parMADRE = parametro;
                break; // Diccionary ID¡: 20 descripcion: PARENTESCO
              case 55:
                toReturn.parPADRE = parametro;
                break; // Diccionary ID¡: 20 descripcion: PARENTESCO
              case 56:
                toReturn.parOtroParentesco = parametro;
                break; // Diccionary ID¡: 20 descripcion: PARENTESCO
              case 57:
                toReturn.parPagoEfectivo = parametro;
                break; // Diccionary ID¡: 21 descripcion: PARENTESCO
              case 58:
                toReturn.parPagoDebitoAutomatico = parametro;
                break; // Diccionary ID¡: 21 descripcion: PARENTESCO
              case 68:
                toReturn.parBOLIVIANOS = parametro;
                break; // Diccionary ID¡: 22 descripcion: CONDICION
              case 69:
                toReturn.parDOLAR = parametro;
                break; // Diccionary ID¡: 22 descripcion: CONDICION
              case 76:
                toReturn.parSolicitudSeguro = parametro;
                break; // Diccionary ID¡: 24 descripcion: MONEDA
              case 79:
                toReturn.parCertificadoSeguro = parametro;
                break; // Diccionary ID¡: 24 descripcion: MONEDA
              case 80:
                toReturn.parOrdenPago = parametro;
                break; // Diccionary ID¡: 24 descripcion: DOCUMENTO POLIZA
              case 83:
                toReturn.parACTIVO = parametro;
                break; // Diccionary ID¡: 25 descripcion: DOCUMENTO POLIZA
              case 84:
                toReturn.parINACTIVO = parametro;
                break; // Diccionary ID¡: 25 descripcion: DOCUMENTO POLIZA
              case 85:
                toReturn.parINACTIVO = parametro;
                break; // Diccionary ID¡: 25 descripcion: ESTADOS DEL BENEFICIARIO
              case 86:
                toReturn.parORIGINAL = parametro;
                break; // Diccionary ID¡: 29 descripcion: ESTADOS DEL BENEFICIARIO
              case 87:
                toReturn.parADICIONAL = parametro;
                break; // Diccionary ID¡: 29 descripcion: ESTADOS DEL BENEFICIARIO
              case 88:
                toReturn.parNumeracionIndependiente = parametro;
                break; // Diccionary ID¡: 30 descripcion: TIPO DE BENEFICIARIO
              case 89:
                toReturn.parNumeracionUnificvada = parametro;
                break; // Diccionary ID¡: 30 descripcion: TIPO DE BENEFICIARIO
              case 90:
                toReturn.parMENSUAL = parametro;
                break; // Diccionary ID¡: 31 descripcion: TIPO DE NUMERIACION DOCUMENTOS
              case 93:
                toReturn.parANUAL = parametro;
                break; // Diccionary ID¡: 31 descripcion: TIPO DE NUMERIACION DOCUMENTOS
              case 95:
                toReturn.parObservacion = parametro;
                toReturn.parObservaciones[parametro.parametro_cod] = parametro.parametro_descripcion;
                break;
              case 96:
                toReturn.parAdvertencia = parametro;
                toReturn.parObservaciones[parametro.parametro_cod] = parametro.parametro_descripcion;
                break;
              case 97:
                toReturn.parInformacion = parametro;
                toReturn.parObservaciones[parametro.parametro_cod] = parametro.parametro_descripcion;
                break;
              case 98:
                toReturn.parSatisfactorio = parametro;
                toReturn.parObservaciones[parametro.parametro_cod] = parametro.parametro_descripcion;
                break;
              case 99:
                toReturn.parPrincipal = parametro;
                break; // Diccionary ID¡: 33 descripcion: TIPOS DE OBSERVACIONES
              case 100:
                toReturn.parAdicional = parametro;
                break; // Diccionary ID¡: 33 descripcion: TIPOS DE OBSERVACIONES
              case 101:
                toReturn.parBENEFICIARIOS = parametro;
                break; // Diccionary ID¡: 34 descripcion: TIPO DE TARJETA
              case 102:
                toReturn.parDEPENDIENTES = parametro;
                break; // Diccionary ID¡: 34 descripcion: TIPO DE TARJETA
              case 103:
                toReturn.parObjetoAnexo = parametro;
                break; // Diccionary ID¡: 35 descripcion: TIPO DE ANEXO
              case 111:
                toReturn.parPANDO = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: TIPO DE ANEXO
              case 112:
                toReturn.parCOCHABAMBA = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: TIPO DE OBJETO
              case 113:
                toReturn.parElAlt = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: SUCURSALES ECOFUTURO
              case 114:
                toReturn.parLaPa = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: SUCURSALES ECOFUTURO
              case 115:
                toReturn.parOficinaNaciona = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: SUCURSALES ECOFUTURO
              case 116:
                toReturn.parORURO = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: SUCURSALES ECOFUTURO
              case 117:
                toReturn.parPOTOSI = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: OFICINASf ECOFUTURO
              case 118:
                toReturn.parSantaCru = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: OFICINAS ECOFUTURO
              case 119:
                toReturn.parCHUQUISACA = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: OFICINAS ECOFUTURO
              case 120:
                toReturn.parTARIJA = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: OFICINAS ECOFUTURO
              case 121:
                toReturn.parBENI = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 38 descripcion: OFICINAS ECOFUTURO
              case 110:
                toReturn.parPLAN = parametro; // Diccionary ID¡: 39 descripcion: OFICINAS ECOFUTURO
              case 278:
                toReturn.parHUAYRAKASA = parametro;
                toReturn.sucursalesEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 257:
                toReturn.Canot = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 258:
                toReturn.Copacaban = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 259:
                toReturn.Guayarameri = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 260:
                toReturn.Charazan = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 261:
                toReturn.ValleConcepcionpa = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 262:
                toReturn.SanPedro = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 263:
                toReturn.VillaCopacaban = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 122:
                toReturn.parAiquile = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 123:
                toReturn.parCapinota = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 124:
                toReturn.parCentro = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 125:
                toReturn.parCliza = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 126:
                toReturn.parCruceTaquin = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 127:
                toReturn.parIvirgarzama = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 128:
                toReturn.parLaCanch = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 129:
                toReturn.parOfExtern = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 130:
                toReturn.parOficina = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 131:
                toReturn.parQuillacollo = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 132:
                toReturn.par16Julio = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 133:
                toReturn.parAchacachi = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 134:
                toReturn.parApolo = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 135:
                toReturn.parCeja = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 136:
                toReturn.Cobij = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 137:
                toReturn.parVillaAdela = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 138:
                toReturn.parEscom = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 139:
                toReturn.parLaj = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 140:
                toReturn.parOfExterna = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 141:
                toReturn.parOfExterna = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 142:
                toReturn.OficinaCentra = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 143:
                toReturn.parPatacamaya = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 144:
                toReturn.parRioSeco = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] = parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 145:
                toReturn.parCalacoto = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 146:
                toReturn.parCAMACHO = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 147:
                toReturn.parCaranavi = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 148:
                toReturn.parElTejar = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 149:
                toReturn.parGuanay = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 150:
                toReturn.parMapiri = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 151:
                toReturn.parOfExterna = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 152:
                toReturn.parOficina = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 153:
                toReturn.parPalosBlancos = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 154:
                toReturn.parSanMigue = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 155:
                toReturn.parVillaFatima = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 156:
                toReturn.parZonaCentral = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 173:
                toReturn.OficinaNaciona = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 174:
                toReturn.par10Febrero = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 175:
                toReturn.parAlValle = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 176:
                toReturn.parCaracollo = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 177:
                toReturn.parChallapata = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 178:
                toReturn.parMercadoBolivar = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 179:
                toReturn.parMercadoYoung = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 180:
                toReturn.parOficina = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 181:
                toReturn.parPagador = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 182:
                toReturn.parTagarete = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 183:
                toReturn.parLlallagua = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 184:
                toReturn.parMercadoUyuni = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 185:
                toReturn.parOficina = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 186:
                toReturn.parPasajeBoulevard = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 187:
                toReturn.parTupiza = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 188:
                toReturn.parUyuni = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 189:
                toReturn.parVitichi = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 194:
                toReturn.par4Canadas = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 195:
                toReturn.parAbasto = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 196:
                toReturn.parAltoSanPedro = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 197:
                toReturn.parArroyoConcepcion = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 198:
                toReturn.Camir = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 199:
                toReturn.parCentral = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 200:
                toReturn.parCharagua = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 201:
                toReturn.parConcepcion = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 202:
                toReturn.parGuarayos = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 203:
                toReturn.parRamada = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 204:
                toReturn.parMontero = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 205:
                toReturn.parMutualista = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 206:
                toReturn.parOficina = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 207:
                toReturn.parPlan3000 = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 208:
                toReturn.parSanIgnacio = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 209:
                toReturn.parSanJose = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 210:
                toReturn.parSanJulian = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 211:
                toReturn.parYapacani = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 217:
                toReturn.Camarg = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 218:
                toReturn.parMercadoCampesino = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 219:
                toReturn.parMercadoCentral = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 220:
                toReturn.parMonteagudo = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 221:
                toReturn.parOfFerial = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 222:
                toReturn.parOficina = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 223:
                toReturn.VillaSerrano = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: OFICINAS ECOFUTURO
              case 224:
                toReturn.parBermejo = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 225:
                toReturn.parCentral = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: UNIDAD TIEMPO
              case 226:
                toReturn.parEntreRios = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: UNIDAD TIEMPO
              case 227:
                toReturn.parMdoCampesino = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: UNIDAD TIEMPO
              case 228:
                toReturn.parOfExterna = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: TIPO ARCHIVO
              case 229:
                toReturn.parOfExternaValleConcepcio = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: TIPO ARCHIVO
              case 230:
                toReturn.parOficinaCentralSucursa = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: TIPO ARCHIVO
              case 231:
                toReturn.Villamontes = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: TIPO COMKPARACION
              case 232:
                toReturn.parYacuiba = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: TIPO COMKPARACION
              case 233:
                toReturn.parOficina = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: TIPO CUENTAS
              case 234:
                toReturn.parPzaPrincipalJoseBallivia = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: TIPO CUENTAS
              case 235:
                toReturn.parRiberalta = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: TIPO CUENTAS
              case 236:
                toReturn.parSanBorja = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: ESTADO FTP
              case 237:
                toReturn.parSanIgnacio = parametro;
                toReturn.oficinasEcofuturo[parametro.parametro_cod] =
                  parametro.parametro_descripcion;
                break; // Diccionary ID¡: 40 descripcion: ESTADO FTP
              case 239:
                toReturn.parDia = parametro;
                break; // Diccionary ID¡: 41 descripcion: ESTADO FTP
              case 240:
                toReturn.parMes = parametro;
                break; // Diccionary ID¡: 41 descripcion: ESTADO DE PLAN PAGO
              case 241:
                toReturn.parAno = parametro;
                break; // Diccionary ID¡: 41 descripcion: ESTADO DE PLAN PAGO
              case 242:
                toReturn.parArchivoExcel = parametro;
                break; // Diccionary ID¡: 42 descripcion: ESTADO DE ENVIO
              case 243:
                toReturn.parArchivoWord = parametro;
                break; // Diccionary ID¡: 42 descripcion: ESTADO DE ENVIO
              case 244:
                toReturn.parArchivoCsv = parametro;
                break; // Diccionary ID¡: 42 descripcion: OFICINAS ECOFUTURO
              case 245:
                toReturn.parComparacionAbsoluta = parametro;
                break; // Diccionary ID¡: 43 descripcion: OFICINAS ECOFUTURO
              case 246:
                toReturn.parComparacionRelativa = parametro;
                break; // Diccionary ID¡: 43 descripcion: OFICINAS ECOFUTURO
              case 247:
                toReturn.parMancomunadaConjunta = parametro;
                break; // Diccionary ID¡: 44 descripcion: OFICINAS ECOFUTURO
              case 248:
                toReturn.parIndividual = parametro;
                break; // Diccionary ID¡: 44 descripcion: OFICINAS ECOFUTURO
              case 249:
                toReturn.parMancomunadaIndistinta = parametro;
                break; // Diccionary ID¡: 44 descripcion: OFICINAS ECOFUTURO
              case 250:
                toReturn.parEnviadoServidorRemoto = parametro;
                break; // Diccionary ID¡: 45 descripcion: OFICINAS ECOFUTURO
              case 251:
                toReturn.parRecibidoDesdeServidorRemoto = parametro;
                break; // Diccionary ID¡: 45 descripcion: ESTADO ENVIO
              case 252:
                toReturn.parParaEnviar = parametro;
                break; // Diccionary ID¡: 45 descripcion: ESTADO ENVIO
              case 253:
                toReturn.parPENDIENTE = parametro;
                break; // Diccionary ID¡: 46 descripcion: FORMA ENVIO
              case 254:
                toReturn.parPAGADO = parametro;
                break; // Diccionary ID¡: 46 descripcion: FORMA ENVIO
              case 255:
                toReturn.parENVIADO = parametro;
                break; // Diccionary ID¡: 47 descripcion: REPORTE TIPOS
              case 256:
                toReturn.parNoEnvieado = parametro;
                break; // Diccionary ID¡: 47 descripcion: REPORTE TIPOS
              case 264:
                toReturn.parNoEnviado = parametro;
                break; // Diccionary ID¡: 48 descripcion: ESTADO ENVIO
              case 265:
                toReturn.parEnviado = parametro;
                break; // Diccionary ID¡: 48 descripcion: SUB ESTADO PLAN DE PAGO
              case 270:
                toReturn.parIniciado = parametro;
                break; // Diccionary ID¡: 48 descripcion: SUB ESTADO PLAN DE PAGO
              case 266:
                toReturn.parManual = parametro;
                break; // Diccionary ID¡: 49 descripcion: SUB ESTADO PLAN DE PAGO
              case 267:
                toReturn.parAutomatico = parametro;
                break; // Diccionary ID¡: 49 descripcion: SUB ESTADO PLAN DE PAGO
              case 268:
                toReturn.parReporteMensualAcumuladoSemanalmente = parametro;
                break; // Diccionary ID¡: 50 descripcion: ESTADO PLAN
              case 269:
                toReturn.parReporteSemanal = parametro;
                break; // Diccionary ID¡: 50 descripcion: ESTADO PLAN
              case 276:
                toReturn.parVIGENTE = parametro;
                break; // Diccionary ID¡: 51 descripcion: OFICINAS ECOFUTURO
              case 277:
                toReturn.parNoVigente = parametro;
                break; // Diccionary ID¡: 51 descripcion: TIPOS DOCUMENTOS
              case 279:
                toReturn.parDocumentoSolicitud = parametro;
                break; // Diccionary ID¡: 52 descripcion: TIPOS DOCUMENTOS
              case 280:
                toReturn.parDocumentoComprobante = parametro;
                break; // Diccionary ID¡: 52 descripcion: TIPOS DOCUMENTOS
              case 281:
                toReturn.parDocumentoCertificado = parametro;
                break; // Diccionary ID¡: 52 descripcion: ESTADOS DE INSTANCIA DE POLIZA
              case 284:
                toReturn.parCISinExtension = parametro;
                toReturn.lugarExtencionDocId[parametro.parametro_cod] =
                  parametro.parametro_abreviacion;
                break;
            }
          }
        }
      }
      return toReturn;
    } catch (e) {
      console.log(e);
      loggerCatch.info(new Date(),e);
    }
  }

  static async setParametrosDescription(diccionarios = []) {
    try {
      let parIdDescription = {};
      let parCodDescription = {};
      if (diccionarios.length) {
        let parametros = await modelos.parametro.findAll({
          where: { diccionario_id: diccionarios },
        });
        if (parametros && parametros.length) {
          for (let i = 0; i < parametros.length; i++) {
            let parametro = parametros[i].dataValues;
            parIdDescription[parametro.id] = parametro.parametro_descripcion;
          }
        }
      }
      return parIdDescription;
    } catch (e) {
      console.log(e);
      loggerCatch.info(new Date(),e);
    }
  }

  static async updateSolicitud(solicitud, idPoliza, correlativo = true) {
    try {
      let response = { error: "ok", message: "", data: {} };
      let colibriParametros = this.setParametros([7, 18, 29, 30, 52, 11]);
      let asegurado = {};
      let respPersona, objPersona, newPersona, personaId;
      let respEntidad, objEntidad, newEntidad, entidadId;
      let respAsegurado, objAsegurado, newAsegurado, aseguradoId;
      let respInstanciaPoliza, objInstanciaPoliza, newInstanciaPoliza, instanciaPolizaId;
      let respAtributoInstanciaPoliza, objAtributoInstanciaPoliza, atributoInstanciaPolizaId;
      let respInstanciaDocumento, objInstanciaDocumento, instanciaDocumentoId;
      let respBeneficiario, objBeneficiario, beneficiarioId;
      let objEntidadBen, respEntidadBen, entidadBenId;
      let newEntidadBen, newPersonaBen, newBeneficiario, personaBenId;

      if (solicitud) {
        let respPoliza = await modelos.poliza.findOne({
          where: { id: idPoliza },
          include: [
            {
              model: modelos.objeto,
              as: "polizaObjeto",
              include: {
                model: modelos.objeto_x_atributo,
                as: "objetoObjetoXAtributo",
                include: {
                  model: modelos.atributo,
                  as: "objetoXAtributoAtributos",
                },
              },
            },
            { model: modelos.parametro, as: "tipo_numeracion" },
          ],
        });
        let poliza = respPoliza && respPoliza.dataValues ? respPoliza.dataValues : null;
        let idUsuario = req.session.passport.user;
        let respUsuario = await modelos.usuario.findOne({
          where: { id: idUsuario },
        });
        let usuario = respUsuario && respUsuario.dataValues ? respUsuario.dataValues : null;
        if (usuario && poliza) {
          if (solicitud.entidad.persona) {
            if (solicitud.entidad.persona.id) {
              newPersona = solicitud.entidad.persona.dataValues ? solicitud.entidad.persona.dataValues : solicitud.entidad.persona;
              personaId = newPersona.id;
              delete newPersona.id;
              newPersona.updatedAt = new Date();
              newPersona.modificada_por = usuario.id;
              respPersona = await modelos.persona.update(newPersona, {
                where: { id: personaId },
              });
              respPersona = await modelos.persona.findOne({
                where: { id: personId },
              });
              objPersona = respPersona && respPersona.dataValues ? respPersona.dataValues : null;
            } else {
              newPersona = solicitud.entidad.persona.dataValues ? solicitud.entidad.persona.dataValues : solicitud.entidad.persona;
              personaId = newPersona.id;
              delete newPersona.id;
              newPersona.createdAt = new Date();
              newPersona.updatedAt = new Date();
              newPersona.adicionada_por = usuario.id;
              newPersona.modificada_por = usuario.id;
              respPersona = await modelos.persona.create(newPersona);
              objPersona = respPersona && respPersona.dataValues ? respPersona.dataValues : null;
            }

            if (solicitud.entidad) {
              if (solicitud.entidad.id) {
                newEntidad = solicitud.entidad.dataValues ? solicitud.entidad.dataValues : solicitud.entidad;
                entidadId = newEntidad.id;
                delete newEntidad.id;
                newEntidad.updatedAt = new Date();
                newEntidad.id_persona = objPersona.id;
                newEntidad.modificado_por = usuario.id;
                respEntidad = await modelos.entidad.update(newEntidad, { where: { id: entidadId }});
                respEntidad = await modelos.entidad.findOne({ where: { id: entidadId }});
                objEntidad = respEntidad && respEntidad.dataValues ? respEntidad.dataValues : null;
              } else {
                newEntidad = solicitud.entidad.dataValues ? solicitud.entidad.dataValues : solicitud.entidad;
                entidadId = newEntidad.id;
                delete newEntidad.id;
                newEntidad.adicionado_por = usuario.id;
                newEntidad.modificado_por = usuario.id;
                newEntidad.id_persona = objPersona.id;
                newEntidad.tipo_entidad = colibriParametros.parPersonaNatural.id;
                newEntidad.createdAt = new Date();
                newEntidad.updatedAt = new Date();
                respEntidad = await modelos.entidad.create(newEntidad);
                objEntidad = respEntidad && respEntidad.dataValues ? respEntidad.dataValues : null;

                if (solicitud.instancia_poliza) {
                  if (solicitud.instancia_poliza.id) {
                    newInstanciaPoliza = solicitud.instancia_poliza.dataValues ? solicitud.instancia_poliza.dataValues : solicitud.instancia_poliza;
                    instanciaPolizaId = newInstanciaPoliza.id;
                    delete newInstanciaPoliza.id;
                    newInstanciaPoliza.updatedAt = new Date();
                    newInstanciaPoliza.modificado_por = usuario.id;
                    respInstanciaPoliza = await modelos.instancia_poliza.update(newInstanciaPoliza, { where: { id: instanciaPolizaId }});
                    respInstanciaPoliza = await modelos.instancia_poliza.findOne({where: { id: instanciaPolizaId }});
                    objInstanciaPoliza = respInstanciaPoliza && respInstanciaPoliza.dataValues ? respInstanciaPoliza.dataValues : null;
                  } else {
                    newInstanciaPoliza = solicitud.instancia_poliza.dataValues ? solicitud.instancia_poliza.dataValues : solicitud.instancia_poliza;
                    instanciaPolizaId = newInstanciaPoliza.id;
                    delete newInstanciaPoliza.id;
                    newInstanciaPoliza.adicionada_por = usuario.id;
                    newInstanciaPoliza.modificada_por = usuario.id;
                    newInstanciaPoliza.createdAt = new Date();
                    newInstanciaPoliza.updatedAt = new Date();
                    respInstanciaPoliza = await modelos.instancia_poliza.create(newInstanciaPoliza);
                    objInstanciaPoliza = respInstanciaPoliza && respInstanciaPoliza.dataValues ? respInstanciaPoliza.dataValues : null;
                  }

                  if (solicitud.instancia_poliza.atributo_instancia_polizas && solicitud.instancia_poliza.atributo_instancia_polizas.length) {
                    objInstanciaPoliza.atributo_instancia_polizas = [];
                    for (let i = 0; i < solicitud.instancia_poliza.atributo_instancia_polizas.length; i++) {
                      let atributoInstanciaPoliza = solicitud.instancia_poliza.atributo_instancia_polizas[i].dataValues ? solicitud.instancia_poliza.atributo_instancia_polizas[i].dataValues : solicitud.instancia_poliza.atributo_instancia_polizas[i];
                      if (atributoInstanciaPoliza.id) {
                        atributoInstanciaPolizaId = atributoInstanciaPoliza.id;
                        delete atributoInstanciaPoliza.id;
                        atributoInstanciaPoliza.updatedAt = new Date();
                        atributoInstanciaPoliza.modificado_por = usuario.id;
                        atributoInstanciaPoliza.id_instancia_poliza = objInstanciaPoliza.id;
                        atributoInstanciaPoliza.id_objeto_x_atributo = poliza.polizaObjeto.objetoObjetoXAtributo.id;
                        respAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.update(atributoInstanciaPoliza, { where: { id: atributoInstanciaPolizaId }});
                        respAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({
                            where: { id: atributoInstanciaPolizaId },
                        });
                        objAtributoInstanciaPoliza = respAtributoInstanciaPoliza && respAtributoInstanciaPoliza.dataValues ? respAtributoInstanciaPoliza.dataValues : null;
                      } else {
                        atributoInstanciaPolizaId = atributoInstanciaPoliza.id;
                        delete atributoInstanciaPoliza.id;
                        atributoInstanciaPoliza.adicionada_por = usuario.id;
                        atributoInstanciaPoliza.modificada_por = usuario.id;
                        atributoInstanciaPoliza.id_instancia_poliza = objInstanciaPoliza.id;
                        atributoInstanciaPoliza.id_objeto_x_atributo = poliza.polizaObjeto.objetoObjetoXAtributo.id;
                        atributoInstanciaPoliza.createdAt = new Date();
                        atributoInstanciaPoliza.updatedAt = new Date();
                        respAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.create(atributoInstanciaPoliza);
                        objAtributoInstanciaPoliza = respAtributoInstanciaPoliza && respAtributoInstanciaPoliza.dataValues ? respAtributoInstanciaPoliza.dataValues : null;
                      }
                      objInstanciaPoliza.atributo_instancia_polizas.push(objAtributoInstanciaPoliza);
                    }
                  }

                  if ( solicitud.instancia_poliza.instancia_documentos && solicitud.instancia_poliza.instancia_documentos.length) {
                    objInstanciaPoliza.instancia_poliza.instancia_documentos = [];
                    let documentos = await modelos.documento.findOne({
                      where: { id_poliza: idPoliza },
                      include: {
                        model: modelos.parametro,
                        as: "tipo_documento",
                      },
                    });

                    let instanciaDocumentoSolicitud = solicitud.instancia_poliza.instancia_documentos.find((param) => param.documento.tipo_documento.id === colibriParametros.parDocumentoSolicitud.id);
                    let instanciaDocumentoComprobante = solicitud.instancia_poliza.instancia_documentos.find((param) => param.documento.tipo_documento.id === colibriParametros.parDocumentoComprobante.id);
                    let instanciaDocumentoCertificado = solicitud.instancia_poliza.instancia_documentos.find((param) => param.documento.tipo_documento.id === colibriParametros.parDocumentoCertificado.id);

                    for (let i = 0; i < solicitud.instancia_poliza.instancia_documentos.length; i++) {
                      let instanciaDocumento = solicitud.instancia_poliza.instancia_documentos[i].dataValues ? solicitud.instancia_poliza.instancia_documentos[i].dataValues : solicitud.instancia_poliza.instancia_documentos[i];
                      let respCurrentDocumento = documentos.find((param) => param.dataValues.id == instanciaDocumento.id_documento);
                      let currentDocumento = respCurrentDocumento && respCurrentDocumento.dataValues ? respCurrentDocumento.dataValues : null;
                      let updatedCurrentDocumento;
                      if (instanciaDocumento.id) {
                        instanciaDocumentoId = instanciaDocumento.id;
                        delete instanciaDocumento.id;
                        instanciaDocumento.updatedAt = new Date();
                        instanciaDocumento.modificado_por = usuario.id;
                        instanciaDocumento.id_instancia_poliza = objInstanciaPoliza.id;
                        respInstanciaDocumento = await modelos.instancia_documento.update(instanciaDocumento, { where: { id: instanciaDocumentoId } });
                        respInstanciaDocumento = await modelos.instancia_documento.findOne({ where: { id: instanciaDocumentoId }});
                        objInstanciaDocumento = respInstanciaDocumento && respInstanciaDocumento.dataValues ? respInstanciaDocumento.dataValues : null;
                      } else {
                        instanciaDocumentoId = instanciaDocumento.id;
                        delete instanciaDocumento.id;
                        instanciaDocumento.adicionada_por = usuario.id;
                        instanciaDocumento.modificada_por = usuario.id;
                        instanciaDocumento.id_instancia_poliza = objInstanciaPoliza.id;
                        instanciaDocumento.createdAt = new Date();
                        instanciaDocumento.updatedAt = new Date();

                        // numeracion documentos
                        if (poliza.tipo_numeracion.id === colibriParametros.parNumeracionIndependiente.id) {
                          if (correlativo && currentDocumento) {
                            instanciaDocumento.nro_documento = currentDocumento.nro_actual;
                            currentDocumento.nro_actual = currentDocumento.nro_actual + 1;
                            updatedCurrentDocumento = await modelos.documento.update(currentDocumento, {where: { id: currentDocumento.id }});
                          }
                        } else if (poliza.tipo_numeracion.id === colibriParametros.parNumeracionUnificvada.id) {
                          instanciaDocumento.nro_documento = instanciaDocumentoSolicitud.nro_documento;
                        }

                        respInstanciaDocumento = await modelos.instancia_documento.create(instanciaDocumento);
                        objInstanciaDocumento = respInstanciaDocumento && respInstanciaDocumento.dataValues ? respInstanciaDocumento.dataValues : null;
                      }
                      objInstanciaPoliza.instancia_documentos.push(
                        objInstanciaDocumento
                      );
                    }
                  }

                  if (solicitud) {
                    if (solicitud.id) {
                      newAsegurado = solicitud.dataValues
                        ? solicitud.dataValues
                        : solicitud;
                      aseguradoId = newAsegurado.id;
                      delete newAsegurado.id;
                      newAsegurado.updatedAt = new Date();
                      newAsegurado.modificado_por = usuario.id;
                      newAsegurado.id_instancia_poliza = objInstanciaPoliza.id;
                      newAsegurado.id_entidad = objEntidad.id;
                      respAsegurado = await modelos.asegurado.update(
                        newAsegurado,
                        { where: { id: aseguradoId } }
                      );
                      respAsegurado = await modelos.asegurado.findOne({
                        where: { id: aseguradoId },
                      });
                      objAsegurado =
                        respAsegurado && respAsegurado.dataValues
                          ? respAsegurado.dataValues
                          : null;
                    } else {
                      newAsegurado = solicitud.dataValues
                        ? solicitud.dataValues
                        : solicitud;
                      aseguradoId = newAsegurado.id;
                      delete newAsegurado.id;
                      newAsegurado.adicionado_por = usuario.id;
                      newAsegurado.modificado_por = usuario.id;
                      newAsegurado.id_instancia_poliza = objInstanciaPoliza.id;
                      newAsegurado.id_entidad = objEntidad.id;
                      newAsegurado.createdAt = new Date();
                      newAsegurado.updatedAt = new Date();
                      respAsegurado = await modelos.asegurado.create(
                        newAsegurado
                      );
                      objAsegurado =
                        respAsegurado && respAsegurado.dataValues
                          ? respAsegurado.dataValues
                          : null;
                    }

                    if (solicitud.beneficiarios) {
                      if (
                        solicitud.beneficiarios &&
                        solicitud.beneficiarios.length
                      ) {
                        objAsegurado.beneficiarios = [];
                        for (
                          let i = 0;
                          i < solicitud.beneficiarios.length;
                          i++
                        ) {
                          let beneficiario = solicitud.beneficiarios[i];
                          if (beneficiario) {
                            let objPersonaBen, respPersonaBen;
                            if (beneficiario.entidad.persona) {
                              if (beneficiario.entidad.persona.id) {
                                newPersonaBen = beneficiario.entidad.persona
                                  .dataValues
                                  ? beneficiario.entidad.persona.dataValues
                                  : beneficiario.entidad.persona;
                                personaBenId = newPersonaBen.id;
                                delete newPersonaBen.id;
                                newPersonaBen.updatedAt = new Date();
                                newPersonaBen.modificado_por = usuario.id;
                                respPersonaBen =
                                  await modelos.entidad.persona.update(
                                    newPersonaBen,
                                    { where: { id: personaBenId } }
                                  );
                                respPersonaBen =
                                  await modelos.entidad.persona.findOne({
                                    where: { id: personaBenId },
                                  });
                                objPersonaBen =
                                  respPersonaBen && respPersonaBen.dataValues
                                    ? respPersonaBen.dataValues
                                    : null;
                              } else {
                                newPersonaBen = solicitud.entidad.persona
                                  .dataValues
                                  ? solicitud.entidad.persona.dataValues
                                  : solicitud.entidad.persona;
                                personaBenId = newPersonaBen.id;
                                delete newPersonaBen.id;
                                newPersonaBen.adicionado_por = usuario.id;
                                newPersonaBen.modificado_por = usuario.id;
                                newPersonaBen.createdAt = new Date();
                                newPersonaBen.updatedAt = new Date();
                                respPersonaBen = await modelos.persona.create(
                                  newPersonaBen
                                );
                                objPersonaBen =
                                  respPersonaBen && respPersonaBen.dataValues
                                    ? respPersonaBen.dataValues
                                    : null;
                              }
                            }
                            if (beneficiario.entidad) {
                              if (beneficiario.entidad.id) {
                                newEntidadBen = beneficiario.entidad.dataValues
                                  ? beneficiario.entidad.dataValues
                                  : beneficiario.entidad;
                                entidadBenId = newEntidadBen.id;
                                delete newEntidadBen.id;
                                newEntidadBen.updatedAt = new Date();
                                newEntidadBen.modificado_por = usuario.id;
                                newEntidadBen.id_persona = objPersonaBen.id;
                                respEntidadBen = await modelos.entidad.update(
                                  newEntidadBen,
                                  { where: { id: entidadBenId } }
                                );
                                respEntidadBen = await modelos.entidad.findOne({
                                  where: { id: entidadBenId },
                                });
                                objEntidadBen =
                                  respEntidadBen && respEntidadBen.dataValues
                                    ? respEntidadBen.dataValues
                                    : null;
                              } else {
                                newEntidadBen = solicitud.entidad.dataValues
                                  ? solicitud.entidad.dataValues
                                  : solicitud.entidad;
                                entidadBenId = newEntidadBen.id;
                                delete newEntidadBen.id;
                                newEntidadBen.adicionado_por = usuario.id;
                                newEntidadBen.modificado_por = usuario.id;
                                newEntidadBen.id_persona = objPersonaBen.id;
                                newEntidadBen.createdAt = new Date();
                                newEntidadBen.updatedAt = new Date();
                                respEntidadBen = await modelos.entidad.create(
                                  newEntidadBen
                                );
                                objEntidadBen =
                                  respEntidadBen && respEntidadBen.dataValues
                                    ? respEntidadBen.dataValues
                                    : null;
                              }
                            }

                            if (beneficiario.id) {
                              newBeneficiario = beneficiario.dataValues
                                ? beneficiario.dataValues
                                : beneficiario;
                              beneficiarioId = newBeneficiario.id;
                              delete newBeneficiario.id;
                              newBeneficiario.updatedAt = new Date();
                              newBeneficiario.modificado_por = usuario.id;
                              newBeneficiario.id_asegurado = objAsegurado.id;
                              newBeneficiario.id_beneficiario =
                                objEntidadBen.id;
                              newBeneficiario.tipo =
                                colibriParametros.parORIGINAL.id;
                              respBeneficiario =
                                await modelos.beneficiario.update(
                                  newBeneficiario,
                                  { where: { id: beneficiarioId } }
                                );
                              respBeneficiario =
                                await modelos.beneficiario.findOne({
                                  where: { id: beneficiarioId },
                                });
                              objBeneficiario =
                                respBeneficiario && respBeneficiario.dataValues
                                  ? respBeneficiario.dataValues
                                  : null;
                            } else {
                              newBeneficiario = beneficiario.dataValues
                                ? beneficiario.dataValues
                                : beneficiario;
                              beneficiarioId = newBeneficiario.id;
                              delete newBeneficiario.id;
                              newBeneficiario.adicionado_por = usuario.id;
                              newBeneficiario.modificado_por = usuario.id;
                              newBeneficiario.id_asegurado = objAsegurado.id;
                              newBeneficiario.id_beneficiario =
                                objEntidadBen.id;
                              newBeneficiario.tipo =
                                colibriParametros.parORIGINAL.id;
                              newBeneficiario.createdAt = new Date();
                              newBeneficiario.updatedAt = new Date();
                              respBeneficiario =
                                await modelos.beneficiario.create(
                                  newBeneficiario
                                );
                              objBeneficiario =
                                respBeneficiario && respBeneficiario.dataValues
                                  ? respBeneficiario.dataValues
                                  : null;
                            }
                          }
                          objAsegurado.beneficiarios.push(beneficiario);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          asegurado = objAsegurado;
          asegurado.entidad = objEntidad;
          asegurado.entidad.persona = objPersona;
          asegurado.instancia_poliza = objInstanciaPoliza;
        }
      }
      return asegurado;
    } catch (e) {
      console.log(e);
      loggerCatch.info(new Date(),e);
    }
  }

  static async createSolicitud(
    solicitud,
    idPoliza,
    idEstado,
    usuario = null,
    correlativo = true,
    gracia = 0,
    idSucursal = null,
    idAgencia = null
  ) {
    try {
      idEstado = idEstado ? idEstado : 59;
      let response = { error: "ok", message: "", data: {} };
      let colibriParametros = await this.setParametros([7, 18, 29, 30, 52, 11, 32,]);
      let asegurado = {};
      let respPersona, objPersona, newPersona, personaId;
      let respEntidad, objEntidad, newEntidad, entidadId;
      let respAsegurado, objAsegurado, newAsegurado, aseguradoId;
      let respInstanciaPoliza, objInstanciaPoliza, oldInstanciaPoliza, newInstanciaPoliza, instanciaPolizaId, oldInstanciaPolizaId;
      let respAtributoInstanciaPoliza, objAtributoInstanciaPoliza, newAtributoInstanciaPoliza, atributoInstanciaPolizaId;
      let respInstanciaDocumento, objInstanciaDocumento, newInstanciaDocumento, instanciaDocumentoId;
      let respBeneficiario, objBeneficiario, newBeneficiario, beneficiarioId;
      let objEntidadBen, respEntidadBen, newEntidadBen, entidadBenId;
      let objPersonaBen, respPersonaBen, newPersonaBen, personaBenId;
      let oldPersona, newSolicitud, solicitudId;
      let updatedCurrentDocumento, objInstanciaPolizaTransicion;
      let respOldInstanciaPoliza, instanciaPolizaTransicionId, respInstanciPolizaTransicion;

      if (solicitud) {
        let respPoliza = await modelos.poliza.findOne({
          where: { id: idPoliza },
          include: [
            {
              model: modelos.objeto,
              as: "polizaObjeto",
              include: {
                model: modelos.objeto_x_atributo,
                as: "objetoObjetoXAtributo",
                include: {
                  model: modelos.atributo,
                  as: "objetoXAtributoAtributos",
                },
              },
            },
            { model: modelos.parametro, as: "tipo_numeracion" },
          ],
        });
        let poliza = respPoliza && respPoliza.dataValues ? respPoliza.dataValues : null;
        if (!usuario && solicitud.adicionado_por) {
          let respUsuario = await modelos.usuario.findOne({
            where: { id: solicitud.adicionado_por },
          });
          usuario = respUsuario.dataValues;
        }
        if (usuario && poliza) {
          if (solicitud.entidad.persona) {
            let docId = solicitud.entidad.persona.persona_doc_id;
            let docIdExt = solicitud.entidad.persona.persona_doc_id_ext;
            if (docId && docIdExt) {
              oldPersona = await modelos.persona.findOne({
                where: { persona_doc_id: docId },
              });
            }
            if (oldPersona) {
              newPersona = solicitud.entidad.persona.dataValues ? solicitud.entidad.persona.dataValues : solicitud.entidad.persona;
              personaId = newPersona.id;
              delete newPersona.id;
              let respUpdatedPersona = await modelos.persona.update({
                  // createdAt: new Date(),
                  updatedAt: new Date(),
                  // adicionada_por: usuario.id,
                  modificada_por: usuario.id
              }, { where: { id: personaId } });
              respPersona = await modelos.persona.findOne({
                where: { id: personaId },
              });
              objPersona = respPersona && respPersona.dataValues ? respPersona.dataValues : null;
            } else {
              newPersona = solicitud.entidad.persona.dataValues ? solicitud.entidad.persona.dataValues : solicitud.entidad.persona;
              personaId = newPersona.id;
              delete newPersona.id;
              newPersona.createdAt = new Date();
              newPersona.updatedAt = new Date();
              newPersona.adicionada_por = usuario.id;
              newPersona.modificada_por = usuario.id;
              // newPersona.persona_fecha_nacimiento = new Date(newPersona.persona_fecha_nacimiento).setHours(0);
              respPersona = await modelos.persona.create(newPersona);
              objPersona = respPersona && respPersona.dataValues ? respPersona.dataValues : null;
            }

            if (solicitud.entidad) {
              newEntidad = solicitud.entidad.dataValues ? solicitud.entidad.dataValues : solicitud.entidad;
              entidadBenId = newEntidad.id;
              delete newEntidad.id;
              newEntidad.adicionado_por = usuario.id;
              newEntidad.modificado_por = usuario.id;
              newEntidad.id_persona = objPersona.id;
              newEntidad.tipo_entidad = colibriParametros.parPersonaNatural.id;
              newEntidad.createdAt = new Date();
              newEntidad.updatedAt = new Date();
              respEntidad = await modelos.entidad.create(newEntidad);
              objEntidad = respEntidad && respEntidad.dataValues ? respEntidad.dataValues : null;

              if (solicitud.instancia_poliza) {
                newInstanciaPoliza = oldInstanciaPoliza = solicitud.instancia_poliza.dataValues ? solicitud.instancia_poliza.dataValues : solicitud.instancia_poliza;
                instanciaPolizaId = oldInstanciaPolizaId = newInstanciaPoliza.id;
                delete newInstanciaPoliza.id;
                newInstanciaPoliza.adicionada_por = usuario.id;
                newInstanciaPoliza.modificada_por = usuario.id;
                newInstanciaPoliza.fecha_registro = new Date();
                newInstanciaPoliza.id_estado = idEstado;
                newInstanciaPoliza.createdAt = new Date();
                newInstanciaPoliza.updatedAt = new Date();
                newInstanciaPoliza.id_instancia_renovada = instanciaPolizaId;
                respInstanciaPoliza = await modelos.instancia_poliza.create(
                  newInstanciaPoliza
                );
                objInstanciaPoliza = respInstanciaPoliza && respInstanciaPoliza.dataValues ? respInstanciaPoliza.dataValues : null;

                respOldInstanciaPoliza = await modelos.instancia_poliza.update(
                  { id_instancia_renovada: instanciaPolizaId },
                  { where: { id: instanciaPolizaId } }
                );

                if (solicitud.instancia_poliza.atributo_instancia_polizas && solicitud.instancia_poliza.atributo_instancia_polizas.length) {
                  objInstanciaPoliza.atributo_instancia_polizas = [];
                  for (let i = 0; i < solicitud.instancia_poliza.atributo_instancia_polizas.length; i++) {
                    let newAtributoInstanciaPoliza = solicitud.instancia_poliza.atributo_instancia_polizas[i].dataValues ? solicitud.instancia_poliza.atributo_instancia_polizas[i].dataValues : solicitud.instancia_poliza.atributo_instancia_polizas[i];
                    atributoInstanciaPolizaId = newAtributoInstanciaPoliza.id;
                    delete newAtributoInstanciaPoliza.id;
                    newAtributoInstanciaPoliza.adicionada_por = usuario.id;
                    newAtributoInstanciaPoliza.modificada_por = usuario.id;
                    newAtributoInstanciaPoliza.id_instancia_poliza = objInstanciaPoliza.id;
                    newAtributoInstanciaPoliza.id_objeto_x_atributo = newAtributoInstanciaPoliza.id_objeto_x_atributo;
                    newAtributoInstanciaPoliza.createdAt = new Date();
                    newAtributoInstanciaPoliza.updatedAt = new Date();
                    if (
                      idAgencia &&
                      newAtributoInstanciaPoliza.objeto_x_atributo &&
                      newAtributoInstanciaPoliza.objeto_x_atributo.atributo &&
                      newAtributoInstanciaPoliza.objeto_x_atributo.atributo.id == "59"
                    ) {
                      newAtributoInstanciaPoliza.valor = idAgencia;
                    }
                    if (
                      idSucursal &&
                      newAtributoInstanciaPoliza.objeto_x_atributo &&
                      newAtributoInstanciaPoliza.objeto_x_atributo.atributo &&
                      newAtributoInstanciaPoliza.objeto_x_atributo.atributo.id == "60"
                    ) {
                      newAtributoInstanciaPoliza.valor = idSucursal;
                    }
                    respAtributoInstanciaPoliza =
                      await modelos.atributo_instancia_poliza.create(newAtributoInstanciaPoliza);
                    objAtributoInstanciaPoliza = respAtributoInstanciaPoliza && respAtributoInstanciaPoliza.dataValues ? respAtributoInstanciaPoliza.dataValues : null;
                    objAtributoInstanciaPoliza.objeto_x_atributo = newAtributoInstanciaPoliza.objeto_x_atributo;
                    objInstanciaPoliza.atributo_instancia_polizas.push(objAtributoInstanciaPoliza);
                  }
                }

                let respInstanciaDocumentoSolicitud = solicitud.instancia_poliza.instancia_documentos.find((param) => param.documento.tipo_documento.id === colibriParametros.parDocumentoSolicitud.id);
                let respInstanciaDocumentoComprobante = solicitud.instancia_poliza.instancia_documentos.find((param) => param.documento.tipo_documento.id === colibriParametros.parDocumentoComprobante.id);
                let respInstanciaDocumentoCertificado = solicitud.instancia_poliza.instancia_documentos.find((param) => param.documento.tipo_documento.id === colibriParametros.parDocumentoCertificado.id);

                let oldInstanciaDocumentoSolicitud = oldInstanciaPoliza.instancia_documentos.find((param) => param.documento.tipo_documento.id === colibriParametros.parDocumentoSolicitud.id);
                let oldInstanciaDocumentoComprobante = oldInstanciaPoliza.instancia_documentos.find((param) => param.documento.tipo_documento.id === colibriParametros.parDocumentoComprobante.id);
                let oldInstanciaDocumentoCertificado = oldInstanciaPoliza.instancia_documentos.find((param) => param.documento.tipo_documento.id === colibriParametros.parDocumentoCertificado.id);

                let instanciaDocumentoSolicitud = respInstanciaDocumentoSolicitud.dataValues;
                let instanciaDocumentoComprobante = respInstanciaDocumentoComprobante.dataValues;
                let instanciaDocumentoCertificado = respInstanciaDocumentoCertificado.dataValues;

                if (solicitud.instancia_poliza.instancia_documentos && solicitud.instancia_poliza.instancia_documentos.length) {
                  objInstanciaPoliza.instancia_documentos = [];
                  let documentos = await modelos.documento.findAll({
                    where: { id_poliza: idPoliza },
                    include: { model: modelos.parametro, as: "tipo_documento" },
                  });

                  for (let i = 0; i < solicitud.instancia_poliza.instancia_documentos.length; i++) {
                    let newInstanciaDocumento = solicitud.instancia_poliza.instancia_documentos[i].dataValues ? solicitud.instancia_poliza.instancia_documentos[i].dataValues : solicitud.instancia_poliza.instancia_documentos[i];
                    instanciaDocumentoId = newInstanciaDocumento.id;
                    delete newInstanciaDocumento.id;
                    let respCurrentDocumento = documentos.find((param) => param.dataValues.id == newInstanciaDocumento.id_documento);
                    let currentDocumento = respCurrentDocumento && respCurrentDocumento.dataValues ? respCurrentDocumento.dataValues : null;
                    newInstanciaDocumento.adicionada_por = usuario.id;
                    newInstanciaDocumento.modificada_por = usuario.id;
                    newInstanciaDocumento.id_instancia_poliza = objInstanciaPoliza.id;
                    newInstanciaDocumento.id_documento_version = respCurrentDocumento.id_documento_version;
                    newInstanciaDocumento.asegurado_doc_id_ext = objPersona.persona_doc_id_ext;
                    newInstanciaDocumento.asegurado_doc_id = objPersona.persona_doc_id;
                    newInstanciaDocumento.createdAt = new Date();
                    newInstanciaDocumento.updatedAt = new Date();

                    let date = new Date();
                    let todayStr = `${util.minTwoDigits(date.getDate())}/${util.minTwoDigits(date.getMonth() + 1)}/${date.getFullYear()}`;
                    let fechaInicioVigencia = instanciaDocumentoCertificado.fecha_inicio_vigencia;
                    let fechaInicioVigenciaStr = `${util.minTwoDigits(fechaInicioVigencia.getDate())}/${util.minTwoDigits(fechaInicioVigencia.getMonth() + 1)}/${fechaInicioVigencia.getFullYear()}`;
                    let momFechaInicioVigencia = moment(fechaInicioVigencia);
                    let momToday = moment(date);
                    let idInstanciaDocumento = instanciaDocumentoCertificado.id;
                    let fechaFinVigencia = instanciaDocumentoCertificado.fecha_fin_vigencia;
                    let fechaFinVigenciaStr = `${util.minTwoDigits(fechaFinVigencia.getDate())}/${util.minTwoDigits(fechaFinVigencia.getMonth() + 1)}/${fechaFinVigencia.getFullYear()}`;
                    let momFechaFinVigencia = moment(fechaFinVigencia);
                    let diffMesesVigencia = momFechaFinVigencia.diff(momFechaInicioVigencia, "months") + 1;
                    let vigenciaActual, vigenciaConGracia;
                    if (gracia) {
                      vigenciaConGracia = momToday.add(gracia, "days").diff(momFechaInicioVigencia, "months") + 1;
                      vigenciaActual = momToday.diff(momFechaInicioVigencia, "months") + 1;
                    } else {
                      vigenciaActual = momToday.diff(momFechaInicioVigencia, "months") + 1;
                      vigenciaConGracia = momToday.diff(momFechaInicioVigencia, "months") + 1;
                    }
                    let instanciaPoliza, respInstanciaPoliza, idInstanciaPoliza;
                    let respInstanciaPolizaTransicions, respOldInstanciaPolizaTransicion, oldInstanciaPolizaTransicion, objInstanciaPolizaTransicions;
                    let fechaNuevoInicioVigencia = momFechaFinVigencia.add(1, "days").add(4, "hours").toDate();
                    let fechaNuevoFinVigencia = momFechaFinVigencia.add(12, "months").add(4, "hours").toDate();

                    if (poliza.vigencia_meses < vigenciaConGracia) {
                      newInstanciaDocumento.fecha_emision = new Date();
                      newInstanciaDocumento.fecha_inicio_vigencia = fechaNuevoInicioVigencia;
                      newInstanciaDocumento.fecha_fin_vigencia = fechaNuevoFinVigencia;
                    } else {
                      newInstanciaDocumento.fecha_emision = new Date();
                      newInstanciaDocumento.fecha_inicio_vigencia = new Date();
                      newInstanciaDocumento.fecha_fin_vigencia = moment(new Date()).add(12, "months").add(4, "hours").toDate();
                    }

                    // numeracion documentos
                    if (poliza.tipo_numeracion.id === colibriParametros.parNumeracionIndependiente.id) {
                      if (correlativo && Object.keys(currentDocumento).length) {
                        newInstanciaDocumento.nro_documento = currentDocumento.nro_actual;
                        currentDocumento.nro_actual = parseInt(currentDocumento.nro_actual) + 1;
                        updatedCurrentDocumento = await modelos.documento.update({ nro_actual: currentDocumento.nro_actual }, { where: { id: currentDocumento.id } });
                      } else {
                        if (currentDocumento.id_tipo_documento !== colibriParametros.parDocumentoCertificado.id) {
                          newInstanciaDocumento.nro_documento = currentDocumento.nro_actual;
                          currentDocumento.nro_actual = parseInt(currentDocumento.nro_actual) + 1;
                          updatedCurrentDocumento = await modelos.documento.update({ nro_actual: currentDocumento.nro_actual }, { where: { id: currentDocumento.id } });
                        }
                      }
                    } else if (poliza.tipo_numeracion.id === colibriParametros.parNumeracionUnificvada.id) {
                      if (currentDocumento.id_tipo_documento === colibriParametros.parDocumentoCertificado.id) {
                        newInstanciaDocumento.nro_documento = instanciaDocumentoSolicitud.nro_documento;
                      } else {
                        newInstanciaDocumento.nro_documento = currentDocumento.nro_actual;
                        currentDocumento.nro_actual = currentDocumento.nro_actual + 1;
                        updatedCurrentDocumento = await modelos.documento.update({ nro_actual: currentDocumento.nro_actual }, { where: { id: currentDocumento.id } });
                      }
                    }

                    respInstanciaDocumento = await modelos.instancia_documento.create(newInstanciaDocumento);
                    objInstanciaDocumento = respInstanciaDocumento && respInstanciaDocumento.dataValues ? respInstanciaDocumento.dataValues : null;
                    objInstanciaDocumento.documento = newInstanciaDocumento.documento;
                    objInstanciaPoliza.instancia_documentos.push(objInstanciaDocumento);
                  }
                }
                let respInstanciaPolizaTransiciones = await this.findInstanciaPolizaTransiciones(solicitud.id_instancia_poliza, idEstado, colibriParametros.parAdvertencia.id);
                respInstanciaPolizaTransiciones.push({
                  dataValues: {
                    adicionado_por: usuario.id,
                    modificado_por: usuario.id,
                    observacion: `Esta solicitud fue renovada de la solicitud Nro: ${oldInstanciaDocumentoSolicitud.nro_documento}, Id: ${oldInstanciaDocumentoSolicitud.id_instancia_renovada}`,
                    par_observacion_id: colibriParametros.parInformacion.id,
                    id_instancia_poliza: objInstanciaPoliza.id,
                    par_estado_id: idEstado,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                  },
                });
                if (respInstanciaPolizaTransiciones && respInstanciaPolizaTransiciones.length) {
                  objInstanciaPoliza.instancia_poliza_transicions = [];
                  let withLogs = true;
                  if (withLogs) {
                    let observaciones = [];
                    for (let i = 0; i < respInstanciaPolizaTransiciones.length; i++) {
                      let newInstanciaPolizaTransicion = respInstanciaPolizaTransiciones[i].dataValues;
                      if (!observaciones.find((param) => param === newInstanciaPolizaTransicion.observacion)) {
                        observaciones.push(newInstanciaPolizaTransicion.observacion);
                        instanciaPolizaTransicionId = newInstanciaPolizaTransicion.id;
                        if (newInstanciaPolizaTransicion.par_estado_id != colibriParametros.parSinVigencia.id) {
                          delete newInstanciaPolizaTransicion.id;
                          newInstanciaPolizaTransicion.adicionado_por = usuario.id;
                          newInstanciaPolizaTransicion.modificado_por = usuario.id;
                          newInstanciaPolizaTransicion.id_instancia_poliza = objInstanciaPoliza.id;
                          newInstanciaPolizaTransicion.createdAt = new Date();
                          newInstanciaPolizaTransicion.updatedAt = new Date();
                          respInstanciPolizaTransicion = await modelos.instancia_poliza_transicion.create(newInstanciaPolizaTransicion);
                          objInstanciaPolizaTransicion = respInstanciPolizaTransicion && respInstanciPolizaTransicion.dataValues ? respInstanciPolizaTransicion.dataValues : null;
                          objInstanciaPoliza.instancia_poliza_transicions.push(objInstanciaPolizaTransicion);
                        }
                      }
                    }
                  } else {
                  }
                }

                if (solicitud) {
                  newSolicitud = solicitud.dataValues ? solicitud.dataValues : solicitud;
                  solicitudId = newSolicitud.id;
                  delete newSolicitud.id;
                  newSolicitud.adicionado_por = usuario.id;
                  newSolicitud.modificado_por = usuario.id;
                  newSolicitud.id_instancia_poliza = objInstanciaPoliza.id;
                  newSolicitud.id_entidad = objEntidad.id;
                  newSolicitud.createdAt = new Date();
                  newSolicitud.updatedAt = new Date();
                  respAsegurado = await modelos.asegurado.create(newSolicitud);
                  objAsegurado = respAsegurado && respAsegurado.dataValues ? respAsegurado.dataValues : null;
                  newSolicitud.id = objAsegurado.id;

                  if (solicitud.beneficiarios) {
                    if (solicitud.beneficiarios && solicitud.beneficiarios.length) {
                      objAsegurado.beneficiarios = [];
                      for (let i = 0; i < solicitud.beneficiarios.length; i++) {
                        let beneficiario = solicitud.beneficiarios[i];
                        if (beneficiario) {
                          if (beneficiario.entidad.persona) {
                            newPersonaBen = beneficiario.entidad.persona.dataValues ? beneficiario.entidad.persona.dataValues : beneficiario.entidad.persona;
                            personaBenId = newPersonaBen.id;
                            delete newPersonaBen.id;
                            newPersonaBen.adicionado_por = usuario.id;
                            newPersonaBen.modificado_por = usuario.id;
                            newPersonaBen.createdAt = new Date();
                            newPersonaBen.updatedAt = new Date();
                            respPersonaBen = await modelos.persona.create(newPersonaBen);
                            objPersonaBen = respPersonaBen && respPersonaBen.dataValues ? respPersonaBen.dataValues : null;
                            newPersonaBen.id = objPersonaBen.id;
                          }
                          if (beneficiario.entidad) {
                            newEntidadBen = beneficiario.entidad.dataValues ? beneficiario.entidad.dataValues : beneficiario.entidad;
                            entidadBenId = newEntidadBen.id;
                            delete newEntidadBen.id;
                            newEntidadBen.adicionado_por = usuario.id;
                            newEntidadBen.modificado_por = usuario.id;
                            newEntidadBen.id_persona = objPersonaBen.id;
                            newEntidadBen.createdAt = new Date();
                            newEntidadBen.updatedAt = new Date();
                            respEntidadBen = await modelos.entidad.create(newEntidadBen);
                            objEntidadBen = respEntidadBen && respEntidadBen.dataValues ? respEntidadBen.dataValues : null;
                            newEntidadBen.id = objEntidadBen.id;
                          }
                          newBeneficiario = beneficiario.dataValues ? beneficiario.dataValues : beneficiario;
                          beneficiarioId = newBeneficiario.id;
                          delete newBeneficiario.id;
                          newBeneficiario.adicionado_por = usuario.id;
                          newBeneficiario.modificado_por = usuario.id;
                          newBeneficiario.id_asegurado = objAsegurado.id;
                          newBeneficiario.id_beneficiario = objEntidadBen.id;
                          newBeneficiario.tipo = colibriParametros.parORIGINAL.id;
                          newBeneficiario.createdAt = new Date();
                          newBeneficiario.updatedAt = new Date();
                          respBeneficiario = await modelos.beneficiario.create(newBeneficiario);
                          objBeneficiario = respBeneficiario && respBeneficiario.dataValues ? respBeneficiario.dataValues : null;
                          newBeneficiario.id = objBeneficiario.id;
                        }
                        objAsegurado.beneficiarios.push(newBeneficiario);
                      }
                    }
                  }
                }
              }
            }
          }
          asegurado = objAsegurado;
          asegurado.entidad = objEntidad;
          asegurado.entidad.persona = objPersona;
          asegurado.instancia_poliza = objInstanciaPoliza;
        }
      }
      return asegurado;
    } catch (e) {
      console.log(e);
      loggerCatch.info(new Date(),e);
    }
  }

  static async findPersonaSolicitudByIdInstanciaPoliza(idInstanciaPoliza) {
    try {
      let asegurado = await modelos.asegurado.find({
        where: { id_instancia_poliza: idInstanciaPoliza },
        include: [
          {
            model: modelos.entidad,
            include: [
              {
                model: modelos.persona,
              },
            ],
          },
          {
            model: modelos.instancia_poliza,
            include: [
              {
                model: modelos.parametro,
                as: "estado",
              },
              {
                model: modelos.poliza,
                include: [
                  {
                    model: modelos.parametro,
                    as: "ramo",
                  },
                  {
                    model: modelos.parametro,
                    as: "tipo_numeracion",
                  },
                  {
                    model: modelos.entidad,
                    as: "aseguradora",
                  },
                ],
              },
              {
                model: modelos.usuario,
                include: {
                  model: modelos.usuario_x_rol,
                  include: {
                    model: modelos.rol,
                  },
                },
              },
            ],
          },
        ],
      });

      let beneficiarios = await modelos.beneficiario.findAll({
        include: [
          {
            model: modelos.asegurado,
            where: { id_instancia_poliza: idInstanciaPoliza },
          },
          {
            model: modelos.entidad,
            include: [
              {
                model: modelos.persona,
              },
            ],
          },
          {
            model: modelos.parametro,
            as: "estado_obj",
          },
          {
            model: modelos.parametro,
            as: "tipo_obj",
          },
          {
            model: modelos.parametro,
            as: "parentesco",
          },
        ],
      });
      let instanciaDocumentos = await modelos.instancia_documento.findAll({
        where: { id_instancia_poliza: idInstanciaPoliza },
        include: [
          {
            model: modelos.documento,
            include: {
              model: modelos.parametro,
              as: "tipo_documento",
            },
          },
          {
            model: modelos.atributo_instancia_documento,
          },
        ],
      });
      let instanciaPolizaAtributos =
        await modelos.atributo_instancia_poliza.findAll({
          where: { id_instancia_poliza: idInstanciaPoliza },
          include: [
            {
              model: modelos.objeto_x_atributo,
              include: [
                {
                  model: modelos.atributo,
                },
                {
                  model: modelos.objeto,
                },
                {
                  model: modelos.parametro,
                  as: "par_editable",
                },
                {
                  model: modelos.parametro,
                  as: "par_comportamiento_interfaz",
                },
              ],
            },
          ],
        });
      asegurado.dataValues.beneficiarios = [];
      asegurado.dataValues.beneficiarios = beneficiarios;
      asegurado.dataValues.instancia_poliza.instancia_documentos = [];
      asegurado.dataValues.instancia_poliza.instancia_documentos = instanciaDocumentos;
      asegurado.dataValues.instancia_poliza.dataValues.instancia_documentos = [];
      asegurado.dataValues.instancia_poliza.dataValues.instancia_documentos = instanciaDocumentos;
      asegurado.dataValues.instancia_poliza.atributo_instancia_polizas = [];
      asegurado.dataValues.instancia_poliza.atributo_instancia_polizas = instanciaPolizaAtributos;
      asegurado.dataValues.instancia_poliza.dataValues.atributo_instancia_polizas = [];
      asegurado.dataValues.instancia_poliza.dataValues.atributo_instancia_polizas = instanciaPolizaAtributos;
      asegurado.dataValues.instancia_poliza.instancia_poliza_transicions = [];
      asegurado.dataValues.instancia_poliza.instancia_poliza_transicions = instanciaPolizaAtributos;
      asegurado.dataValues.instancia_poliza.dataValues.instancia_poliza_transicions = [];
      asegurado.dataValues.instancia_poliza.dataValues.instancia_poliza_transicions = instanciaPolizaAtributos;
      return asegurado;
    } catch (e) {
      console.log(e);
      loggerCatch.info(new Date(),e);
    }
  }

  static async findInstanciaPolizaTransiciones(
    idInstanciaPoliza,
    idEstado,
    idObservacion
  ) {
    try {
      if (idEstado)
        return await modelos.instancia_poliza_transicion.findAll({
          where: {
            id_instancia_poliza: idInstanciaPoliza,
            par_estado_id: idEstado,
            par_observacion_id: idObservacion,
          },
        });
    } catch (e) {
      console.log(e);
      loggerCatch.info(new Date(),e);
    }
  }
}

String.prototype.isLetter = function () {
  return this.match(/[a-z]/i);
};

Array.prototype.isInArray = function (value) {
  return this.indexOf(value) > -1;
};
String.prototype.removeAccents = function () {
  var map = {
    "-": " ",
    "-": "_",
    a: "á|à|ã|â|À|Á|Ã|Â",
    e: "é|è|ê|É|È|Ê",
    i: "í|ì|î|Í|Ì|Î",
    o: "ó|ò|ô|õ|Ó|Ò|Ô|Õ",
    u: "ú|ù|û|ü|Ú|Ù|Û|Ü",
    c: "ç|Ç",
    n: "ñ|Ñ",
  };
  let str = this;
  for (var pattern in map) {
    str = str.replace(new RegExp(map[pattern], "g"), pattern);
  }

  return str;
};

module.exports = SolicitudService;
