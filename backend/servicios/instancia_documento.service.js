const modelos = require('../modelos');

class InstanciaDocumentoService {

    static async getByNroDocumento (nro_documento,id_poliza) {
        try {
            return await modelos.instancia_documento.find({
                where: { nro_documento },
                include: [{
                    model: modelos.instancia_poliza,
                    where: { id_poliza: id_poliza, id_estado:{$and:[{$notIn:[60]}]}}
                    },{
                    model: modelos.documento,
                    where: { id_tipo_documento:279 }
                }]
            });
        } catch (e) {
            console.log(e);
        }
    }

    static async getByNroDocumentoToEstado (idEstado,nro_documento,id_poliza,id_instancia_poliza) {
        try {
            let queryIn;
            if (idEstado && idEstado == 62) {
                queryIn = [59];
            } else {
                queryIn = [59,24,238,81];
            }
            let instanciaDocumentoSolicitud = await modelos.instancia_documento.find({
                where: nro_documento ? { nro_documento } : null,
                include: [{
                    model: modelos.instancia_poliza,
                    where: id_instancia_poliza ? { id:id_instancia_poliza, id_poliza: id_poliza, id_estado:{notIn:[60,282,65]}} : { id_poliza: id_poliza, id_estado:{notIn:[60,282,65]}}
                }, {
                    model: modelos.documento,
                    where: {id_tipo_documento: 279}
                }]
            }).catch(err => {
                console.log(err);
            });
            let toReturn;
            if (instanciaDocumentoSolicitud) {
                toReturn = await modelos.asegurado.findOne({
                    where: {id_instancia_poliza: instanciaDocumentoSolicitud.id_instancia_poliza},
                    include: [{
                        model:modelos.entidad,
                        include: {
                            model:modelos.persona
                        }
                    }, {
                        model: modelos.instancia_poliza,
                        // where: {id_estado:{$in:queryIn}},
                        include: [{
                            model: modelos.instancia_documento,
                            include: [{
                                model: modelos.documento
                            },{
                                model: modelos.atributo_instancia_documento,
                                include:[{
                                    model:modelos.objeto_x_atributo,
                                    include: {
                                        model: modelos.atributo
                                    }
                                },{
                                    model:modelos.parametro,
                                    as:'par_motivo'
                                }]
                            }]
                        },{
                            model:modelos.instancia_poliza_transicion,
                            include:{
                                model:modelos.parametro,
                                as: 'par_estado'
                            }
                        },{
                            model:modelos.poliza,
                        },{
                            model:modelos.parametro,
                            as: 'estado'
                        }]
                    }]
                }).catch(err => {
                    console.log(err)
                });
            } else {
                toReturn = null;
            }
            return toReturn;
        } catch (e) {
            console.log(e);
        }
    }
}

module.exports = InstanciaDocumentoService;
