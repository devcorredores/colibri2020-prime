const modelos = require('../modelos');
const Util = require('../utils/util');
const util = new Util();
const invoke = require("../modulos/soap-request");
const fechas = require('fechas');
const moment = require('moment');
const request = require('request');
const SolicitudService = require('./solicitud.service');
const soap = require('./soap.service');

class MigracionService {

	static async migraEcovida(req, res) {
		try {
			let response = { status: 'OK', message: '', data: [], errors:[] };
			let parametros = await modelos.parametro.findAll({where: {diccionario_id: [1, 4, 7, 11, 18, 20, 21, 22, 25, 29, 31, 38, 40]}});
			let hoy = new Date();
			let createdAt = hoy;
			let year = hoy.getFullYear(), month = hoy.getMonth(), day = hoy.getDate();
			let polizas = await modelos.poliza.findAll();
			let fechaIngreso;
			let datos = await modelos.mig_ecovida.findAll({where:{$and:[
						{id_instancia_poliza:null},
						{ci:{$not:null}},
						{ci:{$not:'NULL'}}
					]}});

			// parametros
			let parTipoDocumentos = parametros.filter(param => param.dataValues.diccionario_id == 1);
			let parMasculino = parametros.find(param => param.dataValues.id == 6);
			let parFemenino = parametros.find(param => param.dataValues.id == 7);
			let parPersonaNatural = parametros.find(param => param.dataValues.id == 12);
			let parPersonaJuridica = parametros.find(param => param.dataValues.id == 13);
			let polizaEcoProteccion = polizas.find(param => param.dataValues.id == 7);
			let polizaEcoVida = polizas.find(param => param.dataValues.id == 6);
			let parEstadoEmitido = parametros.find(param => param.dataValues.id == 59);
			let parEstadoIniciado =  parametros.find(param => param.dataValues.id == 24);
			let parEstadoSolicitado = parametros.find(param => param.dataValues.id == 81);
			let parModalidadPagoMensual = parametros.find(param => param.dataValues.id == 90);
			let parModalidadPagoAnual = parametros.find(param => param.dataValues.id == 93);
			let parSucursales = parametros.filter(param => param.dataValues.diccionario_id == 38);
			let parAgencias = parametros.filter(param => param.dataValues.diccionario_id == 40);
			let parCondiciones = parametros.filter(param => param.dataValues.diccionario_id == 21);
			let parCondicionSi = parametros.find(param => param.dataValues.id == 57);
			let parCondicionNo = parametros.find(param => param.dataValues.id == 58);
			let parMonedaBs = parametros.find(param => param.dataValues.id == 68);
			let parPareentescoConviviente = parametros.find(param => param.dataValues.id == 48);
			let parPareentescoConyuge = parametros.find(param => param.dataValues.id == 49);
			let parPareentescoHijo = parametros.find(param => param.dataValues.id == 50);
			let parPareentescoHermano = parametros.find(param => param.dataValues.id == 52);
			let parPareentescoMadre = parametros.find(param => param.dataValues.id == 53);
			let parPareentescoPadre = parametros.find(param => param.dataValues.id == 55);
			let parPareentescoOtroParentesco = parametros.find(param => param.dataValues.id == 56);
			let parBeneficiarioActivo = parametros.find(param => param.dataValues.id == 83);
			let parBeneficiarioOriginal = parametros.find(param => param.dataValues.id == 86);

			for (let i = 0 ; i < datos.length ; i++) {
				let dato = datos[i].dataValues;
				let fechaEmision = new Date(dato.fecha_emision);
				let fechaEmisionStr = util.minTwoDigits(fechaEmision.getDate())+'/'+util.minTwoDigits(fechaEmision.getMonth()+1)+'/'+fechaEmision.getFullYear();
				let fechaVigenciaInicioStr = fechaEmisionStr
				let fechaVigenciaFinStr = fechas.addMeses(fechaEmisionStr,12);
				let [dayIni,monthIni,yearIni] = fechaVigenciaInicioStr.split('/');
				let [dayFin,monthFin,yearFin] = fechaVigenciaFinStr.split('/');
				let fechaVigenciaInicio = new Date(yearIni,parseInt(monthIni)-1,dayIni);
				let fechaVigenciaFin = new Date(yearFin,parseInt(monthFin)-1,dayFin);
				let fechaFinVigencia = new Date(dato.fecha_emision_real);
				let fechaFinVigenciaToSave = (fechaFinVigencia.getFullYear()+1) + '/' + (fechaFinVigencia.getMonth() + 1) + '/' + fechaFinVigencia.getDate() + ' ' + fechaFinVigencia.getHours() + ':' + fechaFinVigencia.getMinutes() + ':' + fechaFinVigencia.getSeconds();
				if (dato.ci && dato.ci != 'NULL') {
					let [docId,docIdExtTxt] = dato.ci.split(' ');
					let [docIdBen,docIdExtTxtBen] = dato.ci_beneficiario ? dato.ci_beneficiario.split(' ') : ['',''];
					let docIdExt, parDocIdExt, docIdExtBen, parDocIdExtBen;
					if (docIdExtTxt) {
						switch (docIdExtTxt) {
							case 'PT':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxt);
								break;
						}
						if (!parDocIdExt) {
							console.log('parDocIdExt',parDocIdExt);
							break;
						}
					}
					if (docIdExtTxtBen) {
						switch (docIdExtTxtBen) {
							case 'PT':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxtBen);
								break;
						}
						if (!parDocIdExtBen) {
							console.log('parDocIdExt',parDocIdExtBen);
							break;
						}
					}

					docIdExt = parDocIdExt && parDocIdExt.dataValues ? parDocIdExt.dataValues.parametro_cod : null;
					docIdExtBen = parDocIdExtBen && parDocIdExtBen.dataValues ? parDocIdExtBen.dataValues.parametro_cod : null;

					if (docIdExt) {

						let adicionadoPor = dato.cod_usuario_colibri ? dato.cod_usuario_colibri : 583;
						let parTipoDocumento = parTipoDocumentos.find(param => param.dataValues.parametro_cod.toUpperCase() == dato.tipo_documento_ID.toUpperCase());
						let tipoDocumento = parTipoDocumento ? parTipoDocumento.id : 'CE';

						let parSexoId, polizaId, parEstado, modalidadPago, parSucursal, parAgencia, cargo, nroCuotas, tipoDoc, parentescoId, parentescoDesc;

						if(dato.genero == 'varon') {
							parSexoId = parMasculino.id;
						} else if(dato.genero == 'mujer') {
							parSexoId = parFemenino.id;
						}
						if(dato.producto == 'ECOPROTECCION') {
							polizaId = polizaEcoProteccion.id;
						} else if (dato.producto == 'ECOVIDA') {
							polizaId = polizaEcoVida.id;
						}
						if(dato.emitido == 'SI') {
							parEstado = parEstadoEmitido.id;
						}
						if (dato.fecha_de_ingreso) {
							fechaIngreso = dato.fecha_de_ingreso;
						}
						if(dato.modalidad_pago == 'MENSUAL') {
							modalidadPago = parModalidadPagoMensual.id
						} else if (dato.modalidad_pago == 'ANUAL') {
							modalidadPago = parModalidadPagoAnual.id
						}
						cargo = 'ENCARGADO DE PLATAFORMA';

						if (dato.sucursal) {
							parSucursal = parSucursales.find(param => param.dataValues.parametro_cod == dato.cod_sucursal);
						}
						if(dato.desc_agencia) {
							parAgencia = parAgencias.find(param => param.dataValues.parametro_cod == dato.cod_agencia || dato.desc_agencia.includes(param.dataValues.parametro_descripcion));
						}
						if (!parAgencia) {
							dato.desc_agencia = dato.desc_agencia.removeAccents().toUpperCase();
							parAgencia = parAgencias.find(param => param.dataValues.parametro_descripcion.removeAccents().toUpperCase().includes(dato.desc_agencia));
						}
						if (parAgencia && !parSucursal) {
							parSucursal = parSucursales.find(param => param.id == parAgencia.dataValues.id_padre);
						}
						if (dato.tipo_documento_ID) {
							tipoDoc = dato.tipo_documento_ID.toUpperCase();
						}
						if (dato.parentesco.includes('convivi') || dato.parentesco.includes('convivi'.toUpperCase())) {
							parentescoId = parPareentescoConviviente.id;
						} else if (dato.parentesco.includes('conyugue') || dato.parentesco.includes('conyugue'.toUpperCase())){
							parentescoId = parPareentescoConyuge.id;
						} else if (dato.parentesco.includes('hij') || dato.parentesco.includes('hij'.toUpperCase())){
							parentescoId = parPareentescoHijo.id;
						} else if (dato.parentesco.includes('herman') || dato.parentesco.includes('herman'.toUpperCase())){
							parentescoId = parPareentescoHermano.id;
						} else if (dato.parentesco.includes('madre') || dato.parentesco.includes('madre'.toUpperCase())){
							parentescoId = parPareentescoMadre.id;
						} else if (dato.parentesco.includes('padre') || dato.parentesco.includes('padre'.toUpperCase())){
							parentescoId = parPareentescoPadre.id;
						} else {
							parentescoId = parPareentescoOtroParentesco.id;
							parentescoDesc = dato.parentesco;
						}
						nroCuotas = modalidadPago == parModalidadPagoMensual.id ? '12' : modalidadPago == parModalidadPagoAnual ? '1' : '';

						if(parAgencia && parSucursal && adicionadoPor) {

							let objetosPoliza = await modelos.objeto.findAll({where: {id_poliza: polizaId}});
							if (objetosPoliza.length) {
								let objetoXAtributos = await modelos.objeto_x_atributo.findAll(
									{
										where:{id_objeto: objetosPoliza[0].dataValues.id},
										include: { model: modelos.atributo }
									});
								let documentos = await modelos.documento.findAll({where: {id_poliza: polizaId}});
								let newPersona = {
									persona_doc_id: docId,
									persona_doc_id_ext: docIdExt,
									persona_primer_apellido: dato.apellido_paterno,
									persona_segundo_apellido: dato.apellido_materno,
									persona_primer_nombre: dato.nombres,
									persona_direccion_domicilio: dato.dir_domicilio,
									persona_fecha_nacimiento: dato.fecha_nac,
									persona_celular: dato.tel_movil,
									persona_telefono_domicilio: dato.tel_domicilio,
									par_tipo_documento_id: tipoDocumento,
									par_sexo_id: parSexoId,
									createdAt: fechaIngreso,
									updatedAt: createdAt,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor
								};
								let oldPersona = await modelos.persona.findOne({where:{
										persona_doc_id: docId,
										persona_doc_id_ext: docIdExt,
										persona_primer_apellido: dato.apellido_paterno,
										persona_segundo_apellido: dato.apellido_materno,
										persona_primer_nombre: dato.nombres,
									}});
								let persona;
								if (!oldPersona) {
									persona = await modelos.persona.create(newPersona);
									persona = persona.dataValues;
								} else {
									persona = await modelos.persona.update(newPersona,{where:{id:oldPersona.id}});
									persona = await modelos.persona.findOne({where:{id:oldPersona.id}});
									persona = persona.dataValues;
								}

								let newEntidad = {
									id_persona: persona.id,
									tipo_entidad: parPersonaNatural.id,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt
								};
								let oldEntidad = await modelos.entidad.findOne({where:{
										id_persona: persona.id,
										tipo_entidad: parPersonaNatural.id,
									}});

								let entidad;
								if (!oldEntidad) {
									entidad = await modelos.entidad.create(newEntidad);
									entidad = entidad.dataValues;
								} else {
									entidad = await modelos.entidad.update(newEntidad,{where:{id:oldEntidad.id}});
									entidad = await modelos.entidad.findOne({where:{id:oldEntidad.id}});
									entidad = entidad.dataValues;
								}

								let newInstanciaPoliza = {
									id_poliza: polizaId,
									id_estado: parEstado,
									fecha_registro: fechaIngreso,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt,
								};

								let instanciaPoliza;

								instanciaPoliza = await modelos.instancia_poliza.create(newInstanciaPoliza);
								instanciaPoliza = instanciaPoliza.dataValues;
								let updatedIntanciaPoliza = await modelos.instancia_poliza.update({id_instancia_renovada:instanciaPoliza.id}, {where:{id:instanciaPoliza.id}});

								let newAsegurado = {
									id_entidad: entidad.id,
									id_instancia_poliza: instanciaPoliza.id,
									adicionado_por: adicionadoPor,
									modificado_por: adicionadoPor,
									createdAt:  fechaIngreso,
									updatedAt: createdAt
								};

								let asegurado;

								asegurado = await modelos.asegurado.create(newAsegurado);
								asegurado = asegurado.dataValues;

								let instanciaDocumentos = [];
								for (let k = 0 ; k < documentos.length ; k++) {
									let documento = documentos[k].dataValues;
									let newInstanciaDocumeento = {
										id_instancia_poliza: instanciaPoliza.id,
										id_documento: documento.id,
										id_documento_version: documento.id_documento_version,
										nro_documento: documento.id_tipo_documento == 279 ? dato.nro_solicitud : documento.id_tipo_documento == 280 ? dato.nro_comprobante : documento.id_tipo_documento == 281 ? dato.nro_certificado : dato.nro_certificado,
										fecha_emision: fechaEmision,
										fecha_inicio_vigencia: fechaVigenciaInicio,
										fecha_fin_vigencia: fechaVigenciaFin,
										asegurado_doc_id: persona.persona_doc_id,
										asegurado_nombre1: persona.persona_primer_nombre,
										asegurado_nombre2: persona.persona_segundo_nombre,
										asegurado_apellido1: persona.persona_segundo_nombre,
										asegurado_apellido2: persona.persona_segundo_apellido,
										tipo_persona: entidad.id,
										adicionada_por: adicionadoPor,
										modificada_por: adicionadoPor,
										createdAt: fechaIngreso,
										updatedAt: createdAt
									};
									let oldInstanciaDocumento = await modelos.instancia_documento.findOne({where:{
											id_instancia_poliza: instanciaPoliza.id,
											id_documento: documento.id,
											nro_documento: dato.nro_certificado,
											fecha_emision: dato.fecha_emision_real,
										}});
									let instanciaDocumento;
									if (!oldInstanciaDocumento) {
										instanciaDocumento = await modelos.instancia_documento.create(newInstanciaDocumeento);
										instanciaDocumento = instanciaDocumento.dataValues;
									} else {
										instanciaDocumento =  await modelos.instancia_documento.update(newInstanciaDocumeento,{where:{id:oldInstanciaDocumento.id}});
										instanciaDocumento = await modelos.instancia_documento.findOne({where:{id:oldInstanciaDocumento.id}});
										instanciaDocumento = instanciaDocumento.dataValues;
									}

									instanciaDocumentos.push(instanciaDocumento);
								}

								let atributosInstanciaPoliza = [];
								for (let j = 0 ; j < objetoXAtributos.length ; j++) {
									let objetoXAtributo = objetoXAtributos[j].dataValues;
									let valor, tipoError;
									switch (objetoXAtributo.atributo.id) {
										case '3': valor = dato.estado_civil; tipoError = 'El titular no tiene registrado su estado civil'; break;
										case '4': valor = ''; tipoError = 'El titular no tiene registrado su e-mail'; break;
										case '5': valor = ''; tipoError = 'El titular no tiene registrado un numero de cuenta'; break;
										case '10': valor = dato.codigo_de_agenda; tipoError = 'El titular no tiene registrado un codigo de agenda'; break;
										case '12': valor = parCondicionSi.id; tipoError = 'El titular no tiene registrado si el debito sera automatico'; break;
										case '14': valor = ''; tipoError = 'El titular no tiene registrado un numero CAEDEC'; break;
										case '16': valor = ''; tipoError = 'El titular no tiene registrado una localidad'; break;
										case '17': valor = ''; tipoError = 'El titular no tiene registrado un departamento'; break;
										case '18': valor = ''; tipoError = 'El titular no tiene registrado un codigo de sucursal'; break;
										case '19': valor = tipoDoc; tipoError = 'El titular no tiene registrado el tipo de documento utilizado'; break;
										case '20': valor = ''; tipoError = 'El titular no tiene registrado un codigo de manejo'; break;
										case '21': valor = ''; tipoError = 'El titular no tiene registrado un tipo de moneda'; break;
										case '22': valor = ''; tipoError = 'El titular no tiene registrado una descripción en caedec'; break;
										case '28': valor = modalidadPago; tipoError = 'El titular no tiene registrado una modalidad de pago'; break;
										case '29': valor = ''; tipoError = 'El titular no tiene registrado una zona'; break;
										case '30': valor = ''; tipoError = 'El titular no tiene registrado un Nro de direccion'; break;
										case '37': valor = ''; tipoError = 'El titular no tiene registrado una ocupación'; break;
										case '60': valor = parSucursal.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una sucursal'; break;
										case '59': valor = parAgencia.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una agencia'; break;
										case '61': valor = dato.prima; tipoError = 'El titular no tiene registrado el monto de la cuota'; break;
										case '63': valor = ''; tipoError = 'El titular no tiene registrada una ciudad de nacimiento'; break;
										case '64': valor = ''; tipoError = 'La transaccion no tiene registrado un nro de transacción'; break;
										case '65': valor = ''; tipoError = 'La transaccion no tiene registrado una fecha de transacción'; break;
										case '66': valor = dato.prima; tipoError = 'La transaccion no tiene registrado un importe'; break;
										case '67': valor = ''; tipoError = 'La transaccion no tiene registrado un tipo de moneda'; break;
										case '69': valor = ''; tipoError = 'La transaccion no tiene registrado un detalle de transacción'; break;
										case '70': valor = cargo; tipoError = 'El usuario del banco no tiene registrado un cargo'; break;
										case '71': valor = ''; tipoError = 'El titular no tiene un cargo publico y/o politico jerarquico'; break;
										case '72': valor = ''; tipoError = 'El titular no tiene registrado un cargo/entidad pep'; break;
										case '73': valor = ''; tipoError = 'El titular no tiene registrado un periodo del cargo publico pep'; break;
										case '74': valor = ''; tipoError = 'El titular no tiene una  direccion laboral'; break;
										case '75': valor = ''; tipoError = 'El titular no tiene un tipo de cuenta'; break;
										case '76': valor = nroCuotas; tipoError = 'El titular no tiene registrado un Nro. de cuotas'; break;
										case '77': valor = ''; tipoError = 'El titular no tiene registrado la descripción de su ocupación'; break;
									}

									let newAtributoInstanciaPoliza = {
										id_instancia_poliza: instanciaPoliza.id,
										id_objeto_x_atributo: objetoXAtributo.id,
										valor: valor,
										tipo_error: tipoError,
										adicionado_por: adicionadoPor,
										modificado_por: adicionadoPor,
										createdAt: fechaIngreso,
										updatedAt: createdAt
									};
									let oldAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{
											id_instancia_poliza: instanciaPoliza.id,
											id_objeto_x_atributo: objetoXAtributo.id,
											valor: valor,
											tipo_error: tipoError
										}});
									let atributoInstanciaPoliza;
									if (!oldAtributoInstanciaPoliza) {
										atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.create(newAtributoInstanciaPoliza);
										atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
									} else {
										atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.update(newAtributoInstanciaPoliza,{where:{id:oldAtributoInstanciaPoliza.id}});
										atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id:oldAtributoInstanciaPoliza.id}});
										atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
									}
									atributosInstanciaPoliza.push(atributoInstanciaPoliza);
								}

								let newPersonaBeneficiario = {
									persona_primer_apellido:dato.apellido_paterno_beneficiario,
									persona_segundo_apellido:dato.apellido_materno_beneficiario,
									persona_primer_nombre:dato.nombres_beneficiario,
									persona_doc_id:docIdBen,
									persona_doc_id_ext:docIdExtBen,
									createdAt:fechaIngreso,
									updatedAt:createdAt,
									adicionada_por:adicionadoPor,
									modificada_por:adicionadoPor
								}

								let personaBeneficiario;

								personaBeneficiario = await modelos.persona.create(newPersonaBeneficiario);
								personaBeneficiario = personaBeneficiario.dataValues;

								let newEntidadBeneficiario = {
									id_persona: personaBeneficiario.id,
									tipo_entidad:12,
									adicionada_por:adicionadoPor,
									modificada_por:adicionadoPor,
									createdAt:fechaIngreso,
									updatedAt:createdAt
								}

								let entidadBeneficiario;

								entidadBeneficiario = await modelos.entidad.create(newEntidadBeneficiario);
								entidadBeneficiario = entidadBeneficiario.dataValues;

								let newBeneficiario = {
									id_asegurado: asegurado.id,
									id_beneficiario: entidadBeneficiario.id,
									id_parentesco: parentescoId,
									porcentaje: dato.porcentaje,
									adicionado_por: adicionadoPor,
									modificado_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt,
									descripcion_otro: parentescoDesc,
									estado: parBeneficiarioActivo.id,
									tipo: parBeneficiarioOriginal.id,
									fecha_declaracion: fechaIngreso
								};
								let oldBeneficiario = await modelos.beneficiario.findOne({where:{
										id_asegurado: asegurado.id,
										id_beneficiario: entidad.id,
										id_parentesco: parentescoId,
										porcentaje: dato.porcentaje,
									}});

								let beneficiario;
								if (!oldBeneficiario) {
									beneficiario = await modelos.beneficiario.create(newBeneficiario);
									beneficiario = beneficiario.dataValues;
								} else {
									beneficiario = await modelos.beneficiario.update(newBeneficiario,{where:{id:oldBeneficiario.id}});
									beneficiario = await modelos.beneficiario.findOne({where:{id:oldBeneficiario.id}});
									beneficiario = beneficiario.dataValues;
								}
								let tmpEcovidaComplementoBeneficiarioV1 = await modelos.mig_ecovida.update({
									id_instancia_poliza:instanciaPoliza.id.toString(),
									id_persona_beneficiario:personaBeneficiario.id,
									FEC_INI_VIG_REAL:fechaVigenciaInicio,
									FEC_FIN_VIG_REAL:fechaVigenciaFin
								}, {where:{id:dato.id}});
								response.data.push({
									persona: persona,
									entidad: entidad,
									asegurado: asegurado,
									instanciaPoliza: instanciaPoliza,
									atributosInstanciaPoliza: atributosInstanciaPoliza,
									beneficiario: beneficiario,
									instanciaDocumentos: instanciaDocumentos
								});
							}
						} else {
							response.errors.push({
								params: {
									sucursal:parSucursal ? parSucursal.dataValues : '',
									agencia:parAgencia ? parAgencia.dataValues : ''
								},
								dato: dato.dataValues
							});
						}
					}
				}
			}
			return response;
		} catch (e) {
			console.log(e)
		}
	}


	static async migraEcoriesgo(req, res) {
		try {
			let response = { status: 'OK', message: '', data: [], errors:[] };
			let parametros = await modelos.parametro.findAll({where: {diccionario_id: [1, 4, 7, 11, 18, 20, 21, 22, 25, 29, 31, 38, 40]}});
			let hoy = new Date();
			let createdAt = hoy;
			let year = hoy.getFullYear(), month = hoy.getMonth(), day = hoy.getDate();
			let polizas = await modelos.poliza.findAll();
			let fechaIngreso;
			let datos = await modelos.mig_ecoriesgo.findAll({where:{$and:[
						{id_instancia_poliza:null},
						{ci:{$not:null}},
						{ci:{$not:'NULL'}}
					]}});

			// parametros
			let parTipoDocumentos = parametros.filter(param => param.dataValues.diccionario_id == 1);
			let parMasculino = parametros.find(param => param.dataValues.id == 6);
			let parFemenino = parametros.find(param => param.dataValues.id == 7);
			let parPersonaNatural = parametros.find(param => param.dataValues.id == 12);
			let parPersonaJuridica = parametros.find(param => param.dataValues.id == 13);
			let polizaEcoProteccion = polizas.find(param => param.dataValues.id == 7);
			let polizaEcoVida = polizas.find(param => param.dataValues.id == 6);
			let polizaEcoRiesgo = polizas.find(param => param.dataValues.id == 11);
			let parEstadoEmitido = parametros.find(param => param.dataValues.id == 59);
			let parEstadoIniciado =  parametros.find(param => param.dataValues.id == 24);
			let parEstadoSolicitado = parametros.find(param => param.dataValues.id == 81);
			let parModalidadPagoMensual = parametros.find(param => param.dataValues.id == 90);
			let parModalidadPagoAnual = parametros.find(param => param.dataValues.id == 93);
			let parSucursales = parametros.filter(param => param.dataValues.diccionario_id == 38);
			let parAgencias = parametros.filter(param => param.dataValues.diccionario_id == 40);
			let parCondiciones = parametros.filter(param => param.dataValues.diccionario_id == 21);
			let parCondicionSi = parametros.find(param => param.dataValues.id == 57);
			let parCondicionNo = parametros.find(param => param.dataValues.id == 58);
			let parMonedaBs = parametros.find(param => param.dataValues.id == 68);
			let parPareentescoConviviente = parametros.find(param => param.dataValues.id == 48);
			let parPareentescoConyuge = parametros.find(param => param.dataValues.id == 49);
			let parPareentescoHijo = parametros.find(param => param.dataValues.id == 50);
			let parPareentescoHermano = parametros.find(param => param.dataValues.id == 52);
			let parPareentescoMadre = parametros.find(param => param.dataValues.id == 53);
			let parPareentescoPadre = parametros.find(param => param.dataValues.id == 55);
			let parPareentescoOtroParentesco = parametros.find(param => param.dataValues.id == 56);
			let parBeneficiarioActivo = parametros.find(param => param.dataValues.id == 83);
			let parBeneficiarioOriginal = parametros.find(param => param.dataValues.id == 86);

			for (let i = 0 ; i < datos.length ; i++) {
				let dato = datos[i].dataValues;
				let fechaEmision = new Date(dato.fecha_emision);
				let fechaEmisionStr = util.minTwoDigits(fechaEmision.getDate())+'/'+util.minTwoDigits(fechaEmision.getMonth()+1)+'/'+fechaEmision.getFullYear();
				let fechaVigenciaInicioStr = fechaEmisionStr
				let fechaVigenciaFinStr = fechas.addMeses(fechaEmisionStr,12);
				let [dayIni,monthIni,yearIni] = fechaVigenciaInicioStr.split('/');
				let [dayFin,monthFin,yearFin] = fechaVigenciaFinStr.split('/');
				let fechaVigenciaInicio = new Date(yearIni,parseInt(monthIni)-1,dayIni);
				let fechaVigenciaFin = new Date(yearFin,parseInt(monthFin)-1,dayFin);
				let fechaFinVigencia = new Date(dato.fecha_emision_real);
				let fechaFinVigenciaToSave = (fechaFinVigencia.getFullYear()+1) + '/' + (fechaFinVigencia.getMonth() + 1) + '/' + fechaFinVigencia.getDate() + ' ' + fechaFinVigencia.getHours() + ':' + fechaFinVigencia.getMinutes() + ':' + fechaFinVigencia.getSeconds();
				if (dato.ci && dato.ci != 'NULL') {
					let [docId,docIdExtTxt] = dato.ci.split(' ');
					let [docIdBen,docIdExtTxtBen] = dato.ci_beneficiario ? dato.ci_beneficiario.split(' ') : ['',''];
					let docIdExt, parDocIdExt, docIdExtBen, parDocIdExtBen;
					if (docIdExtTxt) {
						switch (docIdExtTxt) {
							case 'PT':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxt);
								break;
						}
						if (!parDocIdExt) {
							console.log('parDocIdExt',parDocIdExt);
							break;
						}
					}
					if (docIdExtTxtBen) {
						switch (docIdExtTxtBen) {
							case 'PT':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxtBen);
								break;
						}
						if (!parDocIdExtBen) {
							console.log('parDocIdExt',parDocIdExtBen);
							break;
						}
					}

					docIdExt = parDocIdExt && parDocIdExt.dataValues ? parDocIdExt.dataValues.parametro_cod : null;
					docIdExtBen = parDocIdExtBen && parDocIdExtBen.dataValues ? parDocIdExtBen.dataValues.parametro_cod : null;

					if (docIdExt) {

						let adicionadoPor = dato.cod_usuario_colibri ? dato.cod_usuario_colibri : 583;
						let parTipoDocumento = parTipoDocumentos.find(param => param.dataValues.parametro_cod.toUpperCase() == dato.tipo_documento_ID.toUpperCase());
						let tipoDocumento = parTipoDocumento ? parTipoDocumento.id : 'CE';

						let parSexoId, polizaId, parEstado, modalidadPago, parSucursal, parAgencia, cargo, nroCuotas, tipoDoc, parentescoId, parentescoDesc;

						if(dato.genero == 'varon') {
							parSexoId = parMasculino.id;
						} else if(dato.genero == 'mujer') {
							parSexoId = parFemenino.id;
						}
						if(dato.producto == 'ECOPROTECCION') {
							polizaId = polizaEcoProteccion.id;
						} else if (dato.producto == 'ECOVIDA') {
							polizaId = polizaEcoVida.id;
						} else if (dato.producto == 'ECORIESGO') {
							polizaId = polizaEcoRiesgo.id;
						}
						if(dato.emitido == 'SI') {
							parEstado = parEstadoEmitido.id;
						}
						if (dato.fecha_de_ingreso) {
							fechaIngreso = dato.fecha_de_ingreso;
						}
						if(dato.modalidad_pago == 'MENSUAL') {
							modalidadPago = parModalidadPagoMensual.id
						} else if (dato.modalidad_pago == 'ANUAL') {
							modalidadPago = parModalidadPagoAnual.id
						}
						cargo = 'ENCARGADO DE PLATAFORMA';

						if (dato.sucursal) {
							parSucursal = parSucursales.find(param => param.dataValues.parametro_cod == dato.cod_sucursal);
						}
						if(dato.desc_agencia) {
							parAgencia = parAgencias.find(param => param.dataValues.parametro_cod == dato.cod_agencia || dato.desc_agencia.includes(param.dataValues.parametro_descripcion));
						}
						if (!parAgencia) {
							dato.desc_agencia = dato.desc_agencia.removeAccents().toUpperCase();
							parAgencia = parAgencias.find(param => param.dataValues.parametro_descripcion.removeAccents().toUpperCase().includes(dato.desc_agencia));
						}
						if (parAgencia && !parSucursal) {
							parSucursal = parSucursales.find(param => param.id == parAgencia.dataValues.id_padre);
						}
						if (dato.tipo_documento_ID) {
							tipoDoc = dato.tipo_documento_ID.toUpperCase();
						}
						if (dato.parentesco.includes('convivi') || dato.parentesco.includes('convivi'.toUpperCase())) {
							parentescoId = parPareentescoConviviente.id;
						} else if (dato.parentesco.includes('conyugue') || dato.parentesco.includes('conyugue'.toUpperCase())){
							parentescoId = parPareentescoConyuge.id;
						} else if (dato.parentesco.includes('hij') || dato.parentesco.includes('hij'.toUpperCase())){
							parentescoId = parPareentescoHijo.id;
						} else if (dato.parentesco.includes('herman') || dato.parentesco.includes('herman'.toUpperCase())){
							parentescoId = parPareentescoHermano.id;
						} else if (dato.parentesco.includes('madre') || dato.parentesco.includes('madre'.toUpperCase())){
							parentescoId = parPareentescoMadre.id;
						} else if (dato.parentesco.includes('padre') || dato.parentesco.includes('padre'.toUpperCase())){
							parentescoId = parPareentescoPadre.id;
						} else {
							parentescoId = parPareentescoOtroParentesco.id;
							parentescoDesc = dato.parentesco;
						}
						nroCuotas = modalidadPago == parModalidadPagoMensual.id ? '12' : modalidadPago == parModalidadPagoAnual ? '1' : '';

						if(parAgencia && parSucursal && adicionadoPor) {

							let objetosPoliza = await modelos.objeto.findAll({where: {id_poliza: polizaId}});
							if (objetosPoliza.length) {
								let objetoXAtributos = await modelos.objeto_x_atributo.findAll(
									{
										where:{id_objeto: objetosPoliza[0].dataValues.id},
										include: { model: modelos.atributo }
									});
								let documentos = await modelos.documento.findAll({where: {id_poliza: polizaId}});
								let newPersona = {
									persona_doc_id: docId,
									persona_doc_id_ext: docIdExt,
									persona_primer_apellido: dato.apellido_paterno,
									persona_segundo_apellido: dato.apellido_materno,
									persona_primer_nombre: dato.nombres,
									persona_direccion_domicilio: dato.dir_domicilio,
									persona_fecha_nacimiento: dato.fecha_nac,
									persona_celular: dato.tel_movil,
									persona_telefono_domicilio: dato.tel_domicilio,
									par_tipo_documento_id: tipoDocumento,
									par_sexo_id: parSexoId,
									createdAt: fechaIngreso,
									updatedAt: createdAt,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor
								};
								let oldPersona = await modelos.persona.findOne({where:{
										persona_doc_id: docId,
										persona_doc_id_ext: docIdExt,
										persona_primer_apellido: dato.apellido_paterno,
										persona_segundo_apellido: dato.apellido_materno,
										persona_primer_nombre: dato.nombres,
									}});
								let persona;
								if (!oldPersona) {
									persona = await modelos.persona.create(newPersona);
									persona = persona.dataValues;
								} else {
									persona = await modelos.persona.update(newPersona,{where:{id:oldPersona.id}});
									persona = await modelos.persona.findOne({where:{id:oldPersona.id}});
									persona = persona.dataValues;
								}

								let newEntidad = {
									id_persona: persona.id,
									tipo_entidad: parPersonaNatural.id,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt
								};
								let oldEntidad = await modelos.entidad.findOne({where:{
										id_persona: persona.id,
										tipo_entidad: parPersonaNatural.id,
									}});

								let entidad;
								if (!oldEntidad) {
									entidad = await modelos.entidad.create(newEntidad);
									entidad = entidad.dataValues;
								} else {
									entidad = await modelos.entidad.update(newEntidad,{where:{id:oldEntidad.id}});
									entidad = await modelos.entidad.findOne({where:{id:oldEntidad.id}});
									entidad = entidad.dataValues;
								}

								let newInstanciaPoliza = {
									id_poliza: polizaId,
									id_estado: parEstado,
									fecha_registro: fechaIngreso,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt,
								};

								let instanciaPoliza;

								instanciaPoliza = await modelos.instancia_poliza.create(newInstanciaPoliza);
								instanciaPoliza = instanciaPoliza.dataValues;
								let updatedIntanciaPoliza = await modelos.instancia_poliza.update({id_instancia_renovada:instanciaPoliza.id}, {where:{id:instanciaPoliza.id}});

								let newAsegurado = {
									id_entidad: entidad.id,
									id_instancia_poliza: instanciaPoliza.id,
									adicionado_por: adicionadoPor,
									modificado_por: adicionadoPor,
									createdAt:  fechaIngreso,
									updatedAt: createdAt
								};

								let asegurado;

								asegurado = await modelos.asegurado.create(newAsegurado);
								asegurado = asegurado.dataValues;

								let instanciaDocumentos = [];
								for (let k = 0 ; k < documentos.length ; k++) {
									let documento = documentos[k].dataValues;
									let newInstanciaDocumeento = {
										id_instancia_poliza: instanciaPoliza.id,
										id_documento: documento.id,
										id_documento_version: documento.id_documento_version,
										nro_documento: documento.id_tipo_documento == 279 ? dato.nro_solicitud : documento.id_tipo_documento == 280 ? dato.nro_comprobante : documento.id_tipo_documento == 281 ? dato.nro_certificado : dato.nro_certificado,
										fecha_emision: fechaEmision,
										fecha_inicio_vigencia: fechaVigenciaInicio,
										fecha_fin_vigencia: fechaVigenciaFin,
										asegurado_doc_id: persona.persona_doc_id,
										asegurado_nombre1: persona.persona_primer_nombre,
										asegurado_nombre2: persona.persona_segundo_nombre,
										asegurado_apellido1: persona.persona_segundo_nombre,
										asegurado_apellido2: persona.persona_segundo_apellido,
										tipo_persona: entidad.id,
										adicionada_por: adicionadoPor,
										modificada_por: adicionadoPor,
										createdAt: fechaIngreso,
										updatedAt: createdAt
									};
									let oldInstanciaDocumento = await modelos.instancia_documento.findOne({where:{
											id_instancia_poliza: instanciaPoliza.id,
											id_documento: documento.id,
											nro_documento: dato.nro_certificado,
											fecha_emision: dato.fecha_emision_real,
										}});
									let instanciaDocumento;
									if (!oldInstanciaDocumento) {
										instanciaDocumento = await modelos.instancia_documento.create(newInstanciaDocumeento);
										instanciaDocumento = instanciaDocumento.dataValues;
									} else {
										instanciaDocumento =  await modelos.instancia_documento.update(newInstanciaDocumeento,{where:{id:oldInstanciaDocumento.id}});
										instanciaDocumento = await modelos.instancia_documento.findOne({where:{id:oldInstanciaDocumento.id}});
										instanciaDocumento = instanciaDocumento.dataValues;
									}

									instanciaDocumentos.push(instanciaDocumento);
								}

								let atributosInstanciaPoliza = [];
								for (let j = 0 ; j < objetoXAtributos.length ; j++) {
									let objetoXAtributo = objetoXAtributos[j].dataValues;
									let valor, tipoError;
									switch (objetoXAtributo.atributo.id) {
										case '3': valor = dato.estado_civil; tipoError = 'El titular no tiene registrado su estado civil'; break;
										case '4': valor = dato.correo ; tipoError = 'El titular no tiene registrado su e-mail'; break;
										case '5': valor = dato.cta; tipoError = 'El titular no tiene registrado un numero de cuenta'; break;
										case '10': valor = dato.codigo_de_agenda; tipoError = 'El titular no tiene registrado un codigo de agenda'; break;
										case '12': valor = parCondicionSi.id; tipoError = 'El titular no tiene registrado si el debito sera automatico'; break;
										case '14': valor = dato.cod_caedec; tipoError = 'El titular no tiene registrado un numero CAEDEC'; break;
										case '16': valor = dato.localidad; tipoError = 'El titular no tiene registrado una localidad'; break;
										case '17': valor = dato.departamento; tipoError = 'El titular no tiene registrado un departamento'; break;
										case '18': valor = dato.cod_sucursal_2; tipoError = 'El titular no tiene registrado un codigo de sucursal'; break;
										case '19': valor = tipoDoc; tipoError = 'El titular no tiene registrado el tipo de documento utilizado'; break;
										case '20': valor = dato.manejo ; tipoError = 'El titular no tiene registrado un codigo de manejo'; break;
										case '21': valor = dato.moneda; tipoError = 'El titular no tiene registrado un tipo de moneda'; break;
										case '22': valor = dato.desc_caedec; tipoError = 'El titular no tiene registrado una descripción en caedec'; break;
										case '28': valor = modalidadPago; tipoError = 'El titular no tiene registrado una modalidad de pago'; break;
										case '29': valor = dato.zona; tipoError = 'El titular no tiene registrado una zona'; break;
										case '30': valor = dato.nro_direccion; tipoError = 'El titular no tiene registrado un Nro de direccion'; break;
										case '37': valor = dato.ocupacion; tipoError = 'El titular no tiene registrado una ocupación'; break;
										case '60': valor = parSucursal.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una sucursal'; break;
										case '59': valor = parAgencia.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una agencia'; break;
										case '61': valor = dato.prima; tipoError = 'El titular no tiene registrado el monto de la cuota'; break;
										case '63': valor = dato.ciudad_nacimiento; tipoError = 'El titular no tiene registrada una ciudad de nacimiento'; break;
										case '64': valor = dato.nro_transaccion; tipoError = 'La transaccion no tiene registrado un nro de transacción'; break;
										case '65': valor = dato.fecha_transaccion; tipoError = 'La transaccion no tiene registrado una fecha de transacción'; break;
										case '66': valor = dato.importe_transaccion; tipoError = 'La transaccion no tiene registrado un importe'; break;
										case '67': valor = dato.moneda; tipoError = 'La transaccion no tiene registrado un tipo de moneda'; break;
										case '69': valor = dato.detalle_transaccion; tipoError = 'La transaccion no tiene registrado un detalle de transacción'; break;
										case '70': valor = cargo; tipoError = 'El usuario del banco no tiene registrado un cargo'; break;
										case '71': valor = ''; tipoError = 'El titular no tiene un cargo publico y/o politico jerarquico'; break;
										case '72': valor = ''; tipoError = 'El titular no tiene registrado un cargo/entidad pep'; break;
										case '73': valor = ''; tipoError = 'El titular no tiene registrado un periodo del cargo publico pep'; break;
										case '74': valor = ''; tipoError = 'El titular no tiene una  direccion laboral'; break;
										case '75': valor = ''; tipoError = 'El titular no tiene un tipo de cuenta'; break;
										case '76': valor = nroCuotas; tipoError = 'El titular no tiene registrado un Nro. de cuotas'; break;
										case '77': valor = ''; tipoError = 'El titular no tiene registrado la descripción de su ocupación'; break;
									}

									let newAtributoInstanciaPoliza = {
										id_instancia_poliza: instanciaPoliza.id,
										id_objeto_x_atributo: objetoXAtributo.id,
										valor: valor,
										tipo_error: tipoError,
										adicionado_por: adicionadoPor,
										modificado_por: adicionadoPor,
										createdAt: fechaIngreso,
										updatedAt: createdAt
									};
									let oldAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{
											id_instancia_poliza: instanciaPoliza.id,
											id_objeto_x_atributo: objetoXAtributo.id,
											valor: valor,
											tipo_error: tipoError
										}});
									let atributoInstanciaPoliza;
									if (!oldAtributoInstanciaPoliza) {
										atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.create(newAtributoInstanciaPoliza);
										atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
									} else {
										atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.update(newAtributoInstanciaPoliza,{where:{id:oldAtributoInstanciaPoliza.id}});
										atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id:oldAtributoInstanciaPoliza.id}});
										atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
									}
									atributosInstanciaPoliza.push(atributoInstanciaPoliza);
								}

								let newPersonaBeneficiario = {
									persona_primer_apellido:dato.apellido_paterno_beneficiario,
									persona_segundo_apellido:dato.apellido_materno_beneficiario,
									persona_primer_nombre:dato.nombres_beneficiario,
									persona_doc_id:docIdBen,
									persona_doc_id_ext:docIdExtBen,
									createdAt:fechaIngreso,
									updatedAt:createdAt,
									adicionada_por:adicionadoPor,
									modificada_por:adicionadoPor
								}

								let personaBeneficiario;

								personaBeneficiario = await modelos.persona.create(newPersonaBeneficiario);
								personaBeneficiario = personaBeneficiario.dataValues;

								let newEntidadBeneficiario = {
									id_persona: personaBeneficiario.id,
									tipo_entidad:12,
									adicionada_por:adicionadoPor,
									modificada_por:adicionadoPor,
									createdAt:fechaIngreso,
									updatedAt:createdAt
								}

								let entidadBeneficiario;

								entidadBeneficiario = await modelos.entidad.create(newEntidadBeneficiario);
								entidadBeneficiario = entidadBeneficiario.dataValues;

								let newBeneficiario = {
									id_asegurado: asegurado.id,
									id_beneficiario: entidadBeneficiario.id,
									id_parentesco: parentescoId,
									porcentaje: dato.porcentaje,
									adicionado_por: adicionadoPor,
									modificado_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt,
									descripcion_otro: parentescoDesc,
									estado: parBeneficiarioActivo.id,
									tipo: parBeneficiarioOriginal.id,
									fecha_declaracion: fechaIngreso
								};
								let oldBeneficiario = await modelos.beneficiario.findOne({where:{
										id_asegurado: asegurado.id,
										id_beneficiario: entidad.id,
										id_parentesco: parentescoId,
										porcentaje: dato.porcentaje,
									}});

								let beneficiario;
								if (!oldBeneficiario) {
									beneficiario = await modelos.beneficiario.create(newBeneficiario);
									beneficiario = beneficiario.dataValues;
								} else {
									beneficiario = await modelos.beneficiario.update(newBeneficiario,{where:{id:oldBeneficiario.id}});
									beneficiario = await modelos.beneficiario.findOne({where:{id:oldBeneficiario.id}});
									beneficiario = beneficiario.dataValues;
								}
								let tmpEcoriesgoComplementoBeneficiarioV1 = await modelos.mig_ecoriesgo.update({
									id_instancia_poliza:instanciaPoliza.id.toString(),
									id_persona_beneficiario:personaBeneficiario.id,
									FEC_INI_VIG_REAL:fechaVigenciaInicio,
									FEC_FIN_VIG_REAL:fechaVigenciaFin
								}, {where:{id:dato.id}});
								response.data.push({
									persona: persona,
									entidad: entidad,
									asegurado: asegurado,
									instanciaPoliza: instanciaPoliza,
									atributosInstanciaPoliza: atributosInstanciaPoliza,
									beneficiario: beneficiario,
									instanciaDocumentos: instanciaDocumentos
								});
							}
						} else {
							response.errors.push({
								params: {
									sucursal:parSucursal ? parSucursal.dataValues : '',
									agencia:parAgencia ? parAgencia.dataValues : ''
								},
								dato: dato.dataValues
							});
						}
					}
				}
			}
			return response;
		} catch (e) {
			console.log(e)
		}
	}

	static async migraEcoaguinaldo(req, res) {
		try {
			let response = { status: 'OK', message: '', data: [], errors:[] };
			let parametros = await modelos.parametro.findAll({where: {diccionario_id: [1, 4, 7, 11, 18, 20, 21, 22, 25, 29, 31, 38, 40]}});
			let hoy = new Date();
			let createdAt = hoy;
			let year = hoy.getFullYear(), month = hoy.getMonth(), day = hoy.getDate();
			let polizas = await modelos.poliza.findAll();
			let fechaIngreso;
			let datos = await modelos.mig_ecoaguinaldo.findAll({where:{$and:[
						{id_instancia_poliza:null},
						{ci:{$not:null}},
						{ci:{$not:'NULL'}}
					]}});

			// parametros
			let parTipoDocumentos = parametros.filter(param => param.dataValues.diccionario_id == 1);
			let parMasculino = parametros.find(param => param.dataValues.id == 6);
			let parFemenino = parametros.find(param => param.dataValues.id == 7);
			let parPersonaNatural = parametros.find(param => param.dataValues.id == 12);
			let parPersonaJuridica = parametros.find(param => param.dataValues.id == 13);
			let polizaEcoProteccion = polizas.find(param => param.dataValues.id == 7);
			let polizaEcoVida = polizas.find(param => param.dataValues.id == 6);
			let polizaEcoRiesgo = polizas.find(param => param.dataValues.id == 11);
			let parEstadoEmitido = parametros.find(param => param.dataValues.id == 59);
			let parEstadoIniciado =  parametros.find(param => param.dataValues.id == 24);
			let parEstadoSolicitado = parametros.find(param => param.dataValues.id == 81);
			let parModalidadPagoMensual = parametros.find(param => param.dataValues.id == 90);
			let parModalidadPagoAnual = parametros.find(param => param.dataValues.id == 93);
			let parSucursales = parametros.filter(param => param.dataValues.diccionario_id == 38);
			let parAgencias = parametros.filter(param => param.dataValues.diccionario_id == 40);
			let parCondiciones = parametros.filter(param => param.dataValues.diccionario_id == 21);
			let parCondicionSi = parametros.find(param => param.dataValues.id == 57);
			let parCondicionNo = parametros.find(param => param.dataValues.id == 58);
			let parMonedaBs = parametros.find(param => param.dataValues.id == 68);
			let parPareentescoConviviente = parametros.find(param => param.dataValues.id == 48);
			let parPareentescoConyuge = parametros.find(param => param.dataValues.id == 49);
			let parPareentescoHijo = parametros.find(param => param.dataValues.id == 50);
			let parPareentescoHermano = parametros.find(param => param.dataValues.id == 52);
			let parPareentescoMadre = parametros.find(param => param.dataValues.id == 53);
			let parPareentescoPadre = parametros.find(param => param.dataValues.id == 55);
			let parPareentescoOtroParentesco = parametros.find(param => param.dataValues.id == 56);
			let parBeneficiarioActivo = parametros.find(param => param.dataValues.id == 83);
			let parBeneficiarioOriginal = parametros.find(param => param.dataValues.id == 86);

			for (let i = 0 ; i < datos.length ; i++) {
				let dato = datos[i].dataValues;
				let fechaEmision = new Date(dato.fecha_emision);
				let fechaEmisionStr = util.minTwoDigits(fechaEmision.getDate())+'/'+util.minTwoDigits(fechaEmision.getMonth()+1)+'/'+fechaEmision.getFullYear();
				let fechaVigenciaInicioStr = fechaEmisionStr
				let fechaVigenciaFinStr = fechas.addMeses(fechaEmisionStr,12);
				let [dayIni,monthIni,yearIni] = fechaVigenciaInicioStr.split('/');
				let [dayFin,monthFin,yearFin] = fechaVigenciaFinStr.split('/');
				let fechaVigenciaInicio = new Date(yearIni,parseInt(monthIni)-1,dayIni);
				let fechaVigenciaFin = new Date(yearFin,parseInt(monthFin)-1,dayFin);
				let fechaFinVigencia = new Date(dato.fecha_emision_real);
				let fechaFinVigenciaToSave = (fechaFinVigencia.getFullYear()+1) + '/' + (fechaFinVigencia.getMonth() + 1) + '/' + fechaFinVigencia.getDate() + ' ' + fechaFinVigencia.getHours() + ':' + fechaFinVigencia.getMinutes() + ':' + fechaFinVigencia.getSeconds();
				if (dato.ci && dato.ci != 'NULL') {
					let [docId,docIdExtTxt] = dato.ci.split(' ');
					let [docIdBen,docIdExtTxtBen] = dato.ci_beneficiario ? dato.ci_beneficiario.split(' ') : ['',''];
					let docIdExt, parDocIdExt, docIdExtBen, parDocIdExtBen;
					if (docIdExtTxt) {
						switch (docIdExtTxt) {
							case 'PT':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxt);
								break;
						}
						if (!parDocIdExt) {
							console.log('parDocIdExt',parDocIdExt);
							break;
						}
					}
					if (docIdExtTxtBen) {
						switch (docIdExtTxtBen) {
							case 'PT':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxtBen);
								break;
						}
						if (!parDocIdExtBen) {
							console.log('parDocIdExt',parDocIdExtBen);
							break;
						}
					}

					docIdExt = parDocIdExt && parDocIdExt.dataValues ? parDocIdExt.dataValues.parametro_cod : null;
					docIdExtBen = parDocIdExtBen && parDocIdExtBen.dataValues ? parDocIdExtBen.dataValues.parametro_cod : null;

					if (docIdExt) {

						let adicionadoPor = dato.cod_usuario_colibri ? dato.cod_usuario_colibri : 583;
						let parTipoDocumento = parTipoDocumentos.find(param => param.dataValues.parametro_cod.toUpperCase() == dato.tipo_documento_ID.toUpperCase());
						let tipoDocumento = parTipoDocumento ? parTipoDocumento.id : 'CE';

						let parSexoId, polizaId, parEstado, modalidadPago, parSucursal, parAgencia, cargo, nroCuotas, tipoDoc, parentescoId, parentescoDesc;

						if(dato.genero == 'varon') {
							parSexoId = parMasculino.id;
						} else if(dato.genero == 'mujer') {
							parSexoId = parFemenino.id;
						}
						if(dato.producto == 'ECOPROTECCION') {
							polizaId = polizaEcoProteccion.id;
						} else if (dato.producto == 'ECOVIDA') {
							polizaId = polizaEcoVida.id;
						} else if (dato.producto == 'ECORIESGO') {
							polizaId = polizaEcoRiesgo.id;
						}
						if(dato.emitido == 'SI') {
							parEstado = parEstadoEmitido.id;
						}
						if (dato.fecha_de_ingreso) {
							fechaIngreso = dato.fecha_de_ingreso;
						}
						if(dato.modalidad_pago == 'MENSUAL') {
							modalidadPago = parModalidadPagoMensual.id
						} else if (dato.modalidad_pago == 'ANUAL') {
							modalidadPago = parModalidadPagoAnual.id
						}
						cargo = 'ENCARGADO DE PLATAFORMA';

						if (dato.sucursal) {
							parSucursal = parSucursales.find(param => param.dataValues.parametro_cod == dato.cod_sucursal);
						}
						if(dato.desc_agencia) {
							parAgencia = parAgencias.find(param => param.dataValues.parametro_cod == dato.cod_agencia || dato.desc_agencia.includes(param.dataValues.parametro_descripcion));
						}
						if (!parAgencia) {
							dato.desc_agencia = dato.desc_agencia.removeAccents().toUpperCase();
							parAgencia = parAgencias.find(param => param.dataValues.parametro_descripcion.removeAccents().toUpperCase().includes(dato.desc_agencia));
						}
						if (parAgencia && !parSucursal) {
							parSucursal = parSucursales.find(param => param.id == parAgencia.dataValues.id_padre);
						}
						if (dato.tipo_documento_ID) {
							tipoDoc = dato.tipo_documento_ID.toUpperCase();
						}
						if (dato.parentesco.includes('convivi') || dato.parentesco.includes('convivi'.toUpperCase())) {
							parentescoId = parPareentescoConviviente.id;
						} else if (dato.parentesco.includes('conyugue') || dato.parentesco.includes('conyugue'.toUpperCase())){
							parentescoId = parPareentescoConyuge.id;
						} else if (dato.parentesco.includes('hij') || dato.parentesco.includes('hij'.toUpperCase())){
							parentescoId = parPareentescoHijo.id;
						} else if (dato.parentesco.includes('herman') || dato.parentesco.includes('herman'.toUpperCase())){
							parentescoId = parPareentescoHermano.id;
						} else if (dato.parentesco.includes('madre') || dato.parentesco.includes('madre'.toUpperCase())){
							parentescoId = parPareentescoMadre.id;
						} else if (dato.parentesco.includes('padre') || dato.parentesco.includes('padre'.toUpperCase())){
							parentescoId = parPareentescoPadre.id;
						} else {
							parentescoId = parPareentescoOtroParentesco.id;
							parentescoDesc = dato.parentesco;
						}
						nroCuotas = modalidadPago == parModalidadPagoMensual.id ? '12' : modalidadPago == parModalidadPagoAnual ? '1' : '';

						if(parAgencia && parSucursal && adicionadoPor) {

							let objetosPoliza = await modelos.objeto.findAll({where: {id_poliza: polizaId}});
							if (objetosPoliza.length) {
								let objetoXAtributos = await modelos.objeto_x_atributo.findAll(
									{
										where:{id_objeto: objetosPoliza[0].dataValues.id},
										include: { model: modelos.atributo }
									});
								let documentos = await modelos.documento.findAll({where: {id_poliza: polizaId}});
								let newPersona = {
									persona_doc_id: docId,
									persona_doc_id_ext: docIdExt,
									persona_primer_apellido: dato.apellido_paterno,
									persona_segundo_apellido: dato.apellido_materno,
									persona_primer_nombre: dato.nombres,
									persona_direccion_domicilio: dato.dir_domicilio,
									persona_fecha_nacimiento: dato.fecha_nac,
									persona_celular: dato.tel_movil,
									persona_telefono_domicilio: dato.tel_domicilio,
									par_tipo_documento_id: tipoDocumento,
									par_sexo_id: parSexoId,
									createdAt: fechaIngreso,
									updatedAt: createdAt,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor
								};
								let oldPersona = await modelos.persona.findOne({where:{
										persona_doc_id: docId,
										persona_doc_id_ext: docIdExt,
										persona_primer_apellido: dato.apellido_paterno,
										persona_segundo_apellido: dato.apellido_materno,
										persona_primer_nombre: dato.nombres,
									}});
								let persona;
								if (!oldPersona) {
									persona = await modelos.persona.create(newPersona);
									persona = persona.dataValues;
								} else {
									persona = await modelos.persona.update(newPersona,{where:{id:oldPersona.id}});
									persona = await modelos.persona.findOne({where:{id:oldPersona.id}});
									persona = persona.dataValues;
								}

								let newEntidad = {
									id_persona: persona.id,
									tipo_entidad: parPersonaNatural.id,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt
								};
								let oldEntidad = await modelos.entidad.findOne({where:{
										id_persona: persona.id,
										tipo_entidad: parPersonaNatural.id,
									}});

								let entidad;
								if (!oldEntidad) {
									entidad = await modelos.entidad.create(newEntidad);
									entidad = entidad.dataValues;
								} else {
									entidad = await modelos.entidad.update(newEntidad,{where:{id:oldEntidad.id}});
									entidad = await modelos.entidad.findOne({where:{id:oldEntidad.id}});
									entidad = entidad.dataValues;
								}

								let newInstanciaPoliza = {
									id_poliza: polizaId,
									id_estado: parEstado,
									fecha_registro: fechaIngreso,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt,
								};

								let instanciaPoliza;

								instanciaPoliza = await modelos.instancia_poliza.create(newInstanciaPoliza);
								instanciaPoliza = instanciaPoliza.dataValues;
								let updatedIntanciaPoliza = await modelos.instancia_poliza.update({id_instancia_renovada:instanciaPoliza.id}, {where:{id:instanciaPoliza.id}});

								let newAsegurado = {
									id_entidad: entidad.id,
									id_instancia_poliza: instanciaPoliza.id,
									adicionado_por: adicionadoPor,
									modificado_por: adicionadoPor,
									createdAt:  fechaIngreso,
									updatedAt: createdAt
								};

								let asegurado;

								asegurado = await modelos.asegurado.create(newAsegurado);
								asegurado = asegurado.dataValues;

								let instanciaDocumentos = [];
								for (let k = 0 ; k < documentos.length ; k++) {
									let documento = documentos[k].dataValues;
									let newInstanciaDocumeento = {
										id_instancia_poliza: instanciaPoliza.id,
										id_documento: documento.id,
										id_documento_version: documento.id_documento_version,
										nro_documento: documento.id_tipo_documento == 279 ? dato.nro_solicitud : documento.id_tipo_documento == 280 ? dato.nro_comprobante : documento.id_tipo_documento == 281 ? dato.nro_certificado : dato.nro_certificado,
										fecha_emision: fechaEmision,
										fecha_inicio_vigencia: fechaVigenciaInicio,
										fecha_fin_vigencia: fechaVigenciaFin,
										asegurado_doc_id: persona.persona_doc_id,
										asegurado_nombre1: persona.persona_primer_nombre,
										asegurado_nombre2: persona.persona_segundo_nombre,
										asegurado_apellido1: persona.persona_segundo_nombre,
										asegurado_apellido2: persona.persona_segundo_apellido,
										tipo_persona: entidad.id,
										adicionada_por: adicionadoPor,
										modificada_por: adicionadoPor,
										createdAt: fechaIngreso,
										updatedAt: createdAt
									};
									let oldInstanciaDocumento = await modelos.instancia_documento.findOne({where:{
											id_instancia_poliza: instanciaPoliza.id,
											id_documento: documento.id,
											nro_documento: dato.nro_certificado,
											fecha_emision: dato.fecha_emision_real,
										}});
									let instanciaDocumento;
									if (!oldInstanciaDocumento) {
										instanciaDocumento = await modelos.instancia_documento.create(newInstanciaDocumeento);
										instanciaDocumento = instanciaDocumento.dataValues;
									} else {
										instanciaDocumento =  await modelos.instancia_documento.update(newInstanciaDocumeento,{where:{id:oldInstanciaDocumento.id}});
										instanciaDocumento = await modelos.instancia_documento.findOne({where:{id:oldInstanciaDocumento.id}});
										instanciaDocumento = instanciaDocumento.dataValues;
									}

									instanciaDocumentos.push(instanciaDocumento);
								}

								let atributosInstanciaPoliza = [];
								for (let j = 0 ; j < objetoXAtributos.length ; j++) {
									let objetoXAtributo = objetoXAtributos[j].dataValues;
									let valor, tipoError;
									switch (objetoXAtributo.atributo.id) {
										case '3': valor = dato.estado_civil; tipoError = 'El titular no tiene registrado su estado civil'; break;
										case '4': valor = dato.correo ; tipoError = 'El titular no tiene registrado su e-mail'; break;
										case '5': valor = dato.cta; tipoError = 'El titular no tiene registrado un numero de cuenta'; break;
										case '10': valor = dato.codigo_de_agenda; tipoError = 'El titular no tiene registrado un codigo de agenda'; break;
										case '12': valor = parCondicionSi.id; tipoError = 'El titular no tiene registrado si el debito sera automatico'; break;
										case '14': valor = dato.cod_caedec; tipoError = 'El titular no tiene registrado un numero CAEDEC'; break;
										case '16': valor = dato.localidad; tipoError = 'El titular no tiene registrado una localidad'; break;
										case '17': valor = dato.departamento; tipoError = 'El titular no tiene registrado un departamento'; break;
										case '18': valor = dato.cod_sucursal_2; tipoError = 'El titular no tiene registrado un codigo de sucursal'; break;
										case '19': valor = tipoDoc; tipoError = 'El titular no tiene registrado el tipo de documento utilizado'; break;
										case '20': valor = dato.manejo ; tipoError = 'El titular no tiene registrado un codigo de manejo'; break;
										case '21': valor = dato.moneda; tipoError = 'El titular no tiene registrado un tipo de moneda'; break;
										case '22': valor = dato.desc_caedec; tipoError = 'El titular no tiene registrado una descripción en caedec'; break;
										case '28': valor = modalidadPago; tipoError = 'El titular no tiene registrado una modalidad de pago'; break;
										case '29': valor = dato.zona; tipoError = 'El titular no tiene registrado una zona'; break;
										case '30': valor = dato.nro_direccion; tipoError = 'El titular no tiene registrado un Nro de direccion'; break;
										case '37': valor = dato.ocupacion; tipoError = 'El titular no tiene registrado una ocupación'; break;
										case '60': valor = parSucursal.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una sucursal'; break;
										case '59': valor = parAgencia.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una agencia'; break;
										case '61': valor = dato.prima; tipoError = 'El titular no tiene registrado el monto de la cuota'; break;
										case '63': valor = dato.ciudad_nacimiento; tipoError = 'El titular no tiene registrada una ciudad de nacimiento'; break;
										case '64': valor = dato.nro_transaccion; tipoError = 'La transaccion no tiene registrado un nro de transacción'; break;
										case '65': valor = dato.fecha_transaccion; tipoError = 'La transaccion no tiene registrado una fecha de transacción'; break;
										case '66': valor = dato.importe_transaccion; tipoError = 'La transaccion no tiene registrado un importe'; break;
										case '67': valor = dato.moneda; tipoError = 'La transaccion no tiene registrado un tipo de moneda'; break;
										case '69': valor = dato.detalle_transaccion; tipoError = 'La transaccion no tiene registrado un detalle de transacción'; break;
										case '70': valor = cargo; tipoError = 'El usuario del banco no tiene registrado un cargo'; break;
										case '71': valor = ''; tipoError = 'El titular no tiene un cargo publico y/o politico jerarquico'; break;
										case '72': valor = ''; tipoError = 'El titular no tiene registrado un cargo/entidad pep'; break;
										case '73': valor = ''; tipoError = 'El titular no tiene registrado un periodo del cargo publico pep'; break;
										case '74': valor = ''; tipoError = 'El titular no tiene una  direccion laboral'; break;
										case '75': valor = ''; tipoError = 'El titular no tiene un tipo de cuenta'; break;
										case '76': valor = nroCuotas; tipoError = 'El titular no tiene registrado un Nro. de cuotas'; break;
										case '77': valor = ''; tipoError = 'El titular no tiene registrado la descripción de su ocupación'; break;
									}

									let newAtributoInstanciaPoliza = {
										id_instancia_poliza: instanciaPoliza.id,
										id_objeto_x_atributo: objetoXAtributo.id,
										valor: valor,
										tipo_error: tipoError,
										adicionado_por: adicionadoPor,
										modificado_por: adicionadoPor,
										createdAt: fechaIngreso,
										updatedAt: createdAt
									};
									let oldAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{
											id_instancia_poliza: instanciaPoliza.id,
											id_objeto_x_atributo: objetoXAtributo.id,
											valor: valor,
											tipo_error: tipoError
										}});
									let atributoInstanciaPoliza;
									if (!oldAtributoInstanciaPoliza) {
										atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.create(newAtributoInstanciaPoliza);
										atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
									} else {
										atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.update(newAtributoInstanciaPoliza,{where:{id:oldAtributoInstanciaPoliza.id}});
										atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id:oldAtributoInstanciaPoliza.id}});
										atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
									}
									atributosInstanciaPoliza.push(atributoInstanciaPoliza);
								}

								let newPersonaBeneficiario = {
									persona_primer_apellido:dato.apellido_paterno_beneficiario,
									persona_segundo_apellido:dato.apellido_materno_beneficiario,
									persona_primer_nombre:dato.nombres_beneficiario,
									persona_doc_id:docIdBen,
									persona_doc_id_ext:docIdExtBen,
									createdAt:fechaIngreso,
									updatedAt:createdAt,
									adicionada_por:adicionadoPor,
									modificada_por:adicionadoPor
								}

								let personaBeneficiario;

								personaBeneficiario = await modelos.persona.create(newPersonaBeneficiario);
								personaBeneficiario = personaBeneficiario.dataValues;

								let newEntidadBeneficiario = {
									id_persona: personaBeneficiario.id,
									tipo_entidad:12,
									adicionada_por:adicionadoPor,
									modificada_por:adicionadoPor,
									createdAt:fechaIngreso,
									updatedAt:createdAt
								}

								let entidadBeneficiario;

								entidadBeneficiario = await modelos.entidad.create(newEntidadBeneficiario);
								entidadBeneficiario = entidadBeneficiario.dataValues;

								let newBeneficiario = {
									id_asegurado: asegurado.id,
									id_beneficiario: entidadBeneficiario.id,
									id_parentesco: parentescoId,
									porcentaje: dato.porcentaje,
									adicionado_por: adicionadoPor,
									modificado_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt,
									descripcion_otro: parentescoDesc,
									estado: parBeneficiarioActivo.id,
									tipo: parBeneficiarioOriginal.id,
									fecha_declaracion: fechaIngreso
								};
								let oldBeneficiario = await modelos.beneficiario.findOne({where:{
										id_asegurado: asegurado.id,
										id_beneficiario: entidad.id,
										id_parentesco: parentescoId,
										porcentaje: dato.porcentaje,
									}});

								let beneficiario;
								if (!oldBeneficiario) {
									beneficiario = await modelos.beneficiario.create(newBeneficiario);
									beneficiario = beneficiario.dataValues;
								} else {
									beneficiario = await modelos.beneficiario.update(newBeneficiario,{where:{id:oldBeneficiario.id}});
									beneficiario = await modelos.beneficiario.findOne({where:{id:oldBeneficiario.id}});
									beneficiario = beneficiario.dataValues;
								}
								let tmpEcoaguinaldoComplementoBeneficiarioV1 = await modelos.mig_ecoaguinaldo.update({
									id_instancia_poliza:instanciaPoliza.id.toString(),
									id_persona_beneficiario:personaBeneficiario.id,
									FEC_INI_VIG_REAL:fechaVigenciaInicio,
									FEC_FIN_VIG_REAL:fechaVigenciaFin
								}, {where:{id:dato.id}});
								response.data.push({
									persona: persona,
									entidad: entidad,
									asegurado: asegurado,
									instanciaPoliza: instanciaPoliza,
									atributosInstanciaPoliza: atributosInstanciaPoliza,
									beneficiario: beneficiario,
									instanciaDocumentos: instanciaDocumentos
								});
							}
						} else {
							response.errors.push({
								params: {
									sucursal:parSucursal ? parSucursal.dataValues : '',
									agencia:parAgencia ? parAgencia.dataValues : ''
								},
								dato: dato.dataValues
							});
						}
					}
				}
			}
			return response;
		} catch (e) {
			console.log(e)
		}
	}

	static async migraTarjetas(req, res) {
		try {
			let response = { status: 'OK', message: '', data: [], errors:[] };
			let parametros = await modelos.parametro.findAll({where: {diccionario_id: [1, 4, 7, 11, 18, 20, 21, 22, 25, 29, 31, 38, 40, 68]}});
			let hoy = new Date();
			let createdAt = hoy;
			let polizas = await modelos.poliza.findAll();
			let datos = await modelos.mig_tarjetas.findAll({where:{id_instancia_poliza:null}});

			// parametros
			let parTipoDocumentos = parametros.filter(param => param.dataValues.diccionario_id == 1);
			let parMasculino = parametros.find(param => param.dataValues.id == 6);
			let parFemenino = parametros.find(param => param.dataValues.id == 7);
			let parPersonaNatural = parametros.find(param => param.dataValues.id == 12);
			let parPersonaJuridica = parametros.find(param => param.dataValues.id == 13);
			let polizaEcoProteccion = polizas.find(param => param.dataValues.id == 7);
			let polizaEcoVida = polizas.find(param => param.dataValues.id == 6);
			let polizaTarjetas = polizas.find(param => param.dataValues.id == 3);
			let parEstadoEmitido = parametros.find(param => param.dataValues.id == 59);
			let parEstadoIniciado =  parametros.find(param => param.dataValues.id == 24);
			let parEstadoSolicitado = parametros.find(param => param.dataValues.id == 81);
			let parModalidadPagoMensual = parametros.find(param => param.dataValues.id == 90);
			let parModalidadPagoAnual = parametros.find(param => param.dataValues.id == 93);
			let parSucursales = parametros.filter(param => param.dataValues.diccionario_id == 38);
			let parAgencias = parametros.filter(param => param.dataValues.diccionario_id == 40);
			let parCondiciones = parametros.filter(param => param.dataValues.diccionario_id == 21);
			let parCondicionSi = parametros.find(param => param.dataValues.id == 57);
			let parCondicionNo = parametros.find(param => param.dataValues.id == 58);
			let parMonedaBs = parametros.find(param => param.dataValues.id == 68);
			let parPareentescoConviviente = parametros.find(param => param.dataValues.id == 48);
			let parPareentescoConyuge = parametros.find(param => param.dataValues.id == 49);
			let parPareentescoHijo = parametros.find(param => param.dataValues.id == 50);
			let parPareentescoHermano = parametros.find(param => param.dataValues.id == 52);
			let parPareentescoMadre = parametros.find(param => param.dataValues.id == 53);
			let parPareentescoPadre = parametros.find(param => param.dataValues.id == 55);
			let parPareentescoOtroParentesco = parametros.find(param => param.dataValues.id == 56);
			let parBeneficiarioActivo = parametros.find(param => param.dataValues.id == 83);
			let parBeneficiarioOriginal = parametros.find(param => param.dataValues.id == 86);
			let parExtenciones = parametros.filter(param => param.dataValues.diccionario_id == 18);
			let parEstadosTarjeta = parametros.filter(param => param.dataValues.diccionario_id == 68);

			let parEstadoTarjetaHabilitada = parEstadosTarjeta.find(param => param.id == 341);
			let parEstadoTarjetaAceptada = parEstadosTarjeta.find(param => param.id == 340);

			for (let i = 0 ; i < datos.length ; i++) {
				let dato = datos[i].dataValues;
				if (dato.ci && dato.ci != 'NULL') {
					let [docId,docIdExtTxt] = [dato.docId,dato.docIdExt];
					// let [docIdBen,docIdExtTxtBen] = dato.ci_beneficiario.split(' ');
					let docIdExt, parDocIdExt, docIdExtBen, parDocIdExtBen;
					if (docIdExtTxt) {
						switch (docIdExtTxt) {
							case 'PT':
								parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PA':
								parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'PA');
								break;
							case 'PE':
								parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							case 'SE':
								parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'SE');
								break;
							case '':
								parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'SE');
								break;
							case null:
								parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'SE');
								break;
							default:
								parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == docIdExtTxt);
								break;
						}
					} else if(docId) {
						parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'SE');
					}

					docIdExt = parDocIdExt && parDocIdExt.dataValues ? parDocIdExt.dataValues.parametro_cod : null;
					docIdExtBen = parDocIdExtBen && parDocIdExtBen.dataValues ? parDocIdExtBen.dataValues.parametro_cod : null;

                    let soapCustomerData = await soap.getCustomer(docId,docIdExt);
                    // let soapAccountData = soapCustomerData ? await soap.getAccount(soapCustomerData.cod_agenda) : null;
                    let aSoapObtenerTarjetasPorNroCIyExtensionData = await soap.obtenerTarjetasPorNroCIyExtension(docId,docIdExt);

                    let fechaEmision = moment(dato.fecha_emision,"DD/MM/YYYY").add('hours',4).toDate();
                    let fechaIngreso = moment(dato.fecha_emision,"DD/MM/YYYY").add('hours',4).toDate();
                    let fechaNacimiento = soapCustomerData ? moment(`${soapCustomerData.anio_fechanac}-${soapCustomerData.mes_fechanac}-${soapCustomerData.dia_fechanac}`).add('hours',4).toDate() : null;
                    let fechaVigenciaInicio =  moment(dato.fecha_emision,"DD/MM/YYYY").add('hours',4).toDate();
                    let fechaVigenciaFin = moment(dato.fecha_emision,"DD/MM/YYYY").add('months',12).toDate();

					if (docIdExt) {
						let oldInstanciaDocumentoCertificado = await modelos.instancia_documento.findOne({
							where: {nro_documento: {$like:dato.nro_certificado}, asegurado_doc_id: {$like:docId.trim()}},
							include: {
								model:modelos.documento,
								where: {
									id_tipo_documento:281
								}
							}
						});
						if (oldInstanciaDocumentoCertificado) {
							await modelos.mig_tarjetas.update({
								id_instancia_poliza:oldInstanciaDocumentoCertificado.id_instancia_poliza+'',
							}, {where:{id:dato.id}});
						} else {
							let usuarioColibri;
							if (!dato.cod_usuario_colibri) {
								let nom1,nom2,nom3,nom4,nom5;
								if (dato.nombre_usuario) {
									[nom1,nom2,nom3,nom4,nom5] = dato.nombre_usuario.split(' ');
								}
								if (nom1 && nom2 && nom3 && nom4 && nom5) {
									usuarioColibri = await modelos.usuario.findOne({where:{$and:[
												{usuario_nombre_completo:{$like:`%${nom1}%`}},
												{usuario_nombre_completo:{$like:`%${nom2}%`}},
												{usuario_nombre_completo:{$like:`%${nom3}%`}},
												{usuario_nombre_completo:{$like:`%${nom4}%`}},
												{usuario_nombre_completo:{$like:`%${nom5}%`}}
											]}})
								} else if (nom1 && nom2 && nom3 && nom4) {
									usuarioColibri = await modelos.usuario.findOne({where:{$and:[
												{usuario_nombre_completo:{$like:`%${nom1}%`}},
												{usuario_nombre_completo:{$like:`%${nom2}%`}},
												{usuario_nombre_completo:{$like:`%${nom3}%`}},
												{usuario_nombre_completo:{$like:`%${nom4}%`}},
											]}})
								} else if (nom1 && nom2 && nom3) {
									usuarioColibri = await modelos.usuario.findOne({where:{$and:[
												{usuario_nombre_completo:{$like:`%${nom1}%`}},
												{usuario_nombre_completo:{$like:`%${nom2}%`}},
												{usuario_nombre_completo:{$like:`%${nom3}%`}},
											]}})
								} else if (nom1 && nom2 ) {
									usuarioColibri = await modelos.usuario.findOne({where:{$and:[
												{usuario_nombre_completo:{$like:`%${nom1}%`}},
												{usuario_nombre_completo:{$like:`%${nom2}%`}},
											]}})
								} else if (nom1) {
									usuarioColibri = await modelos.usuario.findOne({where:{$and:[
												{usuario_login:{$like:`%${nom1}%`}},
											]}});
								}
								if (usuarioColibri) {
									await modelos.mig_tarjetas.update({cod_usuario_colibri:usuarioColibri.id},{where:{id:dato.id}});
									dato.cod_usuario_colibri = usuarioColibri.id;
								}
							}
							let adicionadoPor = dato.cod_usuario_colibri ? dato.cod_usuario_colibri : 583;
							let parTipoDocumento = dato.tipo_documento_ID ? parTipoDocumentos.find(param => param.dataValues.parametro_cod.toUpperCase() == dato.tipo_documento_ID.toUpperCase()) : null;
							let tipoDocumento = dato.tipo_documento_ID ? parTipoDocumento ? parTipoDocumento.id : 2 : 1;

							let parSexoId, polizaId, parEstado, modalidadPago, parSucursal, parAgencia, cargo, nroCuotas, tipoDoc, soapTarjetaCredito, parentescoId, parentescoDesc;

							if (dato.genero) {
								if(dato.genero == 'varon' || dato.genero.toLowerCase().includes('masculino')) {
									parSexoId = parMasculino.id;
								} else if(dato.genero == 'mujer' || dato.genero.toLowerCase().includes('femenino')) {
									parSexoId = parFemenino.id;
								}
							} else if (soapCustomerData) {
								parSexoId = soapCustomerData.sexo == 'F' ? parFemenino.id : soapCustomerData.sexo == 'M' ? parMasculino.id : '';
							}
							for (let j = 0; j < aSoapObtenerTarjetasPorNroCIyExtensionData.length; j++) {
								let soapObtenerTarjetasPorNroCIyExtensionData = aSoapObtenerTarjetasPorNroCIyExtensionData[j];
								if (soapObtenerTarjetasPorNroCIyExtensionData && [parEstadoTarjetaAceptada.parametro_descripcion.toLowerCase(),parEstadoTarjetaHabilitada.parametro_descripcion.toLowerCase()].includes(soapObtenerTarjetasPorNroCIyExtensionData.estado.trim().toLowerCase())) {
									soapTarjetaCredito = soapObtenerTarjetasPorNroCIyExtensionData
								}
							}
							polizaId = polizaTarjetas.id;
							// if (dato.producto) {
							// if(dato.producto.toLowerCase() == 'ecoproteccion') {
							// 	polizaId = polizaEcoProteccion.id;
							// } else if (dato.producto.toLowerCase() == 'ecovida') {
							// 	polizaId = polizaEcoVida.id;
							// } else if (dato.producto.toLowerCase() == 'credito') {
							// 	polizaId = polizaTarjetas.id;
							// }
							// } else {
							// 	polizaId = polizaTarjetas.id;
							// }
							if (dato.emitido) {
								if(dato.emitido == 'SI') {
									parEstado = parEstadoEmitido.id;
								}
							} else {
								parEstado = parEstadoEmitido.id;
							}
							if (dato.modalidad_pago) {
								if(dato.modalidad_pago == 'MENSUAL') {
									modalidadPago = parModalidadPagoMensual.id
								} else if (dato.modalidad_pago == 'ANUAL') {
									modalidadPago = parModalidadPagoAnual.id
								}
							} else {
								modalidadPago = parModalidadPagoMensual.id
							}
							// cargo = 'ENCARGADO DE PLATAFORMA';
							let [codSucursal,sucursal] = dato.sucursal.split(' ');
							let [codAgencia,agencia] = dato.agencia.split('AGENCIA');
							if (dato.sucursal) {
								parSucursal = parSucursales.find(param => param.dataValues.parametro_cod == codSucursal.trim());
							}
							if(dato.agencia) {
								parAgencia = parAgencias.find(param => param.dataValues.parametro_cod == codAgencia.trim());
							}
							if (!parAgencia) {
								dato.agencia = dato.agencia.removeAccents().toUpperCase();
								parAgencia = parAgencias.find(param => param.dataValues.parametro_descripcion.removeAccents().toUpperCase().includes(dato.agencia));
							}
							if (parAgencia && !parSucursal) {
								parSucursal = parSucursales.find(param => param.id == parAgencia.dataValues.id_padre);
							}
							if (dato.tipo_documento_ID) {
								tipoDoc = dato.tipo_documento_ID.toUpperCase();
							} else {
								tipoDoc = tipoDocumento
							}
							if (dato.parentesco) {
								if (dato.parentesco.includes('convivi') || dato.parentesco.includes('convivi'.toUpperCase())) {
									parentescoId = parPareentescoConviviente.id;
								} else if (dato.parentesco.includes('conyugue') || dato.parentesco.includes('conyugue'.toUpperCase())){
									parentescoId = parPareentescoConyuge.id;
								} else if (dato.parentesco.includes('hij') || dato.parentesco.includes('hij'.toUpperCase())){
									parentescoId = parPareentescoHijo.id;
								} else if (dato.parentesco.includes('herman') || dato.parentesco.includes('herman'.toUpperCase())){
									parentescoId = parPareentescoHermano.id;
								} else if (dato.parentesco.includes('madre') || dato.parentesco.includes('madre'.toUpperCase())){
									parentescoId = parPareentescoMadre.id;
								} else if (dato.parentesco.includes('padre') || dato.parentesco.includes('padre'.toUpperCase())){
									parentescoId = parPareentescoPadre.id;
								} else {
									parentescoId = parPareentescoOtroParentesco.id;
									parentescoDesc = dato.parentesco;
								}
							}
							nroCuotas = modalidadPago == parModalidadPagoMensual.id ? '12' : modalidadPago == parModalidadPagoAnual ? '1' : '';
							if(parAgencia && parSucursal && adicionadoPor && polizaId) {
								let objetosPoliza = await modelos.objeto.findAll({where: {id_poliza: polizaId}});
								if (objetosPoliza.length) {
									let objetoXAtributos = await modelos.objeto_x_atributo.findAll(
										{
											where:{id_objeto: objetosPoliza[0].dataValues.id},
											include: { model: modelos.atributo }
										});
									let documentos = await modelos.documento.findAll({where: {id_poliza: polizaId}});
									let newPersona = {
										persona_doc_id: docId,
										persona_doc_id_ext: docIdExt,
										persona_primer_apellido: dato.apellido_paterno,
										persona_segundo_apellido: dato.apellido_materno,
										persona_primer_nombre: dato.nombres,
										persona_direccion_domicilio: dato.dir_domicilio ? dato.dir_domicilio : '',
										persona_fecha_nacimiento: fechaNacimiento ? fechaNacimiento : null,
										persona_celular: dato.tel_movil ? dato.tel_movil : '',
										persona_telefono_domicilio: dato.tel_domicilio ? dato.tel_domicilio : '',
										par_tipo_documento_id: tipoDocumento,
										par_sexo_id: parSexoId ? parSexoId : null,
										createdAt: fechaIngreso,
										updatedAt: createdAt,
										adicionada_por: adicionadoPor,
										modificada_por: adicionadoPor
									};
									let oldPersona = await modelos.persona.findOne({where:{
											persona_doc_id: docId,
											// persona_doc_id_ext: docIdExt,
											//persona_primer_apellido: dato.apellido_paterno,
											//persona_segundo_apellido: dato.apellido_materno,
											//persona_primer_nombre: dato.nombres,
										}});
									let persona;
									if (!oldPersona) {
										persona = await modelos.persona.create(newPersona);
										persona = persona.dataValues;
									}
									else {
										persona = await modelos.persona.update(newPersona,{where:{id:oldPersona.id}});
										persona = await modelos.persona.findOne({where:{id:oldPersona.id}});
										persona = persona.dataValues;
									}

									let newEntidad = {
										id_persona: persona.id,
										tipo_entidad: parPersonaNatural.id,
										adicionada_por: adicionadoPor,
										modificada_por: adicionadoPor,
										createdAt: fechaIngreso,
										updatedAt: createdAt
									};
									let oldEntidad = await modelos.entidad.findOne({where:{
											id_persona: persona.id,
											tipo_entidad: parPersonaNatural.id,
										}});

									let entidad;
									if (!oldEntidad) {
										entidad = await modelos.entidad.create(newEntidad);
										entidad = entidad.dataValues;
									} else {
										entidad = await modelos.entidad.update(newEntidad,{where:{id:oldEntidad.id}});
										entidad = await modelos.entidad.findOne({where:{id:oldEntidad.id}});
										entidad = entidad.dataValues;
									}

									let newInstanciaPoliza = {
										id_poliza: polizaId,
										id_estado: parEstado,
										fecha_registro: fechaIngreso,
										adicionada_por: adicionadoPor,
										modificada_por: adicionadoPor,
										createdAt: fechaIngreso,
										updatedAt: createdAt,
									};

									let instanciaPoliza;

									instanciaPoliza = await modelos.instancia_poliza.create(newInstanciaPoliza);
									instanciaPoliza = instanciaPoliza.dataValues;
									let updatedIntanciaPoliza = await modelos.instancia_poliza.update({id_instancia_renovada:instanciaPoliza.id}, {where:{id:instanciaPoliza.id}});

									let newAsegurado = {
										id_entidad: entidad.id,
										id_instancia_poliza: instanciaPoliza.id,
										adicionado_por: adicionadoPor,
										modificado_por: adicionadoPor,
										createdAt:  fechaIngreso,
										updatedAt: createdAt
									};

									let asegurado;

									asegurado = await modelos.asegurado.create(newAsegurado);
									asegurado = asegurado.dataValues;

									let atributosInstanciaPoliza = [];
									for (let j = 0 ; j < objetoXAtributos.length ; j++) {
										let objetoXAtributo = objetoXAtributos[j].dataValues;
										let valor, tipoError;
										switch (objetoXAtributo.atributo.id) {
											case '23': valor = dato.nro_tarjeta ? dato.nro_tarjeta : soapTarjetaCredito ? soapTarjetaCredito.nrotarjeta : ''; tipoError = 'El titular no tiene registrado un Nro de tarjeta'; break;
											case '24': valor = ''; tipoError = 'El titular no tiene registrado un nombre de tarjeta'; break;
											case '25': valor = ''; tipoError = 'El titular no tiene registrado si su tarjeta es valida'; break;
											case '32': valor = ''; tipoError = 'El titular no tiene registrado una razon social'; break;
											case '33': valor = ''; tipoError = 'El titular no tiene registrado un Nit o Carnet'; break;
											case '36': valor = ''; tipoError = 'El titular no tiene registrado su tipo de tarjeta.'; break;
											case '3': valor = dato.estado_civil ? dato.estado_civil : soapCustomerData ? soapCustomerData.est_civil : ''; tipoError = 'El titular no tiene registrado su estado civil'; break;
											case '4': valor = ''; tipoError = 'El titular no tiene registrado su e-mail'; break;
											case '5': valor = ''; tipoError = 'El titular no tiene registrado un numero de cuenta'; break;
											case '10': valor = dato.codigo_agenda ? dato.codigo_agenda : soapCustomerData ? soapCustomerData.cod_agenda : ''; tipoError = 'El titular no tiene registrado un codigo de agenda'; break;
											case '12': valor = parCondicionNo.id; tipoError = 'El titular no tiene registrado si el debito sera automatico'; break;
											case '14': valor = ''; tipoError = 'El titular no tiene registrado un numero CAEDEC'; break;
											case '16': valor = ''; tipoError = 'El titular no tiene registrado una localidad'; break;
											case '17': valor = ''; tipoError = 'El titular no tiene registrado un departamento'; break;
											case '18': valor = ''; tipoError = 'El titular no tiene registrado un codigo de sucursal'; break;
											case '19': valor = tipoDoc; tipoError = 'El titular no tiene registrado el tipo de documento utilizado'; break;
											case '20': valor = ''; tipoError = 'El titular no tiene registrado un codigo de manejo'; break;
											case '21': valor = ''; tipoError = 'El titular no tiene registrado un tipo de moneda'; break;
											case '22': valor = ''; tipoError = 'El titular no tiene registrado una descripción en caedec'; break;
											case '28': valor = modalidadPago; tipoError = 'El titular no tiene registrado una modalidad de pago'; break;
											case '29': valor = ''; tipoError = 'El titular no tiene registrado una zona'; break;
											case '30': valor = ''; tipoError = 'El titular no tiene registrado un Nro de direccion'; break;
											case '37': valor = ''; tipoError = 'El titular no tiene registrado una ocupación'; break;
											case '60': valor = parSucursal.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una sucursal'; break;
											case '59': valor = parAgencia.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una agencia'; break;
											// case '61': valor = dato.prima; tipoError = 'El titular no tiene registrado el monto de la cuota'; break;
											case '63': valor = ''; tipoError = 'El titular no tiene registrada una ciudad de nacimiento'; break;
											// case '64': valor = ''; tipoError = 'La transaccion no tiene registrado un nro de transacción'; break;
											// case '65': valor = ''; tipoError = 'La transaccion no tiene registrado una fecha de transacción'; break;
											// case '66': valor = dato.prima; tipoError = 'La transaccion no tiene registrado un importe'; break;
											// case '67': valor = ''; tipoError = 'La transaccion no tiene registrado un tipo de moneda'; break;
											// case '69': valor = ''; tipoError = 'La transaccion no tiene registrado un detalle de transacción'; break;
											case '70': valor = cargo ? cargo : ''; tipoError = 'El usuario del banco no tiene registrado un cargo'; break;
											// case '71': valor = ''; tipoError = 'El titular no tiene un cargo publico y/o politico jerarquico'; break;
											// case '72': valor = ''; tipoError = 'El titular no tiene registrado un cargo/entidad pep'; break;
											// case '73': valor = ''; tipoError = 'El titular no tiene registrado un periodo del cargo publico pep'; break;
											case '74': valor = ''; tipoError = 'El titular no tiene una  direccion laboral'; break;
											case '75': valor = ''; tipoError = 'El titular no tiene un tipo de cuenta'; break;
											case '76': valor = nroCuotas; tipoError = 'El titular no tiene registrado un Nro. de cuotas'; break;
											case '77': valor = ''; tipoError = 'El titular no tiene registrado la descripción de su ocupación'; break;
											case '93': valor = soapTarjetaCredito ? soapTarjetaCredito.id : ''; tipoError = 'El titular no tiene registrado el id de tarjeta'; break;
										}

										let newAtributoInstanciaPoliza = {
											id_instancia_poliza: instanciaPoliza.id,
											id_objeto_x_atributo: objetoXAtributo.id,
											valor: valor,
											tipo_error: tipoError,
											adicionado_por: adicionadoPor,
											modificado_por: adicionadoPor,
											createdAt: fechaIngreso,
											updatedAt: createdAt
										};
										let oldAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{
												id_instancia_poliza: instanciaPoliza.id,
												id_objeto_x_atributo: objetoXAtributo.id
											}});
										let atributoInstanciaPoliza;
										if (!oldAtributoInstanciaPoliza) {
											atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.create(newAtributoInstanciaPoliza);
											atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
										} else {
											atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.update(newAtributoInstanciaPoliza,{where:{id:oldAtributoInstanciaPoliza.id}});
											atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id:oldAtributoInstanciaPoliza.id}});
											atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
										}
										atributosInstanciaPoliza.push(atributoInstanciaPoliza);
									}

									let instanciaDocumentos = [];
									for (let k = 0 ; k < documentos.length ; k++) {
										let documento = documentos[k].dataValues;
										let newInstanciaDocumeento = {
											id_instancia_poliza: instanciaPoliza.id,
											id_documento: documento.id,
											id_documento_version: documento.id_documento_version,
											nro_documento: dato.nro_certificado,
											fecha_emision: fechaEmision,
											fecha_inicio_vigencia: fechaVigenciaInicio,
											fecha_fin_vigencia: fechaVigenciaFin,
											asegurado_doc_id: persona.persona_doc_id,
											asegurado_nombre1: persona.persona_primer_nombre,
											asegurado_nombre2: persona.persona_segundo_nombre,
											asegurado_apellido1: persona.persona_segundo_nombre,
											asegurado_apellido2: persona.persona_segundo_apellido,
											tipo_persona: entidad.id,
											adicionada_por: adicionadoPor,
											modificada_por: adicionadoPor,
											createdAt: fechaIngreso,
											updatedAt: createdAt
										};
										let oldInstanciaDocumento = await modelos.instancia_documento.findOne({where:{
												id_instancia_poliza: instanciaPoliza.id,
												id_documento: documento.id,
												nro_documento: dato.nro_certificado,
												//fecha_emision: fechaEmision,
											}});
										let instanciaDocumento;
										if (!oldInstanciaDocumento) {
											instanciaDocumento = await modelos.instancia_documento.create(newInstanciaDocumeento);
											instanciaDocumento = instanciaDocumento.dataValues;
										} else {
											instanciaDocumento =  await modelos.instancia_documento.update(newInstanciaDocumeento,{where:{id:oldInstanciaDocumento.id}});
											instanciaDocumento = await modelos.instancia_documento.findOne({where:{id:oldInstanciaDocumento.id}});
											instanciaDocumento = instanciaDocumento.dataValues;
										}

										instanciaDocumentos.push(instanciaDocumento);
									}
									if (dato.apellido_paterno_beneficiario == 'no se cuenta') {
										dato.apellido_paterno_beneficiario = '';
									}
									if (dato.apellido_materno_beneficiario == 'no se cuenta') {
										dato.apellido_materno_beneficiario = '';
									}
									if (dato.nombres_beneficiario == 'no se cuenta' ) {
										dato.nombres_beneficiario = '';
									}
									let beneficiario;
									if (dato.apellido_paterno_beneficiario || dato.apellido_materno_beneficiario || dato.nombres_beneficiario) {
										let newPersonaBeneficiario = {
											persona_primer_apellido:dato.apellido_paterno_beneficiario,
											persona_segundo_apellido:dato.apellido_materno_beneficiario,
											persona_primer_nombre:dato.nombres_beneficiario,
											persona_doc_id:docIdBen,
											persona_doc_id_ext:docIdExtBen,
											createdAt:fechaIngreso,
											updatedAt:createdAt,
											adicionada_por:adicionadoPor,
											modificada_por:adicionadoPor
										}

										let personaBeneficiario;

										personaBeneficiario = await modelos.persona.create(newPersonaBeneficiario);
										personaBeneficiario = personaBeneficiario.dataValues;

										let newEntidadBeneficiario = {
											id_persona: personaBeneficiario.id,
											tipo_entidad:12,
											adicionada_por:adicionadoPor,
											modificada_por:adicionadoPor,
											createdAt:fechaIngreso,
											updatedAt:createdAt
										}

										let entidadBeneficiario;

										entidadBeneficiario = await modelos.entidad.create(newEntidadBeneficiario);
										entidadBeneficiario = entidadBeneficiario.dataValues;

										let newBeneficiario = {
											id_asegurado: asegurado.id,
											id_beneficiario: entidadBeneficiario.id,
											id_parentesco: parentescoId,
											porcentaje: dato.porcentaje,
											adicionado_por: adicionadoPor,
											modificado_por: adicionadoPor,
											createdAt: fechaIngreso,
											updatedAt: createdAt,
											descripcion_otro: parentescoDesc,
											estado: parBeneficiarioActivo.id,
											tipo: parBeneficiarioOriginal.id,
											fecha_declaracion: fechaIngreso
										};
										let oldBeneficiario = await modelos.beneficiario.findOne({where:{
												id_asegurado: asegurado.id,
												id_beneficiario: entidad.id,
												id_parentesco: parentescoId,
												porcentaje: dato.porcentaje,
											}});

										if (!oldBeneficiario) {
											beneficiario = await modelos.beneficiario.create(newBeneficiario);
											beneficiario = beneficiario.dataValues;
										} else {
											beneficiario = await modelos.beneficiario.update(newBeneficiario,{where:{id:oldBeneficiario.id}});
											beneficiario = await modelos.beneficiario.findOne({where:{id:oldBeneficiario.id}});
											beneficiario = beneficiario.dataValues;
										}
									}

									await modelos.mig_tarjetas.update({
										id_instancia_poliza:instanciaPoliza.id.toString(),
										// id_persona_beneficiario:personaBeneficiario.id,
										FEC_INI_VIG_REAL:fechaVigenciaInicio,
										FEC_FIN_VIG_REAL:fechaVigenciaFin
									}, {where:{id:dato.id}});
									response.data.push({
										persona: persona,
										entidad: entidad,
										asegurado: asegurado,
										instanciaPoliza: instanciaPoliza,
										atributosInstanciaPoliza: atributosInstanciaPoliza,
										beneficiario: beneficiario ? beneficiario : null,
										instanciaDocumentos: instanciaDocumentos
									});
								}
							} else {
								response.errors.push({
									params: {
										sucursal:parSucursal ? parSucursal.dataValues : '',
										agencia:parAgencia ? parAgencia.dataValues : ''
									},
									dato: dato.dataValues
								});
							}
						}
					}
				}
			}
			return response;
		} catch (e) {
			console.log(e)
		}
	}

	static async migracionGeneral(tablaMigracion,idPoliza,columns,consumeSoapServices,limit) {
		try {
			let dataToReturn = [], errors = [];
			let parametros = await modelos.parametro.findAll({where: {diccionario_id: [1, 4, 7, 11, 18, 20, 21, 22, 25, 29, 31, 38, 40, 55, 68]}});
			let hoy = new Date();
			let createdAt = hoy;
			let polizas = await modelos.poliza.findAll();
			let datos;
			if (limit) {
				datos = await modelos[tablaMigracion].findAll({where:{id_instancia_poliza:null,fin:null},limit});
			} else {
				datos = await modelos[tablaMigracion].findAll({where:{id_instancia_poliza:null,fin:null}});
			}
			// parametros
			let parTipoDocumentos = parametros.filter(param => param.dataValues.diccionario_id == 1);
			let parEstadosInstancias = parametros.filter(param => param.dataValues.diccionario_id == 11);
			let parMonedas = parametros.filter(param => param.dataValues.diccionario_id == 22);
			let parMasculino = parametros.find(param => param.dataValues.id == 6);
			let parFemenino = parametros.find(param => param.dataValues.id == 7);
			let parPersonaNatural = parametros.find(param => param.dataValues.id == 12);
			let parPersonaJuridica = parametros.find(param => param.dataValues.id == 13);
			let polizaEcoProteccion = polizas.find(param => param.dataValues.id == 7);
			let polizaEcoVida = polizas.find(param => param.dataValues.id == 6);
			let polizaEcoAccidente = polizas.find(param => param.dataValues.id == 10);
			let parEstadoEmitido = parametros.find(param => param.dataValues.id == 59);
			let parEstadoSinVigencia = parametros.find(param => param.dataValues.id == 282);
			let parEstadoIniciado =  parametros.find(param => param.dataValues.id == 24);
			let parEstadoSolicitado = parametros.find(param => param.dataValues.id == 81);
			let parModalidadPagoMensual = parametros.find(param => param.dataValues.id == 90);
			let parModalidadPagoAnual = parametros.find(param => param.dataValues.id == 93);
			let parSucursales = parametros.filter(param => param.dataValues.diccionario_id == 38);
			let parAgencias = parametros.filter(param => param.dataValues.diccionario_id == 40);
			let parOperacionProductosAsociados = parametros.filter(param => param.dataValues.diccionario_id == 55);
			let parCondiciones = parametros.filter(param => param.dataValues.diccionario_id == 21);
			let parCondicionSi = parametros.find(param => param.dataValues.id == 57);
			let parCondicionNo = parametros.find(param => param.dataValues.id == 58);
			let parMonedaBs = parametros.find(param => param.dataValues.id == 68);
			let parPareentescoConviviente = parametros.find(param => param.dataValues.id == 48);
			let parPareentescoConyuge = parametros.find(param => param.dataValues.id == 49);
			let parPareentescoHijo = parametros.find(param => param.dataValues.id == 50);
			let parPareentescoHermano = parametros.find(param => param.dataValues.id == 52);
			let parPareentescoMadre = parametros.find(param => param.dataValues.id == 53);
			let parPareentescoPadre = parametros.find(param => param.dataValues.id == 55);
			let parPareentescoOtroParentesco = parametros.find(param => param.dataValues.id == 56);
			let parBeneficiarioActivo = parametros.find(param => param.dataValues.id == 83);
			let parBeneficiarioOriginal = parametros.find(param => param.dataValues.id == 86);
			let parExtenciones = parametros.filter(param => param.dataValues.diccionario_id == 18);
			let parEstadosTarjeta = parametros.filter(param => param.dataValues.diccionario_id == 68);
			let parEstadoTarjetaHabilitada = parEstadosTarjeta.find(param => param.id == 341);
			let parEstadoTarjetaAceptada = parEstadosTarjeta.find(param => param.id == 340);
			for (let indexDato = 0 ; indexDato < datos.length ; indexDato++) {

				let dato = datos[indexDato].dataValues;
				console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ('+indexDato+')', 'Hasta '+datos.length);
				let colNroCertificado = columns.colNroCertificado ? dato[columns.colNroCertificado] : null;
				let colFechaCertificado = columns.colFechaCertificado ? dato[columns.colFechaCertificado] : null;
				let colDocumentoCi = columns.colDocumentoCi ? dato[columns.colDocumentoCi] : null;
				let colDocumentoTipoDescripcion = columns.colDocumentoTipoDescripcion ? dato[columns.colDocumentoTipoDescripcion] : null;
				let colDocumentoTipoAbreviacion = columns.colDocumentoTipoAbreviacion ? dato[columns.colDocumentoTipoAbreviacion] : null;
				let colDocumentoTipoCod = columns.colDocumentoTipoCod ? dato[columns.colDocumentoTipoCod] : null;
				let colDocumentoTipoId = columns.colDocumentoTipoId ? dato[columns.colDocumentoTipoId] : null;
				let colExtensionAbreviacion = columns.colExtensionAbreviacion ? dato[columns.colExtensionAbreviacion] : null;
				let colExtensionDescripcion = columns.colExtensionDescripcion ? dato[columns.colExtensionDescripcion] : null;
				let colExtensionCod = columns.colExtensionCod ? dato[columns.colExtensionCod] : null;
				let colGenero = columns.colGenero ? dato[columns.colGenero] : null;
				let colPersonaPrimerApellido = columns.colPersonaPrimerApellido ? dato[columns.colPersonaPrimerApellido] : null;
				let colPersonaSegundoApellido = columns.colPersonaSegundoApellido ? dato[columns.colPersonaSegundoApellido] : null;
				let colPersonaPrimerNombre = columns.colPersonaPrimerNombre ? dato[columns.colPersonaPrimerNombre] : null;
				let colPersonaEstadoCivil = columns.colPersonaEstadoCivil ? dato[columns.colPersonaEstadoCivil] : null;
				let colPersonaNroTarjeta = columns.colPersonaNroTarjeta ? dato[columns.colPersonaNroTarjeta] : null;
				let colIdTarjeta = columns.colIdTarjeta ? dato[columns.colIdTarjeta] : null;
				let colPrimaMensual = columns.colPrimaMensual ? dato[columns.colPrimaMensual] : null;
				let colTransaccionImporte = columns.colTransaccionImporte ? dato[columns.colTransaccionImporte] : null;
				let colTransaccionTipoMoneda = columns.colTransaccionTipoMoneda ? dato[columns.colTransaccionTipoMoneda] : 1;
				let colTransaccionDetalle = columns.colTransaccionDetalle ? dato[columns.colTransaccionDetalle] : null;
				let colPrimaTotal = columns.colPrimaTotal ? dato[columns.colPrimaTotal] : null;
				let colPersonaCodAgenda = columns.colPersonaCodAgenda ? dato[columns.colPersonaCodAgenda] : null;
				let colPersonaSegundoNombre = columns.colPersonaSegundoNombre ? dato[columns.colPersonaSegundoNombre] : null;
				let colPersonaDireccionDomicilio = columns.colPersonaDireccionDomicilio ? dato[columns.colPersonaDireccionDomicilio] : null;
				let colPersonaCelular = columns.colPersonaCelular ? dato[columns.colPersonaCelular] : null;
				let colPersonaTelefonoDomicilio = columns.colPersonaTelefonoDomicilio ? dato[columns.colPersonaTelefonoDomicilio] : null;
				let colEmitido = columns.colEmitido ? dato[columns.colEmitido] : null;
				let colModalidadPago = columns.colModalidadPago ? dato[columns.colModalidadPago] : null;
				let colOperacionPlazo = columns.colOperacionPlazo ? dato[columns.colOperacionPlazo] : null;
				let colOperacionNroSolicitud = columns.colOperacionNroSolicitud ? dato[columns.colOperacionNroSolicitud] : null;
				let colOperacionEstado = columns.colOperacionEstado ? dato[columns.colOperacionEstado] : null;
				let colFechaNacimiento = columns.colFechaNacimiento ? dato[columns.colFechaNacimiento] : null;
				let colMonedaOperacionDescripcion = columns.colMonedaOperacionDescripcion ? dato[columns.colMonedaOperacionDescripcion] : null;
				let colMonedaOperacionAbreviacion = columns.colMonedaOperacionAbreviacion ? dato[columns.colMonedaOperacionAbreviacion] : null;
				let colFechaInicio = columns.colFechaInicio ? dato[columns.colFechaInicio] : null;
				let colFechaFin = columns.colFechaFin ? dato[columns.colFechaFin] : null;
				let colBeneficiarioDocumentoCi = columns.colBeneficiarioDocumentoCi ? dato[columns.colBeneficiarioDocumentoCi] : null;
				let colBeneficiarioExtensionAbreviacion = columns.colBeneficiarioExtensionAbreviacion ? dato[columns.colBeneficiarioExtensionAbreviacion] : null;
				let colBeneficiarioParentescoDescripcion = columns.colBeneficiarioParentescoDescripcion ? dato[columns.colBeneficiarioParentescoDescripcion] : null;
				let colBeneficiarioPorcentaje = columns.colBeneficiarioPorcentaje ? dato[columns.colBeneficiarioPorcentaje] : null;
				let colBeneficiarioExtensionDescripcion = columns.colBeneficiarioExtensionDescripcion ? dato[columns.colBeneficiarioExtensionDescripcion] : null;
				let colBeneficiarioPrimerApellido = columns.colBeneficiarioPrimerApellido ? dato[columns.colBeneficiarioPrimerApellido] : null;
				let colBeneficiarioSegundoApellido = columns.colBeneficiarioSegundoApellido ? dato[columns.colBeneficiarioSegundoApellido] : null;
				let colBeneficiarioSegundoNombre = columns.colBeneficiarioSegundoNombre ? dato[columns.colBeneficiarioSegundoNombre] : null;
				let colBeneficiarioPrimerNombre = columns.colBeneficiarioPrimerNombre ? dato[columns.colBeneficiarioPrimerNombre] : null;
				let colUsuarioCargoDescripcion = columns.colUsuarioCargoDescripcion ? dato[columns.colUsuarioCargoDescripcion] : null;
				let colUsuarioCargoCodigo = columns.colUsuarioCargoCodigo ? dato[columns.colUsuarioCargoCodigo] : null;
				let colOperacionProductoAsociado = columns.colOperacionProductoAsociado ? dato[columns.colOperacionProductoAsociado] : null;
				let colOperacionTipoSeguro = columns.colOperacionTipoSeguro ? dato[columns.colOperacionTipoSeguro] : null;
				let colSucursalDescripcion = columns.colSucursalDescripcion ? dato[columns.colSucursalDescripcion] : null;
				let colSucursalCodigo = columns.colSucursalCodigo ? dato[columns.colSucursalCodigo] : null;
				let colAgenciaDescripcion = columns.colAgenciaDescripcion ? dato[columns.colAgenciaDescripcion] : null;
				let colAgenciaCodigo = columns.colAgenciaCodigo ? dato[columns.colAgenciaCodigo] : null;
				let colUsername = columns.colUsername ? dato[columns.colUsername] : null;
				let colNombreUsuario = columns.colNombreUsuario ? dato[columns.colNombreUsuario] : null;
				let colId = columns.colId ? dato[columns.colId] : null;
				let colAdicionadoPor = columns.colAdicionadoPor ? dato[columns.colAdicionadoPor] : null;
				let colIdInstanciaPoliza = columns.colidInstanciaPoliza ? dato[columns.colidInstanciaPoliza] : null;

				await modelos[tablaMigracion].update({
					fin:0,
				}, {where:{id:colId}});
				if (colDocumentoCi && (colExtensionCod || colExtensionDescripcion || colExtensionAbreviacion)) {
					if (colDocumentoCi != 'NULL' && (colExtensionCod != 'NULL' || colExtensionDescripcion != 'NULL' || colExtensionAbreviacion != 'NULL' )) {
						let parExtencion, parTipoDocumento;
						if (colExtensionCod && util.isNumber(colExtensionCod) && !colDocumentoCi.hasString(['CI','ci','CE','ce'])) {
							parExtencion = colExtensionCod ? parExtenciones.find(param => param.parametro_cod == colExtensionCod) : null;
						} else {
							[colDocumentoTipoAbreviacion,colDocumentoCi] = !colDocumentoTipoAbreviacion ?
								colDocumentoCi.includes('CI') ? colDocumentoCi.split(' ') :
									colDocumentoCi.includes('ci') ? colDocumentoCi.split(' ') :
											colDocumentoCi.split('CE') ? colDocumentoCi.split(' ') :
												colDocumentoCi.split('ce') ? colDocumentoCi.split(' ') :
										[null,colDocumentoCi] :
										[colDocumentoTipoAbreviacion,colDocumentoCi];
							[colDocumentoCi,colExtensionAbreviacion] = colDocumentoCi.includes(' ') && !colExtensionAbreviacion ? colDocumentoCi.split(' ') : [colDocumentoCi,null];
							parExtencion = colExtensionAbreviacion ? parExtenciones.find(param => param.parametro_abreviacion == colExtensionAbreviacion) : colExtensionDescripcion ? parExtenciones.find(param => param.parametro_descripcion == colExtensionDescripcion) : null;
						}
						let [docId,docIdExtTxt] = [colDocumentoCi,parExtencion ? parExtencion.parametro_abreviacion : ''];
						let docIdExt, parDocIdExt, docIdExtBen, parDocIdExtBen;
						if (docIdExtTxt) {
							switch (docIdExtTxt) {
								case 'PT':
									parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'PO');
									break;
								case 'PA':
									parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'PA');
									break;
								case 'PE':
									parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'PE');
									break;
								case 'PJ':
									parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'PJ');
									break;
								case 'SE':
									parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'SE');
									break;
								case '':
									parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'SE');
									break;
								case null:
									parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'SE');
									break;
								default:
									parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == docIdExtTxt);
									break;
							}
						} else if(docId) {
							parDocIdExt = parExtenciones.find(param => param.dataValues.parametro_abreviacion == 'SE');
						}
						docIdExt = parDocIdExt && parDocIdExt.dataValues ? parDocIdExt.dataValues.parametro_cod : null;
						let soapCustomerData = consumeSoapServices.getCustomer ? await soap.getCustomer(docId,docIdExt) : null;
                        let soapAccountData = consumeSoapServices.getAccount ? soapCustomerData ? await soap.getAccount(soapCustomerData.cod_agenda) : null : null;
						let aSoapObtenerTarjetasPorNroCIyExtensionData = consumeSoapServices.obtenerTarjetasPorNroCIyExtension ? await soap.obtenerTarjetasPorNroCIyExtension(docId,docIdExt) : null;
						let fechaEmision = colFechaCertificado ? colFechaCertificado.indexOf('/') >= 0 ? moment(colFechaCertificado,"DD/MM/YYYY").add('hours',4).toDate() : colFechaCertificado.indexOf('-') >= 0? moment(colFechaCertificado,"YYYY-MM-DD").add('hours',4).toDate() : null : null;
						let fechaIngreso = colFechaCertificado ? colFechaCertificado.indexOf('/') >= 0 ? moment(colFechaCertificado,"DD/MM/YYYY").add('hours',4).toDate() : colFechaCertificado.indexOf('-') >=0 ? moment(colFechaCertificado,"YYYY-MM-DD").add('hours',4).toDate() : null : null;
						let fechaNacimiento = colFechaNacimiento ? colFechaNacimiento.indexOf('/') >= 0 ? moment(colFechaNacimiento,"DD/MM/YYYY").add('hours',4).toDate() :colFechaNacimiento.indexOf('-') >= 0 ? moment(colFechaNacimiento,"YYYY-MM-DD").add('hours',4).toDate() : null : null;
						let fechaVigenciaInicio =  colFechaInicio ? colFechaInicio.indexOf('/') >= 0 ? moment(colFechaInicio,"DD/MM/YYYY").add('hours',4).toDate() : colFechaInicio.indexOf('-') >= 0? moment(colFechaInicio,"YYYY-MM-DD").add('hours',4).toDate() : null : null;
						let fechaVigenciaFin = colFechaFin ? colFechaFin.indexOf('/') >= 0 ? moment(colFechaFin,"DD/MM/YYYY").add('hours',4).toDate() : colFechaFin.indexOf('-') >= 0 ? moment(colFechaFin,"YYYY-MM-DD").add('hours',4).toDate() : fechaVigenciaInicio ? moment(fechaVigenciaInicio).add(12,'months').toDate() : null : null;
						if (parDocIdExt) {
							// let oldInstanciaDocumentoCertificado = await modelos.instancia_documento.findOne({
							// 	where: {nro_documento: {$like:colNroCertificado}, asegurado_doc_id: {$like:docId.trim()}},
							// 	include: {
							// 		model:modelos.documento,
							// 		where: {
							// 			id_tipo_documento:281
							// 		}
							// 	}
							// });
							let oldAsegurados = await modelos.asegurado.findAll({
								include:[{
									model:modelos.instancia_poliza,
									where:{id_poliza:idPoliza},
									include:[{
										model:modelos.instancia_documento,
										where:{nro_documento:colNroCertificado},
										include: {
											model: modelos.documento,
											where:{id_tipo_documento:281}
										}
									}]
								},{
									model:modelos.entidad,
									include:{
										model:modelos.persona,
										where:{persona_doc_id:docId}
									}
								}]
							});
							let oldAsegurado = oldAsegurados[0];
							let objetoXAtributos = [];
							let oldAtributosInstanciaPoliza = [];
							let objetosPoliza = await modelos.objeto.findAll({where: {id_poliza: idPoliza, id_tipo: 298}});
							if (objetosPoliza.length) {
								objetoXAtributos = await modelos.objeto_x_atributo.findAll({
									where: {id_objeto: objetosPoliza[0].dataValues.id},
									include: {model: modelos.atributo}
								});
							}
							if (oldAsegurado) {
								oldAtributosInstanciaPoliza = await modelos.atributo_instancia_poliza.findAll({
									where:{id_instancia_poliza: oldAsegurado.id_instancia_poliza}
								});
							}
							if (oldAsegurado && oldAtributosInstanciaPoliza.length == objetoXAtributos.length) {
								await modelos[tablaMigracion].update({
									id_instancia_poliza:oldAsegurado.id_instancia_poliza,
									fin:1
								}, {where:{id:colId}});
							} else {
								let usuarioColibri;
								if (!colAdicionadoPor) {
									let nom1,nom2,nom3,nom4,nom5;
									if (colNombreUsuario) {
										[nom1,nom2,nom3,nom4,nom5] = colNombreUsuario.split(' ');
									}
									if (colUsername) {
										let exp = /[^a-zA-Z 0-9.]+/;
										if(!exp.test(colUsername)) {
											usuarioColibri = await modelos.usuario.findOne({where:{$and:[
												{usuario_login:{$like:`%${colUsername}%`}},
											]}});
											if (!usuarioColibri) {
												let newPersonaColibri = {
													persona_doc_id:null,
													persona_doc_id_ext:null,
													persona_doc_id_comp:null,
													persona_primer_apellido:nom1,
													persona_segundo_apellido:nom2,
													persona_primer_nombre:nom3,
													persona_segundo_nombre:nom4,
													persona_apellido_casada:null,
													persona_direccion_domicilio:'',
													persona_direcion_trabajo:'',
													persona_fecha_nacimiento:null,
													persona_celular:'',
													persona_telefono_domicilio:'',
													persona_telefono_trabajo:'',
													persona_profesion:'',
													par_tipo_documento_id:null,
													par_nacionalidad_id:null,
													par_pais_nacimiento_id:null,
													par_sexo_id:null,
													createdAt:new Date(),
													updatedAt:new Date(),
													persona_origen:null,
													adicionada_por:583,
													modificada_por:583,
													persona_email_personal:null
												};
												let personaColibri = await modelos.persona.create(newPersonaColibri);
												let newUsuarioColibri = {
													usuario_login:colUsername,
													usuario_password:'$2a$10$DXCnKPSJr.Q.KzMOKDmK8ukTOKpZzNGDOhUF3VFGxkGFQMw8RVu8S',
													usuario_email:null,
													usuario_nombre_completo:colNombreUsuario,
													usuario_img:null,
													par_estado_usuario_id:8,
													id_persona:personaColibri.id,
													createdAt:new Date(),
													updatedAt:new Date(),
													pwd_change:false,
													par_local_id:26,
													adicionado_por:583,
													modificado_por:583
												};
												let usuarioColibri = await modelos.usuario.create(newUsuarioColibri);
												let newAutenticacionUsuario = {
													usuario_id:usuarioColibri.id,
													par_autenticacion_id:10,
													createdAt:new Date(),
													updatedAt:new Date(),
													atributo1:null,
												}
												let autenticacionUsuario = await modelos.autenticacion_usuario.create(newAutenticacionUsuario);
											}
										}
									}
									if (nom1 && nom2 && nom3 && nom4 && nom5) {
										usuarioColibri = await modelos.usuario.findOne({where:{$and:[
													{usuario_nombre_completo:{$like:`%${nom1}%`}},
													{usuario_nombre_completo:{$like:`%${nom2}%`}},
													{usuario_nombre_completo:{$like:`%${nom3}%`}},
													{usuario_nombre_completo:{$like:`%${nom4}%`}},
													{usuario_nombre_completo:{$like:`%${nom5}%`}}
												]}})
									} else if (nom1 && nom2 && nom3 && nom4) {
										usuarioColibri = await modelos.usuario.findOne({where:{$and:[
													{usuario_nombre_completo:{$like:`%${nom1}%`}},
													{usuario_nombre_completo:{$like:`%${nom2}%`}},
													{usuario_nombre_completo:{$like:`%${nom3}%`}},
													{usuario_nombre_completo:{$like:`%${nom4}%`}},
												]}})
									} else if (nom1 && nom2 && nom3) {
										usuarioColibri = await modelos.usuario.findOne({where:{$and:[
													{usuario_nombre_completo:{$like:`%${nom1}%`}},
													{usuario_nombre_completo:{$like:`%${nom2}%`}},
													{usuario_nombre_completo:{$like:`%${nom3}%`}},
												]}})
									} else if (nom1 && nom2 ) {
										usuarioColibri = await modelos.usuario.findOne({where:{$and:[
													{usuario_nombre_completo:{$like:`%${nom1}%`}},
													{usuario_nombre_completo:{$like:`%${nom2}%`}},
												]}})
									} else if (nom1) {
										usuarioColibri = await modelos.usuario.findOne({where:{$and:[
													{usuario_login:{$like:`%${nom1}%`}},
												]}});
									}
									if (usuarioColibri) {
										await modelos[tablaMigracion].update({adicionado_por:usuarioColibri.id},{where:{id:colId}});
										colAdicionadoPor = usuarioColibri.id;
									} else {

									}
								}
								let adicionadoPor = colAdicionadoPor ? colAdicionadoPor : 583;
								parTipoDocumento = colDocumentoTipoCod ? parTipoDocumentos.find(param => param.dataValues.parametro_cod.removeAccents().toUpperCase() == colDocumentoTipoCod.removeAccents().toUpperCase()) :
                                                        colDocumentoTipoId ? parTipoDocumentos.find(param => param.dataValues.id == colDocumentoTipoId) :
                                                            colDocumentoTipoDescripcion ? parTipoDocumentos.find(param => param.dataValues.parametro_descripcion.removeAccents().toUpperCase() == colDocumentoTipoDescripcion.removeAccents().toUpperCase()) :
																colDocumentoTipoAbreviacion ? parTipoDocumentos.find(param => param.dataValues.parametro_abreviacion.removeAccents().toUpperCase() == colDocumentoTipoAbreviacion.removeAccents().toUpperCase()) : null;
								let parSexo, parMonedaOperacion, polizaId, parEstado, parModalidadPago, parSucursal, parAgencia,
                                    operacionPlazo, parOperacionProductoAsociado, nroCuotasMonths, nroCuotasYears, tipoDoc, soapTarjetaCredito, parentescoId, parentescoDesc;
								if (colGenero) {
									if(colGenero.removeAccents().toLowerCase().includes('varon') || colGenero.removeAccents().toLowerCase().includes('masculino')) {
										parSexo = parMasculino;
									} else if(colGenero.removeAccents().toLowerCase().includes('mujer') || colGenero.removeAccents().toLowerCase().includes('femenino')) {
										parSexo = parFemenino;
									}
								} else if (soapCustomerData) {
									parSexo = soapCustomerData.sexo == 'F' ? parFemenino : soapCustomerData.sexo == 'M' ? parMasculino : '';
								}
								if(colMonedaOperacionDescripcion) {
                                    parMonedaOperacion = parMonedas.find(param => param.parametro_descripcion.removeAccents().toLowerCase() == colMonedaOperacionDescripcion.removeAccents().toLowerCase());
                                }
								if (!parMonedaOperacion && colMonedaOperacionAbreviacion) {
                                    parMonedaOperacion = parMonedas.find(param => param.parametro_abreviacion.removeAccents().toLowerCase() == colMonedaOperacionAbreviacion.removeAccents().toLowerCase());
                                }
								if (aSoapObtenerTarjetasPorNroCIyExtensionData) {
                                    for (let j = 0; j < aSoapObtenerTarjetasPorNroCIyExtensionData.length; j++) {
                                        let soapObtenerTarjetasPorNroCIyExtensionData = aSoapObtenerTarjetasPorNroCIyExtensionData[j];
                                        if (soapObtenerTarjetasPorNroCIyExtensionData && [parEstadoTarjetaAceptada.parametro_descripcion.toLowerCase(),parEstadoTarjetaHabilitada.parametro_descripcion.toLowerCase()].includes(soapObtenerTarjetasPorNroCIyExtensionData.estado.trim().toLowerCase())) {
                                            soapTarjetaCredito = soapObtenerTarjetasPorNroCIyExtensionData
                                        }
                                    }
                                }
								polizaId = idPoliza;
								if (colOperacionEstado) {
									if (colOperacionEstado.removeAccents().toLowerCase() == 'activo') {
										parEstado = parEstadoEmitido;
									} else if (colOperacionEstado.removeAccents().toLowerCase() == 'inactivo') {
										parEstado = parEstadoSinVigencia;
									} else {
										parEstado = parEstadosInstancias.find(param =>
											param.parametro_descripcion.removeAccents().toLowerCase().includes(colOperacionEstado.removeAccents().toLowerCase()) ||
											colOperacionEstado.removeAccents().toLowerCase().includes(param.parametro_descripcion.removeAccents().toLowerCase()) ||
											param.parametro_descripcion.removeAccents().toLowerCase() == colOperacionEstado.removeAccents().toLowerCase());
									}
                                } else {
                                    if (colEmitido) {
                                        if(colEmitido == 'SI' || colEmitido == 'true' || colEmitido == true) {
                                            parEstado = parEstadoEmitido;
                                        } else {
                                            parEstado = parEstadoSolicitado;
                                        }
                                    } else {
                                        parEstado = parEstadoEmitido;
                                    }
                                }
								if (!parEstado) {
                                    parEstado = parEstadoEmitido;
                                }
								if (colModalidadPago) {
									if(colModalidadPago == 'MENSUAL') {
                                        parModalidadPago = parModalidadPagoMensual
									} else if (colModalidadPago == 'ANUAL') {
                                        parModalidadPago = parModalidadPagoAnual
									}
								} else {
									parModalidadPago = parModalidadPagoMensual
								}
								if (colOperacionPlazo) {
                                    operacionPlazo = colOperacionPlazo;
								} else {
                                    operacionPlazo = fechaVigenciaFin ? moment(fechaVigenciaFin).diff(moment(fechaVigenciaInicio), 'months') : null;
								}
								if (colSucursalCodigo) {
									if (!parSucursal) {
										parSucursal = parSucursales.find(param => param.dataValues.parametro_cod == colSucursalCodigo);
									}
								} else {
									if (colSucursalDescripcion) {
										[colSucursalCodigo,colSucursalDescripcion] = colSucursalDescripcion ? colSucursalDescripcion.includes('SUCURSAL') ? colSucursalDescripcion.split('SUCURSAL') : colSucursalDescripcion.includes('sucursal') ? colSucursalDescripcion.split('sucursal') : [null,colSucursalDescripcion] : [null,colSucursalDescripcion];
									}
									if (colAgenciaDescripcion) {
										[colAgenciaCodigo,colAgenciaDescripcion] = colAgenciaDescripcion ? colAgenciaDescripcion.includes('AGENCIA') ? colAgenciaDescripcion.split('AGENCIA') : colAgenciaDescripcion.includes('agencia') ? colAgenciaDescripcion.split('agencia') : [null,colAgenciaDescripcion] : [null,colAgenciaDescripcion];
									}
									if (colSucursalDescripcion) {
										parSucursal = parSucursales.find(param =>
											colSucursalDescripcion.removeAccents().toUpperCase().trim().includes(param.dataValues.parametro_descripcion.removeAccents().toUpperCase().trim()) ||
											param.dataValues.parametro_descripcion.removeAccents().toUpperCase().trim().includes(colSucursalDescripcion.removeAccents().toUpperCase().trim()) ||
											param.dataValues.parametro_descripcion.removeAccents().toUpperCase().trim() == colSucursalDescripcion.removeAccents().toUpperCase().trim());
									}
								}
								if (!parSucursal) {
									parSucursal = parSucursales.find(param => param.dataValues.parametro_cod == colAgenciaCodigo);
								}
								if (colAgenciaCodigo) {
									if (!parAgencia) {
										parAgencia = parAgencias.find(param => param.dataValues.parametro_cod.removeAccents().toUpperCase().trim() == colAgenciaCodigo.removeAccents().toUpperCase().trim());
									}
								} else {
									if(colAgenciaDescripcion) {
										parAgencia = parAgencias.find(param =>
											colAgenciaDescripcion.removeAccents().toUpperCase().trim().includes(param.dataValues.parametro_descripcion.removeAccents().toUpperCase().trim()) ||
											param.dataValues.parametro_descripcion.removeAccents().toUpperCase().trim().includes(colAgenciaDescripcion.removeAccents().toUpperCase().trim()) ||
											param.dataValues.parametro_descripcion.removeAccents().toUpperCase().trim() == colAgenciaDescripcion.removeAccents().toUpperCase().trim()
										);
									}
									if (!parAgencia && colSucursalDescripcion) {
										parAgencia = parAgencias.find(param =>
											colSucursalDescripcion.removeAccents().toUpperCase().trim().includes(param.dataValues.parametro_descripcion.removeAccents().toUpperCase().trim()) ||
											param.dataValues.parametro_descripcion.removeAccents().toUpperCase().trim().includes(colSucursalDescripcion.removeAccents().toUpperCase().trim()) ||
											param.dataValues.parametro_descripcion.removeAccents().toUpperCase().trim() == colSucursalDescripcion.removeAccents().toUpperCase().trim()
										);
									}
								}
								if (parAgencia && !parSucursal) {
									parSucursal = parSucursales.find(param => param.id == parAgencia.dataValues.id_padre);
								}
								if (colOperacionProductoAsociado) {
									parOperacionProductoAsociado = parOperacionProductosAsociados.find(param => param.dataValues.parametro_cod.trim() == colOperacionProductoAsociado.trim());
								} else {
									parOperacionProductoAsociado = parOperacionProductosAsociados.find(param => param.dataValues.parametro_cod.trim() == idPoliza+'');
								}
                                [colBeneficiarioDocumentoCi,colBeneficiarioExtensionAbreviacion] = colBeneficiarioDocumentoCi ? colBeneficiarioDocumentoCi.includes(' ') ? colBeneficiarioDocumentoCi.split(' ') : [colBeneficiarioDocumentoCi,null] : [null,null];
								if (!parDocIdExtBen && colBeneficiarioExtensionDescripcion && !colBeneficiarioExtensionAbreviacion) {
                                    parDocIdExtBen = parExtenciones.find(param => param.dataValues.parametro_descripcion.removeAccents().toUpperCase().includes(colAgenciaDescripcion.removeAccents().toUpperCase()));
                                }
								if (!parDocIdExtBen && colBeneficiarioExtensionAbreviacion) {
								    parDocIdExtBen = parExtenciones.find(param => param.parametro_abreviacion == colBeneficiarioExtensionAbreviacion);
                                }
								if (parAgencia && parSucursal) {
									await modelos[tablaMigracion].update({agencia_cod:parAgencia.parametro_cod, sucursal_cod:parSucursal.parametro_cod},{where:{id:colId}});
								}
                                if (colBeneficiarioParentescoDescripcion) {
									if (colBeneficiarioParentescoDescripcion.includes('convivi') || colBeneficiarioParentescoDescripcion.includes('convivi'.toUpperCase())) {
										parentescoId = parPareentescoConviviente.id;
									} else if (colBeneficiarioParentescoDescripcion.includes('conyugue') || colBeneficiarioParentescoDescripcion.includes('conyugue'.toUpperCase())){
										parentescoId = parPareentescoConyuge.id;
									} else if (colBeneficiarioParentescoDescripcion.includes('hij') || colBeneficiarioParentescoDescripcion.includes('hij'.toUpperCase())){
										parentescoId = parPareentescoHijo.id;
									} else if (colBeneficiarioParentescoDescripcion.includes('herman') || colBeneficiarioParentescoDescripcion.includes('herman'.toUpperCase())){
										parentescoId = parPareentescoHermano.id;
									} else if (colBeneficiarioParentescoDescripcion.includes('madre') || colBeneficiarioParentescoDescripcion.includes('madre'.toUpperCase())){
										parentescoId = parPareentescoMadre.id;
									} else if (colBeneficiarioParentescoDescripcion.includes('padre') || colBeneficiarioParentescoDescripcion.includes('padre'.toUpperCase())){
										parentescoId = parPareentescoPadre.id;
									} else {
										parentescoId = parPareentescoOtroParentesco.id;
										parentescoDesc = colBeneficiarioParentescoDescripcion;
									}
								}
								nroCuotasMonths = fechaVigenciaInicio && fechaVigenciaFin ? moment(fechaVigenciaFin).diff(moment(fechaVigenciaInicio),'months') : parModalidadPago ? parModalidadPago.id == parModalidadPagoMensual.id ? '12' : null : null;
								nroCuotasYears = fechaVigenciaInicio && fechaVigenciaFin ? moment(fechaVigenciaFin).diff(moment(fechaVigenciaInicio),'years') : parModalidadPago ? parModalidadPago.id == parModalidadPagoAnual.id ? '1' : null : null;
								[colPersonaPrimerApellido,colPersonaSegundoApellido] = colPersonaPrimerApellido.indexOf(' ') ? colPersonaPrimerApellido.split(' ') : [colPersonaPrimerApellido,null];
								[colPersonaPrimerNombre,colPersonaSegundoNombre] = colPersonaPrimerNombre.indexOf(' ') ? colPersonaPrimerNombre.split(' ') : [colPersonaPrimerNombre,null];
								if(parAgencia && parSucursal && adicionadoPor && polizaId) {
									if (objetosPoliza.length) {
										let documentos = await modelos.documento.findAll({where: {id_poliza: polizaId}});
										let newPersona = {
											persona_doc_id: docId,
											persona_doc_id_ext: parDocIdExt ? parDocIdExt.parametro_cod : '',
											persona_primer_apellido: colPersonaPrimerApellido,
											persona_segundo_apellido: colPersonaSegundoApellido,
											persona_primer_nombre: colPersonaPrimerNombre,
											persona_segundo_nombre: colPersonaSegundoNombre,
											persona_direccion_domicilio: colPersonaDireccionDomicilio ? colPersonaDireccionDomicilio : '',
											persona_fecha_nacimiento: fechaNacimiento ? fechaNacimiento : null,
											persona_celular: colPersonaCelular ? colPersonaCelular : '',
											persona_telefono_domicilio: colPersonaTelefonoDomicilio ? colPersonaTelefonoDomicilio : '',
											par_tipo_documento_id: parTipoDocumento ? parTipoDocumento.id : null,
											par_sexo_id: parSexo ? parSexo.id : null,
											createdAt: fechaIngreso,
											updatedAt: createdAt,
											adicionada_por: adicionadoPor,
											modificada_por: adicionadoPor
										};
										let oldPersona = await modelos.persona.findOne({where:{
											persona_doc_id: docId,
										}});
										let persona;
										if (oldPersona) {
											// persona = await modelos.persona.update(newPersona,{where:{id:oldPersona.id}});
											persona = await modelos.persona.findOne({where:{id:oldPersona.id}});
											persona = persona.dataValues;
										} else {
											persona = await modelos.persona.create(newPersona);
											persona = persona.dataValues;
										}
										let newEntidad = {
											id_persona: persona.id,
											tipo_entidad: parPersonaNatural.id,
											adicionada_por: adicionadoPor,
											modificada_por: adicionadoPor,
											createdAt: fechaIngreso,
											updatedAt: createdAt
										};
										let oldEntidad = await modelos.entidad.findOne({where:{
											id_persona: persona.id,
                                            tipo_entidad: parPersonaNatural.id,
										}});
										let entidad;
										if (oldEntidad) {
											// entidad = await modelos.entidad.update(newEntidad,{where:{id:oldEntidad.id}});
											entidad = await modelos.entidad.findOne({where:{id:oldEntidad.id}});
											entidad = entidad.dataValues;
										} else {
											entidad = await modelos.entidad.create(newEntidad);
											entidad = entidad.dataValues;
										}
										let oldInstanciaPoliza, newInstanciaPoliza;
										if (oldAsegurado) {
											oldInstanciaPoliza = await modelos.instancia_poliza.findOne({where:{id:oldAsegurado.id_instancia_poliza,id_poliza:polizaId}});
										} else {
											newInstanciaPoliza = {
												id_poliza: polizaId,
												id_estado: parEstado.id,
												fecha_registro: fechaIngreso,
												adicionada_por: adicionadoPor,
												modificada_por: adicionadoPor,
												createdAt: fechaIngreso,
												updatedAt: createdAt,
											};
										}

										let instanciaPoliza;
										if(oldInstanciaPoliza) {
											// instanciaPoliza = await modelos.instancia_poliza.update(newInstanciaPoliza,{where:{id:oldInstanciaPoliza.id}});
											instanciaPoliza = await modelos.instancia_poliza.findOne({where:{id:oldInstanciaPoliza.id}});
											instanciaPoliza = instanciaPoliza.dataValues;
										} else {
											instanciaPoliza = await modelos.instancia_poliza.create(newInstanciaPoliza);
											await modelos[tablaMigracion].update({id_instancia_poliza:instanciaPoliza.id,}, {where:{id:colId}});
											instanciaPoliza = instanciaPoliza.dataValues;
											let updatedIntanciaPoliza = await modelos.instancia_poliza.update({id_instancia_renovada:instanciaPoliza.id}, {where:{id:instanciaPoliza.id}});
										}

										let newAsegurado = {
											id_entidad: entidad.id,
											id_instancia_poliza: instanciaPoliza.id,
											adicionado_por: adicionadoPor,
											modificado_por: adicionadoPor,
											createdAt:  fechaIngreso,
											updatedAt: createdAt
										};
										let asegurado;
										if (oldAsegurado) {
											// asegurado = await modelos.asegurado.update(newAsegurado, {where:{id:oldAsegurado.id}});
											asegurado = await modelos.asegurado.findOne({where:{id:oldAsegurado.id}});
										} else {
											asegurado = await modelos.asegurado.create(newAsegurado);
										}
										asegurado = asegurado.dataValues;
										let atributosInstanciaPoliza = [];
										for (let j = 0 ; j < objetoXAtributos.length ; j++) {
											let objetoXAtributo = objetoXAtributos[j].dataValues;
											let valor, tipoError;
											switch (objetoXAtributo.atributo.id) {
												case '1': valor = ''; tipoError = 'El titular no tiene registrado su prima'; break;
												case '2': valor = ''; tipoError = 'El titular no tiene registrado su valor asegurado'; break;
												case '3': valor = colPersonaEstadoCivil ? colPersonaEstadoCivil : soapCustomerData ? soapCustomerData.est_civil : ''; tipoError = 'El titular no tiene registrado su estado civil'; break;
												case '4': valor = ''; tipoError = 'El titular no tiene registrado su e-mail'; break;
												case '5': valor = ''; tipoError = 'El titular no tiene registrado un numero de cuenta'; break;
												case '9': valor = ''; tipoError = 'El titular no tiene registrado el tipo debito'; break;
												case '10': valor = colPersonaCodAgenda ? colPersonaCodAgenda : soapCustomerData ? soapCustomerData.cod_agenda : ''; tipoError = 'El titular no tiene registrado un codigo de agenda'; break;
												case '12': valor = parCondicionNo.id; tipoError = 'El titular no tiene registrado si el debito sera automatico'; break;
												case '14': valor = ''; tipoError = 'El titular no tiene registrado un numero CAEDEC'; break;
												case '16': valor = ''; tipoError = 'El titular no tiene registrado una localidad'; break;
												case '17': valor = ''; tipoError = 'El titular no tiene registrado un departamento'; break;
												case '18': valor = ''; tipoError = 'El titular no tiene registrado un codigo de sucursal'; break;
												case '19': valor = parTipoDocumento ? parTipoDocumento.id : ''; tipoError = 'El titular no tiene registrado el tipo de documento utilizado'; break;
												case '20': valor = ''; tipoError = 'El titular no tiene registrado un codigo de manejo'; break;
												case '21': valor = ''; tipoError = 'El titular no tiene registrado un tipo de moneda'; break;
												case '22': valor = ''; tipoError = 'El titular no tiene registrado una descripción en caedec'; break;
												case '23': valor = colPersonaNroTarjeta ? colPersonaNroTarjeta : soapTarjetaCredito ? soapTarjetaCredito.nrotarjeta : ''; tipoError = 'El titular no tiene registrado un Nro de tarjeta'; break;
												case '24': valor = ''; tipoError = 'El titular no tiene registrado un nombre de tarjeta'; break;
												case '25': valor = ''; tipoError = 'El titular no tiene registrado si su tarjeta es valida'; break;
												case '26': valor = ''; tipoError = 'El titular no tiene registrado su tipo de tarjeta'; break;
												case '28': valor = parModalidadPago ? parModalidadPago.id : ''; tipoError = 'El titular no tiene registrado una modalidad de pago'; break;
												case '29': valor = ''; tipoError = 'El titular no tiene registrado una zona'; break;
												case '30': valor = ''; tipoError = 'El titular no tiene registrado un Nro de direccion'; break;
												case '32': valor = ''; tipoError = 'El titular no tiene registrado una razon social'; break;
												case '33': valor = ''; tipoError = 'El titular no tiene registrado un Nit o Carnet'; break;
												case '34': valor = ''; tipoError = 'El titular no tiene registrado los Ultimos cuatro dígitos del número de tarjeta'; break;
												case '35': valor = ''; tipoError = 'El titular no tiene registrado la Fecha de expiracion de la cuenta'; break;
												case '36': valor = ''; tipoError = 'El titular no tiene registrado su tipo de tarjeta.'; break;
												case '37': valor = ''; tipoError = 'El titular no tiene registrado una ocupación'; break;
												case '38': valor = ''; tipoError = 'El titular no tiene registrado el plan'; break;
												case '43': valor = ''; tipoError = 'El titular no tiene registrado el Porcentaje'; break;
												case '44': valor = ''; tipoError = 'El titular no tiene registrado la Relacion'; break;
												case '46': valor = ''; tipoError = 'El titular no tiene registrado la descripcion'; break;
												case '48': valor = ''; tipoError = 'El titular no tiene registrado la edad limite para admision titular'; break;
												case '50': valor = ''; tipoError = 'El titular no tiene registrado la edad limite admision dependientes'; break;
												case '53': valor = ''; tipoError = 'El titular no tiene registrado la edad Limite para admision titular'; break;
												case '54': valor = ''; tipoError = 'El titular no tiene registrado la edad Limite para admision dependiente'; break;
												case '55': valor = ''; tipoError = 'El titular no tiene registrado el nro_maximo_dependientes'; break;
												case '57': valor = ''; tipoError = 'El titular no tiene registrado la declaracion obligatoria dependientes'; break;
												case '58': valor = ''; tipoError = 'El titular no tiene registrado el Plazo'; break;
												case '59': valor = parAgencia.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una agencia'; break;
												case '60': valor = parSucursal.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una sucursal'; break;
												case '61': valor = colPrimaMensual; tipoError = 'El titular no tiene registrado el monto de la prima'; break;
												case '62': valor = colPersonaCelular; tipoError = 'El titular no tiene registrado el Telefono Celular'; break;
												case '63': valor = ''; tipoError = 'El titular no tiene registrada una ciudad de nacimiento'; break;
												case '64': valor = ''; tipoError = 'La transaccion no tiene registrado un nro de transacción'; break;
												case '65': valor = ''; tipoError = 'La transaccion no tiene registrado una fecha de transacción'; break;
												case '66': valor = colTransaccionImporte; tipoError = 'La transaccion no tiene registrado un importe'; break;
												case '67': valor = colTransaccionTipoMoneda; tipoError = 'La transaccion no tiene registrado un tipo de moneda'; break;
												case '69': valor = colTransaccionDetalle; tipoError = 'La transaccion no tiene registrado un detalle de transacción'; break;
												case '70': valor = colUsuarioCargoDescripcion ? colUsuarioCargoDescripcion : colUsuarioCargoCodigo ? colUsuarioCargoCodigo : ''; tipoError = 'El usuario del banco no tiene registrado un cargo'; break;
												case '71': valor = ''; tipoError = 'El titular no tiene un cargo publico y/o politico jerarquico'; break;
												case '72': valor = ''; tipoError = 'El titular no tiene registrado un cargo/entidad pep'; break;
												case '73': valor = ''; tipoError = 'El titular no tiene registrado un periodo del cargo publico pep'; break;
												case '74': valor = ''; tipoError = 'El titular no tiene una  direccion laboral'; break;
												case '75': valor = ''; tipoError = 'El titular no tiene un tipo de cuenta'; break;
												case '76': valor = parModalidadPago ? parModalidadPago.id == parModalidadPagoMensual.id ? nroCuotasMonths : parModalidadPago.id == parModalidadPagoAnual.id ? nroCuotasYears : '' : ''; tipoError = 'El titular no tiene registrado un Nro. de cuotas'; break;
												case '77': valor = ''; tipoError = 'El titular no tiene registrado la descripción de su ocupación'; break;
												case '78': valor = colOperacionNroSolicitud ? colOperacionNroSolicitud : ''; tipoError = 'El titular no tiene registrado la Operacion Nro Solicitud Sci'; break;
												case '79': valor = colOperacionTipoSeguro ? colOperacionTipoSeguro : '' ; tipoError = 'El titular no tiene registrado el Tipo de Seguro'; break;
												case '80': valor = parOperacionProductoAsociado ? parOperacionProductoAsociado.id : '' ; tipoError = 'El titular no tiene registrado el Producto Asociado'; break;
												case '81': valor = ''; tipoError = 'El titular no tiene registrado la Operacion Solicitud Estado'; break;
												case '82': valor = operacionPlazo ? operacionPlazo : ''; tipoError = 'El titular no tiene registrado la Operacion Plazo del credito'; break;
												case '83': valor = parMonedaOperacion ? parMonedaOperacion.id : ''; tipoError = 'El titular no tiene registrado la Operacion Moneda'; break;
												case '84': valor = ''; tipoError = 'El titular no tiene registrado la Operacion Frecuencia Plazo'; break;
												case '85': valor = colPrimaTotal ? colPrimaTotal : '' ; tipoError = 'El titular no tiene registrado la Operacion Prima Total'; break;
												case '86': valor = ''; tipoError = 'El titular no tiene registrado la Forma de Pago'; break;
												case '87': valor = ''; tipoError = 'El titular no tiene registrado la Fecha Activacion Tarjeta'; break;
												case '88': valor = ''; tipoError = 'El titular no tiene registrado la Fecha Fin Vigencia Tarjeta'; break;
												case '89': valor = ''; tipoError = 'El titular no tiene registrado el Estado Tarjeta'; break;
												case '90': valor = ''; tipoError = 'El titular no tiene registrado la Operacion Tipo Credito'; break;
												case '91': valor = ''; tipoError = 'El titular no tiene registrado su provincia'; break;
												case '92': valor = ''; tipoError = 'El titular no tiene registrado el Lugar de Nacimiento'; break;
												case '93': valor = colIdTarjeta ? colIdTarjeta : soapTarjetaCredito ? soapTarjetaCredito.id : ''; tipoError = 'El titular no tiene registrado el id de tarjeta'; break;
												case '94': valor = ''; tipoError = 'El titular no tiene registrado el Motivo de la Anulacion'; break;
											}

											let newAtributoInstanciaPoliza = {
												id_instancia_poliza: instanciaPoliza.id,
												id_objeto_x_atributo: objetoXAtributo.id,
												valor: valor,
												tipo_error: tipoError,
												adicionado_por: adicionadoPor,
												modificado_por: adicionadoPor,
												createdAt: fechaIngreso,
												updatedAt: createdAt
											};
											let oldAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{
												id_instancia_poliza: instanciaPoliza.id,
												id_objeto_x_atributo: objetoXAtributo.id
											}});
											let atributoInstanciaPoliza;
											if (oldAtributoInstanciaPoliza) {
												// atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.update(newAtributoInstanciaPoliza,{where:{id:oldAtributoInstanciaPoliza.id}});
												atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id:oldAtributoInstanciaPoliza.id}});
												atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
											} else {
												atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.create(newAtributoInstanciaPoliza);
												atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
											}
											atributosInstanciaPoliza.push(atributoInstanciaPoliza);
										}
										let instanciaDocumentos = [];
										for (let k = 0 ; k < documentos.length ; k++) {
											let documento = documentos[k].dataValues;
											let newInstanciaDocumeento = {
												id_instancia_poliza: instanciaPoliza.id,
												id_documento: documento.id,
												id_documento_version: documento.id_documento_version,
												nro_documento: colNroCertificado,
												fecha_emision: fechaEmision,
												fecha_inicio_vigencia: fechaVigenciaInicio,
												fecha_fin_vigencia: fechaVigenciaFin,
												asegurado_doc_id: persona.persona_doc_id,
												asegurado_nombre1: persona.persona_primer_nombre,
												asegurado_nombre2: persona.persona_segundo_nombre,
												asegurado_apellido1: persona.persona_segundo_nombre,
												asegurado_apellido2: persona.persona_segundo_apellido,
												tipo_persona: entidad.id,
												adicionada_por: adicionadoPor,
												modificada_por: adicionadoPor,
												createdAt: fechaIngreso,
												updatedAt: createdAt
											};
											let oldInstanciaDocumento = await modelos.instancia_documento.findOne({where:{
													id_instancia_poliza: instanciaPoliza.id,
													id_documento: documento.id,
													nro_documento: colNroCertificado,
													//fecha_emision: fechaEmision,
												}});
											let instanciaDocumento;
											if([279,280,281].includes(parseInt(documento.id_tipo_documento+''))) {
												if (oldInstanciaDocumento) {
													// instanciaDocumento =  await modelos.instancia_documento.update(newInstanciaDocumeento,{where:{id:oldInstanciaDocumento.id}});
													instanciaDocumento = await modelos.instancia_documento.findOne({where:{id:oldInstanciaDocumento.id}});
													instanciaDocumento = instanciaDocumento.dataValues;
												} else {
													instanciaDocumento = await modelos.instancia_documento.create(newInstanciaDocumeento);
													instanciaDocumento = instanciaDocumento.dataValues;
												}
												instanciaDocumentos.push(instanciaDocumento);
											}
										}
										if (colBeneficiarioPrimerApellido == 'no se cuenta') {
                                            colBeneficiarioPrimerApellido = '';
										}
										if (colBeneficiarioSegundoApellido == 'no se cuenta') {
                                            colBeneficiarioSegundoApellido = '';
										}
										if (colBeneficiarioSegundoNombre == 'no se cuenta' ) {
                                            colBeneficiarioSegundoNombre = '';
										}
										if (colBeneficiarioPrimerNombre == 'no se cuenta' ) {
                                            colBeneficiarioPrimerNombre = '';
										}
										let beneficiario;
										if (colBeneficiarioPrimerApellido || colBeneficiarioSegundoApellido || colBeneficiarioPrimerNombre || colBeneficiarioSegundoNombre) {
											let newPersonaBeneficiario = {
												persona_primer_apellido:colBeneficiarioPrimerApellido,
												persona_segundo_apellido:colBeneficiarioSegundoApellido,
												persona_primer_nombre:colBeneficiarioPrimerNombre,
												persona_segundo_nombre:colBeneficiarioSegundoNombre,
												persona_doc_id:colBeneficiarioDocumentoCi,
												persona_doc_id_ext:colBeneficiarioExtensionAbreviacion,
												createdAt:fechaIngreso,
												updatedAt:createdAt,
												adicionada_por:adicionadoPor,
												modificada_por:adicionadoPor
											}

											let personaBeneficiario;

											personaBeneficiario = await modelos.persona.create(newPersonaBeneficiario);
											personaBeneficiario = personaBeneficiario.dataValues;

											let newEntidadBeneficiario = {
												id_persona: personaBeneficiario.id,
												tipo_entidad:12,
												adicionada_por:adicionadoPor,
												modificada_por:adicionadoPor,
												createdAt:fechaIngreso,
												updatedAt:createdAt
											}

											let entidadBeneficiario;

											entidadBeneficiario = await modelos.entidad.create(newEntidadBeneficiario);
											entidadBeneficiario = entidadBeneficiario.dataValues;

											let newBeneficiario = {
												id_asegurado: asegurado.id,
												id_beneficiario: entidadBeneficiario.id,
												id_parentesco: parentescoId,
												porcentaje: colBeneficiarioPorcentaje,
												adicionado_por: adicionadoPor,
												modificado_por: adicionadoPor,
												createdAt: fechaIngreso,
												updatedAt: createdAt,
												descripcion_otro: parentescoDesc,
												estado: parBeneficiarioActivo.id,
												tipo: parBeneficiarioOriginal.id,
												fecha_declaracion: fechaIngreso
											};
											let oldBeneficiario = await modelos.beneficiario.findOne({where:{
												id_asegurado: asegurado.id,
												id_beneficiario: entidad.id,
												id_parentesco: parentescoId,
												porcentaje: colBeneficiarioPorcentaje,
											}});

											if (!oldBeneficiario) {
												beneficiario = await modelos.beneficiario.create(newBeneficiario);
												beneficiario = beneficiario.dataValues;
											} else {
												// beneficiario = await modelos.beneficiario.update(newBeneficiario,{where:{id:oldBeneficiario.id}});
												beneficiario = await modelos.beneficiario.findOne({where:{id:oldBeneficiario.id}});
												beneficiario = beneficiario.dataValues;
											}
										}
										await modelos.instancia_poliza_transicion.create({
											id_instancia_poliza:instanciaPoliza.id,
											par_estado_id:instanciaPoliza.id_estado,
											par_observacion_id:97,
											observacion:`Esta solicitud fue migrada de un sistema anterior, ID: ${instanciaPoliza.id}`,
											adicionado_por:adicionadoPor,
											modificado_por:adicionadoPor,
											updatedAt:new Date(),
											createdAt:new Date()
										})
										await modelos[tablaMigracion].update({
											fin:1,
											adicionado_por:adicionadoPor
										}, {where:{id:colId}});
                                        dataToReturn.push({
											persona: persona,
											entidad: entidad,
											asegurado: asegurado,
											instanciaPoliza: instanciaPoliza,
											atributosInstanciaPoliza: atributosInstanciaPoliza,
											beneficiario: beneficiario ? beneficiario : null,
											instanciaDocumentos: instanciaDocumentos
										});
									}
								} else {
									errors.push({
										params: {
											sucursal:parSucursal ? parSucursal.dataValues : '',
											agencia:parAgencia ? parAgencia.dataValues : ''
										},
										dato: dato.dataValues
									});
								}
							}
						}
					}
				}
			}
			return [dataToReturn,errors];
		} catch (e) {
			console.log(e)
		}
	}

	static async migraEcoProteccion(req, res) {
		try {
			let response = { status: 'OK', message: '', data: [], errors:[] };
			let parametros = await modelos.parametro.findAll({where: {diccionario_id: [1, 4, 7, 11, 18, 20, 21, 22, 25, 29, 31, 38, 40]}});
			let hoy = new Date();
			let createdAt = hoy;
			let year = hoy.getFullYear(), month = hoy.getMonth(), day = hoy.getDate();
			let polizas = await modelos.poliza.findAll();
			let fechaIngreso;
			let datos = await modelos.mig_ecoproteccion.findAll({where:{$and:[
						{id_instancia_poliza:null},
						{ci:{$not:null}},
						{ci:{$not:'NULL'}}
					]}});

			// parametros

			let adicionadoPor;
			let parTipoDocumento;
			let tipoDocumento;
			let parTipoDocumentos = parametros.filter(param => param.dataValues.diccionario_id == 1);
			let parMasculino = parametros.find(param => param.dataValues.id == 6);
			let parFemenino = parametros.find(param => param.dataValues.id == 7);
			let parPersonaNatural = parametros.find(param => param.dataValues.id == 12);
			let parPersonaJuridica = parametros.find(param => param.dataValues.id == 13);
			let polizaEcoProteccion = polizas.find(param => param.dataValues.id == 7);
			let polizaEcoVida = polizas.find(param => param.dataValues.id == 6);
			let parEstadoEmitido = parametros.find(param => param.dataValues.id == 59);
			let parEstadoIniciado =  parametros.find(param => param.dataValues.id == 24);
			let parEstadoSolicitado = parametros.find(param => param.dataValues.id == 81);
			let parModalidadPagoMensual = parametros.find(param => param.dataValues.id == 90);
			let parModalidadPagoAnual = parametros.find(param => param.dataValues.id == 93);
			let parSucursales = parametros.filter(param => param.dataValues.diccionario_id == 38);
			let parAgencias = parametros.filter(param => param.dataValues.diccionario_id == 40);
			let parCondiciones = parametros.filter(param => param.dataValues.diccionario_id == 21);
			let parCondicionSi = parametros.find(param => param.dataValues.id == 57);
			let parCondicionNo = parametros.find(param => param.dataValues.id == 58);
			let parMonedaBs = parametros.find(param => param.dataValues.id == 68);
			let parPareentescoConviviente = parametros.find(param => param.dataValues.id == 48);
			let parPareentescoConyuge = parametros.find(param => param.dataValues.id == 49);
			let parPareentescoHijo = parametros.find(param => param.dataValues.id == 50);
			let parPareentescoHermano = parametros.find(param => param.dataValues.id == 52);
			let parPareentescoMadre = parametros.find(param => param.dataValues.id == 53);
			let parPareentescoPadre = parametros.find(param => param.dataValues.id == 55);
			let parPareentescoOtroParentesco = parametros.find(param => param.dataValues.id == 56);
			let parBeneficiarioActivo = parametros.find(param => param.dataValues.id == 83);
			let parBeneficiarioOriginal = parametros.find(param => param.dataValues.id == 86);

			for (let i = 0 ; i < datos.length ; i++) {
				let dato = datos[i].dataValues;
				parTipoDocumento = parTipoDocumentos.find(param => param.dataValues.parametro_cod.toUpperCase() == dato.tipo_documento_ID.toUpperCase());
				tipoDocumento = parTipoDocumento.id;
				let fechaEmision = new Date(dato.fecha_emision);
				let fechaEmisionStr = util.minTwoDigits(fechaEmision.getDate())+'/'+util.minTwoDigits(fechaEmision.getMonth()+1)+'/'+fechaEmision.getFullYear();
				let fechaVigenciaInicioStr = fechaEmisionStr
				let fechaVigenciaFinStr = fechas.addMeses(fechaEmisionStr,12);
				let [dayIni,monthIni,yearIni] = fechaVigenciaInicioStr.split('/');
				let [dayFin,monthFin,yearFin] = fechaVigenciaFinStr.split('/');
				let fechaVigenciaInicio = new Date(yearIni,parseInt(monthIni)-1,dayIni);
				let fechaVigenciaFin = new Date(yearFin,parseInt(monthFin)-1,dayFin);
				let fechaFinVigencia = new Date(dato.fecha_emision_real);
				let fechaFinVigenciaToSave = (fechaFinVigencia.getFullYear()+1) + '/' + (fechaFinVigencia.getMonth() + 1) + '/' + fechaFinVigencia.getDate() + ' ' + fechaFinVigencia.getHours() + ':' + fechaFinVigencia.getMinutes() + ':' + fechaFinVigencia.getSeconds();
				if (dato.ci && dato.ci != 'NULL') {
					let [docId,docIdExtTxt] = dato.ci.split(' ');
					let [docIdBen,docIdExtTxtBen] = dato.ci_beneficiario ? dato.ci_beneficiario.split(' ') : ['',''];
					let docIdExt, parDocIdExt, docIdExtBen, parDocIdExtBen;
					if (docIdExtTxt) {
						switch (docIdExtTxt) {
							case 'PT':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxt);
								break;
						}
					}
					if (docIdExtTxtBen) {
						switch (docIdExtTxtBen) {
							case 'PT':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxtBen);
								break;
						}
					}

					docIdExt = parDocIdExt && parDocIdExt.dataValues ? parDocIdExt.dataValues.parametro_cod : null;
					docIdExtBen = parDocIdExtBen && parDocIdExtBen.dataValues ? parDocIdExtBen.dataValues.parametro_cod : null;

					if (docIdExt) {

						let adicionadoPor = dato.cod_usuario_colibri ? dato.cod_usuario_colibri : 583;
						let parTipoDocumento = parTipoDocumentos.find(param => param.dataValues.parametro_cod.toUpperCase() == dato.tipo_documento_ID.toUpperCase());
						let tipoDocumento = parTipoDocumento ? parTipoDocumento.id : 'CE';

						let parSexoId, polizaId, parEstado, modalidadPago, parSucursal, parAgencia, cargo, nroCuotas, tipoDoc, parentescoId, parentescoDesc;

						if(dato.genero == 'varon') {
							parSexoId = parMasculino.id;
						} else if(dato.genero == 'mujer') {
							parSexoId = parFemenino.id;
						}
						if(dato.producto == 'ECOPROTECCION') {
							polizaId = polizaEcoProteccion.id;
						} else if (dato.producto == 'ECOVIDA') {
							polizaId = polizaEcoVida.id;
						}
						if(dato.emitido == 'SI') {
							parEstado = parEstadoEmitido.id;
						}
						if (dato.fecha_de_ingreso) {
							fechaIngreso = dato.fecha_de_ingreso;
						}
						if(dato.modalidad_pago == 'MENSUAL') {
							modalidadPago = parModalidadPagoMensual.id
						} else if (dato.modalidad_pago == 'ANUAL') {
							modalidadPago = parModalidadPagoAnual.id
						}
						cargo = 'ENCARGADO DE PLATAFORMA';

						if (dato.sucursal) {
							parSucursal = parSucursales.find(param => param.dataValues.parametro_cod == dato.COD_SUCURSAL);
						}
						if(dato.desc_agencia) {
							parAgencia = parAgencias.find(param => param.dataValues.parametro_cod == dato.COD_AGENCIA || dato.desc_agencia.includes(param.dataValues.parametro_descripcion));
						}
						if (!parAgencia) {
							dato.desc_agencia = dato.desc_agencia.removeAccents().toUpperCase();
							parAgencia = parAgencias.find(param => param.dataValues.parametro_descripcion.removeAccents().toUpperCase().includes(dato.desc_agencia));
						}
						if (parAgencia && !parSucursal) {
							parSucursal = parSucursales.find(param => param.id == parAgencia.dataValues.id_padre);
						}
						if (dato.tipo_documento_ID) {
							tipoDoc = dato.tipo_documento_ID.toUpperCase();
						}
						if (dato.parentesco.includes('convivi') || dato.parentesco.includes('convivi'.toUpperCase())) {
							parentescoId = parPareentescoConviviente.id;
						} else if (dato.parentesco.includes('conyugue') || dato.parentesco.includes('conyugue'.toUpperCase())){
							parentescoId = parPareentescoConyuge.id;
						} else if (dato.parentesco.includes('hij') || dato.parentesco.includes('hij'.toUpperCase())){
							parentescoId = parPareentescoHijo.id;
						} else if (dato.parentesco.includes('herman') || dato.parentesco.includes('herman'.toUpperCase())){
							parentescoId = parPareentescoHermano.id;
						} else if (dato.parentesco.includes('madre') || dato.parentesco.includes('madre'.toUpperCase())){
							parentescoId = parPareentescoMadre.id;
						} else if (dato.parentesco.includes('padre') || dato.parentesco.includes('padre'.toUpperCase())){
							parentescoId = parPareentescoPadre.id;
						} else {
							parentescoId = parPareentescoOtroParentesco.id;
							parentescoDesc = dato.parentesco;
						}
						nroCuotas = modalidadPago == parModalidadPagoMensual.id ? '12' : modalidadPago == parModalidadPagoAnual ? '1' : '';

						if(parAgencia && parSucursal && adicionadoPor) {

							let objetosPoliza = await modelos.objeto.findAll({where: {id_poliza: polizaId}});
							let objetoXAtributos = await modelos.objeto_x_atributo.findAll(
								{
									where:{id_objeto: objetosPoliza[0].dataValues.id},
									include: { model: modelos.atributo }
								});
							let documentos = await modelos.documento.findAll({where: {id_poliza: polizaId}});
							let newPersona = {
								persona_doc_id: docId,
								persona_doc_id_ext: docIdExt,
								persona_primer_apellido: dato.apellido_paterno,
								persona_segundo_apellido: dato.apellido_materno,
								persona_primer_nombre: dato.nombres,
								persona_direccion_domicilio: dato.dir_domicilio,
								persona_fecha_nacimiento: dato.fecha_nac,
								persona_celular: dato.tel_movil,
								persona_telefono_domicilio: dato.tel_domicilio,
								par_tipo_documento_id: tipoDocumento,
								par_sexo_id: parSexoId,
								createdAt: fechaIngreso,
								updatedAt: createdAt,
								adicionada_por: adicionadoPor,
								modificada_por: adicionadoPor
							};
							let oldPersona = await modelos.persona.findOne({where:{
									persona_doc_id: docId,
									persona_doc_id_ext: docIdExt,
									persona_primer_apellido: dato.apellido_paterno,
									persona_segundo_apellido: dato.apellido_materno,
									persona_primer_nombre: dato.nombres,
								}});
							let persona;
							if (!oldPersona) {
								persona = await modelos.persona.create(newPersona);
								persona = persona.dataValues;
							} else {
								persona = await modelos.persona.update(newPersona,{where:{id:oldPersona.id}});
								persona = await modelos.persona.findOne({where:{id:oldPersona.id}});
								persona = persona.dataValues;
							}

							let newEntidad = {
								id_persona:persona.id,
								tipo_entidad:parPersonaNatural.id,
								adicionada_por: adicionadoPor,
								modificada_por: adicionadoPor,
								createdAt: fechaIngreso,
								updatedAt: createdAt
							};

							let entidad;

							entidad = await modelos.entidad.create(newEntidad);
							entidad = entidad.dataValues;

							let newInstanciaPoliza = {
								id_poliza: polizaId,
								id_estado: parEstado,
								fecha_registro: fechaIngreso,
								adicionada_por: adicionadoPor,
								modificada_por: adicionadoPor,
								createdAt: fechaIngreso,
								updatedAt: createdAt,
							};

							let instanciaPoliza;

							instanciaPoliza = await modelos.instancia_poliza.create(newInstanciaPoliza);
							instanciaPoliza = instanciaPoliza.dataValues;
							let updatedIntanciaPoliza = await modelos.instancia_poliza.update({id_instancia_renovada:instanciaPoliza.id}, {where:{id:instanciaPoliza.id}});

							let newAsegurado = {
								id_entidad: entidad.id,
								id_instancia_poliza: instanciaPoliza.id,
								adicionado_por: adicionadoPor,
								modificado_por: adicionadoPor,
								createdAt:  fechaIngreso,
								updatedAt: createdAt
							};

							let asegurado;

							asegurado = await modelos.asegurado.create(newAsegurado);
							asegurado = asegurado.dataValues;


							let atributosInstanciaPoliza = [];
							for (let j = 0 ; j < objetoXAtributos.length ; j++) {
								let objetoXAtributo = objetoXAtributos[j].dataValues;
								let valor, tipoError;
								switch (objetoXAtributo.atributo.id) {
									case '3': valor = dato.estado_civil; tipoError = 'El titular no tiene registrado su estado civil'; break;
									case '4': valor = ''; tipoError = 'El titular no tiene registrado su e-mail'; break;
									case '5': valor = dato.nro_cuenta_para_debito; tipoError = 'El titular no tiene registrado un numero de cuenta'; break;
									case '10': valor = dato.codigo_de_agenda; tipoError = 'El titular no tiene registrado un codigo de agenda'; break;
									case '12': valor = parCondicionNo.id; tipoError = 'El titular no tiene registrado si el debito sera automatico'; break;
									case '14': valor = ''; tipoError = 'El titular no tiene registrado un numero CAEDEC'; break;
									case '16': valor = ''; tipoError = 'El titular no tiene registrado una localidad'; break;
									case '17': valor = ''; tipoError = 'El titular no tiene registrado un departamento'; break;
									case '18': valor = ''; tipoError = 'El titular no tiene registrado un codigo de sucursal'; break;
									case '19': valor = tipoDoc; tipoError = 'El titular no tiene registrado el tipo de documento utilizado'; break;
									case '20': valor = ''; tipoError = 'El titular no tiene registrado un codigo de manejo'; break;
									case '21': valor = parMonedaBs.dataValues.parametro_cod; tipoError = 'El titular no tiene registrado un tipo de moneda'; break;
									case '22': valor = ''; tipoError = 'El titular no tiene registrado una descripción en caedec'; break;
									case '28': valor = modalidadPago; tipoError = 'El titular no tiene registrado una modalidad de pago'; break;
									case '29': valor = ''; tipoError = 'El titular no tiene registrado una zona'; break;
									case '30': valor = ''; tipoError = 'El titular no tiene registrado un Nro de direccion'; break;
									case '37': valor = ''; tipoError = 'El titular no tiene registrado una ocupación'; break;
									case '60': valor = parSucursal.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una sucursal'; break;
									case '59': valor = parAgencia.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una agencia'; break;
									case '61': valor = dato.prima; tipoError = 'El titular no tiene registrado el monto de la cuota'; break;
									case '63': valor = ''; tipoError = 'El titular no tiene registrada una ciudad de nacimiento'; break;
									case '64': valor = ''; tipoError = 'La transaccion no tiene registrado un nro de transacción'; break;
									case '65': valor = ''; tipoError = 'La transaccion no tiene registrado una fecha de transacción'; break;
									case '66': valor = dato.prima; tipoError = 'La transaccion no tiene registrado un importe'; break;
									case '67': valor = ''; tipoError = 'La transaccion no tiene registrado un tipo de moneda'; break;
									case '69': valor = ''; tipoError = 'La transaccion no tiene registrado un detalle de transacción'; break;
									case '70': valor = cargo; tipoError = 'El usuario del banco no tiene registrado un cargo'; break;
									case '71': valor = ''; tipoError = 'El titular no tiene un cargo publico y/o politico jerarquico'; break;
									case '72': valor = ''; tipoError = 'El titular no tiene registrado un cargo/entidad pep'; break;
									case '73': valor = ''; tipoError = 'El titular no tiene registrado un periodo del cargo publico pep'; break;
									case '74': valor = ''; tipoError = 'El titular no tiene una  direccion laboral'; break;
									case '75': valor = ''; tipoError = 'El titular no tiene un tipo de cuenta'; break;
									case '76': valor = nroCuotas; tipoError = 'El titular no tiene registrado un Nro. de cuotas'; break;
									case '77': valor = ''; tipoError = 'El titular no tiene registrado la descripción de su ocupación'; break;
								}

								let newAtributoInstanciaPoliza = {
									id_instancia_poliza: instanciaPoliza.id,
									id_objeto_x_atributo: objetoXAtributo.id,
									valor: valor,
									tipo_error: tipoError,
									adicionado_por: adicionadoPor,
									modificado_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt
								};
								let oldAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{
										id_instancia_poliza: instanciaPoliza.id,
										id_objeto_x_atributo: objetoXAtributo.id,
										valor: valor,
									}});
								let atributoInstanciaPoliza;
								if (!oldAtributoInstanciaPoliza) {
									atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.create(newAtributoInstanciaPoliza);
									atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
								} else {
									atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.update(newAtributoInstanciaPoliza,{where:{id:oldAtributoInstanciaPoliza.id}});
									atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id:oldAtributoInstanciaPoliza.id}});
									atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
								}
								atributosInstanciaPoliza.push(atributoInstanciaPoliza);
							}

							let instanciaDocumentos = [];
							for (let k = 0 ; k < documentos.length ; k++) {
								let documento = documentos[k].dataValues;
								let newInstanciaDocumeento = {
									id_instancia_poliza: instanciaPoliza.id,
									id_documento: documento.id,
									id_documento_version: documento.id_documento_version,
									nro_documento: documento.id_tipo_documento == 279 ? dato.nro_solicitud : documento.id_tipo_documento == 280 ? dato.nro_comprobante : documento.id_tipo_documento == 281 ? dato.nro_certificado : dato.nro_certificado,
									fecha_emision: fechaEmision,
									fecha_inicio_vigencia: fechaVigenciaInicio,
									fecha_fin_vigencia: fechaVigenciaFin,
									asegurado_doc_id: persona.persona_doc_id,
									asegurado_nombre1: persona.persona_primer_nombre,
									asegurado_nombre2: persona.persona_segundo_nombre,
									asegurado_apellido1: persona.persona_segundo_nombre,
									asegurado_apellido2: persona.persona_segundo_apellido,
									tipo_persona: entidad.id,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt
								};
								let oldInstanciaDocumento = await modelos.instancia_documento.findOne({where:{
										id_instancia_poliza: instanciaPoliza.id,
										id_documento: documento.id,
										nro_documento: dato.nro_certificado,
										fecha_emision: dato.fecha_emision_real,
										createdAt: fechaIngreso,
										updatedAt: createdAt
									}});
								let instanciaDocumento;
								if (!oldInstanciaDocumento) {
									instanciaDocumento = await modelos.instancia_documento.create(newInstanciaDocumeento);
									instanciaDocumento = instanciaDocumento.dataValues;
								} else {
									instanciaDocumento =  await modelos.instancia_documento.update(newInstanciaDocumeento,{where:{id:oldInstanciaDocumento.id}});
									instanciaDocumento = await modelos.instancia_documento.findOne({where:{id:oldInstanciaDocumento.id}});
									instanciaDocumento = instanciaDocumento.dataValues;
								}

								instanciaDocumentos.push(instanciaDocumento);
							}

							let newPersonaBeneficiario = {
								persona_primer_apellido:dato.apellido_paterno_beneficiario,
								persona_segundo_apellido:dato.apellido_materno_beneficiario,
								persona_primer_nombre:dato.nombres_beneficiario,
								createdAt:fechaIngreso,
								updatedAt:createdAt,
								adicionada_por:adicionadoPor,
								modificada_por:adicionadoPor
							}

							let personaBeneficiario;

							personaBeneficiario = await modelos.persona.create(newPersonaBeneficiario);
							personaBeneficiario = personaBeneficiario.dataValues;

							let newEntidadBeneficiario = {
								id_persona: personaBeneficiario.id,
								tipo_entidad:12,
								adicionada_por:adicionadoPor,
								modificada_por:adicionadoPor,
								createdAt:fechaIngreso,
								updatedAt:createdAt
							}

							let entidadBeneficiario;

							entidadBeneficiario = await modelos.entidad.create(newEntidadBeneficiario);
							entidadBeneficiario = entidadBeneficiario.dataValues;

							let newBeneficiario = {
								id_asegurado: asegurado.id,
								id_beneficiario: entidadBeneficiario.id,
								id_parentesco: parentescoId,
								porcentaje: dato.porcentaje,
								adicionado_por: adicionadoPor,
								modificado_por: adicionadoPor,
								createdAt: fechaIngreso,
								updatedAt: createdAt,
								descripcion_otro: parentescoDesc,
								estado: parBeneficiarioActivo.id,
								tipo: parBeneficiarioOriginal.id,
								fecha_declaracion: fechaIngreso
							};
							let oldBeneficiario = await modelos.beneficiario.findOne({where:{
									id_asegurado: asegurado.id,
									id_beneficiario: entidad.id,
									id_parentesco: parentescoId,
									porcentaje: dato.porcentaje,
									createdAt: fechaIngreso,
									updatedAt: createdAt
								}});

							let beneficiario;
							if (!oldBeneficiario) {
								beneficiario = await modelos.beneficiario.create(newBeneficiario);
								beneficiario = beneficiario.dataValues;
							} else {
								beneficiario = await modelos.beneficiario.update(newBeneficiario,{where:{id:oldBeneficiario.id}});
								beneficiario = await modelos.beneficiario.findOne({where:{id:oldBeneficiario.id}});
								beneficiario = beneficiario.dataValues;
							}
							let tmpEcoproteccionComplementoBeneficiarioV1 = await modelos.mig_ecoproteccion.update({
								id_instancia_poliza:instanciaPoliza.id.toString(),
								id_persona_beneficiario:personaBeneficiario.id
							}, {where:{id:dato.id}});
							response.data.push({
								persona: persona,
								entidad: entidad,
								asegurado: asegurado,
								instanciaPoliza: instanciaPoliza,
								atributosInstanciaPoliza: atributosInstanciaPoliza,
								beneficiario: beneficiario,
								instanciaDocumentos: instanciaDocumentos
							});
						} else {
							response.errors.push({
								params: {
									sucursal:parSucursal ? parSucursal.dataValues : '',
									agencia:parAgencia ? parAgencia.dataValues : ''
								},
								dato: dato.dataValues
							});
						}
					}
				}
			}
			return response;
		} catch (e) {
			console.log(e)
		}
	}

	static async migraEcoMedicv(req, res) {
		try {
			let response = { status: 'OK', message: '', data: [], errors:[] };
			let parametros = await modelos.parametro.findAll({where: {diccionario_id: [1, 4, 7, 11, 18, 20, 21, 22, 25, 29, 31, 38, 40]}});
			let hoy = new Date();
			let createdAt = hoy;
			let year = hoy.getFullYear(), month = hoy.getMonth(), day = hoy.getDate();
			let polizas = await modelos.poliza.findAll();
			let fechaIngreso;
			let datos = await modelos.mig_ecomedicv.findAll({where:{$and:[
						{id_instancia_poliza:null},
						{ci:{$not:null}},
						{ci:{$not:'NULL'}}
					]}});

			// parametros

			let adicionadoPor;
			let parTipoDocumento;
			let tipoDocumento;
			let parTipoDocumentos = parametros.filter(param => param.dataValues.diccionario_id == 1);
			let parMasculino = parametros.find(param => param.dataValues.id == 6);
			let parFemenino = parametros.find(param => param.dataValues.id == 7);
			let parPersonaNatural = parametros.find(param => param.dataValues.id == 12);
			let parPersonaJuridica = parametros.find(param => param.dataValues.id == 13);
			let polizaEcoProteccion = polizas.find(param => param.dataValues.id == 7);
			let polizaEcoVida = polizas.find(param => param.dataValues.id == 6);
			let polizaEcoMedicv = polizas.find(param => param.dataValues.id == 8);
			let parEstadoEmitido = parametros.find(param => param.dataValues.id == 59);
			let parEstadoIniciado =  parametros.find(param => param.dataValues.id == 24);
			let parEstadoSolicitado = parametros.find(param => param.dataValues.id == 81);
			let parModalidadPagoMensual = parametros.find(param => param.dataValues.id == 90);
			let parModalidadPagoAnual = parametros.find(param => param.dataValues.id == 93);
			let parSucursales = parametros.filter(param => param.dataValues.diccionario_id == 38);
			let parAgencias = parametros.filter(param => param.dataValues.diccionario_id == 40);
			let parCondiciones = parametros.filter(param => param.dataValues.diccionario_id == 21);
			let parCondicionSi = parametros.find(param => param.dataValues.id == 57);
			let parCondicionNo = parametros.find(param => param.dataValues.id == 58);
			let parMonedaBs = parametros.find(param => param.dataValues.id == 68);
			let parPareentescoConviviente = parametros.find(param => param.dataValues.id == 48);
			let parPareentescoConyuge = parametros.find(param => param.dataValues.id == 49);
			let parPareentescoHijo = parametros.find(param => param.dataValues.id == 50);
			let parPareentescoHermano = parametros.find(param => param.dataValues.id == 52);
			let parPareentescoMadre = parametros.find(param => param.dataValues.id == 53);
			let parPareentescoPadre = parametros.find(param => param.dataValues.id == 55);
			let parPareentescoOtroParentesco = parametros.find(param => param.dataValues.id == 56);
			let parBeneficiarioActivo = parametros.find(param => param.dataValues.id == 83);
			let parBeneficiarioOriginal = parametros.find(param => param.dataValues.id == 86);

			for (let i = 0 ; i < datos.length ; i++) {
				let dato = datos[i].dataValues;
				parTipoDocumento = parTipoDocumentos.find(param => param.dataValues.parametro_cod.toUpperCase() == dato.tipo_documento_ID.toUpperCase());
				tipoDocumento = parTipoDocumento.id;
				let fechaEmision = new Date(dato.fecha_emision);
				let fechaEmisionStr = util.minTwoDigits(fechaEmision.getDate())+'/'+util.minTwoDigits(fechaEmision.getMonth()+1)+'/'+fechaEmision.getFullYear();
				let fechaVigenciaInicioStr = fechaEmisionStr
				let fechaVigenciaFinStr = fechas.addMeses(fechaEmisionStr,12);
				let [dayIni,monthIni,yearIni] = fechaVigenciaInicioStr.split('/');
				let [dayFin,monthFin,yearFin] = fechaVigenciaFinStr.split('/');
				let fechaVigenciaInicio = new Date(yearIni,parseInt(monthIni)-1,dayIni);
				let fechaVigenciaFin = new Date(yearFin,parseInt(monthFin)-1,dayFin);
				let fechaFinVigencia = new Date(dato.fecha_emision_real);
				let fechaFinVigenciaToSave = (fechaFinVigencia.getFullYear()+1) + '/' + (fechaFinVigencia.getMonth() + 1) + '/' + fechaFinVigencia.getDate() + ' ' + fechaFinVigencia.getHours() + ':' + fechaFinVigencia.getMinutes() + ':' + fechaFinVigencia.getSeconds();
				if (dato.ci && dato.ci != 'NULL') {
					let [docId,docIdExtTxt] = dato.ci.split(' ');
					let [docIdBen,docIdExtTxtBen] = dato.ci_beneficiario ? dato.ci_beneficiario.split(' ') : ['',''];
					let docIdExt, parDocIdExt, docIdExtBen, parDocIdExtBen;
					if (docIdExtTxt) {
						switch (docIdExtTxt) {
							case 'PT':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExt = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxt);
								break;
						}
						if (!parDocIdExt) {
							console.log('parDocIdExt',parDocIdExt);
							break;
						}
					}
					if (docIdExtTxtBen) {
						switch (docIdExtTxtBen) {
							case 'PT':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PO');
								break;
							case 'PE':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PE');
								break;
							case 'PJ':
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == 'PJ');
								break;
							default:
								parDocIdExtBen = parametros.find(param => param.dataValues.parametro_abreviacion == docIdExtTxtBen);
								break;
						}
						if (!parDocIdExtBen) {
							console.log('parDocIdExt',parDocIdExtBen);
							break;
						}
					}

					docIdExt = parDocIdExt && parDocIdExt.dataValues ? parDocIdExt.dataValues.parametro_cod : null;
					docIdExtBen = parDocIdExtBen && parDocIdExtBen.dataValues ? parDocIdExtBen.dataValues.parametro_cod : null;

					if (docIdExt) {

						let adicionadoPor = dato.cod_usuario_colibri ? dato.cod_usuario_colibri : 583;
						let parTipoDocumento = parTipoDocumentos.find(param => param.dataValues.parametro_cod.toUpperCase() == dato.tipo_documento_ID.toUpperCase());
						let tipoDocumento = parTipoDocumento ? parTipoDocumento.id : 'CE';

						let parSexoId, polizaId, parEstado, modalidadPago, parSucursal, parAgencia, cargo, nroCuotas, tipoDoc, parentescoId, parentescoDesc;

						if(dato.genero == 'varon') {
							parSexoId = parMasculino.id;
						} else if(dato.genero == 'mujer') {
							parSexoId = parFemenino.id;
						}
						if(dato.producto == 'ECOPROTECCION') {
							polizaId = polizaEcoProteccion.id;
						} else if (dato.producto == 'ECOVIDA') {
							polizaId = polizaEcoVida.id;
						} else if (dato.producto == 'ECOMEDICV') {
							polizaId = polizaEcoMedicv.id;
						}
						if(dato.emitido == 'SI') {
							parEstado = parEstadoEmitido.id;
						}
						if (dato.fecha_de_ingreso) {
							fechaIngreso = dato.fecha_de_ingreso;
						}
						if(dato.modalidad_pago == 'MENSUAL') {
							modalidadPago = parModalidadPagoMensual.id
						} else if (dato.modalidad_pago == 'ANUAL') {
							modalidadPago = parModalidadPagoAnual.id
						}
						cargo = 'ENCARGADO DE PLATAFORMA';

						if (dato.sucursal) {
							parSucursal = parSucursales.find(param => param.dataValues.parametro_cod == dato.COD_SUCURSAL);
						}
						if(dato.desc_agencia) {
							parAgencia = parAgencias.find(param => param.dataValues.parametro_cod == dato.COD_AGENCIA || dato.desc_agencia.includes(param.dataValues.parametro_descripcion));
						}
						if (!parAgencia) {
							dato.desc_agencia = dato.desc_agencia.removeAccents().toUpperCase();
							parAgencia = parAgencias.find(param => param.dataValues.parametro_descripcion.removeAccents().toUpperCase().includes(dato.desc_agencia));
						}
						if (parAgencia && !parSucursal) {
							parSucursal = parSucursales.find(param => param.id == parAgencia.dataValues.id_padre);
						}
						if (dato.tipo_documento_ID) {
							tipoDoc = dato.tipo_documento_ID.toUpperCase();
						}
						if (dato.parentesco.includes('convivi') || dato.parentesco.includes('convivi'.toUpperCase())) {
							parentescoId = parPareentescoConviviente.id;
						} else if (dato.parentesco.includes('conyugue') || dato.parentesco.includes('conyugue'.toUpperCase())){
							parentescoId = parPareentescoConyuge.id;
						} else if (dato.parentesco.includes('hij') || dato.parentesco.includes('hij'.toUpperCase())){
							parentescoId = parPareentescoHijo.id;
						} else if (dato.parentesco.includes('herman') || dato.parentesco.includes('herman'.toUpperCase())){
							parentescoId = parPareentescoHermano.id;
						} else if (dato.parentesco.includes('madre') || dato.parentesco.includes('madre'.toUpperCase())){
							parentescoId = parPareentescoMadre.id;
						} else if (dato.parentesco.includes('padre') || dato.parentesco.includes('padre'.toUpperCase())){
							parentescoId = parPareentescoPadre.id;
						} else {
							parentescoId = parPareentescoOtroParentesco.id;
							parentescoDesc = dato.parentesco;
						}
						nroCuotas = modalidadPago == parModalidadPagoMensual.id ? '12' : modalidadPago == parModalidadPagoAnual ? '1' : '';

						if(parAgencia && parSucursal && adicionadoPor) {

							let objetosPoliza = await modelos.objeto.findAll({where: {id_poliza: polizaId}});
							let objetoXAtributos = await modelos.objeto_x_atributo.findAll(
								{
									where:{id_objeto: objetosPoliza[0].dataValues.id},
									include: { model: modelos.atributo }
								});
							let documentos = await modelos.documento.findAll({where: {id_poliza: polizaId}});
							let newPersona = {
								persona_doc_id: docId,
								persona_doc_id_ext: docIdExt,
								persona_primer_apellido: dato.apellido_paterno,
								persona_segundo_apellido: dato.apellido_materno,
								persona_primer_nombre: dato.nombres,
								persona_direccion_domicilio: dato.dir_domicilio,
								persona_fecha_nacimiento: dato.fecha_nac,
								persona_celular: dato.tel_movil,
								persona_telefono_domicilio: dato.tel_domicilio,
								par_tipo_documento_id: tipoDocumento,
								par_sexo_id: parSexoId,
								createdAt: fechaIngreso,
								updatedAt: createdAt,
								adicionada_por: adicionadoPor,
								modificada_por: adicionadoPor
							};
							let oldPersona = await modelos.persona.findOne({where:{
									persona_doc_id: docId,
									persona_doc_id_ext: docIdExt,
									persona_primer_apellido: dato.apellido_paterno,
									persona_segundo_apellido: dato.apellido_materno,
									persona_primer_nombre: dato.nombres,
								}});
							let persona;
							if (!oldPersona) {
								persona = await modelos.persona.create(newPersona);
								persona = persona.dataValues;
							} else {
								persona = await modelos.persona.update(newPersona,{where:{id:oldPersona.id}});
								persona = await modelos.persona.findOne({where:{id:oldPersona.id}});
								persona = persona.dataValues;
							}

							let newEntidad = {
								id_persona:persona.id,
								tipo_entidad:parPersonaNatural.id,
								adicionada_por: adicionadoPor,
								modificada_por: adicionadoPor,
								createdAt: fechaIngreso,
								updatedAt: createdAt
							};

							let entidad;

							entidad = await modelos.entidad.create(newEntidad);
							entidad = entidad.dataValues;

							let newInstanciaPoliza = {
								id_poliza: polizaId,
								id_estado: parEstado,
								fecha_registro: fechaIngreso,
								adicionada_por: adicionadoPor,
								modificada_por: adicionadoPor,
								createdAt: fechaIngreso,
								updatedAt: createdAt,
							};

							let instanciaPoliza;

							instanciaPoliza = await modelos.instancia_poliza.create(newInstanciaPoliza);
							instanciaPoliza = instanciaPoliza.dataValues;
							let updatedIntanciaPoliza = await modelos.instancia_poliza.update({id_instancia_renovada:instanciaPoliza.id}, {where:{id:instanciaPoliza.id}});

							let newAsegurado = {
								id_entidad: entidad.id,
								id_instancia_poliza: instanciaPoliza.id,
								adicionado_por: adicionadoPor,
								modificado_por: adicionadoPor,
								createdAt:  fechaIngreso,
								updatedAt: createdAt
							};

							let asegurado;

							asegurado = await modelos.asegurado.create(newAsegurado);
							asegurado = asegurado.dataValues;


							let atributosInstanciaPoliza = [];
							for (let j = 0 ; j < objetoXAtributos.length ; j++) {
								let objetoXAtributo = objetoXAtributos[j].dataValues;
								let valor, tipoError;
								switch (objetoXAtributo.atributo.id) {
									case '3': valor = dato.estado_civil; tipoError = 'El titular no tiene registrado su estado civil'; break;
									case '4': valor = dato.att_Correo_electronico; tipoError = 'El titular no tiene registrado su e-mail'; break;
									case '5': valor = dato.nro_cuenta_para_debito; tipoError = 'El titular no tiene registrado un numero de cuenta'; break;
									case '10': valor = dato.codigo_de_agenda; tipoError = 'El titular no tiene registrado un codigo de agenda'; break;
									case '12': valor = dato.att_Debito_Automatico; tipoError = 'El titular no tiene registrado si el debito sera automatico'; break;
									case '14': valor = dato.att_Codigo_CAEDEC; tipoError = 'El titular no tiene registrado un numero CAEDEC'; break;
									case '16': valor = dato.att_Localidad; tipoError = 'El titular no tiene registrado una localidad'; break;
									case '17': valor = dato.att_Departamento; tipoError = 'El titular no tiene registrado un departamento'; break;
									case '18': valor = dato.att_Codigo_Sucursal; tipoError = 'El titular no tiene registrado un codigo de sucursal'; break;
									case '19': valor = tipoDoc; tipoError = 'El titular no tiene registrado el tipo de documento utilizado'; break;
									// case '20': valor = dato.att_; tipoError = 'El titular no tiene registrado un codigo de manejo'; break;
									case '21': valor = dato.att_Moneda; tipoError = 'El titular no tiene registrado un tipo de moneda'; break;
									case '22': valor = dato.att_Descripcion_CAEDEC; tipoError = 'El titular no tiene registrado una descripción en caedec'; break;
									case '28': valor = dato.att_Modalidad_Pago; tipoError = 'El titular no tiene registrado una modalidad de pago'; break;
									case '29': valor = dato.att_Zona; tipoError = 'El titular no tiene registrado una zona'; break;
									case '30': valor = dato.att_Nro_Direccion; tipoError = 'El titular no tiene registrado un Nro de direccion'; break;
									case '37': valor = dato.att_Ocupacion; tipoError = 'El titular no tiene registrado una ocupación'; break;
									case '60': valor = parSucursal.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una sucursal'; break;
									case '59': valor = parAgencia.dataValues.parametro_cod; tipoError = 'El usuario del banco no tiene registrado una agencia'; break;
									case '61': valor = dato.prima; tipoError = 'El titular no tiene registrado el monto de la cuota'; break;
									case '63': valor = dato.att_Ciudad_Nacimiento; tipoError = 'El titular no tiene registrada una ciudad de nacimiento'; break;
									case '64': valor = dato.att_Numero_transaccion; tipoError = 'La transaccion no tiene registrado un nro de transacción'; break;
									case '65': valor = dato.att_Fecha_transaccion; tipoError = 'La transaccion no tiene registrado una fecha de transacción'; break;
									case '66': valor = dato.att_Importe_transaccion; tipoError = 'La transaccion no tiene registrado un importe'; break;
									case '67': valor = dato.att_moneda_transaccion; tipoError = 'La transaccion no tiene registrado un tipo de moneda'; break;
									case '69': valor = dato.att_Detalle_transaccion; tipoError = 'La transaccion no tiene registrado un detalle de transacción'; break;
									case '70': valor = dato.att_Cargo_oficial; tipoError = 'El usuario del banco no tiene registrado un cargo'; break;
									// case '71': valor = ''; tipoError = 'El titular no tiene un cargo publico y/o politico jerarquico'; break;
									// case '72': valor = ''; tipoError = 'El titular no tiene registrado un cargo/entidad pep'; break;
									// case '73': valor = ''; tipoError = 'El titular no tiene registrado un periodo del cargo publico pep'; break;
									case '74': valor = dato.att_Direccion_laboral; tipoError = 'El titular no tiene una  direccion laboral'; break;
									// case '75': valor = ''; tipoError = 'El titular no tiene un tipo de cuenta'; break;
									case '76': valor = nroCuotas; tipoError = 'El titular no tiene registrado un Nro. de cuotas'; break;
									case '77': valor = dato.att_Ocupacion; tipoError = 'El titular no tiene registrado la descripción de su ocupación'; break;

									case '32': valor = dato.att_Nombre_Razon_Social; tipoError = 'El titular no tiene registrado un nombre o razon social'; break;
									case '33': valor = dato.att_Nit_Carnet; tipoError = 'El titular no tiene registrado un nit o carnet'; break;
									case '38': valor = dato.att_Plan; tipoError = 'El titular no tiene registrado un plan'; break;
									case '58': valor = dato.att_Plazo; tipoError = 'El titular no tiene registrado un plazo'; break;
									case '1': valor = dato.att_prima; tipoError = 'El titular no tiene registrado la prima'; break;
								}

								let newAtributoInstanciaPoliza = {
									id_instancia_poliza: instanciaPoliza.id,
									id_objeto_x_atributo: objetoXAtributo.id,
									valor: valor,
									tipo_error: tipoError,
									adicionado_por: adicionadoPor,
									modificado_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt
								};
								let oldAtributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{
										id_instancia_poliza: instanciaPoliza.id,
										id_objeto_x_atributo: objetoXAtributo.id,
										valor: valor,
									}});
								let atributoInstanciaPoliza;
								if (!oldAtributoInstanciaPoliza) {
									atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.create(newAtributoInstanciaPoliza);
									atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
								} else {
									atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.update(newAtributoInstanciaPoliza,{where:{id:oldAtributoInstanciaPoliza.id}});
									atributoInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id:oldAtributoInstanciaPoliza.id}});
									atributoInstanciaPoliza = atributoInstanciaPoliza.dataValues;
								}
								atributosInstanciaPoliza.push(atributoInstanciaPoliza);
							}

							let instanciaDocumentos = [];
							for (let k = 0 ; k < documentos.length ; k++) {
								let documento = documentos[k].dataValues;
								let newInstanciaDocumeento = {
									id_instancia_poliza: instanciaPoliza.id,
									id_documento: documento.id,
									id_documento_version: documento.id_documento_version,
									nro_documento: documento.id_tipo_documento == 279 ? dato.nro_solicitud : documento.id_tipo_documento == 280 ? dato.nro_comprobante : documento.id_tipo_documento == 281 ? dato.nro_certificado : dato.nro_certificado,
									fecha_emision: fechaEmision,
									fecha_inicio_vigencia: fechaVigenciaInicio,
									fecha_fin_vigencia: fechaVigenciaFin,
									asegurado_doc_id: persona.persona_doc_id,
									asegurado_nombre1: persona.persona_primer_nombre,
									asegurado_nombre2: persona.persona_segundo_nombre,
									asegurado_apellido1: persona.persona_segundo_nombre,
									asegurado_apellido2: persona.persona_segundo_apellido,
									tipo_persona: entidad.id,
									adicionada_por: adicionadoPor,
									modificada_por: adicionadoPor,
									createdAt: fechaIngreso,
									updatedAt: createdAt
								};
								let oldInstanciaDocumento = await modelos.instancia_documento.findOne({where:{
										id_instancia_poliza: instanciaPoliza.id,
										id_documento: documento.id,
										nro_documento: dato.nro_certificado,
										fecha_emision: dato.fecha_emision_real,
										createdAt: fechaIngreso,
										updatedAt: createdAt
									}});
								let instanciaDocumento;
								if (!oldInstanciaDocumento) {
									instanciaDocumento = await modelos.instancia_documento.create(newInstanciaDocumeento);
									instanciaDocumento = instanciaDocumento.dataValues;
								} else {
									instanciaDocumento =  await modelos.instancia_documento.update(newInstanciaDocumeento,{where:{id:oldInstanciaDocumento.id}});
									instanciaDocumento = await modelos.instancia_documento.findOne({where:{id:oldInstanciaDocumento.id}});
									instanciaDocumento = instanciaDocumento.dataValues;
								}

								instanciaDocumentos.push(instanciaDocumento);
							}

							let newPersonaBeneficiario = {
								persona_primer_apellido:dato.apellido_paterno_beneficiario,
								persona_segundo_apellido:dato.apellido_materno_beneficiario,
								persona_primer_nombre:dato.nombres_beneficiario,
								createdAt:fechaIngreso,
								updatedAt:createdAt,
								adicionada_por:adicionadoPor,
								modificada_por:adicionadoPor
							}

							let personaBeneficiario;

							personaBeneficiario = await modelos.persona.create(newPersonaBeneficiario);
							personaBeneficiario = personaBeneficiario.dataValues;

							let newEntidadBeneficiario = {
								id_persona: personaBeneficiario.id,
								tipo_entidad:12,
								adicionada_por:adicionadoPor,
								modificada_por:adicionadoPor,
								createdAt:fechaIngreso,
								updatedAt:createdAt
							}

							let entidadBeneficiario;

							entidadBeneficiario = await modelos.entidad.create(newEntidadBeneficiario);
							entidadBeneficiario = entidadBeneficiario.dataValues;

							let newBeneficiario = {
								id_asegurado: asegurado.id,
								id_beneficiario: entidadBeneficiario.id,
								id_parentesco: parentescoId,
								porcentaje: dato.porcentaje,
								adicionado_por: adicionadoPor,
								modificado_por: adicionadoPor,
								createdAt: fechaIngreso,
								updatedAt: createdAt,
								descripcion_otro: parentescoDesc,
								estado: parBeneficiarioActivo.id,
								tipo: parBeneficiarioOriginal.id,
								fecha_declaracion: fechaIngreso
							};
							let oldBeneficiario = await modelos.beneficiario.findOne({where:{
									id_asegurado: asegurado.id,
									id_beneficiario: entidad.id,
									id_parentesco: parentescoId,
									porcentaje: dato.porcentaje,
									createdAt: fechaIngreso,
									updatedAt: createdAt
								}});

							let beneficiario;
							if (!oldBeneficiario) {
								beneficiario = await modelos.beneficiario.create(newBeneficiario);
								beneficiario = beneficiario.dataValues;
							} else {
								beneficiario = await modelos.beneficiario.update(newBeneficiario,{where:{id:oldBeneficiario.id}});
								beneficiario = await modelos.beneficiario.findOne({where:{id:oldBeneficiario.id}});
								beneficiario = beneficiario.dataValues;
							}
							await modelos.mig_ecomedicv.update({
								id_instancia_poliza:instanciaPoliza.id.toString(),
								id_persona_beneficiario:personaBeneficiario.id
							}, {where:{id:dato.id}});
							response.data.push({
								persona: persona,
								entidad: entidad,
								asegurado: asegurado,
								instanciaPoliza: instanciaPoliza,
								atributosInstanciaPoliza: atributosInstanciaPoliza,
								beneficiario: beneficiario,
								instanciaDocumentos: instanciaDocumentos
							});
						} else {
							response.errors.push({
								params: {
									sucursal:parSucursal ? parSucursal.dataValues : '',
									agencia:parAgencia ? parAgencia.dataValues : ''
								},
								dato: dato.dataValues
							});
						}
					}
				}
			}
			return response;
		} catch (e) {
			console.log(e)
		}
	}
}

module.exports = MigracionService;
