const modelos = require('../modelos');
const moment = require('moment');
const {loggerPlanPago} = require('../modulos/winston');

class PlanPagoService {
	static async getPlanPagoProcEcoproteccion(fecha, id_poliza, dias_habil_siguiente, id_calendario) {
		try {
			return await modelos.sequelize.query(`EXEC sp_genera_archDebt_Ecoproteccion '${fecha}',${id_poliza},${dias_habil_siguiente},${id_calendario}`)
		} catch (e) {
			console.log(e);
		}
	}

	static async createEcoProteccionPlanPagos() {
		try {
			let id_poliza = 7;
			let planesPagos = [];
			let objetoXAtributos = await modelos.objeto_x_atributo.findAll({
				include: [
					{model: modelos.atributo},
					{model: modelos.objeto, where: {id_poliza:id_poliza}},
				]
			});
			let objetoXAtributoModoPago = objetoXAtributos.find(param => parseInt(param.dataValues.id_atributo) == 28);
			let objetoXAtributoDebito = objetoXAtributos.find(param => parseInt(param.dataValues.id_atributo) == 12);
			if (objetoXAtributoModoPago && objetoXAtributoDebito) {
				let aInstancias = await modelos.instancia_poliza.findAll({
					include: [
						{
							model: modelos.atributo_instancia_poliza, as:'aip_modo_pago', where: {id_objeto_x_atributo: objetoXAtributoModoPago.dataValues.id}, required: true,
							include: {model: modelos.parametro, as:'par_modo_pago', where: {diccionario_id: 31}, required: true}
						},
						{
							model: modelos.atributo_instancia_poliza, as:'aip_debito', where: {id_objeto_x_atributo: objetoXAtributoDebito.dataValues.id}, required: true,
							include: {model: modelos.parametro, as:'par_debito', where: {diccionario_id: 21}, required: true}
						},
						{model: modelos.planPago, required: false},
					],
					where: {id_poliza: id_poliza, id_estado:[59,65], '$plan_pagos.id$': null, '$aip_debito.par_debito.id$': 58}
				});
				if (aInstancias && aInstancias.length) {
					for (let i = 0; i < aInstancias.length; i++) {
						let instancia = aInstancias[i];
						let newPlanPago = {
							id_instancia_poliza:parseInt(instancia.id),
							total_prima: instancia.dataValues.aip_modo_pago.valor == 90 ? 192 : instancia.dataValues.aip_modo_pago.valor == 93 ? 176 : '',
							interes:0,
							plazo_anos:1,
							periodicidad_anual:instancia.dataValues.aip_modo_pago.valor == 90 ? 12 : instancia.dataValues.aip_modo_pago.valor == 93 ? 1 : '',
							prepagable_postpagable:1,
							fecha_inicio:instancia.dataValues.createdAt,
							id_moneda:68,
							adicionado_por:instancia.dataValues.adicionada_por,
							modificado_por:instancia.dataValues.adicionada_por,
							createdAt:instancia.dataValues.createdAt,
							updatedAt:instancia.dataValues.createdAt,
							id_estado:null
						};
						let createdPlanPago = await modelos.planPago.create(newPlanPago);
						planesPagos.push(createdPlanPago.dataValues);
					}
					loggerPlanPago.info('Planes de pago creados: ' + new Date().toString(), planesPagos);
				}
			}
			return planesPagos;
		} catch (e) {
			console.log(e);
		}
	}

	static async createEcoProteccionPlanPagosDetalle() {
		try {
			let id_poliza = 7;
			let planesPagos = [];
			let objetoXAtributos = await modelos.objeto_x_atributo.findAll({
				include: [
					{model: modelos.atributo},
					{model: modelos.objeto, where: {id_poliza:id_poliza}},
				]
			});
			let objetoXAtributoModoPago = objetoXAtributos.find(param => parseInt(param.dataValues.id_atributo) == 28);
			let objetoXAtributoDebito = objetoXAtributos.find(param => parseInt(param.dataValues.id_atributo) == 12);
			if (objetoXAtributoModoPago && objetoXAtributoDebito) {
				let aInstancias = await modelos.instancia_poliza.findAll({
					include: [
						{
							model: modelos.atributo_instancia_poliza, as:'aip_modo_pago', where: {id_objeto_x_atributo: objetoXAtributoModoPago.dataValues.id}, required: true,
							include: {model: modelos.parametro, as:'par_modo_pago', where: {diccionario_id: 31}, required: true}
						},
						{
							model: modelos.atributo_instancia_poliza, as:'aip_debito', where: {id_objeto_x_atributo: objetoXAtributoDebito.dataValues.id}, required: true,
							include: {model: modelos.parametro, as:'par_debito', where: {diccionario_id: 21}, required: true}
						},
						{model: modelos.plan_pago_detalle, required: false},
					],
					where: {id_poliza: id_poliza, id_estado:[59,65], '$plan_pago_detalles.id$': null, '$aip_debito.par_debito.id$': 58}
				});

				if (aInstancias && aInstancias.length) {
					for (let i = 0; i < aInstancias.length; i++) {
						let instancia = aInstancias[i];
						let planPago = await modelos.planPago.findOne({where:{id_instancia_poliza:instancia.id}});
						let periodo = instancia.dataValues.aip_modo_pago.valor == 90 ? 12 : instancia.dataValues.aip_modo_pago.valor == 93 ? 1 : '';
						let prima = instancia.dataValues.aip_modo_pago.valor == 90 ? 16 : instancia.dataValues.aip_modo_pago.valor == 93 ? 176 : '';
						let primaPendiente = instancia.dataValues.aip_modo_pago.valor == 90 ? 192 : instancia.dataValues.aip_modo_pago.valor == 93 ? 176 : '';
						let primaAcumulada = 0;
						if (planPago){
							planPago.dataValues.plan_pago_detalles = [];
							//let maxId = await modelos.plan_pago_detalle.max('id');
							for (let j = 0; j < periodo ; j++) {
								//maxId++;
								let cuota = j+1;
								primaAcumulada += prima;
								primaPendiente -= prima;
								let newPlanPagoDetalle = {
									//id: maxId,
									id_instancia_poliza: parseInt(instancia.id),
									id_planpago: parseInt(planPago.dataValues.id),
									nro_cuota_prog: cuota,
									fecha_couta_prog: moment(instancia.dataValues.createdAt).add(j,'months').toDate(),
									pago_cuota_prog: prima,
									interes_prog:0,
									prima_prog: prima,
									pago_prima_acumulado: primaAcumulada,
									prima_pendiente:primaPendiente,
									id_estado:253,
									FechaPagoEjec:null,
									NroDocPagoEjec:null,
									OrigenActualizacion:'',
									adicionado_por:instancia.dataValues.adicionada_por,
									modificado_por:instancia.dataValues.adicionada_por,
									createdAt:instancia.dataValues.createdAt,
									updatedAt:instancia.dataValues.createdAt,
									id_estado_envio:null,
									id_estado_det:null,
									id_instancia_archivo:null,
									data1:null
								};
								let createdPlanPagoDetalle = await modelos.plan_pago_detalle.create(newPlanPagoDetalle);
								planPago.dataValues.plan_pago_detalles.push(createdPlanPagoDetalle.dataValues);
							}
							planesPagos.push(planPago);
						}
					}
					loggerPlanPago.info('Plane de pago detalles creados: ' + new Date().toString(), planesPagos);
				}
			}
			return planesPagos;
		} catch (e) {
			console.log(e);
		}
	}


	static async createEcoMedicvPlanPagos() {
		try {
			let id_poliza = 8;
			let planesPagos = [];
			let objetoXAtributos = await modelos.objeto_x_atributo.findAll({
				include: [
					{model: modelos.atributo},
					{model: modelos.objeto, where: {id_poliza:id_poliza}},
				]
			});
			let objetoXAtributoModoPago = objetoXAtributos.find(param => parseInt(param.dataValues.id_atributo) == 28);
			let objetoXAtributoDebito = objetoXAtributos.find(param => parseInt(param.dataValues.id_atributo) == 12);
			if (objetoXAtributoModoPago && objetoXAtributoDebito) {
				let aInstancias = await modelos.instancia_poliza.findAll({
					include: [
						{
							model: modelos.atributo_instancia_poliza, as:'aip_modo_pago', where: {id_objeto_x_atributo: objetoXAtributoModoPago.dataValues.id}, required: true,
							include: {model: modelos.parametro, as:'par_modo_pago', where: {diccionario_id: 31}, required: true}
						},
						{
							model: modelos.atributo_instancia_poliza, as:'aip_debito', where: {id_objeto_x_atributo: objetoXAtributoDebito.dataValues.id}, required: true,
							include: {model: modelos.parametro, as:'par_debito', where: {diccionario_id: 21}, required: true}
						},
						{model: modelos.planPago, required: false},
					],
					where: {id_poliza: id_poliza, id_estado:[59,65], '$plan_pagos.id$': null, '$aip_debito.par_debito.id$': 58}
				});
				if (aInstancias && aInstancias.length) {
					for (let i = 0; i < aInstancias.length; i++) {
						let instancia = aInstancias[i];
						let newPlanPago = {
							id_instancia_poliza:parseInt(instancia.id),
							total_prima: 100,
							interes:0,
							plazo_anos:1,
							periodicidad_anual:1,
							prepagable_postpagable:1,
							fecha_inicio:instancia.dataValues.createdAt,
							id_moneda:68,
							adicionado_por:instancia.dataValues.adicionada_por,
							modificado_por:instancia.dataValues.adicionada_por,
							createdAt:instancia.dataValues.createdAt,
							updatedAt:instancia.dataValues.createdAt,
							id_estado:null
						};
						let createdPlanPago = await modelos.planPago.create(newPlanPago);
						planesPagos.push(createdPlanPago.dataValues);
					}
					loggerPlanPago.info('Planes de pago creados: ' + new Date().toString(), planesPagos);
				}
			}
			return planesPagos;
		} catch (e) {
			console.log(e);
		}
	}

	static async createEcoMedicvPlanPagosDetalle() {
		try {
			let id_poliza = 8;
			let planesPagos = [];
			let objetoXAtributos = await modelos.objeto_x_atributo.findAll({
				include: [
					{model: modelos.atributo},
					{model: modelos.objeto, where: {id_poliza:id_poliza}},
				]
			});
			let objetoXAtributoModoPago = objetoXAtributos.find(param => parseInt(param.dataValues.id_atributo) == 28);
			let objetoXAtributoDebito = objetoXAtributos.find(param => parseInt(param.dataValues.id_atributo) == 12);
			if (objetoXAtributoModoPago && objetoXAtributoDebito) {
				let aInstancias = await modelos.instancia_poliza.findAll({
					include: [
						{
							model: modelos.atributo_instancia_poliza, as:'aip_modo_pago', where: {id_objeto_x_atributo: objetoXAtributoModoPago.dataValues.id}, required: true,
							include: {model: modelos.parametro, as:'par_modo_pago', where: {diccionario_id: 31}, required: true}
						},
						{
							model: modelos.atributo_instancia_poliza, as:'aip_debito', where: {id_objeto_x_atributo: objetoXAtributoDebito.dataValues.id}, required: true,
							include: {model: modelos.parametro, as:'par_debito', where: {diccionario_id: 21}, required: true}
						},
						{model: modelos.plan_pago_detalle, required: false},
					],
					where: {id_poliza: id_poliza, id_estado:[59,65], '$plan_pago_detalles.id$': null, '$aip_debito.par_debito.id$': 58}
				});

				if (aInstancias && aInstancias.length) {
					for (let i = 0; i < aInstancias.length; i++) {
						let instancia = aInstancias[i];
						let planPago = await modelos.planPago.findOne({where:{id_instancia_poliza:instancia.id}});
						let periodo = 1;
						let prima = 100;
						let primaPendiente = 100;
						let primaAcumulada = 0;
						if (planPago){
							planPago.dataValues.plan_pago_detalles = [];
							//let maxId = await modelos.plan_pago_detalle.max('id');
							for (let j = 0; j < periodo ; j++) {
								let cuota = j+1;
								//maxId++;
								primaAcumulada += prima;
								primaPendiente -= prima;
								let newPlanPagoDetalle = {
									//id:maxId,
									id_instancia_poliza: parseInt(instancia.id),
									id_planpago: parseInt(planPago.dataValues.id),
									nro_cuota_prog: cuota,
									fecha_couta_prog: moment(instancia.dataValues.createdAt).add(j,'months').toDate(),
									pago_cuota_prog: prima,
									interes_prog:0,
									prima_prog: prima,
									pago_prima_acumulado: primaAcumulada,
									prima_pendiente:primaPendiente,
									id_estado:253,
									FechaPagoEjec:null,
									NroDocPagoEjec:null,
									OrigenActualizacion:'',
									adicionado_por:instancia.dataValues.adicionada_por,
									modificado_por:instancia.dataValues.adicionada_por,
									createdAt:instancia.dataValues.createdAt,
									updatedAt:instancia.dataValues.createdAt,
									id_estado_envio:null,
									id_estado_det:null,
									id_instancia_archivo:null,
									data1:null
								};
								let createdPlanPagoDetalle = await modelos.plan_pago_detalle.create(newPlanPagoDetalle);
								planPago.dataValues.plan_pago_detalles.push(createdPlanPagoDetalle.dataValues);
							}
							planesPagos.push(planPago);
						}
					}
					loggerPlanPago.info('Plane de pago detalles creados: ' + new Date().toString(), planesPagos);
				}
			}
			return planesPagos;
		} catch (e) {
			console.log(e);
		}
	}
}

module.exports = PlanPagoService;
