const modelos = require("../modelos");

class InstanciaAnexoAseguradoService {

    static async SaveOrUpdateInstancia_anexo_asegurado(instancia_anexo_asegurado, id_usuario) {
        //nuevo
        let persona_doc_id=instancia_anexo_asegurado.entidad.persona.persona_doc_id;
        let persona_doc_id_ext=instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext;

        if(persona_doc_id===null || persona_doc_id_ext===null || persona_doc_id===undefined || persona_doc_id===undefined ){
            persona_doc_id='';
            persona_doc_id_ext='';
        }

        let resp = await modelos.persona.find({where: {persona_doc_id,persona_doc_id_ext}});
        if(resp===null){
            instancia_anexo_asegurado.entidad.persona = await this.AdicionarPersona(instancia_anexo_asegurado.entidad.persona);
            instancia_anexo_asegurado.entidad.id_persona=instancia_anexo_asegurado.entidad.persona.id;
            instancia_anexo_asegurado.entidad.id=await this.AdicionarEntidad(instancia_anexo_asegurado.entidad);
        }else{
            instancia_anexo_asegurado.entidad.persona.id=resp.id;
            instancia_anexo_asegurado.entidad.persona.modificado_por=id_usuario;
            await this.ModificarPersona(resp.id,instancia_anexo_asegurado.entidad.persona);
            await modelos.entidad.find({ where: { id_persona: instancia_anexo_asegurado.entidad.persona.id}}).then(async result => {
                if (result === null){
                    instancia_anexo_asegurado.entidad.id_persona=resp.id;
                    instancia_anexo_asegurado.entidad.id= await this.AdicionarEntidad(instancia_anexo_asegurado.entidad);
                }else{
                    instancia_anexo_asegurado.entidad.id=result.id;
                }
            }).catch(err => {
                return err;
            });
        }
        let result;
        if(instancia_anexo_asegurado.id===0){
            instancia_anexo_asegurado.id_entidad=instancia_anexo_asegurado.entidad.id;
            instancia_anexo_asegurado.adicionado_por=id_usuario;
            instancia_anexo_asegurado.modificado_por=id_usuario;
            result = await modelos.instancia_anexo_asegurado.create(instancia_anexo_asegurado);
            instancia_anexo_asegurado.id = result.id;
        } else{
            result = await modelos.instancia_anexo_asegurado.update(instancia_anexo_asegurado,{ where: { id:instancia_anexo_asegurado.id } })
        }
        return instancia_anexo_asegurado
    }

    static async Eliminar(instancia_anexo_asegurado, id_usuario) {
        let persona_doc_id=instancia_anexo_asegurado.entidad.persona.persona_doc_id;
        let persona_doc_id_ext=instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext;
        let id = instancia_anexo_asegurado.id;
        let resp = await modelos.instancia_anexo_asegurado.find({where: {id}})
        let resp2;
        if(resp){
            let resp2 = await resp.destroy()
        }
        return resp2;
    }

    static async AdicionarPersona (persona,id_usuario) {
        return new Promise(async resolve => {
            if(persona.persona_fecha_nacimiento===undefined){
                persona.persona_fecha_nacimiento=new Date();
            }
            await modelos.persona.create(persona).then(result => {
                    resolve(result);
                }
            ).catch(err => {
                resolve(err);
            });
        });
    }

    static async ModificarPersona (id,persona) {
        return new Promise(async resolve => {
            if(persona.persona_fecha_nacimiento===undefined){
                persona.persona_fecha_nacimiento=new Date();
            }else{
                persona.persona_fecha_nacimiento=new Date(persona.persona_fecha_nacimiento);
            }
            await modelos.persona.update(persona, { where: { id } }).then(result => {
                    resolve(result);
                }
            ).catch(err => {
                resolve(result);
            });

        });
    }

    static async AdicionarEntidad (entidad) {
        return new Promise(async resolve => {
            entidad.tipo_entidad=12;
            await modelos.entidad.create(entidad).then(result => {
                    resolve(result.id);
                }
            ).catch(err => {
                resolve(err);
            });

        });
    }
}

module.exports = InstanciaAnexoAseguradoService;
