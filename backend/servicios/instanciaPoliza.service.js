const modelos = require("../modelos");
const modelosDesgravamen = require("../modelos/desgravamen");
const { loggerCatch } = require("../modulos/winston");
const moment = require("moment");
const parNumeracionIndependiente = {id: 88, parametro_descripcion: "NUMERACION INDEPENDIENTE"};
const parNumeracionUnificada = {id: 89, parametro_descripcion: "NUMERACION NUMERACION UNIFICADA"};

var isLoggedIn = (req, res, next) => {
        console.log("session ", req.session);
        if (req.isAuthenticated()) {
            return next();
        }
        return res
            .status(400)
            .json({ statusCode: 400, message: "usuario no autentificado" });
};

async function asyncForEach(array, callback) {
    if (array) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }
}

class InstanciaPolizaService {

    static async GetAll() {
        try {
            return await modelos.sequelize.query(`select A.id,A.id_poliza,A.id_estado,FORMAT(A.fecha_registro,'dd/MM/yyyy') fecha_registro,A.adicionada_por,B.descripcion from instancia_poliza A Inner join poliza B on A.id_poliza=B.id`)
        } catch (e) {
            console.log(e)
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByParametrosEcoVida(objeto) {
        try {
            objeto.sucursal = objeto.sucursal ? (objeto.sucursal+'').trim() : '';
            objeto.agencia = objeto.agencia ? (objeto.agencia+'').trim() : '';
            objeto.id_instancia_poliza = objeto.id_instancia_poliza ? objeto.id_instancia_poliza : '';
            objeto.persona_primer_apellido = objeto.persona_primer_apellido ? (objeto.persona_primer_apellido+'').trim() : '';
            objeto.persona_segundo_apellido = objeto.persona_segundo_apellido ? (objeto.persona_segundo_apellido+'').trim() : '';
            objeto.persona_primer_nombre = objeto.persona_primer_nombre ? (objeto.persona_primer_nombre+'').trim() : '';
            objeto.persona_doc_id = objeto.persona_doc_id ? (objeto.persona_doc_id+'').trim() : '';
            objeto.id_certificado = objeto.id_certificado ? (objeto.id_certificado+'').trim() : '';
            objeto.nro_documento = objeto.nro_documento ? (objeto.nro_documento+'').trim() : '';
            objeto.usuario_login = objeto.usuario_login ? (objeto.usuario_login+'').trim() : '';
            objeto.fecha_min = objeto.fecha_min ? moment(objeto.fecha_min).hour(0).minutes(1).toDate() : null;
            objeto.fecha_max = objeto.fecha_max ? moment(objeto.fecha_max).hour(23).minutes(59).toDate() : null;

            let objQuery = {
                offset:objeto.first ? objeto.first : 0,
                limit:objeto.rows ? objeto.rows : 10,
                where: {
                    $and: [
                        objeto.id_instancia_poliza ? { id: objeto.id_instancia_poliza } : null,
                        objeto.estado ? { id_estado: objeto.estado } : null,
                        objeto.id_poliza ? { id_poliza: objeto.id_poliza } : null,
                        objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                            ? {
                                fecha_registro: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                            } : null
                    ],
                },
                include: [
                    {
                        model:modelos.planPago
                    },
                    {
                        model: modelos.asegurado,
                        include: {
                            model: modelos.entidad,
                            include: {
                                model: modelos.persona,
                                where: {
                                    $and: [
                                        objeto.persona_primer_nombre ? { persona_primer_nombre: { $like: objeto.persona_primer_nombre } } : null,
                                        objeto.persona_primer_apellido ? { persona_primer_apellido: { $like: objeto.persona_primer_apellido } } : null,
                                        objeto.persona_segundo_apellido ? { persona_segundo_apellido: { $like: objeto.persona_segundo_apellido } } : null,
                                        objeto.persona_doc_id ? { persona_doc_id: { $like: objeto.persona_doc_id } } : null,
                                    ],
                                },
                            },
                        },
                    },
                    {
                        model: modelos.usuario,
                        where: {
                            $and: [
                                objeto.usuario_login ? { usuario_login: { $like: objeto.usuario_login } } : null
                            ]
                        }
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.sucursal ? {id_objeto_x_atributo: [192, 217, 289], valor: { $like: objeto.sucursal } } : {id_objeto_x_atributo: [192, 217, 289] }
                            ]
                        },
                        as: "sucursal",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.agencia ? { id_objeto_x_atributo: [191, 216, 288], valor: { $like: objeto.agencia } } : { id_objeto_x_atributo: [191, 216, 288] }
                            ]
                        },
                        as: "agencia",
                    },
                    {
                        model: modelos.instancia_documento,
                        where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                                fecha_emision: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                                nro_documento: {$like: objeto.nro_documento},
                                id_documento: [16, 19, 25],
                            } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                                fecha_emision: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                                id_documento: [16, 19, 25],
                            } : objeto.nro_documento ? {
                                nro_documento: {$like: objeto.nro_documento},
                                id_documento: [16, 19, 25],
                            } : {
                                id_documento: [16, 19, 25],
                            },
                        required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                        as: "solicitudes",
                    },
                    {
                        model: modelos.instancia_documento,
                        where: ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                                nro_documento: {$like: objeto.id_certificado},
                                fecha_emision: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                                id_documento: [18, 21, 27],
                            } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                                fecha_emision: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                                id_documento: [18, 21, 27],
                            } : objeto.id_certificado ? {
                                nro_documento: {$like: objeto.id_certificado},
                                id_documento: [18, 21, 27],
                            } : {
                                id_documento: [18, 21, 27],
                            },
                        required: ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                        as: "certificados",
                    },
                    { model: modelos.poliza }
                ]
            };
            let objQueryCount = {
                offset:objeto.first ? objeto.first : 0,
                limit:objeto.rows ? objeto.rows : 10,
                where: {
                    $and: [
                        objeto.id_instancia_poliza ? { id: objeto.id_instancia_poliza } : null,
                        objeto.estado ? { id_estado: objeto.estado } : null,
                        objeto.id_poliza ? { id_poliza: objeto.id_poliza } : null,
                        objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                            ? {
                                fecha_registro: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                            } : null
                    ],
                },
                include: [
                    {
                        model: modelos.asegurado,
                        include: {
                            model: modelos.entidad,
                            include: {
                                model: modelos.persona,
                                where: {
                                    $and: [
                                        objeto.persona_primer_nombre ? { persona_primer_nombre: { $like: objeto.persona_primer_nombre } } : null,
                                        objeto.persona_primer_apellido ? { persona_primer_apellido: { $like: objeto.persona_primer_apellido } } : null,
                                        objeto.persona_segundo_apellido ? { persona_segundo_apellido: { $like: objeto.persona_segundo_apellido } } : null,
                                        objeto.persona_doc_id ? { persona_doc_id: { $like: objeto.persona_doc_id } } : null,
                                    ],
                                },
                            },
                        },
                    },
                    {
                        model: modelos.usuario,
                        where: {
                            $and: [
                                objeto.usuario_login ? { usuario_login: { $like: objeto.usuario_login } } : null
                            ]
                        }
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.sucursal ? {id_objeto_x_atributo: [192, 217, 289], valor: { $like: objeto.sucursal } } : {id_objeto_x_atributo: [192, 217, 289] }
                            ]
                        },
                        as: "sucursal",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.agencia ? { id_objeto_x_atributo: [191, 216, 288], valor: { $like: objeto.agencia } } : { id_objeto_x_atributo: [191, 216, 288] }
                            ]
                        },
                        as: "agencia",
                    },
                    ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? {
                        model: modelos.instancia_documento,
                        where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [16, 19, 25],
                        } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [16, 19, 25],
                        } : objeto.nro_documento ? {
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [16, 19, 25],
                        } : {
                            id_documento: [16, 19, 25],
                        },
                        required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                        as: "solicitudes",
                    } : null,
                    ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? {
                        model: modelos.instancia_documento,
                        where: ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [18, 21, 27],
                        } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [18, 21, 27],
                        } : objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            id_documento: [18, 21, 27],
                        } : {
                            id_documento: [18, 21, 27],
                        },
                        required: ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                        as: "certificados",
                    } : null,
                    { model: modelos.poliza }
                ]
            };

            objQueryCount.include = objQueryCount.include.filter(Boolean);

            let totalRows = await modelos.instancia_poliza.count(objQueryCount);

            if (totalRows < objQuery.limit) {
                delete objQuery.limit;
                delete objQuery.offset;
            }

            let aData = await modelos.instancia_poliza.findAll(objQuery);
            return [aData,totalRows,objeto.rows,objeto.first];
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByParametrosDesgravamen(objeto) {
        try {
            let parametros = await modelos.parametro.findAll({where:{diccionario_id:[40,38,63,62]}});
            //let parametrosDesgravamen = await modelosDesgravamen.seg_estados.findAll();
            objeto.usuario_login = objeto.usuario_login ? (objeto.usuario_login+'').trim() : '';
            objeto.nro_documento = objeto.nro_documento ? (objeto.nro_documento+'').trim() : '';
            objeto.persona_primer_apellido = objeto.persona_primer_apellido ? (objeto.persona_primer_apellido+'').trim() : '';
            objeto.persona_segundo_apellido = objeto.persona_segundo_apellido ? (objeto.persona_segundo_apellido+'').trim() : '';
            objeto.persona_primer_nombre = objeto.persona_primer_nombre ? (objeto.persona_primer_nombre+'').trim() : '';
            objeto.persona_doc_id = objeto.persona_doc_id ? (objeto.persona_doc_id+'').trim() : '';
            objeto.persona_doc_id_ext = objeto.persona_doc_id_ext ? (objeto.persona_doc_id_ext+'').trim() : '';
            objeto.tipo_cobertura = objeto.tipo_cobertura ? (objeto.tipo_cobertura+'').trim() : '';
            objeto.tipo_credito = objeto.tipo_credito ? (objeto.tipo_credito+'').trim() : '';
            objeto.usuario_login = objeto.usuario_login ? (objeto.usuario_login+'').trim() : '';
            objeto.estado = objeto.estado ? (objeto.estado+'').trim() : '';
            objeto.tipo_seguro = objeto.tipo_seguro ? (objeto.tipo_seguro+'').trim() : '';
            objeto.tipo_cobertura = objeto.tipo_cobertura ? (objeto.tipo_cobertura+'').trim() : '';
            objeto.sucursal = objeto.sucursal ? (objeto.sucursal+'').trim() : '';
            objeto.agencia = objeto.agencia ? (objeto.agencia+'').trim() : '';
            objeto.fecha_min = objeto.fecha_min ? moment(objeto.fecha_min).hour(0).minutes(1).toDate() : null;
            objeto.fecha_max = objeto.fecha_max ? moment(objeto.fecha_max).hour(23).minutes(59).toDate() : null;
            let objQuery = {
                where: {$and: []},
                include: [{
                    model: modelosDesgravamen.seg_solicitudes, as: 'deu_solicitud',
                    where:{$and: [objeto.usuario_login ? { Sol_CodOficial: {$like: objeto.usuario_login} } : null]}
                }, {
                    model: modelosDesgravamen.seg_adicionales, as: 'deu_adicional',
                }, {
                    model: modelosDesgravamen.seg_aprobaciones, as: 'deu_aprobacion',
                }],
                order: [['Deu_Id', 'DESC']],
                offset:objeto.first ? objeto.first : 0,
                limit:objeto.rows ? objeto.rows : 10,
            };
            let objQueryCount = {
                where: {$and: []},
                include: [{
                    model: modelosDesgravamen.seg_solicitudes, as: 'deu_solicitud',
                    where: {$and: [objeto.usuario_login ? { Sol_CodOficial: {$like: objeto.usuario_login} } : null]},
                }],
            };
            if (objeto.poliza) {
                objeto.poliza = (objeto.poliza+'').trim();
            }
            if (objeto.estado) {
                if (Array.isArray(objeto.estado)) {
                    objQuery.include[0].where.$and.push({Sol_EstadoSol: {$in: objeto.estado}});
                } else if (objeto.estado.indexOf(',') >= 0) {
                    let parEstados = objeto.estado.split(',');
                    objQuery.include[0].where.$and.push({Sol_EstadoSol: {$in: parEstados}});
                } else {
                    objQuery.include[0].where.$and.push({Sol_EstadoSol: {$like: objeto.estado}});
                }
            }
            if (objeto.usuarioLogin) {
                // Oficial de Credito
                if(objeto.usuario_login && objeto.usuarioLogin.usuarioRoles.find(param => param.id == 5)) {
                    objQuery.include[0].where.$and.push({Sol_CodOficial: {$like: objeto.usuario_login}});
                }
                // 20	aseguradora_no_licitada	Aseguradora Alianza
                if(objeto.usuarioLogin.usuarioRoles.find(param => param.id == 20)) {
                    // objQuery.limit = 300;
                    objeto.poliza = ['1/2018','2/2018','80268'].find(param => objeto.poliza && objeto.poliza.includes(param)) ? objeto.poliza : ['1/2018','2/2018','80268'];
                    if (objQuery.include[0].where.$and.find(param => param && param.Sol_EstadoSol)) {
                        objQuery.include[0].where.$and.push({Sol_EstadoSol : {$notIn: ['P']}});
                    } else {
                        objQuery.include[0].where.$and.push({Sol_EstadoSol: {$notIn:['P']}});
                    }
                }
                // 21	aseguradora_no_licitada	Aseguradora Boliviana
                if(objeto.usuarioLogin.usuarioRoles.find(param => param.id == 21)) {
                    // objQuery.limit = 300;
                    objeto.poliza = ['20002','20001','20006','20007'].find(param => objeto.poliza && objeto.poliza.includes(param)) ? objeto.poliza : ['20002','20007','20001','20006'];
                    if (objQuery.include[0].where.$and.find(param => param && param.Sol_EstadoSol)) {
                        objQuery.include[0].where.$and.push({Sol_EstadoSol: {$notIn: ['P']}})
                    } else {
                        objQuery.include[0].where.$and.push({Sol_EstadoSol: {$notIn:['P']}});
                    }
                }
                // 22	corredora_desgravamen	Corredora Desgravamen
                if(objeto.usuarioLogin.usuarioRoles.find(param => param.id == 22)) {
                    // objQuery.limit = 300;
                    objeto.poliza = ['1/2018','2/2018','80268','20002','20001','20006','20007'].find(param => objeto.poliza && objeto.poliza.includes(param)) ? objeto.poliza : ['1/2018','2/2018','80268','20002','20007','20001','20006'];
                    objQuery.include[0].where.$and.push({Sol_EstadoSol: {$notIn:['P']}});
                }
                // 23	supervisor_ofcredito	Supervisor General
                if(objeto.usuarioLogin.usuarioRoles.find(param => param.id == 23)) {
                    // objQuery.limit = 300;
                }
                // 24	supervisor_ofcredito_sucursal	Supervisor Sucursal
                if(objeto.usuarioLogin.usuarioRoles.find(param => param.id == 24)) {
                    if (objeto.usuarioLogin.usuario_banco) {
                        objQuery.include[0].where.$and.push({Sol_Sucursal: {$like: objeto.usuarioLogin.usuario_banco.su_sucursal}});
                    }
                    // objQuery.limit = 300;
                }
                // 25	supervisor_ofcredito_agencia	Supervisor Agencia
                if(objeto.usuarioLogin.usuarioRoles.find(param => param.id == 25)) {
                    if (objeto.usuarioLogin.usuario_banco) {
                        objQuery.include[0].where.$and.push({Sol_Agencia: {$like: objeto.usuarioLogin.usuario_banco.su_oficina}});
                    }
                    // objQuery.limit = 300;
                }
                // 26	aseguradora_no_licitada	Aseguradora Boliviana Suscriptor
                if(objeto.usuarioLogin.usuarioRoles.find(param => param.id == 26)) {
                    // objQuery.limit = 300;
                    objeto.poliza = ['20002','20001','20006','20007'].find(param => objeto.poliza && objeto.poliza.includes(param)) ? objeto.poliza : ['20002','20007','20001','20006'];
                    if (objQuery.include[0].where.$and.find(param => param.Sol_EstadoSol)) {
                        objQuery.include[0].where.$and.push({Sol_EstadoSol:{$notIn: ['A','P']}})
                    } else {
                        objQuery.include[0].where.$and.push({Sol_EstadoSol:{$notIn:['A','P']}});
                    }
                    if (objeto.estado == '%') {
                        objQuery.include[0].where.$and.push({Sol_EstadoSol:{$in:["A1","A2","A3","A4","A5","A6","A7","A8","C1","C2","D","O1","O2","O3","O4","O5","O6","R"]}});
                    }
                }
            }
            if (objeto.nro_documento) {
                objQuery.include[0].where.$and.push({Sol_NumSol:{$like: objeto.nro_documento}});
            }
            if (objeto.tipo_seguro) {
                let parTipoSeguro = parametros.find(param => param.parametro_cod == objeto.tipo_seguro && param.diccionario_id == 63);
                if (parTipoSeguro) {
                    objQuery.include[0].where.$and.push({Sol_TipoSeg: {$like: parTipoSeguro.parametro_cod}});
                }
            }
            if (objeto.tipo_credito) {
                objQuery.include[0].where.$and.push({Sol_TipoCred:{$like: objeto.tipo_credito}});
            }
            if (objeto.sucursal) {
                let parSucursal = parametros.find(param => param.parametro_cod == objeto.sucursal && param.diccionario_id == 38);
                if (parSucursal) {
                    objQuery.include[0].where.$and.push({Sol_Sucursal:{$like: '%'+parSucursal.parametro_descripcion+'%'}});
                }
            }
            if (objeto.agencia) {
                let parAgencia = parametros.find(param => param.parametro_cod == objeto.agencia && param.diccionario_id == 40);
                if (parAgencia) {
                    objQuery.include[0].where.$and.push({Sol_Agencia:{$like: '%'+parAgencia.parametro_descripcion+'%'}});
                }
            }
            if (objeto.tipo_cobertura) {
                let parTipoCobertura = parametros.find(param => param.parametro_cod == objeto.tipo_cobertura && param.diccionario_id == 62);
                if(parTipoCobertura) {
                    objQuery.where.$and.push({Deu_cobertura: {$like: parTipoCobertura.parametro_cod}});
                }
            }
            if (objeto.poliza) {
                if (Array.isArray(objeto.poliza)) {
                    objQuery.where.$and.push({Deu_Poliza: {$in: objeto.poliza}});
                } else if (objeto.poliza.indexOf(',') >= 0) {
                    let parPolizas = objeto.poliza.split(',');
                    objQuery.where.$and.push({Deu_Poliza:{$in: parPolizas}});
                } else {
                    objQuery.where.$and.push({Deu_Poliza:{$like: objeto.poliza}});
                }
            }
            if (objeto.persona_doc_id && objeto.persona_doc_id != '%') {
                objQuery.where.$and.push({Deu_NumDoc:{$like: objeto.persona_doc_id}});
            }
            if (objeto.persona_doc_id_ext && objeto.persona_doc_id_ext != '%') {
                objQuery.where.$and.push({Deu_ExtDoc:{$like: objeto.persona_doc_id_ext}});
            }
            if (objeto.persona_primer_apellido && objeto.persona_primer_apellido != '%') {
                objQuery.where.$and.push({Deu_Paterno:{$like: objeto.persona_primer_apellido}});
            }
            if (objeto.persona_segundo_apellido && objeto.persona_segundo_apellido != '%') {
                objQuery.where.$and.push({Deu_Materno:{$like: objeto.persona_segundo_apellido}});
            }
            if (objeto.persona_primer_nombre && objeto.persona_primer_nombre != '%') {
                objQuery.where.$and.push({Deu_Nombre:{$like: objeto.persona_primer_nombre}});
            }
            if (objeto.campo_fecha) {
                objeto.fecha_min = moment(objeto.fecha_min).add('minute',-1).toDate();
                objeto.fecha_max = moment(objeto.fecha_max).toDate();
                switch (objeto.campo_fecha) {
                    case "fecha_solicitud":
                        objQuery.include[0].where.$and.push(objeto.fecha_min && objeto.fecha_max ? {Sol_FechaSol: { $between: [objeto.fecha_min, objeto.fecha_max]}} : null);
                        break;
                    case "fecha_resolucion":
                        if (objeto.fecha_min && objeto.fecha_max) {
                            objQuery.where.$and.push({Deu_FechaResolucion: {$between: [objeto.fecha_min, objeto.fecha_max]}});
                        }
                        break;
                }
            }
            objQueryCount.where = objQuery.where;
            objQueryCount.include[0] = objQuery.include[0];
            let aData = await modelosDesgravamen.seg_deudores.findAll(objQuery);
            let totalRows = await modelosDesgravamen.seg_deudores.count(objQueryCount);
            for (let i = 0; i < aData.length; i++) {
                let data = aData[i];
                data.dataValues.Deu_Imc = parseFloat(parseInt(data.Deu_Peso) / Math.pow(parseInt(data.Deu_Talla), 2)*10000).toFixed(2);
                data.dataValues.Deu_K = parseInt(data.Deu_Peso) + 100 - parseInt(data.Deu_Talla).toFixed(2);
                data.dataValues.Deu_RespPositivas = data.deu_adicional.filter(param => param.Adic_Comment != '').length;
                if (data.dataValues.Deu_Imc < 18.5) {
                    data.dataValues.Deu_FactorImc = 'danger';
                }
                if (data.dataValues.Deu_Imc >= 18.5 && data.dataValues.Deu_Imc <= 24.99) {
                    data.dataValues.Deu_FactorImc = 'good';
                }
                if (data.dataValues.Deu_Imc >= 25.0 && data.dataValues.Deu_Imc <= 29.99) {
                    data.dataValues.Deu_FactorImc = 'good';
                }
                if (data.dataValues.Deu_Imc > 30) {
                    data.dataValues.Deu_FactorImc = 'danger';
                }

                if (data.dataValues.Deu_K < -15) {
                    data.dataValues.Deu_FactorK = 'danger';
                }
                if (data.dataValues.Deu_K >= -15 && data.dataValues.Deu_Imc <= 15) {
                    data.dataValues.Deu_FactorK = 'good';
                }
                if (data.dataValues.Deu_K > 15) {
                    data.dataValues.Deu_FactorK = 'danger';
                }
                if (data.dataValues.Deu_MontoActAcumVerifSum) {
                    data.dataValues.Deu_MontoActAcumVerifSum += parseFloat(data.Deu_MontoActAcumVerif);
                } else {
                    data.dataValues.Deu_MontoActAcumVerifSum = parseFloat(data.Deu_MontoActAcumVerif);
                }
                data.dataValues.Deu_MontoSolAcumVerifSum = parseFloat(data.deu_solicitud.Sol_MontoSol) + parseFloat(data.Deu_MontoActAcumVerif);
                aData[i] = data;
            }
            return [aData,totalRows,objeto.rows,objeto.first];
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByParametrosEcoAccidentes(objeto) {
        try {
            objeto.usuario_login = objeto.usuario_login ? (objeto.usuario_login+'').trim() : '';
            objeto.nro_documento = objeto.nro_documento ? (objeto.nro_documento+'').trim() : '';
            objeto.persona_primer_apellido = objeto.persona_primer_apellido ? (objeto.persona_primer_apellido+'').trim() : '';
            objeto.persona_segundo_apellido = objeto.persona_segundo_apellido ? (objeto.persona_segundo_apellido+'').trim() : '';
            objeto.persona_primer_nombre = objeto.persona_primer_nombre ? (objeto.persona_primer_nombre+'').trim() : '';
            objeto.persona_doc_id = objeto.persona_doc_id ? (objeto.persona_doc_id+'').trim() : '';
            objeto.id_certificado = objeto.id_certificado ? (objeto.id_certificado+'').trim() : '';
            objeto.usuario_login = objeto.usuario_login ? (objeto.usuario_login+'').trim() : '';
            objeto.estado = objeto.estado ? (objeto.estado+'').trim() : '';
            objeto.sucursal = objeto.sucursal ? (objeto.sucursal+'').trim() : '';
            objeto.agencia = objeto.agencia ? (objeto.agencia+'').trim() : '';
            objeto.id_poliza = objeto.id_poliza ? (objeto.id_poliza+'').trim().split(',') : '';
            objeto.id_instancia_poliza = objeto.id_instancia_poliza ? objeto.id_instancia_poliza : '';
            objeto.fecha_min = objeto.fecha_min ? moment(objeto.fecha_min).hour(0).minutes(1).toDate() : null;
            objeto.fecha_max = objeto.fecha_max ? moment(objeto.fecha_max).hour(23).minutes(59).toDate() : null;
            let objQuery = {
                offset: objeto.first ? objeto.first : 0,
                limit: objeto.rows ? objeto.rows : 10,
                where: {
                    $and: [
                        objeto.id_instancia_poliza ? {id: objeto.id_instancia_poliza} : null,
                        objeto.estado ? {id_estado: objeto.estado} : null,
                        objeto.id_poliza ? {id_poliza: objeto.id_poliza} : null,
                        objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                            ? {
                                fecha_registro: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                            } : null
                    ],
                },
                include: [
                    {
                        subQuery: false,
                        model: modelos.asegurado,
                        include: [{
                            subQuery: false,
                            model: modelos.entidad,
                            include: {
                                model: modelos.persona,
                                where: {
                                    $and: [
                                        objeto.persona_primer_nombre ? {
                                            persona_primer_nombre: {
                                                $like: objeto.persona_primer_nombre,
                                            },
                                        } : null,
                                        objeto.persona_primer_apellido ? {
                                            persona_primer_apellido: {
                                                $like: objeto.persona_primer_apellido,
                                            },
                                        } : null,
                                        objeto.persona_segundo_apellido ? {
                                            persona_segundo_apellido: {
                                                $like: objeto.persona_segundo_apellido,
                                            },
                                        } : null,
                                        objeto.persona_doc_id ? {
                                            persona_doc_id: {$like: objeto.persona_doc_id}
                                        } : null,
                                    ],
                                },
                            },
                        },
                        ],
                    },
                    {
                        model: modelos.usuario,
                        where: {
                            $and: [
                                objeto.usuario_login ? {usuario_login: {$like: objeto.usuario_login}} : null
                            ]
                        }
                    },
                    {
                        model: modelos.planPago,
                        subQuery: false,
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.sucursal ? {
                                    id_objeto_x_atributo: [314, 350],
                                    valor: {$like: objeto.sucursal}
                                } : {id_objeto_x_atributo: [314, 350]}
                            ]
                        },
                        as: "sucursal",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.agencia ? {
                                    id_objeto_x_atributo: [313,349],
                                    valor: {$like: objeto.agencia}
                                } : {id_objeto_x_atributo: [313,349]}
                            ]
                        },
                        as: "agencia",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.nro_solicitud_sci ? {
                                    id_objeto_x_atributo: [324,360],
                                    valor: {$like: objeto.nro_solicitud_sci}
                                } : {id_objeto_x_atributo: [324,360]}
                            ]
                        },
                        as: "nro_solicitud_sci",
                        required: false
                    },
                    {
                        model: modelos.instancia_documento,
                        where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [28,31],
                        } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [28,31],
                        } : objeto.nro_documento ? {
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [28,31],
                        } : {
                            id_documento: [28,31],
                        },
                        required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                        as: "solicitudes",
                    },
                    {
                        model: modelos.instancia_documento,
                        where: ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_inicio_cert'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_inicio_vigencia: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_inicio_cert'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_inicio_vigencia: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_fin_cert'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_fin_vigencia: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_fin_cert'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_fin_vigencia: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            id_documento: [30,33],
                        } : {
                            id_documento: [30,33],
                        },
                        required: ['fecha_emision'].includes(objeto.campo_fecha) || ['fecha_inicio_cert'].includes(objeto.campo_fecha) || ['fecha_fin_cert'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                        as: "certificados",
                    },
                    { model: modelos.poliza }
                ],
            };
            let objQueryCount = {
                offset: objeto.first ? objeto.first : 0,
                limit: objeto.rows ? objeto.rows : 10,
                where: {
                    $and: [
                        objeto.id_instancia_poliza ? {id: objeto.id_instancia_poliza} : null,
                        objeto.estado ? {id_estado: objeto.estado} : null,
                        objeto.id_poliza ? {id_poliza: objeto.id_poliza} : null,
                        objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                            ? {
                                fecha_registro: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                            } : null
                    ],
                },
                include: [
                    {
                        subQuery: false,
                        model: modelos.asegurado,
                        include: [{
                            subQuery: false,
                            model: modelos.entidad,
                            include: {
                                model: modelos.persona,
                                where: {
                                    $and: [
                                        objeto.persona_primer_nombre ? {
                                            persona_primer_nombre: {
                                                $like: objeto.persona_primer_nombre,
                                            },
                                        } : null,
                                        objeto.persona_primer_apellido ? {
                                            persona_primer_apellido: {
                                                $like: objeto.persona_primer_apellido,
                                            },
                                        } : null,
                                        objeto.persona_segundo_apellido ? {
                                            persona_segundo_apellido: {
                                                $like: objeto.persona_segundo_apellido,
                                            },
                                        } : null,
                                        objeto.persona_doc_id ? {
                                            persona_doc_id: {$like: objeto.persona_doc_id}
                                        } : null,
                                    ],
                                },
                            },
                        },
                        ],
                    },
                    {
                        model: modelos.usuario,
                        where: {
                            $and: [
                                objeto.usuario_login ? {usuario_login: {$like: objeto.usuario_login}} : null
                            ]
                        }
                    },
                    {
                        model: modelos.planPago,
                        subQuery: false,
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.sucursal ? {
                                    id_objeto_x_atributo: [314, 350],
                                    valor: {$like: objeto.sucursal}
                                } : {id_objeto_x_atributo: [314, 350]}
                            ]
                        },
                        as: "sucursal",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.agencia ? {
                                    id_objeto_x_atributo: [313,349],
                                    valor: {$like: objeto.agencia}
                                } : {id_objeto_x_atributo: [313,349]}
                            ]
                        },
                        as: "agencia",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.agencia ? {
                                    id_objeto_x_atributo: [324,360],
                                    valor: {$like: objeto.agencia}
                                } : {id_objeto_x_atributo: [324,360]}
                            ]
                        },
                        as: "nro_solicitud_sci",
                        required: false
                    },
                    ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? {
                        model: modelos.instancia_documento,
                        where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [28,31],
                        } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [28,31],
                        } : objeto.nro_documento ? {
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [28,31],
                        } : {
                            id_documento: [28,31],
                        },
                        required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                        as: "solicitudes",
                    } : null,
                    ['fecha_emision'].includes(objeto.campo_fecha) || ['fecha_inicio_cert'].includes(objeto.campo_fecha) || ['fecha_fin_cert'].includes(objeto.campo_fecha) ? {
                        model: modelos.instancia_documento,
                        where: ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_inicio_cert'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_inicio_vigencia: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_inicio_cert'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_inicio_vigencia: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_fin_cert'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_fin_vigencia: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : ['fecha_fin_cert'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_fin_vigencia: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [30,33],
                        } : objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            id_documento: [30,33],
                        } : {
                            id_documento: [30,33],
                        },
                        required: ['fecha_emision'].includes(objeto.campo_fecha) || ['fecha_inicio_cert'].includes(objeto.campo_fecha) || ['fecha_fin_cert'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                        as: "certificados",
                    } : null,
                    { model: modelos.poliza }
                ]
            };
            objQueryCount.include = objQueryCount.include.filter(Boolean);
            let totalRows = await modelos.instancia_poliza.count(objQueryCount);
            if (totalRows < objQuery.limit) {
                delete objQuery.limit;
                delete objQuery.offset;
            }
            let aData = await modelos.instancia_poliza.findAll(objQuery);
            return [aData, totalRows, objeto.rows, objeto.first];
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByParametros(objeto) {
        try {
            objeto.usuario_login = objeto.usuario_login ? (objeto.usuario_login+'').trim() : '';
            objeto.nro_documento = objeto.nro_documento ? (objeto.nro_documento+'').trim() : '';
            objeto.id_instancia_poliza = objeto.id_instancia_poliza ? objeto.id_instancia_poliza : '';
            objeto.persona_primer_apellido = objeto.persona_primer_apellido ? (objeto.persona_primer_apellido+'').trim() : '';
            objeto.persona_segundo_apellido = objeto.persona_segundo_apellido ? (objeto.persona_segundo_apellido+'').trim() : '';
            objeto.persona_primer_nombre = objeto.persona_primer_nombre ? (objeto.persona_primer_nombre+'').trim() : '';
            objeto.persona_doc_id = objeto.persona_doc_id ? (objeto.persona_doc_id+'').trim() : '';
            objeto.id_certificado = objeto.id_certificado ? (objeto.id_certificado+'').trim() : '';
            objeto.usuario_login = objeto.usuario_login ? (objeto.usuario_login+'').trim() : '';
            objeto.fecha_min = objeto.fecha_min ? moment(objeto.fecha_min).hour(0).minutes(1).toDate() : null;
            objeto.fecha_max = objeto.fecha_max ? moment(objeto.fecha_max).hour(23).minutes(59).toDate() : null;

            let objQuery = {
                offset: objeto.first ? objeto.first : 0,
                limit: objeto.rows ? objeto.rows : 10,
                where: {
                    $and: [
                        objeto.id_instancia_poliza ? {id: objeto.id_instancia_poliza} : null,
                        objeto.estado ? {id_estado: objeto.estado} : null,
                        objeto.id_poliza ? {id_poliza: objeto.id_poliza} : null,
                        objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                            ? {
                                fecha_registro: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                            } : null
                    ],
                },
                include: [
                    {
                        subQuery: false,
                        model: modelos.asegurado,
                        include: [{
                            subQuery: false,
                            model: modelos.entidad,
                            include: {
                                model: modelos.persona,
                                where: {
                                    $and: [
                                        objeto.persona_primer_nombre ? {
                                            persona_primer_nombre: {
                                                $like: objeto.persona_primer_nombre,
                                            },
                                        } : null,
                                        objeto.persona_primer_apellido ? {
                                            persona_primer_apellido: {
                                                $like: objeto.persona_primer_apellido,
                                            },
                                        } : null,
                                        objeto.persona_segundo_apellido ? {
                                            persona_segundo_apellido: {
                                                $like: objeto.persona_segundo_apellido,
                                            },
                                        } : null,
                                        objeto.persona_doc_id ? {
                                            persona_doc_id: {$like: objeto.persona_doc_id}
                                        } : null,
                                    ],
                                },
                            },
                        },
                        ],
                    },
                    {
                        model: modelos.usuario,
                        where: {
                            $and: [
                                objeto.usuario_login ? {usuario_login: {$like: objeto.usuario_login}} : null
                            ]
                        }
                    },
                    {
                        model: modelos.planPago,
                        subQuery: false,
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.sucursal ? {
                                    id_objeto_x_atributo: [149, 147, 289],
                                    valor: {$like: objeto.sucursal}
                                } : {id_objeto_x_atributo: [149, 147, 289]}
                            ]
                        },
                        as: "sucursal",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.agencia ? {
                                    id_objeto_x_atributo: [148, 146, 288],
                                    valor: {$like: objeto.agencia}
                                } : {id_objeto_x_atributo: [148, 146, 288]}
                            ]
                        },
                        as: "agencia",
                    },
                    {
                        model: modelos.instancia_documento,
                        where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [1,4,25],
                        } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [1,4,25],
                        } : objeto.nro_documento ? {
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [1,4,25],
                        } : {
                            id_documento: [1,4,25],
                        },
                        required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                        as: "solicitudes",
                    },
                    {
                        model: modelos.instancia_documento,
                        where: ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [3,6,27],
                        } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [3,6,27],
                        } : objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            id_documento: [3,6,27],
                        } : {
                            id_documento: [3,6,27],
                        },
                        required: ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                        as: "certificados",
                    },
                    { model: modelos.poliza }
                ],
            };
            let objQueryCount = {
                offset: objeto.first ? objeto.first : 0,
                limit: objeto.rows ? objeto.rows : 10,
                where: {
                    $and: [
                        objeto.id_instancia_poliza ? {id: objeto.id_instancia_poliza} : null,
                        objeto.estado ? {id_estado: objeto.estado} : null,
                        objeto.id_poliza ? {id_poliza: objeto.id_poliza} : null,
                        objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                            ? {
                                fecha_registro: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                            } : null
                    ],
                },
                include: [
                    {
                        subQuery: false,
                        model: modelos.asegurado,
                        include: [{
                            subQuery: false,
                            model: modelos.entidad,
                            include: {
                                model: modelos.persona,
                                where: {
                                    $and: [
                                        objeto.persona_primer_nombre ? {
                                            persona_primer_nombre: {
                                                $like: objeto.persona_primer_nombre,
                                            },
                                        } : null,
                                        objeto.persona_primer_apellido ? {
                                            persona_primer_apellido: {
                                                $like: objeto.persona_primer_apellido,
                                            },
                                        } : null,
                                        objeto.persona_segundo_apellido ? {
                                            persona_segundo_apellido: {
                                                $like: objeto.persona_segundo_apellido,
                                            },
                                        } : null,
                                        objeto.persona_doc_id ? {
                                            persona_doc_id: {$like: objeto.persona_doc_id}
                                        } : null,
                                    ],
                                },
                            },
                        },
                        ],
                    },
                    {
                        model: modelos.usuario,
                        where: {
                            $and: [
                                objeto.usuario_login ? {usuario_login: {$like: objeto.usuario_login}} : null
                            ]
                        }
                    },
                    {
                        model: modelos.planPago,
                        subQuery: false,
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.sucursal ? {
                                    id_objeto_x_atributo: [149, 147, 289],
                                    valor: {$like: objeto.sucursal}
                                } : {id_objeto_x_atributo: [149, 147, 289]}
                            ]
                        },
                        as: "sucursal",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.agencia ? {
                                    id_objeto_x_atributo: [148, 146, 288],
                                    valor: {$like: objeto.agencia}
                                } : {id_objeto_x_atributo: [148, 146, 288]}
                            ]
                        },
                        as: "agencia",
                    },
                    ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? {
                        model: modelos.instancia_documento,
                        where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [1,4,25],
                        } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [1,4,25],
                        } : objeto.nro_documento ? {
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [1,4,25],
                        } : {
                            id_documento: [1,4,25],
                        },
                        required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                        as: "solicitudes",
                    } : null,
                    ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? {
                        model: modelos.instancia_documento,
                        where: ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [3,6,27],
                        } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [3,6,27],
                        } : objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            id_documento: [3,6,27],
                        } : {
                            id_documento: [3,6,27],
                        },
                        required: ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                        as: "certificados",
                    } : null,
                    { model: modelos.poliza }
                ]
            };
            objQueryCount.include = objQueryCount.include.filter(Boolean);
            let totalRows = await modelos.instancia_poliza.count(objQueryCount);
            if (totalRows < objQuery.limit) {
                delete objQuery.limit;
                delete objQuery.offset;
            }
            let aData = await modelos.instancia_poliza.findAll(objQuery);
            return [aData, totalRows, objeto.rows, objeto.first];
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByParametrosTarjetas(objeto) {
        try {
            objeto.persona_primer_apellido = objeto.persona_primer_apellido ? (objeto.persona_primer_apellido+'').trim() : '';
            objeto.persona_segundo_apellido = objeto.persona_segundo_apellido ? (objeto.persona_segundo_apellido+'').trim() : '';
            objeto.persona_primer_nombre = objeto.persona_primer_nombre ? (objeto.persona_primer_nombre+'').trim() : '';
            objeto.persona_doc_id = objeto.persona_doc_id ? (objeto.persona_doc_id+'').trim() : '';
            objeto.id_instancia_poliza = objeto.id_instancia_poliza ? objeto.id_instancia_poliza : '';
            objeto.usuario_login = objeto.usuario_login ? (objeto.usuario_login+'').trim() : '';
            objeto.id_certificado = objeto.id_certificado ? (objeto.id_certificado+'').trim() : '';
            objeto.nro_documento = objeto.nro_documento ? (objeto.nro_documento+'').trim() : '';
            objeto.id_poliza = objeto.id_poliza ? objeto.id_poliza : objeto.tipo_poliza;
            objeto.estado = objeto.estado ? (objeto.estado+'').trim() : '';
            objeto.fecha_min = moment(objeto.fecha_min).hour(0).minutes(1).toDate();
            objeto.fecha_max = moment(objeto.fecha_max).hour(23).minutes(59).toDate();

            let objQuery = {
                offset: objeto.first ? objeto.first : 0,
                limit: objeto.rows ? objeto.rows : 10,
                where: {
                    $and: [
                        objeto.id_instancia_poliza ? {id: objeto.id_instancia_poliza} : null,
                        objeto.estado ? {id_estado: objeto.estado} : null,
                        objeto.id_poliza ? {id_poliza: objeto.id_poliza} : null,
                        objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                            ? {
                                fecha_registro: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                            } : null
                    ],
                },
                include: [
                    {
                        model:modelos.planPago
                    },
                    {
                        model: modelos.asegurado,
                        include: [
                            {
                                model: modelos.entidad,
                                include: {
                                    model: modelos.persona,
                                    where: {
                                        $and: [
                                            objeto.persona_primer_nombre ? {
                                                persona_primer_nombre: {
                                                    $like: objeto.persona_primer_nombre,
                                                },
                                            } : null,
                                            objeto.persona_primer_apellido ? {
                                                persona_primer_apellido: {
                                                    $like: objeto.persona_primer_apellido,
                                                },
                                            } : null,
                                            objeto.persona_segundo_apellido ? {
                                                persona_segundo_apellido: {
                                                    $like: objeto.persona_segundo_apellido,
                                                },
                                            } : null,
                                            objeto.persona_doc_id ? {persona_doc_id: {$like: objeto.persona_doc_id}} : null,
                                        ],
                                    },
                                },
                            },
                        ],
                    },
                    {
                        model: modelos.usuario,
                        where: {
                            $and: [
                                objeto.usuario_login ? {usuario_login: {$like: objeto.usuario_login}} : null
                            ]
                        }
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.tipo_pago ? { id_objeto_x_atributo: [36, 60, 375], valor: {$like: objeto.tipo_pago} } : {id_objeto_x_atributo: [36, 60, 375]}
                            ]
                        },
                        as: "tipo_pago",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.sucursal ? {
                                    id_objeto_x_atributo: [163, 165, 394],
                                    valor: {$like: objeto.sucursal}
                                } : {id_objeto_x_atributo: [163, 165, 394]}
                            ]
                        },
                        as: "sucursal",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.agencia ? {
                                    id_objeto_x_atributo: [162, 164, 393],
                                    valor: {$like: objeto.agencia}
                                } : {id_objeto_x_atributo: [162, 164, 393]}
                            ]
                        },
                        as: "agencia",
                    },
                    {
                        model: modelos.instancia_documento,
                        where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [7,10,34],
                        } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [7,10,34],
                        } : objeto.nro_documento ? {
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [7,10,34],
                        } : {
                            id_documento: [7,10,34],
                        },
                        required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                        as: "solicitudes",
                    },
                    {
                        model: modelos.instancia_documento,
                        where:  ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [9,12,36],
                        } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [9,12,36],
                        } : objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            id_documento: [9,12,36],
                        } : {
                            id_documento: [9,12,36],
                        },
                        required: ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                        as: "certificados",
                    },
                    { model: modelos.poliza }
                ],
            };
            let objQueryCount = {
                offset: objeto.first ? objeto.first : 0,
                limit: objeto.rows ? objeto.rows : 10,
                where: {
                    $and: [
                        objeto.id_instancia_poliza ? {id: objeto.id_instancia_poliza} : null,
                        objeto.estado ? {id_estado: objeto.estado} : null,
                        objeto.id_poliza ? {id_poliza: objeto.id_poliza} : null,
                        objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                            ? {
                                fecha_registro: {
                                    $between: [objeto.fecha_min, objeto.fecha_max],
                                },
                            } : null
                    ],
                },
                include: [
                    {
                        model: modelos.asegurado,
                        include: [
                            {
                                model: modelos.entidad,
                                include: {
                                    model: modelos.persona,
                                    where: {
                                        $and: [
                                            objeto.persona_primer_nombre ? {
                                                persona_primer_nombre: {
                                                    $like: objeto.persona_primer_nombre,
                                                },
                                            } : null,
                                            objeto.persona_primer_apellido ? {
                                                persona_primer_apellido: {
                                                    $like: objeto.persona_primer_apellido,
                                                },
                                            } : null,
                                            objeto.persona_segundo_apellido ? {
                                                persona_segundo_apellido: {
                                                    $like: objeto.persona_segundo_apellido,
                                                },
                                            } : null,
                                            objeto.persona_doc_id ? {persona_doc_id: {$like: objeto.persona_doc_id}} : null,
                                        ],
                                    },
                                },
                            },
                        ],
                    },
                    {
                        model: modelos.usuario,
                        where: {
                            $and: [
                                objeto.usuario_login ? {usuario_login: {$like: objeto.usuario_login}} : null
                            ]
                        }
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.tipo_pago ? { id_objeto_x_atributo: [36, 60, 375], valor: {$like: objeto.tipo_pago} } : {id_objeto_x_atributo: [36, 60, 375]}
                            ]
                        },
                        as: "tipo_pago",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.sucursal ? {
                                    id_objeto_x_atributo: [163, 165, 394],
                                    valor: {$like: objeto.sucursal}
                                } : {id_objeto_x_atributo: [163, 165, 394]}
                            ]
                        },
                        as: "sucursal",
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {
                            $and: [
                                objeto.agencia ? {
                                    id_objeto_x_atributo: [162, 164, 393],
                                    valor: {$like: objeto.agencia}
                                } : {id_objeto_x_atributo: [162, 164, 393]}
                            ]
                        },
                        as: "agencia",
                    },
                    ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? {
                        model: modelos.instancia_documento,
                        where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [7,10,34],
                        } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [7,10,34],
                        } : objeto.nro_documento ? {
                            nro_documento: {$like: objeto.nro_documento},
                            id_documento: [7,10,34],
                        } : {
                            id_documento: [7,10,34],
                        },
                        required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                        as: "solicitudes",
                    } : null,
                    ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? {
                        model: modelos.instancia_documento,
                        where:  ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [9,12,36],
                        } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                            fecha_emision: {
                                $between: [objeto.fecha_min, objeto.fecha_max],
                            },
                            id_documento: [9,12,36],
                        } : objeto.id_certificado ? {
                            nro_documento: {$like: objeto.id_certificado},
                            id_documento: [9,12,36],
                        } : {
                            id_documento: [9,12,36],
                        },
                        required: ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                        as: "certificados",
                    } : null,
                    { model: modelos.poliza }
                ]
            };
            objQueryCount.include = objQueryCount.include.filter(Boolean);
            let totalRows = await modelos.instancia_poliza.count(objQueryCount);
            if (totalRows < objQuery.limit) {
                delete objQuery.limit;
                delete objQuery.offset;
            }
            let aData = await modelos.instancia_poliza.findAll(objQuery);
            return [aData, totalRows, objeto.rows, objeto.first];
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByParametrosEcoMedic(objeto) {
        try {
            objeto.nro_documento = objeto.nro_documento ? (objeto.nro_documento+'').trim() : '';
            objeto.persona_primer_apellido = objeto.persona_primer_apellido ? (objeto.persona_primer_apellido+'').trim() : '';
            objeto.persona_segundo_apellido = objeto.persona_segundo_apellido ? (objeto.persona_segundo_apellido+'').trim() : '';
            objeto.persona_primer_nombre = objeto.persona_primer_nombre ? (objeto.persona_primer_nombre+'').trim() : '';
            objeto.persona_doc_id = objeto.persona_doc_id ? (objeto.persona_doc_id+'').trim() : '';
            objeto.id_instancia_poliza = objeto.id_instancia_poliza ? objeto.id_instancia_poliza : '';
            objeto.id_certificado = objeto.id_certificado ? (objeto.id_certificado+'').trim() : '';
            objeto.usuario_login = objeto.usuario_login ? (objeto.usuario_login+'').trim() : '';
            objeto.fecha_min = moment(objeto.fecha_min).hour(0).minutes(1).toDate();
            objeto.fecha_max = moment(objeto.fecha_max).hour(23).minutes(59).toDate();

            let objQuery = {
                offset:objeto.first ? objeto.first : 0,
                limit:objeto.rows ? objeto.rows : 10,
                        where: {
                            $and: [
                                objeto.id_instancia_poliza ? { id: objeto.id_instancia_poliza } : null,
                                objeto.estado ? { id_estado: objeto.estado } : null,
                                objeto.id_poliza ? { id_poliza: objeto.id_poliza } : null,
                                objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                                    ? {
                                        fecha_registro: {
                                            $between: [objeto.fecha_min, objeto.fecha_max],
                                        },
                                    } : null
                            ],
                        },
                        include: [
                            {
                                model: modelos.planPago
                            },
                            {
                                model: modelos.asegurado,
                                include: [
                                    {
                                        model: modelos.entidad,
                                        include: {
                                            model: modelos.persona,
                                            where: {
                                                $and: [
                                                    objeto.persona_primer_nombre ? {
                                                        persona_primer_nombre: {
                                                            $like: objeto.persona_primer_nombre,
                                                        },
                                                    } : null,
                                                    objeto.persona_primer_apellido ? {
                                                        persona_primer_apellido: {
                                                            $like: objeto.persona_primer_apellido,
                                                        },
                                                    } : null,
                                                    objeto.persona_segundo_apellido ? {
                                                        persona_segundo_apellido: {
                                                            $like: objeto.persona_segundo_apellido,
                                                        },
                                                    } : null,
                                                    objeto.persona_doc_id ? { persona_doc_id: { $like: objeto.persona_doc_id } } : null,
                                                ],
                                            },
                                        },
                                    },
                                ],
                            },
                            {
                                model: modelos.usuario,
                                where: {
                                    $and: [
                                        objeto.usuario_login ? {usuario_login: {$like: objeto.usuario_login}} : null
                                    ],
                                }
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                where: {
                                    $and: [
                                        objeto.plan ? {id_objeto_x_atributo: [110, 258], valor: { $like: objeto.plan } } : {id_objeto_x_atributo: [110, 258] }
                                    ]
                                },
                                as: "plan",
                                required: objeto.plan ? true : false,
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                where: {
                                    $and: [
                                        objeto.tipo_pago ? {id_objeto_x_atributo: [90, 242], valor: { $like: objeto.tipo_pago } } : {id_objeto_x_atributo: [90, 242]}
                                    ]
                                },
                                as: "tipo_pago",
                                required: objeto.tipo_pago ? true : false,
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                where: {
                                    $and: [
                                        objeto.sucursal ? {id_objeto_x_atributo: [138, 261, 163], valor: { $like: objeto.sucursal } } : {id_objeto_x_atributo: [138, 261, 163] }
                                    ]
                                },
                                as: "sucursal",
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                where: {
                                    $and: [
                                        objeto.agencia ? {id_objeto_x_atributo: [137, 260, 162], valor: { $like: objeto.agencia } } : {id_objeto_x_atributo: [137, 260, 162] }
                                    ]
                                },
                                as: "agencia",
                            },
                            {
                                model: modelos.instancia_documento,
                                where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                                    fecha_emision: {
                                        $between: [objeto.fecha_min, objeto.fecha_max],
                                    },
                                    nro_documento: {$like: objeto.nro_documento},
                                    id_documento: [13,22],
                                } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                                    fecha_emision: {
                                        $between: [objeto.fecha_min, objeto.fecha_max],
                                    },
                                    id_documento: [13,22],
                                } : objeto.nro_documento ? {
                                    nro_documento: {$like: objeto.nro_documento},
                                    id_documento: [13,22],
                                } : {
                                    id_documento: [13,22],
                                },
                                required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                                as: "solicitudes",
                            },
                            {
                                model: modelos.instancia_documento,
                                where:  ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                                    nro_documento: {$like: objeto.id_certificado},
                                    fecha_emision: {
                                        $between: [objeto.fecha_min, objeto.fecha_max],
                                    },
                                    id_documento: [15,24],
                                } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                                    fecha_emision: {
                                        $between: [objeto.fecha_min, objeto.fecha_max],
                                    },
                                    id_documento: [15,24],
                                } : objeto.id_certificado ? {
                                    nro_documento: {$like: objeto.id_certificado},
                                    id_documento: [15,24],
                                } : {
                                    id_documento: [15,24],
                                },
                                required: ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                                as: "certificados",
                            },
                            {
                                model: modelos.instancia_documento,
                                include: {
                                    model:modelos.documento
                                }
                            },
                            {
                                model: modelos.poliza,
                            }
                ],
            };
            let objQueryCount = {
                offset:objeto.first ? objeto.first : 0,
                limit:objeto.rows ? objeto.rows : 10,
                        where: {
                            $and: [
                                objeto.id_instancia_poliza ? { id: objeto.id_instancia_poliza } : null,
                                objeto.estado ? { id_estado: objeto.estado } : null,
                                objeto.id_poliza ? { id_poliza: objeto.id_poliza } : null,
                                objeto.fecha_min && objeto.fecha_max && ['fecha_registro'].includes(objeto.campo_fecha)
                                    ? {
                                        fecha_registro: {
                                            $between: [objeto.fecha_min, objeto.fecha_max],
                                        },
                                    } : null
                            ],
                        },
                        include: [
                            {
                                model: modelos.asegurado,
                                include: [
                                    {
                                        model: modelos.entidad,
                                        include: {
                                            model: modelos.persona,
                                            where: {
                                                $and: [
                                                    objeto.persona_primer_nombre ? {
                                                        persona_primer_nombre: {
                                                            $like: objeto.persona_primer_nombre,
                                                        },
                                                    } : null,
                                                    objeto.persona_primer_apellido ? {
                                                        persona_primer_apellido: {
                                                            $like: objeto.persona_primer_apellido,
                                                        },
                                                    } : null,
                                                    objeto.persona_segundo_apellido ? {
                                                        persona_segundo_apellido: {
                                                            $like: objeto.persona_segundo_apellido,
                                                        },
                                                    } : null,
                                                    objeto.persona_doc_id ? { persona_doc_id: { $like: objeto.persona_doc_id } } : null,
                                                ],
                                            },
                                        },
                                    },
                                ],
                            },
                            {
                                model: modelos.usuario,
                                where: {
                                    $and: [
                                        objeto.usuario_login ? {usuario_login: {$like: objeto.usuario_login}} : null
                                    ],
                                }
                            },
                            {
                                model: modelos.planPago,
                                subQuery: false,
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                where: {
                                    $and: [
                                        objeto.plan ? {id_objeto_x_atributo: [110, 258], valor: { $like: objeto.plan } } : {id_objeto_x_atributo: [110, 258] }
                                    ]
                                },
                                as: "plan",
                                required: objeto.plan ? true : false,
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                where: {
                                    $and: [
                                        objeto.tipo_pago ? {id_objeto_x_atributo: [90, 242], valor: { $like: objeto.tipo_pago } } : {id_objeto_x_atributo: [90, 242] }
                                    ]
                                },
                                as: "tipo_pago",
                                required: objeto.tipo_pago ? true : false,
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                where: {
                                    $and: [
                                        objeto.sucursal ? {id_objeto_x_atributo: [138, 261, 163], valor: { $like: objeto.sucursal } } : {id_objeto_x_atributo: [138, 261, 163] }
                                    ]
                                },
                                as: "sucursal",
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                where: {
                                    $and: [
                                        objeto.agencia ? {id_objeto_x_atributo: [137, 260, 162], valor: { $like: objeto.agencia } } : {id_objeto_x_atributo: [137, 260, 162] }
                                    ]
                                },
                                as: "agencia",
                            },
                            ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? {
                                model: modelos.instancia_documento,
                                where: ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.nro_documento ? {
                                    fecha_emision: {
                                        $between: [objeto.fecha_min, objeto.fecha_max],
                                    },
                                    nro_documento: {$like: objeto.nro_documento},
                                    id_documento: [13,22],
                                } : ['fecha_solicitud'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                                    fecha_emision: {
                                        $between: [objeto.fecha_min, objeto.fecha_max],
                                    },
                                    id_documento: [13,22],
                                } : objeto.nro_documento ? {
                                    nro_documento: {$like: objeto.nro_documento},
                                    id_documento: [13,22],
                                } : {
                                    id_documento: [13,22],
                                },
                                required: ['fecha_solicitud'].includes(objeto.campo_fecha) || objeto.nro_documento ? true : false,
                                as: "solicitudes",
                            } : null,
                            ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? {
                                model: modelos.instancia_documento,
                                where:  ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max && objeto.id_certificado ? {
                                    nro_documento: {$like: objeto.id_certificado},
                                    fecha_emision: {
                                        $between: [objeto.fecha_min, objeto.fecha_max],
                                    },
                                    id_documento: [15,24],
                                } : ['fecha_emision'].includes(objeto.campo_fecha) && objeto.fecha_min && objeto.fecha_max ? {
                                    fecha_emision: {
                                        $between: [objeto.fecha_min, objeto.fecha_max],
                                    },
                                    id_documento: [15,24],
                                } : objeto.id_certificado ? {
                                    nro_documento: {$like: objeto.id_certificado},
                                    id_documento: [15,24],
                                } : {
                                    id_documento: [15,24],
                                },
                                required: ['fecha_emision'].includes(objeto.campo_fecha) || objeto.id_certificado ? true : false,
                                as: "certificados",
                            } : null,
                            { model: modelos.poliza }
                        ]
            };
            objQueryCount.include = objQueryCount.include.filter(Boolean);
            let totalRows = await modelos.instancia_poliza.count(objQueryCount);
            if (totalRows < objQuery.limit) {
                delete objQuery.limit;
                delete objQuery.offset;
            }
            let aData = await modelos.instancia_poliza.findAll(objQuery);
            return [aData,totalRows,objeto.rows,objeto.first];
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }


    static async listaAllByEstado(tipo_poliza, estado, id_documento) {
        try {
            return await modelos.sequelize.query(
                    `select G.parametro_abreviacion persona_doc_id_ext,H.parametro_descripcion estado, I.nro_documento,F.parametro_descripcion sexo,D.persona_direccion_domicilio,D.persona_celular,D.persona_telefono_domicilio, A.id,A.id_poliza,A.id_estado,FORMAT(A.fecha_registro,'yyyy/MM/dd') fecha_registro,D.id id_persona,D.persona_doc_id,D.persona_doc_id_comp,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,Z.descripcion,K.parametro_descripcion tipo_tarjeta from instancia_poliza A inner join poliza Z on Z.id=A.id_poliza
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on B.id_entidad=C.id 
    inner join persona D on C.id_persona=D.id 
	inner join usuario E on A.adicionada_por=E.id
	inner join parametro F on F.id=D.par_sexo_id
    inner join parametro G on D.persona_doc_id_ext=G.parametro_cod and G.diccionario_id=18
    inner join parametro H on A.id_estado=H.id
    inner join instancia_documento I on I.id_instancia_poliza=A.id and I.id_documento=${id_documento}
    inner join atributo_instancia_poliza J on A.id=J.id_instancia_poliza and J.id_objeto_x_atributo in (86,87)
    inner join parametro K on K.diccionario_id = 33 and J.valor=K.id
    and A.id_poliza = ${tipo_poliza}
    and A.id_estado=${estado}`
                )
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByIds(ids) {
        try {
            let objQuery = {
                            where: {
                                id: ids,
                            },
                            include: [
                                {
                                    model: modelos.asegurado,
                                    include: [
                                        {
                                            model: modelos.entidad,
                                            include: {
                                                model: modelos.persona,
                                            },
                                        },
                                    ],
                                },
                                {
                                    model: modelos.instancia_documento,
                                    where: { id_documento: [1, 4, 25] },
                                    as: "solicitudes",
                                },
                                {
                                    model: modelos.instancia_documento,
                                    where: { id_documento: [3, 6, 27] },
                                    as: "certificados",
                                    required: false,
                                },
                                {
                                    model: modelos.usuario,
                                },
                                {
                                    model: modelos.atributo_instancia_poliza,
                                    where: { id_objeto_x_atributo: [149, 147, 289] },
                                    as: "sucursal",
                                    required: false,
                                },
                                {
                                    model: modelos.atributo_instancia_poliza,
                                    where: { id_objeto_x_atributo: [148, 146, 288] },
                                    as: "agencia",
                                    required: false,
                                },
                            ],
                    order: [["id_estado", "DESC"]],
                };
            let totalRows = await modelos.instancia_poliza.count(objQuery);
            let aData = await modelos.instancia_poliza.findAll(objQuery);
            return [aData,totalRows,totalRows,0]
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByIdsEcoVida(ids) {
        try {
            let objQuery = {
                where: {
                    id: ids,
                },
                include: [
                    {
                        model: modelos.asegurado,
                        include: [
                            {
                                model: modelos.entidad,
                                include: {
                                    model: modelos.persona,
                                },
                            },
                        ],
                    },
                    {
                        model: modelos.instancia_documento,
                        where: {id_documento: [16, 19, 25]},
                        as: "solicitudes",
                    },
                    {
                        model: modelos.instancia_documento,
                        where: {id_documento: [18, 21, 27]},
                        as: "certificados",
                        required: false,
                    },
                    {
                        model: modelos.usuario,
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {id_objeto_x_atributo: [192, 217, 289]},
                        as: "sucursal",
                        required: false,
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: {id_objeto_x_atributo: [191, 216, 288]},
                        as: "agencia",
                        required: false,
                    },
                    {
                        model: modelos.poliza
                    }
                ],
                order: [["id_estado", "DESC"]],
            };
            let totalRows = await modelos.instancia_poliza.count(objQuery);
            let aData = await modelos.instancia_poliza.findAll(objQuery);
            return [aData,totalRows,totalRows,0]
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByIdsDesgravamen(ids) {
        try {
            return [];

        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByIdsEcoAccidentes(ids) {
        try {
            return await modelos.poliza.findAll({
                include: [
                    {
                        model: modelos.instancia_poliza,
                        where: {
                            id: ids,
                        },
                        include: [
                            {
                                model: modelos.asegurado,
                                include: [
                                    {
                                        model: modelos.entidad,
                                        include: {
                                            model: modelos.persona,
                                        },
                                    },
                                ],
                            },
                            {
                                model: modelos.instancia_documento,
                                include:{model:modelos.documento, where: {id_tipo_documento:279}},
                                as: "solicitudes",
                            },
                            {
                                model: modelos.instancia_documento,
                                include:{model:modelos.documento, where: {id_tipo_documento:281}},
                                as: "certificados",
                                required: false,
                            },
                            {
                                model: modelos.usuario,
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                include:{model:modelos.objeto_x_atributo,
                                    include:{ model:modelos.atributo, where:{id:60},required: false}
                                },
                                as: "sucursal",
                                required: false,
                            },
                            {
                                model: modelos.atributo_instancia_poliza,
                                include:{model:modelos.objeto_x_atributo,
                                    include:{ model:modelos.atributo, where:{id:59},required: false}
                                },
                                as: "agencia",
                                required: false,
                            },
                        ],
                    },
                ],
                order: [[modelos.instancia_poliza, "id_estado", "DESC"]],
            });
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetAllByIdsEcoMedic(ids) {
        try {
            let objQuery = {
                            where: { id: ids },
                            include: [
                                {
                                    model: modelos.asegurado,
                                    include: [
                                        {
                                            model: modelos.entidad,
                                            include: {
                                                model: modelos.persona,
                                            },
                                        },
                                    ],
                                },
                                {
                                    model: modelos.instancia_documento,
                                    include:{
                                        model:modelos.documento
                                    }
                                },
                                {
                                    model: modelos.instancia_documento,
                                    where: { id_documento: [13, 22] },
                                    as: "solicitudes",
                                },
                                {
                                    model: modelos.instancia_documento,
                                    where: { id_documento: [15, 24] },
                                    as: "certificados",
                                    required: false,
                                },
                                {
                                    model: modelos.usuario,
                                },
                                {
                                    model: modelos.atributo_instancia_poliza,
                                    where: { id_objeto_x_atributo: [110, 258] },
                                    as: "plan",
                                },
                                {
                                    model: modelos.atributo_instancia_poliza,
                                    where: { id_objeto_x_atributo: [138, 261] },
                                    as: "sucursal",
                                    required: false,
                                },
                                {
                                    model: modelos.atributo_instancia_poliza,
                                    where: { id_objeto_x_atributo: [137, 260] },
                                    as: "agencia",
                                    required: false,
                                },
                                {
                                    model: modelos.poliza
                                }
                            ],
                    order: [["id_estado", "DESC"]]
                };
            let totalRows = await modelos.instancia_poliza.count(objQuery);
            let aData = await modelos.instancia_poliza.findAll(objQuery);
            return [aData,totalRows,totalRows,0];
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }


    static async GetAllByIdsTarjetas(ids) {
        try {
            let objQuery = {
                            where: {
                                id: ids,
                            },
                            include: [
                                {
                                    model: modelos.asegurado,
                                    include: [
                                        {
                                            model: modelos.entidad,
                                            include: {
                                                model: modelos.persona,
                                            },
                                        },
                                    ],
                                },
                                {
                                    model: modelos.instancia_documento,
                                    where: { id_documento: [7, 10, 34] },
                                    as: "solicitudes",
                                },
                                {
                                    model: modelos.instancia_documento,
                                    where: { id_documento: [9, 12, 36] },
                                    as: "certificados",
                                    required: false,
                                },
                                {
                                    model: modelos.usuario,
                                },
                                {
                                    model: modelos.planPago,
                                    subQuery: false,
                                    required: false,
                                },
                                {
                                    model: modelos.atributo_instancia_poliza,
                                    where: { id_objeto_x_atributo: [163, 165, 394] },
                                    as: "sucursal",
                                    required: false,
                                },
                                {
                                    model: modelos.atributo_instancia_poliza,
                                    where: { id_objeto_x_atributo: [162, 164, 393] },
                                    as: "agencia",
                                    required: false,
                                },
                                {
                                    model: modelos.poliza
                                }
                            ],
                order: [["id_estado", "DESC"]]
                };
            let totalRows = await modelos.instancia_poliza.count(objQuery);
            let aData = await modelos.instancia_poliza.findAll(objQuery);
            return [aData,totalRows,totalRows,0];
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async GetById(id) {
        try {
            let instancia = { instancia_poliza: "", titular: "", beneficiario: "" };
            instancia.instancia_poliza = await modelos.instancia_poliza.findById(id)
            let result = await modelos.sequelize.query(
                    `select C.*,A.id id_asegurado from asegurado A inner join entidad B on A.id_entidad=B.id
                                    inner join persona C on C.id=B.id_persona and A.id_instancia_poliza=${id}`
                );
            instancia.titular = result[0][0];
            let result2 = await modelos.sequelize.query(
                    `select D.* from beneficiario A inner join asegurado B on A.id_asegurado=B.id
                inner join entidad C on A.id_beneficiario=C.id
                inner join persona D on C.id_persona=D.id and B.id_instancia_poliza=${id} and A.id_asegurado=${result[0][0].id_asegurado}`
                );
            instancia.beneficiario = result2[0];
            return instancia;
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async UpdateInstanciaToNextStatus(asegurado, statusIds, id_usuario) {
        try {
            let usuario = null;
            let resp = await modelos.usuario.findOne({
                where: { id: id_usuario },
                include: {
                    model: modelos.usuario_x_rol,
                    include: {
                        model: modelos.rol,
                    }
                },
            });
            if (Object.keys(resp.dataValues).length) {
                usuario = resp.dataValues;
            }
            return await this.updateAseguradoInstanciaPolizaToNextStatus(
                statusIds,
                asegurado,
                usuario
            );
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async updateInstanciaToSolicitado(statusIds, instanciaPoliza) {
        try {
            const statusCycle = statusIds;
            let resp = await modelos.parametro.findOne({ where: { id: statusCycle[1], diccionario_id: 11 } });
            let parametro = resp.dataValues;
            if (Object.keys(parametro).length) {
                let resp2 = await modelos.instancia_poliza.update({
                    id_estado: parametro.id }, {
                    where: {id: instanciaPoliza.id, id_poliza: instanciaPoliza.id_poliza}
                });
                return parametro;
            }
        } catch (e) {
            console.log(e)
            loggerCatch.info(new Date(), e);
        }
    }

    static async verifyCouldBeRenovated(idPoliza, idInstanciaPoliza, callback = null) {
        try {
            if (idInstanciaPoliza) {
                let momDateToFind,
                    momDateWaitAfterDays,
                    momDateWaitBeforeDays,
                    dateToFind,
                    dateWaitAfterDays,
                    dateWaitBeforeDays;
                let poliza = await modelos.poliza.findOne({
                    where: { id: idPoliza },
                });
                let numDaysWaitAfter = poliza.dias_espera_renovacion;
                let numDaysWaitBefore = poliza.dias_anticipado_renovacion;
                momDateToFind = moment(new Date()).subtract(1, "days");
                momDateWaitBeforeDays = moment(new Date())
                    .subtract(1, "days")
                    .subtract(numDaysWaitBefore, "days");
                momDateWaitAfterDays = moment(new Date())
                    .subtract(1, "days")
                    .add(numDaysWaitAfter, "days");
                dateToFind = momDateToFind.toDate();
                dateWaitBeforeDays = momDateWaitBeforeDays.toDate();
                dateWaitAfterDays = momDateWaitAfterDays.toDate();
                try {
                    return await modelos.asegurado.find({
                            where: { id_instancia_poliza: idInstanciaPoliza },
                            include: [
                                {
                                    model: modelos.instancia_poliza,
                                    include: [
                                        {
                                            model: modelos.instancia_documento,
                                            where: {
                                                $and: [
                                                    // {fecha_fin_vigencia: {$lte:dateToFind}},
                                                    { fecha_fin_vigencia: { $gte: dateWaitBeforeDays } },
                                                    { fecha_fin_vigencia: { $lte: dateWaitAfterDays } },
                                                ],
                                            },
                                        },
                                    ],
                                },
                            ],
                        });
                } catch (e) {
                    console.log(e)
                }
            }
            if (typeof callback == 'function') {
                await callback();
            }
        } catch (e) {
            console.log(e)
            loggerCatch.info(new Date(), e);
        }
    }
//
    static async createInstanciaDocumento(asegurado,instancia_poliza,new_instancia_documento, indexDoc,usuario) {
        try {
            let query;
            let hoy = new Date();
            let momToday = moment(hoy);
            let createdAt = new Date();
            let fechaFinVigencia;
            let fechaRegistro = instancia_poliza.fecha_registro;
            let operacionPlazo = instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == 82);
            operacionPlazo = operacionPlazo ? operacionPlazo : instancia_poliza.atributo_instancia_polizas_inter.find(param => param.objeto_x_atributo.id_atributo == 82);
            let operacionPlazoFrecuencia = instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == 84);
            operacionPlazoFrecuencia = operacionPlazoFrecuencia ? operacionPlazoFrecuencia : instancia_poliza.atributo_instancia_polizas_inter.find(param => param.objeto_x_atributo.id_atributo == 84);
            if (operacionPlazo && operacionPlazoFrecuencia) {
                if (operacionPlazoFrecuencia.valor.toLowerCase().includes('mes')) {
                    fechaFinVigencia = moment(new Date()).add('month',parseInt(operacionPlazo.valor) > 60 ? 60 : parseInt(operacionPlazo.valor)).toDate();
                } else if(operacionPlazoFrecuencia.valor.toLowerCase().includes('año')) {
                    fechaFinVigencia = moment(new Date()).add('year',parseInt(operacionPlazo.valor) > 5 ? 5 : parseInt(operacionPlazo.valor)).toDate();
                }
            } else {
                fechaFinVigencia = momToday.add(instancia_poliza.poliza.vigencia_meses, "months").toDate();
            }
            let estado = instancia_poliza.estado;
            let poliza = instancia_poliza.poliza;
            let nroUnificado = 0;
            let id_documento = new_instancia_documento.documento ? new_instancia_documento.documento.id : new_instancia_documento.id_documento ? new_instancia_documento.id_documento: null;
            let persona = await modelos.persona.findOne({where:{id:asegurado.entidad.persona.id}});
            let documento = await modelos.documento.findOne({ where: { id: id_documento } });
            if (documento) {
                if (poliza.tipo_numeracion.id == parNumeracionUnificada.id && instancia_poliza.instancia_documentos.length) {
                    new_instancia_documento.nro_documento = instancia_poliza.instancia_documentos[0].nro_documento;
                } else {
                    new_instancia_documento.nro_documento = documento.nro_actual;
                }
                new_instancia_documento.id = null;
                new_instancia_documento.id_instancia_poliza = instancia_poliza.id;
                new_instancia_documento.id_documento = documento.id;
                new_instancia_documento.id_documento_version = documento.id_documento_version;
                new_instancia_documento.fecha_emision = createdAt;
                new_instancia_documento.fecha_inicio_vigencia = createdAt;
                new_instancia_documento.fecha_fin_vigencia = fechaFinVigencia;
                new_instancia_documento.asegurado_doc_id = asegurado.entidad.persona.persona_doc_id;
                new_instancia_documento.asegurado_doc_id_ext = asegurado.entidad.persona.persona_doc_id_ext ? asegurado.entidad.persona.persona_doc_id_ext : persona.persona_doc_id_ext;
                new_instancia_documento.asegurado_nombre1 = asegurado.entidad.persona.persona_primer_nombre;
                new_instancia_documento.asegurado_nombre2 = asegurado.entidad.persona.persona_segundo_nombre;
                new_instancia_documento.asegurado_apellido1 = asegurado.entidad.persona.persona_primer_apellido;
                new_instancia_documento.asegurado_apellido2 = asegurado.entidad.persona.persona_segundo_apellido;
                new_instancia_documento.tipo_persona = asegurado.entidad.id;
                new_instancia_documento.adicionada_por = usuario != null ? usuario.id : "2";
                new_instancia_documento.modificada_por = usuario != null ? usuario.id : "2";
                new_instancia_documento.createdAt = createdAt;
                new_instancia_documento.updatedAt = createdAt;
                let instancia_documento = await modelos.instancia_documento.create(new_instancia_documento);
                if (instancia_documento) {
                    if (poliza.tipo_numeracion.id == parNumeracionIndependiente.id) {
                        documento.nro_actual++;
                    }
                    documento.modificado_por = usuario != null ? usuario.id : "2";
                    documento.updatedAt = createdAt;
                    let resp = await modelos.documento.update({
                        nro_actual: documento.nro_actual,
                        modificado_por: usuario != null ? usuario.id : "2",
                        updatedAt: documento.updatedAt,
                    }, { where: { id: documento.id } })
                }
                instancia_documento.instancia_poliza = new_instancia_documento.instancia_poliza;
                instancia_documento.documento = new_instancia_documento.documento;
                instancia_documento.atributo_instancia_documentos = [];
                for (let i = 0; i < new_instancia_documento.atributo_instancia_documentos.length; i++) {
                    let new_atributo_instancia_documento = new_instancia_documento.atributo_instancia_documentos[i];
                    new_atributo_instancia_documento.id = null;
                    new_atributo_instancia_documento.id_instancia_poliza = instancia_poliza.id;
                    new_atributo_instancia_documento.adicionado_por = usuario != null ? usuario.id : "2";
                    new_atributo_instancia_documento.modificado_por = usuario != null ? usuario.id : "2";
                    new_atributo_instancia_documento.createdAt = createdAt;
                    new_atributo_instancia_documento.updatedAt = createdAt;
                    let resp2 =  await modelos.atributo_instancia_documento.create(new_atributo_instancia_documento, sequelizeAttr)
                    let atributo_instancia_documento = resp2.dataValues;
                    await instancia_documento.atributo_instancia_documentos.push({
                        id: atributo_instancia_documento.id,
                        id_instancia_documento: atributo_instancia_documento.id_instancia_documento,
                        id_objeto_x_atributo: atributo_instancia_documento.id_objeto_x_atributo,
                        valor: atributo_instancia_documento.valor,
                        tipo_error: atributo_instancia_documento.tipo_error,
                        objeto_x_atributo: new_atributo_instancia_documento.objeto_x_atributo,
                    });
                }
                instancia_poliza.instancia_documentos[indexDoc] = {
                    id: instancia_documento.id,
                    id_instancia_poliza: instancia_documento.id_instancia_poliza,
                    id_documento: instancia_documento.id_documento,
                    id_documento_version: instancia_documento.id_documento_version,
                    nro_documento: instancia_documento.nro_documento,
                    fecha_emision: instancia_documento.fecha_emision,
                    fecha_inicio_vigencia: instancia_documento.fecha_inicio_vigencia,
                    fecha_fin_vigencia: instancia_documento.fecha_fin_vigencia,
                    asegurado_doc_id: instancia_documento.asegurado_doc_id,
                    asegurado_doc_id_ext: instancia_documento.asegurado_doc_id_ext,
                    asegurado_nombre1: instancia_documento.asegurado_nombre1,
                    asegurado_nombre2: instancia_documento.asegurado_nombre2,
                    asegurado_apellido1: instancia_documento.asegurado_apellido1,
                    asegurado_apellido2: instancia_documento.asegurado_apellido2,
                    asegurado_apellido3: instancia_documento.asegurado_apellido3,
                    tipo_persona: instancia_documento.tipo_persona,
                    adicionada_por: instancia_documento.adicionada_por,
                    modificada_por: instancia_documento.modificada_por,
                    createdAt: instancia_documento.createdAt,
                    updatedAt: instancia_documento.updatedAt,
                    atributo_instancia_documentos: instancia_documento.atributo_instancia_documentos,
                    atributo_instancia_documentos_inter: new_instancia_documento.atributo_instancia_documentos_inter,
                };
                return instancia_poliza;
            } else {
                loggerCatch.info(new Date(), new_instancia_documento);
                return null;
            }
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async updateInstanciaDocumento (
        asegurado,
        instancia_poliza,
        new_instancia_documento,
        indexDoc,
        usuario
    ) {
        try {
            let hoy = new Date();
            let query;
            let updatedAt = new Date();
            let estado = instancia_poliza.estado;
            let poliza = instancia_poliza.poliza;
            let documento = new_instancia_documento.documento;
            new_instancia_documento.modificada_por = usuario != null ? usuario.id : "2";
            new_instancia_documento.updatedAt = updatedAt;
            let seqQuery = { where: { id: new_instancia_documento.id } };
            query = seqQuery;
            let resp = await modelos.instancia_documento.update(new_instancia_documento, query);
            if (resp.length && resp[0]) {
                for (let i = 0; i < new_instancia_documento.atributo_instancia_documentos.length; i++) {
                    let new_atributo_instancia_documento = new_instancia_documento.atributo_instancia_documentos[i];
                    if (new_atributo_instancia_documento != null) {
                        if (new_atributo_instancia_documento.id == null) {
                            new_atributo_instancia_documento.id = null;
                            new_atributo_instancia_documento.id_instancia_poliza = instancia_poliza.id;
                            new_atributo_instancia_documento.adicionado_por = usuario != null ? usuario.id : "2";
                            new_atributo_instancia_documento.modificado_por = usuario != null ? usuario.id : "2";
                            // new_atributo_instancia_documento.createdAt = updatedAt;
                            new_atributo_instancia_documento.updatedAt = updatedAt;
                            let atributo_instancia_documento = await modelos.atributo_instancia_documento.create(new_atributo_instancia_documento, sequelizeAttr)
                            await new_instancia_documento.atributo_instancia_documentos.push({
                                id: atributo_instancia_documento.id,
                                id_instancia_poliza: atributo_instancia_documento.id_instancia_poliza,
                                id_objeto_x_atributo: atributo_instancia_documento.id_objeto_x_atributo,
                                valor: atributo_instancia_documento.valor,
                                tipo_error: atributo_instancia_documento.tipo_error,
                                objeto_x_atributo: new_atributo_instancia_documento.objeto_x_atributo,
                            });
                        } else {
                            new_atributo_instancia_documento.modificado_por = usuario != null ? usuario.id : "2";
                            new_atributo_instancia_documento.updatedAt = updatedAt;
                            let seQuery = {where: { id: new_atributo_instancia_documento.id },};
                            query = seQuery;
                            let resp2 = await modelos.atributo_instancia_documento.update(new_atributo_instancia_documento, query)
                            if (resp2.length && resp2[0]) {
                                new_instancia_documento.atributo_instancia_documentos[index] = {
                                    id: atributo_instancia_documento.id,
                                    id_instancia_poliza: atributo_instancia_documento.id_instancia_poliza,
                                    id_objeto_x_atributo: atributo_instancia_documento.id_objeto_x_atributo,
                                    valor: atributo_instancia_documento.valor,
                                    tipo_error: atributo_instancia_documento.tipo_error,
                                    objeto_x_atributo: new_atributo_instancia_documento.objeto_x_atributo
                                };
                            }
                        }
                    }
                }
                //new_instancia_documento.atributo_instancia_documentos_inter = new_instancia_documento.atributo_instancia_documentos_inter;
                instancia_poliza.instancia_documentos[indexDoc] = {
                    id: new_instancia_documento.id,
                    id_instancia_poliza: new_instancia_documento.id_instancia_poliza,
                    id_documento: new_instancia_documento.id_documento,
                    id_documento_version: new_instancia_documento.id_documento_version,
                    nro_documento: new_instancia_documento.nro_documento,
                    fecha_emision: new_instancia_documento.fecha_emision,
                    fecha_inicio_vigencia: new_instancia_documento.fecha_inicio_vigencia,
                    fecha_fin_vigencia: new_instancia_documento.fecha_fin_vigencia,
                    asegurado_doc_id: new_instancia_documento.asegurado_doc_id,
                    asegurado_doc_id_ext: new_instancia_documento.asegurado_doc_id_ext,
                    asegurado_nombre1: new_instancia_documento.asegurado_nombre1,
                    asegurado_nombre2: new_instancia_documento.asegurado_nombre2,
                    asegurado_apellido1: new_instancia_documento.asegurado_apellido1,
                    asegurado_apellido2: new_instancia_documento.asegurado_apellido2,
                    asegurado_apellido3: new_instancia_documento.asegurado_apellido3,
                    tipo_persona: new_instancia_documento.tipo_persona,
                    adicionada_por: new_instancia_documento.adicionada_por,
                    modificada_por: new_instancia_documento.modificada_por,
                    createdAt: new_instancia_documento.createdAt,
                    updatedAt: new_instancia_documento.updatedAt,
                    atributo_instancia_documentos:
                    new_instancia_documento.atributo_instancia_documentos,
                };
            }
            return instancia_poliza;
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async updateTransicion(instancia_poliza_transicion, usuario) {
        try {
            let query;
            let hoy = new Date();
            let modifiedAt = new Date();
            instancia_poliza_transicion.modificado_por = usuario != null ? usuario.id : "2";
            instancia_poliza_transicion.updatedAt = modifiedAt;
            delete instancia_poliza_transicion.createdAt;
            let seQuery = { where: { id: instancia_poliza_transicion.id } };
            query = seQuery;
            await modelos.instancia_poliza_transicion.update(instancia_poliza_transicion, query)
            return instancia_poliza_transicion;
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async createTransicion(
        instancia_poliza_transicion,
        usuario,
        sequelizeAttr = null
    ) {
        try {
            let hoy = new Date();
            let createdAt = new Date();
            let newInstanciaPolizaTransaccion = {
                id_instancia_poliza: instancia_poliza_transicion.id_instancia_poliza,
                observacion: instancia_poliza_transicion.observacion,
                par_observacion_id: instancia_poliza_transicion.par_observacion_id,
                par_estado_id: instancia_poliza_transicion.par_estado_id,
                adicionado_por: usuario != null ? usuario.id : "2",
                modificado_por: usuario != null ? usuario.id : "2",
                updatedAt: createdAt,
                createdAt: createdAt,
            };
            let oldInstanciaTransicion = await modelos.instancia_poliza_transicion.find({
                where: {
                    par_estado_id: instancia_poliza_transicion.par_estado_id,
                    observacion: { $like: instancia_poliza_transicion.observacion },
                    id: instancia_poliza_transicion.id,
                },
            })
            if (oldInstanciaTransicion) {
                oldInstanciaTransicion.par_observacion_id = instancia_poliza_transicion.par_observacion_id;
                instancia_poliza_transicion = await this.updateTransicion(oldInstanciaTransicion, usuario);
            } else {
                instancia_poliza_transicion = await modelos.instancia_poliza_transicion.create(newInstanciaPolizaTransaccion)
            }
            return instancia_poliza_transicion;
        } catch (e) {
            console.log(e)
            loggerCatch.info(new Date(), e);
        }
    }

    static async updateAseguradoInstanciaPolizaToNextStatus(statusIds, asegurado, usuario, callback = null) {
        try {
            const statusCycle = statusIds;
            let actualIndex = 0;
            let hoy = new Date();
            let updatedAt = new Date();
            let fechaRegistro = null;
            let newTransiciones = asegurado.instancia_poliza.instancia_poliza_transicions;
            let resp_instancia_poliza = await modelos.instancia_poliza.findOne({
                where: { id: asegurado.instancia_poliza.id },
                include: {
                    model: modelos.instancia_documento,
                    include: {
                        model: modelos.documento,
                    },
                },
            });
            let saved_instancia_poliza = resp_instancia_poliza && resp_instancia_poliza.dataValues ? resp_instancia_poliza.dataValues : null;
            let instancia_poliza = asegurado.instancia_poliza;
            if (instancia_poliza) {
                let poliza = await modelos.poliza.findOne({
                    where: { id: instancia_poliza.id_poliza },
                    include: {
                        model: modelos.parametro,
                        as: "tipo_numeracion",
                    },
                });
                instancia_poliza.updatedAt = updatedAt;
                let createdAt = instancia_poliza.createdAt;
                let fechaRegistro = instancia_poliza.fecha_registro;
                delete instancia_poliza.createdAt;
                delete instancia_poliza.fecha_registro;
                instancia_poliza.modificada_por = usuario.id;
                let new_instancia_documentos = asegurado.instancia_poliza.instancia_documentos.length ? asegurado.instancia_poliza.instancia_documentos : [];
                await statusCycle.forEach(async (status, index) => {
                    if (status == saved_instancia_poliza.id_estado) {
                        actualIndex = index;
                    }
                });
                let nextIndex = actualIndex + 1;
                if (statusCycle[nextIndex] != null) {
                    let parametro = await modelos.parametro.findOne({where: { id: statusCycle[nextIndex], diccionario_id: 11 }})
                    if (parametro && Object.keys(parametro).length) {
                        instancia_poliza.instancia_documentos = [];
                        let documentosProcesados = [];
                        for (let i = 0; i < new_instancia_documentos.length; i++) {
                            let new_instancia_documento = new_instancia_documentos[i];
                            if (new_instancia_documento) {
                                if (new_instancia_documento.documento) {
                                    if (!documentosProcesados.find((param) => param === new_instancia_documento.documento.id)) {
                                        if (new_instancia_documento.documento && new_instancia_documento.documento.id) {
                                            documentosProcesados.push(new_instancia_documento.documento.id);
                                        }
                                        instancia_poliza.poliza = poliza;
                                        if (new_instancia_documento.id) {
                                              if (new_instancia_documento.documento && new_instancia_documento.documento.id_tipo_documento == 281) {
                                                    new_instancia_documento.fecha_emision = new Date();
                                                    await modelos.instancia_documento.update({fecha_emision:new_instancia_documento.fecha_emision},{where:{id:new_instancia_documento.id}});
                                              }
                                            instancia_poliza.instancia_documentos.push(new_instancia_documento);
                                        } else {
                                            instancia_poliza = await this.createInstanciaDocumento(asegurado, instancia_poliza, new_instancia_documento, i, usuario);
                                        }
                                    }
                                } else {
                                    if (!documentosProcesados.find((param) => param === new_instancia_documento.id_documento)) {
                                        if (new_instancia_documento.id_documento) {
                                            documentosProcesados.push(new_instancia_documento.id_documento);
                                        }
                                        instancia_poliza.poliza = poliza;
                                        if (new_instancia_documento.id) {
                                            instancia_poliza.instancia_documentos.push(new_instancia_documento);
                                        } else {
                                            instancia_poliza = await this.createInstanciaDocumento(asegurado, instancia_poliza, new_instancia_documento, i, usuario);
                                        }
                                    }
                                }
                            }
                        }
                        if (instancia_poliza) {
                            instancia_poliza.id_estado = parametro.id;
                            let resp = await modelos.instancia_poliza.update(instancia_poliza, {
                                where: {
                                    id: instancia_poliza.id,
                                    id_poliza: instancia_poliza.id_poliza,
                                },
                            })
                            if (resp.length && resp[0]) {
                                instancia_poliza.createdAt = createdAt;
                                instancia_poliza.fecha_registro = fechaRegistro;
                                if (asegurado.instancia_poliza.instancia_poliza_transicions) {
                                    for (let i = 0; i < asegurado.instancia_poliza.instancia_poliza_transicions.length; i++) {
                                        let instancia_poliza_transicion = asegurado.instancia_poliza.instancia_poliza_transicions[i];
                                        instancia_poliza_transicion.par_estado_id = instancia_poliza.id_estado;
                                        if (instancia_poliza_transicion.observacion) {
                                            await this.createTransicion(instancia_poliza_transicion, usuario);
                                        }
                                    }
                                    instancia_poliza.instancia_poliza_transicions = await modelos.instancia_poliza_transicion.findAll({
                                        where: { id_instancia_poliza: instancia_poliza.id },
                                    })
                                }
                                asegurado.instancia_poliza.atributo_instancia_polizas = await modelos.atributo_instancia_poliza.findAll({
                                    where:{id_instancia_poliza:instancia_poliza.id},
                                    include:{
                                        model:modelos.objeto_x_atributo,
                                        include:{model:modelos.atributo}
                                    }
                                });
                                asegurado.instancia_poliza = instancia_poliza;
                                if (asegurado.instancia_poliza) {
                                    asegurado.instancia_poliza.usuario = usuario;
                                }
                            }
                        }
                        if (typeof callback == 'function') {
                            await callback(asegurado);
                        }
                    }
                } else {
                    let resp2 = await modelos.parametro.findOne({
                        where: { id: statusCycle[actualIndex], diccionario_id: 11 },
                    });
                    asegurado.instancia_poliza = instancia_poliza;
                    asegurado.instancia_poliza.usuario = usuario;
                    if (typeof callback == 'function') {
                        await callback(asegurado);
                    }
                }
            } else {
                if (typeof callback == 'function') {
                    await callback(asegurado);
                }
            }
            return asegurado
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async findInstanciaPolizaById(idInstanciaPoliza, callback = null) {
        try {
            return await modelos.instancia_poliza.find({
                where: { id: idInstanciaPoliza },
                include: [
                    {
                        model: modelos.atributo_instancia_poliza,
                        include: [
                            {
                                model: modelos.objeto_x_atributo,
                                attributes: [
                                    "id",
                                    "id_objeto",
                                    "id_atributo",
                                    "par_editable_id",
                                    "valor_defecto",
                                    "par_comportamiento_interfaz_id",
                                    "obligatorio",
                                ],
                                include: [
                                    {
                                        model: modelos.atributo,
                                        attributes: ["id", "id_tipo", "codigo", "descripcion"],
                                    },
                                    {
                                        model: modelos.objeto,
                                        attributes: ["id", "id_poliza", "descripcion", "id_tipo"],
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_editable",
                                        attributes: [
                                            "id",
                                            "parametro_cod",
                                            "parametro_descripcion",
                                            "parametro_abreviacion",
                                            "diccionario_id",
                                            "orden",
                                            "id_padre",
                                        ],
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_comportamiento_interfaz",
                                        attributes: [
                                            "id",
                                            "parametro_cod",
                                            "parametro_descripcion",
                                            "parametro_abreviacion",
                                            "diccionario_id",
                                            "orden",
                                            "id_padre",
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        model: modelos.instancia_documento,
                        include: [
                            {
                                model: modelos.documento,
                                attributes: [
                                    "id",
                                    "id_poliza",
                                    "descripcion",
                                    "fecha_inicio_vigencia",
                                    "fecha_fin_vigencia",
                                    "fecha_emision",
                                    "nro_inicial",
                                    "nro_actual",
                                ],
                            },
                            {
                                model: modelos.atributo_instancia_documento,
                            },
                        ],
                    },
                ],
            });
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async findPolizaNrosTransaccion(idPoliza, nroTransaccion) {
        try {
            return await modelos.instancia_poliza.findAll({
                    where: {
                        $and: { id_poliza: idPoliza, id_estado: { $notIn: [60, 62, 65, 282] } },
                    },
                    include: {
                        model: modelos.atributo_instancia_poliza,
                        where: { valor: nroTransaccion },
                        as: "nro_transaccion",
                        include: {
                            model: modelos.objeto_x_atributo,
                            include: { model: modelos.atributo, where: { id: 64 } },
                        },
                    },
                });
        } catch (e) {
            console.log(e);
            loggerCatch.info(new Date(), e);
        }
    }

    static async findSolicitudTransiciones(idInstanciaPoliza, callback = null) {
        try {
            return await modelos.instancia_poliza.find({
                    where: { id: idInstanciaPoliza },
                    include: [
                        {
                            model: modelos.instancia_poliza_transicion,
                            include: [
                                {
                                    model: modelos.parametro,
                                    as: "par_estado",
                                    attributes: [
                                        "id",
                                        "parametro_cod",
                                        "parametro_descripcion",
                                        "parametro_abreviacion",
                                        "diccionario_id",
                                        "orden",
                                        "id_padre",
                                    ],
                                },
                                {
                                    model: modelos.parametro,
                                    as: "par_observacion",
                                    attributes: [
                                        "id",
                                        "parametro_cod",
                                        "parametro_descripcion",
                                        "parametro_abreviacion",
                                        "diccionario_id",
                                        "orden",
                                        "id_padre",
                                    ],
                                },
                                {
                                    model: modelos.usuario,
                                    as: "ipt_usuario",
                                },
                            ],
                        },
                    ],
                });
        } catch (e) {
            console.log(e)
            loggerCatch.info(new Date(), e);
        }
    }

    static async findInstanciaPolizaConAtributosEditablesYVisiblesByIdYPolizaId (
        idInstanciaPoliza,
        idPoliza,
        idEditable,
        idVisible,
        callback = null
    ) {
        try {
            return await modelos.instancia_poliza.find({
                    where: { id: idInstanciaPoliza, id_poliza: idPoliza },
                    include: [
                        {
                            model: modelos.atributo_instancia_poliza,
                            include: [
                                {
                                    model: modelos.objeto_x_atributo,
                                    attributes: [
                                        "id",
                                        "id_objeto",
                                        "id_atributo",
                                        "par_editable_id",
                                        "valor_defecto",
                                        "par_comportamiento_interfaz_id",
                                        "obligatorio",
                                    ],
                                    where: {
                                        par_comportamiento_interfaz_id: idVisible,
                                        par_editable_id: idEditable,
                                    },
                                    include: [
                                        {
                                            model: modelos.atributo,
                                            attributes: ["id", "id_tipo", "codigo", "descripcion"],
                                        },
                                        {
                                            model: modelos.objeto,
                                            attributes: ["id", "id_poliza", "descripcion", "id_tipo"],
                                        },
                                        {
                                            model: modelos.parametro,
                                            as: "par_editable",
                                            attributes: [
                                                "id",
                                                "parametro_cod",
                                                "parametro_descripcion",
                                                "parametro_abreviacion",
                                                "diccionario_id",
                                                "orden",
                                                "id_padre",
                                            ],
                                        },
                                        {
                                            model: modelos.parametro,
                                            as: "par_comportamiento_interfaz",
                                            attributes: [
                                                "id",
                                                "parametro_cod",
                                                "parametro_descripcion",
                                                "parametro_abreviacion",
                                                "diccionario_id",
                                                "orden",
                                                "id_padre",
                                            ],
                                        },
                                    ],
                                },
                                {
                                    model: modelos.instancia_documento,
                                    include: [
                                        {
                                            model: modelos.documento,
                                            attributes: [
                                                "id",
                                                "id_poliza",
                                                "descripcion",
                                                "fecha_inicio_vigencia",
                                                "fecha_fin_vigencia",
                                                "fecha_emision",
                                                "nro_inicial",
                                                "nro_actual",
                                            ],
                                        },
                                        {
                                            model: modelos.atributo_instancia_documento,
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                });
            if (typeof callback == 'function') {
                await callback();
            }
        } catch (e) {
            console.log(e)
            loggerCatch.info(new Date(), e);
        }
    }

    static async findInstanciaPolizaConAtributosByIdYPolizaId (
        idInstanciaPoliza,
        idPoliza,
        callback = null
    ) {
        try {
            return await modelos.instancia_poliza.find({
                where: { id: idInstanciaPoliza, id_poliza: idPoliza },
                include: [
                    {
                        model: modelos.atributo_instancia_poliza,
                        include: [
                            {
                                model: modelos.objeto_x_atributo,
                                attributes: [
                                    "id",
                                    "id_objeto",
                                    "id_atributo",
                                    "par_editable_id",
                                    "valor_defecto",
                                    "par_comportamiento_interfaz_id",
                                    "obligatorio",
                                ],
                                include: [
                                    {
                                        model: modelos.atributo,
                                        attributes: [
                                            "id",
                                            "id_tipo",
                                            "codigo",
                                            "descripcion",
                                            "id_diccionario",
                                        ],
                                    },
                                    {
                                        model: modelos.objeto,
                                        attributes: ["id", "id_poliza", "descripcion", "id_tipo"],
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_editable",
                                        attributes: [
                                            "id",
                                            "parametro_cod",
                                            "parametro_descripcion",
                                            "parametro_abreviacion",
                                            "diccionario_id",
                                            "orden",
                                            "id_padre",
                                        ],
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_comportamiento_interfaz",
                                        attributes: [
                                            "id",
                                            "parametro_cod",
                                            "parametro_descripcion",
                                            "parametro_abreviacion",
                                            "diccionario_id",
                                            "orden",
                                            "id_padre",
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            });
            if (typeof callback == 'function') {
                await callback();
            }
        } catch (e) {
            console.log(e)
            loggerCatch.info(new Date(), e);
        }
    }

    static async deleteInstanciaPolizas(idInstancias = []) {
        try {
            let aData = [];
            for (let i = 0; i < idInstancias.length; i++) {
                let idInstancia = idInstancias[i];
                let resp = await modelos.sequelize.query(`
                    DECLARE @idInstanciaPoliza int;
                    SET @idInstanciaPoliza = ${idInstancia};
                    DELETE FROM instancia_anexo_asegurado WHERE id_asegurado in (select id from asegurado where id_instancia_poliza = @idInstanciaPoliza);
                    DELETE FROM atributo_instancia_poliza where id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM beneficiario where id_asegurado in (select id from asegurado where id_instancia_poliza = @idInstanciaPoliza);
                    DELETE FROM atributo_instancia_documento WHERE id_instancia_documento in (select id from instancia_documento where id_instancia_poliza = @idInstanciaPoliza);
                    DELETE FROM instancia_poliza_transicion WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM asegurado WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM instancia_documento WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM plan_pago_detalle WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM plan_pago WHERE id_instancia_poliza = @idInstanciaPoliza;
                    DELETE FROM instancia_poliza WHERE id = @idInstanciaPoliza;
                `);
                aData.push(resp);
            }
            return aData;
        } catch (e) {
            console.log(e)
        }
    }

}

module.exports = InstanciaPolizaService;
