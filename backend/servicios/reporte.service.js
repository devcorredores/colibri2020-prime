require('../utils/Prototipes');
const modelos = require('../modelos');
const {loggerSolicitud} = require("../modulos/winston");
const request = require('request');
const specialRequest = request.defaults({strictSSL: false});
const soap = require('soap');
const webconfig = require('../config/web-config');
const to_json = require('xmljson').to_json;
const soapHeaderXml = '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';
const invoke = require("../modulos/soap-request");
const fechas = require('fechas');
const moment = require('moment');
const solicitudService = require('./solicitud.service');
const util = require('../utils/util');
const Op = require('sequelize');
const { GenerarPlanPagos } = require('../modulos/funciones_plan_pago');
const generaExcel = require('../modulos/genera_excel');

class ReporteService {

	static async setDataExcelProduccionMensual(aData) {
		let dataExcel = {};
		let instancias = [];
		let params = await solicitudService.setParametros([1,2,4,7,18,29,30,52,11,38,40]);
		let parIdDescription = await solicitudService.setParametrosDescription([1,7,4,18,29,30,52,11,31,38,40]);
		let polizaNombres = '';
		let polizaPeriodo = '';

		for (let i = 0 ; i < aData.length ; i++) {
			let data = aData[i].dataValues;
			let instanciaDocumento = await solicitudService.setInstanciaDocumentos(data.instancia_poliza.instancia_documentos);
			let atributo = await solicitudService.setAtributoInstanciaPoliza(data.instancia_poliza.atributo_instancia_polizas);
			let objeto = {};
			let momFechaEmisionCertificado = instanciaDocumento.documentoCertificado ? moment(instanciaDocumento.documentoCertificado.fecha_emision) : '';
			let momFechaNacimiento = data.entidad.persona ? moment(data.entidad.persona.persona_fecha_nacimiento) : '';
			let momFechaRegistro = data.instancia_poliza ? moment(data.instancia_poliza.fecha_registro) : '';

			let periodo = momFechaEmisionCertificado ? momFechaEmisionCertificado.year()+(momFechaEmisionCertificado.month()+1).pad(2) : '';

			if (!polizaNombres.includes(data.instancia_poliza.poliza.descripcion)) {
				polizaNombres += data.instancia_poliza.poliza.descripcion+'_';
			}
			if (!polizaPeriodo.includes(periodo)) {
				polizaPeriodo += periodo+'_';
			}
			let benPrimerNombre;
			let benSegundoNombre;
			let benPrimerApellido;
			let benSegundoApellido;

			if (data.instancia_poliza) {

				objeto['AEC_ID_CLIENTE'] = data.entidad.persona ? data.entidad.persona.persona_doc_id : '';
				objeto['AEC_ID_CERTIFICADO'] = instanciaDocumento.documentoCertificado ? instanciaDocumento.documentoCertificado.nro_documento : '';
				objeto['AEC_ID_POLIZA'] = data.instancia_poliza.poliza ? data.instancia_poliza.poliza.numero : '';
				objeto['AEC_PERIODO'] = momFechaEmisionCertificado ? momFechaEmisionCertificado.year()+(momFechaEmisionCertificado.month()+1).pad(2) : '';
				objeto['AEC_CODIGO_VOUCHER'] = atributo.atributoNumerotransaccion ? atributo.atributoNumerotransaccion.valor : '';
				objeto['AEC_CODIGO_CAJERO'] = atributo.atributoNumerotransaccion ? atributo.atributoNumerotransaccion.adicionado_por : '';
				objeto['AEC_PRODUCTO'] = data.instancia_poliza.poliza ? data.instancia_poliza.poliza.descripcion : '';
				objeto['AEC_PATERNO'] = data.entidad.persona ? data.entidad.persona.persona_primer_apellido : '' ;
				objeto['AEC_MATERNO'] = data.entidad.persona ? data.entidad.persona.persona_segundo_apellido : '';
				objeto['AEC_CASADA'] = data.entidad.persona ? data.entidad.persona.persona_apellido_casada : '';
				objeto['AEC_NOMBRES'] = data.entidad.persona ? data.entidad.persona.persona_primer_nombre + ' ' + data.entidad.persona.persona_segundo_nombre : '';
				objeto['AEC_IDC'] = data.entidad.persona ? data.entidad.persona.persona_doc_id : '';
				objeto['AEC_EXTIDC'] = data.entidad.persona ? params.lugarExtencionDocId[data.entidad.persona.persona_doc_id_ext] : '';
				objeto['AEC_TIPO_IDC'] = data.entidad.persona ? parIdDescription[data.entidad.persona.par_tipo_documento_id] : '';
				objeto['AEC_FECHA_NACIMIENTO'] = momFechaNacimiento ? momFechaNacimiento.format('DD/MM/YYYY') : '';
				objeto['AEC_SEXO'] = parIdDescription ? parIdDescription[data.entidad.persona.par_sexo_id] : '';
				objeto['AEC_DOM_CIUDAD'] = atributo.atributoCiudadNacimiento ? atributo.atributoCiudadNacimiento.valor : '';
				objeto['AEC_DOM_ZONA'] = atributo.atributoZona ? atributo.atributoZona.valor : '';
				objeto['AEC_DOM_DIRECCION'] = data.entidad.persona ? data.entidad.persona.persona_direccion_domicilio : '';
				objeto['AEC_TELEFONO'] = data.entidad.persona ? data.entidad.persona.persona_celular && data.entidad.persona.persona_telefono_domicilio ? data.entidad.persona.persona_telefono_domicilio + ' ' + data.entidad.persona.persona_celular : data.entidad.persona.persona_celular : '';
				objeto['AEC_ESTADO'] = parIdDescription[data.instancia_poliza.id_estado];
				objeto['AEC_AFI_SUCURSAL'] = params.sucursalesEcofuturo[atributo.atributoSucursal.valor];
				objeto['AEC_AFI_AGENCIA'] = params.oficinasEcofuturo[atributo.atributoOficina.valor];
				objeto['AEC_AFI_FECHA'] = momFechaEmisionCertificado ? momFechaEmisionCertificado.format('DD/MM/YYYY') : '';
				objeto['AEC_AFI_FUNCIONARIO'] = data.adicionado_por;

				objeto['AEC_BENEFICIARIO'] = '';
				objeto['AEC_BEN_PARENTESCO'] = '';
				for (let j = 0 ; j < data.beneficiarios.length ; j++) {
					let beneficiario = data.beneficiarios[j];
					benPrimerNombre = beneficiario.entidad.persona.persona_primer_nombre;
					benSegundoNombre = beneficiario.entidad.persona.persona_segundo_nombre;
					benPrimerApellido = beneficiario.entidad.persona.persona_primer_apellido;
					benSegundoApellido = beneficiario.entidad.persona.persona_segundo_apellido;
					objeto['AEC_BENEFICIARIO'] += benPrimerNombre && benSegundoNombre && benPrimerApellido && benSegundoApellido ? benPrimerNombre+' '+benSegundoNombre+' '+benPrimerApellido+' '+benSegundoApellido : benPrimerNombre && benPrimerApellido && benSegundoApellido ? benPrimerNombre+' '+benPrimerApellido+' '+benSegundoApellido : benPrimerNombre && benPrimerApellido ? benPrimerNombre+' '+benPrimerApellido : benPrimerNombre ? benPrimerNombre : benPrimerApellido + ' | ';
					objeto['AEC_BEN_PARENTESCO'] += beneficiario.parentesco.parametro_descripcion + ' | ';
				}
				objeto['AEC_BENEFICIARIO'] = objeto['AEC_BENEFICIARIO'].substring(0,objeto['AEC_BENEFICIARIO'].length-2)
				objeto['AEC_BEN_PARENTESCO'] = objeto['AEC_BEN_PARENTESCO'].substring(0,objeto['AEC_BEN_PARENTESCO'].length-2)

				objeto['AEC_FECHA_PAGO'] = '';
				for (let j = 0 ; j < data.instancia_poliza.plan_pagos.length ; j++) {
					let planPago = data.instancia_poliza.plan_pagos[j];
					for (let k = 0 ; k < planPago.plan_pago_detalles.length ; k++) {
						let planPagoDetalle = planPago.plan_pago_detalles[k];
						objeto['AEC_FECHA_PAGO'] += planPagoDetalle.id_estado == 254 ? moment(planPagoDetalle.FechaPagoEjec).add(4,'hours').format('DD/MM/YYYY') + ' | ' : ' | ';
					}
					objeto['AEC_FECHA_PAGO'] = objeto['AEC_FECHA_PAGO'].substring(0,objeto['AEC_FECHA_PAGO'].length-2);
				}

				objeto['AEC_MODALIDAD_PAGO'] = atributo.atributoModalidadPago ? parIdDescription[atributo.atributoModalidadPago.valor] : '';
				objeto['AEC_FECHA_REGISTRO'] = momFechaRegistro ? momFechaRegistro.format('DD/MM/YYYY') : '';
				objeto['AEC_PEP'] = atributo.atributoCondicionCargoPublicoPoliticoJerarquico ? atributo.atributoCondicionCargoPublicoPoliticoJerarquico.valor : '';
				objeto['AEC_PEP_CARGO'] = atributo.atributoCargoEntidadPublicoPoliticoJerarquico ? atributo.atributoCargoEntidadPublicoPoliticoJerarquico.valor : '';
				objeto['AEC_PEP_PERIODO'] = atributo.atributoPeriodoCargoPublicoPoliticoJerarquico ? atributo.atributoPeriodoCargoPublicoPoliticoJerarquico.valor : '';
				objeto['AEC_PROFESION'] = data.entidad.persona.persona_profesion;
				objeto['AEC_NACIONALIDAD'] = params.nacionalidad[data.entidad.persona.par_nacionalidad_id];
				objeto['AEC_ESTADO_CIVIL'] = atributo.atributoEstadoCivil.valor;
				objeto['AEC_USUARIO_INGRESO'] = data.usuarioAdicionado.usuario_nombre_completo;
				objeto['AEC_FECHA_INGRESO'] = momFechaRegistro ? momFechaRegistro.format('DD/MM/YYYY') : '';
				objeto['AEC_ACTIVIDAD'] = atributo.atributoOcupacion.valor.substring(7,atributo.atributoOcupacion.valor.length);
				objeto['AEC_TIPO_AGENCIA'] = '';
			}


			instancias.push(objeto);
		}
		polizaNombres = polizaNombres.substring(0,polizaNombres.length-1).replaceAll(' ','_');
		polizaPeriodo = polizaPeriodo.substring(0,polizaPeriodo.length-1).replaceAll(' ','_');
		generaExcel.exportExcel(instancias, `reporte-mensual-${polizaNombres}-${polizaPeriodo}`);
		return instancias;
	}

	static async setDataExcelProduccionMensualDiaria(aData) {
		let dataExcel = {};
		let instancias = [];
		let params = await solicitudService.setParametros([1,2,4,7,18,29,30,52,11,38,40, 46]);
		let parIdDescription = await solicitudService.setParametrosDescription([1,7,4,18,29,30,52,11,31,38,40,46]);
		let polizaNombres = '';
		let polizaPeriodo = '';

		for (let i = 0 ; i < aData.length ; i++) {
			let data = aData[i].dataValues;
			let instanciaDocumento = await solicitudService.setInstanciaDocumentos(data.instancia_poliza.instancia_documentos);
			let atributo = await solicitudService.setAtributoInstanciaPoliza(data.instancia_poliza.atributo_instancia_polizas);
			let objeto = {};
			let momFechaEmisionCertificado = instanciaDocumento.documentoCertificado ? moment(instanciaDocumento.documentoCertificado.fecha_emision) : '';
			let momFechaNacimiento = data.entidad.persona ? moment(data.entidad.persona.persona_fecha_nacimiento) : '';
			let momFechaRegistro = data.instancia_poliza ? moment(data.instancia_poliza.fecha_registro) : '';
			let momInstanciaUpdatedAt = data.instancia_poliza ? moment(data.instancia_poliza.updatedAt) : '';

			let periodo = momFechaEmisionCertificado ? momFechaEmisionCertificado.year()+(momFechaEmisionCertificado.month()+1).pad(2) : '';

			if (!polizaNombres.includes(data.instancia_poliza.poliza.descripcion)) {
				polizaNombres += data.instancia_poliza.poliza.descripcion+'_';
			}
			if (!polizaPeriodo.includes(periodo)) {
				polizaPeriodo += periodo+'_';
			}
			let benPrimerNombre = '';
			let benSegundoNombre = '';
			let benPrimerApellido = '';
			let benSegundoApellido = '';
			let estadoPago = '';
			let importeSeguro = 0;

			for (let j = 0 ; j < data.beneficiarios.length ; j++) {
				let beneficiario = data.beneficiarios[j];
				benPrimerNombre += beneficiario.entidad.persona.persona_primer_nombre;
				benSegundoNombre += beneficiario.entidad.persona.persona_segundo_nombre;
				benPrimerApellido += beneficiario.entidad.persona.persona_primer_apellido;
				benSegundoApellido += beneficiario.entidad.persona.persona_segundo_apellido;
			}

			if (data.instancia_poliza) {
				objeto['producto'] = data.instancia_poliza.poliza ? data.instancia_poliza.poliza.descripcion : '';
				objeto['modalidad de pago'] = parIdDescription[atributo.atributoModalidadPago.valor];
				objeto['sucursal'] = params.sucursalesEcofuturo[atributo.atributoSucursal.valor];
				objeto['agencia'] = params.oficinasEcofuturo[atributo.atributoOficina.valor];
				objeto['tipo de agencia'] = '';
				objeto['nombre cliente'] = data.entidad.persona ? data.entidad.persona.persona_primer_nombre + ' ' + data.entidad.persona.persona_segundo_nombre : '';
				objeto['apellido paterno cliente'] = data.entidad.persona ? data.entidad.persona.persona_primer_apellido : '' ;
				objeto['apellido materno cliente'] = data.entidad.persona ? data.entidad.persona.persona_segundo_apellido : '';
				objeto['fecha de nacimiento'] = momFechaNacimiento ? momFechaNacimiento.format('DD/MM/YYYY') : '';
				objeto['ci'] = data.entidad.persona ? data.entidad.persona.persona_doc_id : '';
				objeto['ext'] = data.entidad.persona ? params.lugarExtencionDocId[data.entidad.persona.persona_doc_id_ext] : '';
				objeto['nro certificado'] = instanciaDocumento.documentoCertificado ? instanciaDocumento.documentoCertificado.nro_documento : '';
				objeto['fecha afiliacion'] = momFechaRegistro ? momFechaRegistro.format('DD/MM/YYYY') : '';
				objeto['estado del seguro'] = parIdDescription[data.instancia_poliza.id_estado];

				objeto['importe del seguro'] = data.instancia_poliza.poliza.anexo_poliza.monto_prima;
				objeto['fecha ultimo pago'] = '';
				objeto['estado de pago'] = '';
				objeto['prima pagada'] = '';
				objeto['num_operacion'] = '';

				for (let j = 0 ; j < data.instancia_poliza.plan_pagos.length ; j++) {
					let planPago = data.instancia_poliza.plan_pagos[j];
					importeSeguro	= planPago.total_prima;
					objeto['prima pagada'] += planPago.total_prima + ' | ';

					for (let k = 0 ; k < planPago.plan_pago_detalles.length ; k++) {
						let planPagoDetalle = planPago.plan_pago_detalles[k];
						estadoPago = parIdDescription[planPagoDetalle.id_estado];
						objeto['estado de pago'] += estadoPago +' | ';
						objeto['fecha ultimo pago'] += planPagoDetalle.id_estado == 254 ? moment(planPagoDetalle.FechaPagoEjec).add(4,'hours').format('DD/MM/YYYY') + ' | ' : ' | ';
						objeto['num_operacion'] += planPagoDetalle.id_estado == 254 ? planPagoDetalle.NroDocPagoEjec + ' | ' : ' | ';
					}
					objeto['num_operacion'] = objeto['num_operacion'].substring(0,objeto['num_operacion'].length-2);
					objeto['estado de pago'] = objeto['estado de pago'].substring(0,objeto['estado de pago'].length-2);
					objeto['fecha ultimo pago'] = objeto['fecha ultimo pago'].substring(0,objeto['fecha ultimo pago'].length-2);
				}
				objeto['prima pagada'] = objeto['prima pagada'].substring(0,objeto['prima pagada'].length-2);

				objeto['fecha anulacion'] = data.instancia_poliza.id_estado == 60 && momInstanciaUpdatedAt ? momInstanciaUpdatedAt.format('DD/MM/YYYY') : '';
				objeto['emitido por'] = instanciaDocumento.documentoCertificado ? instanciaDocumento.documentoCertificado.usuario.usuario_nombre_completo : '' ;
				objeto['referido por'] = data.usuarioAdicionado.usuario_nombre_completo;
				objeto['cargo'] = data.usuarioAdicionado.usuarioRoles.concat(param => param.descripcion+'-');
				objeto['id_colibri'] = data.instancia_poliza.id;
			}

			instancias.push(objeto);
		}
		polizaNombres = polizaNombres.substring(0,polizaNombres.length-1).replaceAll(' ','_');
		polizaPeriodo = polizaPeriodo.substring(0,polizaPeriodo.length-1).replaceAll(' ','_');
		generaExcel.exportExcel(instancias, `reporte-mensual-diario-${polizaNombres}-${polizaPeriodo}`);
		return instancias;
	}

	// static async produccionMensual(idPoliza, month) {
	// 	try {
	// 		let resp = await await modelos.asegurado.findAll({
	// 			include: [
	// 				{ model: modelos.usuario, as:'usuarioAdicionado'},
	// 				{
	// 					model: modelos.entidad,
	// 					include: [{
	// 						model: modelos.persona,
	// 					}]
	// 				}, {
	// 					model: modelos.instancia_poliza,
	// 					where: {$and: modelos.sequelize.where(modelos.sequelize.fn('MONTH', modelos.sequelize.col('fecha_registro')), month), id_poliza: idPoliza},
	// 					include: [{
	// 						model:modelos.parametro, as: 'estado',
	// 					}, {
	// 						model:modelos.poliza,
	// 						include: [{
	// 							model:modelos.parametro, as: 'ramo',
	// 						}, {
	// 							model:modelos.parametro, as: 'tipo_numeracion',
	// 						}, {
	// 							model:modelos.entidad,
	// 							as: 'aseguradora'
	// 						}]
	// 					}, {
	// 						model: modelos.atributo_instancia_poliza,
	// 						include: [{
	// 							model: modelos.objeto_x_atributo,
	// 							include: [{
	// 								model: modelos.atributo,
	// 							}, {
	// 								model: modelos.objeto,
	// 							},{
	// 								model: modelos.parametro,
	// 								as: 'par_editable',
	// 							}, {
	// 								model: modelos.parametro,
	// 								as: 'par_comportamiento_interfaz',
	// 							}]
	// 						}]
	// 					}, {
	// 						model: modelos.instancia_documento,
	// 						include: [{
	// 							model: modelos.documento,
	// 						}, {
	// 							model: modelos.atributo_instancia_documento
	// 						}]
	// 					}]
	// 				}, {
	// 					model: modelos.beneficiario,
	// 					include: [{
	// 						model: modelos.asegurado
	// 					}, {
	// 						model: modelos.entidad,
	// 						include: [{
	// 							model: modelos.persona
	// 						}]
	// 					}, {
	// 						model: modelos.parametro, as: 'estado_obj',
	// 					}, {
	// 						model: modelos.parametro, as: 'tipo_obj',
	// 					}, {
	// 						model: modelos.parametro, as: 'parentesco',
	// 					}]
	// 				}
	// 			]
	// 		});
	//
	// 		return resp;
	// 	} catch (e) {
	// 		console.log(e);
	// 	}
	// }

	static async produccionMensualDiaria(idPoliza,month,date = null) {
		try {
			let condicion;

			if (date) {
				condicion = {fecha_registro:date, id_poliza:idPoliza};
			} else {
				condicion = {$and: modelos.sequelize.where(modelos.sequelize.fn('MONTH', modelos.sequelize.col('fecha_registro')), month), id_poliza: idPoliza};
			}

			let resp = await await modelos.asegurado.findAll({
				include: [
					{
						model: modelos.usuario, as:'usuarioAdicionado',
						include: [
							{model: modelos.rol, as:'usuarioRoles'}
						]
					},
					{
						model: modelos.entidad,
						include: [{
							model: modelos.persona,
						}]
					}, {
						model: modelos.instancia_poliza,
						where: condicion,
						include: [
							{ model:modelos.parametro, as: 'estado'},
							{ model:modelos.planPago,
								include: { model: modelos.plan_pago_detalle}
								},
							{ model:modelos.poliza,
								include: [
									{ model:modelos.parametro, as: 'ramo' },
									{ model:modelos.parametro, as: 'tipo_numeracion' },
									{ model:modelos.entidad, as: 'aseguradora' },
									{ model:modelos.anexo_poliza, where:{vigente:true}}
								]
						}, {
							model: modelos.atributo_instancia_poliza,
							include: [{
								model: modelos.objeto_x_atributo,
								include: [{
									model: modelos.atributo,
								}, {
									model: modelos.objeto,
								},{
									model: modelos.parametro,
									as: 'par_editable',
								}, {
									model: modelos.parametro,
									as: 'par_comportamiento_interfaz',
								}]
							}]
						}, {
							model: modelos.instancia_documento,
							include: [
								{ model: modelos.documento,},
								{ model: modelos.atributo_instancia_documento},
								{ model: modelos.usuario},
							]
						}]
					}, {
						model: modelos.beneficiario,
						include: [{
							model: modelos.asegurado
						}, {
							model: modelos.entidad,
							include: [{
								model: modelos.persona
							}]
						}, {
							model: modelos.parametro, as: 'estado_obj',
						}, {
							model: modelos.parametro, as: 'tipo_obj',
						}, {
							model: modelos.parametro, as: 'parentesco',
						}]
					}
				]
			});

			return resp;
		} catch (e) {
			console.log(e);
		}
	}

	static async produccionDiariaInmedical(idPoliza, date) {
		try {

			let resp = await await modelos.asegurado.findAll({
				include: [
					{
						model: modelos.entidad,
						include: [{
							model: modelos.persona,
						}]
					}, {
						model: modelos.instancia_poliza,
						where: { fecha_registro: date, id_poliza: idPoliza },
						include: [{
							model:modelos.parametro, as: 'estado',
						}, {
							model:modelos.poliza,
							include: [{
								model:modelos.parametro, as: 'ramo',
							}, {
								model:modelos.parametro, as: 'tipo_numeracion',
							}, {
								model:modelos.entidad,
								as: 'aseguradora'
							}]
						}, {
							model: modelos.atributo_instancia_poliza,
							include: [{
								model: modelos.objeto_x_atributo,
								include: [{
									model: modelos.atributo,
								}, {
									model: modelos.objeto,
								},{
									model: modelos.parametro,
									as: 'par_editable',
								}, {
									model: modelos.parametro,
									as: 'par_comportamiento_interfaz',
								}]
							}]
						}, {
							model: modelos.instancia_documento,
							include: [{
								model: modelos.documento,
							}, {
								model: modelos.atributo_instancia_documento
							}]
						}]
					}, {
						model: modelos.beneficiario,
						include: [{
							model: modelos.asegurado
						}, {
							model: modelos.entidad,
							include: [{
								model: modelos.persona
							}]
						}, {
							model: modelos.parametro, as: 'estado_obj',
						}, {
							model: modelos.parametro, as: 'tipo_obj',
						}, {
							model: modelos.parametro, as: 'parentesco',
						}]
					}
				]
			});

			return resp;
		} catch (e) {
			console.log(e);
		}
	}

}

module.exports = ReporteService;
