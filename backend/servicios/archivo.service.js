const modelos = require('../modelos');
const multer = require('multer');
const path = require("path");

class ArchivoService {

	static async upload(req,res = null, callback = null) {
		try {
			let instanciaPoliza, idInstanciaPoliza, instanciaDocumentos, fileName, archivoEcoAccidentesSol, archivoEcoAccidentesCert, file, filePath;
			let idArchivoEcoAccidentesSol = 5;
			let idArchivoEcoAccidentesCert = 6;
			if (req.query && req.query.idInstanciaPoliza) {
				idInstanciaPoliza = req.query.idInstanciaPoliza;
				archivoEcoAccidentesSol = await modelos.archivo.findOne({where:{id:idArchivoEcoAccidentesSol}});
				archivoEcoAccidentesCert = await modelos.archivo.findOne({where:{id:idArchivoEcoAccidentesCert}});
				instanciaPoliza = await modelos.instancia_poliza.findOne({
					where: {id:idInstanciaPoliza},
					include:{
						model: modelos.instancia_documento,
						include: {
							model: modelos.documento,
							include: {
								model: modelos.archivo
							}
						}
					}
				});
				instanciaDocumentos = instanciaPoliza.instancia_documentos;
			}
			let storage = multer.diskStorage({
				destination: async (req, archivo, cb) => {
					if (instanciaPoliza.id_estado+'' == '81') {
						filePath = path.join(__dirname, "../public",archivoEcoAccidentesSol.ubicacion_local);
					} else if (instanciaPoliza.id_estado+'' == '59') {
						filePath = path.join(__dirname, "../public",archivoEcoAccidentesSol.ubicacion_local);
					}
					cb(null, filePath)
				},
				filename: async (req, archivo, cb) => {
					if (req.query && req.query.idInstanciaPoliza) {
						let idInstanciaPoliza = req.query.idInstanciaPoliza;
						if (instanciaPoliza.id_estado+'' == '81') {
							let instanciaDocumento = instanciaDocumentos.find(param => param.documento.id_tipo_documento+'' == '279');
							fileName = `doc_${instanciaDocumento.id}_${archivo.originalname}`;
							cb(null, fileName);
							await modelos.archivo.update({nro_actual:parseInt(archivoEcoAccidentesSol.nro_actual)+1},{where:{id:idArchivoEcoAccidentesSol}});
							await modelos.instancia_documento.update({nombre_archivo:fileName}, {where:{id:instanciaDocumento.id}});
						} else if (instanciaPoliza.id_estado+'' == '59') {
							let instanciaDocumento = instanciaDocumentos.find(param => param.documento.id_tipo_documento+'' == '281');
							fileName = `doc_${instanciaDocumento.id}_${archivo.originalname}`;
							cb(null, fileName);
							await modelos.archivo.update({nro_actual:archivoEcoAccidentesSol.nro_actual++},{where:{id:idArchivoEcoAccidentesCert}});
							await modelos.instancia_documento.update({nombre_archivo:fileName}, {where:{id:instanciaDocumento.id}});
						}
					} else {
						cb(null, archivo.originalname);
					}
				}
			});

			const upload = multer({ storage });

			let multerMiddleware = upload.single('archivos');

			multerMiddleware(req, res, () => {
				if (typeof callback == 'function') {
					file = req.file;
					callback({filename:fileName, filepath:filePath});
				}
			});
		} catch (e) {
			console.log(e);
		}
	}

	static async delete(idInstanciaDocumento) {
		try {
			let resp = await modelos.instancia_documento.update({nombre_archivo:''}, {where:{id:idInstanciaDocumento}});
			let instanciaDocumento = await modelos.instancia_documento.find({where:{id:idInstanciaDocumento}});
			return instanciaDocumento;
		} catch (e) {
			console.log(e)
		}
	}
}

module.exports = ArchivoService;
