const modelos = require("../modelos");

class InstanciaAnexoService {

    static async listarInstanciaAnexosByIdAsegurado(id_asegurado) {
        return await modelos.instancia_anexo_asegurado.findAll({
            where: { id_asegurado },
            include: [{
                model:modelos.entidad,
                include:[{
                    model:modelos.persona
                }]
            },
                {
                    model:modelos.parametro,
                    as: 'par_parentesco'
                },
                {
                    model:modelos.parametro,
                    as: 'par_relacion'
                }]
        });
    }

    static async byid(id) {
        return await modelos.rol.findAll({ where: { id } })
    }

    static async guardar_usuarioxrol(id_usuario, id_rol) {
        return await modelos.usuario_x_rol.create({ id_usuario, id_rol})
    }

    static async ResgistrarEmparejamiento(usuarios) {
        let usuarioRols = [];
        for (let usuario of usuarios){
            for(let rol of usuario.roles_adicionar)
            {
                let usuario_x_rol={
                    id_rol:rol.id,
                    id_usuario:usuario.id,
                    adicionado_por:usuario.adicionado_por,
                    modificado_por:usuario.modificado_por,
                }
                let usuarioRol = await modelos.usuario_x_rol.create(usuario_x_rol);
                if (usuarioRol) {
                    usuarioRols.push(usuarioRol);
                }
            }
        }
        return usuarioRols;
    }

    static async eliminarRol_x_Perfil(id_usuario, id_rol) {
        return await modelos.usuario_x_rol.destroy({where:{id_usuario,id_rol}})
    }
}

module.exports = InstanciaAnexoService;
