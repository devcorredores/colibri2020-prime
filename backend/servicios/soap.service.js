const modelos = require('../modelos');
const {loggerSolicitud} = require("../modulos/winston");
const {loggerCatch} = require("../modulos/winston");
const invoke = require("../modulos/soap-request");
const request = require('request');
const soap = require("../modulos/soap");
const webconfig = require('../config/web-config');
const to_json = require('xmljson').to_json;
const path = require("path");
const specialRequest = request.defaults({strictSSL: false});
const soapHeaderXml = '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';

class SoapService {

	static async verifyInstanciasWithBankServices(id_poliza) {
		try {
			let objetoXAtributo = await modelos.objeto_x_atributo.findAll({
				include: [
					{
						model: modelos.objeto, where: {id_poliza: id_poliza}
					},{
						model: modelos.atributo
					}
				]
			});
			let objetoAtributoNroCuenta = objetoXAtributo.find(param => param.atributo.id == 5);
			let doc_id,ext,asegurados = await modelos.asegurado.findAll({
				include: [
					{
						model: modelos.entidad,
						include: {
							model: modelos.persona
						}
					},{
						model: modelos.instancia_poliza,
						where: {id_poliza: id_poliza},
						include: {
							model: modelos.atributo_instancia_poliza,
							where: {id_objeto_x_atributo: objetoAtributoNroCuenta.id}
						}
					}
				]

			});
			let accountResp, customerResp;
			let observados = [];
			for (let i = 0; i < asegurados.length; i++) {
				let asegurado = asegurados[i];
				doc_id = asegurado.entidad ? asegurado.entidad.persona.persona_doc_id : '';
				ext = asegurado.entidad ? asegurado.entidad.persona.persona_doc_id_ext : '';
				if (doc_id && ext) {
					customerResp = await this.getCustomer(doc_id,ext);
					console.log(customerResp);
					if (customerResp) {
						accountResp = await this.getAccount(customerResp.cod_agenda);
						console.log(accountResp);
						let personaBancoFechaNac = new Date(parseInt(customerResp.anio_fechanac), parseInt(customerResp.mes_fechanac)-1, parseInt(customerResp.dia_fechanac));
						let personaColibriFechaNac = asegurado.entidad.persona.persona_fecha_nacimiento;
						let persona = asegurado.entidad.persona;
						if (personaBancoFechaNac.getFullYear() != personaColibriFechaNac.getFullYear() &&
							personaBancoFechaNac.getMonth() != personaColibriFechaNac.getMonth() &&
							personaBancoFechaNac.getDate() != personaColibriFechaNac.getDate()) {
							if (accountResp) {
								if (accountResp.nrocuenta != asegurado.instancia_poliza.atributo_instancia_polizas[0].valor) {
									observados.push({
										ci:doc_id+ ' ' + ext,
										old_fecha_nac:personaColibriFechaNac,
										old_nro_cuenta:asegurado.instancia_poliza.atributo_instancia_polizas[0].valor,
										new_fecha_nac:personaBancoFechaNac,
										new_nro_cuenta:accountResp.nrocuenta,
										id_poliza: id_poliza
									});
								}
							} else {
								modelos.persona.update({persona_fecha_nacimiento:personaBancoFechaNac}, { where: { id: persona.id } });
								observados.push({
									ci:doc_id+ ' ' + ext,
									old_fecha_nac:personaColibriFechaNac,
									new_fecha_nac:personaBancoFechaNac,
									id_poliza: id_poliza
								});
							}
						}
					}
				}
			}
			for (let i = 0; i < observados.length; i++) {
				let observado = observados[i];
				await modelos.tmp_verificados.create(observado);
			}
			loggerCatch.info('Historial de solicitudes observadas',observados);
			console.log(observados);
			return observados;
		} catch (e) {
			console.log(e);
		}
	}

	static async getAccount(cod_agenda) {
		try {
			let responseSolicitud = null;
			let accountResult = null;

			responseSolicitud = await invoke.soapExecute(
				{ cod_agenda: cod_agenda,
					usuario:'prueba',
					password:'prueba'},
				"getAccount",
				"WS-SOAP-ECOFUTURO"
			);

			if (responseSolicitud.status === "OK") {
				if (responseSolicitud.data.getAccountResult) {
					to_json(responseSolicitud.data.getAccountResult, function (error, data) {
						console.log(data);
						if (data.Cuenta != "") {
							accountResult = data.Cuenta.Cuentas;
						}
					});
				} else {
					accountResult = responseSolicitud.data.getSolicitudPropuestaResult;
				}
			}
			return accountResult;
		} catch (e) {
			console.log(e);
		}
	}

	static async getCustomer(doc_id,ext) {
		try {
			let responseSolicitud = null;
			let customerResult = null;

			responseSolicitud = await invoke.soapExecute({
					doc_id: doc_id,
					ext: ext,
					usuario:'prueba',
					password:'prueba'},
				"getCustomer",
				"WS-SOAP-ECOFUTURO"
			);

			if (responseSolicitud.status && responseSolicitud.status == "OK") {
				if (responseSolicitud.data.getCustomerResult) {
					to_json(responseSolicitud.data.getCustomerResult, function (error, data) {
						customerResult=data.DataSetAll.tmp_general;
					});
				} else {
					customerResult = responseSolicitud.data.getSolicitudPropuestaResult;
				}
			}
			return customerResult;
		} catch (e) {
			console.log(e);
		}
	}

	static async obtenerTarjetasPorNroCIyExtension(doc_id,ext) {
		try {
			let responseObtenerTarjetasPorNroCIyExtension = null;
			let obtenerTarjetasPorNroCIyExtensionResult = null;
			responseObtenerTarjetasPorNroCIyExtension = await invoke.soapExecute({
					numero_ci: doc_id,
					extension: ext,
				},
				"obtenerTarjetasPorNroCIyExtension",
				"WS-SOAP-ECOFUTURO"
			);
			if (responseObtenerTarjetasPorNroCIyExtension.status && responseObtenerTarjetasPorNroCIyExtension.status === "OK") {
				if (responseObtenerTarjetasPorNroCIyExtension.data.obtenerTarjetasPorNroCIyExtensionResult) {
					to_json(responseObtenerTarjetasPorNroCIyExtension.data.obtenerTarjetasPorNroCIyExtensionResult, function (error, data) {
						obtenerTarjetasPorNroCIyExtensionResult = data && data.TC ? data.TC.Table : null;
					});
				} else {
					obtenerTarjetasPorNroCIyExtensionResult = responseObtenerTarjetasPorNroCIyExtension.data.obtenerTarjetasPorNroCIyExtensionResult;
				}
			}
			let toReturn = [];
			for (let key in obtenerTarjetasPorNroCIyExtensionResult){
				if (typeof parseInt(key) == 'number') {
					let obtenerTarjetasPorNroCIyExtensionResultValue = obtenerTarjetasPorNroCIyExtensionResult[key];
					if (typeof obtenerTarjetasPorNroCIyExtensionResultValue == 'object') {
						toReturn.push(obtenerTarjetasPorNroCIyExtensionResultValue);
					}
				} else {
					toReturn.push(obtenerTarjetasPorNroCIyExtensionResult);
				}
			}
			return toReturn;
		} catch (e) {
			console.log(e);
		}
	}

	static async cuentasVinculadas(doc_id, ext) {
		try {
			let responseSolicitud = null;
			let cuentasVinculadasResult = null;

			responseSolicitud = await invoke.soapExecute({numero_ci:doc_id, extension:ext},'cuentasVinculadas',"WS-SOAP-ECOFUTURO");
			console.log(responseSolicitud);
			if (responseSolicitud.status && responseSolicitud.status == "OK") {
				if (responseSolicitud.data.cuentasVinculadasResult) {
					to_json(responseSolicitud.data.cuentasVinculadasResult, function (error, data) {
						cuentasVinculadasResult=data && data.TC ? data.TC.TC : null;
					});
				} else {
					cuentasVinculadasResult = responseSolicitud.data.cuentasVinculadasResult;
				}
			}
			return cuentasVinculadasResult;
		} catch (e) {
			console.log(e);
		}
	}

	static async cuentasAsociadas(docId, ext) {
		try {
			let responseSolicitud = null;
			let cuentasAsociadasResult = null;

			responseSolicitud = await invoke.soapExecute({numero_ci:doc_id, extension:ext},'cuentasAsociadas',"WS-SOAP-ECOFUTURO");

			if (responseSolicitud.status && responseSolicitud.status == "OK") {
				if (responseSolicitud.data.cuentasAsociadasResult) {
					to_json(responseSolicitud.data.cuentasAsociadasResult, function (error, data) {
						cuentasAsociadasResult=data.TC.TC;
					});
				} else {
					cuentasAsociadasResult = responseSolicitud.data.cuentasAsociadasResult;
				}
			}
			return cuentasAsociadasResult;
		} catch (e) {
			console.log(e);
		}
	}

	static async getSolicitudPrimeraEtapa(solicitud) {
		try {
				let responseGetSolicitudPrimeraEtapa = null;
				let getSolicitudPrimeraEtapaResult = null;
			responseGetSolicitudPrimeraEtapa = await invoke.soapExecute(
					{ solicitud },
					"getSolicitudPrimeraEtapa",
					"WS-SOAP-ECOFUTURO-CREDITICIOS"
				);
				if (responseGetSolicitudPrimeraEtapa.status && responseGetSolicitudPrimeraEtapa.status === "OK") {
					if (responseGetSolicitudPrimeraEtapa.data.getSolicitudPrimeraEtapaResult) {
						getSolicitudPrimeraEtapaResult = responseGetSolicitudPrimeraEtapa.data.getSolicitudPrimeraEtapaResult;
					} else {
						getSolicitudPrimeraEtapaResult = responseGetSolicitudPrimeraEtapa.data.getSolicitudPrimeraEtapaResult;
					}
				}
				return getSolicitudPrimeraEtapaResult;
		} catch (e) {
			console.log(e);
		}
	}

	static async getSolicitud(solicitud) {
		try {
				let responseSolicitud = null;
				let getSolicitudResult = null;
				responseSolicitud = await invoke.soapExecute(
					{ solicitud },
					"getSolicitud",
					"WS-SOAP-ECOFUTURO-CREDITICIOS"
				);
				if (responseSolicitud.status && responseSolicitud.status === "OK") {
					if (responseSolicitud.data.getSolicitud) {
						getSolicitudResult = responseSolicitud.data.getSolicitud;
					} else {
						getSolicitudResult = responseSolicitud.data.getSolicitud;
					}
				}
				return getSolicitudResult;
		} catch (e) {
			console.log(e);
		}
	}

	static async invokeSoapExecute(request_body, operation) {
		try {
		  return await soap.soapExecute(request_body, operation, "WS-SOAP-ECOFUTURO");
		} catch (error) {
		  throw error;
		}
	}
}

module.exports = SoapService;
