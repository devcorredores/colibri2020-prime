const modelos = require('../modelos');

class AtributoService {


    static async listaAtributosByIdPoliza (id) {
        try {
            return await modelos.sequelize.query(
                "select A.* from atributo A inner join objeto_x_atributo B on A.id=B.id_atributo inner join objeto C on B.id_objeto=C.id and id_poliza="+req.params.id
            );
        } catch (e) {
            console.log(e)
        }
    }

    static async getAllAtributosByIdObjeto (idsObjetos) {
        try {
            idsObjetos = idsObjetos && idsObjetos.indexOf(',') >= 0 ? idsObjetos.split(',') : idsObjetos;
            let objetoAtributos = await modelos.objeto_x_atributo.findAll({
                where:{id_objeto:idsObjetos},
                include:[{
                    model:modelos.atributo
                }]
            })
            let objetoAtributoWithHijos = objetoAtributos.filter(param => param.atributo.id_padre);
            for (let i = 0; i < objetoAtributoWithHijos.length; i++) {
                let objetoAtributoWithHijo = objetoAtributoWithHijos[i];
                let atributosHijos = await modelos.atributo.findAll({where:{id_padre:objetoAtributoWithHijo.id_atributo}});
                objetoAtributos[i].atributo.dataValues.atributoHijos = atributosHijos;
                objetoAtributos[i].atributo.atributoHijos = atributosHijos;
            }
            return objetoAtributos;
        } catch (e) {
            console.log(e)
        }
    }

    static async findAtributosByIdObjetoEditablesYVisibles (idObjeto,idEditable, idVisible, callback) {
        try {

            return await modelos.objeto_x_atributo.findAll({
                where: {id_objeto:idObjeto,par_comportamiento_interfaz_id:idVisible,par_editable_id:idEditable},
                include: [{
                    model: modelos.atributo,
                }, {
                    model: modelos.objeto,
                }]
            });
        } catch (e) {
            console.log(e)
        }
    }

    static async findAtributosByIdInstanciaPolizaEditablesYVisibles(idObjecto, idInstanciaPoliza,idEditable, idVisible, callback) {
        try {
            return await modelos.atributo_instancia_poliza.findAll({
                where:{id_instancia_poliza:idInstanciaPoliza},
                include:[{
                    model:modelos.objeto_x_atributo,
                    where: {id_objeto:idObjecto,par_comportamiento_interfaz_id:idVisible,par_editable_id:idEditable},
                    include: [{
                        model: modelos.atributo,
                    }, {
                        model: modelos.objeto,
                    }]
                }]
            });
        } catch (e) {
            console.log(e)
        }
    }

    static async findAllAtributosByObjeto(idObjecto, callback) {
        try {

            return await modelos.objeto_x_atributo.findAll({
                where: {id_objeto:idObjecto,par_comportamiento_interfaz_id:71,par_editable_id:70},
                include: [{
                    model: modelos.atributo,
                }, {
                    model: modelos.objeto,
                }]
            });
        } catch (e) {
            console.log(e)
        }
    }

}

module.exports = AtributoService;
