const modelos = require('../modelos');
const {loggerSolicitud} = require("../modulos/winston");
const {loggerCatch} = require("../modulos/winston");
const fechas = require('fechas');
const moment = require('moment');
const solicitudService = require('./solicitud.service');
const util = require('../utils/util');
const soap = require('./soap.service');
const { GenerarPlanPagos } = require('../modulos/funciones_plan_pago');

class SendMailService {

    static async getConfigCorreo(id) {
        try {
            let configCorreo = await modelos.config_correo.findOne({where: {id}});
            return configCorreo;
        } catch (e) {
            console.log(e);
        }
    }
}
module.exports = SendMailService;
