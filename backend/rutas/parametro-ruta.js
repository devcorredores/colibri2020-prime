const { Router } = require('express');
const router = new Router();
const bcrypt = require('bcrypt-nodejs');
const modelos = require("../modelos");
const modelosDesgravamen = require("../modelos/desgravamen");
const authentificacion = require('../modulos/passport');
const solicitudService = require("../servicios/solicitud.service");
const isLoggedIn = solicitudService.isLoggedIn;

// var isLoggedIn = (req, res, next) => {
//     console.log('session ', req.session);
//     if (req.isAuthenticated()) {
//     }
//         return next()
//     //return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
// }

router.get('/', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.parametro.findAll()
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});


router.get('/byid/:id', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const diccionario_id = req.params.id;
    await modelos.parametro.findAll({ where: { diccionario_id } })
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/byidparam/:id', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await modelos.parametro.findAll({ where: { id } })
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/GetParametrosByIdDiccionario/:id', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const diccionario_id = req.params.id;
    await modelos.parametro.findAll({ where: { diccionario_id } })
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/GetAllDiccionarios', isLoggedIn, async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await modelos.diccionario.findAll()
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/GetAllParametros', isLoggedIn, async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.parametro.findAll()
        .then(result => {
            response.data = result;
        }).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/NewDiccionario', isLoggedIn, async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.diccionario.create(req.body)
        .then(resp => {
            response.data = resp;
            response.message = 'Registro creado';
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/NewParametro', isLoggedIn, async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.parametro.create(req.body)
        .then(resp => {
            response.data = resp;
            response.message = 'Registro creado';
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/UpdateDiccionario', (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.body.id;
    modelos.diccionario.findById(id)
        .then(diccionario => {
            diccionario.update(req.body)
                .then(resp => {
                    response.message = "Registro actualizado";
                    response.data = resp;
                    res.json(response);
                })
                .catch(err => { });
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
            res.json(response);
        });
});

router.post('/UpdateParamtro', (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.body.id;
    modelos.parametro.findById(id)
        .then(parametro => {
            parametro.update(req.body)
                .then(resp => {
                    response.message = "Registro actualizado";
                    response.data = resp;
                    res.json(response);
                })
                .catch(err => { });
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
            res.json(response);
        });
});

router.delete("/EliminarDiccionario/:id", async (req, res) => {
    let response = { status: "OK", message: "", data: "" };
    const id = req.params.id;
    await modelos.diccionario
        .findOne({ where: { id } })
        .then(resp => {
            if (resp) {
                let diccionario_id = id;
                modelos.parametro.destroy({ where: { diccionario_id } }).then(resp2 => {
                    resp.destroy().then(resp => {
                        response.message = "Registro eliminado";
                        response.data = resp;
                        res.json(response);
                    })
                        .catch(err => {
                            response.status = "ERROR";
                            response.message = err;
                            res.json(response);
                        });
                })
                    .catch(err => {
                        response.status = "ERROR";
                        response.message = err;
                        res.json(response);
                    });
            } else {
                response.status = "ERROR";
                response.message = "Registro no existe";
                res.json(response);
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
            res.json(response);
        });
});

router.delete("/EliminarParametro/:id", async (req, res) => {
    let response = { status: "OK", message: "", data: "" };
    const id = req.params.id;
    await modelos.parametro
        .findOne({ where: { id } })
        .then(resp => {
            if (resp) {
                resp.destroy().then(resp => {
                    response.message = "Registro eliminado";
                    response.data = resp;
                    res.json(response);
                })
                    .catch(err => {
                        response.status = "ERROR";
                        response.message = err;
                        res.json(response);
                    });
            } else {
                response.status = "ERROR";
                response.message = "Registro no existe";
                res.json(response);
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
            res.json(response);
        });
});

router.get("/GetAllParametrosByIdDiccionarios/:ids", async (req, res) => {
    let response = { status: "OK", message: "", data: "" };
    const ids = req.params.ids.split(',');
    // let desgravamenEstados = await modelosDesgravamen.seg_estados.findAll().catch(err => {
    //     response.status = "ERROR";
    //     response.message = err;
    //     res.json(response);
    // });
    await modelos.parametro.findAll({ where: { diccionario_id: ids } }).then(resp => {
            response.message = "OK";
            response.data = resp;
            res.json(response);
        }).catch(err => {
            response.status = "ERROR";
            response.message = err;
            res.json(response);
        });
});

router.get("/GetAllEstadosDesgravamen", async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let estados = await modelosDesgravamen.seg_estados.findAll({
        attributes:['Est_Grupo','Est_Orden'],
        group:['Est_Grupo','Est_Orden'],
        order:[['Est_Orden','ASC']]
    }).catch(err => {
        response.status = "ERROR";
        response.message = err;
        res.json(response);
    });
    let estadosAll = await modelosDesgravamen.seg_estados.findAll();
    for (let i = 0; i < estadosAll.length; i++) {
        let estado = estadosAll[i];
        let estadoFound = estados.find(param => param.Est_Orden == estado.Est_Orden);
        let estadoFoundIndex = estados.findIndex(param => param.Est_Orden == estado.Est_Orden);
        if (estadoFound) {
            if (estadoFound.Est_Orden == estado.Est_Orden) {
                if (!estadoFound.Est_Codigo) {
                    estadoFound.dataValues.Est_Codigo = estado.Est_Codigo;
                } else {
                    estadoFound.dataValues.Est_Codigo += ','+estado.Est_Codigo;
                }
            }
        }
        estados[estadoFoundIndex] = estadoFound;
    }
    response.message = "OK";
    response.data.estados = estados;
    response.data.estadosAll = estadosAll;
    res.json(response);
});

router.get("/GetAllPolizasDesgravamen", async (req, res) => {
    let response = { status: "OK", message: "", data: "" };
    await modelosDesgravamen.seg_deudores.findAll({attributes:['Deu_Poliza'], group:['Deu_Poliza']}).then(resp => {
        response.message = "OK";
        response.data = resp;
        res.json(response);
    }).catch(err => {
        response.status = "ERROR";
        response.message = err;
        res.json(response);
    });
});

router.get('/GetAllParametrosByDiccionarioId/:idDiccionarios', isLoggedIn, async (req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    let idDiccionarios = req.params.idDiccionarios.split(',');
    await GetAllParametrosByDiccionarioId(idDiccionarios, async resp => {
        response.data = resp;
    });
    res.json(response);
});

async function GetAllParametrosByDiccionarioId(idDiccionarios, callback) {
    let response = { status: 'OK', message: '', data: {} };
    try {
        await modelos.parametro.findAll({
            where: { diccionario_id: idDiccionarios},
            order: [['orden','ASC']],
            attributes:[
              'id',
              'parametro_cod',
              'parametro_descripcion',
              'parametro_abreviacion',
              'diccionario_id',
              'orden',
              'id_padre',
            ],
            include: [{
              model: modelos.diccionario,
              attributes:[
                'id',
                'diccionario_codigo',
                'diccionario_descripcion',
              ]
            }],
        })
          .then(async resp => {
              response.data = resp;
          })
          .catch(err => {
              response.status = 'ERROR';
              response.message = err;
          });
        await callback(response.data);
    } catch (e) {
        response.status = 'ERROR';
        response.message = e;
    }
}

module.exports = router;
