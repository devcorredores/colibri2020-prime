const { Router } = require('express');
const router = new Router();
const ArchivoService = require('../servicios/archivo.service');
const modelos = require('../modelos');
const util = require('../utils/util');
const {loggerCatch} = require("../modulos/winston");

router.post('/upload', async (req, res) => {
  try {
  	  await ArchivoService.upload(req, res, (archivo) => {
        res.json(archivo);
      });
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

router.get('/delete/:idInstanciaDocumento', async (req, res) => {
    try {
        let response = { status: 'OK', message: '', data: '' };
        const idInstanciaDocumento = req.params.idInstanciaDocumento;
        response.data = await ArchivoService.delete(idInstanciaDocumento);
        res.json(response);
    } catch (e) {
        console.log(e)
    }

});

module.exports = router;
