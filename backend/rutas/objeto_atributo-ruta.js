const { Router } = require('express');
const router = new Router();
const bcrypt = require('bcrypt-nodejs');
const modelos = require("../modelos");

const autentificacion = require('../modulos/passport');

var isLoggedIn = (req, res, next) => {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
}

router.get('/getAllObjetoAtributosByidObjeto/:idObjeto', isLoggedIn, async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    let idObjeto = req.params.idObjeto;
    await findAllAtributosByIdObjeto(idObjeto, async resp => {
        response.data = resp;
    });
    res.json(response);
});

async function findAllAtributosByIdObjeto(idObjecto, callback) {
    let response = {status: 'OK', message: '', data: {}};
    await modelos.objeto_x_atributo.findAll({
        where: {id_objeto:idObjecto,par_comportamiento_interfaz_id:71,par_editable_id:70},
        attributes:[
            'id',
            'id_objeto',
            'id_atributo',
            'par_editable_id',
            'valor_defecto',
            'par_comportamiento_interfaz_id',
            'obligatorio',
        ],
        include: [{
            model: modelos.atributo,
            attributes:[
                'id',
                'id_tipo',
                'codigo',
                'descripcion',
                'id_diccionario',
            ],
        }, {
            model: modelos.objeto,
            attributes:[
                'id',
                'id_poliza',
                'descripcion',
                'id_tipo',
            ],
        }]
    })
        .then(async resp => {
            response.data = resp;
        })
        .catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    await callback(response.data);
}

module.exports = router;
