 const { Router } = require('express');
const router = new Router();
const VigenciaService = require('../servicios/vigencia.service');
const modelos = require('../modelos');
const util = require('../utils/util');
const {loggerCatch} = require("../modulos/winston");

router.get('/verificaPersonaDatos/:idPoliza', async (req, res) => {
  try {
  	  let response = await VigenciaService.verificaPersonaDatos(req, res);
      res.json(response);
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

router.get('/verificaVigencia/:idPoliza', async (req, res) => {
  try {
  	  let response = await VigenciaService.verificaVigencia(req, res);
      res.json(response);
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

router.get('/verificaVigencia/:idPoliza/:idInstanciaPoliza', async (req, res) => {
  try {
  	  let response = await VigenciaService.verificaVigencia(req, res);
      res.json(response);
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

router.get('/renuevaSolicitud/:idPoliza', async (req, res) => {
  try {
      let response = await VigenciaService.renuevaSolicitudes(req,res);
      res.json(response);
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

router.get('/renuevaSolicitud/:idPoliza/:idInstanciaPoliza', async (req, res) => {
  try {
      let response = await VigenciaService.renuevaSolicitudes(req,res);
      res.json(response);
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

router.get('/verificaRenovandoSolicitud/:idPoliza', async (req, res) => {
  try {
    let responseVerificaRenovandoSolicitud = await VigenciaService.verificaVigenciaRenovando(req,res);
    res.json({status:'ok', data:responseVerificaRenovandoSolicitud});
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

router.get('/verificaRenovandoSolicitud/:idPoliza/:idInstanciaPoliza', async (req, res) => {
  try {
      let responseVerificaRenovandoSolicitud = await VigenciaService.verificaVigenciaRenovando(req,res);
      res.json({status:'ok', data:responseVerificaRenovandoSolicitud});
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

router.get('/verificaRenovandoSolicitud/:idPoliza/:idInstanciaPoliza/:idEstado', async (req, res) => {
  try {
      let responseVerificaRenovandoSolicitud = await VigenciaService.verificaVigenciaRenovando(req,res);
      res.json({status:'ok', data:responseVerificaRenovandoSolicitud});
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

router.get('/verificaRenovandoSolicitud/:idPoliza/:idInstanciaPoliza/:idEstado/:gracia', async (req, res) => {
  try {
      let responseVerificaRenovandoSolicitud = await VigenciaService.verificaVigenciaRenovando(req,res);
      res.json({status:'ok', data:responseVerificaRenovandoSolicitud});
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});


router.get('/verificaRenovandoSolicitud/:idPoliza/:idInstanciaPoliza/:idEstado/:gracia/:manual', async (req, res) => {
    try {
        let responseVerificaRenovandoSolicitud = await VigenciaService.verificaVigenciaRenovando(req,res);
        res.json({status:'ok', data:responseVerificaRenovandoSolicitud});
    } catch (e) {
        console.log(e);
      loggerCatch.info(new Date(),e);
    }
});

router.get('/verificaRenovandoSolicitud/:idPoliza/:idInstanciaPoliza/:idEstado/:gracia/:manual/:idSucursal', async (req, res) => {
    try {
        let responseVerificaRenovandoSolicitud = await VigenciaService.verificaVigenciaRenovando(req,res);
        res.json({status:'ok', data:responseVerificaRenovandoSolicitud});
    } catch (e) {
        console.log(e);
      loggerCatch.info(new Date(),e);
    }
});

router.get('/verificaRenovandoSolicitud/:idPoliza/:idInstanciaPoliza/:idEstado/:gracia/:manual/:idSucursal/:idAgencia', async (req, res) => {
    try {
        let responseVerificaRenovandoSolicitud = await VigenciaService.verificaVigenciaRenovando(req,res);
        res.json({status:'ok', data:responseVerificaRenovandoSolicitud});
    } catch (e) {
        console.log(e);
      loggerCatch.info(new Date(),e);
    }
});

router.get('/tmpParaRenovacion', async (req, res) => {
  try {
    let response = {data:[]};
    let paraRenovacion = await modelos.sequelize.query(`select * from tmpParaRenovacion`);
    if (paraRenovacion && paraRenovacion.length) {
      let renovaciones = paraRenovacion[0];
      if (renovaciones && renovaciones.length) {
        for (let i = 0 ; i < renovaciones.length ; i++) {
          let renovacion = renovaciones[i];
          let req = {};
          req.params = {};
          req.params.idPoliza = renovacion.id_poliza;
          req.params.idInstanciaPoliza = renovacion.id_instancia_poliza;
          req.params.gracia = 360;
          req.params.bForceSinVigencia = true;
          let responseVerificaRenovandoSolicitud = await VigenciaService.verificaVigenciaRenovando(req,res);
          if (responseVerificaRenovandoSolicitud && responseVerificaRenovandoSolicitud.length) {
            response.data.push(responseVerificaRenovandoSolicitud);
          }
        }
      }
    }
    res.json(response);
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

module.exports = router;
