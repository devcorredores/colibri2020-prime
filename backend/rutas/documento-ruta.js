const { Router } = require('express');
const router = new Router();
const modelos = require("../modelos");
const solicitudService = require("../servicios/solicitud.service");
const isLoggedIn = solicitudService.isLoggedIn;
const Util = require("../utils/util");
const util = new Util();
const DocumentoService = require("../servicios/documento.service");
//
// var isLoggedIn = (req, res, next) => {
//     console.log('session ', req.session);
//     if (req.isAuthenticated()) {
//     }
//         return next()
//     //return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
// }

router.get('/', async (req, res) => {
    try {
        let objData = await DocumentoService.getAll();
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});


router.get('/byid/:id', async (req, res) => {
    try {
        let id = req.params.id;
        let objData = await DocumentoService.byid(id);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/byidparam/:id', async (req, res) => {
    try {
        let id = req.params.id;
        let objData = await DocumentoService.byidparam(id);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/listaDocumentosByIdPoliza/:idPoliza', isLoggedIn, async (req, res) => {
    try {
        let idPoliza = req.params.idPoliza ? req.params.idPoliza.indexOf(',') >= 0 ? req.params.idPoliza.split(',') : req.params.idPoliza : req.params.idPoliza;
        let objData = await DocumentoService.listaDocumentosByIdPoliza(idPoliza);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;
