const { Router } = require("express");
const router = new Router();
const bcrypt = require("bcrypt-nodejs");
const modelos = require("../modelos");
const moment = require("moment");
const autentificacion = require("../modulos/passport");
const { loggerCatch } = require("../modulos/winston");

async function asyncForEach(array, callback) {
    if (array && array.length) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }
}

const idPoliza = 1;
const idObjetoAtributoNroCuenta = 8;
const idObjetoAtributoDebitoAutomatico = 9;
const idObjetoAtributoTipoDocumentoId = 15;
const parNumeracionIndependiente = {
    id: 88,
    parametro_descripcion: "NUMERACION INDEPENDIENTE",
};
const parNumeracionUnificada = {
    id: 89,
    parametro_descripcion:
        "NUMERACION NUMERACION UNIFICADA",
};

router.get("/usuarios", async (req, res) => {
    // let response = { status: 'OK', message: '', data: '' };
    // await modelos.usuario.findAll({ attributes: ['id', 'usuario_login', 'usuario_nombre_completo', 'usuario_email', 'usuario_img', 'par_estado_usuario_id', 'par_estado_usuario_id'] })
    //     .then(usuarios => {
    //         response.data = usuarios;
    //     }
    //     ).catch((err) => {
    //         response.status = 'ERROR';
    //         response.message = err;
    //     });
    // res.json(response);

    /* PROPUESTA
      let response = { status: 'OK', message: '', data: [] };
      await modelos.usuario.findAll()
          .then(async usuarios => {
              response.data = usuarios;
              await  response.data.forEach(async (element,index) => {
                  let id_usuario = element.id
                  await modelos.usuario_x_rol.findAll({ where: { id_usuario } })
                      .then(usuariosxrol => {
                          element.roles = usuariosxrol;
                      }
                      ).catch((err) => {
                          response.status = 'ERROR';
                          response.message = err;
                      });
                      if (index===response.data.length-1){
                          res.json(response);
                      }
              });
              // await response.data.forEach(async element => {
              //     let usuario_id = element.id;
              //     await modelos.autenticacion_usuario.findAll({ where: { usuario_id } })
              //         .then(usuariosxaut => {
              //             element.autentificaciones = usuariosxaut;
              //         }
              //         ).catch((err) => {
              //             response.status = 'ERROR';
              //             response.message = err;
              //         });
              //     // res.json(response);
              // });

          }).catch((err) => {
              response.status = 'ERROR';
              response.message = err;
          });
      END PROPUESTA */

    /* PROPUESTA 2 */
    let response = { status: "OK", message: "datos-usuario", data: "" };
    await modelos.sequelize
        .query(
            // "select * from autenticacion_usuario C inner join (select A.*,B.id_rol from usuario A inner join usuario_x_rol B on A.id=B.id_usuario) D on C.usuario_id=D.id"
            "SELECT * FROM usuario U INNER JOIN autenticacion_usuario A ON U.id =  a.usuario_id"
        )
        .then(([results, metadata]) => {
            response.data = results;
            res.json(response);
            console.log(
                "determinar usuarios, roles y autenticacion ------------------------------------------------------> OK"
            );
        });
    /* END PROPUESTA 2 */
});

router.post("/authenticate", autentificacion.auth(), (req, res) => {
    res.status(200).json({ statusCode: 200, user: req.user });
});

var isLoggedIn = (req, res, next) => {
    console.log("session ", req.session);
    if (req.isAuthenticated()) {
        return next();
    }
    return res
        .status(400)
        .json({ statusCode: 400, message: "usuario no autentificado" });
};

router.post("/register", (req, res) => {
    // res.status(200).json({"statusCode" : 200 ,"user" : req.user});
    let response = { status: "OK", message: "", data: "" };
    modelos.persona
        .create()
        .then((persona) => {
            return res.status(301).send(persona);
        })
        .catch((err) => {
            response.status = "ERROR";
            response.message = err;
            loggerCatch.info(new Date(), err);
            // return done(null, false, req.flash('signupMessage', err.message));
        });
});

router.put("/modifica", (req, res) => {
    let response = { status: "OK", message: "", data: "" };
    // const id = req.params.id;
    const id = req.body.usuario_id;
    const usuario_id = req.body.usuario_id;
    const par_autenticacion_id = req.body.par_autenticacion_id;
    console.log("req.body modifica", req.body);
    modelos.usuario
        .findById(id)
        .then((usuario) => {
            usuario.update(req.body);
            response.data = usuario;
            modelos.autenticacion_usuario
                .findOne({ where: { usuario_id } })
                .then((autenticacion_usuario) => {
                    autenticacion_usuario.update({ par_autenticacion_id });
                    response.message = "Registro autenticacion_usuario actualizado";
                    res.json(response);
                })
                .catch((err) => {
                    response.status = "ERROR";
                    response.message = err;
                    loggerCatch.info(new Date(), err);
                    res.json(response);
                });
            // res.json(response);
        })
        .catch((err) => {
            response.status = "ERROR";
            response.message = err;
            loggerCatch.info(new Date(), err);
            res.json(response);
        });
});

router.post("/restore", isLoggedIn, (req, res) => {
    const usuario_login = req.body.username;
    let response = { status: "OK", message: "", data: "" };
    if (req.body.password != req.body.password2) {
        return res
            .status(301)
            .send({ message: "La contraseña nueva y su repeticion no coinciden." });
        // return done(null, false, req.flash('restoreMessage', 'La contraseña nueva y su repeticion no coinciden.'));
    } else {
        modelos.usuario.find({ where: { usuario_login } })
            .then((usuariosPwd) => {
                response.data = usuariosPwd.dataValues;
                if (response.data) {
                    const salt = bcrypt.hashSync(req.body.password, null, null);
                    usuariosPwd.update({ login, salt })
                        .then((usuario) => {
                            response.data = usuario.dataValues;
                            response.message = "Contraseña actualizada.";
                            return res
                                .status(200)
                                .send({ message: "Contraseña actualizada." });
                            // return done(null, response.data, req.flash('loginMessage', 'Contraseña actualizada.'));
                        }).catch((err) => {
                        response.status = "ERROR";
                        response.message =
                            "Error en actualizar al localizar usuario, reintente.";
                        loggerCatch.info(new Date(), err);
                        return res.status(401).send({
                            message: "Error en actualizar al localizar usuario, reintente.",
                        });
                        // return done(err, false, req.flash('restoreMessage', 'Error en actualizar al localizar usuario, reintente.'));
                    });
                } else {
                    response.status = "ERROR";
                    response.message = "Este usuario no esta registrado.";
                    return res
                        .status(300)
                        .send({ message: "Este usuario no esta registrado." });
                    // return done(null, false, req.flash('restoreMessage', 'Este usuario no esta registrado.'));
                }
            }).catch((err) => {
            response.status = "ERROR";
            response.message = "Error al localizar usuario, reintente.";
            loggerCatch.info(new Date(), err);
            return res
                .status(401)
                .send({ message: "Error al localizar usuario, reintente." });
            // return done(err, false, req.flash('restoreMessage', 'Error al localizar usuario, reintente.'));
        });
    }
});

router.get("/GetDatosAdicionalesPersona/:id", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: "" };
    const id_persona = req.params.id;
    await modelos.sequelize.query(
        `
        SELECT 	atributo.descripcion AS id_objeto_x_atributo,
            atributo_instancia_poliza.valor
        FROM atributo 
        INNER JOIN objeto_x_atributo 
        ON atributo.id = objeto_x_atributo.id_atributo 
        INNER JOIN objeto 
        ON objeto.id = objeto_x_atributo.id_objeto
        INNER JOIN atributo_instancia_poliza 
        ON atributo_instancia_poliza.id_objeto_x_atributo = objeto_x_atributo.id 
        INNER JOIN asegurado 
        ON asegurado.id_instancia_poliza = atributo_instancia_poliza.id_instancia_poliza 
        INNER JOIN entidad 
        ON entidad.id = asegurado.id_entidad
        WHERE entidad.id_persona = ${id_persona};`
    ).then((result) => {
        response.data = result[0];
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    res.json(response);
});

router.get("/GetAtributosDatosComplementariosSuscripcion", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: "" };
    const id_persona = req.params.id;
    await modelos.sequelize.query(
        `
        SELECT	dbo.objeto_x_atributo.id AS id_objeto_x_atributo, 
                dbo.objeto.id AS id_objeto, 
                dbo.objeto.descripcion AS objeto_descripcion, 
                dbo.atributo.id AS id_atributo, 
                dbo.atributo.descripcion AS atributo_descripcion
        FROM	dbo.atributo 
        INNER JOIN dbo.objeto_x_atributo ON dbo.atributo.id = dbo.objeto_x_atributo.id_atributo 
        INNER JOIN dbo.objeto ON dbo.objeto.id = dbo.objeto_x_atributo.id_objeto
        WHERE dbo.objeto.id = 4 AND dbo.objeto_x_atributo.id_atributo NOT IN (
        SELECT id FROM dbo.atributo WHERE id = 5 OR id = 12 OR id = 19);`
    ).then((result) => {
        response.data = result[0];
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
    });
    res.json(response);
});

router.get( "/GetDatosAdicionalesPersonaJuridica/:id", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: "" };
    const id_persona_juridica = req.params.id;
    await modelos.sequelize.query(
        `
        SELECT 	atributo.descripcion AS id_objeto_x_atributo,
		atributo_instancia_poliza.valor
        FROM atributo 
        INNER JOIN objeto_x_atributo ON atributo.id = objeto_x_atributo.id_atributo 
        INNER JOIN objeto ON objeto.id = objeto_x_atributo.id_objeto
        INNER JOIN atributo_instancia_poliza ON atributo_instancia_poliza.id_objeto_x_atributo = objeto_x_atributo.id 
        INNER JOIN asegurado ON asegurado.id_instancia_poliza = atributo_instancia_poliza.id_instancia_poliza 
        INNER JOIN entidad ON entidad.id = asegurado.id_entidad
        WHERE entidad.id_persona_juridica = ${id_persona_juridica};`
    ).then((result) => {
        response.data = result[0];
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
    });
    res.json(response);
});

router.get( "/GetDatosAdicionalesPersonaJuridica/:id", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: "" };
    const id_persona_juridica = req.params.id;
    await modelos.sequelize.query(
        `
        SELECT 	atributo.descripcion AS id_objeto_x_atributo,
		atributo_instancia_poliza.valor
        FROM atributo 
        INNER JOIN objeto_x_atributo ON atributo.id = objeto_x_atributo.id_atributo 
        INNER JOIN objeto ON objeto.id = objeto_x_atributo.id_objeto
        INNER JOIN atributo_instancia_poliza ON atributo_instancia_poliza.id_objeto_x_atributo = objeto_x_atributo.id 
        INNER JOIN asegurado ON asegurado.id_instancia_poliza = atributo_instancia_poliza.id_instancia_poliza 
        INNER JOIN entidad ON entidad.id = asegurado.id_entidad
        WHERE entidad.id_persona_juridica = ${id_persona_juridica};`
    ).then((result) => {
        response.data = result[0];
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    res.json(response);
});

router.get("/FindPersonaSolicitudByIdInstanciaPoliza/:idObjeto/:idInstanciaPoliza", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let idInstanciaPoliza = req.params.idInstanciaPoliza;
    let idObjeto = req.params.idObjeto;
    await oldFindPersonaSolicitudByIdInstanciaPoliza(idObjeto, idInstanciaPoliza,async (resp) => {
        response = resp;
    });
    res.json(response);
});

router.post("/actualizarPersonaExt/:idPersona", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let idInstanciaPoliza = req.params.idInstanciaPoliza;
    let idPersona = req.body.idPersona;
    let docId = req.body.docId;
    let ext = req.body.ext;
    await actualizarPersonaExt(idInstanciaPoliza, idPersona, docId, ext, async (resp) => {
        response = resp;
    });
    res.json(response);
});

router.get("/FindPersonaSolicitudConAtributosByDocIdYPoliza/:docId/:docIdExt/:idPoliza/:idObjeto/:idInstanciaPoliza", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let docId = req.params.docId;
    let docIdExt = req.params.docIdExt;
    let idPoliza = req.params.idPoliza;
    let idObjeto = req.params.idObjeto;
    let idInstanciaPoliza = req.params.idInstanciaPoliza;
    await findPersonaSolicitudConAtributosByDocIdYPoliza(docId, docIdExt, idPoliza, idObjeto, idInstanciaPoliza,async (resp) => {
        response = resp;
    });
    res.json(response);
});

router.get( "/FindPersonaSolicitudesByDocIdYPoliza/:docId/:docIdExt/:idPoliza", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let docId = req.params.docId;
    let docIdExt = req.params.docIdExt;
    let idPoliza = req.params.idPoliza;
    await findPersonaByDocId(docId, docIdExt, async (resp) => {
        let persona = resp && resp.data ? resp.data : null;
        await findSolicitudesByPersonaEntidadYPoliza(persona, idPoliza,async (resp) => {
            response = resp;
        });
    });
    res.json(response);
});

router.get( "/FindPersonaByDocId/:docId/:docIdExt", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let docId = req.params.docId;
    let docIdExt = req.params.docIdExt;
    await findPersonaByDocId(docId, docIdExt, null, async (resp) => {
        response = resp;
    });
    res.json(response);
});

router.get( "/FindAseguradoByDocId/:docId/:docIdExt", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let docId = req.params.docId;
    let docIdExt = req.params.docIdExt;
    await findAseguradoByDocId(docId, docIdExt, async (resp) => {
        response = resp;
    });
    res.json(response);
});

router.get("/FindPersonasAseguradasConAtributosByDocIdExtYPoliza/:docId/:docIdExt/:idObjeto/:idPoliza", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let docId = req.params.docId;
    let docIdExt = req.params.docIdExt ? req.params.docIdExt.split(',') : req.params.docIdExt;
    let idPoliza = req.params.idPoliza ? req.params.idPoliza.split(',') : req.params.idPoliza;
    let idObjeto = req.params.idObjeto ? req.params.idObjeto.split(',') : req.params.idObjeto;
    await findPersonasAseguradasConAtributosByDocIdYPoliza(docId, docIdExt, idObjeto, idPoliza, async (resp) => {
        response = resp;
    });
    res.json(response);
});

router.get("/FindPersonasAseguradasConAtributosByDocIdYPoliza/:docId/:idObjeto/:idPoliza", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let docId = req.params.docId;
    let idPoliza = req.params.idPoliza ? req.params.idPoliza.split(',') : req.params.idPoliza;
    let idObjeto = req.params.idObjeto ? req.params.idObjeto.split(',') : req.params.idObjeto;
    await findPersonasAseguradasConAtributosByDocIdYPoliza(docId, idObjeto, idPoliza, async (resp) => {
        response = resp;
    });
    res.json(response);
});

router.get( "/FindPersonasAseguradasByDocIdYPoliza/:docId/:docIdExt/:idObjeto/:idPoliza", isLoggedIn, async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let docId = req.params.docId;
    let docIdExt = req.params.docIdExt;
    let idPoliza = req.params.idPoliza;
    let idObjeto = req.params.idObjeto;
    await findPersonasAseguradasByDocIdYPoliza( docId, docIdExt, idObjeto, idPoliza, async (resp) => {
        response = resp;
    });
    res.json(response);
});

async function findSolicitudesByPersonaEntidadYPoliza(persona, id_poliza, callback) {
    let response = { status: "OK", message: "", data: {} };
    response.data.persona = persona;
    response.data.solicitudes = [];
    await modelos.asegurado.findAll({
        where: { id_entidad: persona.entidad.id },
        include: [
            {
                model: modelos.instancia_poliza,
                where: { id_poliza: id_poliza },
            },
        ],
    }).then(async (asegurados) => {
        await asyncForEach(asegurados, async (object) => {
            let asegurado = object.get();
            await modelos.objeto_x_atributo.findAll({
                include: [
                    {
                        model: modelos.atributo_instancia_poliza,
                        where: { id_instancia_poliza: asegurado.id_instancia_poliza },
                    },
                    { model: modelos.atributo },
                ],
            }).then(async (resp) => {
                asegurado.atributos = resp;
                await response.data.solicitudes.push(asegurado);
            }).catch((err) => {
                response.status = "ERROR";
                response.message = err;
                loggerCatch.info(new Date(), err);
            });
        });
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    await callback(response);
}

async function findSolicitudByPersonaEntidad(persona, id_instancia_poliza, id_poliza, callback) {
    let response = { status: "OK", message: "", data: {} };
    response.data.persona = persona;
    response.data.solicitud = {};
    await modelos.asegurado.find({
        where: {
            id_entidad: persona.entidad.id,
            id_instancia_poliza: id_instancia_poliza,
        },
        include: [
            {
                model: modelos.instancia_poliza,
                where: { id_poliza: id_poliza },
            },
        ],
    }).then(async (resp) => {
        let asegurado = resp.get();
        await modelos.objeto_x_atributo.findAll({
            include: [
                {
                    model: modelos.atributo_instancia_poliza,
                    where: { id_instancia_poliza: asegurado.id_instancia_poliza },
                },
                { model: modelos.atributo },
            ],
        }).then(async (resp) => {
            asegurado.atributos = resp;
            response.data.solicitud = asegurado;
        }).catch((err) => {
            response.status = "ERROR";
            response.message = err;
            loggerCatch.info(new Date(), err);
        });
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    await callback(response);
}

async function findPersonaByDocId(docId, docIdExt, callback) {
    let query, seqQuery;
    let response = { status: "OK", message: "", data: {} };
    seqQuery = {
        where: { persona_doc_id: docId, persona_doc_id_ext: docIdExt },
        include: [
            {
                model: modelos.entidad,
            },
        ],
    };

    query = seqQuery;

    await modelos.persona.find(query).then(async (resp) => {
        response.data = resp ? resp.get() : null;
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    await callback(response);
}

async function findEntidadPersonaByDocId(docId, docIdExt, idEntidad, callback) {
    let response = { status: "OK", message: "", data: {} };
    await modelos.persona.find({
        where: { persona_doc_id: docId, persona_doc_id_ext: docIdExt },
        include: [
            {
                model: modelos.entidad,
                where: { id: idEntidad },
            },
        ],
    }).then(async (resp) => {
        response.data = resp.get();
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    await callback(response);
}

async function findPersonasAseguradasConAtributosByDocIdYPoliza(docId, idObjeto, idPoliza, callback) {
    let response = { status: "OK", message: "", data: {} };
    let asegurados = [];
    await modelos.asegurado.findAll({
        include: [
            {
                model: modelos.entidad,
                include: [
                    {
                        model: modelos.persona,
                        where: { persona_doc_id: docId },
                    },
                ],
            },
            {
                model: modelos.instancia_poliza,
                where: { id_poliza: idPoliza, id_estado: { $notIn: [60, 62, 65, 282] } },
                include: [
                    { model: modelos.instancia_documento },
                    { model: modelos.instancia_poliza_transicion },
                    { model: modelos.poliza },
                    { model: modelos.parametro, as:'estado' },
                    {
                        model: modelos.atributo_instancia_poliza,
                        include: [
                            {
                                model: modelos.objeto_x_atributo,
                                include: [
                                    {
                                        model: modelos.atributo,
                                    },
                                    {
                                        model: modelos.objeto,
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_editable",
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_comportamiento_interfaz",
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    }).then(async (resp) => {
        response.data = resp;
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    await callback(response);
}

async function findPersonasAseguradasByDocIdYPoliza(docId, docIdExt, idObjeto, idPoliza, callback) {
    let response = { status: "OK", message: "", data: {} };
    let asegurados = [];
    await modelos.asegurado.findAll({
        include: [
            {
                model: modelos.entidad,
                include: [
                    {
                        model: modelos.persona,
                        where: { persona_doc_id: docId },
                    },
                ],
            },
            {
                model: modelos.instancia_poliza,
                where: {
                    id_poliza: idPoliza,
                    id_estado: { $not: [60, 62, 282, 65] },
                },
                include: [{ model: modelos.instancia_documento }],
            },
        ],
    }).then(async (resp) => {
        response.data = resp;
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    await callback(response);
}

async function findAseguradoByDocId(docId, docIdExt, callback) {
    let response = { status: "OK", message: "", data: {} };
    await modelos.asegurado.findAll({
        include: [
            {
                model: modelos.entidad,
                include: [
                    {
                        model: modelos.persona,
                        where: { persona_doc_id: docId, persona_doc_id_ext: docIdExt },
                    },
                ],
            },
        ],
    }).then(async (resp) => {
        response.data = resp;
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    await callback(response);
}

async function findPersonaSolicitudConAtributosByDocIdYPoliza( docId, docIdExt, idPoliza, idObjeto, idInstanciaPoliza, callback) {
    let response = { status: "OK", message: "", data: {} };
    await modelos.asegurado.findAll({
        where: { id_instancia_poliza: idInstanciaPoliza },
        include: [
            {
                model: modelos.entidad,
                include: [
                    {
                        model: modelos.persona,
                        where: { persona_doc_id: docId, persona_doc_id_ext: docIdExt },
                    },
                ],
            },
            {
                model: modelos.instancia_poliza,
                where: { id_poliza: idPoliza },
                include: [
                    {
                        model: modelos.parametro,
                        as: "estado",
                    },
                    {
                        model: modelos.poliza,
                        include: [
                            {
                                model: modelos.parametro,
                                as: "ramo",
                            },
                            {
                                model: modelos.parametro,
                                as: "tipo_numeracion",
                            },
                            {
                                model: modelos.entidad,
                                as: "aseguradora",
                            },
                        ],
                    },
                    {
                        model: modelos.atributo_instancia_poliza,
                        include: [
                            {
                                model: modelos.objeto_x_atributo,
                                include: [
                                    {
                                        model: modelos.atributo,
                                    },
                                    {
                                        model: modelos.objeto,
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_editable",
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_comportamiento_interfaz",
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        model: modelos.instancia_documento,
                        include: [
                            {
                                model: modelos.documento,
                                include:{
                                    model: modelos.archivo,
                                }
                            },
                            {
                                model: modelos.atributo_instancia_documento,
                            },
                        ],
                    },
                ],
            },
            {
                model: modelos.beneficiario,
                include: [
                    {
                        model: modelos.asegurado,
                    },
                    {
                        model: modelos.entidad,
                        include: [
                            {
                                model: modelos.persona,
                            },
                        ],
                    },
                    {
                        model: modelos.parametro,
                        as: "estado_obj",
                    },
                    {
                        model: modelos.parametro,
                        as: "tipo_obj",
                    },
                    {
                        model: modelos.parametro,
                        as: "parentesco",
                    },
                ],
            },
        ],
    }).then(async (resp) => {
        response.data = resp;
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    await callback(response);
}

async function FindPersonaSolicitudByIdInstanciaPoliza(idObjeto, idInstanciaPoliza, callback) {
    let response = { status: "OK", message: "", data: {} };
    await modelos.asegurado.find({
        where: { id_instancia_poliza: idInstanciaPoliza },
        include: [
            {
                model: modelos.entidad,
                include: [
                    {
                        model: modelos.persona,
                    },
                ],
            },
            {
                model: modelos.instancia_poliza,
                include: [
                    {
                        model: modelos.atributo_instancia_poliza,
                        include: [
                            {
                                model: modelos.objeto_x_atributo,
                                include: [
                                    {
                                        model: modelos.atributo,
                                    },
                                    {
                                        model: modelos.objeto,
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_editable",
                                    },
                                    {
                                        model: modelos.parametro,
                                        as: "par_comportamiento_interfaz",
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        model: modelos.instancia_documento,
                        include: [
                            {
                                model: modelos.documento,
                            },
                            {
                                model: modelos.atributo_instancia_documento,
                            },
                        ],
                    },
                    {
                        model: modelos.anexo_poliza
                    }
                ],
            },
        ],
    }).then(async (resp) => {
        let asegurado = resp.dataValues;
        let respUsuario = await modelos.usuario.findOne({
            where: { id: asegurado.adicionado_por },
            include: {
                model: modelos.usuario_x_rol,
                include: {
                    model: modelos.rol,
                },
            },
        });
        let beneficiarios = await modelos.beneficiario.findAll({
            where: { id_asegurado: asegurado.id },
            include: [
                {
                    model: modelos.entidad,
                    include: [
                        {
                            model: modelos.persona,
                        },
                    ],
                },
                {
                    model: modelos.parametro,
                    as: "estado_obj",
                },
                {
                    model: modelos.parametro,
                    as: "tipo_obj",
                },
                {
                    model: modelos.parametro,
                    as: "parentesco",
                },
            ],
        });
        let respEstado = await modelos.parametro.findOne({
            where: { id: asegurado.instancia_poliza.id_estado },
        });
        let respPoliza = await modelos.poliza.findOne({
            where: { id: asegurado.instancia_poliza.id_poliza },
            include: [
                {
                    model: modelos.parametro,
                    as: "ramo",
                },
                {
                    model: modelos.parametro,
                    as: "tipo_numeracion",
                },
                {
                    model: modelos.entidad,
                    as: "aseguradora",
                },
            ],
        });

        asegurado.instancia_poliza.usuario = respUsuario;
        asegurado.instancia_poliza.estado = respEstado;
        asegurado.instancia_poliza.poliza = respPoliza;
        asegurado.beneficiarios = beneficiarios;
        response.data = asegurado;
    }).catch((err) => {
        response.status = "ERROR";
        response.message = err;
        loggerCatch.info(new Date(), err);
    });
    await callback(response);
}

async function actualizarPersonaExt(idInstanciaPoliza, idPersona, docId, ext, callback) {
    try {
        let personaUpdated = await modelos.persona.update({persona_doc_id_ext:ext}, {where:{id: idPersona}});
        let instanciaPolizaUpdated = await modelos.instancia_documento.update({asegurado_doc_id_ext:ext}, {where:{id_instancia_poliza: idInstanciaPoliza}});
        let persona = await modelos.persona.findOne({where:{id:idPersona}});
        let instancia_documentos = await modelos.instancia_documentos.findAll({where:{id_instancia_poliza:idInstanciaPoliza}});
        if (typeof callback == 'function') {
            callback({persona, instancia_documentos});
        }
    } catch (e) {
        console.log(e);
    }
}
async function oldFindPersonaSolicitudByIdInstanciaPoliza(idObjeto, idInstanciaPoliza, callback) {
    let response = { status: "OK", message: "", data: {} };
    if (idInstanciaPoliza) {
        await modelos.asegurado.find({
            where: { id_instancia_poliza: idInstanciaPoliza },
            include: [
                {
                    model: modelos.entidad,
                    include: [
                        {
                            model: modelos.persona,
                        },
                    ],
                },
                {
                    model: modelos.instancia_poliza,
                    include: [
                        {
                            model: modelos.anexo_poliza,
                        },
                        {
                            model: modelos.parametro,
                            as: "estado",
                        },
                        {
                            model: modelos.poliza,
                            include: [
                                {
                                    model: modelos.parametro,
                                    as: "ramo",
                                },
                                {
                                    model: modelos.parametro,
                                    as: "tipo_numeracion",
                                },
                                {
                                    model: modelos.entidad,
                                    as: "aseguradora",
                                },
                            ],
                        },
                    ],
                },
            ],
        }).then(async (resp) => {
            if (resp) {
                let asegurado = resp.dataValues;
                asegurado.entidad.persona.dataValues.fecha_nacimiento_str = '';
                let usuario = await modelos.usuario.findOne({
                    where: { id: asegurado.adicionado_por },
                    include: {
                        model: modelos.usuario_x_rol,
                        include: {
                            model: modelos.rol,
                        },
                    },
                });
                let beneficiarios = await modelos.beneficiario.findAll({
                    where: { id_asegurado: asegurado.id },
                    include: [
                        {
                            model: modelos.entidad,
                            include: [
                                {
                                    model: modelos.persona,
                                },
                            ],
                        },
                        {
                            model: modelos.parametro,
                            as: "estado_obj",
                        },

            {
              model: modelos.parametro,
              as: "condicion_obj",
            },
                        {
                            model: modelos.parametro,
                            as: "tipo_obj",
                        },
                        {
                            model: modelos.parametro,
                            as: "parentesco",
                        },
                    ],
                });
                let instanciaPolizaAtributos = await modelos.atributo_instancia_poliza.findAll({
                    where: { id_instancia_poliza: idInstanciaPoliza },
                    include: [
                        {
                            model: modelos.objeto_x_atributo,
                            include: [
                                {
                                    model: modelos.atributo,
                                },
                                {
                                    model: modelos.objeto,
                                },
                                {
                                    model: modelos.parametro,
                                    as: "par_editable",
                                },
                                {
                                    model: modelos.parametro,
                                    as: "par_comportamiento_interfaz",
                                },
                            ],
                        },
                    ],
                });
                let instanciaPolizaDocumentos = await modelos.instancia_documento.findAll({
                    where: { id_instancia_poliza: idInstanciaPoliza },
                    include: [
                        {
                            model: modelos.documento,
                            include: {
                                model: modelos.archivo
                            }
                        },
                        {
                            model: modelos.atributo_instancia_documento,
                        },
                    ],
                });
                asegurado.instancia_poliza.dataValues.atributo_instancia_polizas = [];
                asegurado.instancia_poliza.dataValues.atributo_instancia_polizas = instanciaPolizaAtributos;
                asegurado.instancia_poliza.dataValues.instancia_documentos = [];
                asegurado.instancia_poliza.dataValues.instancia_documentos = instanciaPolizaDocumentos;
                asegurado.usuario = usuario;
                asegurado.beneficiarios = beneficiarios;
                response.data = asegurado;
            } else {
                await callback(response);
            }
        }).catch((err) => {
            response.status = "ERROR";
            response.message = err;
            loggerCatch.info(new Date(), err);
        });
        await callback(response);
    } else {
        await callback(response);
    }
}

async function saveOrUpdateEntidadPersona(entidad, usuario, callback) {
    try {
        let query;
        let hoy = new Date();
        let createdAt = new Date();
        let fecNacimiento = null;
        let new_entidad = entidad;
        let new_persona = entidad.persona;
        if (entidad.persona.persona_fecha_nacimiento != null) {
            var año = parseInt(entidad.persona.persona_fecha_nacimiento.substring(0, 4));
            var mes = parseInt(entidad.persona.persona_fecha_nacimiento.substring(5, 7));
            var dia = parseInt(entidad.persona.persona_fecha_nacimiento.substring(8, 10));
            fecNacimiento = año + "/" + mes + "/" + dia + " " + "12:00";
        }
        new_persona.persona_fecha_nacimiento = fecNacimiento;
        await findPersonaByDocId(
            new_persona.persona_doc_id,
            new_persona.persona_doc_id_ext,
            async (resp) => {
                let response = { status: "OK", message: "", data: {} };
                let old_persona = resp && resp.data ? resp.data : null;
                if (old_persona && Object.keys(old_persona).length) {
                    if (old_persona.entidad && Object.keys(old_persona.entidad).length) {
                        new_entidad = old_persona.entidad.get();
                        // new_entidad.id_persona = old_persona.id;
                        //new_entidad.modificada_por = '2';
                        //new_entidad.updatedAt = createdAt;
                        //await modelos.entidad.update(new_entidad, {where:{id:old_persona.entidad.id}})
                        //    .then(async resp => {
                        //        if(resp.length && resp[0]) {
                        response.data = new_entidad;
                        new_persona.modificada_por = usuario.id;
                        new_persona.adicionada_por = old_persona.adicionada_por;
                        new_persona.updatedAt = createdAt;
                        new_persona.createdAt = old_persona.createdAt;
                        await modelos.persona
                            .update(new_persona, { where: { id: old_persona.id } })
                            .then(async (resp) => {
                                if (resp.length && resp[0]) {
                                    response.data.persona = new_persona;
                                }
                            })
                            .catch((err) => {
                                response.status = "ERROR";
                                response.message = err;
                                loggerCatch.info(new Date(), err);
                            });
                        //        }
                        //    })
                        //    .catch((err) => {
                        //        response.status = 'ERROR';
                        //        response.message = err;
                        //    });
                    } else {
                        new_persona.id = null;
                        new_persona.createdAt = createdAt;
                        new_persona.updatedAt = createdAt;
                        new_persona.adicionada_por = usuario.id;
                        new_persona.modificada_por = usuario.id;
                        await modelos.persona
                            .create(new_persona)
                            .then(async (resp) => {
                                let persona = resp.dataValues;
                                new_entidad.id = null;
                                new_entidad.id_persona = persona.id;
                                new_entidad.id_persona_juridica = null;
                                new_entidad.tipo_entidad = "12";
                                new_entidad.createdAt = createdAt;
                                new_entidad.updatedAt = createdAt;
                                new_entidad.adicionada_por = usuario.id;
                                new_entidad.modificada_por = usuario.id;
                                await modelos.entidad
                                    .create(new_entidad)
                                    .then(async (resp) => {
                                        response.data = resp.dataValues;
                                        response.data.persona = persona;
                                    })
                                    .catch((err) => {
                                        response.status = "ERROR";
                                        response.message = err;
                                        loggerCatch.info(new Date(), err);
                                    });
                            })
                            .catch((err) => {
                                response.status = "ERROR";
                                response.message = err;
                                loggerCatch.info(new Date(), err);
                            });
                    }
                } else {
                    new_persona.id = null;
                    new_persona.createdAt = createdAt;
                    new_persona.updatedAt = createdAt;
                    new_persona.adicionada_por = usuario.id;
                    new_persona.modificada_por = usuario.id;
                    await modelos.persona
                        .create(new_persona)
                        .then(async (resp) => {
                            let persona = resp.dataValues;
                            new_entidad.id = null;
                            new_entidad.id_persona = persona.id;
                            new_entidad.id_persona_juridica = null;
                            new_entidad.tipo_entidad = "12";
                            new_entidad.createdAt = createdAt;
                            new_entidad.updatedAt = createdAt;
                            new_entidad.adicionada_por = usuario.id;
                            new_entidad.modificada_por = usuario.id;
                            await modelos.entidad
                                .create(new_entidad)
                                .then(async (resp) => {
                                    response.data = resp.dataValues;
                                    response.data.persona = persona;
                                })
                                .catch((err) => {
                                    response.status = "ERROR";
                                    response.message = err;
                                    loggerCatch.info(new Date(), err);
                                });
                        })
                        .catch((err) => {
                            response.status = "ERROR";
                            response.message = err;
                            loggerCatch.info(new Date(), err);
                        });
                }
                await callback(response);
            }
        );
    } catch (e) {
        console.log(e);
        loggerCatch.info(new Date(), e);
    }
}

async function updateTransicion(instancia_poliza_transicion, usuario) {
    let seqQuery;
    let hoy = new Date();
    let modifiedAt = new Date();
    instancia_poliza_transicion.modificado_por = usuario ? usuario.id : "2";
    instancia_poliza_transicion.updatedAt = modifiedAt;
    let createdAt = instancia_poliza_transicion.createdAt;
    delete instancia_poliza_transicion.createdAt;
    let query = {
        where: {
            id: instancia_poliza_transicion.id,
            par_estado_id: instancia_poliza_transicion.par_estado_id,
        },
    };
    seqQuery = query;
    await modelos.instancia_poliza_transicion.update(instancia_poliza_transicion, seqQuery)
        .catch((err) => {
            loggerCatch.info(new Date(), err);
        });
    instancia_poliza_transicion.createdAt = createdAt;
    return instancia_poliza_transicion;
}

async function createTransicion(instancia_poliza_transicion, usuario) {
    let query;
    let hoy = new Date();
    let createdAt = new Date();
    let newInstanciaPolizaTransaccion = {
        id_instancia_poliza: instancia_poliza_transicion.id_instancia_poliza,
        observacion: instancia_poliza_transicion.observacion,
        par_observacion_id: instancia_poliza_transicion.par_observacion_id,
        par_estado_id: instancia_poliza_transicion.par_estado_id,
        adicionado_por: usuario != null ? usuario.id : "2",
        modificado_por: usuario != null ? usuario.id : "2",
        updatedAt: createdAt,
        createdAt: createdAt,
    };
    let seQuery = {
        where: {
            par_estado_id: instancia_poliza_transicion.par_estado_id,
            observacion: { $like: instancia_poliza_transicion.observacion },
            id: instancia_poliza_transicion.id,
        },
    };
    query = seQuery;
    let oldInstanciaTransicion = await modelos.instancia_poliza_transicion
        .find(query)
        .then((resp) => {
            if (resp) {
                return resp.dataValues;
            }
        })
        .catch((err) => {
            loggerCatch.info(new Date(), err);
        });

    if (oldInstanciaTransicion) {
        oldInstanciaTransicion.par_observacion_id =
            instancia_poliza_transicion.par_observacion_id;
        updateTransicion(oldInstanciaTransicion, usuario);
    } else {
        await modelos.instancia_poliza_transicion
            .create(newInstanciaPolizaTransaccion)
            .then(async (resp) => {
                instancia_poliza_transicion = resp.dataValues;
            })
            .catch((err) => {
                loggerCatch.info(new Date(), err);
            });
    }
    return instancia_poliza_transicion;
}

router.post("/ActualizarSolicitud", isLoggedIn, async (req, res) => {
    try {
        let response = { status: "OK", message: "", data: {} };
        let hoy = new Date();
        // let modifiedAt = hoy.getFullYear() + '/' + (hoy.getMonth() + 1) + '/' + hoy.getDate() + ' ' + hoy.getUTCHours() + ':' + hoy.getUTCMinutes() + ':' + hoy.getUTCSeconds();
        // let createdAt = hoy.getFullYear() + '/' + (hoy.getMonth() + 1) + '/' + hoy.getDate() + ' ' + hoy.getUTCHours() + ':' + hoy.getUTCMinutes() + ':' + hoy.getUTCSeconds();
        let modifiedAt = new Date();
        let createdAt = new Date();
        let id_usuario = req.session.passport.user;
        let usuario = null;
        await modelos.usuario.findOne({
            where: { id: id_usuario },
            include: {
                model: modelos.usuario_x_rol,
                include: {
                    model: modelos.rol,
                },
            },
        }).then(async (resp) => {
            if (Object.keys(resp.dataValues).length) {
                usuario = resp.dataValues;
            }
        }).catch((err) => {
            response.status = "ERROR";
            response.message = err;
            loggerCatch.info(new Date(), err);
        });
        let post = req.body;
        let new_asegurado = post;

        //new_asegurado.modificado_por = '2';
        //new_asegurado.updatedAt = 'modifiedAt';
        let new_instancia_poliza = new_asegurado.instancia_poliza;
        let new_instancia_documentos = new_asegurado.instancia_poliza.instancia_documentos;
        let anexoPoliza = await modelos.anexo_poliza.findOne({where:{id_poliza:new_asegurado.instancia_poliza.id_poliza, id_tipo:288, vigente:true}});
        let fechaRegistro = null;
        if (new_instancia_poliza.fecha_registro != null) {
            fechaRegistro = new Date(new_instancia_poliza.fecha_registro);
        }
        new_instancia_poliza.fecha_registro = fechaRegistro;
        let new_atributos_by_instancia_documentos = [];
        let new_atributos_by_instancia_documentos_inter = [];
        await new_instancia_documentos.forEach((new_instancia_documento, index) => {
            new_atributos_by_instancia_documentos[index] = new_instancia_documento.atributo_instancia_documentos;
            new_atributos_by_instancia_documentos_inter[index] = new_instancia_documento.atributo_instancia_documentos_inter;
        });

        await saveOrUpdateEntidadPersona(new_asegurado.entidad, usuario, async (resp) => {
                let entidad = resp && resp.data ? resp.data : null;
                //await modelos.asegurado.update(new_asegurado, {where:{id:new_asegurado.id}})
                //    .then(async resp => {
                //        if (resp.length && resp[0]) {
                new_asegurado.entidad = entidad;
                new_instancia_poliza.modificada_por = usuario ? usuario.id : "2";
                new_instancia_poliza.updatedAt = modifiedAt;
                new_instancia_poliza.id_anexo_poliza = anexoPoliza.id;
                //            await modelos.instancia_poliza.update(new_instancia_poliza,{where:{id:new_instancia_poliza.id}})
                //                .then(async resp => {
                //                    if (resp.length && resp[0]) {
                new_asegurado.instancia_poliza = new_instancia_poliza;
                // new_asegurado.instancia_poliza.atributo_instancia_polizas_inter = new_asegurado.atributo_instancia_polizas_inter;
                //                    }
                // })
                // .catch(err => {
                //     response.status = 'ERROR';
                //     response.message = err;
                // });

                let oldAtributosInstanciPoliza = await modelos.atributo_instancia_poliza.findAll({
                    where: { id_instancia_poliza: new_instancia_poliza.id },
                });
                for (let i = 0; i < new_instancia_poliza.atributo_instancia_polizas.length; i++) {
                    let new_atributo_instancia_poliza = new_instancia_poliza.atributo_instancia_polizas[i];
                    new_atributo_instancia_poliza.modificado_por = usuario ? usuario.id : "2";
                    new_atributo_instancia_poliza.updatedAt = modifiedAt;
                    let oldAtributoInstanciaPoliza = oldAtributosInstanciPoliza.find((param) => param.dataValues.id_instancia_poliza == new_atributo_instancia_poliza.id_instancia_poliza && param.dataValues.id_objeto_x_atributo == new_atributo_instancia_poliza.id_objeto_x_atributo);
                    if ( oldAtributoInstanciaPoliza && new_atributo_instancia_poliza.id_instancia_poliza && new_atributo_instancia_poliza.valor != "" ) {
                        await modelos.atributo_instancia_poliza.update(new_atributo_instancia_poliza, {
                            where: { id: new_atributo_instancia_poliza.id },
                        }).then(async (resp) => {
                            if (resp.length && resp[0]) {
                                new_atributo_instancia_poliza.instancia_poliza = new_instancia_poliza;
                                new_atributo_instancia_poliza.instancia_poliza = null;
                                new_asegurado.instancia_poliza.atributo_instancia_polizas[i] =
                                    {
                                        id: new_atributo_instancia_poliza.id,
                                        id_instancia_poliza: new_atributo_instancia_poliza.id_instancia_poliza,
                                        id_objeto_x_atributo: new_atributo_instancia_poliza.id_objeto_x_atributo,
                                        valor: new_atributo_instancia_poliza.valor,
                                        tipo_error: new_atributo_instancia_poliza.tipo_error,
                                        objeto_x_atributo: new_atributo_instancia_poliza.objeto_x_atributo,
                                    };
                            }
                        }).catch((err) => {
                            response.status = "ERROR";
                            response.message = err;
                            loggerCatch.info(new Date(), err);
                        });
                    } else if ( oldAtributoInstanciaPoliza && oldAtributoInstanciaPoliza.id ) {
                        await modelos.atributo_instancia_poliza.update(new_atributo_instancia_poliza, {
                            where: { id: oldAtributosInstanciPoliza.id },
                        }).then(async (resp) => {
                            if (resp.length && resp[0]) {
                                new_atributo_instancia_poliza.instancia_poliza = new_instancia_poliza;
                                new_atributo_instancia_poliza.instancia_poliza = null;
                                new_asegurado.instancia_poliza.atributo_instancia_polizas[i] =
                                    {
                                        id: new_atributo_instancia_poliza.id,
                                        id_instancia_poliza: new_atributo_instancia_poliza.id_instancia_poliza,
                                        id_objeto_x_atributo: new_atributo_instancia_poliza.id_objeto_x_atributo,
                                        valor: new_atributo_instancia_poliza.valor,
                                        tipo_error: new_atributo_instancia_poliza.tipo_error,
                                        objeto_x_atributo: new_atributo_instancia_poliza.objeto_x_atributo,
                                    };
                            }
                        }).catch((err) => {
                            response.status = "ERROR";
                            response.message = err;
                            loggerCatch.info(new Date(), err);
                        });
                    } else {
                        new_atributo_instancia_poliza.id = null;
                        new_atributo_instancia_poliza.id_instancia_poliza = new_instancia_poliza.id;
                        new_atributo_instancia_poliza.adicionado_por = usuario ? usuario.id : "2";
                        new_atributo_instancia_poliza.modificado_por = usuario ? usuario.id : "2";
                        new_atributo_instancia_poliza.createdAt = createdAt;
                        new_atributo_instancia_poliza.updatedAt = createdAt;
                        new_atributo_instancia_poliza.valor = new_atributo_instancia_poliza.valor ? new_atributo_instancia_poliza.valor.toString() : "";
                        if (!oldAtributoInstanciaPoliza) {
                            await modelos.atributo_instancia_poliza.create(new_atributo_instancia_poliza)
                                .then(async (resp) => {
                                    new_atributo_instancia_poliza = resp.dataValues;
                                    new_atributo_instancia_poliza.instancia_poliza = new_instancia_poliza;
                                    new_atributo_instancia_poliza.instancia_poliza = null;
                                    new_asegurado.instancia_poliza.atributo_instancia_polizas[i] =
                                        {
                                            id: new_atributo_instancia_poliza.id,
                                            id_instancia_poliza: new_atributo_instancia_poliza.id_instancia_poliza,
                                            id_objeto_x_atributo: new_atributo_instancia_poliza.id_objeto_x_atributo,
                                            valor: new_atributo_instancia_poliza.valor,
                                            tipo_error: new_atributo_instancia_poliza.tipo_error,
                                            objeto_x_atributo: new_atributo_instancia_poliza.objeto_x_atributo,
                                            // adicionado_por: new_atributo_instancia_poliza.adicionado_por,
                                            // modificado_por: new_atributo_instancia_poliza.modificado_por,
                                            // createdAt: new_atributo_instancia_poliza.createdAt,
                                            // updatedAt: new_atributo_instancia_poliza.updatedAt
                                        };
                                })
                                .catch((err) => {
                                    response.status = "ERROR";
                                    response.message = err;
                                    loggerCatch.info(new Date(), err);
                                });
                        }
                    }
                }
                let old_instancia_poliza_transicions = await modelos.instancia_poliza_transicion.findAll({
                    where: { id_instancia_poliza: new_instancia_poliza.id },
                });
                await asyncForEach(
                    new_instancia_poliza.instancia_poliza_transicions,
                    async (instancia_poliza_transicion, index) => {
                        if (instancia_poliza_transicion.par_estado_id == new_instancia_poliza.id_estado) {
                            let soloObservacion = instancia_poliza_transicion.observacion.split("La persona").join("").split("El titular").join("").trim();
                            let oldInstanciaPolizaTransicion = old_instancia_poliza_transicions.find((param) => param.dataValues.observacion.includes(soloObservacion));
                            if (instancia_poliza_transicion.id) {
                                await updateTransicion(instancia_poliza_transicion, usuario);
                            }
                            if ( oldInstanciaPolizaTransicion && oldInstanciaPolizaTransicion.id) {
                                instancia_poliza_transicion.id = oldInstanciaPolizaTransicion.id;
                                await updateTransicion(instancia_poliza_transicion, usuario);
                            } else {
                                await createTransicion(instancia_poliza_transicion, usuario);
                            }
                        }
                    }
                );
                new_asegurado.instancia_poliza.instancia_poliza_transicions = await modelos.instancia_poliza_transicion.findAll({
                    where: { id_instancia_poliza: new_instancia_poliza.id },
                }).then(async (resp) => {
                    return resp;
                }).catch((err) => {
                    response.status = "ERROR";
                    response.message = err;
                    loggerCatch.info(new Date(), err);
                });
                response.data = new_asegurado;
                res.json(response);
            }
        );
    } catch (e) {
        loggerCatch.info(new Date(), e);
        console.log(e);
    }
});

router.post("/CrearNuevaSolicitud", isLoggedIn, async (req, res) => {
    try {
        let response = { status: "OK", message: "", data: {} };
        let asegurado;
        let hoy = new Date();
        let createdAt = new Date();
        let id_usuario = req.session.passport.user;
        let usuario = await modelos.usuario.findOne({
            where: { id: id_usuario },
            include: {
                model: modelos.usuario_x_rol,
                include: {
                    model: modelos.rol,
                },
            },
        })
            .catch((err) => {
                response.status = "ERROR";
                response.message = err;
                loggerCatch.info(new Date(), err);
            });
        let post = req.body;
        let new_asegurado = post;
        let new_instancia_poliza = new_asegurado.instancia_poliza;
        let new_instancia_documentos = new_asegurado.instancia_poliza.instancia_documentos.length ? new_asegurado.instancia_poliza.instancia_documentos : [];
        let estado = new_asegurado.instancia_poliza.estado;
        let condicion = new_asegurado.instancia_poliza.condicion;
        let poliza = new_asegurado.instancia_poliza.poliza;
        let anexoPoliza = await modelos.anexo_poliza.findOne({where:{id_poliza:poliza.id, id_tipo:288, vigente:true}});
        let atrPlazoCredito;
        atrPlazoCredito = new_asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == 82);
        atrPlazoCredito = atrPlazoCredito ? atrPlazoCredito : new_asegurado.instancia_poliza.atributo_instancia_polizas_inter.find(param => param.objeto_x_atributo.id_atributo == 82);
        let fechaFinVigencia;
        for (let i = 0; i < new_instancia_documentos.length; i++) {
            let newInstanciaDocumento = new_instancia_documentos[i];
            newInstanciaDocumento.fecha_emision = newInstanciaDocumento.fecha_emision ? moment(newInstanciaDocumento.fecha_emision).toDate() : moment(new Date()).toDate();
            newInstanciaDocumento.fecha_inicio_vigencia = newInstanciaDocumento.fecha_inicio_vigencia ? moment(newInstanciaDocumento.fecha_inicio_vigencia).toDate() : moment(new Date()).toDate();
            newInstanciaDocumento.fecha_fin_vigencia = fechaFinVigencia = atrPlazoCredito ? moment(new Date()).add(parseInt(atrPlazoCredito.valor+'') > 60 ? 60 : parseInt(atrPlazoCredito.valor+''), 'months').toDate() : moment(new Date()).add(poliza.vigencia_meses,'months');
        }

        await saveOrUpdateEntidadPersona(
            new_asegurado.entidad,
            usuario,
            async (resp) => {
                let entidad = resp && resp.data ? resp.data : null;
                new_instancia_poliza.id = null;
                new_instancia_poliza.id_poliza = poliza.id;
                new_instancia_poliza.id_estado = estado.id;
                new_instancia_poliza.fecha_registro = createdAt;
                new_instancia_poliza.adicionada_por = usuario ? usuario.id : "2";
                new_instancia_poliza.modificada_por = usuario ? usuario.id : "2";
                new_instancia_poliza.createdAt = createdAt;
                new_instancia_poliza.updatedAt = createdAt;
                new_instancia_poliza.id_anexo_poliza = anexoPoliza.id;
                delete new_instancia_poliza.id_instancia_renovada;
                await modelos.instancia_poliza.create(new_instancia_poliza).then(async (resp) => {
                        let instancia_poliza = resp.dataValues;
                        let respInstanciaUpdated = await modelos.instancia_poliza.update(
                            { id_instancia_renovada: instancia_poliza.id },
                            { where: { id: instancia_poliza.id } }
                        );
                        instancia_poliza.id_instancia_renovada = instancia_poliza.id;
                        instancia_poliza.instancia_poliza_transicions = [];
                        for (let i = 0; i < new_asegurado.instancia_poliza.instancia_poliza_transicions.length; i++) {
                            let instancia_poliza_transicion = new_asegurado.instancia_poliza.instancia_poliza_transicions[i];
                            instancia_poliza_transicion.id_instancia_poliza = instancia_poliza.id;
                            instancia_poliza.instancia_poliza_transicions[i] = await createTransicion(instancia_poliza_transicion, usuario);
                        }
                        new_asegurado.id = null;
                        new_asegurado.id_entidad = entidad.id;
                        new_asegurado.id_instancia_poliza = instancia_poliza.id;
                        new_asegurado.adicionado_por = usuario ? usuario.id : "2";
                        new_asegurado.modificado_por = usuario ? usuario.id : "2";
                        new_asegurado.createdAt = createdAt;
                        new_asegurado.updatedAt = createdAt;
                        await modelos.asegurado.create(new_asegurado).then(async (resp) => {
                                asegurado = resp.dataValues;
                                asegurado.entidad = entidad;
                                asegurado.instancia_poliza = instancia_poliza;
                                asegurado.instancia_poliza.poliza = poliza;
                                asegurado.instancia_poliza.estado = estado;
                                asegurado.instancia_poliza.condicion = condicion;
                                asegurado.beneficiarios = [];
                                asegurado.instancia_poliza.atributo_instancia_polizas = [];
                                let idAtributos = [64, 65, 66, 67, 69];
                                new_instancia_poliza.atributo_instancia_polizas = new_instancia_poliza.atributo_instancia_polizas.filter( (param) => !idAtributos.includes(parseInt(param.objeto_x_atributo.id_atributo)));
                                for ( let i = 0; i < new_instancia_poliza.atributo_instancia_polizas.length; i++ ) {
                                    let new_atributo_instancia_poliza = new_instancia_poliza.atributo_instancia_polizas[i];
                                    new_atributo_instancia_poliza.id = null;
                                    new_atributo_instancia_poliza.id_instancia_poliza = instancia_poliza.id;
                                    new_atributo_instancia_poliza.adicionado_por = usuario ? usuario.id : "2";
                                    new_atributo_instancia_poliza.modificado_por = usuario ? usuario.id : "2";
                                    new_atributo_instancia_poliza.createdAt = createdAt;
                                    new_atributo_instancia_poliza.updatedAt = createdAt;
                                    new_atributo_instancia_poliza.valor = new_atributo_instancia_poliza.valor ? new_atributo_instancia_poliza.valor : "";
                                    await modelos.atributo_instancia_poliza .create(new_atributo_instancia_poliza)
                                        .then(async (resp) => {
                                            let atributo_instancia_poliza = resp.dataValues;
                                            // atributo_instancia_poliza.instancia_poliza = new_instancia_poliza;
                                            await asegurado.instancia_poliza.atributo_instancia_polizas.push(
                                                {
                                                    id: atributo_instancia_poliza.id,
                                                    id_instancia_poliza: atributo_instancia_poliza.id_instancia_poliza,
                                                    id_objeto_x_atributo: atributo_instancia_poliza.id_objeto_x_atributo,
                                                    valor: atributo_instancia_poliza.valor,
                                                    tipo_error: atributo_instancia_poliza.tipo_error,
                                                    objeto_x_atributo: new_atributo_instancia_poliza.objeto_x_atributo,
                                                    // adicionado_por: new_atributo_instancia_poliza.adicionado_por,
                                                    // modificado_por: new_atributo_instancia_poliza.modificado_por,
                                                    // createdAt: new_atributo_instancia_poliza.createdAt,
                                                    // updatedAt: new_atributo_instancia_poliza.updatedAt
                                                }
                                            );
                                        })
                                        .catch((err) => {
                                            response.status = "ERROR";
                                            response.message = err;
                                            loggerCatch.info(new Date(), err);
                                        });
                                    asegurado.instancia_poliza.atributo_instancia_polizas_inter = new_instancia_poliza.atributo_instancia_polizas_inter;
                                }
                                asegurado.instancia_poliza.instancia_documentos = [];

                                for (let i = 0; i < new_instancia_documentos.length; i++) {
                                    let new_instancia_documento = new_instancia_documentos[i];
                                    let documento = new_instancia_documento.documento;
                                    if (documento != null) {
                                        try {
                                            await modelos.documento
                                                .findOne({ where: { id: documento.id } })
                                                .then(async (resp) => {
                                                    documento = resp.dataValues;
                                                })
                                                .catch((err) => {
                                                    response.status = "ERROR";
                                                    response.message = err;
                                                    loggerCatch.info(new Date(), err);
                                                });
                                            new_instancia_documento.id = null;
                                            new_instancia_documento.id_instancia_poliza = instancia_poliza.id;
                                            new_instancia_documento.id_documento = documento.id;
                                            new_instancia_documento.id_documento_version = documento.id_documento_version;
                                            new_instancia_documento.nro_documento = documento.nro_actual;
                                            new_instancia_documento.fecha_emision = createdAt;
                                            new_instancia_documento.fecha_inicio_vigencia = createdAt;
                                            new_instancia_documento.fecha_fin_vigencia = fechaFinVigencia;
                                            new_instancia_documento.asegurado_doc_id = entidad.persona.persona_doc_id;
                                            new_instancia_documento.asegurado_doc_id_ext = entidad.persona.persona_doc_id_ext;
                                            new_instancia_documento.asegurado_nombre1 = entidad.persona.persona_primer_nombre;
                                            new_instancia_documento.asegurado_nombre2 = entidad.persona.persona_segundo_nombre;
                                            new_instancia_documento.asegurado_apellido1 = entidad.persona.persona_primer_apellido;
                                            new_instancia_documento.asegurado_apellido2 = entidad.persona.persona_segundo_apellido;
                                            new_instancia_documento.tipo_persona = entidad.id;
                                            new_instancia_documento.adicionada_por = usuario != null ? usuario.id : "2";
                                            new_instancia_documento.modificada_por = usuario != null ? usuario.id : "2";
                                            new_instancia_documento.createdAt = createdAt;
                                            new_instancia_documento.updatedAt = createdAt;
                                            documento.nro_actual++;
                                            documento.modificado_por = usuario != null ? usuario.id : "2";
                                            documento.updatedAt = createdAt;
                                            await modelos.documento.update(documento, { where: { id: documento.id } })
                                                .then(async (resp) => {
                                                    if (resp.length && resp[0]) {
                                                        await modelos.instancia_documento.create(new_instancia_documento)
                                                            .then(async (resp) => {
                                                                let instancia_documento = resp.dataValues;
                                                                instancia_documento.instancia_poliza = new_instancia_documento.instancia_poliza;
                                                                instancia_documento.documento = new_instancia_documento.documento;
                                                                instancia_documento.atributo_instancia_documentos = [];
                                                                for ( let j = 0; j < new_instancia_documento.atributo_instancia_documentos.length; j++ ) {
                                                                    let new_atributo_instancia_documento = new_instancia_documento.atributo_instancia_documentos[j];
                                                                    new_atributo_instancia_documento.id = null;
                                                                    new_atributo_instancia_documento.id_instancia_documento = instancia_documento.id;
                                                                    new_atributo_instancia_documento.id_objeto_x_atributo = new_atributo_instancia_documento.id_objeto_x_atributo
                                                                    new_atributo_instancia_documento.tipo_error = new_atributo_instancia_documento.tipo_error
                                                                    new_atributo_instancia_documento.adicionado_por = usuario != null ? usuario.id : "2";
                                                                    new_atributo_instancia_documento.modificado_por = usuario != null ? usuario.id : "2";
                                                                    new_atributo_instancia_documento.createdAt = createdAt;
                                                                    new_atributo_instancia_documento.updatedAt = createdAt;
                                                                    await modelos.atributo_instancia_documento.create(new_atributo_instancia_documento)
                                                                        .then(async (resp) => {
                                                                            let atributo_instancia_documento = resp.dataValues;
                                                                            // atributo_instancia_documento.instancia_documento = new_instancia_documento;
                                                                            await instancia_documento.atributo_instancia_documentos.push(
                                                                                {
                                                                                    id: atributo_instancia_documento.id,
                                                                                    id_instancia_documento: atributo_instancia_documento.id_instancia_documento,
                                                                                    id_objeto_x_atributo: atributo_instancia_documento.id_objeto_x_atributo,
                                                                                    valor: atributo_instancia_documento.valor,
                                                                                    tipo_error: atributo_instancia_documento.tipo_error,
                                                                                    objeto_x_atributo: new_atributo_instancia_documento.objeto_x_atributo,
                                                                                }
                                                                            );
                                                                        })
                                                                        .catch((err) => {
                                                                            response.status = "ERROR";
                                                                            response.message = err;
                                                                            loggerCatch.info(new Date(), err);
                                                                        });
                                                                }
                                                                instancia_documento.atributo_instancia_documentos_inter = new_instancia_documento.atributo_instancia_documentos_inter;
                                                                asegurado.instancia_poliza.instancia_documentos.push(
                                                                    {
                                                                        id: instancia_documento.id,
                                                                        id_instancia_poliza: instancia_documento.id_instancia_poliza,
                                                                        id_documento: instancia_documento.id_documento,
                                                                        id_documento_version: instancia_documento.id_documento_version,
                                                                        nro_documento: instancia_documento.nro_documento,
                                                                        fecha_emision: instancia_documento.fecha_emision,
                                                                        fecha_inicio_vigencia: instancia_documento.fecha_inicio_vigencia,
                                                                        fecha_fin_vigencia: instancia_documento.fecha_fin_vigencia,
                                                                        asegurado_doc_id: instancia_documento.asegurado_doc_id,
                                                                        asegurado_nombre1: instancia_documento.asegurado_nombre1,
                                                                        asegurado_nombre2: instancia_documento.asegurado_nombre2,
                                                                        asegurado_apellido1: instancia_documento.asegurado_apellido1,
                                                                        asegurado_apellido2: instancia_documento.asegurado_apellido2,
                                                                        asegurado_apellido3: instancia_documento.asegurado_apellido3,
                                                                        tipo_persona: instancia_documento.tipo_persona,
                                                                        adicionada_por: instancia_documento.adicionada_por,
                                                                        modificada_por: instancia_documento.modificada_por,
                                                                        createdAt: instancia_documento.createdAt,
                                                                        updatedAt: instancia_documento.updatedAt,
                                                                    }
                                                                );
                                                            })
                                                            .catch((err) => {
                                                                response.status = "ERROR";
                                                                response.message = err;
                                                                loggerCatch.info(new Date(), err);
                                                            });
                                                    }
                                                })
                                                .catch((err) => {
                                                    response.status = "ERROR";
                                                    response.message = err;
                                                    loggerCatch.info(new Date(), err);
                                                });
                                            asegurado.instancia_poliza.usuario = usuario;
                                            return asegurado;
                                        } catch (err) {
                                            response.status = "ERROR";
                                            response.message = err;
                                            loggerCatch.info(new Date(), err);
                                        }
                                    }
                                }
                            })
                            .catch((err) => {
                                response.status = "ERROR";
                                response.message = err;
                                loggerCatch.info(new Date(), err);
                            });
                    })
                    .catch((err) => {
                        response.status = "ERROR";
                        response.message = err;
                        loggerCatch.info(new Date(), err);
                    });
                if (asegurado) {
                    response.data = asegurado;
                }
                res.json(response);
            }
        );
    } catch (e) {
        console.log(e);
        loggerCatch.info(new Date(), err);
    }
});

router.post("/AdicionarPersonaBeneficiario", async (req, res) => {
    let personaBeneficiario = req.body.personaCompleto;
    if (personaBeneficiario.par_tipo_documento_id == "") {
        personaBeneficiario.par_tipo_documento_id = null;
    }
    if (personaBeneficiario.par_nacionalidad_id == "") {
        personaBeneficiario.par_nacionalidad_id = null;
    }
    if (personaBeneficiario.par_pais_nacimiento_id == "") {
        personaBeneficiario.par_pais_nacimiento_id = null;
    }
    let fecha_declaracion = new Date();
    // fecha_declaracion = Date.parse(hoyBeneficiario.getFullYear() + '/' + (hoyBeneficiario.getMonth() + 1) + '/' + hoyBeneficiario.getDate() + ' ' + hoyBeneficiario.getHours() + ':' + hoyBeneficiario.getMinutes() + ':' + hoyBeneficiario.getSeconds());
    let response = { status: "OK", message: "", data: "" };
    // if ((personaBeneficiario.persona_doc_id == null) && (personaBeneficiario.persona_doc_id_ext == null)) {
    if (!req.body.displayBeneficiarioFound) {
        if (req.body.nuevoRegistroConCI == "0") {
            personaBeneficiario.persona_doc_id = null;
            personaBeneficiario.persona_doc_id_ext = null;
        }

        personaBeneficiario.persona_celular = req.body.persona_celular
            ? req.body.persona_celular.toString()
            : null;
        await modelos.persona
            .create(personaBeneficiario)
            .then(async (persona1) => {
                const id_persona = parseInt(persona1.id, 10);
                const tipo_entidad = 12;
                await modelos.entidad
                    .create({ id_persona, tipo_entidad })
                    .then(async (entidad) => {
                        const id_beneficiario = parseInt(entidad.id, 10);
                        const id_asegurado = req.body.id_asegurado;
                        const id_parentesco = req.body.id_parentesco;
                        const porcentaje = req.body.porcentaje;
                        const tipo = req.body.tipo;
                        const estado = req.body.estado;
                        const condicion = req.body.condicion;
                        var descripcion_otro = "";
                        if (id_parentesco == 56) {
                            descripcion_otro = req.body.descripcion_otro;
                        } else {
                            descripcion_otro = "";
                        }
                        await modelos.beneficiario
                            .create({
                                id_asegurado,
                                id_beneficiario,
                                id_parentesco,
                                descripcion_otro,
                                porcentaje,
                                tipo,
                                estado,
                                condicion,
                                fecha_declaracion,
                            })
                            .then(async (beneficiario) => {
                                response.data = beneficiario;
                                console.log(
                                    "Beneficiario creado...==>>",
                                    beneficiario.dataValues
                                );
                            })
                            .catch((err) => {
                                response.status = "ERROR";
                                response.message = err;
                                loggerCatch.info(new Date(), err);
                            });
                    })
                    .catch((err) => {
                        response.status = "ERROR";
                        response.message = err;
                        loggerCatch.info(new Date(), err);
                        console.log(response.message);
                    });
            })
            .catch((err) => {
                response.status = "ERROR";
                response.message = err;
                loggerCatch.info(new Date(), err);
                console.log(response.message);
            });
        res.json(response);
    } else {
        const persona_doc_id = personaBeneficiario.persona_doc_id;
        const persona_doc_id_ext = personaBeneficiario.persona_doc_id_ext;
        await modelos.persona
            .findOne({ where: { persona_doc_id, persona_doc_id_ext } })
            .then(async (persona) => {
                if (persona != null) {
                    const id_persona = persona.id;
                    if (personaBeneficiario.persona_celular) {
                        const persona_celular = personaBeneficiario.persona_celular;
                        persona.update({ where: { persona_celular } });
                    }
                    await modelos.entidad
                        .findOne({ where: { id_persona } })
                        .then(async (entidad) => {
                            const id_beneficiario = parseInt(entidad.id, 10);
                            const id_asegurado = req.body.id_asegurado;
                            const id_parentesco = req.body.id_parentesco;
                            const porcentaje = req.body.porcentaje;
                            // const persona_celular = req.body.persona_celular;
                            const tipo = req.body.tipo;
                            const estado = req.body.estado;
                            const condicion = req.body.condicion;
                            var descripcion_otro = "";
                            if (id_parentesco == 56) {
                                descripcion_otro = req.body.descripcion_otro;
                            } else {
                                descripcion_otro = "";
                            }
                            await modelos.beneficiario
                                .create({
                                    id_asegurado,
                                    id_beneficiario,
                                    id_parentesco,
                                    descripcion_otro,
                                    porcentaje,
                                    tipo,
                                    estado,
                                    condicion,
                                    fecha_declaracion,
                                })
                                .then(async (beneficiario) => {
                                    response.data = beneficiario;
                                    console.log(
                                        "Beneficiario creado...==>>",
                                        beneficiario.dataValues
                                    );
                                })
                                .catch((err) => {
                                    response.status = "ERROR";
                                    response.message = err;
                                    loggerCatch.info(new Date(), err);
                                });
                        })
                        .catch((err) => {
                            response.status = "ERROR";
                            response.message = err;
                            loggerCatch.info(new Date(), err);
                        });
                    res.json(response);
                } else {
                    personaBeneficiario.persona_celular = req.body.persona_celular
                        ? req.body.persona_celular.toString()
                        : null;
                    await modelos.persona
                        .create(personaBeneficiario)
                        .then(async (persona1) => {
                            const id_persona = parseInt(persona1.id, 10);
                            const tipo_entidad = 12;
                            await modelos.entidad
                                .create({ id_persona, tipo_entidad })
                                .then(async (entidad) => {
                                    const id_beneficiario = parseInt(entidad.id, 10);
                                    const id_asegurado = req.body.id_asegurado;
                                    const id_parentesco = req.body.id_parentesco;
                                    const porcentaje = req.body.porcentaje;
                                    const tipo = req.body.tipo;
                                    const estado = req.body.estado;
                                    const condicion = req.body.condicion;
                                    var descripcion_otro = "";
                                    if (id_parentesco == 56) {
                                        descripcion_otro = req.body.descripcion_otro;
                                    } else {
                                        descripcion_otro = "";
                                    }
                                    await modelos.beneficiario
                                        .create({
                                            id_asegurado,
                                            id_beneficiario,
                                            id_parentesco,
                                            descripcion_otro,
                                            porcentaje,
                                            tipo,
                                            estado,
                                            condicion,
                                            fecha_declaracion,
                                        })
                                        .then(async (beneficiario) => {
                                            response.data = beneficiario;
                                            console.log(
                                                "Beneficiario creado...==>>",
                                                beneficiario.dataValues
                                            );
                                        })
                                        .catch((err) => {
                                            response.status = "ERROR";
                                            response.message = err;
                                            loggerCatch.info(new Date(), err);
                                        });
                                })
                                .catch((err) => {
                                    response.status = "ERROR";
                                    response.message = err;
                                    loggerCatch.info(new Date(), err);
                                    console.log(response.message);
                                });
                        })
                        .catch((err) => {
                            response.status = "ERROR";
                            response.message = err;
                            loggerCatch.info(new Date(), err);
                            console.log(response.message);
                        });
                    res.json(response);
                }
            })
            .catch((err) => {
                response.status = "ERROR";
                response.message = err;
                loggerCatch.info(new Date(), err);
            });
        // res.json(response);
    }
});

router.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/");
});

module.exports = router;
