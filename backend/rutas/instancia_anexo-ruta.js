const { Router } = require('express');
const router = new Router();
const Util = require("../utils/util");
const util = new Util();
const InstanciaAnexoService = require("../servicios/instanciaAnexo.service");

router.get('/listarInstanciaAnexosByIdAsegurado/:id_asegurado', async (req, res) => {
    try {
        let id_asegurado = req.params.id_asegurado;
        let objData = await InstanciaAnexoService.listarInstanciaAnexosByIdAsegurado(id_asegurado);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/byid/:id', async (req, res) => {
    try {
        let id = req.params.id;
        let objData = await InstanciaAnexoService.byid(id);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/guardar_usuarioxrol', async (req, res) => {
    try {
        let id_usuario = req.body.id_usuario;
        let id_rol = req.body.id_rol;
        let objData = await InstanciaAnexoService.guardar_usuarioxrol(id_usuario,id_rol);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/ResgistrarEmparejamiento', async (req, res) => {
    try {
        let usuarios = req.body;
        let objData = await InstanciaAnexoService.ResgistrarEmparejamiento(usuarios);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/eliminarRol_x_Perfil', async (req, res) => {
    try {
        let id_usuario = req.body.id_usuario;
        let id_rol = req.body.id_rol;
        let objData = await InstanciaAnexoService.eliminarRol_x_Perfil(id_usuario,id_rol);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;