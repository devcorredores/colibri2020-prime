const { Router } = require('express');
const router = new Router();
const FileService = require('../servicios/file.service');
const modelos = require('../modelos');
const util = require('../utils/util');
const {loggerCatch} = require("../modulos/winston");

router.get('/upload', async (req, res) => {
  try {
  	  let response = await FileService.upload(req, res);
      res.json(response);
  } catch (e) {
    console.log(e);
    loggerCatch.info(new Date(),e);
  }
});

module.exports = router;
