const { Router } = require('express');
const router = new Router();
const modelos = require("../modelos");
const autentificacion = require('../modulos/passport');
const funciones = require('../modulos/funciones');
const funciones_archivo = require('../modulos/funciones_archivo');
const funciones_plan_pago = require('../modulos/funciones_plan_pago');
const PlanPagoController = require('../controladores/plan_pago.controller');

require('datejs');

var isLoggedIn = (req, res, next) => {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
}

router.get('/', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.poliza.findAll()
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/GenerarPlanPagos', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const v_pp = req.body;
    // const id_solicitud = idSolicitud;

    let v_pp_g = {};
    let datos;
    //OBTENER DATOS SOLICITUD
    response=await funciones_plan_pago.GenerarPlanPagos(v_pp);
    //OBTENER DATOS DEUDOR

    //response = await generaPdf.generarReporteCertificadoEcoVida(datos, nombre_archivo);

    res.json(response);
});

router.post('/GenerarPlanPagosMigrados', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const v_pp = req.body;
    // const id_solicitud = idSolicitud;

    let v_pp_g = {};
    let datos;
    //OBTENER DATOS SOLICITUD
    await modelos.planPago.create(v_pp)
        .then(async resp => {
            if (resp) {
                datos = resp.dataValues;
                let v_ppd={};
                v_ppd.id_instancia_poliza=parseInt(v_pp.id_instancia_poliza);
                v_ppd.id_planpago=parseInt(datos.id);
                v_ppd.pago_cuota_prog=datos.total_prima/(datos.periodicidad_anual*datos.plazo_anos);
                v_ppd.adicionado_por=datos.adicionado_por;
                v_ppd.modificado_por=datos.modificado_por;
                for(var i=0;i<(datos.periodicidad_anual*datos.plazo_anos);i++){
                    v_ppd.nro_cuota_prog=(i+1);
                    let fecha=new Date(datos.fecha_inicio);
                    var ultimoDia = new Date(fecha.getFullYear(), fecha.getMonth() + 1, 0);
                    ultimoDia.setDate(ultimoDia.getDate()-2);
                    fecha = fecha.add(i).month();
                    v_ppd.fecha_couta_prog=funciones.formatoFechaBarrasMedia(fecha)+'T00:00:00.000Z';
                    console.log(v_ppd.fecha_couta_prog);
                    v_ppd.interes_prog=(datos.total_prima-(v_ppd.pago_cuota_prog*i))*datos.interes;
                    v_ppd.prima_prog=(v_ppd.pago_cuota_prog + v_ppd.interes_prog);
                    v_ppd.pago_prima_acumulado=v_ppd.pago_cuota_prog*(i+1);
                    v_ppd.prima_pendiente=datos.total_prima-v_ppd.pago_prima_acumulado;
                    v_ppd.id_estado=253;
                    await modelos.plan_pago_detalle.create(v_ppd)
                    .then(resp2 => {
                        response.data = resp2;
                        response.message = 'Registro creado';
                    })
                    .catch((err) => {
                        response.status = 'ERROR';
                        response.message = err;
                    });
                }

            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    //response = await generaPdf.generarReporteCertificadoEcoVida(datos, nombre_archivo);

    res.json(response);
});


router.get('/listaTodosSinPlanPagos', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.sequelize.query(`select A.id,B.valor plazo,C.valor monto,D.fecha_emision from instancia_poliza A
    inner join  atributo_instancia_poliza B on B.id_objeto_x_atributo=236 and A.id=B.id_instancia_poliza
    inner join atributo_instancia_poliza C on C.id_objeto_x_atributo=237 and A.id=C.id_instancia_poliza
    inner join instancia_documento D on D.id_documento=21 and D.id_instancia_poliza=A.id and A.id_estado=59 and cast(D.nro_documento as Integer)>=90000
     and A.id_poliza=7 and A.id not in (select distinct id_instancia_poliza from plan_pago) `)
        .then(result => {
            response.data = result[0];
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});


router.get('/listaTodosSinPlanPagosMigrados', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.sequelize.query(`select A.id,B.valor plazo,C.valor monto,D.fecha_emision from instancia_poliza A
    inner join  atributo_instancia_poliza B on B.id_objeto_x_atributo=236 and A.id=B.id_instancia_poliza
    inner join atributo_instancia_poliza C on C.id_objeto_x_atributo=237 and A.id=C.id_instancia_poliza
    inner join instancia_documento D on D.id_documento=21 and D.id_instancia_poliza=A.id and cast(D.nro_documento as Integer)<90000
     and A.id_poliza=7 and A.id not in (select distinct id_instancia_poliza from plan_pago) `)
        .then(result => {
            response.data = result[0];
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/GenerarArchivo', async (req, res) => {
    PlanPagoController.setPlanPagoFile(req.body, res);
});

router.post('/GenerarPagos', async (req, res) => {
    let id_poliza = req.body.id_poliza;
    let fecha = req.body.fecha;
    fecha= new Date(fecha);
    fecha= fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate();
    let response = { status: 'OK', message: '', data: '' };
    await modelos.sequelize.query(`
    select TB.* from (
    select (nro_carga+1) num_carga,D.valor cod_agenda,H.valor cuenta,'1' moneda,C.codigo_producto,L.nro_documento nro_certificado,ROW_NUMBER() OVER(ORDER BY D.valor DESC) correlativo,B.id glosa,getdate() fecha_envio,
        M.nombre,N.fecha,P3.persona_primer_apellido,P3.persona_segundo_apellido,P3.persona_primer_nombre,P3.persona_segundo_nombre,
        B.nro_cuota_prog,format(B.fecha_couta_prog,'dd/MM/yyyy') fecha_couta_prog,N.id_estado,B.pago_cuota_prog,BP.parametro_descripcion,B.id id_pago
        from instancia_poliza A
        inner join asegurado P1 on P1.id_instancia_poliza=A.id
        inner join entidad P2 on P1.id_entidad=P2.id
        inner join persona P3 on P2.id_persona=P3.id
        inner join plan_pago_detalle B on A.id=B.id_instancia_poliza
        and B.nro_cuota_prog=(select isnull(max(nro_cuota_prog),0)+1 from plan_pago_detalle where id_instancia_poliza=A.id and id_estado=254)
        inner join parametro BP on B.id_estado=BP.id  
        inner join poliza C on A.id_poliza=C.id  
        inner join atributo_instancia_poliza D on A.id=D.id_instancia_poliza   
        inner join objeto_x_atributo E on E.id=D.id_objeto_x_atributo  
        inner join objeto F on E.id_objeto=F.id  and F.id_poliza=${id_poliza}
        inner join atributo G on  E.id_atributo=G.id and G.id=10  
        inner join atributo_instancia_poliza H on A.id=H.id_instancia_poliza   
        inner join objeto_x_atributo I on I.id=H.id_objeto_x_atributo  
        inner join objeto J on I.id_objeto=J.id  and J.id_poliza=${id_poliza}  
        inner join atributo K on  I.id_atributo=K.id and K.id=5  
        inner join instancia_documento L on A.id=L.id_instancia_poliza  and A.id_estado=59 and L.id_documento in (3,6,9,12,15,18,21,24) and convert(numeric,isnull(L.nro_documento,0))>=90000
        and B.id_estado=253 and B.fecha_couta_prog between dateadd(DAY,-C.dias_gracia,convert(datetime,'${fecha}')) and convert(datetime,'${fecha}')
        left join instancia_archivo M on B.id_instancia_archivo=M.id
        left join envio N on N.id_instancia_archivo=M.id
        ) TB where (TB.nombre is not null and TB.fecha is not null and Tb.id_estado=264) or (TB.nombre is null and TB.fecha is null)`).then(result => {
            response.data = result[0];
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/GenerarPagosProcedimiento', async (req, res) => {
    PlanPagoController.setPlanPagoProcedure(req.body, res);
});

router.post('/GetPlanPagoByNroCertAndIdPoliza', async (req, res) => {
    let id_poliza = req.body.id_poliza;
    let nro_certificado = req.body.nro_certificado;
    let response = { status: 'OK', message: '', data: '' };
    await modelos.sequelize.query(`
    select J.parametro_descripcion estado_instancia, format(DATEADD(day,A6.dias_gracia,A.fecha_couta_prog),'dd/MM/yyyy')  fecha_limite, format(A.nro_cuota_prog,'00') nro_cuota_prog,format(A.fecha_couta_prog,'dd/MM/yyyy') fecha_couta_prog,A.pago_cuota_prog,A.pago_prima_acumulado,A.prima_pendiente,isnull(format(A.FechaPagoEjec,'dd/MM/yyyy'),'') FechaPagoEjec,D.parametro_descripcion estado,E.parametro_descripcion  estado_det
,A1.total_prima,H.parametro_descripcion moneda,A1.interes,A1.plazo_anos,A1.periodicidad_anual,A1.prepagable_postpagable,format(A1.fecha_inicio,'dd/MM/yyyy') fecha_inicio
,isnull(A4.persona_primer_apellido,'')+' '+isnull(A4.persona_segundo_apellido,'')+' '+isnull(A4.persona_primer_nombre,'')+' '+isnull(A4.persona_segundo_nombre,'') asegurado
,B.nro_documento nro_certificado,format(B.fecha_emision,'dd/MM/yyyy') fecha_emision,format(B.fecha_inicio_vigencia,'dd/MM/yyyy') fecha_inicio_vigencia,format(B.fecha_fin_vigencia,'dd/MM/yyyy') fecha_fin_vigencia,I.parametro_descripcion estado_plan,A6.descripcion poliza
from plan_pago_detalle A
 inner join plan_pago A1 on A.id_instancia_poliza=A1.id_instancia_poliza
 inner join asegurado A2 on A.id_instancia_poliza=A2.id_instancia_poliza
 inner join entidad A3 on A2.id_entidad=A3.id
 inner join persona A4 on A4.id=A3.id_persona
 inner join instancia_poliza A5 on A.id_instancia_poliza=A5.id
 inner join poliza A6 on A5.id_poliza=A6.id
 inner join instancia_documento B on A.id_instancia_poliza=B.id_instancia_poliza
 inner join documento C on B.id_documento=C.id and C.id_poliza=${id_poliza} and C.id in (3,6,9,12,15,18,21,24) and B.nro_documento='${nro_certificado}'
 left join parametro D on A.id_estado=D.id
 left join parametro E on A.id_estado_det=E.id 
 left join archivo F on A.id_instancia_archivo=F.id
 left join envio G on F.id=G.id_instancia_archivo
 left join parametro H on H.id=A1.id_moneda and H.diccionario_id = 22
 left join parametro I on A1.id_estado=I.id
 left join parametro J on J.id=A5.id_estado and J.diccionario_id = 11
 order by A.nro_cuota_prog 
    `).then(result => {
            response.data = result[0];
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/GetPlanPagoByNroCertAndIdPolizaAndInstanciaPoliza', async (req, res) => {
    let nro_certificado = req.body.nro_certificado;
    let id_instancia_poliza = req.body.id_instancia_poliza;
    let response = { status: 'OK', message: '', data: '' };
    await modelos.sequelize.query(`
    select 
    J.parametro_descripcion estado_instancia
    , format(DATEADD(day,A6.dias_gracia,A.fecha_couta_prog),'dd/MM/yyyy')  fecha_limite
    , format(A.nro_cuota_prog,'00') nro_cuota_prog
    ,format(A.fecha_couta_prog,'dd/MM/yyyy') fecha_couta_prog
    ,A.pago_cuota_prog
    ,A.pago_prima_acumulado
    ,A.prima_pendiente
    ,isnull(format(A.FechaPagoEjec,'dd/MM/yyyy'),'') FechaPagoEjec
    ,D.parametro_descripcion estado
    ,E.parametro_descripcion  estado_det
,A1.total_prima
,H.parametro_descripcion moneda
,A1.interes
,A1.plazo_anos
,A1.periodicidad_anual
,A1.prepagable_postpagable
,format(A1.fecha_inicio,'dd/MM/yyyy') fecha_inicio
,isnull(A4.persona_primer_apellido,'')+' '+isnull(A4.persona_segundo_apellido,'')+' '+isnull(A4.persona_primer_nombre,'')+' '+isnull(A4.persona_segundo_nombre,'') asegurado
,B.nro_documento nro_certificado
,format(B.fecha_emision,'dd/MM/yyyy') fecha_emision
,format(B.fecha_inicio_vigencia,'dd/MM/yyyy') fecha_inicio_vigencia
,format(B.fecha_fin_vigencia,'dd/MM/yyyy') fecha_fin_vigencia
,I.parametro_descripcion estado_plan
,A6.descripcion poliza
from plan_pago_detalle A
 inner join plan_pago A1 on A.id_instancia_poliza=A1.id_instancia_poliza
 inner join asegurado A2 on A.id_instancia_poliza=A2.id_instancia_poliza
 inner join entidad A3 on A2.id_entidad=A3.id
 inner join persona A4 on A4.id=A3.id_persona
 inner join instancia_poliza A5 on A.id_instancia_poliza = A5.id and A5.id = ${id_instancia_poliza} 
 inner join poliza A6 on A5.id_poliza=A6.id
 inner join instancia_documento B on A.id_instancia_poliza = B.id_instancia_poliza
 inner join documento C on B.id_documento = C.id and C.id_tipo_documento = 281
 left join parametro D on A.id_estado=D.id
 left join parametro E on A.id_estado_det=E.id 
 left join archivo F on A.id_instancia_archivo=F.id
 left join envio G on F.id=G.id_instancia_archivo
 left join parametro H on H.id=A1.id_moneda and H.diccionario_id = 22
 left join parametro I on A1.id_estado=I.id
 left join parametro J on J.id=A5.id_estado and J.diccionario_id = 11
 order by A.nro_cuota_prog 
    `).then(result => {
            response.data = result[0];
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/GetPendientesByIdPolizaAndFechaAndDiasGracia', async (req, res) => {
    let id_poliza = req.body.id_poliza;
    let fecha = req.body.fecha;
    fecha=fecha.split('T')[0];
    let dias_gracia = req.body.dias_gracia;
    let response = { status: 'OK', message: '', data: '' };
    await modelos.sequelize.query(`
    select D1.nro_documento,format(D1.fecha_emision,'dd-MM-yyyy') fecha_emision
,isnull(D.persona_primer_apellido,'')+' '+isnull(D.persona_segundo_apellido,'')+' '+isnull(D.persona_primer_nombre,'')+' '+isnull(D.persona_segundo_nombre,'') asegurado
,F1.valor cuenta ,D.persona_celular,G1.valor email,RIGHT('00' + Ltrim(Rtrim(H.nro_cuota_prog)),2) nro_cuota_prog,format(H.fecha_couta_prog,'dd/MM/yyyy') fecha_couta_prog,H1.parametro_descripcion motivo
,RIGHT('00' + Ltrim(Rtrim(datediff(day,format(H.fecha_couta_prog,'yyyy-MM-dd'),'${fecha}'))),2) dias_transcuridos
,format(DATEADD(day,A1.dias_gracia,H.fecha_couta_prog),'dd/MM/yyyy')  fecha_vencimiento
,'' agencia
,'' sucursal
,M.usuario_nombre_completo
,M.usuario_login
from instancia_poliza A 
inner join poliza A1 on A.id_poliza=A1.id and A.id_estado=59
inner join asegurado B on A.id=B.id_instancia_poliza
inner join entidad C on B.id_entidad=C.id
inner join persona D on D.id=C.id_persona
inner join instancia_documento D1 on D1.id_instancia_poliza=A.id and D1.id_documento in (3,6,9,12,15,18,21,24)
inner join objeto E on E.id_poliza=A.id_poliza and E.id_poliza=${id_poliza}
inner join objeto_x_atributo F on F.id_objeto=E.id and F.id_atributo=5 --cuenta
inner join objeto_x_atributo G on G.id_objeto=E.id and G.id_atributo=4 --correo
inner join objeto_x_atributo I on I.id_objeto=E.id and I.id_atributo=59 --agencia
--inner join atributo_instancia_poliza I1 on I1.id_objeto_x_atributo=I.id and I1.id_instancia_poliza=A.id
inner join objeto_x_atributo J on J.id_objeto=E.id and J.id_atributo=60 --sucursal
--inner join atributo_instancia_poliza J1 on J1.id_objeto_x_atributo=J.id and J1.id_instancia_poliza=A.id
inner join plan_pago_detalle H on H.id_instancia_poliza=A.id 
and H.nro_cuota_prog=(select isnull(max(nro_cuota_prog),0)+1 from plan_pago_detalle where id_instancia_poliza=A.id and id_estado=254)
and datediff(day,format(H.fecha_couta_prog,'yyyy-MM-dd'),'${fecha}')<=isnull(${dias_gracia},A1.dias_gracia) and H.fecha_couta_prog<=convert(datetime,'${fecha}',102)
--left join parametro K on K.parametro_cod=I1.valor and K.diccionario_id=40
--left join parametro L on L.parametro_cod=J1.valor and L.diccionario_id=38
left join atributo_instancia_poliza F1 on F1.id_objeto_x_atributo=F.id and F1.id_instancia_poliza=A.id
left join parametro H1 on H1.id=H.id_estado_det
left join atributo_instancia_poliza G1 on G1.id_objeto_x_atributo=G.id and G1.id_instancia_poliza=A.id
left join usuario M on A.adicionada_por=M.id
    `).then(result => {
            response.data = result[0];
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/listaCertificadosPorNroDocYIdPoliza', async (req, res) => {
    let id_poliza = req.body.id_poliza;
    let nro_certificado = req.body.nro_certificado;
    let response = { status: 'OK', message: '', data: '' };
    await modelos.sequelize.query(`select C.id,A.nro_documento,D.parametro_descripcion estado,format(A.fecha_emision,'dd/MM/yyyy') fecha_emision,format(A.fecha_inicio_vigencia,'dd/MM/yyyy') fecha_inicio_vigencia,format(A.fecha_fin_vigencia,'dd/MM/yyyy') fecha_fin_vigencia
    ,CASE C.ID WHEN C.id_instancia_renovada THEN 'ORIGINAL' ELSE 'RENOVADA' END RENOVADA
    from instancia_documento A 
    inner join instancia_poliza C on A.id_instancia_poliza=C.id and A.nro_documento='${nro_certificado}' and C.id_poliza=${id_poliza}
    inner join parametro D on C.id_estado=D.id and A.id_documento in (3,6,9,12,15,18,21,24)
    `).then(result => {
            response.data = result[0];
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

router.get(`/createPlanPagos/:idPoliza`, (req, res) => PlanPagoController.createPlanPagos(req.params,res));
router.get(`/createPlanPagosDetalle/:idPoliza`, (req, res) => PlanPagoController.createPlanPagosDetalle(req.params,res));

module.exports = router;
