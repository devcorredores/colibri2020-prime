const { Router } = require('express');
const router = new Router();
const bcrypt = require('bcrypt-nodejs');
const modelos = require("../modelos");
const authentificacion = require('../modulos/passport');
const solicitudService = require("../servicios/solicitud.service");
const isLoggedIn = solicitudService.isLoggedIn;

// var isLoggedIn = (req, res, next) => {
//     console.log('session ', req.session);
//     if (req.isAuthenticated()) {
//     }
//         return next()
//     //return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
// }

router.get('/', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.poliza.findAll()
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});


router.get('/byid/:id', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const diccionario_id = req.params.id;
    await modelos.poliza.findAll({ where: { diccionario_id } })
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/byidparam/:id', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await modelos.poliza.findAll({ where: { id } })
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/listapolizas',async (req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    await listapolizas(req, res, async resp => {
        response.data = resp;
    });
    res.json(response);
});

router.get('/getPolizaById/:idPoliza', isLoggedIn, async (req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const idPoliza = typeof req.params.idPoliza == 'string' ? req.params.idPoliza.indexOf(',') >= 0 ? req.params.idPoliza.split(',') : req.params.idPoliza : req.params.idPoliza;
    await getPolizaById(idPoliza, async resp => {
        response.data = resp;
    });
    res.json(response);
});

router.get('/getPolizaByIds/:idPolizas', isLoggedIn, async (req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const { idPolizas } = req.params;
    await getPolizaByIds(idPolizas, async resp => {
        response.data = resp;
    });
    res.json(response);
});

async function getPolizaById(idPoliza, callback) {
    let response = { status: 'OK', message: '', data: {} };
    let poliza = await modelos.poliza.findOne({
        where: {id:idPoliza},
        include: [
            {model:modelos.objeto, as: 'polizaObjeto', where:{id_tipo:298}},
            {
                model:modelos.objeto,
                as: 'documentoObjeto',
                required:false,
                where:{id_tipo:299},
                include:{
                    model:modelos.objeto_x_atributo,
                    include: {
                        model:modelos.atributo,
                    }
                }
            },
            {
                model:modelos.anexo_poliza,
                where:{id_tipo: 288, vigente : true},
                include: [
                    {model: modelos.parametro, as:'par_moneda'},
                    {
                        model: modelos.anexo_asegurado,
                        include: [
                            { model: modelos.dato_anexo_asegurado },
                            { model: modelos.parametro },
                            { model: modelos.parametro, as:'edad_unidad' }
                        ]
                    },
                ]
            },
            {model:modelos.parametro, as: 'tipo_numeracion'},
            {model:modelos.parametro, as: 'ramo'},
            {model:modelos.entidad, as: 'aseguradora'}
        ]
    });
    if (poliza.documentoObjeto) {
        for (let i = 0; i < poliza.documentoObjeto.objeto_x_atributos.length; i++) {
            let polizaDocumentoObjetoAtributo = poliza.documentoObjeto.objeto_x_atributos[i];
            let atributos = await modelos.atributo.findAll({where:{id_padre:polizaDocumentoObjetoAtributo.id_atributo}});
            poliza.documentoObjeto.objeto_x_atributos[i].atributo.dataValues.atributoHijos = atributos;
            poliza.documentoObjeto.objeto_x_atributos[i].atributo.atributoHijos = atributos;
        }
    }
    response.data = poliza;
    await callback(response.data);
}

async function getPolizaByIds(idPolizas, callback) {
    let response = { status: 'OK', message: '', data: {} };
    await modelos.poliza.findAll({
        where: {id:typeof idPolizas == 'string' ? idPolizas.split(',') : idPolizas },
        include: [
            {model:modelos.objeto, as: 'polizaObjeto', where:{id_tipo:298}},
            {
                model:modelos.anexo_poliza,
                where:{id_tipo: 288, vigente: true},
                include: [
                    {model: modelos.parametro, as:'par_moneda'},
                    {
                        model: modelos.anexo_asegurado,
                        include: [
                            { model: modelos.dato_anexo_asegurado },
                            { model: modelos.parametro },
                            { model: modelos.parametro, as:'edad_unidad' }
                        ]
                    }
                ]
            },
            {model:modelos.parametro, as: 'tipo_numeracion'},
            {model:modelos.parametro, as: 'ramo'},
            {model:modelos.entidad, as: 'aseguradora'},
        ]
    })
      .then(async resp => {
          response.data = resp;
      })
      .catch(err => {
          response.status = 'ERROR';
          response.message = err;
      });
    await callback(response.data);
}

async function listapolizas(req,res,callback) {
    let response = { status: 'OK', message: '', data: {} };
    const idsPolizas = typeof req.params.idsPolizas == 'string' ? req.params.idsPolizas.indexOf(',') >= 0 ? req.params.idsPolizas.split(',') : req.params.idsPolizas : req.params.idsPolizas;
    await modelos.poliza.findAll({
        where:idsPolizas ? {id:idsPolizas} : {},
        include: [
            {
                model:modelos.anexo_poliza,
                where:{id_tipo: 288, vigente: true},
                include: [
                    {model: modelos.parametro, as:'par_moneda'},
                    {
                        model: modelos.anexo_asegurado,
                        include: [
                            { model: modelos.dato_anexo_asegurado },
                            { model: modelos.parametro },
                            { model: modelos.parametro, as:'edad_unidad' }
                        ]
                    }
                ]
            },
            {model:modelos.objeto, as: 'polizaObjeto', where:{id_tipo:298}},
            {model:modelos.parametro, as: 'ramo'},
            {model:modelos.parametro, as: 'tipo_numeracion'},
            {model:modelos.entidad, as: 'aseguradora'}
        ]
    })
      .then(async resp => {
          response.data = resp;
      })
      .catch(err => {
          response.status = 'ERROR';
          response.message = err;
      });
    await callback(response.data);
}

module.exports = router;
