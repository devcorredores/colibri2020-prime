const { Router } = require("express");
const router = new Router();
const instanciaPolizaCtrl = require("../controladores/instancia_poliza.controller");
const Util = require("../utils/util");
const util = new Util();
const InstanciaPolizaService = require("../servicios/instanciaPoliza.service");

var isLoggedIn = (req, res, next) => {
  console.log("session ", req.session);
  if (req.isAuthenticated()) {
    return next();
  }
  return res
    .status(400)
    .json({ statusCode: 400, message: "usuario no autentificado" });
};

async function asyncForEach(array, callback) {
  if (array) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }
}

router.get("/GetAll", isLoggedIn, async (req, res) => {
  try {
    let objData = await InstanciaPolizaService.GetAll();
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByParametrosEcoVida", isLoggedIn, async (req, res) => {
  try {
    let objeto = req.body;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByParametrosEcoVida(objeto);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByParametrosDesgravamen", isLoggedIn, async (req, res) => {
  try {
    let objeto = req.body;
    let [objData,totalRows,rows,first]= await InstanciaPolizaService.GetAllByParametrosDesgravamen(objeto);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByParametrosEcoAccidentes", isLoggedIn, async (req, res) => {
  try {
    let objeto = req.body;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByParametrosEcoAccidentes(objeto);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByParametros", isLoggedIn, async (req, res) => {
  try {
    let objeto = req.body;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByParametros(objeto);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByParametrosTarjetas", async (req, res) => {
  try {
    let objeto = req.body;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByParametrosTarjetas(objeto);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByParametrosEcoMedic", async (req, res) => {
  try {
    let objeto = req.body;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByParametrosEcoMedic(objeto);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/listaAllByEstado", isLoggedIn, async (req, res) => {
  try {
    let tipo_poliza = req.body.tipo_poliza;
    let estado = req.body.estado;
    let id_documento = req.body.id_documento;
    let objData = await InstanciaPolizaService.listaAllByEstado(tipo_poliza,estado,id_documento);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByIds", isLoggedIn, async (req, res) => {
  try {
    let ids = req.body.id_instancia_poliza;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByIds(ids);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByIdsEcoVida", isLoggedIn, async (req, res) => {
  try {
    let ids = req.body.id_instancia_poliza;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByIdsEcoVida(ids);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByIdsDesgravamen", isLoggedIn, async (req, res) => {
  try {
    let ids = req.body.id_instancia_poliza;
    let objData = await InstanciaPolizaService.GetAllByIdsDesgravamen(ids);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByIdsEcoAccidentes", isLoggedIn, async (req, res) => {
  try {
    let ids = req.body.id_instancia_poliza;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByIdsEcoAccidentes(ids);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByIdsEcoMedic", isLoggedIn, async (req, res) => {
  try {
    let ids = req.body.id_instancia_poliza;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByIdsEcoMedic(ids);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/GetAllByIdsTarjetas", isLoggedIn, async (req, res) => {
  try {
    let ids = req.body.id_instancia_poliza;
    let [objData,totalRows,rows,first] = await InstanciaPolizaService.GetAllByIdsTarjetas(ids);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData,totalRows,rows,first);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.get("/GetById/:id", isLoggedIn, async (req, res) => {
  try {
    let id = req.params.id;
    let objData = await InstanciaPolizaService.GetById(id);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.get("/FindInstanciaPolizaById/:idInstanciaPoliza", isLoggedIn, async (req, res) => {
  try {
    let idInstanciaPoliza = req.params.idInstanciaPoliza;
    let objData = await InstanciaPolizaService.findInstanciaPolizaById(idInstanciaPoliza);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.get("/verifyCouldBeRenovated/:idPoliza/:idInstanciaPoliza", isLoggedIn, async (req, res) => {
  try {
    let idPoliza = req.params.idPoliza;
    let idInstanciaPoliza = req.params.idInstanciaPoliza;
    let objData = await InstanciaPolizaService.verifyCouldBeRenovated(idPoliza,idInstanciaPoliza);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setWarning(200, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.get("/FindInstanciaPolizaConAtributosEditablesYVisiblesByIdYPolizaId/:idInstanciaPoliza/:idPoliza/:idEditable/:idVisible", isLoggedIn, async (req, res) => {
  try {
    let idInstanciaPoliza = req.params.idInstanciaPoliza;
    let idPoliza = req.params.idPoliza;
    let idEditable = req.params.idEditable;
    let idVisible = req.params.idVisible;
    let objData = await InstanciaPolizaService.findInstanciaPolizaConAtributosEditablesYVisiblesByIdYPolizaId(
        idInstanciaPoliza,
        idPoliza,
        idEditable,
        idVisible);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.get("/FindInstanciaPolizaConAtributosByIdYPolizaId/:idInstanciaPoliza/:idPoliza", isLoggedIn, async (req, res) => {
  try {
    let idInstanciaPoliza = req.params.idInstanciaPoliza;
    let idPoliza = req.params.idPoliza;
    let objData = await InstanciaPolizaService.findInstanciaPolizaConAtributosByIdYPolizaId(idInstanciaPoliza, idPoliza);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.post("/UpdateInstanciaToNextStatus/:statusIds", isLoggedIn, async (req, res) => {
  try {
    let statusIds = req.params.statusIds.split(",");
    let asegurado = req.body;
    let id_usuario = req.session.passport.user;
    let objData = await InstanciaPolizaService.UpdateInstanciaToNextStatus(asegurado, statusIds, id_usuario);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.get("/findSolicitudTransiciones/:idInstanciaPoliza", isLoggedIn, async (req, res) => {
  try {
    let idInstanciaPoliza = req.params.idInstanciaPoliza;
    let objData = await InstanciaPolizaService.findSolicitudTransiciones(idInstanciaPoliza);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.get("/findPolizaNrosTransaccion/:idPoliza/:nroTransaccion", isLoggedIn, async (req, res) => {
  try {
    let idPoliza = req.params.idPoliza;
    let nroTransaccion = req.params.nroTransaccion;
    let objData = await InstanciaPolizaService.findPolizaNrosTransaccion(idPoliza, nroTransaccion);
    if (objData) {
      util.setSuccess(200, "data retrieved", objData);
    } else {
      util.setError(404, `No data found`);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
});

router.get("/deleteInstancias/:idInstancias", async (req, res) => await instanciaPolizaCtrl.deleteInstanciaPolizas(req.params, res));

module.exports = router;
