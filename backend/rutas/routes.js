// app/routes.js
module.exports = function (app, passport) {

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	// app.get('/', function (req, res) {
	// 	res.render('index.jade'); // load the index.jade file
	// 	// console.log('Example app listening on port 5001!');
	// });

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form

	app.get('/login', function (req, res) {
		// render the page and pass in any flash data if it exists
		res.render('login.jade', { message: req.flash('loginMessage') });
	});

	// app.get('/', function (req, res) {

	// 	// render the page and pass in any flash data if it exists
	// 	res.render('/public/index.html' );
	// });

	// process the login form
	// app.post('/login', passport.authenticate('local-login', {
	// 	successRedirect: '/profile', // redirect to the secure profile section
	// 	failureRedirect: '/signup', // redirect back to the signup page if there is an error
	// 	failureFlash: true // allow flash messages
	// }),
	// 	function (req, res) {
	// 		console.log("hello");

	// 		if (req.body.remember) {
	// 			req.session.cookie.maxAge = 1000 * 60 * 3;
	// 		} else {
	// 			req.session.cookie.expires = false;
	// 		}
	// 		res.redirect('/');
	// 	});

		app.get('/profile', function (req, res) {
			// return res.send({"user":"raul"});
			 res.send({"user":req.user});
			// res.render('profile.jade', {
			// 	user: req.user // get the user out of session and pass to template
			// });
		});		

	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function (req, res) {
		// render the page and pass in any flash data if it exists
		// res.render('signup.jade', { message: req.flash('signupMessage') });
		res.send(null);
	});

	// process the signup form
	// app.post('/signup', passport.authenticate('local-signup', {
	// 	successRedirect: '/profile', // redirect to the secure profile section
	// 	failureRedirect: '/signup', // redirect back to the signup page if there is an error
	// 	failureFlash: true // allow flash messages
	// }));


	// =====================================
	// RESTORE ==============================
	// =====================================
	// show the restore form
	app.get('/restore', function (req, res) {
		// render the page and pass in any flash data if it exists
		res.render('restore.jade', { message: req.flash('restoreMessage') });
	});

	// process the signup form
	app.post('/restore', passport.authenticate('local-restore', {
		successRedirect: '/login', // redirect to the secure profile section
		failureRedirect: '/restore', // redirect back to the signup page if there is an error
		failureFlash: true // allow flash messages
	}));



	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	

	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function (req, res) {
		req.logout();
		res.redirect('/');
	});
};

// route middleware to make sure
// function isLoggedIn(req, res, next) {
// 	// if user is authenticated in the session, carry on
// 	if (req.isAuthenticated())
// 	{
// 		return next();
// 	}else{
// 		// res.redirect('/');
// 		res.send({"dato": "no conectado"});
// 	}
// 	// if they aren't redirect them to the home page
	
// }
