const { Router } = require('express');
const router = new Router();
const invoke = require("../modulos/soap-request");
const request = require('request');
const soap = require('soap');
const webconfig = require('../config/web-config');
const to_json = require('xmljson').to_json;
const modelos = require('../modelos');

const specialRequest = request.defaults({
  strictSSL: false
});

const soapHeaderXml = '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';

// router.get('/', async (req, res) => {
//   res.json({ status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' });
// });


router.get('/fillTmpCICodAgenda', async (req, res) => {
    let newData;
    let response = {status: 'OK', message: 'api-corredores-ecofuturo/tmp_CI_CODAGENDA', data: []};
    let registros = await modelos.tmp_CI_CODAGENDA.findAll();
    let parametros = await modelos.parametro.findAll({where: {diccionario_id: [18, 22]}});

    if (registros.length && parametros.length) {

        let ciudades = parametros.filter(param => param.dataValues.diccionario_id == 18);
        let monedas = parametros.filter(param => param.dataValues.diccionario_id == 22);

        for (let j = 0; j < registros.length; j++) {
            let doc_id = registros[j].dataValues.Num_doc_sinExt;
            let ext2 = doc_id.substring(doc_id.length - 2, doc_id.length);
            if (ext2.isLetter()) {
                doc_id = doc_id.substring(0, doc_id.length - 2);
            }
            let ext = null;
            for (let i = 0; i < ciudades.length; i++) {
                if (ciudades[i].dataValues.parametro_abreviacion == registros[j].dataValues.num_doc_ext) {
                    ext = ciudades[i].dataValues.parametro_cod;
                    break;
                }
            }
            let responseSolicitud = {status: "OK", message: "", data: ""};
            responseSolicitud = await invoke.soapExecute(
                {
                    doc_id: doc_id,
                    ext: ext,
                    usuario: 'prueba',
                    password: 'prueba'
                },
                "getCustomer",
                "WS-SOAP-ECOFUTURO"
            );

            if (responseSolicitud.status === "OK") {
                if (responseSolicitud.data.getCustomerResult) {
                    to_json(responseSolicitud.data.getCustomerResult, function (error, data) {
                        newData = data.DataSetAll.tmp_general;
                    });
                } else {
                    response.status = "ERROR";
                    response.data = responseSolicitud.data.getSolicitudPropuestaResult;
                }
                if (newData) {

                    let cod_agenda = newData.cod_agenda;
                    let newAccountData;
                    responseSolicitud = await invoke.soapExecute(
                        {
                            cod_agenda: cod_agenda,
                            usuario: 'prueba',
                            password: 'prueba'
                        },
                        "getAccount",
                        "WS-SOAP-ECOFUTURO"
                    );

                    if (responseSolicitud.status === "OK") {
                        if (responseSolicitud.data.getAccountResult) {
                            to_json(responseSolicitud.data.getAccountResult, function (error, data) {
                                if (data.Cuenta != "") {
                                    newAccountData = data.Cuenta.Cuentas;
                                } else {
                                    response.status = "ERROR";
                                }
                            });
                        } else {
                            response.status = "ERROR";
                            response.data = responseSolicitud.data.getSolicitudPropuestaResult;
                        }
                    } else {
                        response = responseSolicitud;
                    }

                    let newRegistro = registros[j].dataValues;
                    newRegistro.paterno = newData.paterno;
                    newRegistro.materno = newData.materno;
                    newRegistro.NOMBRE = newData.nombre;
                    newRegistro.doc_id = doc_id;
                    newRegistro.sexo = newData.sexo;
                    newRegistro.codAgenda = newData.cod_agenda;
                    await modelos.tmp_CI_CODAGENDA.update(newRegistro, {where: {id: newRegistro.id}}).catch(err => {
                        response.status = 'ERROR';
                        response.message = err;
                    });
                    response.data.push(newRegistro);

                    if (newAccountData) {
                        let personaBancoAccount;
                        let personaBancoAccounts = [];
                        let cuentas = [];
                        if (newAccountData.nrocuenta != undefined) {
                            personaBancoAccount = newAccountData;
                            personaBancoAccounts.push(personaBancoAccount);
                            cuentas.push({
                                manejo: personaBancoAccount.manejo,
                                cod_agenda: personaBancoAccount.cod_agenda,
                                moneda: personaBancoAccount.moneda,
                                nrocuenta: personaBancoAccount.nrocuenta
                            });
                        } else {
                            delete newAccountData['isInArray'];
                            let keys = Object.keys(newAccountData);
                            let values = Object.values(newAccountData);
                            await keys.forEach((key) => {
                                cuentas.push({
                                    manejo: values[key].manejo,
                                    cod_agenda: values[key].cod_agenda,
                                    moneda: values[key].moneda,
                                    nrocuenta: values[key].nrocuenta
                                });
                                personaBancoAccounts.push(values[key]);
                            });
                        }
                        if (cuentas.length) {
                            for (let j = 0; j < cuentas.length; j++) {
                                let cuenta = cuentas[j];
                                let oldCiCuentas = await modelos.tmp_CI_CUENTA.findAll({
                                    where: {
                                        cod_agenda: cuenta.cod_agenda,
                                        manejo: cuenta.manejo,
                                        nrocuenta: cuenta.nrocuenta,
                                        moneda: cuenta.moneda
                                    }
                                });
                                let parMoneda = monedas.find(param => param.dataValues.parametro_cod == cuenta.moneda);
                                if (oldCiCuentas.length) {

                                } else {
                                    if(parMoneda){
                                        cuenta.moneda_abbr = parMoneda.dataValues.parametro_abreviacion;
                                    }
                                    await modelos.tmp_CI_CUENTA.create(cuenta).then((resp) => {
                                        response.data.push(resp);
                                    }).catch(err => {
                                        response.status = 'ERROR';
                                        response.message = err;
                                    });
                                }
                            }
                        }
                    }
                }
            } else {
                response = responseSolicitud;
            }
        }
    }
    console.log(response.data);

    res.json(response);
});

router.get('/updateUsuarioBancoDataOnInstances/:idPoliza', async (req, res) => {
  let newData;
  let idPoliza = req.params.idPoliza;
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/tmp_CI_CODAGENDA', data: {} };
  let sucursales = await modelos.parametro.findAll({where:{diccionario_id:[38]}});
  let usuarios = await modelos.usuario.findAll();
  let objetoAtributoSucursal, objetoAtributoAgencia;
  let objetoXAtributos = await modelos.objeto_x_atributo.findAll({
    include:[
      {
        model:modelos.objeto,
        where:{id_poliza:idPoliza},
      },
      {
        model:modelos.atributo,
        where:{id:{$in:[59,60]}}
      },
    ]
  });
  if(objetoXAtributos.length) {
    objetoXAtributos.forEach(objetoXAtributo => {
      switch (objetoXAtributo.dataValues.atributo.id.toString()) {
        case '59':
          objetoAtributoAgencia = objetoXAtributo.dataValues;
          break;
        case '60':
          objetoAtributoSucursal = objetoXAtributo.dataValues;
          break;
      }
    });
    await FindPersonaSolicitudByIdPoliza(idPoliza, async solicitudes => {
      if(solicitudes.length) {
        for(let i = 0 ; i < solicitudes.length ; i++) {
          try {
            let solicitud = solicitudes[i];
            if(solicitud.instancia_poliza.atributo_instancia_polizas.length) {
              let atributoInstanciaPoliza = solicitud.instancia_poliza.atributo_instancia_polizas;
              let instancia_poliza = solicitud.instancia_poliza;
              let objeto = await modelos.objeto.findOne({where:{id_poliza:idPoliza}});
              let objetoAtributoSucursal = await modelos.objeto_x_atributo.findOne({where:{id_objeto:objeto.dataValues.id, id_atributo:60}});
              let objetoAtributoAgencia = await modelos.objeto_x_atributo.findOne({where:{id_objeto:objeto.dataValues.id, id_atributo:59}});
              let objetoAtributoCargo = await modelos.objeto_x_atributo.findOne({where:{id_objeto:objeto.dataValues.id, id_atributo:70}});

              let atributoOficialSucursal = await modelos.atributo_instancia_poliza.find({where:{id_instancia_poliza:instancia_poliza.id, id_objeto_x_atributo:objetoAtributoSucursal.dataValues.id}});
              let atributoOficialAgencia = await modelos.atributo_instancia_poliza.find({where:{id_instancia_poliza:instancia_poliza.id, id_objeto_x_atributo:objetoAtributoAgencia.dataValues.id}});
              let atributoOficialCargo = await modelos.atributo_instancia_poliza.find({where:{id_instancia_poliza:instancia_poliza.id, id_objeto_x_atributo:objetoAtributoCargo.dataValues.id}});
              // let atributoOficialSucursal = atributoInstanciaPoliza.find(param => param.objeto_x_atributo.atributo.id == 60);
              // let atributoOficialAgencia = atributoInstanciaPoliza.find(param => param.objeto_x_atributo.atributo.id == 59);
              // let atributoOficialCargo = atributoInstanciaPoliza.find(param => param.objeto_x_atributo.atributo.id == 70);

              let userAdicionadaPor = usuarios.find(param => param.dataValues.id == instancia_poliza.usuario.dataValues.id);
              let usuarioColibri = await modelos.usuarioColibri.find({where:{codusuario:userAdicionadaPor.dataValues.usuario_login}});
              // let usuarioColibri = registros.find(param => param.dataValues.codusuario = userAdicionadaPor.dataValues.usuario_login);
              let parSucursal = sucursales.find(param => param.dataValues.parametro_descripcion.trim() == usuarioColibri.dataValues.sucursal.trim());
              let agencias = await modelos.parametro.findAll({where:{id_padre:parSucursal.id}});
              let parAgencia;
              agencias.forEach(agencia => {
                if(agencia.dataValues.parametro_descripcion.trim() == usuarioColibri.dataValues.Agencia.trim() ||
                  agencia.dataValues.parametro_descripcion.toLowerCase().trim() == usuarioColibri.dataValues.Agencia.toLowerCase().trim()
                ) {
                  parAgencia = agencia
                }
              });
              // let parAgencia = agencias.find(param => param.dataValues.parametro_descripcion.trim() == usuarioColibri.dataValues.Agencia.trim());
              // response.data[usuarioColibri.ID] = usuarioColibri.dataValues;
              if(parSucursal != undefined) {
                if(atributoOficialSucursal == undefined || atributoOficialSucursal == null) {
                  let new_atributo_instancia_poliza = {};
                  new_atributo_instancia_poliza.id = null;
                  new_atributo_instancia_poliza.id_instancia_poliza = instancia_poliza.id;
                  new_atributo_instancia_poliza.id_objeto_x_atributo = objetoAtributoSucursal.id;
                  new_atributo_instancia_poliza.adicionado_por = instancia_poliza.adicionada_por != null ? instancia_poliza.adicionada_por : '2';
                  new_atributo_instancia_poliza.modificado_por = instancia_poliza.adicionada_por != null ? instancia_poliza.adicionada_por : '2';
                  new_atributo_instancia_poliza.createdAt = instancia_poliza.createdAt;
                  new_atributo_instancia_poliza.updatedAt = instancia_poliza.createdAt;
                  new_atributo_instancia_poliza.valor = parSucursal.dataValues.parametro_cod;
                  new_atributo_instancia_poliza.tipo_error = 'El usuario del banco no tiene registrado una sucursal';
                  await modelos.atributo_instancia_poliza.create(new_atributo_instancia_poliza)
                    .then(async resp => {
                      response.data[instancia_poliza.id] = resp.dataValues;
                    })
                    .catch(err => {
                      response.status = 'ERROR';
                      response.message = err;
                    });
                } else {
                  // response.data[instancia_poliza.id] = atributoOficialSucursal;
                }
              }
              if(parAgencia != undefined) {
                if(atributoOficialAgencia == undefined || atributoOficialAgencia == null) {
                  let new_atributo_instancia_poliza = {};
                  new_atributo_instancia_poliza.id = null;
                  new_atributo_instancia_poliza.id_instancia_poliza = instancia_poliza.id;
                  new_atributo_instancia_poliza.id_objeto_x_atributo = objetoAtributoAgencia.id;
                  new_atributo_instancia_poliza.adicionado_por = instancia_poliza.adicionada_por != null ? instancia_poliza.adicionada_por : '2';
                  new_atributo_instancia_poliza.modificado_por = instancia_poliza.adicionada_por != null ? instancia_poliza.adicionada_por : '2';
                  new_atributo_instancia_poliza.createdAt = instancia_poliza.createdAt;
                  new_atributo_instancia_poliza.updatedAt = instancia_poliza.createdAt;
                  new_atributo_instancia_poliza.valor = parAgencia.dataValues.parametro_cod;
                  new_atributo_instancia_poliza.tipo_error = 'El usuario del banco no tiene registrado una agencia';
                  await modelos.atributo_instancia_poliza.create(new_atributo_instancia_poliza)
                    .then(async resp => {
                      response.data[instancia_poliza.id] = resp.dataValues;
                    })
                    .catch(err => {
                      response.status = 'ERROR';
                      response.message = err;
                    });
                } else {
                  // response.data.push(atributoOficialSucursal);
                }
              }

                if(atributoOficialCargo == undefined || atributoOficialCargo == null) {
                  let new_atributo_instancia_poliza = {};
                  new_atributo_instancia_poliza.id = null;
                  new_atributo_instancia_poliza.id_instancia_poliza = instancia_poliza.id;
                  new_atributo_instancia_poliza.id_objeto_x_atributo = objetoAtributoCargo.id;
                  new_atributo_instancia_poliza.adicionado_por = instancia_poliza.adicionada_por != null ? instancia_poliza.adicionada_por : '2';
                  new_atributo_instancia_poliza.modificado_por = instancia_poliza.adicionada_por != null ? instancia_poliza.adicionada_por : '2';
                  new_atributo_instancia_poliza.createdAt = instancia_poliza.createdAt;
                  new_atributo_instancia_poliza.updatedAt = instancia_poliza.createdAt;
                  new_atributo_instancia_poliza.valor = usuarioColibri.dataValues.cargo;
                  new_atributo_instancia_poliza.tipo_error = 'El usuario del banco no tiene registrado un cargo';
                  await modelos.atributo_instancia_poliza.create(new_atributo_instancia_poliza)
                    .then(async resp => {
                      response.data[instancia_poliza.id] = resp.dataValues;
                    })
                    .catch(err => {
                      response.status = 'ERROR';
                      response.message = err;
                    });
                } else {
                  // response.data.push(atributoOficialSucursal);
                }
            }
          } catch (e) {
            console.log(e);
          }
        }
      }
      console.log(response.data);
    });
  }
res.json(response);
});


router.get('/updateAtributosDubplicados/:idPoliza', async (req, res) => {
  let newData;
  let idPoliza = req.params.idPoliza;
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/tmp_CI_CODAGENDA/updateAtributosDubplicados', data: {} };
  try {
    let objeto = await modelos.objeto.find({where:{id_poliza:idPoliza}});
    let objetoAtributos = await modelos.objeto_x_atributo.findAll({where:{id_objeto:objeto.dataValues.id}});
    let aObjetoAtributos = [];
    objetoAtributos.forEach(objetoAtributo => {
      aObjetoAtributos.push(objetoAtributo.dataValues.id);
    });
    await FindPersonaSolicitudByIdPoliza(idPoliza, async (asegurados) => {
      if(asegurados.length && objetoAtributos.length) {
        for (let i = 0 ; i < asegurados.length ; i++) {
          let asegurado = asegurados[i].dataValues;
          let tieneDuplicados = false;
          await objetoAtributos.forEach(objetoAtributo => {
            let duplicados = asegurado.instancia_poliza.atributo_instancia_polizas.filter(param => param.dataValues.id_objeto_x_atributo == objetoAtributo.id);
            if(duplicados.length > 1) {
              tieneDuplicados = true;
            }
          });
          if(tieneDuplicados) {
            //eliminacion de datos duplicados
            let atributosInstanciaPoliza = asegurado.instancia_poliza.atributo_instancia_polizas;
            for (let j = 0 ; j < objetoAtributos.length ; j++) {
              let objetoAtributo = objetoAtributos[j].dataValues;
              let atributosDuplicados = atributosInstanciaPoliza.filter(param => param.dataValues.id_objeto_x_atributo == objetoAtributo.id);
              if(atributosDuplicados.length > 1) {
                let atributosDupSinValor = atributosDuplicados.filter(param => param.dataValues.valor == '');
                for (let k = 0 ; k < atributosDupSinValor.length-1 ; k++) {
                  let atributo = atributosDupSinValor[k];
                  let respAtributoSinValorUpdated = await modelos.atributo_instancia_poliza.destroy({where:{id:atributo.id}});
                  // let respAtributoSinValorUpdated = await modelos.atributo_instancia_poliza.update({tipo_error:'eliminar'},{where:{id:atributo.id}});
                  response.data[atributo.id_instancia_poliza] = atributo;
                }
                let atributosDupConValor = atributosDuplicados.filter(param => param.dataValues.valor != '');
                for (let k = 0 ; k < atributosDupConValor.length-1 ; k++) {
                  let atributo = atributosDupConValor[k];
                  let respAtributoConValorUpdated = await modelos.atributo_instancia_poliza.destroy({where:{id:atributo.id}});
                  // let respAtributoConValorUpdated = await modelos.atributo_instancia_poliza.update({tipo_error:'eliminar'},{where:{id:atributo.id}});
                  response.data[atributo.id_instancia_poliza] = atributo;
                }
              }
            }
          }
        }
      }
      console.log(response.data);
    });
  } catch (e) {
    console.log(e);
  }
  res.json(response);
});

router.get('/updateAtributosValores/:idPoliza', async (req, res) => {
  let newData;
  let idPoliza = req.params.idPoliza;
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/tmp_CI_CODAGENDA/updateAtributosValores', data: {} };
  try {
    let objeto = await modelos.objeto.find({where:{id_poliza:idPoliza}});
    let objetoAtributos = await modelos.objeto_x_atributo.findAll(
      {
        where:{id_objeto:objeto.dataValues.id},
        include:[
          {model:modelos.objeto},
          {model:modelos.atributo},
        ]
    });
    let parametros = await modelos.parametro.findAll({where:{diccionario_id:[1,21,22]}});
    let monedas = parametros.filter(param => param.dataValues.diccionario_id == 22);
    let tipoDocumentos = parametros.filter(param => param.dataValues.diccionario_id == 1);
    let condiciones = parametros.filter(param => param.dataValues.diccionario_id == 21);
    let aObjetoAtributos = [];
    objetoAtributos.forEach(objetoAtributo => {
      aObjetoAtributos.push(objetoAtributo.dataValues.id);
    });
    await FindPersonaSolicitudByIdPoliza(idPoliza, async (asegurados) => {
      if(asegurados.length && objetoAtributos.length) {
        for (let i = 0 ; i < asegurados.length ; i++) {
          let asegurado = asegurados[i].dataValues;
          let tieneDuplicados = false;
          objetoAtributos.forEach(objetoAtributo => {
            let duplicados = asegurado.instancia_poliza.atributo_instancia_polizas.filter(param => param.dataValues.id_objeto_x_atributo == objetoAtributo.id);
            if(duplicados > 1) {
              tieneDuplicados = true;
            }
          });
          if(!tieneDuplicados) {
            //actualizacion de informacion en blanco
            let atributosInstaciaPoliza = asegurado.instancia_poliza.atributo_instancia_polizas;
            let personaBanco;
            let personaBancoAccounts;
            let persona_doc_id = asegurado.entidad.persona.persona_doc_id;
            let persona_doc_id_ext = asegurado.entidad.persona.persona_doc_id_ext;
            let responseSolicitud = { status: "OK", message: "", data: "" };
            responseSolicitud = await invoke.soapExecute(
              { doc_id: persona_doc_id,
                ext: persona_doc_id_ext,
                usuario:'prueba',
                password:'prueba'},
              "getCustomer",
              "WS-SOAP-ECOFUTURO"
            );

            if (responseSolicitud.status === "OK") {
              if (responseSolicitud.data.getCustomerResult) {
                to_json(responseSolicitud.data.getCustomerResult, function (error, data) {
                  personaBanco = data.DataSetAll.tmp_general;
                });
              } else {
                response.status = "ERROR";
                response.data['error'] = responseSolicitud.data.getSolicitudPropuestaResult;
              }
            } else {
              response.status = "ERROR";
              response.data['error'] = responseSolicitud;
            }

            if(personaBanco != undefined && personaBanco != null){

              let cod_agenda = personaBanco.cod_agenda;
              let responseSolicitudAccount = { status: "OK", message: "", data: "" };

              responseSolicitudAccount = await invoke.soapExecute(
                { cod_agenda: cod_agenda,
                  usuario:'prueba',
                  password:'prueba'},
                "getAccount",
                "WS-SOAP-ECOFUTURO"
              );

              if (responseSolicitudAccount.status === "OK") {
                if (responseSolicitudAccount.data.getAccountResult) {
                  to_json(responseSolicitudAccount.data.getAccountResult, function (error, data) {
                    if (data.Cuenta != "") {
                      personaBancoAccounts = data.Cuenta.Cuentas;
                    } else {
                      response.status = "ERROR";
                    }
                  });
                } else {
                  response.status = "ERROR";
                  response.data['error'] = responseSolicitudAccount.data.getSolicitudPropuestaResult;
                }
              } else {
                response.status = "ERROR";
                response.data['error'] = responseSolicitudAccount.data.getSolicitudPropuestaResult;
              }

              if(personaBancoAccounts != undefined && personaBancoAccounts != null){

                let cuentas = [];
                if (personaBancoAccounts.nrocuenta != undefined) {
                  cuentas.push({nrocuenta: personaBancoAccounts.nrocuenta,moneda:personaBancoAccounts.moneda});
                } else {
                  let keys = Object.keys(personaBancoAccounts);
                  let values = Object.values(personaBancoAccounts);
                  await keys.forEach((key) => {
                    cuentas.push({nrocuenta: values[key].nrocuenta,moneda:values[key].moneda});
                  });
                }

                // tiene debito automatico
                let tieneDebitoAutomatico = false;
                let atributoModalidadPago = atributosInstaciaPoliza.find(param => param.dataValues.objeto_x_atributo.atributo.id == 12);
                let condicionNo = condiciones.find(param => param.dataValues.parametro_cod == 'N');
                let condicionSi = condiciones.find(param => param.dataValues.parametro_cod == 'S');
                if(atributoModalidadPago != undefined && atributoModalidadPago != null) {
                  if(atributoModalidadPago.dataValues.valor == condicionNo.dataValues.id) {
                    tieneDebitoAutomatico = true;
                  }
                }
                try {
                  for (let j = 0; j < atributosInstaciaPoliza.length; j++) {
                    let atributoInstaciaPoliza = atributosInstaciaPoliza[j];
                    if (atributoInstaciaPoliza.valor == '') {
                      switch (atributoInstaciaPoliza.objeto_x_atributo.atributo.id.toString()) {
                        case '10':
                          // Código Agenda
                          if(personaBanco.cod_agenda != '') {
                            atributoInstaciaPoliza.valor = personaBanco.cod_agenda;
                            modelos.atributo_instancia_poliza.update({
                              valor: personaBanco.cod_agenda,
                              // tipo_error: 'actualizado'
                              tipo_error:'La persona no tiene registrado un codigo de agenda'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '14':
                          //   Código CAEDEC
                          if(personaBanco.caedec != '') {
                            atributoInstaciaPoliza.valor = personaBanco.caedec;
                            modelos.atributo_instancia_poliza.update({
                              valor: personaBanco.caedec,
                              // tipo_error: 'actualizado',
                              tipo_error:'El titular no tiene registrado un numero CAEDEC'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '16':
                          //   Localidad
                          if(personaBanco.localidad = '') {
                            atributoInstaciaPoliza.valor = personaBanco.localidad;
                            modelos.atributo_instancia_poliza.update({
                              valor: personaBanco.localidad,
                              // tipo_error: 'actualizado',
                              tipo_error:'El titular no tiene registrado una localidad'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '17':
                          //   Departamento
                          if(personaBanco.departamento != '') {
                            atributoInstaciaPoliza.valor = personaBanco.departamento;
                            modelos.atributo_instancia_poliza.update({
                              valor: personaBanco.departamento,
                              // tipo_error: 'actualizado'
                              tipo_error:'El titular no tiene registrado un departamento'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '18':
                          //   Código de Sucursal
                          if(personaBanco.cod_sucursal != '') {
                            atributoInstaciaPoliza.valor = personaBanco.cod_sucursal;
                            modelos.atributo_instancia_poliza.update({
                              valor: personaBanco.cod_sucursal,
                              // tipo_error: 'actualizado'
                              tipo_error:'El titular no tiene registrado un codigo de sucursal'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '19':
                          //   Tipo Documento
                          let tipoDoc;
                          if (persona_doc_id != '' && persona_doc_id_ext != '') {
                            tipoDoc = tipoDocumentos.find(param => param.dataValues.parametro_descripcion == 'CEDULA DE IDENTIDAD');
                          }
                          if(tipoDoc != undefined) {
                            atributoInstaciaPoliza.valor = tipoDoc.dataValues.parametro_descripcion;
                            modelos.atributo_instancia_poliza.update({
                              valor: tipoDoc.dataValues.parametro_descripcion,
                              // tipo_error: 'actualizado'
                              tipo_error:'El titular no tiene registrado el tipo de documento utilizado'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '4':
                          //   Correo electronico
                          if(personaBanco.e_mail != '') {
                            atributoInstaciaPoliza.valor = personaBanco.e_mail;
                            modelos.atributo_instancia_poliza.update({
                              valor: personaBanco.e_mail,
                              // tipo_error: 'actualizado'
                              tipo_error:'El titular no tiene registrado su e-mail'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '22':
                          //   Descripcion CAEDEC
                          if(personaBanco.desc_caedec != '') {
                            atributoInstaciaPoliza.valor = personaBanco.desc_caedec;
                            modelos.atributo_instancia_poliza.update({
                              valor: personaBanco.desc_caedec,
                              // tipo_error: 'actualizado'
                              tipo_error:'El titular no tiene registrado una descripción en caedec'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '20':
                          //   Manejo
                          if (cuentas.length == 1 && tieneDebitoAutomatico) {
                            if(personaBanco.manejo != '') {
                              atributoInstaciaPoliza.valor = personaBanco.manejo;
                              modelos.atributo_instancia_poliza.update({
                                valor: personaBancoAccounts.manejo,
                                // tipo_error: 'actualizado'
                                tipo_error:'El titular no tiene registrado un codigo de manejo'
                              }, {where: {id: atributoInstaciaPoliza.id}});
                              response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                            }
                          }
                          break;
                        case '5':
                          //   Cuenta bancaria
                          if (cuentas.length == 1 && tieneDebitoAutomatico) {
                            atributoInstaciaPoliza.valor = cuentas[0].nrocuenta;
                            modelos.atributo_instancia_poliza.update({
                              valor: cuentas[0].nrocuenta,
                              // tipo_error: 'actualizado'
                              tipo_error:'El titular no tiene registrado un numero de cuenta'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '21':
                          //   Cuenta bancaria moneda
                          if (cuentas.length == 1 && tieneDebitoAutomatico) {
                            atributoInstaciaPoliza.valor = cuentas[0].moneda;
                            modelos.atributo_instancia_poliza.update({
                              valor: cuentas[0].moneda,
                              // tipo_error: 'actualizado'
                              tipo_error:'El titular no tiene registrado un tipo de moneda'
                            }, {where: {id: atributoInstaciaPoliza.id}});
                            response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          }
                          break;
                        case '12':
                          //   Pago en efectivo
                          atributoInstaciaPoliza.valor = condicionSi.dataValues.id;
                          modelos.atributo_instancia_poliza.update({
                            valor: condicionSi.dataValues.id,
                            // tipo_error: 'actualizado'
                            tipo_error:'El titular no tiene registrado si el debito sera automatico'
                          }, {where: {id: atributoInstaciaPoliza.id}});
                          response.data[atributoInstaciaPoliza.id_instancia_poliza] = atributoInstaciaPoliza.dataValues;
                          break;
                      }
                    }
                  }
                } catch (e) {
                  console.log(e);
                }
              }
            }
          }
        }
      }
      console.log(response.data);
    });
  } catch (e) {
    console.log(e);
  }
  res.json(response);
});

router.get('/updateSolicitudTrancisiones/:idPoliza', async (req, res) => {
    let newData;
    let idPoliza = req.params.idPoliza;
    let response = {status: 'OK', message: 'api-corredores-ecofuturo/tmp_CI_CODAGENDA/updateSolicitudTrancisiones', data: {}};
    try {
        let objeto = await modelos.objeto.find({where:{id_poliza:idPoliza}});
        let objetoAtributos = await modelos.objeto_x_atributo.findAll(
            {
                where:{id_objeto:objeto.dataValues.id},
                include:[
                    {model:modelos.objeto},
                    {model:modelos.atributo},
                ]
            });
        let parametros = await modelos.parametro.findAll({where:{diccionario_id:[32,11]}});
        let documentos = await modelos.documento.findAll({where:{id_poliza:idPoliza}});
        let solicitudEstados = parametros.filter(param => param.dataValues.diccionario_id == 11);

      let parObservacionSuccess = parametros.find(param => param.dataValues.id == 98);
      let parObservacionInfo = parametros.find(param => param.dataValues.id == 97);
      let parEstadoEmitido = parametros.find(param => param.dataValues.id == 59);
      let parEstadoSolicitado = parametros.find(param => param.dataValues.id == 81);
      let parEstadoPorPagar = parametros.find(param => param.dataValues.id == 238);
      let parEstadoAnulado = parametros.find(param => param.dataValues.id == 60);
      let parEstadoIniciado = parametros.find(param => param.dataValues.id == 24);
      let docSolicitud = documentos[0];
      let docOrdenCobro = documentos[1];
      let docEmision = documentos[2];

      parObservacionSuccess = parObservacionSuccess.dataValues;
      parObservacionInfo = parObservacionInfo.dataValues;
      parEstadoEmitido = parEstadoEmitido.dataValues;
      parEstadoSolicitado = parEstadoSolicitado.dataValues;
      parEstadoPorPagar = parEstadoPorPagar.dataValues;
      parEstadoAnulado = parEstadoAnulado.dataValues;
      parEstadoIniciado = parEstadoIniciado.dataValues;

        await findTrancisionesSolicitudByIdPoliza(idPoliza, async (asegurados) => {
            if (asegurados.length) {
                for (let i = 0 ; i < asegurados.length ; i++) {
                  let asegurado = asegurados[i].dataValues;
                  for (let j = 0 ; j < solicitudEstados.length ; j++) {
                    let parSolicitudEstado = solicitudEstados[j].dataValues;
                    let transicionesEstado = asegurado.instancia_poliza.instancia_poliza_transicions.filter(param => param.dataValues.par_estado_id == parSolicitudEstado.id);
                    let transicionesDuplicadas = [];
                    let transicionesDiferentes = [];

                    let transicionPorPagar = {};
                    if (transicionesEstado.length) {
                      transicionPorPagar.observacion = 'La solicitud tiene algunas advertencias, sin embargo puede pasar a la siguiente instanciá.';
                    } else {
                      transicionPorPagar.observacion = 'La solicitud no tiene observaciones ni advertencias, por lo que se la pasará a la siguiente instanciá.';
                    }
                    transicionPorPagar.par_observacion_id = parObservacionSuccess.id;
                    transicionPorPagar.id_instancia_poliza = asegurado.instancia_poliza.id;
                    transicionPorPagar.par_estado_id = parEstadoPorPagar.id;

                    let transicionEmitido = {};
                    transicionEmitido.par_observacion_id = parObservacionSuccess.id;
                    transicionEmitido.id_instancia_poliza = asegurado.instancia_poliza.id;
                    transicionEmitido.par_estado_id = parEstadoEmitido.id;

                    let transicionEmitidoFecha = {};
                    if (transicionesEstado.length) {
                      transicionEmitidoFecha.observacion = 'La solicitud tiene algunas advertencias, sin embargo puede pasar a la siguiente instanciá.';
                    } else {
                      transicionEmitidoFecha.observacion = 'La solicitud no tiene observaciones ni advertencias, por lo que se la pasará a la siguiente instanciá.';
                    }
                    transicionEmitidoFecha.par_observacion_id = parObservacionSuccess.id;
                    transicionEmitidoFecha.id_instancia_poliza = asegurado.instancia_poliza.id;
                    transicionEmitidoFecha.par_estado_id = parEstadoEmitido.id;

                    for (let k = 0 ; k < transicionesEstado.length ; k++) {
                        let transicionEstado = transicionesEstado[k].dataValues;
                        if(!transicionesDiferentes.isInArray(transicionEstado.observacion)) {
                          transicionesDiferentes.push(transicionEstado.observacion);
                        } else {
                          transicionesDuplicadas.push(transicionEstado);
                        }
                        if (parEstadoPorPagar) {
                            if(transicionEstado.par_estado_id == parEstadoPorPagar.id) {
                              let respTransicionEstadoPorpagar = await modelos.instancia_poliza_transicion.destroy({where:{id:transicionEstado.id}});
                              response.data[transicionEstado.id_instancia_poliza] = transicionEstado;
                              let oldTransicionPorPagar = await modelos.instancia_poliza_transicion.findAll({
                                where:{
                                  observacion:{$like: transicionPorPagar.observacion},
                                  par_estado_id:transicionPorPagar.par_estado_id
                                }
                              });
                              if(!oldTransicionPorPagar.length) {
                                transicionPorPagar.modificado_por = transicionEstado.modificado_por;
                                transicionPorPagar.adicionado_por = transicionEstado.adicionado_por;
                                let respTransicionEstadoPorpagar = await modelos.instancia_poliza_transicion.create(transicionPorPagar);
                                response.data[transicionEstado.id_instancia_poliza] = transicionPorPagar;
                              }
                            }
                        }
                      if (parEstadoEmitido) {
                        if(transicionEstado.par_estado_id == parEstadoEmitido.id) {
                          let respTransicionEmitido = await modelos.instancia_poliza_transicion.destroy({where:{id:transicionEstado.id}});
                          response.data[transicionEstado.id_instancia_poliza] = transicionEstado;
                          let oldTransicionEmitido = await modelos.instancia_poliza_transicion.findAll({
                            where:{
                              observacion: {$like:transicionEmitido.observacion},
                              par_estado_id:transicionEmitido.par_estado_id
                            }
                          });
                          let insDocEmision = asegurado.instancia_poliza.instancia_documentos.find(param => param.dataValues.id_documento == docEmision.id);
                          if(!oldTransicionEmitido.length && insDocEmision) {
                            let dateEmision = insDocEmision.dataValues.fecha_emision;
                            let strDate = dateEmision.getDate() + '/' + (dateEmision.getMonth()+1) + '/' + dateEmision.getFullYear();
                            transicionEmitido.observacion = 'La fecha de emisión del certificado de seguro sera la siguiente: ' + strDate;
                            transicionEmitido.modificado_por = transicionEstado.modificado_por;
                            transicionEmitido.adicionado_por = transicionEstado.adicionado_por;
                            let respTransicionEstadoEmitido = await modelos.instancia_poliza_transicion.create(transicionEmitido);
                            response.data[transicionEstado.id_instancia_poliza] = transicionEmitido;
                          }
                        }
                      }
                    }
                    if(transicionesDuplicadas.length) {
                      for (let k = 0 ; k < transicionesDuplicadas.length ; k++) {
                          let transicionDuplicada = transicionesDuplicadas[k];
                          let respTransicionDuplicada = await modelos.instancia_poliza_transicion.destroy({where:{id:transicionDuplicada.id}});
                          // let respTransicionInstanciaPoliza = await modelos.atributo_instancia_poliza.update({tipo_error:'eliminar'},{where:{id:atributo.id}});
                          response.data[transicionDuplicada.id_instancia_poliza] = transicionDuplicada.dataValues;
                      }
                    }
                  }
                }
            }
            console.log(response.data);
        });
        res.json(response.data);
    } catch (e) {
        console.log(e);
    }
});

async function FindPersonaSolicitudByIdPoliza(idPoliza, callback) {
  let response = { status: 'OK', message: '', data: {} };
  await modelos.asegurado.findAll({
    include:[
      {
        model:modelos.entidad,
        include:[
          {model:modelos.persona}
        ]
      },
      {
        model:modelos.instancia_poliza,
        where:{id_poliza: idPoliza},
        include:[
          {
            model:modelos.atributo_instancia_poliza,
            include:[
              {
                model: modelos.objeto_x_atributo,
                include: [
                  {
                    model: modelos.atributo,
                    // where: {id:{$notIn:[59,60]}}
                  }
                ]
              },
            ]
          }, {
            model:modelos.usuario
          }
        ]
      }
    ]
  })
    .then(async resp => {
      response.data = resp;
    })
    .catch(err => {
      response.status = 'ERROR';
      response.message = err;
    });
  await callback(response.data);
}

async function findTrancisionesSolicitudByIdPoliza(idPoliza, callback) {
  let response = { status: 'OK', message: '', data: {} };
  await modelos.asegurado.findAll({
    include:[
      {
        model:modelos.instancia_poliza,
        where:{id_poliza: idPoliza},
        include:[
         {
            model: modelos.instancia_poliza_transicion,
            include: [
                {
                  model: modelos.parametro,
                    as: 'par_estado'
                },{
                  model: modelos.parametro,
                    as: 'par_observacion'
                }
            ]
          }, {
            model:modelos.instancia_documento
          }
        ]
      }
    ]
  })
    .then(async resp => {
      response.data = resp;
    })
    .catch(err => {
      response.status = 'ERROR';
      response.message = err;
    });
  await callback(response.data);
}

String.prototype.isLetter = function() {
    return this.match(/[a-z]/i);
}

Array.prototype.isInArray = function(value) {
  return this.indexOf(value) > -1;
}
module.exports = router;
