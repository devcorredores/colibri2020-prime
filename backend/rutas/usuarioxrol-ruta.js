const { Router } = require('express');
const router = new Router();
const bcrypt = require('bcrypt-nodejs');
const modelos = require("../modelos");

const autentificacion = require('../modulos/passport');

router.get('/usuarios', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.usuario.findAll({ attributes: ['id', 'usuario_login', 'usuario_nombre_completo', 'usuario_email', 'usuario_img', 'par_estado_usuario_id', 'par_estado_usuario_id'] })
        .then(usuarios => {
            response.data = usuarios;
        }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/authenticate', autentificacion.auth(), (req, res) => {
    res.status(200).json({ "statusCode": 200, "user": req.user });
});

var isLoggedIn = (req, res, next) => {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
}
router.post('/register', (req, res) => {
    // res.status(200).json({"statusCode" : 200 ,"user" : req.user});
    let response = { status: 'OK', message: '', data: '' };
    const usuario_login = req.body.usuario_login;
    const usuario_password = bcrypt.hashSync(req.body.usuario_password, null, null);
    const usuario_nombre_completo = req.body.usuario_nombre_completo;
    const usuario_email = req.body.usuario_email;
    const par_estado_usuario_id = req.body.par_estado_usuario_id;
    modelos.usuario.find({ where: { usuario_login } }).then(usuarios => {
        response.data = usuarios;
        if (response.data) {
            return res.status(401).send({ message: 'Email de usuario duplicado, este email ya existe en la base de datos' })
        } else {
            modelos.usuario.create({ usuario_login, usuario_password, usuario_nombre_completo, usuario_email , par_estado_usuario_id})
                .then(usuario => {
                    response.data = usuario;
                    response.message = 'Registro creado';
                    return res.status(201).send({ message: 'Usuario creado' })
                })
                .catch((err) => {
                    response.status = 'ERROR';
                    response.message = err;
                    return res.status(201).send({ message: 'Usuario no creado' })
                });
        }
    }
    ).catch((err) => {
        response.status = 'ERROR';
        response.message = err;
        // return done(null, false, req.flash('signupMessage', err.message));
    });
});

router.put('/modifica', (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    // const id = req.params.id;
    const id = req.body.id;
    console.log('req.body modifica', req.body)
    modelos.usuario.findById(id)
        .then(usuario => {
            usuario.update(req.body);
            response.message = 'Registro actualizado';
            res.json(response);
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
            res.json(response);
        });
});

router.post('/restore', isLoggedIn, (req, res) => {
    const usuario_login = req.body.username;
    let response = { status: 'OK', message: '', data: '' };
    if (req.body.password != req.body.password2) {
        return res.status(301).send({ message: 'La contraseña nueva y su repeticion no coinciden.' });
        // return done(null, false, req.flash('restoreMessage', 'La contraseña nueva y su repeticion no coinciden.'));
    } else {
        modelos.usuario.find({ where: { usuario_login } }).then(usuariosPwd => {
            response.data = usuariosPwd.dataValues;
            if (response.data) {
                const salt = bcrypt.hashSync(req.body.password, null, null);
                usuariosPwd.update({ login, salt })
                    .then(usuario => {
                        response.data = usuario.dataValues;
                        response.message = 'Contraseña actualizada.';
                        return res.status(200).send({ message: 'Contraseña actualizada.' });
                        // return done(null, response.data, req.flash('loginMessage', 'Contraseña actualizada.'));
                    }).catch((err) => {
                        response.status = 'ERROR';
                        response.message = 'Error en actualizar al localizar usuario, reintente.';
                        return res.status(401).send({ message: 'Error en actualizar al localizar usuario, reintente.' });
                        // return done(err, false, req.flash('restoreMessage', 'Error en actualizar al localizar usuario, reintente.'));
                    });
            } else {
                response.status = 'ERROR';
                response.message = 'Este usuario no esta registrado.';
                return res.status(300).send({ message: 'Este usuario no esta registrado.' });
                // return done(null, false, req.flash('restoreMessage', 'Este usuario no esta registrado.'));
            }
        }).catch((err) => {
            response.status = 'ERROR';
            response.message = 'Error al localizar usuario, reintente.';
            return res.status(401).send({ message: 'Error al localizar usuario, reintente.' });
            // return done(err, false, req.flash('restoreMessage', 'Error al localizar usuario, reintente.'));
        });
    }
});


router.get('/prueba', isLoggedIn, (req, res) => {
    res.send({ 'mensaje': 'hola mundo' });
});


router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

module.exports = router;
