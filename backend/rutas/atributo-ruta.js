const { Router } = require('express');
const router = new Router();
const modelos = require("../modelos");
const solicitudService = require("../servicios/solicitud.service");
const isLoggedIn = solicitudService.isLoggedIn;
const Util = require("../utils/util");
const util = new Util();
const AtributoService = require("../servicios/atributo.service");

// var isLoggedIn = (req, res, next) => {
//     console.log('session ', req.session);
//     if (req.isAuthenticated()) {
//     }
//         return next()
//     //return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
// }

router.get('/listaAtributosByIdPoliza/:id',async(req, res) => {
    try {
        const id_poliza = req.params.id;
        let objData = await AtributoService.listaAtributosByIdPoliza(id_poliza);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/getAllAtributosByIdObjeto/:idsObjetos',async(req, res) => {
    try {
        let idsObjetos = req.params.idsObjetos.split(',');
        let objData = await AtributoService.getAllAtributosByIdObjeto(idsObjetos);
        if (objData) {
            util.setSuccess(200, "datos atributos encontrados", objData);
        } else {
            util.setError(404, `datos atributos no encontrados`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});


router.get('/getAllAtributosByIdObjetoEditablesYVisibles/:idObjeto/:idEditable/:idVisible', isLoggedIn, async(req, res) => {
    try {
        let idObjeto = req.params.idObjeto;
        let idEditable = req.params.idEditable;
        let idVisible = req.params.idVisible;
        let objData = await AtributoService.findAtributosByIdObjetoEditablesYVisibles(idObjeto,idEditable,idVisible);
        if (objData) {
            util.setSuccess(200, "datos objeto", objData);
        } else {
            util.setError(404, `datos objeto no retrivied`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/getAllAtributosByObjetoIdInstanciaPolizaEditablesYVisibles/:idObjeto/:idInstanciaPoliza/:idEditable/:idVisible', isLoggedIn, async(req, res) => {
    try {
        let idObjeto = req.params.idObjeto;
        let idInstanciaPoliza = req.params.idInstanciaPoliza;
        let idEditable = req.params.idEditable;
        let idVisible = req.params.idVisible;
        let objData = await AtributoService.findAtributosByIdInstanciaPolizaEditablesYVisibles(idObjeto,idInstanciaPoliza,idEditable,idVisible);
        if (objData) {
            util.setSuccess(200, "data retrivied", objData);
        } else {
            util.setError(404, `no data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/getAllAtributosByObjetoIdInstanciaPoliza/:idObjeto/:idInstanciaPoliza', isLoggedIn, async(req, res) => {
    try {
        let idObjeto = req.params.idObjeto;
        let objData = await AtributoService.findAtributosByIdInstanciaPolizaEditablesYVisibles(idObjeto);
        if (objData) {
            util.setSuccess(200, "data retrivied", objData);
        } else {
            util.setError(404, `no data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/getAllAtributosByObjeto/:idObjeto', isLoggedIn, async(req, res) => {
    try {
        let idObjeto = req.params.idObjeto;
        let objData = await AtributoService.getAllAtributosByIdObjeto(idObjeto);
        if (objData) {
            util.setSuccess(200, "data retrivied", objData);
        } else {
            util.setError(404, `no data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;
