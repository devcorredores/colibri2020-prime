const { Router } = require('express');
const router = new Router();
const Util = require("../utils/util");
const util = new Util();
const BeneficiarioService = require("../servicios/beneficiario.service");
const autentificacion = require('../modulos/passport');

router.get('/usuarios', async (req, res) => {
    // let response = { status: 'OK', message: '', data: '' };
    // await modelos.usuario.findAll({ attributes: ['id', 'usuario_login', 'usuario_nombre_completo', 'usuario_email', 'usuario_img', 'par_estado_usuario_id', 'par_estado_usuario_id'] })
    //     .then(usuarios => {
    //         response.data = usuarios;
    //     }
    //     ).catch((err) => {
    //         response.status = 'ERROR';
    //         response.message = err;
    //     });
    // res.json(response);


    /* PROPUESTA
    let response = { status: 'OK', message: '', data: [] };
    await modelos.usuario.findAll()
        .then(async usuarios => {
            response.data = usuarios;
            await  response.data.forEach(async (element,index) => {
                let id_usuario = element.id
                await modelos.usuario_x_rol.findAll({ where: { id_usuario } })
                    .then(usuariosxrol => {
                        element.roles = usuariosxrol;
                    }
                    ).catch((err) => {
                        response.status = 'ERROR';
                        response.message = err;
                    });
                    if (index===response.data.length-1){
                        res.json(response);
                    }
            });
            // await response.data.forEach(async element => {
            //     let usuario_id = element.id;
            //     await modelos.autenticacion_usuario.findAll({ where: { usuario_id } })
            //         .then(usuariosxaut => {
            //             element.autentificaciones = usuariosxaut;
            //         }
            //         ).catch((err) => {
            //             response.status = 'ERROR';
            //             response.message = err;
            //         });
            //     // res.json(response);
            // });

        }).catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    END PROPUESTA */

    /* PROPUESTA 2 */
    try {
        let objData = await BeneficiarioService.usuarios();
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
    /* END PROPUESTA 2 */
});

router.post('/authenticate', autentificacion.auth(), (req, res) => {
    res.status(200).json({ "statusCode": 200, "user": req.user });
});

var isLoggedIn = (req, res, next) => {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
}

router.post('/register',async (req, res) => {
    try {
        let objData = await BeneficiarioService.register();
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/:id',async (req, res) => {
    try {
        let id = req.params.id;
        let objData = await BeneficiarioService.getById(id);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
})

router.get('/byId_beneficiario/:id',async (req, res) => {
    try {
        let id_beneficiario = req.params.id;
        let objData = await BeneficiarioService.byId_beneficiario(id_beneficiario);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.delete('/eliminar/:id', async (req, res) => {
    try {
        let id = req.params.id;
        let objData = await BeneficiarioService.eliminar(id);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
})


router.put('/modificar', async (req, res) => {
    try {
        let data = req.body;
        let objData = await BeneficiarioService.modificar(data);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/restore', isLoggedIn, async (req, res) => {
    try {
        let usuario_login = req.body.username;
        let objData = await BeneficiarioService.restore(usuario_login);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/GetDatosAdicionalesPersona/:id', isLoggedIn, async (req, res) => {
    try {
        let id_persona = req.params.id;
        let objData = await BeneficiarioService.getDatosAdicionalesPersona(id_persona);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/migrarBeneficiariosDesgravamen', isLoggedIn, async (req, res) => {
    try {
        let idInstanciaPoliza = req.body.idInstanciaPoliza;
        let nroSolicitudSci = req.body.nroSolicitudSci;
        let docId = req.body.docId;
        let docIdExt = req.body.docIdExt;
        let objSolicitud = await BeneficiarioService.getDatosDeudor(nroSolicitudSci,docId,docIdExt);
        let objData = await BeneficiarioService.createBeneficiariosFromSolicitud(objSolicitud,idInstanciaPoliza);
        if (objData) {
            util.setSuccess(200, "beneficiarios migrados", objData);
        } else {
            util.setError(404, `no se pudo migrar los beneficiarios`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/verificaBeneficiariosDesgravamen', isLoggedIn, async (req, res) => {
    try {
        let nroSolicitudSci = req.body.nroSolicitudSci;
        let docId = req.body.docId;
        let docIdExt = req.body.docIdExt;
        let objData = await BeneficiarioService.getDatosDeudor(nroSolicitudSci,docId,docIdExt);
        if (objData) {
            util.setSuccess(200, "beneficiarios encontrados", objData);
        } else {
            util.setWarning(200, `no se pudo encontrar ningun beneficiario`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/AdicionarPersonaCompleto', async (req, res) => {
    try {
        let datos_adicionales = [{ id_persona: 1 }];
        let persona = { id: 1, persona_datos_adicionales: datos_adicionales };
        persona = req.body;
        let objData = await BeneficiarioService.adicionarPersonaCompleto(persona);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

module.exports = router;
