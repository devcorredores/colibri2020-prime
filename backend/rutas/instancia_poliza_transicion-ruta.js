const { Router } = require('express');
const router = new Router();
const bcrypt = require('bcrypt-nodejs');
const modelos = require("../modelos");
const moment = require("moment");

var isLoggedIn = (req, res, next) => {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

router.post('/registerObservacion',  async (req, res) => {
    try {
        let response = {status: 'OK', message: '', data: ''};
        let {newObservationValue, idInstanciaPoliza, idEstado} = req.body;
        let updatedInstanciaPolizaTransicion = await modelos.instancia_poliza_transicion.update({observacion:newObservationValue}, {where:{id_instancia_poliza:idInstanciaPoliza, par_estado_id:idEstado}});
        let instanciaPolizaTransicion = await await modelos.instancia_poliza_transicion.findOne({where:{id_instancia_poliza:idInstanciaPoliza, par_estado_id:idEstado}});
        response.data = instanciaPolizaTransicion;
        response.status = 'OK';
        res.json(response);
    } catch (e) {
        console.log(e);
    }
});
router.post('/register',  async (req, res) => {
    try {
        let response = { status: 'OK', message: '', data: '' };
        let {id_instancia_poliza,id_poliza,par_estado_id,id_motivo,observacion,adicionado_por} = req.body;
        let updatednstanciaPoliza = await modelos.instancia_poliza.update({id_estado:par_estado_id},{where:{id:id_instancia_poliza}});
        let asegurado = await modelos.asegurado.find({
            include: [{
                model:modelos.instancia_poliza,
                where:{id:id_instancia_poliza},
                include: [{
                   model: modelos.atributo_instancia_poliza,
                   include:{
                       model:modelos.objeto_x_atributo,
                       include: {
                           model: modelos.atributo
                       }
                   }
                }, {
                    model: modelos.instancia_documento,
                    include: {
                        model: modelos.documento
                    }
                },{
                    model:modelos.instancia_poliza_transicion,
                },{
                    model:modelos.poliza,
                },{
                    model:modelos.parametro,
                    as:'estado'
                }]
            }, {
                model: modelos.entidad,
                include: {
                    model:modelos.persona
                }
            }]
        });
        let poliza = await modelos.poliza.findOne({
            include: [
                {
                    model: modelos.objeto,
                    where:{id_poliza:id_poliza, id_tipo: 298},
                    include: {
                        model:modelos.objeto_x_atributo,
                        include: {
                            model: modelos.atributo
                        }
                    }
                },{
                    model: modelos.anexo_poliza,
                    where: {vigente:true}
                }
            ]
        });
        let documentoDesistimiento = await modelos.documento.findOne({
            where:{id_tipo_documento:348, id_poliza:asegurado.instancia_poliza.id_poliza},
            includes:{
                model:modelos.atributo_instancia_documento
            }
        });
        if (documentoDesistimiento) {
            let nroTotalMeses;
            let instanciaDocumentoCertificado = asegurado.instancia_poliza.instancia_documentos.find(param => param.documento.id_tipo_documento == 281);
            // let anexoPoliza = poliza.anexo_polizas.find(param => param.id_tipo == 288 && param.vigente == true);
            let anexoPoliza = await modelos.anexo_poliza.findOne({where:{id: asegurado.instancia_poliza.id_anexo_poliza}});
            let fechaInicioVigencia,fechaFinVigencia,periodoMesesResarsimiento,periodoMesesResarsimientoRounded,periodoMesesResarsimientoDecimals,montoPrimaResarcimiento = 0,fechaDesistimiento;
            let nroMesesTotalVigencia,nroMeses,nextDateAniversary,lastDateAniversary;
            if (instanciaDocumentoCertificado && anexoPoliza) {
                fechaDesistimiento = moment(new Date());
                fechaInicioVigencia = moment(instanciaDocumentoCertificado.fecha_inicio_vigencia);
                fechaFinVigencia = moment(instanciaDocumentoCertificado.fecha_fin_vigencia);
                nroTotalMeses = fechaFinVigencia.diff(fechaInicioVigencia,'months', true);
                periodoMesesResarsimiento = fechaFinVigencia.diff(fechaDesistimiento,'months', true);
                periodoMesesResarsimientoRounded = Math.round(periodoMesesResarsimiento);
                periodoMesesResarsimientoDecimals = periodoMesesResarsimiento - Math.floor(periodoMesesResarsimiento);
                if (periodoMesesResarsimientoDecimals > 0 && periodoMesesResarsimientoDecimals < 0.5) {
                    periodoMesesResarsimientoRounded += 1;
                }
                for (let i = 0; i < nroTotalMeses; i++) {
                    let nextPeriodo = fechaInicioVigencia.add(1,'months');
                    let diffNextPeriodoAnulacion = fechaDesistimiento.diff(nextPeriodo,'months', true);
                    if (!lastDateAniversary && diffNextPeriodoAnulacion < 1) {
                        lastDateAniversary = moment(nextPeriodo.toDate());
                        nextDateAniversary = moment(nextPeriodo.add(1,'months').toDate());
                        break;
                    }
                }
                periodoMesesResarsimientoRounded = fechaDesistimiento.isSameOrAfter(lastDateAniversary) ? periodoMesesResarsimientoRounded - 1 : periodoMesesResarsimientoRounded;
                montoPrimaResarcimiento = periodoMesesResarsimientoRounded * parseInt(anexoPoliza.monto_prima)
            }
            if (par_estado_id == 62) {
                await modelos.documento.update({nro_actual:parseInt(documentoDesistimiento.nro_actual)+1},{where:{id:documentoDesistimiento.id}});
                let oldInstanciaDocumento = await modelos.instancia_documento.findOne(
                    {
                        where:{id_instancia_poliza:asegurado.id_instancia_poliza,id_documento:documentoDesistimiento.id},
                        include:[
                            {
                                model:modelos.atributo_instancia_documento
                            }, {
                                model:modelos.documento
                            }
                        ]
                });
                let instanciaDocDesistimiento;
                if(oldInstanciaDocumento) {
                    instanciaDocDesistimiento = oldInstanciaDocumento;
                } else {
                    instanciaDocDesistimiento = await modelos.instancia_documento.create({
                        id_instancia_poliza:asegurado.id_instancia_poliza,
                        id_documento:documentoDesistimiento.id,
                        nro_documento:documentoDesistimiento.nro_actual,
                        fecha_emision:new Date(),
                        fecha_inicio_vigencia:fechaDesistimiento.isSameOrAfter(lastDateAniversary) ? nextDateAniversary.toDate() : lastDateAniversary.toDate(),
                        fecha_fin_vigencia:fechaFinVigencia.toDate(),
                        asegurado_doc_id:asegurado.entidad.persona.persona_doc_id,
                        asegurado_nombre1:asegurado.entidad.persona.persona_primer_nombre,
                        asegurado_nombre2:asegurado.entidad.persona.persona_segundo_nombre,
                        asegurado_apellido1:asegurado.entidad.persona.persona_primer_apellido,
                        asegurado_apellido2:asegurado.entidad.persona.persona_segundo_apellido,
                        asegurado_apellido3:asegurado.entidad.persona.persona_apellido_casada,
                        tipo_persona:'',
                        adicionada_por:adicionado_por,
                        modificada_por:adicionado_por,
                        createdAt:new Date(),
                        updatedAt:new Date(),
                        nombre_archivo:'',
                        id_documento_version:documentoDesistimiento.id_documento_version
                    });
                    instanciaDocDesistimiento.dataValues.documento = documentoDesistimiento;
                    instanciaDocDesistimiento.atributo_instancia_documentos = [];
                }
                let objetoDocumento = await modelos.objeto.findOne({
                    where:{id_poliza:poliza.id,id_tipo:299},
                    include: {
                        model:modelos.objeto_x_atributo,
                        include: {
                            model: modelos.atributo
                        }
                    }
                });
                let objetoXAtributoMotivo = objetoDocumento ? objetoDocumento.objeto_x_atributos.find(param => param.id_atributo == 94) : null;
                let objetoXAtributoPrimaDevuelta = objetoDocumento ? objetoDocumento.objeto_x_atributos.find(param => param.id_atributo == 95) : null;
                let newAtributoInstanciaDocumentoPrimaDevuelta, newAtributoInstanciaDocumentoMotivo;
                let atributo_instancia_documento_prima_devuelta, atributo_instancia_documento_motivo;
                if (objetoXAtributoMotivo) {
                    atributo_instancia_documento_motivo = {
                        id_instancia_documento:instanciaDocDesistimiento.id,
                        id_objeto_x_atributo:objetoXAtributoMotivo.id,
                        valor:id_motivo,
                        tipo_error: 'La instancia documento no tiene registrado el motivo de la anulacion/desistimiento',
                        adicionado_por:adicionado_por,
                        modificado_por:adicionado_por
                    };
                    newAtributoInstanciaDocumentoMotivo = await modelos.atributo_instancia_documento.create(atributo_instancia_documento_motivo);
                    instanciaDocDesistimiento.atributo_instancia_documentos.push(newAtributoInstanciaDocumentoMotivo);
                }
                if (objetoXAtributoPrimaDevuelta) {
                    atributo_instancia_documento_prima_devuelta = {
                        id_instancia_documento:instanciaDocDesistimiento.id,
                        id_objeto_x_atributo:objetoXAtributoPrimaDevuelta.id,
                        valor:montoPrimaResarcimiento ? montoPrimaResarcimiento : '',
                        tipo_error: 'La Instancia documento no tiene registrado el monto de la prima a devolver',
                        adicionado_por:adicionado_por,
                        modificado_por:adicionado_por
                    };
                    newAtributoInstanciaDocumentoPrimaDevuelta = await modelos.atributo_instancia_documento.create(atributo_instancia_documento_prima_devuelta);
                    instanciaDocDesistimiento.atributo_instancia_documentos.push(newAtributoInstanciaDocumentoPrimaDevuelta);
                }
                asegurado.instancia_poliza.instancia_documentos.push(instanciaDocDesistimiento);
            }
        }
        let instancia_poliza_transicion = {
            id_instancia_poliza: id_instancia_poliza,
            par_estado_id: par_estado_id,
            par_observacion_id: 305,
            observacion: observacion,
            adicionado_por:adicionado_por,
            modificado_por:adicionado_por
        };
        let newInstanciaPolizaTransicion = await modelos.instancia_poliza_transicion.create(instancia_poliza_transicion);
        asegurado.instancia_poliza.instancia_poliza_transicions.push(newInstanciaPolizaTransicion);
        response.data = asegurado;
        response.status = 'OK';
        res.json(response);
    } catch (e) {
        console.log(e)
    }
});

router.post('/saveOrUpdateTransition',  async (req, res) => {
    let response = { status: 'OK', message: '', data: [] };
    let id_usuario = req.session.passport.user;
    let usuario = null;
    await modelos.usuario.findOne({ where: { id: id_usuario } })
        .then(async resp => {
            if (Object.keys(resp.dataValues).length) {
                usuario = resp.dataValues;
            }
        })
        .catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    let new_instancia_poliza_transicions = req.body;
    if(new_instancia_poliza_transicions.length && usuario) {
        let old_instancia_poliza_transicions = await modelos.instancia_poliza_transicion.findAll({where:{id_instancia_poliza:new_instancia_poliza_transicions[0].id_instancia_poliza}});
        for (let index = 0 ; index < new_instancia_poliza_transicions.length ; index++) {
            let instancia_poliza_transicion = new_instancia_poliza_transicions[index];
            if(instancia_poliza_transicion.par_estado_id == new_instancia_poliza.id_estado) {
                let soloObservacion = instancia_poliza_transicion.observacion.split('La persona').join('').split('El titular').join('').trim();
                let oldInstanciaPolizaTransicion = old_instancia_poliza_transicions.find(param => param.dataValues.observacion.includes(soloObservacion));
                if (instancia_poliza_transicion.id) {
                    await updateTransicion(instancia_poliza_transicion, usuario);
                } if(oldInstanciaPolizaTransicion && oldInstanciaPolizaTransicion.id) {
                    instancia_poliza_transicion.id = oldInstanciaPolizaTransicion.id;
                    await updateTransicion(instancia_poliza_transicion, usuario);
                } else {
                    await createTransicion(instancia_poliza_transicion, usuario);
                }
            }
        }
        let instancia_poliza_transicions = await modelos.instancia_poliza_transicion.findAll({where:{id_instancia_poliza: new_instancia_poliza.id}}).then(async resp => {
            response.data = resp;
        }).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    }
    res.json(response);
});

async function updateTransicion(instancia_poliza_transicion, usuario) {
    let hoy = new Date();
    let modifiedAt = new Date();
    instancia_poliza_transicion.modificado_por = usuario != null ? usuario.id : '2';
    instancia_poliza_transicion.updatedAt = modifiedAt;
    await modelos.instancia_poliza_transicion.update(instancia_poliza_transicion, {
        where: {
            id: instancia_poliza_transicion.id,
            par_estado_id:instancia_poliza_transicion.par_estado_id
        }
    }).catch(err => {
        response.status = 'ERROR';
        response.message = err;
    });
    return instancia_poliza_transicion;
}

async function createTransicion(instancia_poliza_transicion, usuario) {
    let hoy = new Date();
    let createdAt = new Date();
    let newInstanciaPolizaTransaccion = {
        id_instancia_poliza: instancia_poliza_transicion.id_instancia_poliza,
        observacion:instancia_poliza_transicion.observacion,
        par_observacion_id:instancia_poliza_transicion.par_observacion_id,
        par_estado_id: instancia_poliza_transicion.par_estado_id,
        adicionado_por: usuario != null ? usuario.id : '2',
        modificado_por: usuario != null ? usuario.id : '2',
        updatedAt: createdAt,
        createdAt: createdAt
    };
    let oldInstanciaTransicion = await modelos.instancia_poliza_transicion.find({where:{
            par_estado_id:instancia_poliza_transicion.par_estado_id,
            observacion:instancia_poliza_transicion.observacion,
            id:instancia_poliza_transicion.id,
        }}).then(resp => {
        if(resp) {
            return resp.dataValues;
        }
    }).catch(err => {
        response.status = 'ERROR';
        response.message = err;
    });

    if(oldInstanciaTransicion) {
        oldInstanciaTransicion.par_observacion_id = instancia_poliza_transicion.par_observacion_id;
        updateTransicion((oldInstanciaTransicion))
    } else {
        await modelos.instancia_poliza_transicion.create(newInstanciaPolizaTransaccion).then(async resp => {
            instancia_poliza_transicion = resp.dataValues;
        }).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    }
    return instancia_poliza_transicion;
}

module.exports = router;
