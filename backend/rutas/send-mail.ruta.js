const { Router } = require("express");
const router = new Router();
const modelos = require("../modelos");
const sendMail = require("../modulos/send-mail");
const sendMail2 = require("../modulos/send-mail2");
const sendMailService = require('../servicios/sendMail.service');
const transporter = require("../modulos/send-mail3");
const {loggerEmail} = require("../modulos/winston");
// const sql = require('mssql');

// /api-corredores-ecofuturo/send-email
router.get("/", async (req, res) => {
  let response = {
    status: "OK",
    message: "send-mail",
    data: "Enviar por el metodo POST parametros : para,asunto, mensaje y cc"
  };
  res.json(response);
});

router.post("/", async (req, res) => {
  let response = { status: "OK", message: "", data: "" };
  const { para, asunto, mensaje } = req.body;

  const mailOptions = {
    to: para,
    subject: asunto,
    html: mensaje
  };

  const resp = await sendMail.sendMail(mailOptions);
  response.message = resp;

  res.json(response);
});

router.post("/:id", async (req, res) => {
  let response = { status: "OK", message: "", data: "" };
  try {
    const { id } = req.params;
    const { parametros } = req.body;
    const { dirFileExt } = req.body;
    let configCorreo = await sendMailService.getConfigCorreo(id);
    if (parametros && parametros.length) {
      parametros.forEach(element => {
        configCorreo.para = configCorreo.para && configCorreo.para.hasString(element.nombre) ? configCorreo.para.replaceAll(element.nombre, element.valor) : configCorreo.para;
        configCorreo.asunto = configCorreo.asunto && configCorreo.asunto.hasString(element.nombre) ? configCorreo.asunto.replaceAll(element.nombre, element.valor) : configCorreo.asunto;
        configCorreo.texto = configCorreo.texto && configCorreo.texto.hasString(element.nombre) ? configCorreo.texto.replaceAll(element.nombre, element.valor) : configCorreo.texto;
        configCorreo.html = configCorreo.html && configCorreo.html.hasString(element.nombre) ? configCorreo.html.replaceAll(element.nombre, element.valor) : configCorreo.html;
        configCorreo.con_copia = configCorreo.con_copia && configCorreo.con_copia.hasString(element.nombre) ? configCorreo.con_copia.replaceAll(element.nombre, element.valor) : configCorreo.con_copia;
        configCorreo.con_copia_oculta = configCorreo.con_copia_oculta && configCorreo.con_copia_oculta.hasString(element.nombre) ? configCorreo.con_copia_oculta.replaceAll(element.nombre, element.valor) : configCorreo.con_copia_oculta;
        configCorreo.nombre_archivo_adjunto = configCorreo.nombre_archivo_adjunto && configCorreo.nombre_archivo_adjunto.hasString(element.nombre) ? configCorreo.nombre_archivo_adjunto.replaceAll(element.nombre, element.valor) : configCorreo.nombre_archivo_adjunto;
        configCorreo.texto_vacio = configCorreo.texto_vacio && configCorreo.texto_vacio.hasString(element.nombre) ? configCorreo.texto_vacio.replaceAll(element.nombre, element.valor) : configCorreo.texto_vacio;
      });
    }
    let mailOptions;
    if (dirFileExt && configCorreo.nombre_archivo_adjunto) {
      mailOptions = {
        from: '"' + configCorreo.nombre_remitente + '" <' + configCorreo.correo_remitente + '>', // sender address
        html: configCorreo.html,
        to: configCorreo.para,
        cc: configCorreo.con_copia,
        bcc: configCorreo.con_copia_oculta,
        attachments: [
          {
            filename: configCorreo.nombre_archivo_adjunto,
            path: dirFileExt
          }
        ],
        subject: configCorreo.asunto,
        text: configCorreo.texto
      };
    } else {
      mailOptions = {
        from: '"' + configCorreo.nombre_remitente + '" <' + configCorreo.correo_remitente + '>', // sender address
        html: configCorreo.html,
        to: configCorreo.para,
        cc: configCorreo.con_copia,
        bcc: configCorreo.con_copia_oculta,
        subject: configCorreo.asunto,
        text: configCorreo.texto
      };
    }
    const resp = await transporter.sendMail(mailOptions, configCorreo.correo_remitente, configCorreo.contraseña_remitente,configCorreo.host,configCorreo.port);
    response.message = resp;
    loggerEmail.info('Correo enviado: ' + (new Date()).toString(), mailOptions);
    res.json(response);
  } catch (e) {
    console.log(e);
  }
});

router.post("/estado", async (req, res) => {
  let response = { status: "OK", message: "send-mail", data: "" };
  const {
    num_solicitud,
    estado,
    sucursal,
    oficial_creditos,
    email_oficial,
    garantia,
    cliente,
    nombre_usuario
  } = req.body;

  console.log('--------------------------------------------');
  console.log(req.body);
  console.log('--------------------------------------------');

  const mailOptions = {
    to: "",
    subject: "",
    html: "",
    cc: ""
  };

  const correos = [];

  await modelos.sucursal_supervisor
    .findAll({ where: { sucursal } })
    .then(resp => {
      if (resp) {
        //response.data = resp;
        resp.forEach(itm => {
          correos.push(itm.correo_supervisor);
          correos.push(itm.correo_cc1);
          correos.push(itm.correo_cc2);
        });
      }
    })
    .catch(err => {
      response.status = "ERROR";
      response.message = err;
    });

  response.data = correos.join(",");
  console.log('CC=====>', correos)

  // filtramos : SUCURSAL-SUPERVISOR
  switch (estado) {
    case "REVISION-SUPERVISOR":
      mailOptions.to = email_oficial;
      mailOptions.cc = correos.join(',');
      mailOptions.subject = `Envío de Solicitud para Revisión Supervisor – Solicitud ${num_solicitud} – Sucursal ${sucursal}`;
      mailOptions.html = `<br>
            El Oficial de Créditos ${oficial_creditos}, de la Sucursal ${sucursal}, ha generado la Solicitud de Seguro de Garantía, <br>
            para la garantía ${garantia}, del cliente ${cliente}.<br>
            El Supervisor debe ingresar al Sistema Colibri Garantías y proceder a la revisión correspondiente antes de la <br>
            aprobación de la operación.<br>
            La Solicitud de Seguro no se encuentra aun aprobada.<br>
            Nota: En esta instancia, el Oficial debe imprimir obligatoriamente el Formulario de Solicitud de Seguro del Sistema <br>
            Colibrí y hacer firmar al cliente.`;
      break;
    case "OBSERVADO-SUPERVISOR":
      mailOptions.to = email_oficial;
      mailOptions.cc = correos.join(',');
      mailOptions.subject = `Solicitud con observaciones del nivel de Revisión Supervisor – Solicitud ${num_solicitud} – Sucursal ${sucursal}`;
      mailOptions.html = `<br>
      El Supervisor ${nombre_usuario}, asignado a la Sucursal ${sucursal}, SI ha emitido observaciones a la Solicitud de Seguro de Garantía, <br>
      para la garantía ${garantia}.<br>
      El Oficial debe corregir o modificar la información observada y proceder nuevamente con el envío al nivel de Revisión Supervisor.<br>
      La Solicitud de Seguro no se encuentra aun aprobada.`;
      break;
    case "APROBADO-SUPERVISOR":
      mailOptions.to = email_oficial;
      mailOptions.cc = correos.join(',');
      mailOptions.subject = `Solicitud sin observaciones del nivel de Revisión Supervisor – Solicitud ${num_solicitud} – Sucursal ${sucursal}`;
      mailOptions.html = `<br>
      El Supervisor ${nombre_usuario}, asignado a la Sucursal ${sucursal}, NO ha emitido observaciones a la Solicitud de Seguro de Garantía, <br>
      para la garantía ${garantia}.<br>
      La Solicitud de Seguro se encuentra aprobada y el Oficial de Créditos puede proceder a la emisión e impresión del<br>
       Certificado de Cobertura para entrega al cliente`;
      break;
    case "EMITIDO":
      mailOptions.to = correos.join(",");
      mailOptions.subject = `Envío de Solicitud para Revisión Supervisor – Solicitud ${num_solicitud} – Sucursal ${sucursal}`;
      mailOptions.html = `<br>
                  El Oficial de Créditos ${oficial_creditos}, de la Sucursal ${sucursal}, ha generado la Solicitud de Seguro de Garantía, <br>
                  para la garantía TIPO ASFI ${tipo_garantia_asfi} + Descripción ASFI ${descripcion_garantia}, del cliente ${nombre} ${paterno}.<br>
                  El Supervisor debe ingresar al Sistema Colibri Garantías y proceder a la revisión correspondiente antes de la <br>
                  aprobación de la operación.<br>
                  La Solicitud de Seguro no se encuentra aun aprobada.<br>
                  Nota: En esta instancia, el Oficial debe imprimir obligatoriamente el Formulario de Solicitud de Seguro del Sistema <br>
                  Colibrí y hacer firmar al cliente.
                  `;
      break;
  }
  console.log('mailOptions ===>', mailOptions)
  sendMail.sendMail(mailOptions);

  res.json(response);
});

router.post("/Observaciones", async (req, res) => {
  let response = { status: "OK", message: "send-mail", data: "" };
  const {
    num_solicitud,
    estado,
    sucursal,
    oficial_creditos,
    email_oficial,
    garantia,
    cliente,
    nombre_usuario
  } = req.body;

  console.log('--------------------------------------------');
  console.log(req.body);
  console.log('--------------------------------------------');

  const mailOptions = {
    to: "",
    subject: "",
    html: "",
    cc: ""
  };

  const correos = [];

  await modelos.sucursal_supervisor
    .findAll({ where: { sucursal } })
    .then(resp => {
      if (resp) {
        //response.data = resp;
        resp.forEach(itm => {
          correos.push(itm.correo_supervisor);
          correos.push(itm.correo_cc1);
          correos.push(itm.correo_cc2);
        });
      }
    })
    .catch(err => {
      response.status = "ERROR";
      response.message = err;
    });

  response.data = correos.join(",");
  console.log('CC=====>', correos)

  // filtramos : SUCURSAL-SUPERVISOR
  switch (estado) {
    case "REVISION-SUPERVISOR":
      mailOptions.to = email_oficial;
      mailOptions.cc = correos.join(',');
      mailOptions.subject = `Envío de Solicitud para Revisión Supervisor – Solicitud ${num_solicitud} – Sucursal ${sucursal}`;
      mailOptions.html = `<br>
            El Oficial de Créditos ${oficial_creditos}, de la Sucursal ${sucursal}, ha generado la Solicitud de Seguro de Garantía, <br>
            para la garantía ${garantia}, del cliente ${cliente}.<br>
            El Supervisor debe ingresar al Sistema Colibri Garantías y proceder a la revisión correspondiente antes de la <br>
            aprobación de la operación.<br>
            La Solicitud de Seguro no se encuentra aun aprobada.<br>
            Nota: En esta instancia, el Oficial debe imprimir obligatoriamente el Formulario de Solicitud de Seguro del Sistema <br>
            Colibrí y hacer firmar al cliente.`;
      break;
    case "OBSERVADO-SUPERVISOR":
      mailOptions.to = email_oficial;
      mailOptions.cc = correos.join(',');
      mailOptions.subject = `Solicitud con observaciones del nivel de Revisión Supervisor – Solicitud ${num_solicitud} – Sucursal ${sucursal}`;
      mailOptions.html = `<br>
      El Supervisor ${nombre_usuario}, asignado a la Sucursal ${sucursal}, SI ha emitido observaciones a la Solicitud de Seguro de Garantía, <br>
      para la garantía ${garantia}.<br>
      El Oficial debe corregir o modificar la información observada y proceder nuevamente con el envío al nivel de Revisión Supervisor.<br>
      La Solicitud de Seguro no se encuentra aun aprobada.`;
      break;
    case "APROBADO-SUPERVISOR":
      mailOptions.to = email_oficial;
      mailOptions.cc = correos.join(',');
      mailOptions.subject = `Solicitud sin observaciones del nivel de Revisión Supervisor – Solicitud ${num_solicitud} – Sucursal ${sucursal}`;
      mailOptions.html = `<br>
      El Supervisor ${nombre_usuario}, asignado a la Sucursal ${sucursal}, NO ha emitido observaciones a la Solicitud de Seguro de Garantía, <br>
      para la garantía ${garantia}.<br>
      La Solicitud de Seguro se encuentra aprobada y el Oficial de Créditos puede proceder a la emisión e impresión del<br>
       Certificado de Cobertura para entrega al cliente`;
      break;
    case "EMITIDO":
      mailOptions.to = correos.join(",");
      mailOptions.subject = `Envío de Solicitud para Revisión Supervisor – Solicitud ${num_solicitud} – Sucursal ${sucursal}`;
      mailOptions.html = `<br>
                  El Oficial de Créditos ${oficial_creditos}, de la Sucursal ${sucursal}, ha generado la Solicitud de Seguro de Garantía, <br>
                  para la garantía TIPO ASFI ${tipo_garantia_asfi} + Descripción ASFI ${descripcion_garantia}, del cliente ${nombre} ${paterno}.<br>
                  El Supervisor debe ingresar al Sistema Colibri Garantías y proceder a la revisión correspondiente antes de la <br>
                  aprobación de la operación.<br>
                  La Solicitud de Seguro no se encuentra aun aprobada.<br>
                  Nota: En esta instancia, el Oficial debe imprimir obligatoriamente el Formulario de Solicitud de Seguro del Sistema <br>
                  Colibrí y hacer firmar al cliente.
                  `;
      break;
  }
  console.log('mailOptions ===>', mailOptions)
  sendMail.sendMail(mailOptions);

  res.json(response);
});

// router.post("/Observaciones2", async (req, res) => {
//   let response = { status: "OK", message: "send-mail", data: "" };
//   const {
//     num_solicitud,
//     name_user,
//     tipo_cambio,
//     valor,
//     nuevo_valor,
//     mensaje
//   } = req.body;

//   const mailOptions = {
//     to: "",
//     subject: "",
//     html: "",
//     cc: ""
//   };

//   const correos = [];

//   let response2 = { status: 'OK', message: '/api-corredores-ecofuturo/soporte-desgravamen/histosol', data: '' };
//   const nrosol = req.params.nrosol;
//   let cmd = `select  Sol_oficial,(select top 1 Usr_Mail from Seg_Usuarios where Usr_Codigo=Sol_CodOficial) email from Seg_Solicitudes where Sol_NumSol='${num_solicitud}'`;

//     //const config = await webconfig.getWebConfig();
//     const conn = webconfig.dbs.find(db => db.nombre === 'SOPORTE-DESGRAVAMEN' );

//     try {
//         await sql.connect(conn.valor)
//         const consultaSolicitudes = await sql.query(cmd);
//         response2.data = consultaSolicitudes.recordset;
//         response2.message = conn.descripcion;
//         } catch (err) {
//         console.log('ERROR:',err);
//     }
//     sql.close();

//   // filtramos : SUCURSAL-SUPERVISOR
//       let i=0
//       let copias=[];
//       let excluidos=[';eeguilos@corredoresecofuturo.com.bo'];
//       let correos2=response2.data[0].email;
//       let a='';

//       excluidos.forEach(element => {
//         correos2=correos2.replace(element,''); 
//       });

//       await correos2.split(';').forEach( correo=> {  
//         if (i===0){
//           mailOptions.to = correo;
//         }else{
//           copias.push(correo);
//         }
//         i++;
//       });
      
//       mailOptions.cc = copias.join(',');
//       mailOptions.subject = `Respuesta solicitud de Soporte de seguro de desgravamen  numero - ${num_solicitud}.`;
//       mailOptions.html = `Estimado(a) ${response2.data[0].Sol_oficial}:<br>
//       Se ha realizado   EL CAMBIO DE ${tipo_cambio} (de ${valor} a ${nuevo_valor}), en atención a su requerimiento de soporte para la solicitud ${num_solicitud}, de acuerdo a lo siguiente:<br><br>
//       ${mensaje}<br><br>
//       Saludos<br>`;

//   sendMail2.sendMail(mailOptions);

//   res.json(response);
// });

module.exports = router;
