const { Router } = require('express');
const router = new Router();
const invoke = require("../modulos/soap-request");
const request = require('request');
const soap = require('soap');
const webconfig = require('../config/web-config');
const to_json = require('xmljson').to_json;
const soapCtrl = require("../controladores/soap.controller");
const {soapExecute} = require("../modulos/soap");

const specialRequest = request.defaults({
  strictSSL: false
});

const soapHeaderXml = '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';

// router.get('/', async (req, res) => {
//   res.json({ status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' });
// });


router.post('/', async (req, res) => {
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };
  const { wsdl, operation, request } = req.body;

  if(! wsdl){
    wsdl = await webconfig.wssoap.find(soap => soap.nombre === 'WS-SOAP-ECOFUTURO');
  }

  soap.createClient(wsdl, { request: specialRequest }, (err, client) => {
    if (!err) {
      method = client[operation];
      client.addSoapHeader(soapHeaderXml);
      method(request, (err, result) => {
        if (!err) {
          response.data = result;
          res.json(response);
        } else {
          response.status = 'ERROR';
          response.message = 'Error: soap client, Server was unable to read request';
          res.json(response);
        }
      });
    } else {
      response.status = 'ERROR';
      response.message = 'Error : en comunicación soap';
      res.json(response);
    }

  });

});

router.get('/getCustomer/:doc_id/:ext', async (req, res) => {
  const doc_id = req.params.doc_id;
  const ext = req.params.ext;
  let responseSolicitud = { status: "OK", message: "", data: "" };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };

  responseSolicitud = await invoke.soapExecute(
  { doc_id: doc_id,
    ext: ext,
    usuario:'prueba',
    password:'prueba'},
  "getCustomer",
  "WS-SOAP-ECOFUTURO"
  );

  if (responseSolicitud.status === "OK") {
    if (responseSolicitud.data.getCustomerResult) {
      to_json(responseSolicitud.data.getCustomerResult, function (error, data) {
        response.data=data.DataSetAll.tmp_general;
      });
    } else {
      response.status = "ERROR";
      response.data = responseSolicitud.data.getSolicitudPropuestaResult;
    }
  } else {
    response = responseSolicitud;
  }
  res.json(response);
});

router.get('/getCustomerSol/:doc_id/:ext', async (req, res) => {
  const doc_id = req.params.doc_id;
  const ext = req.params.ext;
  let responseSolicitud = { status: "OK", message: "", data: {} };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: {} };

  responseSolicitud = await invoke.soapExecute(
  { doc_id: doc_id,
    ext: ext,
    usuario:'prueba',
    password:'prueba'},
  "getCustomerSol",
  "WS-SOAP-ECOFUTURO"
  );

  if (responseSolicitud.status === "OK") {
    if (responseSolicitud.data.getCustomerSolResult) {
      to_json(responseSolicitud.data.getCustomerSolResult, function (error, data) {
        response.data.general = data.DataSetAll.tmp_general;
	      response.data.solicitud = {solicitudes_sci:[]};
	      for (const property in data.DataSetAll.tmp_solicitud) {
	        if (typeof data.DataSetAll.tmp_solicitud[property] != 'function') {
	          if (data.DataSetAll.tmp_solicitud[property].solicitud_sci) {
		          response.data.solicitud.solicitudes_sci.push(data.DataSetAll.tmp_solicitud[property].solicitud_sci);
            } else {
		          response.data.solicitud.solicitudes_sci.push(data.DataSetAll.tmp_solicitud[property]);
            }
	        }
	      }
      });
    } else {
      response.status = "ERROR";
      response.data = responseSolicitud.data.getSolicitudPropuestaResult;
    }
  } else {
    response = responseSolicitud;
  }
  res.json(response);
});

router.get(`/cuentasVinculadas/:doc_id/:ext`,
  //token.authenticateJwt,
  (req, res) => soapCtrl.cuentasVinculadas(req.params, res)
);
router.get(`/cuentasAsociadas/:doc_id/:ext`,
  //token.authenticateJwt,
  (req, res) => soapCtrl.cuentasAsociadas(req.params, res)
);

router.get('/getSolicitudPrimeraEtapa/:solicitud',
    //token.authenticateJwt,
    (req, res) => soapCtrl.getSolicitudPrimeraEtapa(req.params, res)
);

router.get('/getSolicitud/:solicitud',
    //token.authenticateJwt,
    (req, res) => soapCtrl.getSolicitudPrimeraEtapa(req.params, res)
);

router.get('/getAccount/:cod_agenda', async (req, res) => {
  const cod_agenda = req.params.cod_agenda;
  let responseSolicitud = { status: "OK", message: "", data: "" };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };

  responseSolicitud = await invoke.soapExecute(
      { cod_agenda: cod_agenda,
        usuario:'prueba',
        password:'prueba'},
      "getAccount",
      "WS-SOAP-ECOFUTURO"
  );

  if (responseSolicitud.status === "OK") {
    if (responseSolicitud.data.getAccountResult) {
      to_json(responseSolicitud.data.getAccountResult, function (error, data) {
        if (data.Cuenta != "") {
          response.data=data.Cuenta.Cuentas;
        } else {
          response.status = "ERROR";
        }
      });
    } else {
      response.status = "ERROR";
      response.data = responseSolicitud.data.getSolicitudPropuestaResult;
    }
  } else {
    response = responseSolicitud;
  }
  res.json(response);
});


router.get('/validaTarjeta/:cod_agenda/:nro_tarjeta', async (req, res) => {
  const cod_agenda = req.params.cod_agenda;
  const nro_tarjeta = req.params.nro_tarjeta;
  let responseSolicitud = { status: "OK", message: "", data: "" };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };

  responseSolicitud = await invoke.soapExecute(
    { cod_agenda: cod_agenda,
      nro_tarjeta: nro_tarjeta,
      usuario:'prueba',
      password:'prueba'},
    "validaTarjeta",
    "WS-SOAP-ECOFUTURO"
  );

  if (responseSolicitud.status === "OK") {
    if (responseSolicitud.data.validaTarjetaResult) {
      to_json(responseSolicitud.data.validaTarjetaResult, function (error, data) {
        response.data=data.TC.Tarjeta;
      });
    } else {
      response.status = "ERROR";
      response.data = responseSolicitud.data.getSolicitudPropuestaResult;
    }
  } else {
    response = responseSolicitud;
  }
  res.json(response);
});

router.get('/getTarjetaDebitoCuentas/:nro_tarjeta', async (req, res) => {
  const nro_tarjeta = req.params.nro_tarjeta;
  let responseSolicitud = { status: "OK", message: "", data: "" };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };
  try {
    responseSolicitud = await invoke.soapExecute(
      { nro_tarjeta: nro_tarjeta,
        usuario:'prueba',
        password:'prueba'},
      "getTarjetaDebitoCuentas",
      "WS-SOAP-ECOFUTURO"
    );

    if (responseSolicitud.status === "OK") {
      if (responseSolicitud.data.getTarjetaDebitoCuentasResult) {
        to_json(responseSolicitud.data.getTarjetaDebitoCuentasResult, function (error, data) {
          response.data=data.Debito;
        });
      } else {
        response.status = "ERROR";
        response.data = responseSolicitud.data.getSolicitudPropuestaResult;
      }
    } else {
      response = responseSolicitud;
    }
    res.json(response);
  } catch (e) {
    response.status = 'ERROR';
    response.message = e;
  }
});

router.get('/validaTran/:nro_tran', async (req, res) => {
  const nro_tran= req.params.nro_tran;
  let responseTransaccion = { status: "OK", message: "", data: "" };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };
  try {
    responseTransaccion = await invoke.soapExecute(
      { nro_tran: nro_tran,
        usuario:'prueba',
        password:'prueba'},
      "validaTran",
      "WS-SOAP-ECOFUTURO"
    );

    if (responseTransaccion.status === "OK") {
      if (responseTransaccion.data.validaTranResult) {
        to_json(responseTransaccion.data.validaTranResult, function (error, data) {
          response.data=data.Transac.Transac;
        });
      } else {
        response.status = "ERROR";
        response.data = responseTransaccion.data.validaTranResult;
      }
    } else {
      response = responseTransaccion;
    }
    res.json(response);
  } catch (e) {
    response.status = 'ERROR';
    response.message = e;
  }
});

router.post('/validaTranDebitoCsg', async (req, res) => {
  const nro_tran = req.body.nro_tran;
  const nro_solicitud = req.body.nro_solicitud;
  const monto = req.body.monto;
  const moneda = req.body.moneda ? req.body.moneda : 1;
  let responseTransaccion = { status: "OK", message: "", data: "" };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };
  try {
    responseTransaccion = await invoke.soapExecute(
      { NroTran: nro_tran,
        NroSolicitud:nro_solicitud,
        Monto:monto,
        Moneda:moneda},
      "validaTranDebitoCSG",
      "WS-SOAP-ECOFUTURO"
    );

    if (responseTransaccion.status === "OK") {
      if (responseTransaccion.data.validaTranDebitoCSGResult) {
        to_json(responseTransaccion.data.validaTranDebitoCSGResult, function (error, data) {
          response.data=data.transac.transac;
        });
      } else {
        response.status = "ERROR";
        response.data = responseTransaccion.data.validaTranDebitoCSGResult;
      }
    } else {
      response = responseTransaccion;
    }
    res.json(response);
  } catch (e) {
    response.status = 'ERROR';
    response.message = e;
  }
});

router.post('/regCertificado', async (req, res) => {
  const SolicitudId = req.body.SolicitudId;
  const certificado = req.body.certificado;
  const prima = req.body.prima;
  const moneda = req.body.moneda ? req.body.moneda : 1;
  const fecha_vigencia = req.body.fecha_vigencia;
  let responseCertificado = { status: "OK", message: "", data: "" };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };
  try {
    responseCertificado = await invoke.soapExecute({
          SolicitudId: SolicitudId,
          certificado: certificado,
          prima:prima,
          moneda:moneda,
          fecha_vigencia:fecha_vigencia
      },
      "regCertificado",
      "WS-SOAP-ECOFUTURO"
    );
    if (responseCertificado.status === "OK") {
      if (responseCertificado.data.regCertificadoResult) {
        to_json(responseCertificado.data.regCertificadoResult, function (error, data) {
          response.data=data.reg_certificado.reg_certificado;
        });
      } else {
        response.status = "ERROR";
        response.data = responseCertificado.data.regCertificadoResult;
      }
    } else {
      response = responseCertificado;
    }
    res.json(response);
  } catch (e) {
    response.status = 'ERROR';
    response.message = e;
  }
});

router.get('/getAgencia/:id_sucursal', async (req, res) => {
  const id_sucursal = req.params.id_sucursal;
  let responseSolicitud = { status: "OK", message: "", data: "" };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };
  try {
    responseSolicitud = await invoke.soapExecute(
      { sucursal: id_sucursal,
        usuario:'prueba',
        password:'prueba'},
      "getAgencia",
      "WS-SOAP-ECOFUTURO"
    );

    if (responseSolicitud.status === "OK") {
      if (responseSolicitud.data.getAgenciaResult) {
        to_json(responseSolicitud.data.getAgenciaResult, function (error, data) {
          response.data=data.Agencias;
        });
      } else {
        response.status = "ERROR";
        response.data = responseSolicitud.data.getAgenciaResult;
      }
    } else {
      response = responseSolicitud;
    }
    res.json(response);
  } catch (e) {
    response.status = 'ERROR';
    response.message = e;
  }
});

router.get('/getSucursal', async (req, res) => {
  let responseSolicitud = { status: "OK", message: "", data: "" };
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };
  try {
    responseSolicitud = await invoke.soapExecute(
      {
        usuario:'prueba',
        password:'prueba'},
      "getSucursal",
      "WS-SOAP-ECOFUTURO"
    );

    if (responseSolicitud.status === "OK") {
      if (responseSolicitud.data.getSucursalResult) {
        to_json(responseSolicitud.data.getSucursalResult, function (error, data) {
          response.data=data.Sucursales;
        });
      } else {
        response.status = "ERROR";
        response.data = responseSolicitud.data.getSucursalResult;
      }
    } else {
      response = responseSolicitud;
    }
    res.json(response);
  } catch (e) {
    response.status = 'ERROR';
    response.message = e;
  }
});

router.post('/invoke', async (req, res) => {
  let response = { status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' };
  try {
    let { request_body, operation } = req.body;
    console.log('invocando servicios del banco',request_body, operation )
    response.data = await soapExecute(
        request_body,
        operation,
        "WS-SOAP-SEGUROS"
      );
    res.json(response);
  } catch (e) {
    response.status = 'ERROR';
    response.message = e;
  }
});

router.get("/verifyInstanciasWithBankServices/:id_poliza", async (req, res) => await soapCtrl.verifyInstanciasWithBankServices(req.params, res));

module.exports = router;






/*

POST : http://localhost:3000/api-corredores-ecofuturo/soap-ui
{ "wsdl":"http://www.dneonline.com/calculator.asmx?wsdl", "operation":"Add", "request":{"intA":1,"intB":1} }

https://104.42.198.35:3000/api-corredores-ecofuturo/ws-ecofuturo
http://localhost:3000/api-corredores-ecofuturo/ws-ecofuturo

{
  "wsdl":"http://www.dneonline.com/calculator.asmx?wsdl",
  "soapHeader":{"Username":"roger","Password":"123456"},
  "operation":"Add",
  "request":{"intA":22,"intB":33}
}


{
  "wsdl":"https://172.16.15.98:7000/ECOServicesCobranza/ECOCobranza.asmx?wsdl",
  "soapHeader":{"Username":"ECOS","Password":"SIS"},
  "operation":"getValida",
  "request":{"UsuarioIn": {"usuario":"mmoscoso","password":"123456","idsistema":"58"}}
}

{"UsuarioIn": {"usuario":"mmoscoso","password":"123456","idsistema":"58"}}


{
  "wsdl":"https://171.3.0.10:40448/SegurosServices/seguros.asmx?wsdl",
  "soapHeader":{"Username":"ECOS","Password":"SIS"},
  "operation":"getSolicitud",
  "request":{"solicitud":"130206"}
}

soap.createClient(url, options, function(err, client) {
var method = client['Add'];
client.soapHeader = soapHeader;
method(requestArgs, function(err, result, envelope) {
  //response envelope
  console.log('Response Envelope: \n' + envelope);
  //'result' is the response body
  console.log('Result: \n' + JSON.stringify(result));
});
});

https://172.16.15.98:40462/SeguroServices/Seguros.asmx?wsdl
195583


{
  "wsdl":"https://172.16.15.98:40448/SeguroServices/Seguros.asmx?wsdl",
  "soapHeader":{"Username":"ECOS","Password":"SIS"},
  "operation":"getSolicitudPropuesta",
  "request":{"solicitud":"141122"}
}

{
  "wsdl":"https://172.16.15.98:40448/SeguroServices/Seguros.asmx?wsdl",
  "soapHeader":{"Username":"ECOS","Password":"SIS"},
  "operation":"getSolicitudPropuesta",
  "request":{"solicitud":"141122"}
}

{
  "wsdl":"https://171.3.0.10:40448/SegurosServices/seguros.asmx?wsdl",
  "soapHeader":{"Username":"ECOS","Password":"SIS"},
  "operation":"getSolicitudPropuesta",
  "request":{"solicitud":"104593"}
}



*/
