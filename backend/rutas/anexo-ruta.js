const { Router } = require('express');
const router = new Router();
const modelos = require("../modelos");
const Util = require("../utils/util");
const util = new Util();
const AnexoService = require("../servicios/anexo.service");

router.post('/AnexoAndDantoAnexoByIdPoliza', async (req, res) => {
    try {
        const id_poliza = req.body.id_poliza;
        const id_asegurado = req.body.id_asegurado;
        const id_anexo_poliza = req.body.id_anexo_poliza;
        let objData = await AnexoService.anexoAndDantoAnexoByIdPoliza(id_poliza,id_asegurado,id_anexo_poliza);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/findPlanesAnexoPoliza/:idPoliza', async (req, res) => {
    try {
        const id_poliza = req.params.idPoliza;
        let objData = await AnexoService.findPlanesAnexoPoliza(id_poliza);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/findAnexoPoliza/:idPoliza', async (req, res) => {
    try {
        const id_poliza = req.params.idPoliza;
        let objData = await AnexoService.findAnexoPoliza(id_poliza);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/findAnexoPolizaWithAsegurado/:idPoliza/:idAsegurado', async (req, res) => {
    try {
        const id_asegurado = req.params.idAsegurado;
        const id_poliza = req.params.idPoliza;
        let objData = await AnexoService.findAnexoPolizaWithAsegurado(id_poliza,id_asegurado);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/byid/:id', async (req, res) => {
    try {
        const id = req.params.id;
        let objData = await AnexoService.byid(id);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/guardar_usuarioxrol', async (req, res) => {
    try {
        const id_usuario = req.body.id_usuario;
        const id_rol = req.body.id_rol;
        let objData = await AnexoService.guardar_usuarioxrol(id_usuario,id_rol);
        if (objData) {
            util.setSuccess(200, "UsuarioxRol creado", objData);
        } else {
            util.setError(404, `UsuarioxRol no creado`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/ResgistrarEmparejamiento', async (req, res) => {
    try {
        const usuarios = req.body;
        let objData = await AnexoService.resgistrarEmparejamiento(usuarios);
        if (objData) {
            util.setSuccess(200, "rol usuario creado", objData);
        } else {
            util.setError(404, `rol usuario no creado`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/eliminarRol_x_Perfil', async (req, res) => {
    try {
        const id_usuario = req.body.id_usuario;
        const id_rol = req.body.id_rol;
        let objData = await AnexoService.eliminarRol_x_Perfil(id_usuario,id_rol);
        if (objData) {
            util.setSuccess(200, "rol usuario eliminado", objData);
        } else {
            util.setError(404, `UsuarioxRol no eliminado`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/findAllAnexoPolizaByIdPoliza/:id', async (req, res) => {
    try {
        const id_poliza = req.params.id;
        let objData = await AnexoService.findAllAnexoPolizaByIdPoliza(id_poliza);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;
