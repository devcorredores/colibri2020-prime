const { Router } = require('express');
const router = new Router();
const Util = require("../utils/util");
const util = new Util();
const InstanciaAnexoAseguradoService = require("../servicios/instanciaAnexoAsegurado.service");

router.post('/SaveOrUpdateInstancia_anexo_asegurado', async (req, res) => {
    try {
        let instancia_anexo_asegurado = req.body.instancia_anexo_asegurado;
        let id_usuario = req.body.id_usuario;
        let objData = await InstanciaAnexoAseguradoService.SaveOrUpdateInstancia_anexo_asegurado(instancia_anexo_asegurado,id_usuario);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.post('/Eliminar', async (req, res) => {
    try {
        let instancia_anexo_asegurado = req.body.instancia_anexo_asegurado;
        let id_usuario = req.body.id_usuario;
        let objData = await InstanciaAnexoAseguradoService.Eliminar(instancia_anexo_asegurado,id_usuario);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;