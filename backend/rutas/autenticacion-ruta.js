const { Router } = require('express');
const router = new Router();
const modelos = require("../modelos");
const Util = require("../utils/util");
const util = new Util();
const AutenticacionService = require("../servicios/autenticacion.service");

router.get('/', async (req, res) => {
    try {
        const id_poliza = req.params.id;
        let objData = await AutenticacionService.autenticacionUsuario();
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/autenticacionusuario/:id_usuario', async (req, res) => {
    try {
        let id_usuario = req.params.id_usuario;
        let objData = await AutenticacionService.autenticacionusuario(id_usuario);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/byid/:id', async (req, res) => {
    try {
        let id = req.params.id;
        let objData = await AutenticacionService.byid(id);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});


router.post('/guardar_autenticacionxusuario', async (req, res) => {
    try {
        let usuario_id= req.body.id_usuario;
        let par_autenticacion_id = req.body.par_autenticacion_id;
        let objData = await AutenticacionService.guardar_autenticacionxusuario(usuario_id, par_autenticacion_id);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;
