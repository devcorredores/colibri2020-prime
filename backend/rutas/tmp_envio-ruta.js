const { Router } = require('express');
const router = new Router();
const invoke = require("../modulos/soap-request");
const request = require('request');
const soap = require('soap');
const webconfig = require('../config/web-config');
const to_json = require('xmljson').to_json;
const modelos = require('../modelos');
const solicitud = require('./persona-ruta');
const { Op } = require("sequelize");
const specialRequest = request.defaults({
  strictSSL: false
});
const sftp = require('../modulos/sftp');
const soapHeaderXml = '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';

// router.get('/', async (req, res) => {
//   res.json({ status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' });
// });


router.get('/envioFtpTest/:id', async (req, res) => {
	try {
		let response = {status: 'OK', message: 'envioFtpTest', data: []};
		let id = req.params.id;
		response.data = await sftp.envio_1(id);
		res.json(response);
	} catch (e) {
		console.log(e);
	}
});

String.prototype.isLetter = function() {
    return this.match(/[a-z]/i);
}

Array.prototype.isInArray = function(value) {
  return this.indexOf(value) > -1;
}
String.prototype.removeAccents = function() {
	var map = {
		'-' : ' ',
		'-' : '_',
		'a' : 'á|à|ã|â|À|Á|Ã|Â',
		'e' : 'é|è|ê|É|È|Ê',
		'i' : 'í|ì|î|Í|Ì|Î',
		'o' : 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
		'u' : 'ú|ù|û|ü|Ú|Ù|Û|Ü',
		'c' : 'ç|Ç',
		'n' : 'ñ|Ñ'
	};
	let str = this;
	for (var pattern in map) {
		str = str.replace(new RegExp(map[pattern], 'g'), pattern);
	};

	return str;
}

function isInt(n){
	return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
	return n === +n && n !== (n|0);
}

module.exports = router;
