const { Router } = require('express');
const router = new Router();
const invoke = require("../modulos/soap-request");
const fechas = require('fechas');
const request = require('request');
const soap = require('soap');
const webconfig = require('../config/web-config');
const to_json = require('xmljson').to_json;
const modelos = require('../modelos');
const solicitud = require('./persona-ruta');
const MigracionSeervice = require('../servicios/migracion.service');
const specialRequest = request.defaults({ strictSSL: false });
const soapHeaderXml = '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';

// router.get('/', async (req, res) => {
//   res.json({ status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' });
// });


router.get('/createSolicitudesEcoproteccionFromReports', async (req, res) => {
  try {
	  let response = await MigracionSeervice.migraEcoProteccion(req, res);
    res.json(response);
  } catch (e) {
    console.log(e);
  }
});

router.get('/createSolicitudesEcomedicvFromReports', async (req, res) => {
  try {
	  let response = await MigracionSeervice.migraEcoMedicv(req, res);
    res.json(response);
  } catch (e) {
    console.log(e);
  }
});

router.get('/createSolicitudesEcovidaFromReports', async (req, res) => {
  try {
	  let response = await MigracionSeervice.migraEcovida(req, res);
    res.json(response);
  } catch (e) {
    console.log(e);
  }
});

router.get('/createSolicitudesEcoriesgoFromReports', async (req, res) => {
  try {
	  let response = await MigracionSeervice.migraEcoriesgo(req, res);
    res.json(response);
  } catch (e) {
    console.log(e);
  }
});

router.get('/createSolicitudesEcoaguinaldoFromReports', async (req, res) => {
  try {
	  let response = await MigracionSeervice.migraEcoaguinaldo(req, res);
    res.json(response);
  } catch (e) {
    console.log(e);
  }
});

router.get('/createSolicitudesTarjetasFromReports', async (req, res) => {
  try {
      let columns = {
          colNroCertificado: 'nro_certificado',
          colFechaCertificado: 'fecha_emision',
          colDocumentoCi: 'docId',
          colDocumentoTipoDescripcion: null,
          colDocumentoTipoAbreviacion: null,
          colDocumentoTipoCod: null,
          colDocumentoTipoId: null,
          colExtensionAbreviacion: 'docIdExt',
          colExtensionDescripcion: null,
          colBeneficiarioDocumentoCi:null,
          colBeneficiarioExtensionAbreviacion:null,
          colBeneficiarioExtensionDescripcion:null,
          colBeneficiarioParentescoDescripcion:'parentesco',
          colBeneficiarioPrimerApellido:'apellido_paterno_beneficiario',
          colBeneficiarioSegundoApellido:'apellido_materno_beneficiario',
          colBeneficiarioSegundoNombre:null,
          colBeneficiarioPrimerNombre:'nombres_beneficiario',
          colBeneficiarioPorcentaje:'porcentaje',
          colPersonaPrimerApellido:'apellido_paterno',
          colPersonaSegundoApellido:'apellido_materno',
          colPersonaPrimerNombre:'nombres',
          colPersonaEstadoCivil:'estado_civil',
          colIdTarjeta:'nro_cuenta',
          colPersonaNroTarjeta:null,
          colPrimaMensual:null,
          colTransaccionImporte:'prima',
          colTransaccionTipoMoneda:null,
          colTransaccionDetalle:null,
          colPrimaTotal: 'prima',
          colPersonaCodAgenda:null,
          colPersonaSegundoNombre:null,
          colPersonaDireccionDomicilio:null,
          colPersonaCelular:null,
          colPersonaTelefonoDomicilio:null,
          colGenero: null,
          colEmitido: null,
          colModalidadPago: null,
          colOperacionPlazo: null,
          colFechaNacimiento: null,
          colMonedaOperacionDescripcion: null,
          colMonedaOperacionAbreviacion: null,
          colFechaInicio: 'fecha_inicio_vigencia',
          colFechaFin: 'fecha_fin_vigencia',
          colOperacionEstado: null,
          colUsuarioCargoCodigo:null,
          colOperacionProductoAsociado:null,
          colOperacionTipoSeguro:null,
          colUsuarioCargoDescripcion:null,
          colSucursalCodigo: null,
          colSucursalDescripcion: 'sucursal',
          colAgenciaDescripcion: 'agencia',
          colAgenciaCodigo: 'cod_agenda',
          colOperacionNroSolicitud: 'nro_operacion',
          colUsername: 'nombre_usuario',
          colNombreUsuario: null,
          colId: 'id',
          colAdicionadoPor: 'adicionado_por',
          colidInstanciaPoliza: 'id_instancia_poliza'
      }
      let consumeSoapServices = {
          getCustomer:false,
          obtenerTarjetasPorNroCIyExtension:false,
          getAccount:false,
          getCustomerSol:false
      }
      let idPoliza = 3;
      let limit = req.query.limit ? parseInt(req.query.limit+'') : null;
      let response = await MigracionSeervice.migracionGeneral('mig_tarjetas',idPoliza,columns,consumeSoapServices,limit);
    res.json(response);
  } catch (e) {
    console.log(e);
  }
});

router.get('/createSolicitudesEcoAccidenteFromReports', async (req, res) => {
  try {
      let columns = {
          colNroCertificado: 'ID_CERTIFICADO',
          colFechaCertificado: 'FECHA_CERTIFICADO',
          colDocumentoCi: 'num_doc_id',
          colDocumentoTipoDescripcion: null,
          colDocumentoTipoAbreviacion: 'num_doc_id_tipo',
          colDocumentoTipoCod: null,
          colDocumentoTipoId: null,
          colExtensionAbreviacion: '',
          colExtensionDescripcion: 'EXTENSION',
          colExtensionCod: 'num_doc_id_ext',
          colBeneficiarioDocumentoCi:null,
          colBeneficiarioExtensionAbreviacion:null,
          colBeneficiarioExtensionDescripcion:null,
          colBeneficiarioParentescoDescripcion:null,
          colBeneficiarioPrimerApellido:null,
          colBeneficiarioSegundoApellido:null,
          colBeneficiarioSegundoNombre:null,
          colBeneficiarioPrimerNombre:null,
          colBeneficiarioPorcentaje:null,
          colPersonaPrimerApellido:'APELLIDOS',
          colPersonaSegundoApellido:null,
          colPersonaPrimerNombre:'NOMBRES',
          colPersonaEstadoCivil:null,
          colIdTarjeta:null,
          colPersonaNroTarjeta:null,
          colPrimaMensual:null,
          colTransaccionImporte:null,
          colTransaccionTipoMoneda:null,
          colTransaccionDetalle:null,
          colPrimaTotal: 'PRIMA_TOTAL',
          colPersonaCodAgenda:null,
          colPersonaSegundoNombre:null,
          colPersonaDireccionDomicilio:null,
          colPersonaCelular:null,
          colPersonaTelefonoDomicilio:null,
          colGenero: null,
          colEmitido: null,
          colModalidadPago: null,
          colOperacionPlazo: null,
          colFechaNacimiento: 'FECHA_NACIMIENTO',
          colMonedaOperacionDescripcion: 'MONEDA',
          colMonedaOperacionAbreviacion: null,
          colFechaInicio: 'FECHA_INICIO',
          colFechaFin: 'FECHA_FIN',
          colOperacionEstado: 'ESTADO',
          colUsuarioCargoCodigo:null,
          colOperacionProductoAsociado:null,
          colOperacionTipoSeguro:null,
          colUsuarioCargoDescripcion:null,
          colSucursalCodigo: 'sucursal_cod',
          colSucursalDescripcion: '',
          colAgenciaDescripcion: 'AGENCIA',
          colAgenciaCodigo: 'agencia_cod',
          colOperacionNroSolicitud: 'NRO_SOLICITUD',
          colUsername: 'USERNAME',
          colNombreUsuario: 'NOMBRE_USUARIO',
          colId: 'id',
          colAdicionadoPor: 'adicionado_por',
          colidInstanciaPoliza: 'id_instancia_poliza'
      }
      let consumeSoapServices = {
          getCustomer:false,
          obtenerTarjetasPorNroCIyExtension:false,
          getAccount:false,
          getCustomerSol:false
      }
      let idPoliza = 10;
      let limit = req.query.limit ? parseInt(req.query.limit+'') : null;
	  let response = await MigracionSeervice.migracionGeneral('mig_ecoaccidente',idPoliza,columns,consumeSoapServices,limit);
    res.json(response);
  } catch (e) {
    console.log(e);
  }
});

router.get('/createSolicitudesEcoResguardoFromReports', async (req, res) => {
  try {
      let columns = {
          colNroCertificado: 'ID_CERTIFICADO',
          colFechaCertificado: 'FECHA_CERTIFICADO',
          colDocumentoCi: 'num_doc_id',
          colDocumentoTipoDescripcion: null,
          colDocumentoTipoAbreviacion: 'num_doc_id_tipo',
          colDocumentoTipoCod: null,
          colDocumentoTipoId: null,
          colExtensionAbreviacion: '',
          colExtensionDescripcion: 'EXTENSION',
          colExtensionCod: 'num_doc_id_ext',
          colBeneficiarioDocumentoCi:null,
          colBeneficiarioExtensionAbreviacion:null,
          colBeneficiarioExtensionDescripcion:null,
          colBeneficiarioParentescoDescripcion:null,
          colBeneficiarioPrimerApellido:null,
          colBeneficiarioSegundoApellido:null,
          colBeneficiarioSegundoNombre:null,
          colBeneficiarioPrimerNombre:null,
          colBeneficiarioPorcentaje:null,
          colPersonaPrimerApellido:'APELLIDOS',
          colPersonaSegundoApellido:null,
          colPersonaPrimerNombre:'NOMBRES',
          colPersonaEstadoCivil:null,
          colIdTarjeta:null,
          colPersonaNroTarjeta:null,
          colPrimaMensual:null,
          colTransaccionImporte:null,
          colTransaccionTipoMoneda:null,
          colTransaccionDetalle:null,
          colPrimaTotal: 'PRIMA_TOTAL',
          colPersonaCodAgenda:null,
          colPersonaSegundoNombre:null,
          colPersonaDireccionDomicilio:null,
          colPersonaCelular:null,
          colPersonaTelefonoDomicilio:null,
          colGenero: null,
          colEmitido: null,
          colModalidadPago: null,
          colOperacionPlazo: null,
          colFechaNacimiento: 'FECHA_NACIMIENTO',
          colMonedaOperacionDescripcion: 'MONEDA',
          colMonedaOperacionAbreviacion: null,
          colFechaInicio: 'FECHA_INICIO',
          colFechaFin: 'FECHA_FIN',
          colOperacionEstado: 'ESTADO',
          colUsuarioCargoCodigo:null,
          colOperacionProductoAsociado:null,
          colOperacionTipoSeguro:null,
          colUsuarioCargoDescripcion:null,
          colSucursalCodigo: 'sucursal_cod',
          colSucursalDescripcion: '',
          colAgenciaDescripcion: 'AGENCIA',
          colAgenciaCodigo: 'agencia_cod',
          colOperacionNroSolicitud: 'NRO_SOLICITUD',
          colUsername: 'USERNAME',
          colNombreUsuario: 'NOMBRE_USUARIO',
          colId: 'id',
          colAdicionadoPor: 'adicionado_por',
          colidInstanciaPoliza: 'id_instancia_poliza'
      }
      let consumeSoapServices = {
          getCustomer:false,
          obtenerTarjetasPorNroCIyExtension:false,
          getAccount:false,
          getCustomerSol:false
      }
      let idPoliza = 12;
      let limit = req.query.limit ? parseInt(req.query.limit+'') : null;
	  let response = await MigracionSeervice.migracionGeneral('mig_ecoresguardo',idPoliza,columns,consumeSoapServices,limit);
    res.json(response);
  } catch (e) {
    console.log(e);
  }
});

String.prototype.isLetter = function() {
    return this.match(/[a-z]/i);
}

Array.prototype.isInArray = function(value) {
  return this.indexOf(value) > -1;
}
String.prototype.removeAccents = function() {
	var map = {
		'-' : ' ',
		'-' : '_',
		'a' : 'á|à|ã|â|À|Á|Ã|Â',
		'e' : 'é|è|ê|É|È|Ê',
		'i' : 'í|ì|î|Í|Ì|Î',
		'o' : 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
		'u' : 'ú|ù|û|ü|Ú|Ù|Û|Ü',
		'c' : 'ç|Ç',
		'n' : 'ñ|Ñ'
	};
	let str = this;
	for (var pattern in map) {
		str = str.replace(new RegExp(map[pattern], 'g'), pattern);
	};




	return str;
}

	function minTwoDigits(n) {
		return (n < 10 ? '0' : '') + n;
	}

module.exports = router;
