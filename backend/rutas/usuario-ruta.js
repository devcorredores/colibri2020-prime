const { Router } = require("express");
const router = new Router();
const bcrypt = require("bcrypt-nodejs");
const modelos = require("../modelos");
const { loggerCatch } = require("../modulos/winston");

const autentificacion = require("../modulos/passport");
const { forever } = require("request");

router.get("/usuarios", async (req, res) => {
  let response = { status: "OK", message: "datos-usuario", data: "" };
  await modelos.sequelize
    .query(
      "SELECT * FROM usuario U INNER JOIN autenticacion_usuario A ON U.id =  a.usuario_id"
    )
    .then(([results, metadata]) => {
      response.data = results;
      res.json(response);
      console.log(
        "determinar usuarios, roles y autenticacion ------------------------------------------------------> OK"
      );
    });
});

// router.get('/guestaccess/:usr', async (req, res) => {
// let response = { status: "OK", message: "datos-usuario", data: "" };
// const usuario_login = req.params.usr;
// /* Hace login  */
// autentificacion.auth(("guest", "guest", true).then(usuario => {
//     console.log('devuelto', usuario, usuario_login)
// }).catch((err) => {

//     response.status = 'ERROR';
//     response.message = 'Error al localizar usuario, reintente.';
//     return res.status(401).send({ message: 'Error al localizar usuario, reintente.' });
// });
/*********/

// modelos.usuario.find({ where: { usuario_email } }).then(usuariosPwd => {
//     // response.data = usuariosPwd.dataValues;
//     if (usuariosPwd) {
//         response.message = 'Email registrado';
//         return res.status(200).send({ message: 'Email registrado' });
//     } else {
//         response.message = 'Email no registrado';
//         return res.status(300).send({ message: 'Email no registrado' });
//     }
// }).catch((err) => {
//     response.status = 'ERROR';
//     response.message = 'Error al localizar usuario, reintente.';
//     return res.status(401).send({ message: 'Error al localizar usuario, reintente.' });
// });

//     })
// });

router.get("/valida_email/:usuario_email", async (req, res) => {
  let response = { status: "OK", message: "datos-usuario", data: "" };
  const usuario_email = req.params.usuario_email;
  modelos.usuario
    .find({ where: { usuario_email } })
    .then((usuariosPwd) => {
      // response.data = usuariosPwd.dataValues;
      if (usuariosPwd) {
        console.log(usuariosPwd);
        response.message = "Email registrado";
        return res
          .status(200)
          .send({ data: usuariosPwd.dataValues, message: "Email registrado" });
        // return res.status(200).send({ message: 'Email registrado' });
      } else {
        response.message = "Email no registrado";
        return res
          .status(201)
          .send({ data: {}, message: "Email no registrado" });
      }
    })
    .catch((err) => {
      response.status = "ERROR";
      response.message = "Error al localizar usuario, reintente.";
      loggerCatch.info(new Date(), err);
      return res
        .status(401)
        .send({ message: "Error al localizar usuario, reintente." });
    });
});

router.get("/datos_email", async (req, res) => {
  let response = { status: "OK", message: "datos-usuario", data: "" };
  const usuario_email = req.body.usuario_email;
  modelos.usuario
    .find({ where: { usuario_email } })
    .then((usuariosPwd) => {
      // response.data = usuariosPwd.dataValues;
      if (usuariosPwd) {
        response.data = usuariosPwd.dataValues;
        response.message = "Email registrado";
        res.status(200).send({ user: response.data });
      } else {
        response.message = "Email no registrado";
        return res.status(300).send({ message: "Email no registrado" });
      }
    })
    .catch((err) => {
      response.status = "ERROR";
      response.message = "Error al localizar usuario, reintente.";
      loggerCatch.info(new Date(), err);
      return res
        .status(401)
        .send({ message: "Error al localizar usuario, reintente." });
    });
});

router.post("/authenticate", autentificacion.auth(), (req, res) => {
  res.status(200).json({ statusCode: 200, user: req.user });
  // const open = require('open');
  // (async () => {
  //     await open(`http://localhost:4200/#/cambiapwd?username=${req.body.username}`);
  // })();
});

// router.get('/authenticateguest', autentificacion.auth2(), (req, res) => {
//     res.status(200).json({ "statusCode": 200, "user": req.user });
//     // const open = require('open');
//     // (async () => {
//     //     await open('http://localhost:4200/#/cambiapwd');
//     //     Specify app arguments
//     //     await open('https://sindresorhus.com', { app: ['google chrome', '--incognito'] });
//     // })();
// });

// router.post('/restoreguest', isLoggedIn, (req, res) => {
//     const usuario_login = req.body.username;
//     let response = { status: 'OK', message: '', data: '' };
//     if (req.body.password != req.body.password2) {
//         return res.status(301).send({ message: 'La contraseña nueva y su repeticion no coinciden.' });
//     } else {
//         modelos.usuario.find({ where: { usuario_login } }).then(usuariosPwd => {
//             response.data = usuariosPwd.dataValues;
//             if (response.data) {
//                 const pwd_change = req.body.pwdchange;
//                 const usuario_password = bcrypt.hashSync(req.body.password, null, null);
//                 usuariosPwd.update({ usuario_login, usuario_password, pwd_change })
//                     .then(usuario => {
//                         response.data = usuario.dataValues;
//                         response.message = 'Contraseña actualizada.';
//                         return res.status(200).send({ message: 'Contraseña actualizada.' });
//                     }).catch((err) => {
//                         response.status = 'ERROR';
//                         response.message = 'Error en actualizar al localizar usuario, reintente.';
//                         return res.status(401).send({ message: 'Error en actualizar al localizar usuario, reintente.' });
//                     });
//             } else {
//                 response.status = 'ERROR';
//                 response.message = 'Este usuario no esta registrado.';
//                 return res.status(300).send({ message: 'Este usuario no esta registrado.' });
//             }
//         }).catch((err) => {
//             response.status = 'ERROR';
//             response.message = 'Error al localizar usuario, reintente.';
//             return res.status(401).send({ message: 'Error al localizar usuario, reintente.' });
//         });
//     }
// });

// router.get('/authenticateguest', autentificacion.auth2(), (req, res) => {
//     res.status(200).json({ "statusCode": 200, "user": req.user });
//     const open = require('open');
//     (async () => {
//         await open('http://localhost:4200/#/cambiapwd');
// Specify app arguments
// await open('https://sindresorhus.com', { app: ['google chrome', '--incognito'] });
//     })();
// });

var isLoggedIn = (req, res, next) => {
  console.log("session ", req.session);
  if (req.isAuthenticated()) {
    return next();
  }
  return res
    .status(400)
    .json({ statusCode: 400, message: "usuario no autentificado" });
};

router.post("/register", isLoggedIn, (req, res) => {
  let response = { status: "OK", message: "", data: "" };
  const usuario_login = req.body.usuario_login;
  const usuario_password = bcrypt.hashSync(
    req.body.usuario_password,
    null,
    null
  );
  const usuario_nombre_completo = req.body.usuario_nombre_completo;
  const usuario_email = req.body.usuario_email;
  const par_estado_usuario_id = req.body.par_estado_usuario_id;
  const par_autenticacion_id = req.body.par_autenticacion_id;
  const par_local_id = req.body.par_autenticacion_id == 10 ? req.body.par_local_id : null;
  const adicionado_por = req.body.adicionado_por;
  const pwd_change = req.body.pwd_change;
  const createdAt = new Date();
  modelos.usuario
    .find({
      where: {
        $or: [
          { usuario_login: req.body.usuario_login },
          { usuario_email: req.body.usuario_email },
        ],
      },
    })
    .then((usuarios) => {
      response.data = usuarios;
      if (response.data) {
        if (response.data.usuario_login == usuario_login) {
          response.message = "Usuario duplicado";
          return res.status(201).send({ message: response.message });
        }
        if (response.data.usuario_email == usuario_email) {
          response.message = "Email duplicado";
          return res.status(201).send({ message: response.message });
        }
      } else {
        modelos.persona
          .create({ persona_origen: "1" })
          .then((personaNueva) => {
            const id_persona = personaNueva.id;
            modelos.entidad
              .create({ id_persona })
              .then((entidadNUeva) => {
                modelos.usuario
                  .create({
                    usuario_login,
                    usuario_password,
                    usuario_nombre_completo,
                    usuario_email,
                    par_estado_usuario_id,
                    id_persona,
                    par_local_id,
                    pwd_change,
                    adicionado_por,
                    createdAt,
                  })
                  .then((usuario) => {
                    response.data = usuario;
                    const usuario_id = usuario.id;
                    modelos.autenticacion_usuario
                      .create({ usuario_id, par_autenticacion_id })
                      .then((autusuario) => {
                        response.data = autusuario;
                        response.message = "Registro y autenticacion creado";
                        return res
                          .status(200)
                          .send({ message: response.message });
                      })
                      .catch((err) => {
                        response.status = "ERROR";
                        response.message = err;
                        loggerCatch.info(new Date(), err);
                        return res
                          .status(301)
                          .send({ message: "autenticacion usuario no creado" });
                      });
                  })
                  .catch((err) => {
                    response.status = "ERROR";
                    response.message = err;
                    loggerCatch.info(new Date(), err);
                    return res
                      .status(201)
                      .send({ message: "Registro usuario no creado" });
                  });
              })
              .catch((err) => {
                response.status = "ERROR";
                response.message = err;
                loggerCatch.info(new Date(), err);
                return res
                  .status(201)
                  .send({ message: "Registro entidad no creado" });
              });
          })
          .catch((err) => {
            response.status = "ERROR";
            response.message = err;
            loggerCatch.info(new Date(), err);
            return res
              .status(201)
              .send({ message: "Registro persona no creado" });
          });
      }
    })
    .catch((err) => {
      response.status = "ERROR";
      response.message = err;
      loggerCatch.info(new Date(), err);
      // return done(null, false, req.flash('signupMessage', err.message));
    });
});

router.post("/restore", isLoggedIn, (req, res) => {
  const usuario_login = req.body.username;
  let response = { status: "OK", message: "", data: "" };
  if (req.body.password != req.body.password2) {
    return res
      .status(301)
      .send({ message: "La contraseña nueva y su repeticion no coinciden." });
  } else {
    modelos.usuario
      .find({ where: { usuario_login } })
      .then((usuariosPwd) => {
        response.data = usuariosPwd.dataValues;
        if (response.data) {
          const pwd_change = req.body.pwdchange;
          const usuario_password = bcrypt.hashSync(
            req.body.password,
            null,
            null
          );
          usuariosPwd
            .update({ usuario_login, usuario_password, pwd_change })
            .then((usuario) => {
              response.data = usuario.dataValues;
              response.message = "Contraseña actualizada.";
              return res
                .status(200)
                .send({ message: "Contraseña actualizada." });
            })
            .catch((err) => {
              response.status = "ERROR";
              response.message =
                "Error en actualizar al localizar usuario, reintente.";
              loggerCatch.info(new Date(), err);
              return res.status(401).send({
                message: "Error en actualizar al localizar usuario, reintente.",
              });
            });
        } else {
          response.status = "ERROR";
          response.message = "Este usuario no esta registrado.";
          return res
            .status(300)
            .send({ message: "Este usuario no esta registrado." });
        }
      })
      .catch((err) => {
        response.status = "ERROR";
        response.message = "Error al localizar usuario, reintente.";
        loggerCatch.info(new Date(), err);
        return res
          .status(401)
          .send({ message: "Error al localizar usuario, reintente." });
      });
  }
});

router.post("/restore_first", isLoggedIn, (req, res) => {
  const usuario_login = req.body.username;
  let response = { status: "OK", message: "", data: "" };
  if (req.body.password != req.body.password2) {
    return res
      .status(301)
      .send({ message: "La contraseña nueva y su repeticion no coinciden." });
  } else {
    modelos.usuario
      .find({ where: { usuario_login } })
      .then((usuariosPwd) => {
        response.data = usuariosPwd.dataValues;
        if (response.data) {
          bcrypt.compare(
            req.body.pwdold,
            response.data.usuario_password,
            function (err, resx) {
              if (!resx) {
                return res
                  .status(200)
                  .send({ message: "Contraseña anterior invalida" });
              } else {
                const pwd_change = req.body.pwdchange;
                const usuario_password = bcrypt.hashSync(
                  req.body.password,
                  null,
                  null
                );
                usuariosPwd
                  .update({ usuario_login, usuario_password, pwd_change })
                  .then((usuario) => {
                    response.data = usuario.dataValues;
                    response.message = "Contraseña actualizada.";
                    return res
                      .status(200)
                      .send({ message: "Contraseña actualizada." });
                  })
                  .catch((err) => {
                    response.status = "ERROR";
                    response.message =
                      "Error en actualizar al localizar usuario, reintente.";
                    loggerCatch.info(new Date(), err);
                    return res.status(401).send({
                      message:
                        "Error en actualizar al localizar usuario, reintente.",
                    });
                  });
              }
            }
          );
        } else {
          response.status = "ERROR";
          response.message = "Este usuario no esta registrado.";
          return res
            .status(300)
            .send({ message: "Este usuario no esta registrado." });
        }
      })
      .catch((err) => {
        response.status = "ERROR";
        response.message = "Error al localizar usuario, reintente.";
        loggerCatch.info(new Date(), err);
        return res
          .status(401)
          .send({ message: "Error al localizar usuario, reintente." });
      });
  }
});

router.post("/restore_request", isLoggedIn, (req, res) => {
  const usuario_login = req.body.username;
  let response = { status: "OK", message: "", data: "" };
  if (req.body.password != req.body.password2) {
    return res
      .status(301)
      .send({ message: "La contraseña nueva y su repeticion no coinciden." });
  } else {
    modelos.usuario
      .find({ where: { usuario_login } })
      .then((usuariosPwd) => {
        response.data = usuariosPwd.dataValues;
        if (response.data) {
          const pwd_change = req.body.pwdchange;
          const usuario_password = bcrypt.hashSync(
            req.body.password,
            null,
            null
          );
          usuariosPwd
            .update({ usuario_login, usuario_password, pwd_change })
            .then((usuario) => {
              response.data = usuario.dataValues;
              response.message = "Contraseña actualizada.";
              return res
                .status(200)
                .send({ message: "Contraseña actualizada." });
            })
            .catch((err) => {
              response.status = "ERROR";
              response.message =
                "Error en actualizar al localizar usuario, reintente.";
              loggerCatch.info(new Date(), err);
              return res.status(401).send({
                message: "Error en actualizar al localizar usuario, reintente.",
              });
            });
        } else {
          response.status = "ERROR";
          response.message = "Este usuario no esta registrado.";
          return res
            .status(300)
            .send({ message: "Este usuario no esta registrado." });
        }
      })
      .catch((err) => {
        response.status = "ERROR";
        response.message = "Error al localizar usuario, reintente.";
        loggerCatch.info(new Date(), err);
        return res
          .status(401)
          .send({ message: "Error al localizar usuario, reintente." });
        // return done(err, false, req.flash('restoreMessage', 'Error al localizar usuario, reintente.'));
      });
  }
});

router.post("/restore_required", isLoggedIn, (req, res) => {
  const usuario_login = req.body.username;
  const passwordold = req.body.passwordold;
  let response = { status: "OK", message: "", data: "" };
  if (req.body.password != req.body.password2) {
    return res
      .status(301)
      .send({ message: "La contraseña nueva y su repeticion no coinciden." });
  } else {
    modelos.usuario
      .find({ where: { usuario_login } })
      .then((usuariosPwd) => {
        response.data = usuariosPwd.dataValues;
        if (response.data) {
          bcrypt.compare(
            passwordold,
            response.data.usuario_password,
            function (err, res) {
              if (!res) {
                return res
                  .status(400)
                  .send({ message: "Contraseña anterior invalida" });
              } else {
                const pwd_change = 0;
                const usuario_password = bcrypt.hashSync(
                  req.body.password,
                  null,
                  null
                );
                usuariosPwd
                  .update({ usuario_login, usuario_password, pwd_change })
                  .then((usuario) => {
                    response.data = usuario.dataValues;
                    response.message = "Contraseña actualizada.";
                    return res
                      .status(200)
                      .send({ message: "Contraseña actualizada." });
                  })
                  .catch((err) => {
                    response.status = "ERROR";
                    response.message =
                      "Error en actualizar al localizar usuario, reintente.";
                    loggerCatch.info(new Date(), err);
                    return res.status(401).send({
                      message:
                        "Error en actualizar al localizar usuario, reintente.",
                    });
                  });
              }
            }
          );
        } else {
          response.status = "ERROR";
          response.message = "Este usuario no esta registrado.";
          return res
            .status(300)
            .send({ message: "Este usuario no esta registrado." });
        }
      })
      .catch((err) => {
        response.status = "ERROR";
        response.message = "Error al localizar usuario, reintente.";
        loggerCatch.info(new Date(), err);
        return res
          .status(401)
          .send({ message: "Error al localizar usuario, reintente." });
        // return done(err, false, req.flash('restoreMessage', 'Error al localizar usuario, reintente.'));
      });
  }
});

router.put("/modifica", (req, res) => {
  let response = { status: "OK", message: "", data: "" };
  // const id = req.params.id;
  const id = req.body.usuario_id;
  const usuario_id = req.body.usuario_id;
  const par_autenticacion_id = req.body.par_autenticacion_id;
  var hoy = new Date();
  const updatedAt =
    hoy.getFullYear() +
    "/" +
    (hoy.getMonth() + 1) +
    "/" +
    hoy.getDate() +
    " " +
    hoy.getHours() +
    ":" +
    hoy.getMinutes() +
    ":" +
    hoy.getSeconds();
  var usuarioEditAux = {};
  usuarioEditAux = req.body;
  usuarioEditAux.updatedAt = updatedAt;
  modelos.usuario
    .findById(id)
    .then((usuario) => {
      usuario.update(usuarioEditAux);
      response.data = usuario;
      modelos.autenticacion_usuario
        .findOne({ where: { usuario_id } })
        .then((autenticacion_usuario) => {
          autenticacion_usuario.update({ par_autenticacion_id });
          response.message = "Registro autenticacion_usuario actualizado";
          res.json(response);
        })
        .catch((err) => {
          response.status = "ERROR";
          response.message = err;
          loggerCatch.info(new Date(), err);
          res.json(response);
        });
      // res.json(response);
    })
    .catch((err) => {
      response.status = "ERROR";
      response.message = err;
      loggerCatch.info(new Date(), err);
      res.json(response);
    });
});

router.get("/findUsuarioById/:idUsuario", isLoggedIn, async (req, res) => {
  let response = { status: "OK", message: "", data: {} };
  let idUsuario = req.params.idUsuario;
  await findUsuarioById(idUsuario, async (resp) => {
    response.data = resp;
  });
  res.json(response);
});

router.get("/findAllUsuariosWithRol/:usuario_login", async (req, res) => {
  let response = { status: "OK", message: "", data: {} };
  let usuario_login = req.params.usuario_login;
  let result = await modelos.usuario.findAll({
      where:{usuario_login:{$like:`%${usuario_login}%`}},
      include:{
          model: modelos.rol,
          as: 'usuarioRoles'
      }
  }).catch((err) => {
      response.status = "ERROR";
      response.message = err;
      loggerCatch.info(new Date(), err);
      res.json(response);
  });
  response.data = result;
  res.json(response);
});

router.get(
  "/findUsuarioByUserLoginAndPassword/:usuarioLogin/:usuarioPassword",
  isLoggedIn,
  async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let usuarioLogin = req.params.usuarioLogin;
    let usuarioPassword = req.params.usuarioPassword;
    await findUsuarioByUserLoginAndPassword(
      usuarioLogin,
      usuarioPassword,
      async (resp) => {
        response.data = resp;
      }
    );
    res.json(response);
  }
);

router.get(
  "/findUsuarioByUserLogin/:usuarioLogin",
  isLoggedIn,
  async (req, res) => {
    let response = { status: "OK", message: "", data: {} };
    let usuarioLogin = req.params.usuarioLogin;
    await findUsuarioByUserLogin(usuarioLogin, async (resp) => {
      response.data = resp;
    });
    res.json(response);
  }
);

router.get("/findUserById/:userId", isLoggedIn, async (req, res) => {
  let response = { status: "OK", message: "", data: {} };
  let userId = req.params.userId;
  await findUserById(userId, async (resp) => {
    response.data = resp;
  });
  res.json(response);
});

async function findUsuarioById(idUsuario, callback) {
  let response = { status: "OK", message: "", data: {} };
  await modelos.usuario.find({
      where: { id: idUsuario },
      include: [
        {
          model: modelos.parametro,
          as: "par_estado_usuario",
        },
        {
          model: modelos.parametro,
          as: "par_local",
        },
        {
          model: modelos.persona,
          include: [
            {
              model: modelos.parametro,
              as: "par_tipo_documento",
            },
            {
              model: modelos.parametro,
              as: "par_nacionalidad",
            },
            {
              model: modelos.parametro,
              as: "par_pais_nacimiento",
            },
            {
              model: modelos.parametro,
              as: "par_sexo",
            },
          ],
        },
        {
          model: modelos.usuario_x_rol,
          include: [
            {
              model: modelos.rol,
            },
          ],
        },
      ],
    })
    .then(async (resp) => {
      response.data = resp;
    })
    .catch((err) => {
      response.status = "ERROR";
      response.message = err;
      loggerCatch.info(new Date(), err);
    });
  await callback(response.data);
}

async function findUsuarioByUserLoginAndPassword(
  usuarioLogin,
  usuarioPassword,
  callback
) {
  let response = { status: "OK", message: "", data: {} };
  await modelos.usuario
    .find({
      where: { usuario_login: usuarioLogin, usuario_password: usuarioPassword },
      include: [
        {
          model: modelos.parametro,
          as: "par_estado_usuario",
        },
        {
          model: modelos.parametro,
          as: "par_local",
        },
        {
          model: modelos.persona,
          include: [
            {
              model: modelos.parametro,
              as: "par_tipo_documento",
            },
            {
              model: modelos.parametro,
              as: "par_nacionalidad",
            },
            {
              model: modelos.parametro,
              as: "par_pais_nacimiento",
            },
            {
              model: modelos.parametro,
              as: "par_sexo",
            },
          ],
        },
        {
          model: modelos.usuario_x_rol,
          include: [
            {
              model: modelos.rol,
            },
          ],
        },
      ],
    })
    .then(async (resp) => {
      response.data = resp;
    })
    .catch((err) => {
      response.status = "ERROR";
      response.message = err;
      loggerCatch.info(new Date(), err);
    });
  await callback(response.data);
}

async function findUsuarioByUserLogin(usuarioLogin, callback) {
  let response = { status: "OK", message: "", data: {} };
  await modelos.usuario
    .find({
      where: { usuario_login: usuarioLogin },
      include: [
        {
          model: modelos.parametro,
          as: "par_estado_usuario",
        },
        {
          model: modelos.parametro,
          as: "par_local",
        },
        {
          model: modelos.persona,
          include: [
            {
              model: modelos.parametro,
              as: "par_tipo_documento",
            },
            {
              model: modelos.parametro,
              as: "par_nacionalidad",
            },
            {
              model: modelos.parametro,
              as: "par_pais_nacimiento",
            },
            {
              model: modelos.parametro,
              as: "par_sexo",
            },
          ],
        },
        {
          model: modelos.usuario_x_rol,
          include: [
            {
              model: modelos.rol,
            },
          ],
        },
      ],
    })
    .then(async (resp) => {
      response.data = resp;
    })
    .catch((err) => {
      response.status = "ERROR";
      response.message = err;
      loggerCatch.info(new Date(), err);
    });
  await callback(response.data);
}

async function findUserById(userId, callback) {
  let response = { status: "OK", message: "", data: {} };
  await modelos.usuario
    .findOne({
      where: { id: userId },
      include: [
        {
          model: modelos.parametro,
          as: "par_estado_usuario",
        },
        {
          model: modelos.parametro,
          as: "par_local",
        },
        {
          model: modelos.rol,
          as: "usuarioRoles",
        },
      ],
    })
    .then(async (resp) => {
      response.data = resp;
    })
    .catch((err) => {
      response.status = "ERROR";
      response.message = err;
      loggerCatch.info(new Date(), err);
    });
  await callback(response.data);
}

router.get("/prueba", isLoggedIn, (req, res) => {
  res.send({ mensaje: "hola mundo" });
});

router.get("/logout", function (req, res) {
  req.logout();
  res.send({ mensaje: "sessoin cerrada" });
});

module.exports = router;
