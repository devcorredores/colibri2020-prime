const { Router } = require('express');
const router = new Router();
const Util = require("../utils/util");
const util = new Util();
const InstanciaDocumentoService = require("../servicios/instancia_documento.service");

var isLoggedIn = (req, res, next) => {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
}

router.get('/getByNroDocumento/:nro_documento/:id_poliza', async (req, res) => {
    try {
        let nro_documento = req.params.nro_documento;
        let id_poliza = req.params.id_poliza;
        let objData = await InstanciaDocumentoService.getByNroDocumento(nro_documento,id_poliza);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/getByNroDocumentoToEstado/:idEstado/:nro_documento/:id_poliza', async (req, res) => {
    try {
        const idEstado = req.params.idEstado;
        const nro_documento = req.params.nro_documento;
        const id_instancia_poliza = req.params.id_instancia_poliza;
        const id_poliza = req.params.id_poliza ? req.params.id_poliza.indexOf(',') >= 0 ? req.params.id_poliza.split(',') : req.params.id_poliza : req.params.id_poliza;
        let objData = await InstanciaDocumentoService.getByNroDocumentoToEstado(idEstado,nro_documento,id_poliza,id_instancia_poliza);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/getByIdInstanciaPolizaToEstado/:idEstado/:id_instancia_poliza/:id_poliza', async (req, res) => {
    try {
        const idEstado = req.params.idEstado;
        const id_instancia_poliza = req.params.id_instancia_poliza;
        const nro_documento = req.params.nro_documento;
        const id_poliza = req.params.id_poliza ? req.params.id_poliza.indexOf(',') >= 0 ? req.params.id_poliza.split(',') : req.params.id_poliza : req.params.id_poliza;
        let objData = await InstanciaDocumentoService.getByNroDocumentoToEstado(idEstado,nro_documento,id_poliza,id_instancia_poliza);
        if (objData) {
            util.setSuccess(200, "data retrieved", objData);
        } else {
            util.setError(404, `No data found`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;
