const invoke = require("../modulos/soap-request");
const { Router } = require("express");
const router = new Router();

  router.get("/ws/:numSolicitud", async (req, res) => {
    const numSolicitud = req.params.numSolicitud;
    let responseSolicitud = { status: "OK", message: "", data: "" };
    //let responseOperacion = { status: "OK", message: "", data: "" };
    //let responseGarantias = { status: "OK", message: "", data: "" };
    let response = { status: "OK", message: "", data: "" };
  
    let solicitud = {
      numSolicitud,
      deudor: {},
      codeudores: [],
      operacion: {},
      garantias: []
    };
  
    // invocando servicio de solicitud : getSolicitudPropuesta WS-SOAP-ECOFUTURO
    responseSolicitud = await invoke.soapExecute(
      { solicitud: numSolicitud },
      "getSolicitudPropuesta",
      "WS-SOAP-ECOFUTURO"
    );

    if (responseSolicitud.status === "OK") {
            // if (responseSolicitud.data.getSolicitudPropuestaResult.clienteDeudor) {
            //     solicitud.deudor =
            //     responseSolicitud.data.getSolicitudPropuestaResult.clienteDeudor;
            //     const codeu = JSON.stringify(
            //     responseSolicitud.data.getSolicitudPropuestaResult.clienteCodeudor
            //     );
            //     if (codeu !== "null") {
            //     solicitud.codeudores =
            //         responseSolicitud.data.getSolicitudPropuestaResult.clienteCodeudor.ClienteCodeudor;
            //     } else {
            //     solicitud.codeudores = [];
            //     }
            //     // invocando servicio : getOperacionPropuesta
            //     responseOperacion = await invoke.soapExecute(
            //     { solicitud: numSolicitud },
            //     "getOperacionPropuesta",
            //     "WS-SOAP-ECOFUTURO"
            //     );
            //     if (responseOperacion.status === "OK") {
            //     solicitud.operacion =
            //         responseOperacion.data.getOperacionPropuestaResult.operacionPropuesta;
            //     }
            //     // invocando servicio : getGarantiasPropuestas
            //     responseGarantias = await invoke.soapExecute(
            //     { solicitud: numSolicitud },
            //     "getGarantiasPropuestas",
            //     "WS-SOAP-ECOFUTURO"
            //     );
            //     if (responseGarantias.status === "OK") {
            //     solicitud.garantias =
            //         responseGarantias.data.getGarantiasPropuestasResult.GarantiaPropuesta.GarantiaPropuesta;
            //     }
            // } else {
            //     response.status = "ERROR";
            //     response.data = responseSolicitud.data.getSolicitudPropuestaResult;
            // }
    } else {
      response = responseSolicitud;
    }
  
    response.data = solicitud;
    res.json(response);
  });

  module.exports = router;