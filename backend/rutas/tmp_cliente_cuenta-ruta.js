const { Router } = require('express');
const router = new Router();
const invoke = require("../modulos/soap-request");
const request = require('request');
const soap = require('soap');
const webconfig = require('../config/web-config');
const to_json = require('xmljson').to_json;
const modelos = require('../modelos');
const solicitud = require('./persona-ruta');
const { Op } = require("sequelize");
const specialRequest = request.defaults({
  strictSSL: false
});

const soapHeaderXml = '<AuthHeader xmlns="http://tempuri.org/"><Username>ECOS</Username><Password>SIS</Password></AuthHeader>';

// router.get('/', async (req, res) => {
//   res.json({ status: 'OK', message: 'api-corredores-ecofuturo/soap-ui', data: '' });
// });


router.get('/updateAtributoClienteCuentaSeleccionado', async (req, res) => {
	try {
		let response = {status: 'OK', message: 'updateAtributoClienteCuentaSeleccionado', data: []};
		let dataClienteCuentas = await modelos.sequelize.query(`
			select id, ci, ext, colibri_cod_agenda, convert(FLOAT,colibri_cuenta) as "colibri_cuenta", revision_cod_agenda, revision_cuenta, id_instancia_poliza, "createdat", "updatedat", solucionado, revision_cueenta_seleccionada
			from tmp_cliente_cuenta
			where revision_cuenta not like '' and colibri_cuenta not like '' and revision_cod_agenda like colibri_cod_agenda and solucionado is null OR solucionado = 'False'
		`);
		if (dataClienteCuentas.length) {
			let clienteCuentas = dataClienteCuentas[0];
			for (let i = 0 ; i < clienteCuentas.length ; i++) {
				let clienteCuenta = clienteCuentas[i];
				if (clienteCuenta) {
					if (clienteCuenta.colibri_cod_agenda == clienteCuenta.revision_cod_agenda && clienteCuenta.colibri_cuenta != clienteCuenta.revision_cuenta) {
						let cuentas = clienteCuenta.revision_cuenta.split(',');
						if (cuentas.length == 1) {
							let revisionCuenta = cuentas[0];
							 let instanciaPoliza = await modelos.instancia_poliza.findOne({where:{id:clienteCuenta.id_instancia_poliza}});
							 let idPoliza = instanciaPoliza.dataValues.id_poliza;
							 let objetoAtributoCuenta = await modelos.objeto_x_atributo.findOne({
								 include:[
									 {model:modelos.objeto, where:{id_poliza:idPoliza}},
									 {model:modelos.atributo, where:{id:5}}
								 ]
							 });
							let atributoCuentaInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id_instancia_poliza:instanciaPoliza.id, id_objeto_x_atributo:objetoAtributoCuenta.dataValues.id}});
							if (atributoCuentaInstanciaPoliza.dataValues.valor != revisionCuenta) {
								let atributoCuentaUpdated = await modelos.atributo_instancia_poliza.update({valor:revisionCuenta},{where:{id:atributoCuentaInstanciaPoliza.dataValues.id}});
								let tmpClienteCuentaUPdated = await modelos.tmp_cliente_cuenta.update({solucionado: true},{where:{id_instancia_poliza:clienteCuenta.id_instancia_poliza}});
								response.data.push({updated:tmpClienteCuentaUPdated, id_instancia_poliza:clienteCuenta.id_instancia_poliza})
							}
						} else {
							let revisionCuenta = clienteCuenta.revision_cueenta_seleccionada;
							let instanciaPoliza = await modelos.instancia_poliza.findOne({where:{id:clienteCuenta.id_instancia_poliza}});
							let idPoliza = instanciaPoliza.dataValues.id_poliza;
							let objetoAtributoCuenta = await modelos.objeto_x_atributo.findOne({
								include:[
									{model:modelos.objeto, where:{id_poliza:idPoliza}},
									{model:modelos.atributo, where:{id:5}}
								]
							});
							let atributoCuentaInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id_instancia_poliza:instanciaPoliza.id, id_objeto_x_atributo:objetoAtributoCuenta.dataValues.id}});
							if (atributoCuentaInstanciaPoliza.dataValues.valor != revisionCuenta) {
								let atributoCuentaUpdated = await modelos.atributo_instancia_poliza.update({valor:revisionCuenta},{where:{id:atributoCuentaInstanciaPoliza.dataValues.id}});
								let tmpClienteCuentaUPdated = await modelos.tmp_cliente_cuenta.update({solucionado: true},{where:{id_instancia_poliza:clienteCuenta.id_instancia_poliza}});
								response.data.push({updated:tmpClienteCuentaUPdated, id_instancia_poliza:clienteCuenta.id_instancia_poliza})
							}
						}
					}
				} 
			}
		}
		res.json(response);
	} catch (e) {
		console.log(e);
	}
});


router.get('/updateAtributoClienteCuenta', async (req, res) => {
	try {
		let response = {status: 'OK', message: 'updateAtributoClienteCuenta', data: []};
		let dataClienteCuentas = await modelos.sequelize.query(`
			select *
			from tmp_cliente_cuenta
			where revision_cuenta not like '' and colibri_cuenta not like '' and revision_cod_agenda like colibri_cod_agenda and solucionado is null OR solucionado = 'False'
		`);
		if (dataClienteCuentas.length) {
			let clienteCuentas = dataClienteCuentas[0];
			for (let i = 0 ; i < clienteCuentas.length ; i++) {
				let clienteCuenta = clienteCuentas[i];
				if (clienteCuenta) {
					if (clienteCuenta.colibri_cod_agenda == clienteCuenta.revision_cod_agenda && clienteCuenta.colibri_cuenta != clienteCuenta.revision_cuenta) {
						let cuentas = clienteCuenta.revision_cuenta.split(',');
						if (cuentas.length == 1) {
							let revisionCuenta = cuentas[0];
							 let instanciaPoliza = await modelos.instancia_poliza.findOne({where:{id:clienteCuenta.id_instancia_poliza}});
							 let idPoliza = instanciaPoliza.dataValues.id_poliza;
							 let objetoAtributoCuenta = await modelos.objeto_x_atributo.findOne({
								 include:[
									 {model:modelos.objeto, where:{id_poliza:idPoliza}},
									 {model:modelos.atributo, where:{id:5}}
								 ]
							 });
							let atributoCuentaInstanciaPoliza = await modelos.atributo_instancia_poliza.findOne({where:{id_instancia_poliza:instanciaPoliza.id, id_objeto_x_atributo:objetoAtributoCuenta.dataValues.id}});
							if (atributoCuentaInstanciaPoliza.dataValues.valor != revisionCuenta) {
								let atributoCuentaUpdated = await modelos.atributo_instancia_poliza.update({valor:revisionCuenta},{where:{id:atributoCuentaInstanciaPoliza.dataValues.id}});
								let tmpClienteCuentaUPdated = await modelos.tmp_cliente_cuenta.update({solucionado: true},{where:{id_instancia_poliza:clienteCuenta.id_instancia_poliza}});
								response.data.push({updated:tmpClienteCuentaUPdated, id_instancia_poliza:clienteCuenta.id_instancia_poliza})
							}
						}
					}
				}
			}
		}
		res.json(response);
	} catch (e) {
		console.log(e);
	}
});

router.get('/updateClienteCuenta/:idPoliza', async (req, res) => {
  try {
	  let response = { status: 'OK', message: 'updateClienteCuenta', data: [] };
	  let idPoliza = req.params.idPoliza;
	  let polizas = await modelos.poliza.findAll({where:{id:idPoliza}});
  	for (let i = 0 ; i < polizas.length ; i++) {
  		let poliza = polizas[i].dataValues;
  		let objetoAtributos = await modelos.objeto_x_atributo.findAll({
			  include:[
				  { model:modelos.objeto, where: {id_poliza: poliza.id}},
				  { model:modelos.atributo, where: {id: [5,10]}}
			  ]
		  });

  		let objetoAtributoAgenda = await objetoAtributos.find(param => param.atributo.id == 10);
  		let objetoAtributoCuenta = await objetoAtributos.find(param => param.atributo.id == 5);

		  let instanciaPolizas = await modelos.instancia_poliza.findAll({
			  where : {id_poliza:poliza.id},
			  include: [
				  {
				  	model: modelos.asegurado,
					  include: {
				  		model: modelos.entidad,
						  include: {
				  			model: modelos.persona
						  }
				  	}
				  },
				  { model: modelos.atributo_instancia_poliza, where: {
						  $or: [
							  { id_objeto_x_atributo: objetoAtributoCuenta.id },
							  { id_objeto_x_atributo: objetoAtributoAgenda.id }
						  ]
					  }}
			  ]
		  });

	    for (let j = 0 ; j < instanciaPolizas.length ; j++) {
	      let instanciaPoliza = instanciaPolizas[j].dataValues;
				if (instanciaPoliza.asegurados.length == 1) {
					let asegurado = instanciaPoliza.asegurados[0].dataValues;
					if (asegurado.entidad) {
						const doc_id = asegurado.entidad.persona.persona_doc_id;
						const ext = asegurado.entidad.persona.persona_doc_id_ext;
						let responseGetCustomeer = { status: "OK", message: "", data: "" };
						let dataPersonaBanco = null;
						responseGetCustomeer = await invoke.soapExecute(
							{
								doc_id: doc_id,
								ext: ext,
								usuario:'prueba',
								password:'prueba'
							},
							"getCustomer",
							"WS-SOAP-ECOFUTURO"
						);

						if (responseGetCustomeer.status === "OK") {
							if (responseGetCustomeer.data.getCustomerResult) {
								to_json(responseGetCustomeer.data.getCustomerResult, function (error, data) {
									dataPersonaBanco = data.DataSetAll.tmp_general;
								});
							} else {
								response.status = "ERROR";
								response.data.push({doc_id:doc_id, ext: ext, data: responseGetCustomeer.data.getSolicitudPropuestaResult});
							}
						} else {
							response.data.push(responseGetCustomeer);
						}

						let dataCuentaBanco = null;

						if (dataPersonaBanco) {

							const cod_agenda = dataPersonaBanco.cod_agenda;
							let responseGetAccount = { status: "OK", message: "", data: "" };

							responseGetAccount = await invoke.soapExecute(
								{ cod_agenda: cod_agenda,
									usuario:'prueba',
									password:'prueba'},
								"getAccount",
								"WS-SOAP-ECOFUTURO"
							);

							if (responseGetAccount.status === "OK") {
								if (responseGetAccount.data.getAccountResult) {
									to_json(responseGetAccount.data.getAccountResult, function (error, data) {
										if (data.Cuenta != "") {
											dataCuentaBanco = data.Cuenta.Cuentas;
										} else {
											response.status = "ERROR";
										}
									});
								} else {
									response.status = "ERROR";
									response.data.push({doc_id: doc_id, ext: ext, data: responseGetAccount.data.getSolicitudPropuestaResult});
								}
							} else {
								response.data.push(responseGetAccount);
							}

							let cuentas = [];

							let atributoInstanciaCodAgenda = instanciaPoliza.atributo_instancia_polizas.find(param => param.dataValues.id_objeto_x_atributo == objetoAtributoAgenda.id);
							let atributoInstanciaCuenta = instanciaPoliza.atributo_instancia_polizas.find(param => param.dataValues.id_objeto_x_atributo == objetoAtributoCuenta.id);

							if (dataCuentaBanco) {

								if (Object.keys(dataCuentaBanco).length) {
									if (dataCuentaBanco.nrocuenta != undefined) {
										cuentas.push({moneda: dataCuentaBanco.moneda, nrocuenta: dataCuentaBanco.nrocuenta, manejo: dataCuentaBanco.manejo, cod_agenda: dataCuentaBanco.cod_agenda});
									} else {
										let keys = Object.keys(dataCuentaBanco);
										let values = Object.values(dataCuentaBanco);
										cuentas = values;
									}

								} else if (dataCuentaBanco.length) {
									cuentas = dataCuentaBanco;
								}

								let cuentasStr = '', justCuentas = [];
								for (let k = 0 ; k < cuentas.length ; k++) {
									let cuenta = cuentas[k];
									justCuentas.push(cuenta.nrocuenta);
								}
								cuentasStr = justCuentas.toString();

								if (instanciaPoliza.atributo_instancia_polizas.length) {

									if (cuentas.length) {

										if (atributoInstanciaCuenta) {

											let cuenta = cuentas.find(param => param.nrocuenta == atributoInstanciaCuenta.valor);

											// if (atributoInstanciaCodAgenda.valor != dataPersonaBanco.cod_agenda || !cuenta) {

												let issue = await modelos.tmp_cliente_cuenta.findOne({where:{
														ci: doc_id,
														ext: ext
													}});
												if (!issue) {
													let newTmpClienteCuenta = {
														ci:doc_id,
														ext:ext,
														colibri_cod_agenda:atributoInstanciaCodAgenda.valor,
														colibri_cuenta:atributoInstanciaCuenta.valor,
														revision_cod_agenda: dataPersonaBanco.cod_agenda,
														revision_cuenta: cuentasStr,
														id_instancia_poliza: atributoInstanciaCuenta.id_instancia_poliza
													}
													let issue = await modelos.tmp_cliente_cuenta.create(newTmpClienteCuenta);
													response.data.push(issue.dataValues);
												}
											// }
										}
									}
								}

							} else {
								// sin cuenta
								// if (atributoInstanciaCodAgenda.valor != dataPersonaBanco.cod_agenda) {
									let issue = await modelos.tmp_cliente_cuenta.findOne({where:{
											ci: doc_id,
											ext: ext
										}});
									if (!issue) {
										let newTmpClienteCuenta = {
											ci:doc_id,
											ext:ext,
											colibri_cod_agenda:atributoInstanciaCodAgenda.valor,
											colibri_cuenta:atributoInstanciaCuenta.valor,
											revision_cod_agenda: dataPersonaBanco.cod_agenda,
											revision_cuenta: cuentas.toString(),
											id_instancia_poliza: atributoInstanciaCuenta.id_instancia_poliza
										}
										let issue = await modelos.tmp_cliente_cuenta.create(newTmpClienteCuenta);
										response.data.push(issue.dataValues);
									}
								// }
							}
						}
					} else {
						console.log('error: Se encontro una solicitud sin entidad');
					}
				} else {
					console.log('error: Se encontro una solicitud con dos asegurados');
				}
	    }
	  }
  	res.json(response);
  } catch (e) {
    console.log(e);
  }
});

String.prototype.isLetter = function() {
    return this.match(/[a-z]/i);
}

Array.prototype.isInArray = function(value) {
  return this.indexOf(value) > -1;
}
String.prototype.removeAccents = function() {
	var map = {
		'-' : ' ',
		'-' : '_',
		'a' : 'á|à|ã|â|À|Á|Ã|Â',
		'e' : 'é|è|ê|É|È|Ê',
		'i' : 'í|ì|î|Í|Ì|Î',
		'o' : 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
		'u' : 'ú|ù|û|ü|Ú|Ù|Û|Ü',
		'c' : 'ç|Ç',
		'n' : 'ñ|Ñ'
	};
	let str = this;
	for (var pattern in map) {
		str = str.replace(new RegExp(map[pattern], 'g'), pattern);
	};

	return str;
}

function isInt(n){
	return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
	return n === +n && n !== (n|0);
}

module.exports = router;
