const { Router } = require('express');
const { query } = require('../config/create-init');
const router = new Router();
const modelos = require('../modelos');
const modelosDesgravamen = require('../modelos/desgravamen');
const funciones = require('../modulos/funciones');
const generaPdf = require('../modulos/genera-pdf');
const reporteQueryService = require('../servicios/reporteQuery.service');

var isLoggedIn = (req, res, next) => {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(200).json({ "statusCode": 200, "message": "usuario no autentificado" })
}

router.get('/GetWithParametrosByRol/:id',isLoggedIn,async (req, res) => {
    let id = req.params.id;
    id = Array.isArray(id) ? id : id.indexOf(',') ? id.split(',') : id;
    let response = { status: 'OK', message: '', data: '' };
    await modelos.reporte_query.findAll({
        attributes: ['id', 'descripcion','id_rol','estado','id_poliza'],
        where: { id_rol: id },
        include: [{
            model:modelos.reporte_query_parametro,
            required:false,
            include:{
                model:modelos.diccionario,
                requiered:false
            }

        },{
            model:modelos.poliza,
            requiered:false
        }],
        order: [
            ['id_poliza', 'ASC']
        ]
    })
        .then(resp => {
                response.data = resp;
            }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/EjecutarQuery',isLoggedIn,async (req, res) => {
    let reporte_query=req.body;
    let response = { status: 'OK', message: '', data: '' };
    await modelos.reporte_query.findOne({
        where: { id: reporte_query.id },
    })
        .then(async resp => {
                let query = resp.query;
                reporte_query.reporte_query_parametros.forEach(element => {
                    if (element.tipo === 'date') {
                        element.valor = funciones.formatoFechaQuery(element.valor);
                    }
                    query=query.replaceAll(element.nombre, element.valor);
                });
                await modelos.sequelize.query(query)
                    .then(resp1 => {
                            if(Array.isArray(resp1)){
                                response.data=resp1[0]
                            }else{
                                response.data=resp1
                            }
                        }
                    ).catch((err) => {
                        response.status = 'ERROR';
                        response.message = err;
                    });
            }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/executeQueryCertificado',isLoggedIn,async (req, res) => {
    try {
        let {id_poliza} = req.body;
        let {id_instancia_poliza} = req.body;
        let {reporte_query_parametros} = req.body;
        let {nombre_archivo} = req.body;
        let response = { status: 'OK', message: '', data: '' };
        let poliza = await modelos.poliza.findOne({where:{id:id_poliza}});
        let instanciaDocumento = await modelos.instancia_documento.findOne({
            where: {id_instancia_poliza:id_instancia_poliza},
            include: [{
                model: modelos.documento_version,
                include: [{
                    model: modelos.reporte_query
                }, {
                    model: modelos.documento_version_params
                }]
            },{
                model:modelos.documento,
                where:{id_tipo_documento:281}
            }]
        });
        let query;
        if (instanciaDocumento) {
            query = instanciaDocumento.documento_version.reporte_query.query;
            reporte_query_parametros.forEach(element => {
                if (element.tipo === 'date') {
                    element.valor = funciones.formatoFechaQuery(element.valor);
                }
                query = query.replaceAll(element.nombre, element.valor);
            });
            let datos = await modelos.sequelize.query(query);
            if (Array.isArray(datos)) {
                datos = datos[0];
            }
            if (datos.length > 0) {
                response = await generaPdf.generarArchivo(datos, nombre_archivo, instanciaDocumento.documento_version, reporte_query_parametros, poliza);
            } else {
                response.status = "ERROR";
                response.message = "Faltan algunos datos, por favor comuniquese con soporte COLIBRI";
            }
            res.json(response);
        } else {
            response.status = "ERROR";
            response.message = "El certificado de cobertura no pudo ser generado, por favor comuniquese con soporte COLIBRI";
            res.json(response);
        }
    } catch (e) {
        console.log(e)
    }
});

router.post('/executeQueryCrediticios',
    // isLoggedIn,
    async (req, res) => {
    try {
        let {id_deudor} = req.body;
        let {id_documento_version} = req.body;
        let reporte_query_parametros = req.body.reporte_query_parametros ? req.body.reporte_query_parametros : [{nombre:'@param1',tipo:'int', valor:id_deudor}];
        let response = { status: 'OK', message: '', data: '' };
        let solicitud = await modelosDesgravamen.seg_solicitudes.findOne({include:{model:modelosDesgravamen.seg_deudores,as:'sol_deudor',where:{Deu_Id:id_deudor}}});
        let nombre_archivo = req.body.nombre_archivo ? req.body.nombre_archivo : 'crediticios'+solicitud.Sol_NumSol+'_'+id_deudor;
        let documentoVersion = await modelos.documento_version.findOne({
            where: {id: solicitud.sol_deudor[0].id_documento_version},
            include: [{
                model: modelos.reporte_query,
            }, {
                model: modelos.documento_version_params,
                where: {id_padre:{$is:null}},
                include:{
                    model: modelos.documento_version_params,
                    as: 'doc_param_hijos',
                    where: {id_padre:{$not:null}},
                    required: false
                }
            }]
        });
        if (documentoVersion) {
            let query = documentoVersion.reporte_query.query;
            reporte_query_parametros.forEach(element => {
                if (element.tipo === 'date') {
                    element.valor = funciones.formatoFechaQuery(element.valor);
                }
                query = query.replaceAll(element.nombre, element.valor);
            });
            let datos = await modelosDesgravamen.sequelize.query(query);
            if (Array.isArray(datos)) {
                datos = datos[0];
            }
            if (datos.length > 0) {
                response = await generaPdf.generarArchivo(datos, nombre_archivo, documentoVersion, reporte_query_parametros);
            } else {
                response.status = "ERROR";
                response.message = "Reporte no disponible, consulte a soprte COLIBRI";
            }
        } else {
            response.status = "ERROR";
            response.message = "Reporte no disponible, consulte a soprte COLIBRI";
        }
        res.json(response);
    } catch (e) {
        console.log(e)
    }
});

router.post('/executeQuerySolicitud',isLoggedIn,async (req, res) => {
    try {
        let {id_poliza} = req.body;
        let {id_instancia_poliza} = req.body;
        let {reporte_query_parametros} = req.body;
        let {nombre_archivo} = req.body;
        let response = { status: 'OK', message: '', data: '' };
        let poliza = await modelos.poliza.findOne({where:{id:id_poliza}});
        let instanciaDocumento = await modelos.instancia_documento.findOne({
            where: {id_instancia_poliza:id_instancia_poliza},
            include: [{
                model: modelos.documento_version,
                include: [{
                    model: modelos.reporte_query
                }, {
                    model: modelos.documento_version_params,
                    include: {
                        model: modelos.documento_version_params,
                        as:'doc_param_hijos',
                        required: false
                    }
                }]
            },{
                model:modelos.documento,
                where:{id_tipo_documento:279}
            }]
        });
        let query = instanciaDocumento.documento_version.reporte_query.query;
        reporte_query_parametros.forEach(element => {
            if (element.tipo === 'date') {
                element.valor = funciones.formatoFechaQuery(element.valor);
            }
            query=query.replaceAll(element.nombre, element.valor);
        });
        let datos = await modelos.sequelize.query(query);
        if (Array.isArray(datos)) {
            datos = datos[0];
        }
        if (datos.length > 0) {
            response = await generaPdf.generarArchivo(datos, nombre_archivo, instanciaDocumento.documento_version, reporte_query_parametros, poliza);
        } else {
            response.status = "ERROR";
            response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
        }
        res.json(response);
    } catch (e) {
        console.log(e);
    }
});

router.post('/executeQueryAnulacion',isLoggedIn,async (req, res) => {
    try {
        let {id_poliza} = req.body;
        let {id_instancia_poliza} = req.body;
        let {reporte_query_parametros} = req.body;
        let {nombre_archivo} = req.body;
        let response = { status: 'OK', message: '', data: '' };
        let poliza = await modelos.poliza.findOne({where:{id:id_poliza}});
        let instanciaDocumento = await modelos.instancia_documento.findOne({
            where: {id_instancia_poliza:id_instancia_poliza},
            include: [{
                model: modelos.documento_version,
                include: [{
                    model: modelos.reporte_query
                }, {
                    model: modelos.documento_version_params,
                    include: {
                        model: modelos.documento_version_params,
                        as:'doc_param_hijos',
                        required: false
                    }
                }]
            },{
                model:modelos.documento,
                where:{id_tipo_documento:348}
            }]
        });
        if(instanciaDocumento) {
            let query = instanciaDocumento.documento_version.reporte_query.query;
            reporte_query_parametros.forEach(element => {
                if (element.tipo === 'date') {
                    element.valor = funciones.formatoFechaQuery(element.valor);
                }
                query=query.replaceAll(element.nombre, element.valor);
            });
            let datos = await modelos.sequelize.query(query);
            if (Array.isArray(datos)) {
                datos = datos[0];
            }
            if (datos.length > 0) {
                response = await generaPdf.generarArchivo(datos, nombre_archivo, instanciaDocumento.documento_version, reporte_query_parametros, poliza);
            } else {
                response.status = "ERROR";
                response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
            }
        }
        res.json(response);
    } catch (e) {
        console.log(e);
    }
});

router.post('/executeQueryCarta',isLoggedIn,async (req, res) => {
    try {
        let {id_poliza} = req.body;
        let {id_instancia_poliza} = req.body;
        let {reporte_query_parametros} = req.body;
        let poliza = await modelos.poliza.findOne({where:{id:id_poliza}});
        let {nombre_archivo} = req.body;
        let response = { status: 'OK', message: '', data: '' };
        let documentoVersion = await modelos.documento_version.findOne({
            where:{id:47},
            include: [{
                model: modelos.reporte_query
            }, {
                model: modelos.documento_version_params,
                include: {
                    model: modelos.documento_version_params,
                    as:'doc_param_hijos',
                    required: false
                }
            }]
        });
        let query = documentoVersion.reporte_query.query;
        reporte_query_parametros.forEach(element => {
            if (element.tipo === 'date') {
                element.valor = funciones.formatoFechaQuery(element.valor);
            }
            query=query.replaceAll(element.nombre, element.valor);
        });
        let datos = await modelos.sequelize.query(query);
        if (Array.isArray(datos)) {
            datos = datos[0];
        }
        if (datos.length > 0) {
            response = await generaPdf.generarArchivo(datos, nombre_archivo, documentoVersion, reporte_query_parametros, poliza);
        } else {
            response.status = "ERROR";
            response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
        }
        res.json(response);
    } catch (e) {
        console.log(e);
    }
});

router.post('/executeQueryOrdenPago',isLoggedIn,async (req, res) => {
    try {
        let {id_poliza} = req.body;
        let {id_instancia_poliza} = req.body;
        let {reporte_query_parametros} = req.body;
        let {nombre_archivo} = req.body;
        let response = { status: 'OK', message: '', data: '' };
        let poliza = await modelos.poliza.findOne({where:{id:id_poliza}});
        let instanciaDocumento = await modelos.instancia_documento.findOne({
            where: {id_instancia_poliza:id_instancia_poliza},
            include:[{
                model:modelos.documento_version,
                include: [{
                    model:modelos.reporte_query
                },{
                    model:modelos.documento_version_params
                }]
            },{
                model:modelos.documento,
                where:{id_tipo_documento:280}
            }]
        });
        let query = instanciaDocumento.documento_version.reporte_query.query;
        reporte_query_parametros.forEach(element => {
            if (element.tipo === 'date') {
                element.valor = funciones.formatoFechaQuery(element.valor);
            }
            query=query.replaceAll(element.nombre, element.valor);
        });
        let datos = await modelos.sequelize.query(query);
        if (Array.isArray(datos)) {
            datos = datos[0];
        }
        if (datos.length > 0) {
            response = await generaPdf.generarArchivo(datos, nombre_archivo, instanciaDocumento.documento_version, reporte_query_parametros, poliza);
        } else {
            response.status = "ERROR";
            response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
        }
        res.json(response);
    } catch (e) {
        console.log(e);
    }
});

router.post('/ReporteCobertura',isLoggedIn,async (req, res) => {
    let objeto = req.body;
    fecha_ini=funciones.formatoFechaQuery(objeto.fecha_ini);
    fecha_fin=funciones.formatoFechaQuery(objeto.fecha_fin);
    let response = { status: 'OK', message: '', data: '' };
    await modelos.sequelize.query(`select D1.nro_documento,format(D1.fecha_emision,'dd-MM-yyyy') fecha_emision
        ,format(D1.fecha_fin_vigencia,'dd-MM-yyyy') fecha_fin_vigencia
        ,format(D1.fecha_inicio_vigencia,'dd-MM-yyyy') fecha_inicio_vigencia
        ,isnull(D.persona_primer_apellido,'')+' '+isnull(D.persona_segundo_apellido,'')+' '+isnull(D.persona_primer_nombre,'')+' '+isnull(D.persona_segundo_nombre,'') asegurado
        ,RIGHT('00' + Ltrim(Rtrim(datediff(day,format(getdate(),'yyyy-MM-dd'),format(D1.fecha_fin_vigencia,'yyyy-MM-dd')))),2) dias_vigencia
        ,K.parametro_descripcion agencia
        ,L.parametro_descripcion sucursal
        ,K.parametro_abreviacion tipo_agencia	
        ,M.usuario_nombre_completo
        ,M.usuario_login
        from instancia_poliza A 
        inner join poliza A1 on A.id_poliza=A1.id and A.id_estado=59
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on B.id_entidad=C.id
        inner join persona D on D.id=C.id_persona
        inner join instancia_documento D1 on D1.id_instancia_poliza=A.id and D1.id_documento in (3,6,9,12,15,18,21,24) and D1.fecha_fin_vigencia between '${fecha_ini}' and '${fecha_fin}'
        inner join objeto E on E.id_poliza=A.id_poliza and E.id_poliza=${objeto.id_poliza}
        inner join objeto_x_atributo F on F.id_objeto=E.id and F.id_atributo=5 --cuenta
        inner join objeto_x_atributo G on G.id_objeto=E.id and G.id_atributo=4 --correo
        inner join objeto_x_atributo I on I.id_objeto=E.id and I.id_atributo=59 --agencia
        inner join atributo_instancia_poliza I1 on I1.id_objeto_x_atributo=I.id and I1.id_instancia_poliza=A.id
        inner join objeto_x_atributo J on J.id_objeto=E.id and J.id_atributo=60 --sucursal
        inner join atributo_instancia_poliza J1 on J1.id_objeto_x_atributo=J.id and J1.id_instancia_poliza=A.id
        left join parametro K on K.parametro_cod=I1.valor and K.diccionario_id=40
        left join parametro L on L.parametro_cod=J1.valor and L.diccionario_id=38
        left join usuario M on A.adicionada_por=M.id
        `)
        .then(resp => {
                response.data = resp[0];
            }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

module.exports = router;
