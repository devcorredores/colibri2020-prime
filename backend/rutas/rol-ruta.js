const { Router } = require('express');
const router = new Router();
const bcrypt = require('bcrypt-nodejs');
const modelos = require("../modelos");

router.get('/', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await modelos.rol.findAll()
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/rolusuario/:id_usuario', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id_usuario = req.params.id_usuario;
    await modelos.usuario_x_rol.findAll({ where: { id_usuario } })
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/byid/:id', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await modelos.rol.findAll({ where: { id } })
        .then(result => {
            response.data = result;
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});


router.post('/guardar_usuarioxrol', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id_usuario = req.body.id_usuario;
    const id_rol = req.body.id_rol;
    modelos.usuario_x_rol.create({ id_usuario, id_rol})
        .then(usuario => {
            response.data = usuario;
            response.message = 'rol usuario creado';
            return res.status(201).send({ message: 'UsuarioxRol creado' })
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
            return res.status(201).send({ message: 'UsuarioxRol no creado' })
        });
});

router.post('/ResgistrarEmparejamiento', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const usuarios = req.body;

    for (let usuario of usuarios){
        for(let rol of usuario.roles_adicionar)
        {
            let usuario_x_rol={
            id_rol:rol.id,
            id_usuario:usuario.id,
            adicionado_por:usuario.adicionado_por,
            modificado_por:usuario.modificado_por,
            }
            await modelos.usuario_x_rol.create(usuario_x_rol)
            .then(usuario => {
                response.data = usuario;
                response.message = 'rol usuario creado';
            })
            .catch((err) => {
                response.status = 'ERROR';
                response.message = err;
                res.json(response);
            });
        }
    }
    res.json(response);
});

router.post('/eliminarRol_x_Perfil', async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id_usuario = req.body.id_usuario;
    const id_rol = req.body.id_rol;
    modelos.usuario_x_rol.destroy({where:{id_usuario,id_rol}})
        .then(usuario => {
            response.data = usuario;
            response.message = 'rol usuario eliminado';
            return res.status(201).send({ message: 'UsuarioxRol eliminado' })
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
            return res.status(201).send({ message: 'UsuarioxRol no eliminado' })
        });
});

module.exports = router