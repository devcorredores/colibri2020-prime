require('../utils/Prototipes');
const { Router } = require('express');
const router = new Router();
const modelos = require('../modelos');
const generaPdf = require('../modulos/genera-pdf');
const personaRuta = require('./persona-ruta');
const reporteService = require('../servicios/reporte.service');
const Util = require('../utils/util');
const util = new Util();

var isLoggedIn = (req, res, next) => {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
}

router.get('/FindPersonaSolicitudByIdInstanciaPolizaAndAnexos/:idInstanciaPoliza', isLoggedIn, async (req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    let idInstanciaPoliza = req.params.idInstanciaPoliza;
    await FindPersonaSolicitudByIdInstanciaPolizaAndAnexos(idInstanciaPoliza, async resp => {
        response.data = resp;
    });
    res.json(response);
});

router.get('/', async (req, res) => {
    let response = { status: 'OK', message: 'reportes', data: '' };
    res.json(response);
});

// {"id_solicitud":54, "tipo":"certificado"}
router.post('/certificado_ecoaguinaldo', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select distinct A.id,A.id_poliza,H.nro_documento,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_doc_id,D.persona_doc_id_comp,J.parametro_abreviacion persona_doc_id_ext,D.persona_fecha_nacimiento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia,G.persona_primer_apellido persona_primer_apellido_b,G.persona_segundo_apellido persona_segundo_apellido_b,G.persona_primer_nombre persona_primer_nombre_b,G.persona_segundo_nombre persona_segundo_nombre_b,E.porcentaje,I.parametro_descripcion,N.monto_prima,N.monto_capital 
        from instancia_poliza A      
        inner join anexo_poliza N on A.id_poliza = N.id_poliza
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join beneficiario E on B.id=E.id_asegurado and E.estado = 83
        inner join entidad F on E.id_beneficiario=F.id
        inner join persona G on F.id_persona=G.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
        inner join parametro I on I.id=E.id_parentesco
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
        and A.id=${id} and (H.id_documento=3 or H.id_documento=6)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    // response = await generaPdf.generarCertificadoEcoaguinaldo(datos, 2, nombre_archivo);
                    response = await generaPdf.generarCertificadoEcoriesgo(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    res.json(response);
});

router.post('/certificado_ecoriesgo', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select distinct A.id,A.id_poliza,H.nro_documento,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_doc_id,D.persona_doc_id_comp,J.parametro_abreviacion persona_doc_id_ext,D.persona_fecha_nacimiento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia,G.persona_primer_apellido persona_primer_apellido_b,G.persona_segundo_apellido persona_segundo_apellido_b,G.persona_primer_nombre persona_primer_nombre_b,G.persona_segundo_nombre persona_segundo_nombre_b,E.porcentaje,I.parametro_descripcion, N.monto_prima, N.monto_capital 
        from instancia_poliza A 
        inner join anexo_poliza N on A.id_poliza = N.id_poliza 
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join beneficiario E on B.id=E.id_asegurado and E.estado = 83
        inner join entidad F on E.id_beneficiario=F.id
        inner join persona G on F.id_persona=G.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
        inner join parametro I on I.id=E.id_parentesco
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
        and A.id=${id} and (H.id_documento=3 or H.id_documento=6 or H.id_documento=27)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarCertificadoEcoriesgo(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    res.json(response);
});

router.post('/OrdenPago', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select K.usuario_nombre_completo,A.id_poliza,A.id,H.nro_documento,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_doc_id,D.persona_doc_id_comp,J.parametro_abreviacion persona_doc_id_ext,D.persona_fecha_nacimiento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia,L.persona_primer_apellido persona_primer_apellido_a, L.persona_segundo_apellido persona_segundo_apellido_a,L.persona_primer_nombre persona_primer_nombre_a, L.persona_segundo_nombre persona_segundo_nombre_a, M.nro_documento nro_documento_sol,CONVERT (varchar, H.fecha_emision, 103)+' '+CONVERT (varchar, H.fecha_emision, 108) fecha_emision, N.monto_capital, N.monto_prima
        from instancia_poliza A 
        inner join anexo_poliza N on A.id_poliza=N.id_poliza
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
		inner join usuario K on K.id=H.adicionada_por
		inner join persona L on L.id=K.id_persona
		inner join instancia_documento M on M.id_instancia_poliza=${id}
        and A.id=${id} and (H.id_documento=2 or H.id_documento=5) and (M.id_documento=1 or M.id_documento=4)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    // response = await generaPdf.generarOrdenCobroEcoaguinaldo(datos, 2, nombre_archivo);
                    response = await generaPdf.generarOrdenCobroEcoriesgo(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "La poliza tiene que tener al menos un ASEGURADO";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    res.json(response);
});

router.post('/OrdenPagoEcoriesgo', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select K.usuario_nombre_completo,A.id_poliza,A.id,H.nro_documento,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_doc_id,D.persona_doc_id_comp,J.parametro_abreviacion persona_doc_id_ext,D.persona_fecha_nacimiento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia,L.persona_primer_apellido persona_primer_apellido_a, L.persona_segundo_apellido persona_segundo_apellido_a,L.persona_primer_nombre persona_primer_nombre_a, L.persona_segundo_nombre persona_segundo_nombre_a, M.nro_documento nro_documento_sol,CONVERT (varchar, H.fecha_emision, 103)+' '+CONVERT (varchar, H.fecha_emision, 108) fecha_emision, N.monto_capital, N.monto_prima
        from instancia_poliza A 
        inner join anexo_poliza N on A.id_poliza=N.id_poliza
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
		inner join usuario K on K.id=H.adicionada_por
		inner join persona L on L.id=K.id_persona
		inner join instancia_documento M on M.id_instancia_poliza=${id}
        and A.id=${id} and (H.id_documento=2 or H.id_documento=5 or H.id_documento=26) and (M.id_documento=1 or M.id_documento=4 or M.id_documento=25)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarOrdenCobroEcoriesgo(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Hubo un error en la solicitud, por favor contactate con el administrador";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    res.json(response);
});

router.post('/OrdenPagoEcovida', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select K.usuario_nombre_completo,A.id_poliza,A.id,H.nro_documento,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_doc_id,D.persona_doc_id_comp,J.parametro_abreviacion persona_doc_id_ext,D.persona_fecha_nacimiento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia,L.persona_primer_apellido persona_primer_apellido_a, L.persona_segundo_apellido persona_segundo_apellido_a,L.persona_primer_nombre persona_primer_nombre_a, L.persona_segundo_nombre persona_segundo_nombre_a, M.nro_documento nro_documento_sol,CONVERT (varchar, H.fecha_emision, 103)+' '+CONVERT (varchar, H.fecha_emision, 108) fecha_emision
        from instancia_poliza A 
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
		inner join usuario K on K.id=H.adicionada_por
		inner join persona L on L.id=K.id_persona
		inner join instancia_documento M on M.id_instancia_poliza=${id}
        and A.id=${id} and (H.id_documento=17) and (M.id_documento=16)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarOrdenCobroEcoVida(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "La poliza tiene que tener al menos un ASEGURADO";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    res.json(response);
});

router.post('/OrdenPagoEcoAccidentes', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select K.usuario_nombre_completo,A.id_poliza,A.id,H.nro_documento,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_doc_id,D.persona_doc_id_comp,J.parametro_abreviacion persona_doc_id_ext,D.persona_fecha_nacimiento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia,L.persona_primer_apellido persona_primer_apellido_a, L.persona_segundo_apellido persona_segundo_apellido_a,L.persona_primer_nombre persona_primer_nombre_a, L.persona_segundo_nombre persona_segundo_nombre_a, M.nro_documento nro_documento_sol,CONVERT (varchar, H.fecha_emision, 103)+' '+CONVERT (varchar, H.fecha_emision, 108) fecha_emision
        from instancia_poliza A 
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
		inner join usuario K on K.id=H.adicionada_por
		inner join persona L on L.id=K.id_persona
		inner join instancia_documento M on M.id_instancia_poliza=${id}
        and A.id=${id} and (H.id_documento=29) and (M.id_documento=28)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarOrdenCobroEcoAccidentes(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "La poliza tiene que tener al menos un ASEGURADO";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    res.json(response);
});

router.post('/OrdenPagoEcoResguardo', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select K.usuario_nombre_completo,A.id_poliza,A.id,H.nro_documento,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_doc_id,D.persona_doc_id_comp,J.parametro_abreviacion persona_doc_id_ext,D.persona_fecha_nacimiento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia,L.persona_primer_apellido persona_primer_apellido_a, L.persona_segundo_apellido persona_segundo_apellido_a,L.persona_primer_nombre persona_primer_nombre_a, L.persona_segundo_nombre persona_segundo_nombre_a, M.nro_documento nro_documento_sol,CONVERT (varchar, H.fecha_emision, 103)+' '+CONVERT (varchar, H.fecha_emision, 108) fecha_emision
        from instancia_poliza A 
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
		inner join usuario K on K.id=H.adicionada_por
		inner join persona L on L.id=K.id_persona
		inner join instancia_documento M on M.id_instancia_poliza=${id}
        and A.id=${id} and (H.id_documento=29) and (M.id_documento=28)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarOrdenCobroEcoResguardo(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "La poliza tiene que tener al menos un ASEGURADO";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    res.json(response);
});

router.post('/OrdenPagoEcoMedic', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select K.usuario_nombre_completo,A.id_poliza,A.id,H.nro_documento,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_doc_id,D.persona_doc_id_comp,J.parametro_abreviacion persona_doc_id_ext,D.persona_fecha_nacimiento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia,L.persona_primer_apellido persona_primer_apellido_a, L.persona_segundo_apellido persona_segundo_apellido_a,L.persona_primer_nombre persona_primer_nombre_a, L.persona_segundo_nombre persona_segundo_nombre_a, M.nro_documento nro_documento_sol,CONVERT (varchar, H.fecha_emision, 103)+' '+CONVERT (varchar, H.fecha_emision, 108) fecha_emision
        from instancia_poliza A 
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
		inner join usuario K on K.id=H.adicionada_por
		inner join persona L on L.id=K.id_persona
		inner join instancia_documento M on M.id_instancia_poliza=${id}
        and A.id=${id} and (H.id_documento=23) and (M.id_documento=22)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarOrdenCobroEcoMedic(datos, 3, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "La poliza tiene que tener al menos un ASEGURADO";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    res.json(response);
});

router.post('/ReporteSolicitud', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select A.id_poliza,A.id,D.persona_direccion_domicilio,D.persona_celular,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_apellido_casada,D.persona_doc_id,D.persona_doc_id_comp,D.persona_fecha_nacimiento
        ,G.persona_primer_apellido persona_primer_apellido_b,G.persona_segundo_apellido persona_segundo_apellido_b,G.persona_primer_nombre persona_primer_nombre_b,G.persona_segundo_nombre persona_segundo_nombre_b,G.persona_doc_id persona_doc_id_b,G.persona_celular persona_celular_b
		,isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=8 and id_instancia_poliza = A.id),'') cta
		,I.parametro_descripcion parentesco,K.parametro_descripcion tipo_documento,L.parametro_descripcion sexo,J.parametro_abreviacion persona_doc_id_ext,I.parametro_descripcion
		,H.fecha_emision,H.nro_documento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia
		,E.porcentaje
        from instancia_poliza A 
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join beneficiario E on B.id=E.id_asegurado and E.estado = 83
        inner join entidad F on E.id_beneficiario=F.id
        inner join persona G on F.id_persona=G.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
        inner join parametro I on I.id=E.id_parentesco
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
		inner join parametro k on k.id=D.par_tipo_documento_id
		inner join parametro L on L.id=D.par_sexo_id
        and A.id=${id} and (H.id_documento=1 or H.id_documento=4)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    // response = await generaPdf.generarReporteSolicitud(datos, 2, nombre_archivo);
                    response = await generaPdf.generarReporteSolicitud(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR
    res.json(response);
});

router.post('/ReporteSolicitudEcoRiesgo', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select A.id_poliza,A.id,D.persona_direccion_domicilio,D.persona_celular,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_apellido_casada,D.persona_doc_id,D.persona_doc_id_comp,D.persona_fecha_nacimiento
        ,G.persona_primer_apellido persona_primer_apellido_b,G.persona_segundo_apellido persona_segundo_apellido_b,G.persona_primer_nombre persona_primer_nombre_b,G.persona_segundo_nombre persona_segundo_nombre_b,G.persona_doc_id persona_doc_id_b,G.persona_celular persona_celular_b
		,isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=273 and id_instancia_poliza=A.id),'') cta
		,I.parametro_descripcion parentesco,K.parametro_descripcion tipo_documento,L.parametro_descripcion sexo,J.parametro_abreviacion persona_doc_id_ext,I.parametro_descripcion
		,H.fecha_emision,H.nro_documento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia
		,E.porcentaje
        from instancia_poliza A 
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
        inner join beneficiario E on B.id=E.id_asegurado and E.estado = 83
        inner join entidad F on E.id_beneficiario=F.id
        inner join persona G on F.id_persona=G.id
        inner join instancia_documento H on A.id=H.id_instancia_poliza
        inner join parametro I on I.id=E.id_parentesco
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
		inner join parametro k on k.id=D.par_tipo_documento_id
		inner join parametro L on L.id=D.par_sexo_id
        and A.id=${id} and (H.id_documento=1 or H.id_documento=4 or H.id_documento=25)`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarReporteSolicitudEcoRiesgo(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR
    res.json(response);
});

router.post('/ReporteSolicitudEcoVida', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;
    let datos = {};
    await modelos.sequelize.query(`select A.id_poliza,A.id,D.persona_direccion_domicilio,D.persona_celular,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_apellido_casada,D.persona_doc_id,D.persona_doc_id_comp,D.persona_fecha_nacimiento
    ,G.persona_primer_apellido persona_primer_apellido_b,G.persona_segundo_apellido persona_segundo_apellido_b,G.persona_primer_nombre persona_primer_nombre_b,G.persona_segundo_nombre persona_segundo_nombre_b,G.persona_doc_id persona_doc_id_b,G.persona_celular persona_celular_b, G.persona_doc_id_ext persona_doc_id_ext_b,Z.parametro_abreviacion ext_b,
    isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=176 and id_instancia_poliza=A.id),'') cta
    ,I.parametro_descripcion parentesco,K.parametro_descripcion tipo_documento,L.parametro_descripcion sexo,J.parametro_abreviacion persona_doc_id_ext,I.parametro_descripcion
    ,H.fecha_emision,H.nro_documento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia
    ,E.porcentaje,
    Y.parametro_descripcion sucursal,
    Y1.parametro_descripcion agencia
    from instancia_poliza A 
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join instancia_documento H on A.id=H.id_instancia_poliza
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
    inner join parametro k on k.id=D.par_tipo_documento_id
    inner join parametro L on L.id=D.par_sexo_id
    and A.id=${id} and (H.id_documento=16)
    left join beneficiario E on B.id=E.id_asegurado and E.estado = 83 and E.tipo = 86
    left join entidad F on E.id_beneficiario=F.id
    left join persona G on F.id_persona=G.id
    left join parametro Z on Z.parametro_cod=G.persona_doc_id_ext and Z.diccionario_id=18
    left join parametro I on I.id=E.id_parentesco
    left join atributo_instancia_poliza X on X.id_instancia_poliza=A.id and X.id_objeto_x_atributo=192
    left join parametro Y on Y.parametro_cod=X.valor and Y.diccionario_id=38
    left join atributo_instancia_poliza X1 on X1.id_instancia_poliza=A.id and X1.id_objeto_x_atributo=191
    left join parametro Y1 on Y1.parametro_cod=X1.valor and Y1.diccionario_id=40
    `)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarReporteSolicitudEcoVida(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR
    res.json(response);
});

router.post('/ReporteSolicitudEcoAccidentes', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    await modelos.sequelize.query(`select A.id_poliza,A.id,
D.persona_direccion_domicilio
     ,D.persona_celular
     ,D.persona_primer_apellido
     ,D.persona_segundo_apellido
     ,D.persona_primer_nombre
     ,D.persona_segundo_nombre
     ,D.persona_apellido_casada
     ,D.persona_doc_id
     ,D.persona_doc_id_comp
     ,D.persona_fecha_nacimiento
     ,D.persona_telefono_domicilio
     ,datediff(YYYY,D.persona_fecha_nacimiento,getdate()) as asegurado_edad
     ,X2.valor as asegurado_ciudad_nacimiento
     ,X3.valor as asegurado_correo
     ,X4.valor as asegurado_estado_civil
    ,G.persona_primer_apellido persona_primer_apellido_b
    ,G.persona_segundo_apellido persona_segundo_apellido_b
    ,G.persona_primer_nombre persona_primer_nombre_b
    ,G.persona_segundo_nombre persona_segundo_nombre_b
    ,G.persona_doc_id persona_doc_id_b
    ,G.persona_celular persona_celular_b
    ,G.persona_doc_id_ext persona_doc_id_ext_b
    ,Z.parametro_abreviacion ext_b
    ,isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=298 and id_instancia_poliza=A.id),'') cta
    ,I.parametro_descripcion parentesco
    ,M.parametro_descripcion condicion
    ,N.parametro_descripcion estado
    ,K.parametro_descripcion tipo_documento
    ,L.parametro_descripcion sexo
    ,L.id sexo_id
    ,J.parametro_abreviacion persona_doc_id_ext
    ,I.parametro_descripcion
    ,H.fecha_emision
    ,H.nro_documento
    ,H.fecha_inicio_vigencia
    ,H.fecha_fin_vigencia
    ,E.porcentaje
    ,E.condicion
    ,E.estado
    ,Y.parametro_descripcion sucursal
    ,Y1.parametro_descripcion agencia
    ,X5.valor as metodo_pago
    ,X6.valor as nro_solicitud_sci
    ,X7.valor as prima_total
    ,X8.valor as forma_pago
    from instancia_poliza A 
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join instancia_documento H on A.id=H.id_instancia_poliza
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
    inner join parametro k on k.id=D.par_tipo_documento_id
    inner join parametro L on L.id=D.par_sexo_id and A.id=${id} and (H.id_documento=28)
    left join beneficiario E on B.id=E.id_asegurado and E.estado = 83 and E.tipo = 86
    left join entidad F on E.id_beneficiario=F.id
    left join persona G on F.id_persona=G.id
    left join parametro M on E.condicion=M.id and M.diccionario_id=58
    left join parametro N on E.estado=M.id and M.diccionario_id=25
    left join parametro Z on Z.parametro_cod=G.persona_doc_id_ext and Z.diccionario_id=18
    left join parametro I on I.id=E.id_parentesco
    left join atributo_instancia_poliza X on X.id_instancia_poliza=A.id and X.id_objeto_x_atributo=314
    left join parametro Y on Y.parametro_cod=X.valor and Y.diccionario_id=38
    left join atributo_instancia_poliza X2 on X2.id_instancia_poliza=A.id and X2.id_objeto_x_atributo=320
    left join atributo_instancia_poliza X3 on X3.id_instancia_poliza=A.id and X3.id_objeto_x_atributo=305
    left join atributo_instancia_poliza X4 on X4.id_instancia_poliza=A.id and X4.id_objeto_x_atributo=306
    left join atributo_instancia_poliza X5 on X5.id_instancia_poliza=A.id and X5.id_objeto_x_atributo=299
    left join atributo_instancia_poliza X6 on X6.id_instancia_poliza=A.id and X6.id_objeto_x_atributo=324
    left join atributo_instancia_poliza X1 on X1.id_instancia_poliza=A.id and X1.id_objeto_x_atributo=313
    left join atributo_instancia_poliza X7 on X7.id_instancia_poliza=A.id and X7.id_objeto_x_atributo=335
    left join atributo_instancia_poliza X8 on X8.id_instancia_poliza=A.id and X8.id_objeto_x_atributo=336
    left join parametro Y1 on Y1.parametro_cod=X1.valor and Y1.diccionario_id=40
   `)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarReporteSolicitudEcoAccidentes(datos, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR
    res.json(response);
});

router.post('/ReporteSolicitudEcoResguardo', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    await modelos.sequelize.query(`select A.id_poliza,A.id,
D.persona_direccion_domicilio,
D.persona_celular,
D.persona_primer_apellido,
D.persona_segundo_apellido,
D.persona_primer_nombre,
D.persona_segundo_nombre,
D.persona_apellido_casada,
D.persona_doc_id,
D.persona_doc_id_comp,
D.persona_fecha_nacimiento,
D.persona_telefono_domicilio,
datediff(YYYY,D.persona_fecha_nacimiento,getdate()) as asegurado_edad,
X2.valor as asegurado_ciudad_nacimiento,
X3.valor as asegurado_correo,
X4.valor as asegurado_estado_civil
    ,G.persona_primer_apellido persona_primer_apellido_b
    ,G.persona_segundo_apellido persona_segundo_apellido_b
    ,G.persona_primer_nombre persona_primer_nombre_b
    ,G.persona_segundo_nombre persona_segundo_nombre_b
    ,G.persona_doc_id persona_doc_id_b
    ,G.persona_celular persona_celular_b
    ,G.persona_doc_id_ext persona_doc_id_ext_b
    ,Z.parametro_abreviacion ext_b
    ,isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=331 and id_instancia_poliza=A.id),'') cta
    ,I.parametro_descripcion parentesco
    ,M.parametro_descripcion condicion
    ,N.parametro_descripcion estado
    ,K.parametro_descripcion tipo_documento
    ,L.parametro_descripcion sexo
    ,L.id sexo_id
    ,J.parametro_abreviacion persona_doc_id_ext
    ,I.parametro_descripcion
    ,H.fecha_emision
    ,H.nro_documento
    ,H.fecha_inicio_vigencia
    ,H.fecha_fin_vigencia
    ,E.porcentaje
    ,E.condicion
    ,E.estado
    ,Y.parametro_descripcion sucursal
    ,Y1.parametro_descripcion agencia
    ,X5.valor as metodo_pago
    ,X6.valor as nro_solicitud_sci
    ,X7.valor as prima_total
    ,X8.valor as forma_pago
    from instancia_poliza A 
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join instancia_documento H on A.id=H.id_instancia_poliza
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
    inner join parametro k on k.id=D.par_tipo_documento_id
    inner join parametro L on L.id=D.par_sexo_id and A.id=${id} and (H.id_documento=31  )
    left join beneficiario E on B.id=E.id_asegurado and E.estado = 83 and E.tipo = 86
    left join entidad F on E.id_beneficiario=F.id
    left join persona G on F.id_persona=G.id
    left join parametro M on E.condicion=M.id and M.diccionario_id=58
    left join parametro N on E.estado=M.id and M.diccionario_id=25
    left join parametro Z on Z.parametro_cod=G.persona_doc_id_ext and Z.diccionario_id=18
    left join parametro I on I.id=E.id_parentesco
    left join atributo_instancia_poliza X on X.id_instancia_poliza=A.id and X.id_objeto_x_atributo=350
    left join parametro Y on Y.parametro_cod=X.valor and Y.diccionario_id=38
    left join atributo_instancia_poliza X2 on X2.id_instancia_poliza=A.id and X2.id_objeto_x_atributo=351
    left join atributo_instancia_poliza X3 on X3.id_instancia_poliza=A.id and X3.id_objeto_x_atributo=330
    left join atributo_instancia_poliza X4 on X4.id_instancia_poliza=A.id and X4.id_objeto_x_atributo=329
    left join atributo_instancia_poliza X5 on X5.id_instancia_poliza=A.id and X5.id_objeto_x_atributo=337
    left join atributo_instancia_poliza X6 on X6.id_instancia_poliza=A.id and X6.id_objeto_x_atributo=360
    left join atributo_instancia_poliza X1 on X1.id_instancia_poliza=A.id and X1.id_objeto_x_atributo=349
    left join atributo_instancia_poliza X7 on X7.id_instancia_poliza=A.id and X7.id_objeto_x_atributo=367
    left join atributo_instancia_poliza X8 on X8.id_instancia_poliza=A.id and X8.id_objeto_x_atributo=368
    left join parametro Y1 on Y1.parametro_cod=X1.valor and Y1.diccionario_id=40
   `)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarReporteSolicitudEcoResguardo(datos, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR
    res.json(response);
});

router.post('/ReporteSolicitudEcoProteccion', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    await modelos.sequelize.query(`select Y2.parametro_descripcion modalidad,X2.valor cod_modalidad,A1.descripcion poliza,A.id_poliza,A.id,D.persona_direccion_domicilio,D.persona_celular,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_apellido_casada,D.persona_doc_id,D.persona_doc_id_comp,D.persona_fecha_nacimiento
    ,G.persona_primer_apellido persona_primer_apellido_b,G.persona_segundo_apellido persona_segundo_apellido_b,G.persona_primer_nombre persona_primer_nombre_b,G.persona_segundo_nombre persona_segundo_nombre_b,G.persona_doc_id persona_doc_id_b,G.persona_celular persona_celular_b, G.persona_doc_id_ext persona_doc_id_ext_b,Z.parametro_abreviacion ext_b
    ,isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=201 and id_instancia_poliza=A.id),'') cta
    ,I.parametro_descripcion parentesco,K.parametro_descripcion tipo_documento,L.parametro_descripcion sexo,J.parametro_abreviacion persona_doc_id_ext,I.parametro_descripcion
    ,H.fecha_emision,H.nro_documento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia
    ,E.porcentaje,
    Y.parametro_descripcion sucursal,
    Y1.parametro_descripcion agencia
    from instancia_poliza A 
    inner join poliza A1 on A1.id=A.id_poliza
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join instancia_documento H on A.id=H.id_instancia_poliza
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
    inner join parametro k on k.id=D.par_tipo_documento_id
    inner join parametro L on L.id=D.par_sexo_id
    inner join atributo_instancia_poliza X2 on X2.id_instancia_poliza=A.id and X2.id_objeto_x_atributo=234
    inner join parametro Y2 on Y2.id=X2.valor and Y2.diccionario_id=31
    and A.id=${id} and (H.id_documento in (19))
    left join beneficiario E on B.id=E.id_asegurado and E.estado = 83
    left join entidad F on E.id_beneficiario=F.id
    left join persona G on F.id_persona=G.id
    left join parametro Z on Z.parametro_cod=G.persona_doc_id_ext and Z.diccionario_id=18
    left join parametro I on I.id=E.id_parentesco
    left join atributo_instancia_poliza X on X.id_instancia_poliza=A.id and X.id_objeto_x_atributo in (217)
    left join parametro Y on Y.parametro_cod=X.valor and Y.diccionario_id=38
    left join atributo_instancia_poliza X1 on X1.id_instancia_poliza=A.id and X1.id_objeto_x_atributo in (216)
    left join parametro Y1 on Y1.parametro_cod=X1.valor and Y1.diccionario_id=40`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarReporteSolicitudEcoProteccion(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR
    res.json(response);
});

router.post('/ReporteSolicitudTarjeta', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize.query(`
        select A.id_poliza
        ,K.nro_documento
        ,A.id
        ,A.fecha_registro
        ,D.persona_direccion_domicilio 
        ,D.persona_primer_apellido
        ,D.persona_segundo_apellido
        ,D.persona_primer_nombre
        ,D.persona_celular
        ,D.persona_segundo_nombre
        ,D.persona_apellido_casada
        ,D.persona_doc_id
        ,D.persona_doc_id_comp
        ,J.parametro_abreviacion persona_doc_id_ext
        ,D.persona_fecha_nacimiento
        ,L.parametro_descripcion sexo
        ,G.persona_primer_apellido persona_primer_apellido_b
        ,G.persona_segundo_apellido persona_segundo_apellido_b
        ,G.persona_primer_nombre persona_primer_nombre_b
        ,G.persona_segundo_nombre persona_segundo_nombre_b
        ,G.persona_doc_id persona_doc_id_b
        ,G.persona_celular persona_celular_b
        ,G.persona_doc_id_ext persona_doc_id_ext_b
        ,E.porcentaje
        ,I.parametro_descripcion parentesco
        ,Z.parametro_abreviacion ext_b
        ,H.parametro_descripcion tipo_documento
        ,Y.parametro_descripcion sucursal
        ,Y1.parametro_descripcion agencia
        ,N.monto_prima
        ,M.parametro_abreviacion as moneda_prima
        ,M.parametro_descripcion as moneda_prima_des
        ,O.descripcion as poliza
		,isnull((select top 1 valor from atributo_instancia_poliza where id_objeto_x_atributo in (47,71,384) and id_instancia_poliza=A.id),'') nro_tarjeta  
		,isnull((select top 1 valor from atributo_instancia_poliza where id_objeto_x_atributo in (35,59,373) and id_instancia_poliza=A.id),'') cta
		,isnull((select top 1 parametro_abreviacion from parametro where id = (select valor from atributo_instancia_poliza where id_objeto_x_atributo in (52,74,388) and id_instancia_poliza=A.id)),'') modalidad
		,isnull((select top 1 Z.parametro_abreviacion from atributo_instancia_poliza Y inner join parametro Z  on valor=z.parametro_cod and Z.diccionario_id=22 and Y.id_objeto_x_atributo in (45,69,382) and Y.id_instancia_poliza=A.id),'') moneda
        from instancia_poliza A 
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
		inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
		left join atributo_instancia_poliza X on X.id_instancia_poliza=A.id and X.id_objeto_x_atributo in (163,165,395)
        left join parametro Y on Y.parametro_cod=X.valor and Y.diccionario_id=38
        left join atributo_instancia_poliza X1 on X1.id_instancia_poliza=A.id and X1.id_objeto_x_atributo in (162,164,394)
        left join parametro Y1 on Y1.parametro_cod=X1.valor and Y1.diccionario_id=40
		left join instancia_documento K on K.id_instancia_poliza=A.id and k.id_documento in (7,10,34) 
		left join parametro H on H.id=D.par_tipo_documento_id
		left join parametro L on L.id=D.par_sexo_id
		inner join anexo_poliza N on A.id_poliza = N.id_poliza
		inner join parametro M on M.id=N.id_moneda
		inner join poliza O on O.id=A.id_poliza and A.id=${id}
        left join beneficiario E on B.id=E.id_asegurado and E.estado = 83
        left join entidad F on E.id_beneficiario=F.id
        left join persona G on F.id_persona=G.id
        left join parametro Z on Z.parametro_cod=G.persona_doc_id_ext and Z.diccionario_id=18
        left join parametro I on I.id=E.id_parentesco
		`)
        .then(resp => {
            if (resp) {
                datos = resp[0][0];
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReporteSolicitudTarjetas(datos, nombre_archivo);

    res.json(response);
});

router.post('/ReporteSolicitudEcoProteccion', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    await modelos.sequelize.query(``)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.generarReporteSolicitudEcoProteccion(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR
    res.json(response);
});

router.post('/ReporteSolicitudEcoMedic', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    await modelos.sequelize.query(`select Y2.valor pago_efectivo,Y3.valor cuenta,A1.descripcion poliza,A.id_poliza,A.id,D.persona_direccion_domicilio,D.persona_celular,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_apellido_casada,D.persona_doc_id,D.persona_doc_id_comp,D.persona_fecha_nacimiento
    ,G.persona_primer_apellido persona_primer_apellido_b,G.persona_segundo_apellido persona_segundo_apellido_b,G.persona_primer_nombre persona_primer_nombre_b,G.persona_segundo_nombre persona_segundo_nombre_b,G.persona_doc_id persona_doc_id_b,G.persona_celular persona_celular_b, G.persona_doc_id_ext persona_doc_id_ext_b,Z.parametro_abreviacion ext_b
    ,isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=241 and id_instancia_poliza = A.id),'') cta
    ,I.parametro_descripcion parentesco,K.parametro_descripcion tipo_documento,L.parametro_descripcion sexo,J.parametro_abreviacion persona_doc_id_ext,I.parametro_descripcion
    ,H.fecha_emision,H.nro_documento,H.fecha_inicio_vigencia,H.fecha_fin_vigencia
    ,E.porcentaje,
    Y.parametro_descripcion sucursal,
    Y1.parametro_descripcion agencia
    from instancia_poliza A 
    inner join poliza A1 on A.id_poliza=A1.id
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join instancia_documento H on A.id=H.id_instancia_poliza
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and J.diccionario_id=18
    inner join parametro k on k.id=D.par_tipo_documento_id
    inner join parametro L on L.id=D.par_sexo_id
    inner join atributo_instancia_poliza Y2 on Y2.id_instancia_poliza=A.id and Y2.id_objeto_x_atributo=242
    and (H.id_documento=22) and A.id=${id}
    left join atributo_instancia_poliza Y3 on Y3.id_instancia_poliza=A.id and Y3.id_objeto_x_atributo=241
    left join beneficiario E on B.id=E.id_asegurado and E.estado = 83
    left join entidad F on E.id_beneficiario=F.id
    left join persona G on F.id_persona=G.id
    left join parametro Z on Z.parametro_cod=G.persona_doc_id_ext and Z.diccionario_id=18
    left join parametro I on I.id=E.id_parentesco
    left join atributo_instancia_poliza X on X.id_instancia_poliza=A.id and X.id_objeto_x_atributo=261
    left join parametro Y on Y.parametro_cod=X.valor and Y.diccionario_id=38
    left join atributo_instancia_poliza X1 on X1.id_instancia_poliza=A.id and X1.id_objeto_x_atributo=260
    left join parametro Y1 on Y1.parametro_cod=X1.valor and Y1.diccionario_id=40`)
        .then(async resp => {
            if (resp) {
                datos = resp[0];
                if (datos.length > 0) {
                    response = await generaPdf.GenerarReporteSolicitudEcoMedic(datos, 2, nombre_archivo);
                } else {
                    response.status = "ERROR";
                    response.message = "Faltan algunos datos, comuniquese con ADMINISTRACIÓN";
                }
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR
    res.json(response);
});

router.post('/ReporteCertificadoTarjeta', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize
        .query(`select J.numero
        ,A.id_poliza
        ,H.fecha_emision
        ,H.nro_documento
        ,A.id id_instancia_documento
        ,D.persona_primer_apellido
        ,D.persona_segundo_apellido
        ,D.persona_primer_nombre
        ,D.persona_segundo_nombre
        ,D.persona_doc_id
        ,D.persona_doc_id_comp
        ,D.persona_fecha_nacimiento
        ,G.persona_primer_apellido persona_primer_apellido_b
        ,G.persona_segundo_apellido persona_segundo_apellido_b
        ,G.persona_primer_nombre persona_primer_nombre_b
        ,G.persona_segundo_nombre persona_segundo_nombre_b
        ,G.persona_doc_id persona_doc_id_b
        ,G.persona_celular persona_celular_b
        ,G.persona_doc_id_ext persona_doc_id_ext_b
        ,E.porcentaje
        ,G.persona_doc_id persona_doc_id_b
        ,G.persona_celular persona_celular_b
        ,Z.parametro_abreviacion persona_doc_id_ext_b
        ,Z.parametro_abreviacion ext_b
        ,I.parametro_descripcion parentesco
        ,J.numero 
        ,isnull((select top 1 valor from atributo_instancia_poliza where id_objeto_x_atributo in (47,71,384) and id_instancia_poliza=A.id),'') nro_tarjeta
        from instancia_poliza A 
        inner join asegurado B on A.id=B.id_instancia_poliza
        inner join entidad C on C.id=B.id_entidad
        inner join persona D on C.id_persona=D.id
		left join instancia_documento H on A.id=H.id_instancia_poliza and H.id_documento in (9,12,36)
		inner join poliza J on A.id_poliza=J.id and id_documento in (9,12,36) and A.id=${id}
        left join beneficiario E on B.id=E.id_asegurado and E.estado = 83
        left join entidad F on E.id_beneficiario=F.id
        left join persona G on F.id_persona=G.id
        left join parametro Z on Z.parametro_cod=G.persona_doc_id_ext and Z.diccionario_id=18
        left join parametro I on I.id=E.id_parentesco
		`)
        .then(resp => {
            if (resp) {
                datos = resp[0][0];
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReporteCertificadoTarjetas(datos, nombre_archivo);

    res.json(response);
});

router.post('/ReporteCertificadoEcoVida', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize.query(`select L.fecha_emision,D.persona_telefono_domicilio,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_apellido_casada,D.persona_doc_id, J.parametro_abreviacion extension, O.parametro_descripcion sucursal,P.parametro_descripcion agencia,K.nro_documento nro_solicitud,L.nro_documento nro_certificado,FORMAT(D.persona_fecha_nacimiento,'dd/MM/yyyy') fecha_nacimiento,Z.id id_beneficiario,X.persona_primer_nombre persona_primer_nombre_b,X.persona_segundo_nombre persona_segundo_nombre_b,X.persona_primer_apellido persona_primer_apellido_b,X.persona_segundo_apellido persona_segundo_apellido_b, W.parametro_descripcion parentesco, Z.porcentaje
    from instancia_poliza A 
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and diccionario_id=18
    inner join instancia_documento K on K.id_instancia_poliza=A.id and K.id_documento in (16)
    inner join instancia_documento L on L.id_instancia_poliza=A.id and L.id_documento in (18) and A.id=${id}
    left join atributo_instancia_poliza M on M.id_objeto_x_atributo in (192, 217) and M.id_instancia_poliza=A.id
    left join atributo_instancia_poliza N on N.id_objeto_x_atributo in (191, 216) and N.id_instancia_poliza=A.id
    left join parametro O on O.parametro_cod=M.valor and O.diccionario_id=38
    left join parametro P on P.parametro_cod=N.valor and P.diccionario_id=40
    left join beneficiario Z on Z.id_asegurado=B.id and Z.estado = 83
    left join entidad Y on Z.id_beneficiario=Y.id
    left join persona X on X.id=Y.id_persona
    left join parametro W on W.id=Z.id_parentesco
    where Z.estado = 83`
    )
        .then(resp => {
            if (resp) {
                datos = resp[0];
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReporteCertificadoEcoVida(datos, nombre_archivo);

    res.json(response);
});

router.post('/ReporteCertificadoEcoAccidentes', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo, origin } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize.query(`
select L.fecha_emision
,D.persona_telefono_domicilio
,D.persona_primer_nombre
,D.persona_segundo_nombre
,D.persona_primer_apellido
,D.persona_segundo_apellido
,D.persona_apellido_casada
,D.persona_doc_id
,J.parametro_abreviacion extension
,O.parametro_descripcion sucursal
,P.parametro_descripcion agencia
,K.nro_documento nro_solicitud
,L.nro_documento nro_certificado
,FORMAT(L.fecha_inicio_vigencia,'dd/MM/yyyy') fecha_inicio_vigencia
,FORMAT(L.fecha_fin_vigencia,'dd/MM/yyyy') fecha_fin_vigencia
,PO.numero
,FORMAT(D.persona_fecha_nacimiento,'dd/MM/yyyy') fecha_nacimiento
,Z.id id_beneficiario
     ,X.persona_primer_nombre persona_primer_nombre_b
     ,X.persona_segundo_nombre persona_segundo_nombre_b
     ,X.persona_primer_apellido persona_primer_apellido_b
     ,X.persona_segundo_apellido persona_segundo_apellido_b
     ,W.parametro_descripcion parentesco, Z.porcentaje
    from instancia_poliza A 
    inner join poliza PO on A.id_poliza=PO.id
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and diccionario_id=18
    inner join instancia_documento K on K.id_instancia_poliza=A.id and K.id_documento in (28)
    inner join instancia_documento L on L.id_instancia_poliza=A.id and L.id_documento in (30) and A.id=${id}
    left join atributo_instancia_poliza M on M.id_objeto_x_atributo in (314, 217) and M.id_instancia_poliza=A.id
    left join atributo_instancia_poliza N on N.id_objeto_x_atributo in (313, 216) and N.id_instancia_poliza=A.id
    left join parametro O on O.parametro_cod=M.valor and O.diccionario_id=38
    left join parametro P on P.parametro_cod=N.valor and P.diccionario_id=40
    left join beneficiario Z on Z.id_asegurado=B.id and Z.estado = 83
    left join entidad Y on Z.id_beneficiario=Y.id
    left join persona X on X.id=Y.id_persona
    left join parametro W on W.id=Z.id_parentesco`
    )
        .then(resp => {
            if (resp) {
                datos = resp[0];
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReporteCertificadoEcoAccidentes(datos, nombre_archivo, origin);

    res.json(response);
});

router.post('/ReporteCertificadoEcoProteccion', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize.query(`select L.fecha_emision,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_apellido_casada,D.persona_doc_id, J.parametro_abreviacion extension, O.parametro_descripcion sucursal,P.parametro_descripcion agencia,K.nro_documento nro_solicitud,L.nro_documento nro_certificado,FORMAT(D.persona_fecha_nacimiento,'dd/MM/yyyy') fecha_nacimiento,Z.id id_beneficiario,X.persona_primer_nombre persona_primer_nombre_b,X.persona_segundo_nombre persona_segundo_nombre_b,X.persona_primer_apellido persona_primer_apellido_b,X.persona_segundo_apellido persona_segundo_apellido_b, W.parametro_descripcion parentesco, Z.porcentaje,
    isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=201 and id_instancia_poliza=A.id),'') cta
    from instancia_poliza A 
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and diccionario_id=18
    inner join instancia_documento K on K.id_instancia_poliza=A.id and K.id_documento in (19,25)
    inner join instancia_documento L on L.id_instancia_poliza=A.id and L.id_documento in (21,27) and A.id=${id}
    left join atributo_instancia_poliza M on M.id_objeto_x_atributo in (192, 217,289) and M.id_instancia_poliza=A.id
    left join atributo_instancia_poliza N on N.id_objeto_x_atributo in (191, 216,288) and N.id_instancia_poliza=A.id
    left join parametro O on O.parametro_cod=M.valor and O.diccionario_id=38
    left join parametro P on P.parametro_cod=N.valor and P.diccionario_id=40
    left join beneficiario Z on Z.id_asegurado=B.id and Z.estado = 83
    left join entidad Y on Z.id_beneficiario=Y.id
    left join persona X on X.id=Y.id_persona
    left join parametro W on W.id=Z.id_parentesco`)
        .then(resp => {
            if (resp) {
                datos = resp[0];
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReporteCertificadoEcoProteccion(datos, nombre_archivo);

    res.json(response);
});

router.post('/ReporteCertificadoEcoMedic2', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize.query(`select V.numero numero_poliza, D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_apellido_casada,D.persona_doc_id, J.parametro_abreviacion extension, O.parametro_descripcion sucursal,P.parametro_descripcion agencia,K.nro_documento nro_solicitud,L.nro_documento nro_certificado,L.fecha_emision,FORMAT(D.persona_fecha_nacimiento,'dd/MM/yyyy') fecha_nacimiento,Z.id id_beneficiario,X.persona_primer_nombre persona_primer_nombre_b,X.persona_segundo_nombre persona_segundo_nombre_b,X.persona_primer_apellido persona_primer_apellido_b,X.persona_segundo_apellido persona_segundo_apellido_b, W.parametro_descripcion parentesco, Z.porcentaje,
    isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=201 and id_instancia_poliza=A.id),'') cta
    from instancia_poliza A 
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and diccionario_id=18
    inner join instancia_documento K on K.id_instancia_poliza=A.id and K.id_documento in (22)
    inner join instancia_documento L on L.id_instancia_poliza=A.id and L.id_documento in (24) and A.id=${id}
    inner join poliza V on V.id=A.id_poliza
    left join atributo_instancia_poliza M on M.id_objeto_x_atributo in (261) and M.id_instancia_poliza=A.id
    left join atributo_instancia_poliza N on N.id_objeto_x_atributo in (260) and N.id_instancia_poliza=A.id
    left join parametro O on O.parametro_cod=M.valor and O.diccionario_id=38
    left join parametro P on P.parametro_cod=N.valor and P.diccionario_id=40
    left join beneficiario Z on Z.id_asegurado=B.id and Z.estado = 83
    left join entidad Y on Z.id_beneficiario=Y.id
    left join persona X on X.id=Y.id_persona
    left join parametro W on W.id=Z.id_parentesco`)
        .then(resp => {
            if (resp) {
                datos = resp[0];
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReporteCertificadoEcoMedic2(datos, nombre_archivo);

    res.json(response);
});

router.post('/ReporteCertificadoEcoResguardo', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo, origin } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize.query(`
select L.fecha_emision
,D.persona_telefono_domicilio
,D.persona_primer_nombre
,D.persona_segundo_nombre
,D.persona_primer_apellido
,D.persona_segundo_apellido
,D.persona_apellido_casada
,D.persona_doc_id
, J.parametro_abreviacion extension
, O.parametro_descripcion sucursal
,P.parametro_descripcion agencia
,K.nro_documento nro_solicitud
,L.nro_documento nro_certificado
,FORMAT(L.fecha_inicio_vigencia,'dd/MM/yyyy') fecha_inicio_vigencia
,FORMAT(L.fecha_fin_vigencia,'dd/MM/yyyy') fecha_fin_vigencia
,PO.numero
,FORMAT(D.persona_fecha_nacimiento,'dd/MM/yyyy') fecha_nacimiento
,Z.id id_beneficiario
,X.persona_primer_nombre persona_primer_nombre_b
,X.persona_segundo_nombre persona_segundo_nombre_b
,X.persona_primer_apellido persona_primer_apellido_b
,X.persona_segundo_apellido persona_segundo_apellido_b
, W.parametro_descripcion parentesco
, Z.porcentaje
    from instancia_poliza A 
    inner join poliza PO on A.id_poliza=PO.id
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and diccionario_id=18
    inner join instancia_documento K on K.id_instancia_poliza=A.id and K.id_documento in (28,31)
    inner join instancia_documento L on L.id_instancia_poliza=A.id and L.id_documento in (30,33) and A.id=${id}
    left join atributo_instancia_poliza M on M.id_objeto_x_atributo in (314, 217, 350) and M.id_instancia_poliza=A.id
    left join atributo_instancia_poliza N on N.id_objeto_x_atributo in (313, 216, 349) and N.id_instancia_poliza=A.id
    left join parametro O on O.parametro_cod=M.valor and O.diccionario_id=38
    left join parametro P on P.parametro_cod=N.valor and P.diccionario_id=40
    left join beneficiario Z on Z.id_asegurado=B.id and Z.estado = 83
    left join entidad Y on Z.id_beneficiario=Y.id
    left join persona X on X.id=Y.id_persona
    left join parametro W on W.id=Z.id_parentesco
    where Z.estado = 83`
    )
        .then(resp => {
            if (resp) {
                datos = resp[0];
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReporteCertificadoEcoResguardo(datos, nombre_archivo, origin);

    res.json(response);
});

router.post('/ReporteCertificadoEcoProteccion', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize.query(`select L.fecha_emision,D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_apellido_casada,D.persona_doc_id, J.parametro_abreviacion extension, O.parametro_descripcion sucursal,P.parametro_descripcion agencia,K.nro_documento nro_solicitud,L.nro_documento nro_certificado,FORMAT(D.persona_fecha_nacimiento,'dd/MM/yyyy') fecha_nacimiento,Z.id id_beneficiario,X.persona_primer_nombre persona_primer_nombre_b,X.persona_segundo_nombre persona_segundo_nombre_b,X.persona_primer_apellido persona_primer_apellido_b,X.persona_segundo_apellido persona_segundo_apellido_b, W.parametro_descripcion parentesco, Z.porcentaje,
    isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=201 and id_instancia_poliza=A.id),'') cta
    from instancia_poliza A 
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and diccionario_id=18
    inner join instancia_documento K on K.id_instancia_poliza=A.id and K.id_documento in (19,25)
    inner join instancia_documento L on L.id_instancia_poliza=A.id and L.id_documento in (21,27) and A.id=${id}
    left join atributo_instancia_poliza M on M.id_objeto_x_atributo in (192, 217,289) and M.id_instancia_poliza=A.id
    left join atributo_instancia_poliza N on N.id_objeto_x_atributo in (191, 216,288) and N.id_instancia_poliza=A.id
    left join parametro O on O.parametro_cod=M.valor and O.diccionario_id=38
    left join parametro P on P.parametro_cod=N.valor and P.diccionario_id=40
    left join beneficiario Z on Z.id_asegurado=B.id and Z.estado = 83
    left join entidad Y on Z.id_beneficiario=Y.id
    left join persona X on X.id=Y.id_persona
    left join parametro W on W.id=Z.id_parentesco`)
        .then(resp => {
            if (resp) {
                datos = resp[0];
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReporteCertificadoEcoProteccion(datos, nombre_archivo);

    res.json(response);
});

router.post('/ReporteCertificadoEcoMedic2', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await modelos.sequelize.query(`select V.numero numero_poliza, D.persona_primer_nombre,D.persona_segundo_nombre,D.persona_primer_apellido,D.persona_segundo_apellido,D.persona_apellido_casada,D.persona_doc_id, J.parametro_abreviacion extension, O.parametro_descripcion sucursal,P.parametro_descripcion agencia,K.nro_documento nro_solicitud,L.nro_documento nro_certificado,L.fecha_emision,FORMAT(D.persona_fecha_nacimiento,'dd/MM/yyyy') fecha_nacimiento,Z.id id_beneficiario,X.persona_primer_nombre persona_primer_nombre_b,X.persona_segundo_nombre persona_segundo_nombre_b,X.persona_primer_apellido persona_primer_apellido_b,X.persona_segundo_apellido persona_segundo_apellido_b, W.parametro_descripcion parentesco, Z.porcentaje,
    isnull((select valor from atributo_instancia_poliza where id_objeto_x_atributo=201 and id_instancia_poliza=A.id),'') cta
    from instancia_poliza A 
    inner join asegurado B on A.id=B.id_instancia_poliza
    inner join entidad C on C.id=B.id_entidad
    inner join persona D on C.id_persona=D.id
    inner join parametro J on J.parametro_cod=D.persona_doc_id_ext and diccionario_id=18
    inner join instancia_documento K on K.id_instancia_poliza=A.id and K.id_documento in (22)
    inner join instancia_documento L on L.id_instancia_poliza=A.id and L.id_documento in (24) and A.id=${id}
    inner join poliza V on V.id=A.id_poliza
    left join atributo_instancia_poliza M on M.id_objeto_x_atributo in (261) and M.id_instancia_poliza=A.id
    left join atributo_instancia_poliza N on N.id_objeto_x_atributo in (260) and N.id_instancia_poliza=A.id
    left join parametro O on O.parametro_cod=M.valor and O.diccionario_id=38
    left join parametro P on P.parametro_cod=N.valor and P.diccionario_id=40
    left join beneficiario Z on Z.id_asegurado=B.id and Z.estado = 83
    left join entidad Y on Z.id_beneficiario=Y.id
    left join persona X on X.id=Y.id_persona
    left join parametro W on W.id=Z.id_parentesco`)
        .then(resp => {
            if (resp) {
                datos = resp[0];
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReporteCertificadoEcoMedic2(datos, nombre_archivo);

    res.json(response);
});

async function getParametros(ids, callback) {
    let response = { status: 'OK', message: '', data: {} };
    await modelos.parametro.findAll({
        where: { diccionario_id: ids },
    }).then(async resp => {
        response.data = resp;
    })
        .catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    await callback(response.data);
}

async function getAnexos(ids, callback) {
    let response = { status: 'OK', message: '', data: {} };
    await modelos.anexo_poliza.findAll({
        where: { id_tipo: ids, vigente : true },
    }).then(async resp => {
        response.data = resp;
    })
        .catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    await callback(response.data);
}

async function FindPersonaSolicitudByIdInstanciaPolizaAndAnexos(idInstanciaPoliza, callback) {
    let response = { status: 'OK', message: '', data: {} };
    await modelos.asegurado.find({
        where: { id_instancia_poliza: idInstanciaPoliza },
        include: [
            {
                model: modelos.entidad,
                include: [{
                    model: modelos.persona,
                    include: [
                        {
                            model: modelos.parametro,
                            as: 'par_tipo_documento'
                        }, {
                            model: modelos.parametro,
                            as: 'par_sexo'
                        }
                    ]
                }]
            }, {
                model: modelos.instancia_poliza,
                include: [{
                    model: modelos.parametro,
                    as: 'estado'
                }, {
                    model: modelos.poliza,
                    include: [
                        {
                            model: modelos.anexo_poliza,
                            where:{vigente:true},
                            include: [
                                {
                                    model: modelos.parametro,
                                    as: 'plan_moneda'
                                }
                            ]
                        }, {
                            model: modelos.parametro,
                            as: 'ramo'
                        }, {
                            model: modelos.parametro,
                            as: 'tipo_numeracion'
                        }, {
                            model: modelos.entidad,
                            as: 'aseguradora'
                        }]
                }, {
                    model: modelos.atributo_instancia_poliza,
                    include: [{
                        model: modelos.objeto_x_atributo,
                        include: [{
                            model: modelos.atributo
                        }, {
                            model: modelos.objeto
                        }, {
                            model: modelos.parametro,
                            as: 'par_editable'
                        }, {
                            model: modelos.parametro,
                            as: 'par_comportamiento_interfaz'
                        }]
                    }]
                }, {
                    model: modelos.instancia_documento,
                    include: [{
                        model: modelos.documento
                    }, {
                        model: modelos.atributo_instancia_documento
                    }]
                }]
            }, {
                model: modelos.instancia_anexo_asegurado,
                include: [{
                    model: modelos.entidad,
                    include: [{
                        model: modelos.persona,
                        include: [
                            {
                                model: modelos.parametro,
                                as: 'par_sexo'
                            }
                        ]
                    }]
                }, {
                    model: modelos.parametro,
                    as: 'par_parentesco'
                }]
            }]
    })
        .then(async resp => {
            response.data = resp.dataValues;
        })
        .catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    await callback(response.data);
}

router.post('/ReporteCertificadoEcoMedic', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await FindPersonaSolicitudByIdInstanciaPolizaAndAnexos(id, async asegurado => {
        if (res) {
            response.data = res;
            await getParametros([18, 40, 38, 4, 31], async parametros => {
                await getAnexos([110], async anexos => {
                    response = await generaPdf.GenerarReporteCertificadoEcoMedic(asegurado, anexos, parametros, nombre_archivo);
                });
            });
        }
    })
    //OBTENER DATOS DEUDOR
    res.json(response);
});


router.post('/ReporteSolicitudEcoMedicComentado', async (req, res) => {
    let response = { status: 'OK', message: 'reporte generado', data: '' };
    const { id, nombre_archivo } = req.body;
    // const id_solicitud = idSolicitud;

    let datos = {};
    //OBTENER DATOS SOLICITUD
    await FindPersonaSolicitudByIdInstanciaPolizaAndAnexos(id, async asegurado => {
        if (res) {
            response.data = res;
            await getParametros([18, 40, 38], async parametros => {
                await getAnexos([110], async anexos => {
                    response = await generaPdf.GenerarReporteSolicitudEcoMedic(asegurado, anexos, parametros, nombre_archivo);
                });
            });
        }
    });
    //OBTENER DATOS DEUDOR
    res.json(response);
});

router.post('/ReportePlanPago', async (req, res) => {
    let nro_certificado = req.body.nro_certificado;
    let id_instancia_poliza = req.body.id_instancia_poliza;
    let nombre_archivo = req.body.nombre_archivo;
    let response = { status: 'OK', message: '', data: '' };
    await modelos.sequelize.query(`
            select format(DATEADD(day,A6.dias_gracia,A.fecha_couta_prog),'dd/MM/yyyy')  fecha_limite, A.nro_cuota_prog,format(A.fecha_couta_prog,'dd/MM/yyyy') fecha_couta_prog,A.pago_cuota_prog,A.pago_prima_acumulado,A.prima_pendiente,isnull(format(A.FechaPagoEjec,'dd/MM/yyyy'),'') FechaPagoEjec,D.parametro_descripcion estado,E.parametro_descripcion  estado_det
            ,A1.total_prima,H.parametro_descripcion moneda,A1.interes,A1.plazo_anos,A1.periodicidad_anual,A1.prepagable_postpagable,format(A1.fecha_inicio,'dd/MM/yyyy') fecha_inicio
            ,isnull(A4.persona_primer_apellido,'')+' '+isnull(A4.persona_segundo_apellido,'')+' '+isnull(A4.persona_primer_nombre,'')+' '+isnull(A4.persona_segundo_nombre,'') asegurado
            ,B.nro_documento nro_certificado,format(B.fecha_emision,'dd/MM/yyyy') fecha_emision,format(B.fecha_inicio_vigencia,'dd/MM/yyyy') fecha_inicio_vigencia,format(B.fecha_fin_vigencia,'dd/MM/yyyy') fecha_fin_vigencia,I.parametro_descripcion estado_plan,A6.descripcion poliza
            from plan_pago_detalle A
            inner join plan_pago A1 on A.id_instancia_poliza=A1.id_instancia_poliza
            inner join asegurado A2 on A.id_instancia_poliza=A2.id_instancia_poliza
            inner join entidad A3 on A2.id_entidad=A3.id
            inner join persona A4 on A4.id=A3.id_persona
            inner join instancia_poliza A5 on A.id_instancia_poliza=A5.id and A5.id=${id_instancia_poliza} 
            inner join poliza A6 on A5.id_poliza=A6.id
            inner join instancia_documento B on A.id_instancia_poliza=B.id_instancia_poliza
            inner join documento C on B.id_documento=C.id and C.id_poliza=A5.id_poliza and C.id in (3,6,9,12,15,18,21,24) and B.nro_documento='${nro_certificado}'
            left join parametro D on A.id_estado=D.id
            left join parametro E on A.id_estado_det=E.id 
            left join archivo F on A.id_instancia_archivo=F.id
            left join envio G on F.id=G.id_instancia_archivo
            left join parametro H on H.id=A1.id_moneda and H.diccionario_id = 22
            left join parametro I on A1.id_estado=I.id
            order by A.nro_cuota_prog
        `)
        .then(resp => {
            if (resp) {
                datos = resp[0];
            }
        })
        .catch(err => {
            response.status = "ERROR";
            response.message = err;
        });
    //OBTENER DATOS DEUDOR

    response = await generaPdf.generarReportePlanPago(datos, nombre_archivo);

    res.json(response);
});

router.get('/produccionMensualDiaria/:month', async (req, res) => {
    try {
        let {month} = req.params;
        let responseProduccionMensualDiaria = await reporteService.produccionMensualDiaria(8,month);
        if (responseProduccionMensualDiaria && responseProduccionMensualDiaria.length) {
            let dataExcelProduccionMensualDiaria = await reporteService.setDataExcelProduccionMensualDiaria(responseProduccionMensualDiaria);
            util.setSuccess(200, 'solicitudes encontradas', dataExcelProduccionMensualDiaria);
        } else {
            util.setError(404, `No se encontraron solicitudes`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/produccionMensualDiaria/:month/:date', async (req, res) => {
    try {
        let {month} = req.params;
        let {date} = req.params;
        let responseProduccionMensualDiaria = await reporteService.produccionMensualDiaria(8,month,date);
        if (responseProduccionMensualDiaria && responseProduccionMensualDiaria.length) {
            let dataExcelProduccionMensualDiaria = await reporteService.setDataExcelProduccionMensualDiaria(responseProduccionMensualDiaria);
            util.setSuccess(200, 'solicitudes encontradas', dataExcelProduccionMensualDiaria);
        } else {
            util.setError(404, `No se encontraron solicitudes`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/produccionMensual/:month', async (req, res) => {
    try {
        let {month} = req.params;
        let responseProduccionMensual = await reporteService.produccionMensualDiaria(8,month);
        if (responseProduccionMensual && responseProduccionMensual.length) {
            let dataExcelProduccionMensual = await reporteService.setDataExcelProduccionMensual(responseProduccionMensual);
            util.setSuccess(200, 'solicitudes encontradas', dataExcelProduccionMensual);
        } else {
            util.setError(404, `No se encontraron solicitudes`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

router.get('/produccionDiariaInmedical/:date', async (req, res) => {
    try {
        let {date} = req.params;
        let responseProduccionMensual = await reporteService.produccionDiariaInmedical(8,date);
        if (responseProduccionMensual && responseProduccionMensual.length) {
            util.setSuccess(200, 'solicitudes encontradas', responseProduccionMensual);
        } else {
            util.setError(404, `No se encontraron solicitudes`);
        }
        return util.send(res);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;
