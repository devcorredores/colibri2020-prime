const { Router } = require('express');
const router = new Router();
const Util = require("../utils/util");
const util = new Util();
const AdminPermisosService = require("../servicios/adminPermisos.service");

var isLoggedIn = (req, res, next) => {
    console.log('session ', req.session);
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(400).json({ "statusCode": 400, "message": "usuario no autentificado" })
}

    router.post('/NewModulo', async (req, res) => {
        try {
            let objData = await AdminPermisosService.newModulo(req.body);
            if (!objData) {
                util.setError(404, `No data found`);
            } else {
                util.setSuccess(200, "Registro creado", objData);
            }
            return util.send(res);
        } catch (e) {
            console.log(e);
        }
    });

    router.post('/NewVista', async (req, res) => {
        try {
            let objData = await AdminPermisosService.newVista(req.body);
            if (!objData) {
                util.setError(404, `No data found`);
            } else {
                util.setSuccess(200, "Registro creado", objData);
            }
            return util.send(res);
        } catch (e) {
            console.log(e);
        }
    });

    router.post('/NewComponente', async (req, res) => {
        try {
            let objData = await AdminPermisosService.newComponente(req.body);
            if (!objData) {
                util.setError(404, `No data found`);
            } else {
                util.setSuccess(200, "Registro creado", objData);
            }
            return util.send(res);
        } catch (e) {
            console.log(e);
        }
    });

    router.get('/GetAllModulos',isLoggedIn,async (req, res) => {
        try {
            let objData = await AdminPermisosService.getAllModulos(req.body);
            if (!objData) {
                util.setError(404, `No data found`);
            } else {
                util.setSuccess(200, "data retrieved", objData);
            }
            return util.send(res);
        } catch (e) {
            console.log(e);
        }
    });

    router.get('/getComponentes/:idVista/:idPerfil',isLoggedIn,async (req, res) => {
        try {
            let idVista = req.params.idVista.indexOf(',') >=0 ? req.params.idVista.split(',') : req.params.idVista;
            let idPerfil = req.params.idPerfil.indexOf(',') >=0 ? req.params.idPerfil.split(',') : req.params.idPerfil;
            let objData = await AdminPermisosService.getComponentes(idVista,idPerfil);
            if (!objData) {
                util.setError(404, `No data found`);
            } else {
                util.setSuccess(200, "data retrieved", objData);
            }
            return util.send(res);
        } catch (e) {
            console.log(e);
        }
    });

    router.post('/ListarMenu',async (req, res) => {
        try {
            let id = req.body.id;
            let objData = await AdminPermisosService.listarMenu(id);
            //let objData = await AdminPermisosService.generarMenu2(usuario);
            if (!objData) {
                util.setError(404, `No data found`);
            } else {
                util.setSuccess(200, "data retrieved", objData);
            }
            return util.send(res);
        } catch (e) {
            console.log(e);
        }
    });
module.exports = router;
