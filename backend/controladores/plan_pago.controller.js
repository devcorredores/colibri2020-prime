const Util = require("../utils/util");
const util = new Util();
const planPagoService = require("../servicios/plan_pago.service");
const funcionesArchivo = require("../modulos/funciones_archivo");
// Controller for DB Mongoose

const planPagoCtrl = {};
planPagoCtrl.service = planPagoService;

planPagoCtrl.setPlanPagoProcedure = async (attr, res) => {
  try {
    let { fecha } = attr;
    let { id_poliza } = attr;
    let { dias_habil_siguiente } = attr;
    let { id_calendario } = attr;
    fecha= new Date(fecha);
    fecha= fecha.getFullYear()+'/'+(fecha.getMonth()+1)+'/'+fecha.getDate();
    let objData = await planPagoService.getPlanPagoProcEcoproteccion(fecha, id_poliza, dias_habil_siguiente, id_calendario);
    if (!objData || !objData[0] || !objData[0].length) {
      util.setError(404, `No data found`);
    } else {
      util.setSuccess(200, "data retrieved", objData[0]);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
};

planPagoCtrl.setPlanPagoFile = async (attr, res) => {
  try {
    let { data } = attr;
    let { id_poliza } = attr;
    let id_usuario = attr.id_usuario ? attr.id_usuario : 'rvillca';
    await funcionesArchivo.crearTextoArchivoEnviar(data,id_usuario,id_poliza, async (data, message) => {
	    if (!data) {
	      util.setError(404, `No data found`);
	    } else {
	      util.setSuccess(200, message, data);
	    }
	    return util.send(res);
	  });
  } catch (e) {
    console.log(e);
  }
};

planPagoCtrl.createPlanPagos = async (attr, res) => {
  try {
    let { idPoliza } = attr;
    let planePagos;
    switch (idPoliza) {
      case '7':
        planePagos = await planPagoService.createEcoProteccionPlanPagos();
        break;
      case '8':
        planePagos = await planPagoService.createEcoMedicvPlanPagos();
        break;
    }
    if (planePagos && planePagos.length) {
          util.setSuccess(200, 'Planes de Pago creados correctamente', planePagos);
      } else {
          util.setError(404, `No data found`);
      }
      return util.send(res);
  } catch (e) {
    console.log(e);
  }
};

planPagoCtrl.createPlanPagosDetalle = async (attr, res) => {
  try {
    let { idPoliza } = attr;
    let planePagos;
    switch (idPoliza) {
      case '7':
        planePagos = await planPagoService.createEcoProteccionPlanPagosDetalle();
        break;
      case '8':
        planePagos = await planPagoService.createEcoMedicvPlanPagosDetalle();
        break;
    }
    if (planePagos && planePagos.length) {
          util.setSuccess(200, 'Planes de Pago detalles creados correctamente', planePagos);
      } else {
          util.setError(404, `No data found`);
      }
      return util.send(res);
  } catch (e) {
    console.log(e);
  }
};

//<es-section>
module.exports = planPagoCtrl;
//</es-section>
