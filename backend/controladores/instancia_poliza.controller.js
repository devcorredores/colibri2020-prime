const Util = require("../utils/util");
const util = new Util();
const instanciaPolizaService = require("../servicios/instanciaPoliza.service");
const funcionesArchivo = require("../modulos/funciones_archivo");
// Controller for DB Mongoose

const instanciaPolizaCtrl = {};
instanciaPolizaCtrl.service = instanciaPolizaService;

instanciaPolizaCtrl.deleteInstanciaPolizas = async (attr, res) => {
  try {
    let { idInstancias } = attr;
    idInstancias = idInstancias.split(',');
    let objData = await instanciaPolizaService.deleteInstanciaPolizas(idInstancias);
    if (!objData || !objData[0] || !objData[0].length) {
      util.setError(404, `No data found`);
    } else {
      util.setSuccess(200, "data proceced", objData[0]);
    }
    return util.send(res);
  } catch (e) {
    console.log(e);
  }
};

//<es-section>
module.exports = instanciaPolizaCtrl;
//</es-section>
