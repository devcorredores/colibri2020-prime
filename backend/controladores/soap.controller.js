const Util = require("../utils/util");
const util = new Util();
const soapService = require("../servicios/soap.service");

// Controller for DB Mongoose

const soapCtrl = {};
soapCtrl.service = soapService;

soapCtrl.verifyInstanciasWithBankServices = async (attr, res) => {
  try {
      let {id_poliza} = attr;
      let objSqlData = await soapService.verifyInstanciasWithBankServices(id_poliza);
      if (!objSqlData) {
          util.setError(404, `soap service failed`);
      } else {
          util.setSuccess(200, "soap created", objSqlData);
      }
      return util.send(res);
  } catch (e) {
    console.log(e);
  }
};


soapCtrl.cuentasVinculadas = async (params, res) => {
  try {
    const { doc_id } = params;
    const { ext } = params;
    const soapCustomer = await soapService.cuentasVinculadas(doc_id, ext);
    if (soapCustomer) {
      util.setSuccess(200, "cuentasVinculadas retrieved", soapCustomer);
    } else {
      util.setSuccess(200, "No cuentasVinculadas found");
    }
    return util.send(res);
  } catch (e) {
    util.setError(400, e);
    return util.send(res);
  }
};

soapCtrl.cuentasAsociadas = async (params, res) => {
  try {
    const { doc_id } = params;
    const { ext } = params;
    const soapCustomer = await soapService.cuentasAsociadas(doc_id, ext);
    if (soapCustomer) {
      util.setSuccess(200, "cuentasAsociadas retrieved", soapCustomer);
    } else {
      util.setSuccess(200, "No cuentasAsociadas found");
    }
    return util.send(res);
  } catch (e) {
    util.setError(400, e);
    return util.send(res);
  }
};

soapCtrl.getSolicitudPrimeraEtapa = async (params, res) => {
  try {
    const { solicitud } = params;
    const soapCustomer = await soapService.getSolicitudPrimeraEtapa(solicitud);
    if (soapCustomer) {
      util.setSuccess(200, "cuentasAsociadas retrieved", soapCustomer);
    } else {
      util.setSuccess(200, "No cuentasAsociadas found");
    }
    return util.send(res);
  } catch (e) {
    util.setError(400, e);
    return util.send(res);
  }
};

soapCtrl.getSolicitud= async (params, res) => {
  try {
    const { solicitud } = params;
    const soapCustomer = await soapService.getSolicitud(solicitud);
    if (soapCustomer) {
      util.setSuccess(200, "cuentasAsociadas retrieved", soapCustomer);
    } else {
      util.setSuccess(200, "No cuentasAsociadas found");
    }
    return util.send(res);
  } catch (e) {
    util.setError(400, e);
    return util.send(res);
  }
};

//<es-section>
module.exports = soapCtrl;
//</es-section>
