const Util = require("../utils/util");
const util = new Util();
const fileService = require("../servicios/file.service");

// Controller for DB Mongoose

const fileCtrl = {};
fileCtrl.service = fileService;

fileCtrl.upload = async (attr, res) => {
  try {
      let objSqlData = await fileService.upload(req, res,(err, data) => {
          if (!data) {
              util.setError(404, `The file file was not created`, err);
          } else {
              util.setSuccess(200, "File created", data);
          }
          return util.send(res);
      });
  } catch (e) {
    console.log(e);
  }
};

//<es-section>
module.exports = fileCtrl;
//</es-section>
