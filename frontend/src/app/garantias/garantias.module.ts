import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GarantiasRoutingModule } from './garantias-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    GarantiasRoutingModule
  ]
})
export class GarantiasModule { }
