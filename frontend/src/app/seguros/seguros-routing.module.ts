import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SeguroFormComponent } from './seguro-form/seguro-form.component';
import { SeguroListComponent } from './seguro-list/seguro-list.component';
import { BuscaSolicitudComponent } from './busca-solicitud/busca-solicitud.component';
import { SoporteSolicitudComponent } from './soporte-solicitud/soporte-solicitud.component';

const routes: Routes = [
  {
    path:'seguro-list',
    component: SeguroListComponent
  },
  {
    path:'seguro-form',
    component: SeguroFormComponent
  },
  {
    path:'seguro-add',
    component: BuscaSolicitudComponent
  },
  {
    path:'soporte-solicitud',
    component: SoporteSolicitudComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SegurosRoutingModule { }
