import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';

import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

import { AuthService } from '../../service/auth.service';
import { ReportesService } from '../service/reportes.service';

import { EcofuturoService} from '../service/ecofuturo.service';
import { SendMailService } from '../service/send-mail.service';

import { SegAprobacionesService } from '../service/seg-aprobaciones.service';
import { SegObservacionesService } from '../service/seg-observaciones.service';
import { SegBeneficiariosService } from '../service/seg-beneficiarios.service';
import { SegSolicitudesService } from '../service/seg-solicitudes.service';
import { SegDeudoresService } from '../service/seg-deudores.service';
import { SegAdicionalesService } from '../service/seg-adicionales.service';

import { FileUploadService} from '../service/file-upload.service';

import { ParametrosCampo} from '../models/parametros-campo';

import { SegSolicitudes } from '../models/seg-solicitudes';
import { SegBeneficiarios } from '../models/seg-beneficiarios';
import { SegAdicionales } from '../models/seg-adicionales';
import { SegAprobaciones } from '../models/seg-aprobaciones';
import { SegObservaciones } from '../models/seg-observaciones';
import { SegDeudores } from '../models/seg-deudores';
import { SegOperaciones } from '../models/seg-operaciones';
import { environment } from 'src/environments/environment';
import { MessageEmail } from '../models/message-email';
import { UserInfo } from '../../models/UserInfo';

@Component({
  selector: 'app-seguro-form',
  templateUrl: './seguro-form.component.html',
  styleUrls: ['./seguro-form.component.css']
})
export class SeguroFormComponent implements OnInit {

  num_solicitud:number = 0;
  id_solicitud:number;
  nro_solicitud:number =0;

  id_folder: number = 0;
  id_prestamo: number = 0;
  numero_documento: number = 0;
  id_entidad_seguro : number = 0;
  titular: string = '';
  estado_general: string = '';
  estado_codigo: string = '';
  numero_poliza : string = '';
  tipo_cobertura : string = '';
  tipo_cartera : string = '';
  id_codeudor=0;
  id_deudor=0;
  id_cargo=0;

  id_deudor_Selec = 0;
  deudor_nombre_sel = '';

  resolucion : {
    aprobado:boolean,
    rechazado:boolean,
    extra_prima:number,
    condiciones_exclusiones:string,
    causal:string,
  }={aprobado:false,rechazado:false,
    extra_prima:null,condiciones_exclusiones:null,causal:null};

  desistimiento : {
    causal : string,
    autorizado_por : string
  }={causal:'',autorizado_por:''};

  id_tipo_seguro=0;
  file : File = null;
  download_url = environment.URL_PUBLIC_FILES;

  exa_lab : {
    examen_medico:boolean,
    analisis_general_orina:boolean,
    test_hvi:boolean,
    hemograma:boolean,
    glucosa_sanguinea:boolean
  }

  innerHtml: SafeHtml;
  tab_active = 0;
  openModal = 'none';
  openBen= 'none';
  openModalSeguros = 'none';
  openModalCodeu = 'none';
  openModalBene = 'none';
  openModalCuestion = 'none';
  openModalMensajes = 'none';
  openModalImprimir = 'none';
  openModalHistorial = 'none';
  openModalValida = 'none';
  openModalDesistir='none';
  display = '';
  showDocs : boolean = false;
  showDocs_1 : boolean = false;
  showDocs_2 : boolean = false;
  showImp : boolean[] = [false,false,false,false,false,false,false,false,false,false];
  solicitudValid: boolean = false;

  usuario_rol : string;

  campos: ParametrosCampo[];
  configParam : ConfigParam;
  estadosSolicitud:EstadosSolicitud[]=[];

  segBeneficiarios:SegBeneficiarios[]=[];
  segBeneficiariosIdDeu:SegBeneficiarios[]=[];
  segBeneficiariosInfo:[{idDeu:number,count:number}];
  segObservaciones : SegObservaciones[] = [];
  segDeudores : SegDeudores[] = [];
  segAprobaciones : SegAprobaciones[] = [];
  deudorSeleccionado:SegDeudores = new SegDeudores();
  autorizacionesSolicitud = [];
  
  benIdDeu:number=0;

  declaraciones: SegAdicionales[];
  codeudores : SegDeudores[];
  beneficiarios : SegBeneficiarios[] = [];
  beneficiarioDe:string = '';

  // data:DataSolicitud = {} as DataSolicitud;
  
  data : DataSolicitud = {
    seg_solicitudes : new SegSolicitudes(),
    seg_deudores : [ new SegDeudores() ],
    seg_operaciones : new SegOperaciones(),
    seg_beneficiarios : [ new SegBeneficiarios()],
    seg_adicionales : [ new SegAdicionales()],
    seg_aprobaciones : [ new SegAprobaciones()],
    seg_observaciones : [new SegObservaciones()],
    estado_general : '',
    config_param : {}
  };
  

  datosCalculados : DatosCalculados = {
    tasa_mensual_ref : 0.00,
    prima_mensual_ref: 0.00,
    entidad_seguros_1 : "",
    entidad_seguros_2 : "",
    saldo_actual_solicitado : 0.00
  };

  isEdit = true;


  folder_seguro_def: ParametrosCampo[];
  solicitud_credito_def: ParametrosCampo[];
  deudor_def: ParametrosCampo[];
  operacion_crediticia_def: ParametrosCampo[];

  codeudor_def : ParametrosCampo[];
  beneficiario_def : ParametrosCampo[];
  codeudores_def = [];

  observaciones: string[] = [];
  advertencias: string[] = [];

  deus_ : string[] = ['none','none','none','none','none','none','none','none','none','none'];
  deus : boolean[] = [false,false,false,false,false,false,false,false,false,false];
  bens : boolean[] = [false,false,false,false,false,false,false,false,false,false];
  btns : boolean[] = [false,false,false,false,false,false,false,false,false,false];
  op : boolean[] = [true,false,false,false,false,false,false,false,false,false];


  enviarFinanciera : boolean = false;

  param_buscar = {
    accion : 'add-codeudor',
    numero_documento : '',
    id_folder : 0,
  };

  hoy = new Date();
  respuestaObser:string = '';

  offGuardarDeudor = true;
  offGuardarCoDeudor = true;

  viewTable = '';

  userinfo : UserInfo;
  formData = new FormData();

  segObservacionesUlt = [new SegObservaciones()];

  esTarjeta:boolean=false;
  isLoading:boolean = false;
  esObligadosIncluidos:boolean = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private msg: ToastrService,
    private reporteService: ReportesService,
    private segBeneficiariosService:SegBeneficiariosService,
    private ecofuturoService:EcofuturoService,
    private segAprobacionesService:SegAprobacionesService,
    private sendMailService:SendMailService,
    private segObservacionesService:SegObservacionesService,
    private fileUploadService:FileUploadService,
    private authService:AuthService,
    private segSolicitudesService : SegSolicitudesService,
    private segDeudoresService : SegDeudoresService,
    private segAdicionalesService:SegAdicionalesService
  ) { }

  ngOnInit(): void {

    this.id_solicitud = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    this.getDatosSolicitud();

    try {
      // get rol del usuario
      //this.userinfo =  this.authService.getUserInfo() as UserInfo;
      this.userinfo = this.authService.getItemSync('userInfo');

      if(this.userinfo){
        this.usuario_rol = this.userinfo.rol[0].codigo; // of | aseguradora
      }else{
        this.msg.warning('Rol del usuario no definido','SOLICITUD');
      }

    }
    catch (error) { 
      this.msg.warning('No se pudo recuperar la session del usuario','SOLICITUD')
    }

  }

  ngAfterViewInit() {}

  getSolicitudSegundaEtapa(num_solicitud:number){
    this.ecofuturoService.getSolicitudSegundaEtapa(num_solicitud).subscribe(resp=>{
      const response = resp as { status: string, messages: string[], data: any };
      if(response.status=='OK'){
        const datosOperacion = response.data.getSolicitudSegundaEtapaResult;
        this.data.seg_operaciones = datosOperacion;
      }
    },ex=>{
      console.error('Error en comunicacion, Segunda Etapa : recuperando Servicio del Banco')
    });
  };

  getSolicitudPrimeraEtapa(num_solicitud:number){
    this.ecofuturoService.getSolicitudPrimeraEtapa(num_solicitud).subscribe(resp=>{
      const response = resp as { status: string, messages: string[], data: any };
      if(response.status=='OK'){
        this.data.seg_solicitudes.Sol_SolTipoSol = response.data.getSolicitudPrimeraEtapaResult.Operacion_Tipo_Solicitud; 
      }
    },ex=>{
      console.error('Error en comunicacion, Primera Etapa : recuperando Servicio del Banco')
    })
  };

  setEstadoGeneral(estado:string){
    // estado actual de la solicitud
    if(estado){
      this.data.estado_general = this.configParam.estado_solicitud.find(row=>row.codigo==estado).estado;
    }else{
      console.error('Config Param : Estados de la Solicitud Vacio');
    };

  };


  getDatosSolicitud =()=>{
    Swal.fire({
      allowOutsideClick: false,
      text: 'Recuperando Información  ...'
    });
    Swal.showLoading();
    // recuperamos datos, deudor, codeudor, solicitud

    if(!this.id_solicitud){
      this.id_solicitud = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    }

    this.segSolicitudesService.getSolicitudIdSolAll(this.id_solicitud).subscribe(resp=>{
      Swal.close();
      const response = resp as {status:string,message:string,messages:[],data:DataSolicitud };

      if(response.status === 'OK'){

        // init folder
        this.data = response.data;
        this.configParam = this.data.config_param;
        //this.data.seg_solicitudes = response.data.seg_solicitudes;
        //this.data.seg_deudores = response.data.seg_deudores;
        //this.data.seg_beneficiarios = response.data.seg_beneficiarios;
        //this.data.seg_observaciones = response.data.seg_observaciones;
        //this.data.seg_adicionales = response.data.seg_adicionales;
        //this.data.seg_aprobaciones = response.data.seg_aprobaciones;

        // recuperando titulos
        this.nro_solicitud = Number(this.data.seg_solicitudes.Sol_NumSol);
        this.tipo_cartera = this.data.seg_solicitudes.Sol_TipoSeg;
        this.estado_codigo = this.data.seg_solicitudes.Sol_EstadoSol;
        
        const estadoSolicitud = this.configParam.estado_solicitud.find(row=>row.codigo==this.estado_codigo);
        if(estadoSolicitud){
          this.data.estado_general = estadoSolicitud.estado;
        }

        // verificar este campo si es nulo
        if(!this.data.seg_solicitudes.Sol_SolTipoSol){
          this.getSolicitudPrimeraEtapa(this.nro_solicitud);
        };
        
        // SEG OPERACIONES
        if(response.data.seg_operaciones){
          this.data.seg_operaciones = response.data.seg_operaciones
        }else{
          this.getSolicitudSegundaEtapa(this.nro_solicitud);
        };

        //verificamos la poliza
        //if(!this.data.seg_solicitudes.Sol_Poliza){
        //  this.segSolicitudesService.getUpdatePoliza(this.data.seg_solicitudes.Sol_IdSol).subscribe(resp=>{
        //    const response = resp as { status: string; message:string ; messages: []; data: {} };
        //    if (response.status=='OK'){  
        //    }
        //  });
        //}

        // BENEFICIARIOS
        this.segBeneficiarios = [...this.data.seg_beneficiarios];

        // ADICIONALES - DJS


        // APROBACIONES
        this.segAprobaciones = [...this.data.seg_aprobaciones];

        // OBSERVACIONES
        this.segObservaciones = [...this.data.seg_observaciones];


        // CALCULO DE TASAS y Datos Completados 
        this.datosCompletados();
        this.sumaSaldoActualSolicitud();
        
        this.solicitudValid = (this.data.seg_solicitudes.Sol_EstadoSol!='P'?true:false);
        
        this.viewTable = this.getViewData(this.data.config_param);

        //ultima comunicacion
        if(['C1','C2'].includes(this.estado_codigo) ){

          if (this.usuario_rol == 'of'){
            this.segObservacionesUlt = this.data.seg_observaciones.filter(row=>row.Obs_Eeff !='1')
          }else{
            this.segObservacionesUlt = this.data.seg_observaciones.filter(row=>row.Obs_Eeff =='1')
          }
          
        };

        //es tarjeta
        if(this.data.seg_solicitudes.Sol_NumSol.toString().substring(0,1) == '9'){
          this.esTarjeta = true;
        }

      }else{
        Swal.close();
        this.msg.error(`Servicio principal : ${response.messages.toString() } `,'Datos del Seguros')
      }

    },ex=>{
      Swal.close();
      this.msg.error('No se pudo establecer comunicación','SOLICITUDES');
    });


  };

  // Calculo de tasas y completados
  datosCompletados(){

    this.estado_codigo = this.data.seg_solicitudes.Sol_EstadoSol;

    // Tasa Mensual Ref.  | Prima Mensual Ref. var conDecimal = numero.toFixed(2);
    try { 

      const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;

      if(nro_poliza){

        const monto_solicitado = this.data.seg_solicitudes.Sol_MontoSol;
        const cant_obligados = this.data.seg_deudores.length;
      
        const tasas = this.configParam.datos_poliza.find(row=>row.nro_poliza==nro_poliza);

        if(tasas){

          const tasaTitular = tasas.tasa_anual_ref;
          const tasaCodeudor = tasas.tasa_anual_ref_2;
    
          const tasa_mensual = parseFloat(tasaTitular) + ( parseFloat(tasaCodeudor)*(cant_obligados-1));
          const prima_mensual = monto_solicitado * (tasa_mensual/1000);
    
          //let tasa_anual = this.data.seg_solicitudes.Sol_TasaSeg?parseFloat(this.data.seg_solicitudes.Sol_TasaSeg.toString()):0.00;
          //let monto_solicitado = this.data.seg_solicitudes.Sol_MontoSol?this.data.seg_solicitudes.Sol_MontoSol:0.00;
          //const tasa_mensual = tasa_anual/12;
          //const prima_mensual = monto_solicitado * (tasa_mensual / 100);
    
          this.datosCalculados.tasa_mensual_ref = tasa_mensual;
          this.datosCalculados.prima_mensual_ref = prima_mensual;

        }


      }



    } catch (error) {
      console.error(error);
    }

    // Entidad Financiera
    if(this.data.seg_solicitudes.Sol_Poliza){
      const nro_poliza = this.data.seg_solicitudes.Sol_Poliza;
      const entidadPoliza = this.configParam.datos_poliza.find(row=>row.nro_poliza == nro_poliza);
      if(entidadPoliza){
        this.datosCalculados.entidad_seguros_1 = entidadPoliza.entidad_seguros;
      }
    }
    //obtener djs, valor para validar djs 
    const tipo_seguro = this.data.seg_solicitudes.Sol_TipoSeg; //NO LICITADA |  LICITADA
    const djsData = this.configParam.djs_salud.filter(row=>row.tipo_seguro == tipo_seguro);

    this.data.seg_adicionales.forEach(row=>{
      row.valida_por = djsData.find(el=>el.nro == row.Adic_Pregunta).valida_por;
      if(row.Adic_Respuesta){
        if(row.Adic_Respuesta == 'S||||||||||'){
          row.valor_si = true;
          row.valor_no = false
        }
        if(row.Adic_Respuesta == '|S|||||||||'){
          row.valor_si = false;
          row.valor_no = true;
        }
      }
    });

    this.autorizacionesSolicitud = this.configParam.autorizaciones_solicitud;

  }

  sumaSaldoActualSolicitud(){

    try {
      // Saldo Actual Solicitado =  this.datosCalculados.saldo_actual_solicitado
      const monto_solicitado : number = this.data.seg_solicitudes.Sol_MontoSol;
      const moneda = this.data.seg_solicitudes.Sol_MonedaSol;
      const tipo_seguro = this.data.seg_solicitudes.Sol_TipoSeg;// NO LICITADA | LICITADA
      const nro_poliza = this.data.seg_solicitudes.Sol_Poliza;
      let cobertura_sol = '';
          
      this.data.seg_deudores.forEach((deudor:SegDeudores)=>{
        const saldo_actual_verificado : number = parseFloat(deudor.Deu_MontoActAcumVerif);
        const saldo_actual_solicitud = monto_solicitado + saldo_actual_verificado;
        deudor['saldo_actual_solicitud'] = saldo_actual_solicitud;
        // determinar cobertura
        if(tipo_seguro){

            //const cobertura = this.configParam.tipos_cobertura.find(row=> 
            //saldo_actual_solicitud  >= row.monto_desde && saldo_actual_solicitud  <= row.monto_hasta
            //&& row.moneda == moneda && row.nro_poliza == nro_poliza && row.tipo_seguro==tipo_seguro);

            console.log(`DETERMINAR COBERTURA: 
              monto_solicitado : ${monto_solicitado}
              saldo_actual_verificado : ${saldo_actual_verificado} 
              saldo_actual_solicitud : ${saldo_actual_solicitud} 
              tipo seguro : ${tipo_seguro} 
              nro_poliza : ${nro_poliza} `);

            const cobertura = this.configParam.tipos_cobertura.find(row=> 
              (monto_solicitado  >= row.monto_desde && monto_solicitado  <= row.monto_hasta)
              && (saldo_actual_solicitud >= row.cumulo_desde && saldo_actual_solicitud <= row.cumulo_hasta)
              && row.moneda == moneda  && row.tipo_seguro==tipo_seguro
              );

            if(cobertura){
              deudor.Deu_cobertura = cobertura.tipo_cobertura;
              if(this.data.seg_solicitudes.Sol_TipoSeg=='NO LICITADA'){
                deudor.id_documento_version = (cobertura.tipo_cobertura =='VG'?13:14);
              };

              if(this.data.seg_solicitudes.Sol_TipoSeg=='LICITADA'){
                deudor.id_documento_version = 27;
              };
              
              deudor.Deu_Poliza = cobertura.nro_poliza;	
            }else{
              this.msg.warning(`${deudor.Deu_Nombre} : No se pudo determinar la cobertura `)
            }


        }

      });

    } catch (error) {
      console.error(error);
    }

  }


  onGuardarDatosEdit(){

    this.sumaSaldoActualSolicitud();

    //Datos deudores editados
    let deudores = [];
    this.data.seg_deudores.forEach(deudor=>{
      let deudorObj = {};
      deudorObj['Deu_Id'] = deudor.Deu_Id;
      deudorObj['Deu_CiudadNac'] = deudor.Deu_CiudadNac;
      deudorObj['Deu_Peso'] = deudor.Deu_Peso;
      deudorObj['Deu_Talla'] = deudor.Deu_Talla;
      deudorObj['Deu_DetACtiv'] = deudor.Deu_DetACtiv;
      deudorObj['Deu_Mano'] = deudor.Deu_Mano;
      deudorObj['Deu_MontoActAcumVerif'] = deudor.Deu_MontoActAcumVerif;
      deudorObj['Deu_Incluido'] = deudor.Deu_Incluido;
      deudorObj['Deu_cobertura'] = deudor.Deu_cobertura;
      deudorObj['id_documento_version'] = deudor.id_documento_version;
      deudorObj['Deu_Poliza']= deudor.Deu_Poliza;
      deudorObj['Deu_TasaSeg']= deudor.Deu_TasaSeg;
      deudorObj['Deu_PrimaSeg']= deudor.Deu_PrimaSeg;
      deudores.push(deudorObj);
    })

    this.segDeudoresService.updateDeudores(deudores).subscribe(resp=>{
      console.log('Respuesta la guardar deudores:',resp);
      const response = resp as { status: string; messages: []; data: [] };
      if(response.status=='OK'){
        this.msg.success('Datos de los deudores se guardaron')
      }else{
        this.msg.error('No se pudeieron guardar los datos de los deudores')
      }
      
    });

  };

  onDesistir(){
    if(this.desistimiento.causal && this.desistimiento.autorizado_por){

      const model = {
        causal: this.desistimiento.causal,
        autorizado_por : this.desistimiento.autorizado_por,
        idsol: this.data.seg_solicitudes.Sol_IdSol,
        usuario:`${this.authService.usuario.usuario_nombre_completo} `
      };

      this.segSolicitudesService.desistimientoSolicitud(model).subscribe(resp=>{
        const response = resp as { status: string; messages: []; data: [] };
        if(response.status=='OK'){
          this.msg.success('La Solicitud Desistio con exito','SOLICITUD');
          ///seguros/seguro-add
          this.router.navigate(['/seguros/seguro-add']);
        }else{
          this.msg.error('La Solicitud no se pudo Desistir','SOLICITUD')
        }
      })


    }else{
      this.msg.error('Causal y Autorizado por , requeridos','Desistimiento');
    }

  };

  onEliminarSolicitud(){

    Swal.fire({
      title: `¿ESTÁ USTED SEGURO DE ELIMINAR LA SOLICITUD ${this.data.seg_solicitudes.Sol_NumSol} ?`,
      showCancelButton: true,
      confirmButtonText: `Eliminar`,
      cancelButtonText: `Cancelar`,
      confirmButtonColor: '#16A085',
      cancelButtonColor: '#F1948A',
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowOutsideClick: false,
          text: 'Eliminando la Solicitud ...'
        });
        Swal.showLoading();
        const id = this.data.seg_solicitudes.Sol_IdSol;
        this.segSolicitudesService.delete(id).subscribe(resp=>{
          Swal.close();
          const response = resp as { status: string; messages: []; data: [] };
          if(response.status=='OK'){
            this.msg.info('La Solicitud se borro con exito','SOLICITUD');
            ///seguros/seguro-add
            this.router.navigate(['/AppMain/pnetseg/seguros/seguro-add']);
          }else{
            this.msg.error('La Solicitud no se pudo borrar','SOLICITUD')
          }

        })
      }
    });

  }

  // funciion DJS filtra declaraciones del deudor
  onDeclaracionDeudor= async(deuId:number)=>{
    
    this.declaraciones = [];
    this.openModalCuestion='block';
    const declara_codeu = await this.data.seg_adicionales.filter(row=>row.Adic_IdDeu==deuId);
    declara_codeu.sort((a,b)=> parseInt(a.Adic_Pregunta) - parseInt(b.Adic_Pregunta) );
    //this.declaraciones = JSON.parse(JSON.stringify(declara_codeu));
    this.declaraciones = [...declara_codeu];

  }

  // funcion DJS cuestionario
  onAceptarCuestionario(){

    let aceptar : boolean = true;
    let messages : string[] = [];

    for(const row of this.declaraciones){

      if(row.valida_por){
        if(row.valor_si){
          row.Adic_Respuesta = 'S||||||||||';
          row.Adic_Comment = '';
        }else{
          if(row.valor_no){
            if (row.Adic_Comment){
              row.Adic_Respuesta = '|S|||||||||';
            }else{
              messages.push(`Pregunta ${row.Adic_Pregunta}, requiere Aclaración`);
            }
          }else{
            messages.push(`Pregunta ${row.Adic_Pregunta}, requiere SI o NO`);
          }
        }
      }else{
        if(row.valor_no){
          row.Adic_Respuesta = '|S|||||||||';
          row.Adic_Comment = '';
        }else{
          if(row.valor_si){
            if (row.Adic_Comment){
              row.Adic_Respuesta = 'S||||||||||';
            }else{
              messages.push(`Pregunta ${row.Adic_Pregunta}, requiere Aclaración`);
            }
          }else{
            messages.push(`Pregunta ${row.Adic_Pregunta}, requiere SI o NO`);
          }
        }
      }

    };

    if(messages.length>0){
      this.msg.error(messages.toString(),'Cuestionario');
    }else{
      console.log('Declaraciones',this.declaraciones);
      // guardar adicionales
      this.segAdicionalesService.updateAdiconales(this.declaraciones).subscribe(resp=>{
        Swal.close();
        console.log('Response Update declaraciones:',resp);
        const response = resp as { status: string; messages: []; data: {} };
        if (response.status == 'OK') {
          this.msg.success('Se Guardo con exito', 'DJS');
          /*
          for(const row of this.declaraciones){
            const djsObj = this.data.seg_adicionales.find(el=>el.Adic_Id == row.Adic_Id);
            
            djsObj.Adic_Respuesta = row.Adic_Respuesta;
            djsObj.Adic_Texto = row.Adic_Texto;
            console.log(djsObj);
          }
          */

        } else {
          this.msg.error(response.messages.toString(), 'DJS');
        }
      });
      this.openModalCuestion='none';
    };

  }

  onAclaracion(preg:SegAdicionales){
    let esComentario = false;
    if(this.estado_codigo == 'P' && (preg.valida_por != preg.valor_si) ){
      esComentario = true;
    }
    return esComentario;
  }

  getEif(eif:string){
    let eif_texto= ""
    if(eif=='1'){
      eif_texto= "Banco PyME Ecofuturo S.A."
    }
    return eif_texto
  }


  onChangeInputDeu(valor:string,campo:string):void {
    //const segDeudor = this.folderSolicitud.seg_deudores.filter((row) => row.Deu_NIvel == 0)[0];
    const segDeudor = this.segDeudores.filter(row => row.Deu_Id == this.id_deudor)[0];
    if(segDeudor[campo]){
      if(segDeudor[campo] != valor){
        this.offGuardarDeudor = false;
      }else{
        this.offGuardarDeudor = true;
      }
    }else{
      if(valor){
        this.offGuardarDeudor = false;
      }else{
        this.offGuardarDeudor = true;
      }
    }    
  };

  onChangeInputCoDeu(valor:string,campo:string,origen:SegDeudores):void {
    //const segDeudor = this.folderSolicitud.seg_deudores.filter((row) => row.Deu_Id == origen.Deu_Id)[0];
    const segDeudor = this.segDeudores.filter(row => row.Deu_Id == origen.Deu_Id)[0];
    if(segDeudor[campo]){
      if(segDeudor[campo] != valor){
        this.offGuardarCoDeudor = false;
      }else{
        this.offGuardarCoDeudor = true;
      }
    }else{
      if(valor){
        this.offGuardarCoDeudor = false;
      }else{
        this.offGuardarCoDeudor = true;
      }
    }    
  };

  dosPasos(){
    Swal.mixin({
      confirmButtonText: 'siguiente',
      confirmButtonColor: '#16A085',
      showCancelButton: true,
      cancelButtonColor: '#F1948A',
      progressSteps: ['1', '2']
    }).queue([
      {
        title: 'Advertencia!',
        text: 'MARIANO ROJAS, excedio los límites de asegurabilidad permitidos, por lo que este será excluido, para fines de la suscripción.'
      },
      'Observaciones'
    ]).then((result:any) => {

      if(result.value){
        console.log(result.value);
      }

    })
  }



  onValidar = async()=>{


    this.onGuardarDatosEdit();

    this.observaciones = [];
    this.advertencias = [];

    // datos requeridos SOLICITUD
    // Sol_MontoSol | Sol_MonedaSol | Sol_TipoSeg | Sol_TipoParam | Sol_Poliza | Sol_EstadoSol
    if (!this.data.seg_solicitudes.Sol_MontoSol){this.observaciones.push(`Solicitud :  Monto solicitado requerido `)};
    if (!this.data.seg_solicitudes.Sol_MonedaSol){this.observaciones.push(`Solicitud :  Moneda requerido `)};
    if (!this.data.seg_solicitudes.Sol_TipoSeg){this.observaciones.push(`Solicitud :  Tipo de Seguro requerido `)};
    //if (!this.data.seg_solicitudes.Sol_TipoParam){this.observaciones.push(`Solicitud :  Tipo de Cobertura requerido `)};
    //if (!this.data.seg_solicitudes.Sol_Poliza){this.observaciones.push(`Solicitud :  Nro.Poliza requerido `)};
    if (!this.data.seg_solicitudes.Sol_EstadoSol){this.observaciones.push(`Solicitud :  Estado Solicitud requerido `)};

    // datos requeridos DEUDORES
    // Deu_DetACtiv | Deu_CiudadNac | Deu_Peso | Deu_Talla | Deu_MontoActAcumVerif | Deu_Mano
    // saldo_actual_solicitud | Hist_TipoSeg
    
    const cant_obligados = this.data.seg_deudores.length;
    let cant_incluido = 0;
    // 4.CAMPOS REQUERIDOS EN DJS >> Adic_Respuesta: "S||||||||||"
    let djs = true; // DJS esta NORMAL

    this.data.seg_deudores.forEach(deudor=>{

      //solo deudores incluidos
      if(deudor.Deu_Incluido == 'S'){
        let hay_djs = false;
        let hay_ben = false;

        if(!deudor.Deu_CiudadNac){this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')} : Cuidad de Nacimiento requerido `)};
        if(!deudor.Deu_Peso){this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')} : Peso (KGrs) requerido `)};
        if(!deudor.Deu_Talla){this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')} : Talla (Cms) requerido `)};
        if(!deudor.Deu_Mano){this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')} : Zurd@/Derecho@: requerido `)};
        if(!deudor.Deu_DetACtiv){this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')} : Detalle de la Actividad requerido `)};
        if( parseFloat(deudor.Deu_MontoActAcumVerif)<0){this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')} : SALDO ACTUAL VERIFICADO no valido `)};
        if(!deudor.saldo_actual_solicitud){this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')} : SALDO ACTUAL + SOLICITUD requerido `)};
  
        const benDeudorObj = this.data.seg_beneficiarios.find(el=>el.Ben_IdDeu==deudor.Deu_Id);
        if(!benDeudorObj){
          this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')} : Beneficiarios requeridos `)
        }

        cant_incluido++;

        // control de DJS
        this.data.seg_adicionales.forEach(row=>{
          if(row.Adic_IdDeu == deudor.Deu_Id){
            hay_djs = true;
            if(row.valor_si != row.valida_por){
              djs = false;
              if(!row.Adic_Comment){
                this.observaciones.push(`DJS ${this.getDeudor(row.Adic_IdDeu,'nombres')} : Pregunta ${row.Adic_Pregunta}, requiere SI o NO o Aclaración `);
              }
            }
          }
        });
        if(!hay_djs){
          this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')}: DJS es requerido  `)
        }

        // validamos BENEFICIARIOS
        let num_p = 0;
        let num_c = 0;
        this.data.seg_beneficiarios.forEach(el=>{
          console.log(el.Ben_IdDeu,deudor.Deu_Id);
          if(el.Ben_IdDeu == deudor.Deu_Id){
            hay_ben = true;
            if(el.Ben_Tipo == 'CONTINGENTE'){num_c++};
            if(el.Ben_Tipo == 'PRIMARIO'){num_p++};
          }
        });
        if(this.data.seg_solicitudes.Sol_TipoSeg == 'NO LICITADA' ){
          if(num_c==0){this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')}: Debe completar los Beneficiarios CONTINGENTES `)};
        }
        if(num_p==0){this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')}: Debe completar los Beneficiarios PRIMARIOS  `)};
        if(!hay_ben){
          this.observaciones.push(`${this.getDeudor(deudor.Deu_Id,'nombres')}: Beneficiarios es requerido  `)
        }

      }

    });

    // CONTROL DE OBLIGADOS
    console.log(`OBLIGADOS : ${cant_obligados} INCLUIDO : ${cant_incluido} `);

    if( ! this.esObligadosIncluidos){
      if(cant_obligados>1 && cant_incluido<=1){
        // this.observaciones.push(`Se requiere por lo menos un codeudor `);
      };
    };

    // HAY OBSERVACIONES
    if(this.observaciones.length>0){
      this.openModalValida = 'block';
      //this.solicitudValid = false;
    }else{

      this.msg.success('Validación satisfactoria ','VALIDACION');
      this.solicitudValid = true;
      // E S T A D O  D E  L A  S O L I C I T U D
      // FREE COVER O A+B+C
      const poliza = this.data.seg_solicitudes.Sol_Poliza;
      const cartera = this.data.seg_solicitudes.Sol_TipoSeg;// NO LICITADA | LICITADA
      const cobertura = this.data.seg_solicitudes.Sol_TipoParam; // VG | DH
      const monto = this.data.seg_solicitudes.Sol_MontoSol;
      const moneda = this.data.seg_solicitudes.Sol_MonedaSol;
      let nro_poliza;// = this.data.seg_solicitudes.Sol_Poliza;
      let estado_evaluado:string;
      let respuestasEstadosEvaluados:RespuestaEvaluados[]=[];

      if(cartera=='LICITADA'){

        this.data.seg_deudores.forEach(deudor=>{

          if(deudor.Deu_Incluido=='S'){
            // k_param = (parseFloat(peso)+100)-parseFloat(talla);
            // imc_param = parseFloat(peso)/((paserFloat(talla)/100)*(paserFloat(talla)/100))
            const edad = deudor.Deu_Edad;
            const peso = deudor.Deu_Peso;
            const talla = deudor.Deu_Talla;
            const saldo_actual_verificado_titular = deudor.Deu_MontoActAcumVerif;
            const k_param = (parseFloat(peso)+100)-parseFloat(talla);
            const monto_saldo = monto + parseFloat(saldo_actual_verificado_titular);
            nro_poliza = deudor.Deu_Poliza;

            // DJS del deudor >> Adic_Respuesta: "S||||||||||"
            let djs_deu = true; // DJS esta NORMAL
            this.data.seg_adicionales.forEach(row=>{
              if(row.Adic_IdDeu == deudor.Deu_Id){
                if(row.valor_si != row.valida_por){
                  djs_deu = false;
                }
              }
            });

            console.log(`1.VALIDACION PARAMETROS :
            deudor : ${deudor.Deu_Nombre}
            cartera: ${cartera},
            cobertura: ${cobertura},
            monto_saldo:${monto_saldo},
            peso:${peso},
            talla:${talla},
            djs:${djs_deu},
            k:${k_param},
            estado_evaludado:${estado_evaluado}`);

            console.log(deudor.Deu_Nombre,':',cobertura,monto_saldo,k_param);
            const estadoDeudor = this.configParam.estado_solicitud_licitada_update.find(row=>
              (edad >= row.edad_min && edad <= row.edad_max) &&
              (monto_saldo >= row.monto_desde && monto_saldo <= row.monto_hasta) &&
              (k_param >= row.k_desde && k_param <= row.k_hasta) &&
              djs_deu == row.djs && row.nro_poliza == nro_poliza
            );

            respuestasEstadosEvaluados.push(estadoDeudor);

            if(estadoDeudor){
              if(!estado_evaluado){
                estado_evaluado = estadoDeudor.estado
              }
              if(estadoDeudor.estado != 'A'){
                estado_evaluado = estadoDeudor.estado
              }
            }else{
              estado_evaluado = '?';
              this.msg.warning('Rangos NO DETERMINADOS para el Estado de la Solicitud ','VALIDACION')
            }


          };

        });

      }

      // djs, k , monto , imc
      if(cartera=='NO LICITADA'){

        this.data.seg_deudores.forEach(deudor=>{

          if(deudor.Deu_Incluido=='S'){
            // k_param = (parseFloat(peso)+100)-parseFloat(talla);
            // imc_param = parseFloat(peso)/((paserFloat(talla)/100)*(paserFloat(talla)/100))
            const peso = deudor.Deu_Peso;
            const talla = deudor.Deu_Talla;
            const edad = deudor.Deu_Edad;
            const saldo_actual_verificado_titular = deudor.Deu_MontoActAcumVerif;
            const k_param = (parseFloat(peso)+100)-parseFloat(talla);
            const imc_param = parseFloat(peso)/(parseFloat(talla)*(parseFloat(talla)/10000));
            const monto_saldo = monto + parseFloat(saldo_actual_verificado_titular);
            const seguro_anterior = (deudor.Hist_TipoSeg?deudor.Hist_TipoSeg:cobertura);
            const seguro_actual = (deudor.Deu_cobertura?deudor.Deu_cobertura:cobertura);
            nro_poliza = deudor.Deu_Poliza;

            // DJS del deudor >> Adic_Respuesta: "S||||||||||"
                        let djs_deu = true; // DJS esta NORMAL
                        this.data.seg_adicionales.forEach(row=>{
                          if(row.Adic_IdDeu == deudor.Deu_Id){
                            if(row.valor_si != row.valida_por){
                              djs_deu = false;
                            }
                          }
                        });

            console.log(`1.VALIDACION PARAMETROS :
            deudor : ${deudor.Deu_Nombre}
            cartera: ${cartera},
            cobertura: ${cobertura},
            edad: ${edad},
            monto_saldo:${monto_saldo},
            peso:${peso},
            talla:${talla},
            djs:${djs_deu},
            k:${k_param},
            imc:${imc_param}
            estado_evaludado:${estado_evaluado}`);

            const estadosSolicitudNoLicitada = this.configParam.estados_solicitud_no_licitada.find(row=>
              row.nro_poliza == nro_poliza);

            console.log(`2.VALICION RANGOS : `,estadosSolicitudNoLicitada);

            if (estadosSolicitudNoLicitada){
              const factor = estadosSolicitudNoLicitada.factor;
              if(factor == 'imc'){
                const estadosPoliza = estadosSolicitudNoLicitada.estados.find(row=>
                  (edad >= row.edad_min && edad <= row.edad_max) &&
                  (monto_saldo >= row.monto_min && monto_saldo <= row.monto_max) &&
                  (imc_param >= row.imc_desde && imc_param <= row.imc_hasta) &&
                  djs_deu == row.djs
                );

                respuestasEstadosEvaluados.push({...estadosPoliza,deudor: `${deudor.Deu_Nombre} ${deudor.Deu_Paterno}` ,id_deudor:deudor.Deu_Id });

                console.log(`3.VALICION RESPUESTA ESTADO : `,estadosPoliza);
                if(estadosPoliza){
                  if(!estado_evaluado){
                    estado_evaluado = estadosPoliza.estado;
                    console.log('3.1.set estado_evaludado :',estado_evaluado)
                  }
                  if(estadosPoliza.estado != 'A'){
                    estado_evaluado = estadosPoliza.estado;
                    console.log('3.2.set estado_evaludado :',estado_evaluado)
                  }
                }else{
                  estado_evaluado = '?';
                  this.msg.error(`${deudor.Deu_Nombre},Rangos NO DETERMINADOS`,'VALIDACION')
                }    
              }else{
                const estadosPoliza = estadosSolicitudNoLicitada.estados.find(row=>
                  row.seguro_actual == seguro_actual &&
                  row.seguro_anterior == seguro_anterior &&
                  (monto_saldo >= row.monto_desde && monto_saldo <= row.monto_hasta) &&
                  (k_param >= row.k_desde && k_param <= row.k_hasta) &&
                  djs == row.djs
                );
                console.log(`4.VALICION RESPUESTA ESTADO : `,estadosPoliza);
                if(estadosPoliza){
                  if(!estado_evaluado){
                    estado_evaluado = estadosPoliza.estado;
                    console.log('4.1.set estado_evaludado :',estado_evaluado)
                  }
                  if(estadosPoliza.estado != 'A'){
                    estado_evaluado = estadosPoliza.estado;
                    console.log('4.2.set estado_evaludado :',estado_evaluado)
                  }
                }else{
                  estado_evaluado = '?';
                  this.msg.warning(`${deudor.Deu_Nombre},Rangos NO DETERMINADOS`,'VALIDACION')}
              
              }

            }else{
              estado_evaluado = '?';
              this.msg.warning('Rangos NO DETERMINADOS para la POLIZA ','VALIDACION')};

          };

        });

      }

      console.log('Estado final estado_evaludado :',estado_evaluado);
      if(estado_evaluado){
        if(estado_evaluado=='A'){
          this.aprobacionAutomatica(estado_evaluado);
        }
        if(['C1','C2'].includes(estado_evaluado) ){

          let mensajeFinal:string;
          // DETERMINAR MENSAJE A LA COMPANIA
          if(respuestasEstadosEvaluados.length > 0){

            mensajeFinal='Se esta determinando enviar a la compañia por: <br>';
            respuestasEstadosEvaluados.forEach(row=>{

              if(['C1','C2'].includes(row.estado)){

                if(!row.djs){
                  mensajeFinal+=`Declaración de Salud tiene Obsevaciones <br>`;
                }
                if(row.imc_desde){
                  mensajeFinal+=`Revisar los rangos de IMC <br>`;
                }
                if(row.k_desde){
                  mensajeFinal+=`Revisar los los rangos del Factor  K <br>`;
                }
                if(row.monto_max){
                  mensajeFinal+=`Excedio el monto de ${row.monto_max}  <br>`;
                }

                if(row.requisitos){
                  mensajeFinal+=`Los requisitos son: ${row.requisitos}  <br>`;
                }

              };

            });
            
            console.log('6. MENSAJE FINAL',mensajeFinal);

          };
          

          this.evaluacionPendiente(estado_evaluado,mensajeFinal);
        }
        if(estado_evaluado=='R'){
          const rechazo =  respuestasEstadosEvaluados.find(el=>el.estado == 'R');
          if(rechazo){
            Swal.fire({
              icon: 'warning',
              title: 'Advertencia!',
              text: `${rechazo.deudor}, excedio los límites de asegurabilidad permitidos, por lo que este será excluido, para fines de la suscripción.`,
               showCancelButton: true,
               confirmButtonText: `Aceptar`,
               cancelButtonText: `Cancelar`,
               confirmButtonColor: '#16A085',
               cancelButtonColor: '#F1948A',
            }).then(result=>{
              if (result.isConfirmed){
                if(rechazo.id_deudor){
                  console.log('EXCLUIR A : ',rechazo.id_deudor);
                  this.data.seg_deudores.find(el=>el.Deu_Id==rechazo.id_deudor).Deu_Incluido = 'N';
                  this.esObligadosIncluidos = true;
                }
              }
            });
          }
        }
      }else{
        this.msg.warning('No se pudo determinar el estado de la EVALUACION','EVALUACION')
      }

    };

  };



  aprobacionAutomatica(estado_evaluado:string){
    Swal.fire({
      icon: 'question',
      html:`LA SOLICITUD SE APROBARA AUTOMATICAMENTE ?`,
      showCancelButton: true,
      confirmButtonText: `Aceptar`,
      cancelButtonText: `Cancelar`,
      confirmButtonColor: '#16A085',
      cancelButtonColor: '#F1948A',
    }).then((result) => {
      if (result.isConfirmed) {

        this.msg.success('Solicitud Aprobada','SOLICITUD EN PROCESO');
        // cambiar estado a la solicitud
        this.data.seg_solicitudes.Sol_EstadoSol = estado_evaluado;
        let segSolicitudes = new SegSolicitudes();
        segSolicitudes.Sol_IdSol = this.data.seg_solicitudes.Sol_IdSol;
        segSolicitudes.Sol_EstadoSol = estado_evaluado;
        
        this.estado_codigo = estado_evaluado;
        this.data.seg_solicitudes.Sol_EstadoSol = estado_evaluado;
        // Guardar fecha de aprobacion automatica Deu_FechaResolucion
        this.setFechaResolucion();

        this.guardaSegSolicitudes(segSolicitudes);
        this.setEstadoGeneral(estado_evaluado);
        this.guardarObservaciones(estado_evaluado);
        //this.onSendMessage(estado_evaluado);
        this.onSendMessage_A();

      }
    });
  }

  evaluacionPendiente(estado_evaluado:string,msg?:string){

    Swal.fire({
      icon: 'question',
      html:`SOLICITUD SERA ENVIADA PREVIAMENTE A LA COMPAÑIA Y DEBE SER APROBADA POR ELLA`,
      showCancelButton: true,
      confirmButtonText: `Aceptar`,
      cancelButtonText: `Cancelar`,
      confirmButtonColor: '#16A085',
      cancelButtonColor: '#F1948A',
    }).then((result) => {
      if (result.isConfirmed) {
        this.msg.warning('Solicitud en evaluación','SOLICITUD EN PROCESO');
        // cambiar estado a la solicitud
        this.data.seg_solicitudes.Sol_EstadoSol = estado_evaluado;
        let segSolicitudes = new SegSolicitudes();
        segSolicitudes.Sol_IdSol = this.data.seg_solicitudes.Sol_IdSol;
        segSolicitudes.Sol_EstadoSol = estado_evaluado;

        this.estado_codigo = estado_evaluado;
        this.setEstadoGeneral(estado_evaluado);

        this.guardaSegSolicitudes(segSolicitudes);
        
        this.guardarObservaciones(estado_evaluado,msg);
        //this.onSendMessage(estado_evaluado,msg);

        this.onSendMessage_C1_C2(estado_evaluado,msg);

      }
    });
  };

  // Deu_FechaResolucion
  setFechaResolucion(){

    const fechaActual = getDateYYYYmmDD();

    let deudores = [];
    this.data.seg_deudores.forEach(row=>{

        let deudorObj = {};
        row.Deu_FechaResolucion = fechaActual;
        deudorObj['Deu_Id'] = row.Deu_Id;
        deudorObj['Deu_FechaResolucion']= fechaActual;
        deudores.push(deudorObj);

    });

    Swal.fire({
      allowOutsideClick: false,
      text: 'Registrando fecha de resolución  ...'
    });
    Swal.showLoading();
    this.segDeudoresService.updateDeudores(deudores).subscribe(resp=>{
      Swal.close();
      const response = resp as { status: string; messages: []; data: [] };
      if(response.status=='OK'){
        this.msg.success('Se registro la fecha de resolución con exito ');
      }else{
        this.msg.error('No se pudo registrar la fecha de resolución ')
      }
      console.log('Respuesta la guardar deudores:',response);
    });

  }

  onSendMessage_A(){

    const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
    let messageHtml = '';
    const estado_evaluado = 'A';

    if(nro_poliza){

      this.data.seg_deudores.forEach(row=>{
        if(row.Deu_Incluido=='S'){
          messageHtml += `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Deudor ${row.Deu_Nombre} ${row.Deu_Paterno} ${row.Deu_Materno} Aprobada Automáticamente <br>`;
        }
      });

      messageHtml += `Enviado por: ${this.data.seg_solicitudes.Sol_Oficial} `;

      //const msgObj = this.configParam.notificaciones_mensajes.find(row=>
      //  row.estado_evaluado == 'A' && row.nro_poliza.split(',').includes(nro_poliza)
      //);

      const correosEstados = this.configParam.correos_estados.find(row=>
        row.estados_evaluados.split(',').includes(estado_evaluado)
         && row.nro_poliza.split(',').includes(nro_poliza)
      );

      if(correosEstados){

        //[year, month, day].join('-');

        // envio de correo
        let message = new MessageEmail();
        message.to = correosEstados.emails;
        message.cc = correosEstados.cc;
        message.subject = `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} `;
        message.html = messageHtml;
        this.sendMailService.sendMessage(message).subscribe(resp=>{
          const response = resp as { status: string; message: string; data: {} };
          console.log('envio de mensajes',response);
        });

      }else{
        this.msg.warning('No estan definidos las notificaciones');
      }

    };

  }

  onSendMessage_C1_C2(estado_evaluado:string,msg:string){

    const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
    let messageHtml = '';

    if(nro_poliza){

      //const msgObj = this.configParam.notificaciones_mensajes.find(row=>
      //  row.estado_evaluado == estado_evaluado && row.nro_poliza.split(',').includes(nro_poliza)
      //);

      const correosEstados = this.configParam.correos_estados.find(row=>
        row.estados_evaluados.split(',').includes(estado_evaluado)
         && row.nro_poliza.split(',').includes(nro_poliza)
      );

      if(correosEstados){

        messageHtml += `${this.data.seg_solicitudes.Sol_Oficial} , Solicita su atencion a la Solicitud ${this.data.seg_solicitudes.Sol_NumSol}
        de la Endidad Financiera Banco PyME Ecofuturo S.A. <br> 
        RAZON DE LA SOLICITUD: <br>  ${msg}  <br>`;

        messageHtml += `Enviado por: ${this.data.seg_solicitudes.Sol_Oficial} `;

        // envio de correo
        let message = new MessageEmail();
        message.to = correosEstados.emails;
        message.cc = correosEstados.cc;
        message.subject = `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} - Banco PyME Ecofuturo S.A. - ${this.data.seg_solicitudes.Sol_Oficial} `;
        message.html = messageHtml;
        this.sendMailService.sendMessage(message).subscribe(resp=>{
          const response = resp as { status: string; message: string; data: {} };
          console.log('envio de mensajes',response);
        });

      }else{
        this.msg.warning('No estan definidos las notificaciones');
      }

    }

  }

  onSendMessage(estado_evaluado:string,msg?:string){

    const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
    let messageHtml = '';

    if(nro_poliza){

      const msgObj = this.configParam.notificaciones_mensajes.find(row=>
        row.estado_evaluado == estado_evaluado && row.nro_poliza.split(',').includes(nro_poliza)
      );

      const correosEstados = this.configParam.correos_estados.find(row=>
        row.estados_evaluados.split(',').includes(estado_evaluado)
         && row.nro_poliza.split(',').includes(nro_poliza)
      );

      
      if(correosEstados){

        // envio de correo
        let message = new MessageEmail();
        message.to = correosEstados.emails;
        message.cc = correosEstados.cc;
        message.subject = `Pronunciamiento Compañia sobre la Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A.`;

        message.html = `Pronunciamiento Compañia sobre Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A. <br>
         ${msgObj.asunto} <br>
         Enviado por ${this.userinfo.usuario_nombre_completo}`;

        this.sendMailService.sendMessage(message).subscribe(resp=>{
          const response = resp as { status: string; message: string; data: {} };
          console.log('envio de mensajes',response);
        });

      }else{
        this.msg.warning('No estan definidos las notificaciones');
      }

    };

  };

  getEifAseguradora(cod:string){

    // Banco PyME Ecofuturo S.A. = 1 |  Asegurador Alianza = 101 
    // Asegurador La Boliviana Ciacruz = 20001,20002,20006,20007
    //const asegurador = this.configParam.datos_poliza.find(row=>row.nro_poliza==nro_poliza).entidad_seguros;

    let asegurador:string;

    if(cod){
      if(cod == '1'){
        asegurador = 'Banco PyME Ecofuturo S.A.'
      };
      if(cod == '101'){
        asegurador = 'Asegurador Alianza';
      };
      const datosPoliza = this.configParam.datos_poliza.find(row=>row.nro_poliza==cod);
      if(datosPoliza){
        asegurador = datosPoliza.entidad_seguros;
      }
    }

    return asegurador;
  }

  

  guardarObservaciones(estado_evaluado:string,msg?:string){

    const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;

    console.log('USUARIO_ROL:',this.usuario_rol);
    
    if(nro_poliza){

      const msgObj = this.configParam.notificaciones_mensajes.find(row=>
        row.estado_evaluado == estado_evaluado && row.nro_poliza.split(',').includes(nro_poliza)
      );

      if(msgObj){

        let obser : SegObservaciones = new SegObservaciones();

        obser.Obs_IdSol = this.data.seg_solicitudes.Sol_IdSol;
        obser.Obs_Usuario = this.userinfo.usuario_nombre_completo;
        obser.Obs_Observacion = ( msg ? `${msgObj.mensaje_historial} ${msg} ` : `${msgObj.mensaje_historial} ` );
        if(this.usuario_rol=='of'){
          obser.Obs_Eeff = '1';
          obser.Obs_Aseg = '';
        }else{
          obser.Obs_Eeff = '';
          obser.Obs_Aseg = nro_poliza;
        };
        this.segObservacionesService.add(obser).subscribe(resp=>{
          const response = resp as { status: string, message: string, data: SegObservaciones };
          if(response.status == 'OK'){
            this.data.seg_observaciones.push(response.data);
          }
        });

      }else{
        this.msg.warning('No estan definidos las notificaciones');
      }

    }

  };



  guardaSegSolicitudes(model:SegSolicitudes){

    this.segSolicitudesService.update(model).subscribe(resp=>{
      const response = resp as { status: string; messages: []; data: [] };
      if (response.status == 'OK'){
        this.msg.success('Se actualizo la solicitud','SOLICITUD');
      }else{
        this.msg.error('No se pudo guardar datos de la Solicitud','SOLICITUD');
      }
    });

  };

  getSegObservaciones(idSol:number){
    this.segObservacionesService.getIdSol(idSol).subscribe(resp=>{
      const response = resp as { status: string, message: string, data: SegObservaciones[] };
      if(response.status == 'OK'){
        this.segObservaciones = response.data;
      }
    });
  };


  calculate_age = (dob) => {
    var diff_ms = Date.now() - dob.getTime();
    var age_dt = new Date(diff_ms);
    return Math.abs(age_dt.getUTCFullYear() - 1970);
  }


  onCloseBuscarCodeudor(result:boolean){
    this.openModalCodeu='none';
    this.param_buscar.numero_documento = '';
    this.param_buscar.accion ='add-codeudor';
    this.param_buscar.id_folder = this.id_folder;
    if(result){
      //this.getDatosFolder();
    }
  }

  //  GESTION B E N E F I C I A R I O S
  loadBeneficiarios=()=>{
    this.segBeneficiarios = [];
    //this.segBeneficiariosIdDeu = [];
    Swal.fire({
      allowOutsideClick: false,
      text: 'Recuperando datos ...'
    });
    Swal.showLoading();
    this.segBeneficiariosService.getIdSol(this.id_solicitud).subscribe(resp=>{
      Swal.close();
      const response = resp as { status: string, message: string, data: SegBeneficiarios[] };
      if(response.status === 'OK'){
        this.segBeneficiarios = [...response.data];
      }else{
        this.msg.error(response.message);
      }
    },
    err=>{
      Swal.close();
      this.msg.error('No se pudo establecer comunicacion','BENEFICIARIOS');
    }
    );
  };

  // button BENEFICIARIOS (click)
  onOpenBeneficiarios = (idDeu:number,idBen?:number)=>{


    this.id_deudor_Selec = idDeu;

    console.log('ID BENEFICIARIO:',idDeu,this.id_deudor);
    this.deudorSeleccionado.Deu_Id = idDeu;

    const deudorBusca = this.data.seg_deudores.find(el=>el.Deu_Id==idDeu)

    if(deudorBusca){
      this.deudor_nombre_sel = `${deudorBusca.Deu_Nombre} ${deudorBusca.Deu_Paterno} ${deudorBusca.Deu_Materno}`;
    }

    this.segBeneficiariosIdDeu = [];
    this.benIdDeu = idDeu;
    const beneficiarios = this.data.seg_beneficiarios.filter(row=>row.Ben_IdDeu==idDeu);

    //this.segBeneficiariosIdDeu = [...beneficiarios];
    this.segBeneficiariosIdDeu = JSON.parse(JSON.stringify(beneficiarios));

    this.segBeneficiariosIdDeu.map((el,idx)=>{
      if(idBen){
        if(el.Ben_Id==idBen){
          console.log('idx',idx);
          this.bens[idx] = true;
        }else{
          this.bens[idx] = false;
        }
      }else{
        this.bens[idx] = false;
      }
    });
    this.openModalBene='block';
    

  };

  addBeneficiario(){

        const newBen = new SegBeneficiarios();
        newBen.Ben_IdSol = this.data.seg_solicitudes.Sol_IdSol;
        newBen.Ben_IdDeu = this.id_deudor_Selec; 
        newBen.Ben_Porcentaje = "100";
        this.segBeneficiariosIdDeu.push(newBen);
        const len = this.segBeneficiariosIdDeu.length;
        for(let i=0;i<len;i++){
          this.bens[i] = false;
        }
        this.bens[len-1]= true;

  };

  onBeneficiariosValidarCerrar(){

    let valido:boolean=true;
    let obser:string[]=[];
    let porcentaje_p:number=0;
    let porcentaje_c:number=0;
    let nro_p=0;
    let nro_c=0;

    // validacion del beneficiarios
    this.segBeneficiariosIdDeu.forEach(ben=>{
      if(ben.Ben_Tipo == 'PRIMARIO'){
        const num = parseInt(ben.Ben_Porcentaje);
        if(num == 0){
          obser.push('Porcentaje no puede ser cero');
        }
        porcentaje_p +=  num;
        nro_p++;
      }
      if(ben.Ben_Tipo == 'CONTINGENTE'){
        const num = parseInt(ben.Ben_Porcentaje);
        if(num == 0){
          obser.push('Porcentaje no puede ser cero');
        }
        porcentaje_c +=  num;
        nro_c++;
      }
      
      if(!ben.Ben_Nombre){obser.push('Nombre completo es requerido')};
      if(!ben.Ben_NumDoc){obser.push('Numero de documento es requerido')};
      if(!ben.Ben_TipoDoc){obser.push('Tipo de documento es requerido')};
      if(!ben.Ben_Relacion){obser.push('Relación es requerido')};
      if(!ben.Ben_Porcentaje){obser.push('Porcentaje es requerido')};
      if(!ben.Ben_Tipo){obser.push('Tipo de Beneficiario es requerido')};

      if(!ben.Ben_Paterno){
        if(!ben.Ben_Materno){
          obser.push('Por la menos un Apellido es requerido')
        }
      }
    });

    if(nro_p>0){
      if(porcentaje_p!=100){
        obser.push(`Porcentaje PRIMARIA es ${porcentaje_p} debe ser 100%`);
      }
    }
    if(nro_c>0){
      if(porcentaje_c!=100){
        obser.push(`Porcentaje CONTINGENTE es ${porcentaje_c} debe ser 100%`);
      }
    }

    if(obser.length>0){
      valido = false;
    }

    if(valido){
      this.openModalBene='none';
    }else{
      this.msg.error(obser.toString(),'Beneficiarios');
    }

  }

  updateBeneficiarios(){

    let valido:boolean=true;
    let obser:string[]=[];
    let porcentaje_p:number=0;
    let porcentaje_c:number=0;
    let nro_p=0;
    let nro_c=0;

    // validacion del beneficiarios
    this.segBeneficiariosIdDeu.forEach(ben=>{

      if(ben.Ben_Tipo == 'PRIMARIO'){
        const num = parseInt(ben.Ben_Porcentaje);
        if(num == 0){
          obser.push('Porcentaje no puede ser cero');
        }
        porcentaje_p +=  num;
        nro_p++;
      }
      if(ben.Ben_Tipo == 'CONTINGENTE'){
        const num = parseInt(ben.Ben_Porcentaje);
        if(num == 0){
          obser.push('Porcentaje no puede ser cero');
        }
        porcentaje_c +=  num;
        nro_c++;
      }
      
      if(!ben.Ben_Nombre){obser.push('Nombre completo es requerido')};
      

      if(!ben.Ben_Relacion){obser.push('Relación es requerido')};
      if(!ben.Ben_Porcentaje){obser.push('Porcentaje es requerido')};
      if(!ben.Ben_Tipo){obser.push('Tipo de Beneficiario es requerido')}
      else{
        if(ben.Ben_Tipo=='PRIMARIO'){
          if(!ben.Ben_TipoDoc){obser.push('Tipo de documento es requerido')};
          if(!ben.Ben_NumDoc){obser.push('Numero de documento es requerido')};
        }
      };

      if(!ben.Ben_Paterno){
        if(!ben.Ben_Materno){
          obser.push('Por la menos un Apellido es requerido')
        }
      }

    });

    if(nro_p>0){
      if(porcentaje_p!=100){
        obser.push(`Porcentaje PRIMARIO es ${porcentaje_p} debe ser 100%`);
      }
    }
    if(nro_c>0){
      if(porcentaje_c!=100){
        obser.push(`Porcentaje CONTINGENTE es ${porcentaje_c} debe ser 100%`);
      }
    }


    if(obser.length>0){
      valido = false;
    }

    if(valido){
      Swal.fire({
        allowOutsideClick: false,
        text: 'Guardando datos de Beneficiarios ...'
      });
      Swal.showLoading();
      const idSol = this.data.seg_solicitudes.Sol_IdSol;
      this.segBeneficiariosService.updateAll(this.segBeneficiariosIdDeu,idSol).subscribe(resp=>{
        Swal.close();
        const response = resp as { status: string, messages: string[], data: any };
        console.log('updateAll:',response);
        if(response.status == 'OK'){
          this.openModalBene='none';
          this.msg.success('Beneficiarios se guardaron con exito');
          this.data.seg_beneficiarios = response.data;
        }else{
          this.msg.error(`Error guardando beneficiarios ${response.messages}`);
        }
      },ex=>{this.msg.error('No se pudo establecer comunicacion','Beneficiarios')}
      );
    }else{
      this.msg.error(obser.toString(),'Beneficiarios');
    }

  };

  onDelBeneficiario(idBen:number,index:number){

    console.log('onDelBeneficiario:',idBen,index);

    Swal.fire({
      title: 'No Incluir Beneficiario',
      showCancelButton: true,
      confirmButtonText: `Aceptar`,
      cancelButtonText: `Cancelar`,
      confirmButtonColor: '#16A085',
      cancelButtonColor: '#F1948A',
    }).then((result) => {
      if (result.isConfirmed) {
        if(idBen){
          //borrar en el servicios
          this.segBeneficiariosService.delete(idBen).subscribe(resp=>{
            const response = resp as { status: string, message: string, data:{} };
            if(response.status=='OK'){
              // borra filtro y desenfoca la seleccion
              this.segBeneficiariosIdDeu.map((el,idx)=>{
                if(el.Ben_Id==idBen){
                  this.segBeneficiariosIdDeu.splice(index,1);
                }
                this.bens[idx] = false;
                // actualiza data
                let benList = this.data.seg_beneficiarios;
                for( var i = 0; i < benList.length; i++){ 
                  if ( benList[i].Ben_Id === idBen) { 
                    benList.splice(i, 1); 
                  }
                }
              });
              this.msg.success('Beneficiario fue excluido con exito','BENEFICIARIOS');
            }else{
              this.msg.error('Beneficiario no pudo ser excluido con exito','BENEFICIARIOS');
            }
          })
        }else{
          this.segBeneficiariosIdDeu.map((el,idx)=>{
            this.segBeneficiariosIdDeu.splice(index,1);
            this.bens[idx] = false;
          });
        }
      }
    });
  }

  getDeudor(id:number,campo:string){
    const ben = this.data.seg_deudores.find(el=>el.Deu_Id==id);
    if(ben){
      if ( campo == 'nombres'){
        return `${ben.Deu_Nombre} ${ben.Deu_Paterno} ${ben.Deu_Materno}`;
      }
      if( campo == 'tipo'){
        return (ben.Deu_NIvel == 0 ? 'DEUDOR(A)' : 'CODEUDOR(A)');
      }
    }

  };

  onImprimirV2(iddeu,iddoc){

    const param = {
      id_deudor : iddeu
    }

    this.msg.info('Generando los reportes para el Codeudor, favor espere...');
    this.isLoading = true;
    this.reporteService.generarReporteV2(param).subscribe(resp=>{
      const response = resp as {data};
      console.log('RESPONSE REPORTE:',resp);
      this.isLoading = false;
      if(resp){
        // PUBLICAR REPORTE 
        const urlPdf = environment.URL_PUBLIC_PDF_MASIVOS;
        const rutaPdf = urlPdf+`/${response.data.base}`;
        window.open(rutaPdf,'_blank');
      }else{
        this.msg.warning('No se pudo generar el reporte','REPORTE')
      }
    })

  }

  onImprimirDoc(){

    const tipo_seguro = this.data.seg_solicitudes.Sol_TipoSeg; // NO LICITADA | LICITADA
    let tipo_cobertura = this.data.seg_solicitudes.Sol_TipoParam; // VG | DH

    if(!tipo_cobertura && tipo_seguro == 'LICITADA'){
      tipo_cobertura = 'DH'
    }

    // obtener plantilla de reportes, this.configParam  
    const pts = this.configParam.impresion_documentos.filter(row=>
      row.tipo_seguro==tipo_seguro && row.tipo_cobertura == tipo_cobertura);

    this.openModalImprimir = 'block';

  }

  loadDocumentosDeudor(){
    (this.showDocs_1 == true ? this.showDocs_1 = false : this.showDocs_1 = true );
  }

  loadDocumentosCoDeudor(){
    (this.showDocs_2 == true ? this.showDocs_2 = false : this.showDocs_2 = true );
  }

  onViewHistorial(){
    this.openModalHistorial = 'block';
  }

  onDatosEco() {

    const estado_evaluado = 'C1';
    const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;

    const correosEstados = this.configParam.correos_estados.find(row=>
      row.estados_evaluados.split(',').includes(estado_evaluado)
       && row.nro_poliza.split(',').includes(nro_poliza)
    );

    console.log('CORREOS:',correosEstados);

    Swal.fire({
      title: ' Desea actualizar la información de la Solicitud desde el sistema Central del Banco ?',
      showCancelButton: true,
      confirmButtonText: `SI`,
      cancelButtonText: `NO`,
      confirmButtonColor: '#009688',
      cancelButtonColor: '#f44336',
    }).then((result) => {
      if (result.isConfirmed) {
        
        this.getDatosEco();

      }
    });
  }

  getDatosEco = async()=>{
    if (this.id_solicitud){
      Swal.fire({
        allowOutsideClick: false,
        text: 'Actualizando datos ...'
      });
      Swal.showLoading();

      const model = { idsol : this.id_solicitud};

      this.segSolicitudesService.updateServiciosData(model).subscribe(resp=>{
        Swal.close();
        const response = resp  as {status:string,messages:[],data:any,message:string};
        console.log('update-servicios-data:',response);
        if(response.status==='OK'){
          this.msg.success(`Se actualizo con exito la solicitud nro.${this.data.seg_solicitudes.Sol_NumSol}, por favor vuelva a ingresar `);

          //this.router.navigate(['/AppMain/pnetseg/seguros/seguro-add']);
          this.router.navigate(['/seguros/seguro-add']);

        }else{
          this.msg.error(`No se pudo actualizar ${response.message} `);
        }

      });

    }
  };


  onChangeUpload(event){
    this.file = event.target.files[0];
    this.formData.append('file',event.target.files[0],event.target.files[0].name);
  };

  onResponderObser(){

    if(this.respuestaObser){

      const nro_poliza = this.data.seg_deudores[0].Deu_Poliza;
      const estado_evaluado = this.data.seg_solicitudes.Sol_EstadoSol;

      // Observaciones
      let model = new SegObservaciones();
      model.Obs_IdSol = this.id_solicitud;
      model.Obs_Observacion = this.respuestaObser;

      if(this.usuario_rol == 'of'){
        model.Obs_Eeff = '1';
        model.Obs_Aseg = '';
      }else{
        model.Obs_Eeff = '';
        model.Obs_Aseg = nro_poliza;
      };

      model.Obs_Usuario = `${this.userinfo.usuario_nombre_completo} `;

      // registro en las observaciones
      this.segObservacionesService.add(model).subscribe(resp=>{
              const response = resp as { status: string, message: string, data: any };
              if(response.status == 'OK'){
                //this.getSegObservaciones(this.id_solicitud);
                this.data.seg_observaciones.push(response.data);
                this.respuestaObser = '';
      
                this.msg.info('Se registro y envio con exito','Ultima Comunicación')
              }else{
                this.msg.error('No se pudo enviar ni ni registrar','Ultima Comunicación')
              }
      });

      // ENVIO DE MENSAJE DE CORREO
      console.log(`usuario rol [${this.usuario_rol}]  `);

      // enviar oficial => compañia
      let messageHtml = '';

      // oficial
      if(this.usuario_rol == 'of'){

        if(this.file){

          const correosEstados = this.configParam.correos_estados.find(row=>
            row.estados_evaluados.split(',').includes(estado_evaluado)
             && row.nro_poliza.split(',').includes(nro_poliza)
          );

          if(correosEstados){
    
            let mensajeObj = {
              subject : `Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A.`,
              message : `${this.data.seg_solicitudes.Sol_Oficial}, Solicita su atención : <br><br> ${this.respuestaObser}`,
              to : correosEstados.emails,
              cc : correosEstados.cc,
              file : this.file
            }
  
            this.fileUploadService.upload(mensajeObj).subscribe(resp=>{
              console.log('UPLOAD FILE - SEND MAIL : ',resp);
            });
    
          }else{
            this.msg.warning('No estan definidos las notificaciones');
          }

        }

      }else{

        if(this.file){

          const correosEstados = this.configParam.correos_estados.find(row=>
            row.estados_evaluados.split(',').includes(estado_evaluado)
             && row.nro_poliza.split(',').includes(nro_poliza)
          );

          if(correosEstados){
    
            let mensajeObj = {
              subject : `Atención de la Compañia sobre la Solicitud ${this.data.seg_solicitudes.Sol_NumSol} Banco PyME Ecofuturo S.A.`,
              message : `La Compañia solicita su atención :  <br><br> ${this.respuestaObser}`,
              to : correosEstados.emails,
              cc : correosEstados.cc,
              file : this.file
            }
  
            this.fileUploadService.upload(mensajeObj).subscribe(resp=>{
              console.log('UPLOAD FILE - SEND MAIL : ',resp);
            });
    
          }else{
            this.msg.warning('No estan definidos las notificaciones');
          }

        }



      }

    } else {
      this.msg.warning('Respuesta requerida','Ult.Comunicacion');
    }
    

  };


  onSaveResolucion = async()=>{

    Swal.fire({
      title: ` Emitir Resolución  ?`,
      showCancelButton: true,
      confirmButtonText: `Aceptar`,
      cancelButtonText: `Cancelar`,
      confirmButtonColor: '#16A085',
      cancelButtonColor: '#F1948A',
    }).then((result) => {

      if (result.isConfirmed) {

        Swal.fire({
          allowOutsideClick: false,
          text: 'Guardando datos ...'
        });
    
        Swal.showLoading();
        
        let aprobaciones : SegAprobaciones[] = [];
    
        let aprobado = false;
        let rechazado = false;
        let codeudor = false;
        let extra_prima = false;
        let condiciones_exclusiones = false;
    
        const resoluciones = this.configParam.estado_solicitud_resolucion;

        console.log(this.data.seg_deudores);
    
        // persiste el la tabla SegAprobaciones
        this.data.seg_deudores.forEach(deu=>{

          const idDeu = deu.Deu_Id;
          const idSol = deu.Deu_IdSol;
          const esCode = deu.Deu_NIvel;
          (deu.aprobado?aprobado=true:null);
          (deu.rechazado?rechazado=true:null);
          const extra = deu.extra_prima;
          const condi = deu.condiciones_exclusiones;
          const causal = deu.causal;
    
          //determinar estado
          (esCode == 1 ? codeudor=true : null);
          (extra? extra_prima=true:null);
          (condi? condiciones_exclusiones=true:null);
    
          //registro para guardar
          let aprobacion = new SegAprobaciones();
          aprobacion.Apr_IdDeu = deu.Deu_Id;
          aprobacion.Apr_IdSol = deu.Deu_IdSol;
          if(aprobado){

            aprobacion.Apr_Aprobado = 'S'
            aprobacion.Apr_ExtraPrima = extra;
            aprobacion.Apr_Condiciones = condi;
            
          }else{

            if(rechazado){
              aprobacion.Apr_Aprobado = 'N';
              aprobacion.Apr_Condiciones = causal;
              deu.Deu_Incluido = 'N';
            };

          }

          console.log(aprobacion);

          
          this.segAprobacionesService.add(aprobacion).subscribe(resp=>{
            const response = resp as { status: string, message: string, data:{} };
            if(response.status=='OK'){
              this.data.seg_aprobaciones.push(aprobacion)
            }
          });
        


        });
        // determinar el estado
        const estadoResol = resoluciones.find(row=>
                row.aprobado == aprobado && row.rechazado == rechazado &&
                row.codeudor == codeudor && row.extra_prima == extra_prima && 
                row.condiciones_exclusiones == condiciones_exclusiones
        );
        
        
        if(estadoResol){

          // guardar el estado de la solicitud
          const estadoSol = this.configParam.estado_solicitud.find(row=>row.codigo==estadoResol.estado);
          this.estado_general = estadoSol.estado;
          this.estado_codigo = estadoResol.estado;

          this.data.seg_solicitudes.Sol_EstadoSol = estadoResol.estado;

          let segSolicitudes:SegSolicitudes = new SegSolicitudes();
          segSolicitudes.Sol_IdSol = this.id_solicitud;
          segSolicitudes.Sol_EstadoSol = estadoResol.estado;
          this.setEstadoGeneral(estadoResol.estado);
          this.guardaSegSolicitudes(segSolicitudes);
          this.setFechaResolucion();

          // crear la plantilla
          let contenidoHtml: string = `
            <br><br>
            <table border="1">
              <tr><td rowspan="2" style="text-align:center;">CODEUDOR</td><td colspan="4" style="text-align:center;">RESULTADO DE LA EVALUACION</td></tr>
              <tr><td>APROBADO</td><td>EXTRAPRIMA</td><td>CONDICIONES Y EXCLUSIONES</td><td>CAUSAL RECHAZO</td></tr>
          `;
    
          this.data.seg_deudores.forEach(deu=>{
            contenidoHtml += `<tr>
                <td>${deu.Deu_Nombre} ${deu.Deu_Paterno} ${deu.Deu_Materno}</td>
                <td>${deu.aprobado ? 'SI': ''} ${deu.rechazado ? 'NO' : ''}</td>
                <td>${deu.extra_prima ? deu.extra_prima : '-'}</td>
                <td>${deu.condiciones_exclusiones ? deu.condiciones_exclusiones : '-'}</td>
                <td>${deu.causal ? deu.causal : ''}</td>
              </tr>`;
          });
          contenidoHtml += '</table>';

          this.guardarObservaciones(estadoResol.estado);
          
          this.onGuardarDatosEdit();

          this.onSendMessage(estadoResol.estado,contenidoHtml);
          
          this.msg.success('Operación se realizo con exito','RESOLUCION');

        }else{
          this.msg.warning('No esta definido los estados de la resolucion','RESOLUCION');
        }
        
        
        Swal.close();


      }
    });

  };


  guardarDeudores(){
    //this.codeudores_def
    let deudores = [];
    this.codeudores_def.forEach(deudor_def=>{
      let deudor = {};
      deudor_def.forEach((elem) => {
        //if( elem.editable == true){
        if(elem.campo!=" "){
          deudor[elem.campo] = elem.valor;
        }
        //}
      });
      deudores.push(deudor);
    });
    this.segDeudoresService.updateDeudores(deudores).subscribe(resp=>{
      const response = resp as { status: string; messages: []; data: [] };
      console.log('Respuesta la guardar deudores al validar:',response);
    })
  };


  getViewData(dataObj){

    let html1 = '';
    const data1 = Object.keys(dataObj);

    data1.forEach((campo,i)=> {
      html1 += `<b class='w3-tiny w3-text-teal'>${campo}</b><br>`;
      const valor = dataObj[campo];
      if(Array.isArray(valor)){
        html1 += this.getTable(valor);
      }

    });

    return html1;
    
  }

  getTable = (content)=>{
    const rows = content;
    let html = '<div class="w3-padding-16 w3-responsive"><table class="w3-table-all w3-tiny w3-hoverable">';
    html += '<thead><tr>';
    for (let campo in rows[0]) {
        html += '<th>' + campo + '</th>';
    }
    html += '</tr></thead><tbody>';
    for (let f = 0; f < rows.length; f++) {
        html += '<tr>';
        for (var j in rows[f]) {
          const rowsObj = rows[f][j];
          if(Array.isArray(rowsObj)){
            html += '<td> ' +  this.getTable(rowsObj) + '</td>';
          }else{
            html += '<td> ' + rowsObj + '</td>';
          }
          
        }
    }
    html += '</tr></tbody></table></div>';
    return html;
  }



}

interface RespuestaObs{
  respuesta:string;
  adjunto:string;
};

interface Observaciones{
  concepto?:string;
  observacion?:string;
};

interface ConfigParam {
  datos_poliza?:any[];
  estado_solicitud?:any[];
  tipo_cobertura_update?:any[];
  estado_solicitud_update?:any[];
  estado_solicitud_licitada_update?:any[];
  djs_salud?:any[];
  impresion_documentos?:any[];
  tipo_cobertura_no_licitada?:any[];
  polizas?:{productos:any[]};
  numero_poliza?:any[];
  entidades_seguro_poliza?:any[];
  estado_solicitud_resolucion?:any[];
  notificaciones_mensajes?:any[];
  tipos_cobertura?:any[];
  estado_solicitud_no_licitada_alianza?:any[];
  autorizaciones_solicitud?:any[],
  estados_solicitud_no_licitada?:any[],
  correos_estados?:any[]
};

interface EstadosSolicitud{
  estado?:string;
  codigo?:string;
};

interface DatosCalculados{
  tasa_mensual_ref:number;
  prima_mensual_ref:number;
  entidad_seguros_1:string;
  entidad_seguros_2:string;
  saldo_actual_solicitado:number;
};

interface DataSolicitud{
  seg_solicitudes : SegSolicitudes;
  seg_deudores : SegDeudores[];
  seg_operaciones : SegOperaciones;
  seg_beneficiarios : SegBeneficiarios[];
  seg_adicionales : SegAdicionales[];
  seg_aprobaciones : SegAprobaciones[];
  seg_observaciones : SegObservaciones[];
  estado_general : string;
  config_param : {}
};

interface RespuestaEvaluados{
  djs?: boolean;
  edad_max: number;
  edad_min: number;
  estado: string;
  imc_desde?: number;
  imc_hasta?: number;
  k_desde?:number;	
  k_hasta?:number;
  monto_max?: number;
  monto_min?: number;
  requisitos?: string;
  tipo_covertura?: string;
  deudor?:string;
  id_deudor?:number;
};

const getDateYYYYmmDD = () => {
  var d = new Date(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
};


