import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  private URL_API: string = `${environment.URL_API}/upload-file`;

  constructor(private http:HttpClient) { }

  upload(mensajeObj):Observable<any>{

    console.log('file-upload.service:',mensajeObj)

    const formData = new FormData();
    formData.append('file',mensajeObj.file);
    formData.append('subject',mensajeObj.subject);
    formData.append('message',mensajeObj.message);
    formData.append('to',mensajeObj.to);
    formData.append('cc',mensajeObj.cc);

    return this.http.post(`${this.URL_API}/upload-send-email/123`,formData);

  };

  subirArchivoPdf(formData:FormData,param:string){
    return this.http.post(`${this.URL_API}/upload-send-email/${param}`,formData);
  }




}
