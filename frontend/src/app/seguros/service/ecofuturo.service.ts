import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EcofuturoService {

  private URL_API: string = `${environment.URL_API}/ecofuturo`;

  constructor(private http: HttpClient) {}

  // http://localhost:4005/api-seguros/ecofuturo/getsolicitud/172839
  getSolicitud(nro_solicitud:number){
    return this.http.get(`${this.URL_API}/getsolicitud/${nro_solicitud}`);
  }

  // http://localhost:4005/api-seguros/ecofuturo/getsolicitudprimera/172839
  getSolicitudPrimeraEtapa(nro_solicitud:number){
    return this.http.get(`${this.URL_API}/getsolicitudprimera/${nro_solicitud}`);
  }

  // http://localhost:4005/api-seguros/ecofuturo/getsolicitudsegunda/172839
  getSolicitudSegundaEtapa(nro_solicitud:number){
    return this.http.get(`${this.URL_API}/getsolicitudsegunda/${nro_solicitud}`);
  }

  // http://localhost:4005/api-seguros/ecofuturo/getsolicitudtercera/172839
  getSolicitudTerceraEtapa(nro_solicitud:number){
    return this.http.get(`${this.URL_API}/getsolicitudtercera/${nro_solicitud}`);
  }

  // http://localhost:4005/api-seguros/ecofuturo/getsolicitudall/172839
  // http://localhost:4005/api-seguros/ecofuturo/getsolicitudall/159322
  getSolicitudAll(nro_solicitud:number){
    return this.http.get(`${this.URL_API}/getsolicitudall/${nro_solicitud}`);
  }

}
