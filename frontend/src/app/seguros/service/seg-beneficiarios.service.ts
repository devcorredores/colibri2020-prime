import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SegBeneficiarios } from '../models/seg-beneficiarios';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SegBeneficiariosService {
  private URL_API: string = `${environment.URL_API}/seg-beneficiarios`;

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.URL_API);
  }

  getOne(id:number){
    return this.http.get(`${this.URL_API}/${id}`);
  }

  add(model: SegBeneficiarios) {
    return this.http.post(this.URL_API, model);
  }

  update(model: SegBeneficiarios) {
    return this.http.put(`${this.URL_API}/${model.Ben_Id}`, model);
  }

  delete(id:number) {
    return this.http.delete(`${this.URL_API}/${id}`);
  }

  getIdSol(Ben_IdSol:number){
    return this.http.get(`${this.URL_API}/IdSol/${Ben_IdSol}`);
  }

  updateAll(models : SegBeneficiarios[],idSol?:number){
    return this.http.put(`${this.URL_API}/all/${idSol}`,models);
  }
}
