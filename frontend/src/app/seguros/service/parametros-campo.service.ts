import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParametrosCampo } from '../models/parametros-campo';
//import * as app from '../../app.settings';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ParametrosCampoService {

  private URL_API: string = `${environment.URL_API}/parametros-campo`;

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.URL_API);
  };

  getOne(id:number){
    return this.http.get(`${this.URL_API}/${id}`);

  };

  getCamposConfigParam(){
    return this.http.get(`${this.URL_API}/campos-config-param`);
  };

  add(model: ParametrosCampo) {
    return this.http.post(this.URL_API, model);
  };

  update(model: ParametrosCampo) {
    return this.http.put(`${this.URL_API}/${model.id}`, model);
  };

  delete(id:number) {
    return this.http.delete(`${this.URL_API}/${id}`);
  };

  fechaActual(formato?:String){
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1; //January is 0!
    const yyyy = today.getFullYear();
    let fechaActual = `${yyyy}-${mm}-${dd}`;
    if(formato){
      if(formato === 'dd-mm-yyyy'){
        fechaActual = `${dd}-${mm}-${yyyy}`;
      }
    }
    return fechaActual;
  };

}
