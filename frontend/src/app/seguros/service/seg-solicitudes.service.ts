import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SegSolicitudes } from '../models/seg-solicitudes';

@Injectable({
  providedIn: 'root'
})
export class SegSolicitudesService {

  private URL_API: string = `${environment.URL_API}/seg-solicitudes`;

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.URL_API);
  }

  getAllUsr(nombre_usuario:string){
    return this.http.get(`${this.URL_API}/usuario/${nombre_usuario}`);
  }

  getOne(id:number){
    return this.http.get(`${this.URL_API}/${id}`);
  }

  getNroSolicitud(nro:number){
    return this.http.get(`${this.URL_API}/solicitud/${nro}`);
  }

  getBuscar(param:{take:number,skip:number,campo:string,valor:string}){
    return this.http.post(`${this.URL_API}/buscar`,param);
  }

  getSolicitud(){
    return this.http.get(`${this.URL_API}/solicitud/1`);
  }
  
  getUpdatePoliza(idsol:number){
    return this.http.get(`${this.URL_API}/update-poliza/${idsol}`);
  }

  getSolicitudesPag(model : { take:number,skip: number,user?:string}){
    return this.http.post(`${this.URL_API}/pag`, model);
  }

  add(model: SegSolicitudes) {
    return this.http.post(this.URL_API, model);
  }

  update(model: SegSolicitudes) {
    return this.http.put(`${this.URL_API}/${model.Sol_IdSol}`, model);
  }

  delete(id:number) {
    return this.http.delete(`${this.URL_API}/${id}`);
  }

  addDesgravamen(nro_solicitud:string,nombre_usuario:string){
    return this.http.post(`${this.URL_API}/add-desgravamen`,{nro_solicitud,nombre_usuario});
  }

  desistimientoSolicitud(model){
    return this.http.post(`${this.URL_API}/desistimiento-solicitud`,model);
  }

  getSolicitudIdSolAll(idsol:number){
    return this.http.get(`${this.URL_API}/idsol-all/${idsol}`);
  }

  updateServiciosData(model:{idsol:number} ){
    return this.http.put(`${this.URL_API}/update-servicios-data/${model.idsol}`,model);
  }

  //soporte-solicitud
  updateSolicitudDeudores(request){
    return this.http.put(`${this.URL_API}/soporte-solicitud/${request.data.seg_solicitudes.Sol_IdSol}`,request);
  }

  sendEmailSoporte(sendObj:any){
    return this.http.post(`${this.URL_API}/send-email-soporte`,sendObj);
  }

}
