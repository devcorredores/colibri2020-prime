import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SegAprobaciones } from '../models/seg-aprobaciones';

@Injectable({
  providedIn: 'root'
})
export class SegAprobacionesService {

  private URL_API: string = `${environment.URL_API}/seg-aprobaciones`;

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.URL_API);
  }

  getIdSol(idsol:number){
    return this.http.get(`${this.URL_API}/${idsol}`);
  }

  add(model: SegAprobaciones) {
    return this.http.post(this.URL_API, model);
  }

  update(model: SegAprobaciones) {
    return this.http.put(`${this.URL_API}/${model.Apr_IdSol}/${model.Apr_IdDeu}`, model);
  }

}
