import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigParamService {

  private URL_API: string = `${environment.URL_API}/config-param`;

  constructor(private http: HttpClient) {}

  getConfigParam() {
    return this.http.get(this.URL_API);
  };

  getConfigParamKey(key:string){
    return this.http.get(`${this.URL_API}/${key}`);
  };

  getConfigParamKeys(keys:string[]){
    return this.http.post(`${this.URL_API}/keys`,{keys});
  };

  update(model:{contenido:string}) {
    return this.http.post(`${this.URL_API}`, model);
  };


}