import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SegAdicionales} from '../models/seg-adicionales';

@Injectable({
  providedIn: 'root'
})
export class SegAdicionalesService {

  private URL_API: string = `${environment.URL_API}/seg-adicionales`;

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.URL_API);
  }

  updateAll(model: SegAdicionales[]) {
    return this.http.put(`${this.URL_API}`, model);
  }

  updateAdiconales(model: SegAdicionales[]){
    return this.http.put(`${this.URL_API}/seg-adicionales/1`,{seg_adicionales : model});
  }

}
