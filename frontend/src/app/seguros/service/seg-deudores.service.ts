import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SegDeudores } from '../models/seg-deudores';
import { SegSolicitudes } from '../models/seg-solicitudes';

@Injectable({
  providedIn: 'root'
})
export class SegDeudoresService {

  private URL_API: string = `${environment.URL_API}/seg-deudores`;

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.URL_API);
  }


  getIdSol(idSol:number){
    return this.http.get(`${this.URL_API}/idsol/${idSol}`);
  }

  updateDeudores(models: SegDeudores[]) {
    return this.http.put(`${this.URL_API}/update-deudores/1`, models);
  }

  updateDeudoresSolicitud(model:{seg_solicitud:SegSolicitudes,seg_deudores:SegDeudores[]}){
    return this.http.put(`${this.URL_API}/update-deudores-solicitud/1`, model);
  }


}
