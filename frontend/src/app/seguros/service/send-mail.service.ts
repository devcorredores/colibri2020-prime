import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MessageEmail } from '../models/message-email';

@Injectable({
  providedIn: 'root'
})
export class SendMailService {

  private URL_API: string = `${environment.URL_API}/send-mail`;

  constructor(private http: HttpClient) {}
  

  sendMessage(model : MessageEmail){
    return this.http.post(this.URL_API, model);
  }

}
