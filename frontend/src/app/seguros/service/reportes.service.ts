import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import * as app from '../../app.settings';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportesService {

  private URL_API: string = `${environment.URL_API}/reportes`;
  private URL_API_MASIVOS:string = `${environment.URL_API_MASIVOS}/reporte_query`;

  constructor(private http: HttpClient) {}

  generarNroSolicitud(param:{ id_solicitud:number, plantilla:string , id_deudor:number}) {
    return this.http.post(this.URL_API,param);
  }

  //generarReporteV2(param:{id_deudor:number}){
  //  return this.http.post(`${environment.URL_PUBLIC_PDF_MASIVOS}/reporte_query/executeQueryCrediticios/`,param);
  //}

  generarReporteV2(param:{id_deudor:number}){

    console.log(`REPORTE RUTA : ${this.URL_API_MASIVOS}/executeQueryCrediticios/`);

    return this.http.post(`${this.URL_API_MASIVOS}/executeQueryCrediticios/`,param, {withCredentials:true});
  }


}
