import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SegObservaciones} from '../models/seg-observaciones';

@Injectable({
  providedIn: 'root'
})
export class SegObservacionesService {

  private URL_API: string = `${environment.URL_API}/seg-observaciones`;

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.URL_API);
  }

  getId(id:number){
    return this.http.get(`${this.URL_API}/${id}`);
  }

  getIdSol(id:number){
    return this.http.get(`${this.URL_API}/IdSol/${id}`);
  }

  add(model: SegObservaciones) {
    return this.http.post(this.URL_API, model);
  }

  update(model: SegObservaciones) {
    return this.http.put(`${this.URL_API}/${model.Obs_Id}`, model);
  }

  delete(id:number) {
    return this.http.delete(`${this.URL_API}/${id}`);
  }

}
