import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SegurosRoutingModule } from './seguros-routing.module';
import { SeguroListComponent } from './seguro-list/seguro-list.component';
import { SeguroFormComponent } from './seguro-form/seguro-form.component';

import {SharedModule} from '../shared/shared.module';
import { BuscaSolicitudComponent } from './busca-solicitud/busca-solicitud.component';
import { SoporteSolicitudComponent } from './soporte-solicitud/soporte-solicitud.component';


@NgModule({
  declarations: [
    SeguroListComponent,
    SeguroFormComponent,
    BuscaSolicitudComponent,
    SoporteSolicitudComponent
  ],
  imports: [
    CommonModule,
    SegurosRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class SegurosModule { }
