import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Router, ActivatedRoute } from '@angular/router';

import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

import { SegSolicitudesService } from '../service/seg-solicitudes.service';
import { SegSolicitudes } from '../models/seg-solicitudes';
import { AuthService } from '../../service/auth.service';
import { ConfigParamService } from '../service/config-param.service';

import { UserInfo } from '../../models/UserInfo';


@Component({
  selector: 'app-busca-solicitud',
  templateUrl: './busca-solicitud.component.html',
  styleUrls: ['./busca-solicitud.component.css']
})
export class BuscaSolicitudComponent implements OnInit {

  numero_solicitud: number;
  solicitudes: SegSolicitudes[] = [];
  operacion:DatosOperacion = {};
  deudor:DatosDeudor = {};

  verOperacion = 'none';
  url_servicios_ext : string = '';

  estadosSolicitud:EstadosSolicitud[]=[];

  userinfo : UserInfo;

  constructor(
    public authService:AuthService,
    private segSolicitudesService:SegSolicitudesService,
    private configParamService:ConfigParamService,
    private router : Router,
    private http: HttpClient,
    private msg: ToastrService,
    ) { }

  ngOnInit(): void {

    try {
      // get rol del usuario
      //this.userinfo =  this.authService.getUserInfo() as UserInfo;
      this.userinfo = this.authService.getItemSync('userInfo');

      console.log(`USERINFO-FORM => `,this.userinfo);
    }
    catch (error) { 
      this.msg.warning('No se pudo recuperar la session del usuario','SOLICITUD')
    }

    this.cargarServiciosExt();
    this.cargarEstadosSolicitud();
  }

  // obtenemos la url del servicio externo
  cargarServiciosExt(){
    this.configParamService.getConfigParamKey("servicios_endpoint").subscribe(resp=>{
      const response = resp as {status: string,message: string,data:any};
      const endpoints = response.data;
      this.url_servicios_ext = endpoints.find(row=>row.nombre=='servicio_soap_ecofuturo').endpoint;
    });
  }

  cargarEstadosSolicitud(){

    // estado_solicitud
    this.configParamService.getConfigParamKey('estado_solicitud').subscribe(resp=>{
      const response = resp as {status:string,message:string,data:EstadosSolicitud[]};
      if(response.status == 'OK'){
        this.estadosSolicitud = response.data;
      }
    });

  }

  onBuscar(){

    if (this.numero_solicitud && this.numero_solicitud.toString().length < 10){
      Swal.fire({
        allowOutsideClick: false,
        text: 'Recuperando datos ...'
      });
      Swal.showLoading();
      // busqueda de numero de solicitud local
      this.segSolicitudesService.getNroSolicitud(this.numero_solicitud).subscribe(resp=>{
        Swal.close();
        let response = resp as {status: string,message: string,data:any};
        this.solicitudes = [];
        this.verOperacion = 'none';
        if(response.status =="OK"){
          if(response.data.solicitud && response.data.solicitud.length > 0){
            this.solicitudes = response.data.solicitud;
          }else{
            if(response.data.operacion.Operacion_Solicitud){
              this.operacion = response.data.operacion;
              this.deudor = response.data.deudor;
              this.solicitudes = [];
              this.verOperacion = 'block';
            }else{
              this.msg.info('Nro.Solicitud no existe','Busqueda de Solicitud');
            }
          }
        }else{
          this.msg.info(response.message);
        }
      },ex=>{
        Swal.close();
        this.msg.error('No se pudo establecer comunicación','SOLICITUDES');
      })
    }
    else{
      this.msg.error('Numero de Solicitud no valido','SOLICITUDES');
    }
  }

  onBuscarEcofuturo(numsol:string){

    if(numsol){
      Swal.fire({
        allowOutsideClick: false,
        text: 'Recuperando datos ...'
      });
      Swal.showLoading();
      let response : {status: string,message: string,data:any};
      const model = {
        request_body:{solicitud:numsol},
        operation:"getSolicitudPrimeraEtapa"
      };
      this.http.post(this.url_servicios_ext,model).subscribe(resp=>{
        Swal.close();
        response = resp as {status: string,message: string,data:any};
        this.solicitudes = [];
        if(response.status == 'OK'){
          if(response.data.getSolicitudPrimeraEtapaResult){
            this.operacion = response.data.getSolicitudPrimeraEtapaResult;
            this.solicitudes = [];
            this.verOperacion = 'block';
          }
        }
      },ex=>{
        Swal.close();
        this.msg.error('No se pudo establecer comunicación','SOLICITUDES');
      });

    }

  };

  onCrearSuscripcion(numsol:string){
    Swal.fire({
      title: `Instrumentar Seguro y Continuar?`,
      showCancelButton: true,
      confirmButtonText: `Aceptar`,
      cancelButtonText: `Cancelar`,
      confirmButtonColor: '#16A085',
      cancelButtonColor: '#F1948A',
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowOutsideClick: false,
          text: 'Verificando y creando ...'
        });
        Swal.showLoading();
        //this.datosFolderSeguroService.addDesgravamen(numsol,this.authService.usuario.nombre_usuario)
        this.segSolicitudesService.addDesgravamen(numsol,this.authService.usuario.usuario_login).subscribe(resp=>{
          Swal.close();
          const response = resp as {status:string,messages:[],data:any,message:string};
          console.log('PROCESO DE NUEVA SOLICITUD:',response.messages);
          console.log('Respuesta addDesgravamen ',resp);
          if(response.status == 'OK'){
            const id = response.data.Sol_IdSol
            this.router.navigate(['/AppMain/pnetseg/seguros/seguro-form',{ id }]);
            //this.router.navigate(['/seguros/seguro-form',{ id }]);
          }else{
            this.msg.error(response.message.toString(),'Instrumentar Seguro')
          }
        });
      }
    });
  };

  onGestionarSeguro(id:number){
    this.router.navigate(['/AppMain/pnetseg/seguros/seguro-form',{ id }]);
    //  this.router.navigate(['/seguros/seguro-form',{ id }]);
  };

  onCancelar(){
    this.solicitudes = [];
    this.numero_solicitud;
    this.verOperacion = 'none';
  }



  getEstadoSolicitud(estado){
    return this.estadosSolicitud.find(row=>row.codigo == estado).estado;
  }

}

interface DatosOperacion{
  Operacion_Solicitud?:string;
  Operacion_Fecha_Solicitud?:string;
  Operacion_Oficial_Creditos?:string;
  Operacion_Estado?:string;
  Operacion_Usuario_Oficial?:string;
  Operacion_Tipo_Solicitud?:string;
}

interface DatosDeudor{
  Deudor_Nombre?:string;
  Deudor_Paterno?:string;
  Deudor_Materno?:string;
  Deudor_Numero_Documento?:string;
}

interface EstadosSolicitud{
  estado?:string;
  codigo?:string;
}
