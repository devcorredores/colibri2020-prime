export class SegAprobaciones {
    Apr_IdSol : number;
    Apr_IdDeu : number;
    Apr_Aprobado : string;
    Apr_ExtraPrima : number;
    Apr_Condiciones : string;
}