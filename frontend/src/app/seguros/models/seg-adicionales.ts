export class SegAdicionales{
  Adic_Id:number;
  Adic_IdSol: number;
  Adic_IdDeu: number;
  Adic_Pregunta: string;
  Adic_Texto: string;
  Adic_Opciones: string;
  Adic_Respuesta: string;
  Adic_Comment: string;
  valor_si:boolean;
  valor_no:boolean;
  valida_por:boolean;
}
