export class SegBeneficiarios{
  Ben_Id:number;
  Ben_IdSol: number;
  Ben_IdDeu: number;
  Ben_Nombre: string;
  Ben_Paterno: string;
  Ben_Materno: string;
  Ben_Casada: string;
  Ben_Telefono: string;
  Ben_TipoDoc: string;
  Ben_NumDoc: string;
  Ben_ExtDoc: string;
  Ben_CompDoc: string;
  Ben_Relacion: string;
  Ben_Porcentaje: string;
  Ben_Sexo: string;
  Ben_OtraRel: string;
  Ben_Tipo : string;
  tipo: string;
  deudor: string;
}
