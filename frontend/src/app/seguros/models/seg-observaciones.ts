export class SegObservaciones{
    Obs_Id:number;
    Obs_IdSol:number;
    Obs_Observacion:string;
    Obs_Usuario:string;
    Obs_Eeff:string;
    Obs_Aseg:string;
    Obs_FechaReg:string;
    Obs_HoraReg:string;
    Obs_Cerrado:string;
}