export class SegOperaciones{
    Operacion_Id: number;
    Operacion_IdSol: number;
    Operacion_Numero_Aprobado: string;
    Operacion_Fecha_Aprobacion: string;
    Operacion_Monto_Aprobado: string;
    Operacion_Moneda_Aprobada: string;
    Operacion_Plazo_Aprobado: string;
    Operacion_Frecuencia_Aprobada:string;
    Operacion_Actual_Acumulado:string;
    Operacion_Tipo_Garantia:string;
    Operacion_Tipo_Grantia_Codigo:string;
    Operacion_Valor_Garantia:string;
    Operacion_Destino_Prestamo:string;
    Operacion_Destino_Codigo:string;
    Operacion_Fecha_Reprogramacion:string;
    Operacion_Fecha_Vencimiento:string;
    Operacion_Fecha_Desembolso:string;
    Operacion_Monto_Desembolsado:string;
  }