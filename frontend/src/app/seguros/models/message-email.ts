export class MessageEmail{
    to : string;
    cc : string;
    subject : string;
    html : string;
}