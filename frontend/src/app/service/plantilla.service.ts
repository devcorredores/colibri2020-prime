import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as app from '../app.settings';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlantillaService {

  //private URL_API: string = `${app.settings.URL_API}/templates`;
  private URL_API: string = `${environment.URL_API}/templates`;

  constructor(private http: HttpClient) {}

  getPlantillas() {
    return this.http.get(this.URL_API);
  }

  getPlantilla(plantilla:string){
    return this.http.get(`${this.URL_API}/${plantilla}`);
  }

  add(plantilla: string) {
    return this.http.post(this.URL_API,{plantilla});
  }

  update(model:{plantilla:string,contenido:string}) {
    return this.http.put(`${this.URL_API}/${model.plantilla}`, model);
  }

  delete(id:number) {
    return this.http.delete(`${this.URL_API}/${id}`);
  }

  generarPdf(param:{ id_folder:number, plantilla:string , id_deudor:number, id_codeudor:number }){
    return this.http.post(`${this.URL_API}/generar-pdf`,param);
  }


}
