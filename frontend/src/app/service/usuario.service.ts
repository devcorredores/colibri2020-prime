import { Injectable } from '@angular/core';
import { Usuario } from '../models/Usuario';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private URL_API: string = `${environment.URL_API}/usuarios`;

  constructor(private http: HttpClient) {}

  obtenerRegistros() {
    return this.http.get(this.URL_API);
  }

  obtenerRegistro(id:number){
    return this.http.get(`${this.URL_API}/${id}`);
  }

  nuevoRegistro(usuario: Usuario) {
    return this.http.post(this.URL_API, usuario);
  }

  modificarRegistro(usuario: Usuario) {
    return this.http.put(`${this.URL_API}/${usuario.id}`, usuario);
  }

  eliminarRegistro(id:number) {
    return this.http.delete(`${this.URL_API}/${id}`);
  }

  login(nombre_usuario:string,password:string) {
    return this.http.post(`${this.URL_API}/login`, {nombre_usuario,password});
  }

  existeUsuario(usuario:string){
    return this.http.get(`${this.URL_API}/existe-usuario/${usuario}`);
  }

  changePwd(id:number,password){
    return this.http.put(`${this.URL_API}/change/${id}`,{password});
  }

}
