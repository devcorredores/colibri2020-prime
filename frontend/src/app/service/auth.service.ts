import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../models/Usuario';

import * as CryptoJS from 'crypto-js';

import { UserInfo } from '../models/UserInfo';

const KEY_ENCRYPT_SECRET = 'pr4bvrw3ghdpbpsxuhnxrhyc4u5q3pjx';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  isLogin: boolean = false;
  grupo: string;
  socket;
  usuario: Usuario = new Usuario();

  private encryptSecretKey = 'pr4bvrw3ghdpbpsxuhnxrhyc4u5q3pjx';
  

  constructor(private router: Router) {}

  setupSocketConnection() {
    //environment.SOCKET_ENDPOINT
  };

  // userInfo
  getUserInfo(){
    try {
      const value = sessionStorage.getItem('userInfo');
      if(value){
        const valueDes = this.decryptData(value);
        console.log(':) getUerInfo:',valueDes);
        return valueDes;
      }else{
        return null;
      }
    } catch (error) {
      return null;
    }
  };

  setItemSync(key: string, value: any, encrypted: boolean = true) {
    if (encrypted) {
      value = this.encryptData(value);
    }
    localStorage.setItem(key, value);
    sessionStorage.setItem(key, value);

  }

  getItemSync(key: string, encrypted: boolean = true) {
    let localValue = localStorage.getItem(key);
    let sessionValue = sessionStorage.getItem(key);
    if (!localValue && !sessionValue) {
      return null;
    }
    if (localValue) {
      if (encrypted) {
        localValue = this.decryptData(localValue);
      }
      return localValue ? typeof localValue == 'object' ? localValue : typeof localValue == 'string' ? JSON.parse(localValue) : localValue : localValue;
    } else {
      return localValue;
    }
  };

  // userInfo
  setUserInfo(value: UserInfo) {
    const valueEnc = this.encryptData(value);
    sessionStorage.setItem('userInfo', valueEnc);
    console.log('set sessionStore');
  };

  removeUserInfo(){
    sessionStorage.removeItem('userInfo');
  };

  private encryptData(data) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(data), KEY_ENCRYPT_SECRET).toString();
    } catch (e) {
      // console.log(e);
    }
  };

  private decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, KEY_ENCRYPT_SECRET);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      // console.log(e);
    }
  };


}
