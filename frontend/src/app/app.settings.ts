export const settings = {
  URL_API: 'http://localhost:3003/api-seguros',
  URL_PUBLIC_PDF: 'http://localhost:3003/pdfs',
  URL_PUBLIC_VIEWER: 'http://localhost:3003/viewer',
  URL_API_: 'https://colibriseguros.xyz/api-seguros',
  URL_PUBLIC_PDF_: 'https://colibriseguros.xyz/pdfs',
  URL_PUBLIC_VIEWER_: 'https://colibriseguros.xyz/viewer',
  URL_API__: 'http://40.118.239.31:3003/api-seguros',
  URL_PUBLIC_PDF__: 'http://40.118.239.31:3003/pdfs',
  URL_PUBLIC_VIEWER__: 'http://40.118.239.31:3003/viewer',
  VERSION_APP: '10.09.16.03.22',

  httpTimeout: 10000,
  tomador: 'IDEPRO S.A.',
  estadosSolicitud: [
    { codigo: '400', estado: 'ANULADO', rol: 'OFCREDITO', secuencia: 1 },
    { codigo: '500', estado: 'DESISTIDO', rol: 'OFCREDITO', secuencia: 1 },
    {
      codigo: '201',
      estado: 'OBSERVADO-SUPERVISOR',
      rol: 'SUPERVISOR',
      secuencia: 2,
    },
    {
      codigo: '202',
      estado: 'APROBADO-SUPERVISOR',
      rol: 'SUPERVISOR',
      secuencia: 2,
    },
    {
      codigo: '301',
      estado: 'OBSERVADO-ASEGURADOR',
      rol: 'ASEGURADOR',
      secuencia: 2,
    },
    {
      codigo: '302',
      estado: 'APROBADO-ASEGURADOR',
      rol: 'ASEGURADOR',
      secuencia: 2,
    },
    { codigo: '100', estado: 'INICIADO', rol: 'TODOS', secuencia: 2 },
    {
      codigo: '200',
      estado: 'REVISION-SUPERVISOR',
      rol: 'TODOS',
      secuencia: 2,
    },
    {
      codigo: '300',
      estado: 'REVISION-ASEGURADOR',
      rol: 'TODOS',
      secuencia: 2,
    },
  ],
  parametros: [
    { nombre: 'C - NORMAL', valor: 'NORMAL' },
    { nombre: 'C - REFINANCIAMIENTO', valor: 'NORMAL' },
    { nombre: 'C - PARALELO', valor: 'NORMAL' },
    { nombre: 'C - SUBROGACION', valor: 'NORMAL' },
    { nombre: 'C - REPROGRAMACION', valor: 'NORMAL' },
    { nombre: 'L - SOLIC. BAJO LINEA SFI', valor: 'LC' },
    { nombre: 'L - SOLIC. BAJO LINEA NUEVA', valor: 'LC' },
    { nombre: 'L - SOLIC. BAJO AMPLIACION DE LINEA', valor: 'LC' },
    { nombre: 'HI1', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HI2', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HI3', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HI4', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HI5', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HO1', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HO2', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HO3', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HO4', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HO5', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HR1', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HR2', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HR3', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HR4', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HR5', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HT1', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HT2', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HT3', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HT4', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HT5', valor: 'INMUEBLE', codigo: 3 },
    { nombre: 'HV1', valor: 'VEHICULOS', codigo: 5 },
    { nombre: 'HV2', valor: 'VEHICULOS', codigo: 5 },
    { nombre: 'HV3', valor: 'VEHICULOS', codigo: 5 },
    { nombre: 'HV4', valor: 'VEHICULOS', codigo: 5 },
    { nombre: 'HV5', valor: 'VEHICULOS', codigo: 5 },
    { nombre: 'P03', valor: 'MAQUINARIA-EQUIPO', codigo: 4 },
    { nombre: 'P04', valor: 'MAQUINARIA-EQUIPO', codigo: 4 },
  ],
  tipo_credito: {
    10	:'CREDITO VIVIENDA PVS SUB 2',
    11	:'CREDITO VIVIENDA PVS SUB 3',
    12	:'CREDITO VIVIENDA PVS SUB 4',
    60	:'CREDITO VIVIENDA PVS SUB 2 REPRPG.BOL',
    61	:'CREDITO VIVIENDA PVS SUB 3 REPROG. BOL.',
    62	:'CREDITOVIVIENDA PVS SUB 4 REPROG.BOL.',
    15	:'FIDEICOMISO VITIVINICOLA',
    65	:'FIDEICOMISO VITIVI REPROGRA',
    17	:'FIDEICOMISO INFRAESTRUC PRODUCTIVA',
    67	:'FIDEICOMISO INFRAESTRU PRODUCTIV REPROGR',
    18	:'FIDEICOMISO GRANOS',
    68	:'FIDEICOMISO GRANOS REPROGRAMADO',
    19	:'FIDEICOMISO AVICOLA',
    69	:'FIDEICOMISO AVICOLA REPROGRAMADO',
    20	:'FIDEICOMISO DESARROLLO FDP',
    70	:'FIDEICOMISO DESARROLLO FDP REPROGRAMADO',
    21	:'FIDEICOMISO QUINUA ORGANICA',
    71	:'FIDEICOMISO QUINUA ORGANI REPROGRAMADO',
    23	:'FIDEICOMISO SEMILLA',
    73	:'FIDEICOMISO SEMILLA REPROGRAMADO',
    31	:'MICROLINEA DE CONVENIO',
    81	:'MICROLINEA DE CONVENIO REPROGRAMADO',
    32	:'MICROLINEA PLUS',
    34	:'MICROLINEA OPORTUNA',
    82	:'MICROLINEA PLUS REPROGRAMADO',
    84	:'MICROLINEA OPORTUNA REPROGRAMADO',
    33	:'MICROLINEA AZUL',
    83	:'MICROLINEA AZUL REPROGRAMADO',
    37	:'PROFIN',
    87	:'PROFIN REP.',
    38	:'FIDEICOMISO PROMYPE - PROFIN',
    88	:'FIDEICOMISO PROMYPE -PROFIN REPROGRAMADO',
    406	:'PROAGRO',
    407	:'PROTRANSFORMA',
    408	:'PROMICRO',
    415	:'PROTEMPORADA',
    416	:'PROCONSUMO',
    453	:'CONSTRUCCION REP.',
    456	:'PROAGRO REP.',
    457	:'PROTRANSFORMA REP.',
    458	:'PROMICRO REP.',
    465	:'PROTEMPORADA REP.',
    466	:'PROCONSUMO REP.',
    806	:'PROAGRO PF',
    807	:'PROTRANSFORMA P.F.',
    808	:'PROMICRO PF.',
    815	:'PROTEMPORADA PF.',
    856	:'PROAGRO REP.PF.',
    857	:'PROTRANSFORMA REP. P.F.',
    858	:'PROMICRO REP.PF.',
    865	:'PROTEMPORADA REP.PF.',
    419	:'PROVIVIENDA',
    469	:'PROVIVIENDA REP.',
    211	:'MICROLINEA OPORTUNA PROTRANSFORMA',
    212	:'MICROLINEA PROTRANSFORMA',
    213	:'PROCONSUMO HIPOTECARIO',
    214	:'PROVIVIENDA HIPOTECARIO',
    216	:'MICROLINEA PROMICRO OPORTUNA',
    217	:'MICROLINEA CONVENIO PROMICRO',
    261	:'MICROLINEA OPORTUNA PROTRANSFORMA REP.',
    262	:'MICROLINEA PROTRANSFORMA REP.',
    263	:'PROCONSUMO HIPOTECARIO REP.',
    264	:'PROVIVIENDA HIPOTECARIO REP.',
    266	:'MICROLINEA PROMICRO OPORTUNA REP.',
    267	:'MICROLINEA CONVENIO PROMICRO REP.',
    311	:'MICROLINEA OPORTUNA PROTRANSFORMA P.F.',
    312	:'MICROLINEA PROTRANSFORMA P.F.',
    313	:'PROCONSUMO HIPOTECARIO P.F.',
    314	:'PROVIVIENDA HIPOTECARIO P.F.',
    316	:'MICROLINEA PROMICRO OPORTUNA P.F.',
    317	:'MICROLINEA CONVENIO PROMICRO P.F.',
    361	:'MICROLINEA OPORTUNA PROTRANSFORMA REP. P',
    362	:'MICROLINEA PROTRANSFORMA REP. P.F.',
    363	:'PROCONSUMO HIPOTECARIO REP. P.F.',
    364	:'PROVIVIENDA HIPOTECARIO REP. P.F.',
    366	:'MICROLINEA PROMICRO OPORTUNA REP. P.F.',
    367	:'MICROLINEA CONVENIO PROMICRO REP. P.F.',
    420	:'SOLUCION PRODUCTIVO',
    421	:'SOLUCION PRODUCTIVO REP.',
  },
  cargos:{
    56:'SERVICIO IDEPRO LLAMADAS M/E',
    55:'SERVICIO IDEPRO LLAMADAS M/N',
    101:'SEGURO DESGRAVAMEN M/N',
    102:'SEGURO DESGRAVAMEN M/E',
    104:'IDEPROTECCION COVID M/N',
    105:'IDEPRO FAMILIA M/N',
    119:'SEG. DESGRAVAMEN DEUDOR M/N',
    120:'SEG. DESGRAVAMEN DEUDOR M/E',
    121:'SEG.DESGRAVAMEN C/CODEUDOR M/N',
    122:'SEG.DESGRAVAMEN C/CODEUDOR M/E',
  },
  monedas : {
    'BOLIVIANOS BS.':'BOLIVIANOS BS.',
    'DOLARES $US.':'DOLARES $US.'
  },
  cargosArray : [
    {id_cargo:56,cargo:'SERVICIO IDEPRO LLAMADAS M/E',monto_cargo_prestamo:0,moneda:'DOLARES AMERICANOS'},
    {id_cargo:55,cargo:'SERVICIO IDEPRO LLAMADAS M/N',monto_cargo_prestamo:0,moneda:'BOLIVIANOS'},
    {id_cargo:101,cargo:'SEGURO DESGRAVAMEN M/N',monto_cargo_prestamo:0,moneda:'BOLIVIANOS'},
    {id_cargo:102,cargo:'SEGURO DESGRAVAMEN M/E',monto_cargo_prestamo:0,moneda:'DOLARES AMERICANOS'},
    {id_cargo:104,cargo:'IDEPROTECCION COVID M/N',monto_cargo_prestamo:0,moneda:'BOLIVIANOS'},
    {id_cargo:105,cargo:'IDEPRO FAMILIA M/N',monto_cargo_prestamo:0,moneda:'BOLIVIANOS'},
    {id_cargo:119,cargo:'SEG. DESGRAVAMEN DEUDOR M/N',monto_cargo_prestamo:0,moneda:'BOLIVIANOS'},
    {id_cargo:120,cargo:'SEG. DESGRAVAMEN DEUDOR M/E',monto_cargo_prestamo:0,moneda:'DOLARES AMERICANOS'},
    {id_cargo:121,cargo:'SEG.DESGRAVAMEN C/CODEUDOR M/N',monto_cargo_prestamo:0,moneda:'BOLIVIANOS'},
    {id_cargo:122,cargo:'SEG.DESGRAVAMEN C/CODEUDOR M/E',monto_cargo_prestamo:0,moneda:'DOLARES AMERICANOS'}
  ],
  titulosGarantias : [
    {tipo:'INMUEBLE',titulo:'Depto/Muni.',campo:'dato_1'},
    {tipo:'INMUEBLE',titulo:'Zona/Calle',campo:'dato_2'},
    {tipo:'INMUEBLE',titulo:'Numero',campo:'dato_3'},
    {tipo:'INMUEBLE',titulo:'Numero de folio',campo:'dato_4'},
    {tipo:'INMUEBLE',titulo:'',campo:'dato_5'},
    {tipo:'INMUEBLE',titulo:'',campo:'dato_6'},
    {tipo:'INMUEBLE',titulo:'',campo:'dato_7'},
    {tipo:'INMUEBLE',titulo:'',campo:'dato_8'},
    {tipo:'INMUEBLE',titulo:'',campo:'id_tipo_vehiculo'},
    {tipo:'INMUEBLE',titulo:'',campo:'tipo_vehiculo'},
    {tipo:'VEHICULO',titulo:'Clase',campo:'dato_1'},
    {tipo:'VEHICULO',titulo:'Marca',campo:'dato_2'},
    {tipo:'VEHICULO',titulo:'Modelo',campo:'dato_3'},
    {tipo:'VEHICULO',titulo:'Año',campo:'dato_4'},
    {tipo:'VEHICULO',titulo:'Color',campo:'dato_5'},
    {tipo:'VEHICULO',titulo:'Placa',campo:'dato_6'},
    {tipo:'VEHICULO',titulo:'Motor',campo:'dato_7'},
    {tipo:'VEHICULO',titulo:'Chasis',campo:'dato_8'},
    {tipo:'VEHICULO',titulo:'Id vehiculo',campo:'id_tipo_vehiculo'},
    {tipo:'VEHICULO',titulo:'Tipo vehiculo',campo:'tipo_vehiculo'},
    {tipo:'MAQUINARIA',titulo:'Marca',campo:'dato_1'},
    {tipo:'MAQUINARIA',titulo:'Chasis',campo:'dato_2'},
    {tipo:'MAQUINARIA',titulo:'Modelo',campo:'dato_3'},
    {tipo:'MAQUINARIA',titulo:'Año',campo:'dato_4'},
    {tipo:'MAQUINARIA',titulo:'Color',campo:'dato_5'},
    {tipo:'MAQUINARIA',titulo:'',campo:'dato_6'},
    {tipo:'MAQUINARIA',titulo:'',campo:'dato_7'},
    {tipo:'MAQUINARIA',titulo:'',campo:'dato_8'},
    {tipo:'MAQUINARIA',titulo:'',campo:'id_tipo_vehiculo'},
    {tipo:'MAQUINARIA',titulo:'',campo:'tipo_vehiculo'},
  ],
  requisitos_seguro : [
    {
        edad_min:18,edad_max:49,
        monto_min:1.00,monto_max:12000.00,
        id_moneda:2,
        requisitos:"FREE COVER",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:18,edad_max:49,
        monto_min:12001.00,monto_max:50000.00,
        id_moneda:2,
        requisitos:"IMC",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:18,edad_max:49,
        monto_min:50001.00,monto_max:100000.00,
        id_moneda:2,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:18,edad_max:49,
        monto_min:100001.00,monto_max:200000.00,
        id_moneda:2,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:50,edad_max:59,
        monto_min:1.00,monto_max:12000.00,
        id_moneda:2,
        requisitos:"FREE COVER",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:50,edad_max:59,
        monto_min:12001.00,monto_max:50000.00,
        id_moneda:2,
        requisitos:"IMC",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:50,edad_max:59,
        monto_min:50001.00,monto_max:100000.00,
        id_moneda:2,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:50,edad_max:59,
        monto_min:100001.00,monto_max:200000.00,
        id_moneda:2,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"L+HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:60,edad_max:70,
        monto_min:1.00,monto_max:12000.00,
        id_moneda:2,
        requisitos:"FREE COVER",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:60,edad_max:70,
        monto_min:12001.00,monto_max:50000.00,
        id_moneda:2,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:60,edad_max:70,
        monto_min:50001.00,monto_max:100000.00,
        id_moneda:2,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:60,edad_max:70,
        monto_min:100001.00,monto_max:200000.00,
        id_moneda:2,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"L+HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:18,edad_max:49,
        monto_min:1.00,monto_max:84000.00,
        id_moneda:1,
        requisitos:"FREE COVER",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:18,edad_max:49,
        monto_min:84001.00,monto_max:350000.00,
        id_moneda:1,
        requisitos:"IMC",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:18,edad_max:49,
        monto_min:350001.00,monto_max:700000.00,
        id_moneda:1,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:18,edad_max:49,
        monto_min:700001.00,monto_max:1400000.00,
        id_moneda:1,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:50,edad_max:59,
        monto_min:1.00,monto_max:84000.00,
        id_moneda:1,
        requisitos:"FREE COVER",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:50,edad_max:59,
        monto_min:84001.00,monto_max:350000.00,
        id_moneda:1,
        requisitos:"IMC",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:50,edad_max:59,
        monto_min:350001.00,monto_max:700000.00,
        id_moneda:1,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:50,edad_max:59,
        monto_min:700001.00,monto_max:1400000.00,
        id_moneda:1,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"L+HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:60,edad_max:70,
        monto_min:1.00,monto_max:84000.00,
        id_moneda:1,
        requisitos:"FREE COVER",
        examen_medico:"",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:60,edad_max:70,
        monto_min:84001.00,monto_max:350000.00,
        id_moneda:1,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"",
        electro_cardiograma:"",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:60,edad_max:70,
        monto_min:350001.00,monto_max:700000.00,
        id_moneda:1,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
        edad_min:60,edad_max:70,
        monto_min:700001.00,monto_max:1400000.00,
        id_moneda:1,
        requisitos:"DJS",
        examen_medico:"EM+AO",
        laboratorio:"L+HIV",
        electro_cardiograma:"ECG",
        tipo_seguro:"NO LICITADA"
    },
    {
      edad_min:60,edad_max:70,
      monto_min:700001.00,monto_max:1400000.00,
      id_moneda:1,
      requisitos:"DJS",
      examen_medico:"EM+AO",
      laboratorio:"L+HIV",
      electro_cardiograma:"ECG",
      tipo_seguro:"NO LICITADA"
    },
    {
      edad_min:60,edad_max:70,
      monto_min:1400001.00,monto_max:9900000.00,
      id_moneda:1,
      requisitos:"DJS",
      examen_medico:"EM+AO",
      laboratorio:"L+HIV",
      electro_cardiograma:"ECG",
      tipo_seguro:"NO LICITADA"
    },
    {
      edad_min:18,edad_max:70,
      monto_min:1.00,monto_max:126000.00,
      id_moneda:1,
      requisitos:"FREE COVER",
      examen_medico:"",
      laboratorio:"",
      electro_cardiograma:"",
      tipo_seguro:"LICITADA"
    },
    {
      edad_min:18,edad_max:70,
      monto_min:126001.00,monto_max:350000.00,
      id_moneda:1,
      requisitos:"IMC",
      examen_medico:"EM+AO",
      laboratorio:"L+HIV",
      electro_cardiograma:"ECG",
      tipo_seguro:"LICITADA"
    },
    {
      edad_min:18,edad_max:70,
      monto_min:350001.00,monto_max:9900000.00,
      id_moneda:1,
      requisitos:"DJS",
      examen_medico:"EM+AO",
      laboratorio:"L+HIV",
      electro_cardiograma:"ECG",
      tipo_seguro:"LICITADA"
    },
    {
      edad_min:18,edad_max:70,
      monto_min:1.00,monto_max:18000.00,
      id_moneda:2,
      requisitos:"IMC",
      examen_medico:"EM+AO",
      laboratorio:"L+HIV",
      electro_cardiograma:"ECG",
      tipo_seguro:"LICITADA"
    },
    {
      edad_min:18,edad_max:70,
      monto_min:18001.00,monto_max:50000.00,
      id_moneda:2,
      requisitos:"IMC",
      examen_medico:"EM+AO",
      laboratorio:"L+HIV",
      electro_cardiograma:"ECG",
      tipo_seguro:"LICITADA"
    },
    {
      edad_min:18,edad_max:70,
      monto_min:50001.00,monto_max:900000.00,
      id_moneda:2,
      requisitos:"DJS",
      examen_medico:"EM+AO",
      laboratorio:"L+HIV",
      electro_cardiograma:"ECG",
      tipo_seguro:"LICITADA"
    }
  ]
};
