export class  Usuario {
  id:number;
  usuario_login: string;
  usuario_password: string;
  usuario_email: string;
  usuario_nombre_completo: string;
  rol:string;
}
