export interface UserInfo{
    id?: string;
    usuario_login?: string;
    usuario_password?: string;
    pwd_change?: boolean;
    usuario_email?: string;
    usuario_nombre_completo?: string;
    usuario_img?: any;
    par_estado_usuario_id?: string;
    id_persona?: string;
    par_local_id?: string;
    adicionado_por?: string;
    modificado_por?: any;
    createdAt?: Date;
    updatedAt?: Date;
    rol?: Rol[];
    perfil?: Perfil[];
};

export interface Rol {
    id?: string;
    codigo?: string;
    descripcion?: string;
}

export interface Perfil {
    id?: string;
    id_rol?: string;
    id_perfil?: string;
    adicionado_por?: string;
    modificado_por?: string;
    createdAt?: Date;
    updatedAt?: Date;
}
