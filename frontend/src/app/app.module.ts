// import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { ToastrModule } from 'ngx-toastr';


import { SanitizeHtmlPipe } from './pipe/sanitize-html.pipe';


//import { BnNgIdleService } from 'bn-ng-idle';

import {CommonModule} from "@angular/common";
import {ToastrModule} from "ngx-toastr";

@NgModule({
  declarations: [
    AppComponent,

    SanitizeHtmlPipe,

  ],
  imports: [
    // BrowserModule
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    // BrowserAnimationsModule,
    FormsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
