import { Pipe, PipeTransform } from '@angular/core';
import { ParametrosCampo} from '../models/parametros-campos';

@Pipe({
  name: 'nombreCampo'
})
export class NombreCampoPipe implements PipeTransform {

  transform(value: any, parametrosCampos: ParametrosCampo[]): any {
    const parametrosCampo = parametrosCampos.find(param=>param.campo === value);
    if(parametrosCampo){
      return parametrosCampo.titulo
    }else{
      return null;
    }
  }

}
