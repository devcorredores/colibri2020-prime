import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as app from '../../app.settings';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntidadFinService {

  private URL_API: string = `${environment.URL_API}/idepro`;

  constructor(private http: HttpClient) {}

  // se obtiene participante, solicitud y operacion
  getParticipanteSolicitudOperacion(id:number){
    return this.http.get(`${this.URL_API}/${id}`);
  }

  getParticipante(id:number){
    return this.http.get(`${this.URL_API}/participante/${id}`);
  }

  getSolicitud(id:number){
    return this.http.get(`${this.URL_API}/solicitud/${id}`);
  }

  getOperacion(id:number){
    return this.http.get(`${this.URL_API}/operacion/${id}`);
  }

  getPrestamos(id:string){
    return this.http.get(`${this.URL_API}/prestamos/${id}`);
  }

  getCliente(id:string){
    return this.http.get(`${this.URL_API}/cliente/${id}`);
  }

}
