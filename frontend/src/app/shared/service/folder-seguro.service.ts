import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FolderSeguro } from '../models/folder-seguro';
import * as app from '../../app.settings';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FolderSeguroService {

  private URL_API: string = `${environment.URL_API}/folder-seguro`;

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.URL_API);
  }

  getOne(id:number){
    return this.http.get(`${this.URL_API}/${id}`);
  }

  getIdPrestamo(id:number){
    return this.http.get(`${this.URL_API}/prestamo/${id}`);
  }

  add(model: FolderSeguro) {
    return this.http.post(this.URL_API, model);
  }

  update(model: FolderSeguro) {
    return this.http.put(`${this.URL_API}/${model.id}`, model);
  }

  delete(id:number) {
    return this.http.delete(`${this.URL_API}/${id}`);
  }

}
