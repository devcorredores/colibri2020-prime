import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as app from '../../app.settings';
import { DatosFolderSeguro } from '../models/datos-folder-seguro';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DatosFolderSeguroService {

  private URL_API: string = `${environment.URL_API}/datos-folder-seguro`;


  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.URL_API);
  }

  getId(id:number){
    return this.http.get(`${this.URL_API}/${id}`);
  }

  getIdPrestamo(id:number){
    return this.http.get(`${this.URL_API}/prestamo/${id}`);
  }

  add(idPrestamo:number) {
    return this.http.post(this.URL_API,{id_prestamo:idPrestamo});
  }

  update(model: DatosFolderSeguro) {
    return this.http.put(`${this.URL_API}/${model.folder_seguro.id}`, model);
  }

  delete(id:number) {
    return this.http.delete(`${this.URL_API}/${id}`);
  }

  addCodeudor(param:{numero_documento:string,id_folder:number,id_prestamo:number}){
    return this.http.post(`${this.URL_API}/add-codeudor`,param);
  }

  delCodeudor(param:{numero_documento:string,id_folder:number,id_prestamo:number}){
    return this.http.post(`${this.URL_API}/del-codeudor`,param);
  }

}
