import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuscarDeudorComponent } from './buscar-deudor/buscar-deudor.component';
import { FormsModule } from '@angular/forms';
import { NombreCampoPipe } from './pipe/nombre-campo.pipe';

@NgModule({
  declarations: [BuscarDeudorComponent,NombreCampoPipe],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [BuscarDeudorComponent]
})
export class SharedModule { }
