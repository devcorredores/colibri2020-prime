export interface DeclaracionSalud {
  id:number;
  id_folder: number;
  id_tipo_seguro: number;
  nro: number;
  pregunta: string;
  valor_si: boolean;
  valor_no: boolean;
  respuesta: string;
  validacion: string;
  id_deudor: number;
  id_codeudor: number;
  fecha_evaluacion: string;
  tipo_seguro: string;
  valor_respuesta: string;
  nombre_usuario: string;
}
