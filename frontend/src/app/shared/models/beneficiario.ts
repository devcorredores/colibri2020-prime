export class Beneficiario {
  id:number;
  id_folder: number;
  id_prestamo: number;
  nombre: string;
  apellido_paterno: string;
  apellido_materno: string;
  apellido_casada: string;
  telefono_contacto: string;
  numero_documento: string;
  tipo_documento: string;
  complemento_documento: string;
  extension_tipo_doc_identidad: string;
  parentesco: string;
  sexo: string;
  porcentaje_cobertura: string;
  nombre_usuario:string;
}
