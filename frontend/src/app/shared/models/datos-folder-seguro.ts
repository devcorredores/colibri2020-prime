import { FolderSeguro } from './folder-seguro';
import { SolicitudCredito } from './solicitud-credito';
import { Deudor } from './deudor';
import { Codeudor } from './codeudor';
import { Beneficiario } from './beneficiario';
import { OperacionCrediticia } from './operacion-crediticia';
import { FolderMensaje } from './folder-mensaje';
import { DeclaracionSalud } from './declaracion-salud';
import { CargosSeguro } from './cargos-seguro';

export interface DatosFolderSeguro {

  folder_seguro : FolderSeguro,
  solicitud_credito : SolicitudCredito,
  deudor : Deudor,
  codeudor : Codeudor[],
  beneficiario : Beneficiario[],
  operacion_crediticia : OperacionCrediticia,
  folder_mensaje : FolderMensaje[],
  declaracion_salud : DeclaracionSalud[]
  cargos_seguro : CargosSeguro[]

}
