export interface OperacionCrediticia {
  id:number;
  id_folder: number;
  id_prestamo: number;
  numero_operacion: string;
  monto_aprobado: number;
  moneda: string;
  tipo_garantia: string;
  destino_credito: string;
  monto_acumulado_despues_op: number;
  monto_desembolsado: number;
  numero_tc: string;
  estado_operacion: string;
  calificacion: string;
  rubro: string;
  origen: string;
  responsable_seguimiento: string;
  fecha_aprobacion: string;
  plazo: string;
  frecuencia_pago: string;
  valor_garantia: string;
  fecha_reprogramacion: string;
  fecha_vencimiento: string;
  fecha_desembolso: string;
  id_tc: string;
  tasa_seguro_instrumentado: string;
  importe_seguro_cobrado: string;
  saldo_capital_fecha: number;
  nombre_usuario: string;
  id_numero_operacion: number;
  id_moneda: number;
  dias_incumplidos: number
}
