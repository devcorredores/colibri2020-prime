export interface ParametrosCampo {
  id: number;
  grupo: string;
  campo: string;
  titulo: string;
  descripcion: string;
  requerido: boolean;
  visible: boolean;
  editable: boolean;
  tipo: string;
  validacion: string;
  permisos: string;
  valor: string;
  orden: number;
}
