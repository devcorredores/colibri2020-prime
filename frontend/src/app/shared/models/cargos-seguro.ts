export class CargosSeguro {
  id:number;
  id_folder: number;
  id_tipo_seguro: number;
  tipo_seguro: string;
  fecha: string;
  id_cargo: number;
  cargo: string;
  monto_cargo_prestamo: number;
  moneda_cargo: string;
  prima: number;
  moneda_prima: string;
  cobertura: string;
}
