export class FolderMensaje {
  id:number;
  id_folder: number;
  mensaje: string;
  nombre_usuario_origen: string;
  nombre_usuario_destino: string;
  fecha: string;
  permisos: string;
  estado_general: string;
}
