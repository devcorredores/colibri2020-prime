export class Cliente {
  nombre: string;
  sexo: string;
  pais: string;
  caedec: string;
  actividad: string;
  celular: string;
  id_cliente: number;
  apellido_paterno: string;
  apellido_materno: string;
  apellido_casada: string;
  direccion_domicilio: string;
  direccion_negocio: string;
  idEstado_civil: string;
  estado_civil: string;
  id_sexo: number;
  numero_documento: string;
  tipo_documento: string;
  complemento_documento: string;
  fecha_nacimiento: string;
  id_caedec: number;
  fecha_registro: string;
  telefono_1: string;
  telefono_2: string;
  prestamos_anteriores: [
    {
      estado: string;
      id_prestamo: number;
      id_estado: number;
    }
  ];
}
