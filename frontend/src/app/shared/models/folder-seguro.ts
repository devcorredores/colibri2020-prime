
export interface FolderSeguro {
  id: number;
  fecha: string;
  titular: string;
  numero_documento: string;
  estado_general: string;
  entidad_financiera: string;
  nombre_usuario: string;
  observaciones: string;
  id_prestamo: number;
}
