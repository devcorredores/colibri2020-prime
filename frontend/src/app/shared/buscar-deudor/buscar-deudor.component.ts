import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { Cliente } from '../models/cliente';
import { EntidadFinService} from '../service/entidad-fin.service';
import { ParametrosCampoService} from '../service/parametros-campo.service';
import { DatosFolderSeguroService } from '../service/datos-folder-seguro.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-buscar-deudor',
  templateUrl: './buscar-deudor.component.html',
  styleUrls: ['./buscar-deudor.component.css']
})
export class BuscarDeudorComponent implements OnInit {

  @Input() param: {accion:string,numero_documento:string,id_folder:number,id_prestamo:number};
  @Output() result = new EventEmitter<boolean>();

  parametrosCampo = [];
  data:any;

  clientes: Cliente[] = [];

  active:boolean=false;
  tab_active = 0;
  openModal = 'none';

  ops = [false,false,false,false,false];

  constructor(
    private ds : EntidadFinService,
    private msg: ToastrService,
    private parametrosCamposService: ParametrosCampoService,
    private datosFolderSeguroService : DatosFolderSeguroService
  ) { }

  ngOnInit(): void {
    this.getParametrosCampo();
  }

  getParametrosCampo(){
    this.parametrosCamposService.getAll().subscribe(resp=>{
      const response = resp as any;
      this.parametrosCampo = response.data;
    })
  }

  onBuscarCodeu(){
    if(this.param.numero_documento){
      Swal.fire({
        allowOutsideClick: false,
        text: 'Obteniendo datos del prestamo ...'
      });
      Swal.showLoading();
      //cliente
      this.ds.getCliente(this.param.numero_documento).subscribe(cli=>{
        Swal.close();
        const resp = cli as {status:string,messages:[],data:any};
        if(resp.status==='OK'){
          this.clientes = resp.data;
        }else{
          this.msg.error(resp.messages.toString(),'Consulta servicio')
        }
      });
    }
  }

  onContinuarCrear(){
    Swal.fire({
      title: 'Aplicar seguro',
      showCancelButton: true,
      confirmButtonText: `SI`,
      cancelButtonText: `NO`,
      confirmButtonColor: '#ff9933',
      cancelButtonColor: '#d33',
    }).then((result) => {
      if (result.isConfirmed) {
       //this.crearDatosFolderSeguros(documento);
       this.param.numero_documento = '';
       this.clientes = [];
      }
    });
  }

  onContinuarAdd(num_doc:string){
    Swal.fire({
      title: 'Incluir como codeudor',
      showCancelButton: true,
      confirmButtonText: `SI`,
      cancelButtonText: `NO`,
      confirmButtonColor: '#ff9933',
      cancelButtonColor: '#d33',
    }).then((result) => {
      if (result.isConfirmed) {
       const request = {numero_documento:num_doc,id_folder:this.param.id_folder,id_prestamo:0};
       this.datosFolderSeguroService.addCodeudor(request).subscribe(resp=>{
        const respose = resp as {status:string,messages:[],data:any};
        if(respose.status==='OK'){
          this.result.emit(true);
        }else{
          this.result.emit(false);
          this.msg.error(respose.messages.toString(),'Codeudores');
        }
       })
       this.param.numero_documento = '';
       this.param.id_folder = 0;
       this.param.accion = '';
       this.clientes = [];
      }
    });
  }

}
