export class persona_banco {
    cod_agenda?:string = '';
    doc_id:string = '';
    extension:string = '';
    apcasada:string = '';
    dia_fechanac:string = '';
    anio_fechanac:string = '';
    mes_fechanac:string = '';
    departamento:string = '';
    direccion:string = '';
    cod_sucursal:string = '';
    caedec:string = '';
    complemento:string = '';
    est_civil:string = '';
    e_mail:string = '';
    desc_caedec:string = '';
    tipo_doc:string = '';
    estado_civil:string = '';
    fono_domicilio:string = '';
    fono_oficina:string = '';
    localidad:string = '';
    materno:string = '';
    nombre:string = '';
    nro_celular:string = '';
    paterno:string = '';
    sexo:string = '';
    debito_automatico:string = '';
    fecha_nacimiento:Date = null;
    fecha_nacimiento_str:string = '';
}

export class persona_banco_solicitud {
    solicitudes_sci?:string[]= [];
    solicitud_sci_selected?:string= '';
    solicitud_tipo?:string= '';
    solicitud_sci_estado?:string= '';
    solicitud_prima_total?:string= '';
    solicitud_plazo_credito?:string= '';
    solicitud_moneda?:string= '';
    solicitud_forma_pago?:string= '';
}

export class solicitud_sci{
    solicitud_sci:string = '';
}

