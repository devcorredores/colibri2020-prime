import {Parametro} from "./parametro";
import {Entidad} from "./entidad";
import {Asegurado} from "./asegurado";

export class Beneficiario {
    id?:number = null;
    id_beneficiario: number = null;
    id_asegurado: number = null;
    porcentaje: string = '';
    parentesco: string = '';
    id_parentesco: number=0;
    persona_primer_apellido: string = '';
    persona_segundo_apellido: string = '';
    persona_primer_nombre: string = '';
    par_sexo_id: number=0;
    // adicionado_por:string = '';
    // modificado_por:string = '';

    descripcion_otro:string = '';
    estado:string;
    condicion:string;
    tipo:string;
    // estado:number = null;
    // tipo:number = null;
    // fecha_declaracion:Date = new Date();
    fecha_declaracion: string;
    // estado_obj:Parametro = new Parametro();
    // tipo_obj:Parametro = new Parametro();
    // parentesco_obj:Parametro = new Parametro();
    // entidad:Entidad = new Entidad();
    // asegurado:Asegurado = new Asegurado();
    persona_celular: string;
    persona_doc_id: string;
    persona_doc_id_ext: string;

    /**  STASHED JAC   *****
     id_beneficiario: number;
     id_asegurado: number;
     porcentaje: string;
     parentesco: string;
     descripcion_otro: string;
     persona_doc_id: string;
     persona_doc_id_ext: string;
     persona_primer_apellido: string;
     persona_segundo_apellido: string;
     persona_primer_nombre: string;
     persona_celular: string;
     tipo: string;
     estado: string;
     fecha_declaracion: string;
     */
}
