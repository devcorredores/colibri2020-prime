export class Persona_juridica {
    id:number= 0;
    codigo:string= '';
    descripcion:string= '';
    numid_fiscal:string= '';
    direccion:string= '';
    telefono:string= '';
    celular:string= '';
    adicionada_por:string= '';
    modificada_por:string= '';
    createdAt:string= '';
    updatedAt:string= '';
}
