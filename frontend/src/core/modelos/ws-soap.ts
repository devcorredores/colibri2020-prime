export class WsSoap {

    wsdl: string;
    soapHeader: { Username: string, Password: string };
    operation: string;
    request: any;
    name: string;

}
