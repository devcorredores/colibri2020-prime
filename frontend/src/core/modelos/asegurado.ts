import {Instancia_poliza} from "./instancia_poliza";
import {Entidad} from "./entidad";
import {Atributo_instancia_poliza} from "./atributo_instancia_poliza";
import {Beneficiario} from "./beneficiario";
import {Usuario} from "./usuario";

export class Asegurado {
    id: number = 0;
    id_entidad:number = 0;
    id_instancia_poliza:number = 0;
    adicionado_por:string = '';
    modificado_por:string = '';
    createdAt:string = '';
    updatedAt:string = '';
    entidad:Entidad = new Entidad();
    usuario:Usuario = new Usuario();
    beneficiarios:Beneficiario[] = [];
    instancia_poliza:Instancia_poliza = new Instancia_poliza();
    atributo_instancia_polizas:Atributo_instancia_poliza[] = [];
}
