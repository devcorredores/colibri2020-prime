import {Objeto_x_atributo} from "./objeto_x_atributo";
import {ValidationErrors} from "@angular/forms";


export class Atributo_instancia_poliza {
    id: number = null;
    id_instancia_poliza:number = 0;
    id_objeto_x_atributo:number = 0;
    valor:string = '';
    tipo_error:string = '';
    requerido:boolean = false;
    adicionado_por:string = '';
    modificado_por:string = '';
    createdAt:string = '';
    updatedAt:string = '';
    objeto_x_atributo:Objeto_x_atributo = new Objeto_x_atributo();
}
