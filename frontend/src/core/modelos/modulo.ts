export class Modulo {
    id?:string;
    id_modulo_super?:number;
    codigo?:string;
    descripcion?:string;
    obreviacion?:string;
    icon?:string;
    adicionado_por?:string;
    modificado_por?:string;
}