export class Upload {
    name:string;
    newName:string;
    source:string;
    path:string = '';
    pathName:string;
    file:string;
    ext:string;
    size:string;
    type:string;
    tipoDocumento:string;
    idTipoDocumento:number;
    lastModifiedDate:string;
    id_instancia_poliza:number;
}
