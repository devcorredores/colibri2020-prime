import { Diccionario } from './diccionario';
import { SelectItem } from 'primeng/primeng';
export class Atributo {
    id:number = 0;
    id_tipo:number = 0;
    codigo:string = '';
    descripcion:string = '';
    adicionado_por:string = '';
    modificado_por:string = '';
    id_diccionario:number = 0;
    diccionarios:SelectItem[] = [];
    diccionario:Diccionario=new Diccionario();
    valor:string = '';
    id_padre:number = 0;
    atributoPadre:Atributo = null;
    atributoHijos:Atributo[] = []
}


