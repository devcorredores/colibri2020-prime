export class Perfil {
    id:number;
    codigo:string;
    descripcion:string;
    abreviacion:string;
    adicionado_por:string;
    modificado_por:string;
}
