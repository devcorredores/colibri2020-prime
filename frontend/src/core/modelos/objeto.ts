import { Objeto_x_atributo } from './objeto_x_atributo';
export class Objeto {
    id: number = 0;
    id_poliza:number = 0;
    descripcion:string = '';
    adicionado_por:string = '';
    modificado_por:string = '';
    objeto_x_atributos:Objeto_x_atributo[]=[];
}
