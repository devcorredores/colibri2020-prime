import {Perfil_x_Componente} from "./componente";

export class Parametro {
    id: number = 0;
    parametro_cod: string = '';
    parametro_descripcion: string = '';
    parametro_abreviacion: string = '';
    diccionario_id: number = 0;
    createdAt:Date;
    updatedAt:Date;
    adicionado_por:string;
    modificado_por:string;
    orden:number;
    id_padre:number;
}

export class EstadosDesgravamen {
    Est_Codigo: string = '';
    Est_Descripcion: string = '';
    Est_Cerrado: string = '';
    Est_Grupo: string = '';
    Est_Visible: string = '';
    Est_Orden: number = 0;
}

export class PolizasDesgravamen {
    Deu_Poliza: string = '';
}

export class ParametroRuteo {
    id:number;
    ruta:string = '';
    openForm:boolean = true;
    id_vista:number;
    parametro_vista:string;
    parametro_ruteo:any;
    ComponentesInvisibles: Perfil_x_Componente[] = [];
}