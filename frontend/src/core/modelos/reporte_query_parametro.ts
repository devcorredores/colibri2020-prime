import { SelectItem } from 'primeng/primeng';
import { Diccionario } from './diccionario';
import { Dato_complementario } from './dato_complementario';
export class Reporte_query_parametro{
    id:number;
    nombre: string='';
    descripcion:string='';
    tipo:string='';
    id_reporte_query:string = '';
    id_diccionario:number=null;
    valor:string='';
    diccionario:Diccionario;
    diccionarios:SelectItem[]=[];
}