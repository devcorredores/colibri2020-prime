export class Dato_anexo_asegurado {
    id:number = 0;
    id_tipo:number = 0;
    codigo:string = '';
    descripcion:string = '';
    visible:boolean =false;
    obligatorio:boolean=false;
    adicionado_por:string = '';
    modificado_por:string = '';
}