import {Instancia_poliza} from "./instancia_poliza";
import {Documento} from "./documento";
import {Atributo_instancia_documento} from "./atributo_instancia_documento";

export class Instancia_documento {
    id:number = null;
    id_instancia_poliza:number = 0;
    id_documento:number = 0;
    nro_documento:string = '';
    fecha_emision:Date;
    fecha_inicio_vigencia:Date;
    fecha_fin_vigencia:Date;
    asegurado_doc_id:string = '';
    asegurado_nombre1:string = '';
    asegurado_nombre2:string = '';
    asegurado_apellido1:string = '';
    asegurado_apellido2:string = '';
    asegurado_apellido3:string = '';
    tipo_persona:string = '';
    nombre_archivo:string = '';
    adicionada_por:string = '';
    modificada_por:string = '';
    createdAt:string = '';
    updatedAt:string = '';
    instancia_poliza:Instancia_poliza = new Instancia_poliza();
    atributo_instancia_documentos:Atributo_instancia_documento[] = [];
    atributo_instancia_documentos_inter:Atributo_instancia_documento[] = [];
    documento:Documento = new Documento();
    id_documento_version:number;
}
