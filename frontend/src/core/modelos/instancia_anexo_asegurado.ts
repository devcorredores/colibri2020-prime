import { Parametro } from './parametro';
import { Asegurado } from './asegurado';
import { Entidad } from './entidad';
import { Anexo_asegurado } from './anexo_asegurado';
import { Message } from 'primeng/api';
export class Instancia_anexo_asegurado {
    id:number = 0;
    id_entidad:number;
    id_anexo_asegurado: number;
    id_asegurado: number;
    id_parentesco: number;
    id_relacion:number;
    id_estado:number;
    anexo: Anexo_asegurado;
    asegurado: Asegurado;
    parentesco: Parametro;
    relacion: Parametro;
    estado: Parametro;
    adicionado_por:string = '';
    modificado_por:string = '';
    entidad: Entidad=new Entidad();
    observaciones: Message[]=[];
}