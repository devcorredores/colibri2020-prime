import {Atributo} from "./atributo";
import {Atributo_instancia_poliza} from "./atributo_instancia_poliza";
import {Objeto} from "./objeto";
import {Parametro} from "./parametro";

export class Objeto_x_atributo {
    id: number = 0;
    id_objeto:number = 0;
    id_atributo:number = 0;
    par_editable_id:string = '';
    valor_defecto:string = '';
    adicionado_por:string = '';
    modificado_por:string = '';
    createdAt:string = '';
    updatedAt:string = '';
    par_comportamiento_interfaz_id:number;
    par_comportamiento_interfaz:Parametro = new Parametro();
    par_editable:Parametro = new Parametro();
    atributo:Atributo = new Atributo();
    objeto:Objeto = new Objeto();
    obligatorio:boolean=false;
}
