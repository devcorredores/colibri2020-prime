import { Instancia_poliza } from './instancia_poliza';
import { Anexo_poliza } from './anexo_poliza';
import {Parametro} from "./parametro";
import {Entidad} from "./entidad";
import {Objeto} from "./objeto";

export class Poliza {
    id:number = 0;
    id_ramo:number = 0;
    id_aseguradora:number = 0;
    numero:string = '';
    descripcion:string = '';
    fecha_inicio_vigencia:string = '';
    fecha_fin_vigencia:string = '';
    fecha_emision:string = '';
    adicionado_por:string = '';
    modificado_por:string = '';
    createdAt:string = '';
    updatedAt:string = '';
    tipo_numeracion:Parametro;
    ramo:Parametro;
    entidad:Entidad= new Entidad();
    objetos:Objeto[]=[];
    anexo_polizas:Anexo_poliza[]=[];
    instancia_polizas:Instancia_poliza[]=[];
    plan_pago:boolean;
    dias_espera_renovacion:number;
    dias_anticipado_renovacion:number;
    dias_habil_siguiente:number;
    id_calendario:number;
    anexo_poliza:Anexo_poliza;
    polizaObjeto:Objeto;
    documentoObjeto:Objeto;
}
