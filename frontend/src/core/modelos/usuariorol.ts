export class Usuariorol {
    id: number;
    id_usuario: number;
    id_rol: number;
    adicionado_por: string;
    modificado_por: string;
}
