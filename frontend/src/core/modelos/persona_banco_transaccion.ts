export class persona_banco_transaccion {
    detalle: string = '';
    fechatran: string = '';
    importe: string = '';
    moneda: string = '';
    nrotran: string = '';
    codigo_via: string = '';
}

export class persona_banco_transaccion_debito_csg {
    nroSolicitud: string = '';
    descripcion: string = '';
    importe: string = '';
    moneda: string = '' ;
    nrotran: string = '';
    estado: number = 0;
}

export class persona_banco_certificado {
    IdSolicitud: string = '';
    certificado: string = '';
    prima: string = '';
    moneda: string = '' ;
    fecha_vigencia: string = '';
    estado: number = 0;
    descripcion: string = '';
}
