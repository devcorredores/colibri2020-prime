export class Plan_pago {
    id:number = 0;
    id_instancia_poliza:number = 0;
    total_prima:number = 0;
    interes:number = 0;
    plazo_anos:number=0;
    periodicidad_anual:number=0;
    prepagable_postpagable:number=0;
    fecha_inicio:Date;
    adicionado_por:string='';
    modificado_por:string='';
    id_moneda:number=0;
    createdAt:Date;
    updatedAt:Date;
}
