import { Rol } from './rol';

import {Persona} from "./persona";
import {Parametro} from "./parametro";
import {Usuario_Banco} from "./usuario_banco";
import {Autenticacion_usuario} from "./autenticacion_usuario";

export class Usuario {
    id: number;
    usuario_login: string;
    usuario_password: string;
    usuario_email: string;
    usuario_nombre_completo: string;
    usuario_img: string;
    pwd_change: boolean;
    par_estado_usuario_id: number;
    par_estado_usuario_descripcion: string;
    par_autenticacion_id: number;
    par_aut_usuario_descripcion: string;
    par_local_id: number;
    par_local_descripcion: string;
    id_persona: number;
    adicionado_por: string;
    modificado_por: string;
    par_estado_usuario:Parametro;
    persona:Persona;
    usuarioRoles:Rol[] = [];
    roles_adicionar:Rol[] = [];
    usuario_banco:Usuario_Banco;
    token?:string;
    time?:string;
    autenticacion_usuario:Autenticacion_usuario = new Autenticacion_usuario();
}


