export class Componente {
    id?:string;
    codigo?:string;
    descripcion?:string;
    tipo_componente?:string;
    adicionado_por?:string;
    modificado_por?:string;
    estado?:string;
}

export class Filtro {
    agencia_required?: boolean = true;
    sucursal_required?: boolean = true;
    estado?: number = null;
    agencia?: string = null;
    sucursal?: string = null;
    usuario_login?: string = '';
    persona_primer_apellido?: string = '';
    persona_segundo_apellido?: string = '';
    persona_primer_nombre?: string = '';
    persona_doc_id?: string = '';
    persona_doc_id_ext?: string = '';
    id_poliza?: number = null;
    campo_fecha?: string = null;
    fecha_min?: Date = null;
    fecha_max?: Date = null;
    fecha_registro_min?: Date = null;
    fecha_registro_max?: Date = null;
    fecha_solicitud_min?: Date = null;
    fecha_solicitud_max?: Date = null;
    fecha_emision_min?: Date = null;
    fecha_emision_max?: Date = null;
    id_certificado?: string = '';
    id_instancia_poliza?: number = null;
    nro_documento?: string = '';
    usuarioLogin?: any = null;
    first?: number = 0;
    rows?: number = 10;
    tipo_poliza?: number;
    id_documento?: number;
}

export class Perfil_x_Componente {
    id?:string;
    id_perfil?:number;
    id_componente?:number;
    observacion?:string;
    codigo?:string;
    descripcion?:string;
    tipo_componente?:string;
    adicionado_por?:string;
    modificado_por?:string;
    estado?:string;
    componente:Componente
}
