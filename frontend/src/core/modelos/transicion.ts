import {Parametro} from "./parametro";
import {Instancia_poliza} from "./instancia_poliza";

export class Transicion {
    id:number;
    id_instancia_poliza:number;
    par_estado_id:number;
    par_observacion_id:number;
    observacion:string;
    adicionado_por:string;
    modificado_por:string;
    updatedAt:Date;
    createdAt:Date;
    par_estado:Parametro;
    par_observacion:Parametro;
    instancia_poliza:Instancia_poliza;
}
