export class persona_banco_datos {
    nro_sci:string = "";
    tipo_cuenta:string = "";
    nro_cuotas:number = 0;
    modalidad_pago:string = "";
    zona:string = "";
    nro_direccion:string = "";
    nro_tarjeta:string = "";
    razon_social:string = "";
    nit_carnet:string = "";
    ocupacion:string = "";
    desc_ocupacion:string = "";
    amp_con_amb_med_general:string = "";
    amp_con_amb_med_especializada:string = "";
    amp_sum_amb_medicamentos:string = "";
    amp_sum_exa_laboratorio:string = "";
    plan:number;
    plazo:number;
    prima:number;
    telefono_celular:string = "";
    ciudad_nacimiento:string = "";
    moneda:number;
    tipo_doc:string = "";
    direccion_laboral:string = "";
    fecha_nacimiento:Date;
    pais_nacimiento:string = "";
    monto:number = 0;
    producto_asociado:string = "";
    ruta_archivo:Archivo = null;
    provincia: string = '';
    lugar_nacimiento: string = '';
    cuestionario:Cuestionario = new Cuestionario();
}

export class Cuestionario {
    preguntas_abiertas: Pregunta_abierta[] = [];
    preguntas_cerradas: Pregunta_cerrada[] = [];
}

export class Pregunta_abierta {
    control_name:String;
    texto:String;
    respuesta: string;
}
export class Pregunta_cerrada {
    control_name:string;
    id?:number;
    texto:String;
    opciones: Opcion[];
    respuestas: Opcion[];
}
export class Opcion {
    texto:String;
    value:any;
}
export class Respuesta {
    texto:String;
    value:boolean;
}


export class Archivo {
    descripcion:string;
    abreviacion:string;
    delimitador:string;
    formato:string;
    tipo:string;
    nro_actual:string;
    id_poliza:string;
    ubicacion_remota:string;
    adicionado_por:string;
    modificado_por:string;
    ubicacion_local:string;
}
