import {Agencia} from "./agencia";
import {Sucursal} from "./sucursal";

export class Usuario_Banco {
    ro_nombre:string;
    si_nombre:string;
    si_sistema:number;
    su_cuenta:string;
    su_oficina:string;
    su_sucursal:string;
    ui_login:string;
    ui_usuario:number;
    ur_rol:number;
    ur_usersfi:string;
    us_cargo:string;
    us_codcargo:number;
    us_codsfi:number;
    us_correoe:string;
    us_doc_id:string;
    us_materno:string;
    us_nombre:string;
    us_oficina:number;
    us_paterno:string;
    us_sucursal:number;
    agencia: Agencia;
    sucursal: Sucursal;
}
