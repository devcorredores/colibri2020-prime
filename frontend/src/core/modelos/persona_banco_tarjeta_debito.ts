export class persona_banco_tarjeta_debito {
    tarjeta_nro:number;
    fecha_expiracion:Date;
    nro_cuenta:string;
    moneda:string;
    tarjeta_ultimos_cuatro_digitos:string = "";
}
