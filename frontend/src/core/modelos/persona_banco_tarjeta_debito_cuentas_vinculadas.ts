export class persona_banco_tarjeta_debito_cuentas_vinculadas {
    nro_tarjeta:string;
    fecha_activacion:Date;
    fin_vigencia:Date;
    estado_tarjeta:string;
    tipo:string
    cuentas:stuff;
    cuentaSelected:cuenta = new cuenta()
}

export class cuenta {
  nro_cuenta:string;
  moneda:string;
}
export class stuff {
  Cuentas:any;
  $:string;
}

export class TarjetaDebito {
  nro_tarjeta: string = '';
  tipo: string = '';
  fecha_activacion: Date;
  fin_vigencia: Date;
  estado_tarjeta:string;
  cuentas:cuenta[] = [];
}
