import {Instancia_poliza} from "./instancia_poliza";
import {Parametro} from "./parametro";

export class Instancia_poliza_transicion {
    id?:number;
    id_instancia_poliza:number;
    par_estado_id: number;
    par_observacion_id:number;
    observacion: string;
    adicionado_por: string;
    modificado_por: string;
    updatedAt?:Date;
    createdAt?:Date;
    instancia_poliza?:Instancia_poliza;
    par_estado?:Parametro;
    par_observacion?:Parametro;
    ipt_usuario?:Parametro;
}
