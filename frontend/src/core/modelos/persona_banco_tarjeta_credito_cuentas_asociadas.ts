export class persona_banco_tarjeta_credito_cuentas_asociadas {
  ci:string;
  nro_tarjeta:string;
  cuenta_vinculada:string;
  fin_vigencia:Date;
}
