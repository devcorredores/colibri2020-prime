import {Reporte_query} from "./reporte_query";

export class Documento_version {
    id:number;
    html:string;
    descripcion:string;
    id_reporte_query:number;
    reporteQuery:Reporte_query;
}
