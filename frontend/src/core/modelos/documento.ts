import {Poliza} from "./poliza";
import {Archivo} from "./persona_banco_datos";
import {Documento_version} from "./documento_version";

export class Documento {
    id:number = 0;
    id_poliza:number = 0;
    id_archivo:number = 0;
    descripcion:string = '';
    fecha_inicio_vigencia:string = '';
    fecha_fin_vigencia:string = '';
    fecha_emision:string = '';
    nro_inicial:string = '';
    nro_documento:string='';
    adicionado_por:string = '';
    modificado_por:string = '';
    createdAt:string = '';
    updatedAt:string = '';
    nro_actual:number = 0;
    id_tipo_documento:number;
    id_documento_version:number;
    poliza:Poliza = new Poliza();
    archivo:Archivo = new Archivo();
    documentoVersion:Documento_version;
}
