import { Dato_anexo_asegurado } from './dato_anexo_asegurado';
import { Instancia_anexo_asegurado } from './instancia_anexo_asegurado';
import { Parametro } from './parametro';
export class Anexo_asegurado {
    id:number = 0;
    id_tipo:number = 0;
    edad_minima:number;
    edad_maxima:number;
    codigo:string = '';
    descripcion:string = '';
    adicionado_por:string = '';
    modificado_por:string = '';
    instancia_anexo_asegurados:Instancia_anexo_asegurado[]=[];
    dato_anexo_asegurados:Dato_anexo_asegurado[]=[];
    parametro:Parametro=new Parametro();
    nro_obligatorios:number;
    nro_requeridos:number;
    descripcion_otro:string='';
    edad_min_permanencia:number;
    edad_max_permanencia:number;
    id_edad_unidad:number;
    edad_unidad:Parametro;
}
