import {Rol} from "./rol";
import {Usuario} from "./usuario";

export class Usuario_x_rol {
    id:number = 0;
    id_usuario:number = 0;
    id_rol:number = 0;
    adicionado_por:string = "";
    modificado_por:string = "";
    createdAt:Date;
    updatedAt:Date;
    rol:Rol;
    usuario:Usuario;
}


