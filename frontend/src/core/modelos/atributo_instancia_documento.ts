import {Instancia_documento} from "./instancia_documento";
import {Objeto_x_atributo} from "./objeto_x_atributo";
import {Parametro} from "./parametro";

export class Atributo_instancia_documento {
    id?:number = null;
    id_instancia_documento:number= 0;
    id_objeto_x_atributo:number = 0;
    valor:string = '';
    tipo_error?:string = '';
    adicionado_por:string = '';
    modificado_por:string = '';
    createdAt?:string = '';
    updatedAt?:string = '';
    instancia_documento?:Instancia_documento = new Instancia_documento();
    objeto_x_atributo?:Objeto_x_atributo = new Objeto_x_atributo();
    par_motivo?:Parametro
}
