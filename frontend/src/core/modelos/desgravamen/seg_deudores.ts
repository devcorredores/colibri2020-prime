import {SegBeneficiarios} from "./seg_beneficiarios";
import {SegSolicitudes} from "../../../app/seguros/models/seg-solicitudes";
import {SegAprobaciones} from "../../../app/seguros/models/seg-aprobaciones";
import {SegAdicionales} from "../../../app/seguros/models/seg-adicionales";

export class SegDeudores {
    Nro:number;
    Deu_Id:number;
    Deu_IdSol:number;
    Deu_NIvel:number;
    Deu_Nombre:string;
    Deu_Paterno:string;
    Deu_Materno:string;
    Deu_Casada:string;
    Deu_DirDom:string;
    Deu_DirOfi:string;
    Deu_TelDom:string;
    Deu_TelOfi:string;
    Deu_TelCel:string;
    Deu_EstCiv:number;
    Deu_Sexo:string;
    Deu_Actividad:string;
    Deu_DetACtiv:string;
    Deu_FecNac:string;
    Deu_PaisNac:string;
    Deu_CiudadNac:string;
    Deu_TipoDoc:string;
    Deu_NumDoc:string;
    Deu_ExtDoc:string;
    Deu_CompDoc:string;
    Deu_CodCli:string;
    Deu_FecRegCli:string;
    Deu_Mano:string;
    Deu_Peso:string;
    Deu_Talla:string;
    Deu_MontoActAcum:string;
    Deu_Incluido:string;
    Deu_MontoActAcumVerif:string;
    Deu_Verificado:string;
    Deu_Edad:string;
    Deu_FechaResolucion:Date;
    Hist_TipoSeg:string;
    Deu_cobertura:string;
    deu_beneficiarios:SegBeneficiarios[];
    id_documento_version:number;
    deu_solicitud:SegSolicitudes;
    deu_aprobacion:SegAprobaciones;
    deu_adicional:SegAdicionales
}