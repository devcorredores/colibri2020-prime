
export class Rol_x_perfil {
    id:number = 0;
    id_rol:number = 0;
    id_perfil:number = 0;
    adicionado_por?:string = "";
    modificado_por?:string = "";
    createdAt:Date;
    updatedAt:Date;
}
