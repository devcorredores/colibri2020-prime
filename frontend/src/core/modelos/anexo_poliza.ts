import { Anexo_asegurado } from './anexo_asegurado';
import {Parametro} from "./parametro";
export class Anexo_poliza {
    id: number = 0;
    id_poliza:number = 0;
    id_tipo:number = 0;
    codigo:string = '';
    descripcion:string = '';
    abreviacion:string='';
    createdAt:string = '';
    updatedAt:string = '';
    monto_prima:number = 0.0;
    monto_capital:number = 0.0;
    id_moneda:number;
    anexo_asegurados:Anexo_asegurado[]=[];
    par_moneda:Parametro = null
}