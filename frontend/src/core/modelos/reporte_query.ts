import { Poliza } from './poliza';
import { Reporte_query_parametro } from './reporte_query_parametro';
import { Dato_complementario } from './dato_complementario';
import {Parametro} from "./parametro";
export class Reporte_query {
    id:number;
    descripcion: string='';
    query:string='';
    id_rol:number;
    estado:string = '';
    id_poliza:number;
    reporte_query_parametros:Reporte_query_parametro[]=[];
    poliza:Poliza=new Poliza();
}