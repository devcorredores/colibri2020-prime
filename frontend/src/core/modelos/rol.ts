import {Usuario_x_rol} from "./usuario_x_rol";
import {Perfil} from "./perfil";


export class Rol {
    id: number;
    codigo: string;
    descripcion: string;
    abreviacion: string;
    des_aux:string;
    adicionado_por: string;
    modificado_por: string;
    rolPerfiles: Perfil[];
}


