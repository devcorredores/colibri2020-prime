export class Diccionario {
    id: number;
    diccionario_codigo: string;
    diccionario_descripcion: string;
    adicionado_por:string;
    modificado_por:string;
}
