export class persona_banco_tarjeta_ci_ext {
    id:string = '';
    nrotarjeta:string = '';
    fechaactivacion:Date = null;
    fechafinvigencia:Date = null;
    estado:string = '';
    tipotarjeta:string = '';
}
