import {Persona} from "./persona";
import {Persona_juridica} from "./persona_juridica";

export class Entidad {
    id: number = 0;
    id_persona:number = 0;
    id_persona_juridica:number = null;
    tipo_entidad:string = '';
    adicionado_por:string = '';
    modificado_por:string = '';
    persona:Persona = new Persona();
    persona_juridica:Persona_juridica = new Persona_juridica();
}
