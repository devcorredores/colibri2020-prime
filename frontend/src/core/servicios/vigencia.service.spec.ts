import { TestBed } from '@angular/core/testing';

import { VigenciaService } from './vigencia.service';

describe('VigenciaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VigenciaService = TestBed.get(VigenciaService);
    expect(service).toBeTruthy();
  });
});
