import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SoapuiService {
    URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/consumo-servicios`;
    //URL_API_MASIVOS: string = "http://localhost:5001/api-corredores-ecofuturo/usuario";

    constructor(private http: HttpClient) { }

    getCustomer(doc_id:string,ext:string){
        return this.http.get(this.URL_API_MASIVOS + '/getCustomer/'+doc_id+'/'+ext, {withCredentials:true});
    }

    getCustomerSol(doc_id:string,ext:number){
        return this.http.get(this.URL_API_MASIVOS + '/getCustomerSol/'+doc_id+'/'+ext, {withCredentials:true});
    }

    cuentasVinculadas(doc_id:string,ext:string){
        return this.http.get(this.URL_API_MASIVOS + '/cuentasVinculadas/'+doc_id+'/'+ext, {withCredentials:true});
    }

    cuentasAsociadas(doc_id:number,ext:number){
        return this.http.get(this.URL_API_MASIVOS + '/cuentasAsociadas/'+doc_id+'/'+ext, {withCredentials:true});
    }

    getSolicitudPrimeraEtapa(solicitud:string){
        return this.http.get(this.URL_API_MASIVOS + '/getSolicitudPrimeraEtapa/'+solicitud, {withCredentials:true});
    }
    getSolicitud(solicitud:string){
        return this.http.get(this.URL_API_MASIVOS + '/getSolicitud/'+solicitud, {withCredentials:true});
    }

    getAccount(cod_cliente:string){
        return this.http.get(this.URL_API_MASIVOS + '/getAccount/'+cod_cliente, {withCredentials:true});
    }

    validaTarjeta(cod_agenda:string, nro_tarjeta:string){
        return this.http.get(this.URL_API_MASIVOS + '/validaTarjeta/'+cod_agenda+'/'+nro_tarjeta, {withCredentials:true});
    }

    getTarjetaDebitoCuentas(nro_tarjeta:string){
        return this.http.get(this.URL_API_MASIVOS + '/getTarjetaDebitoCuentas/'+nro_tarjeta, {withCredentials:true});
    }

    getAgencia(id_sucursal:number){
        return this.http.get(this.URL_API_MASIVOS + '/getAgencia/'+id_sucursal, {withCredentials:true});
    }

    validaTran(nro_tran:string){
        return this.http.get(this.URL_API_MASIVOS + '/validaTran/'+nro_tran, {withCredentials:true});
    }

    validaTranDebitoCsg(nro_tran:string,nro_solicitud:string,monto:string,moneda:string){
        return this.http.post(this.URL_API_MASIVOS + '/validaTranDebitoCsg', {nro_tran,nro_solicitud,monto,moneda}, {withCredentials:true});
    }

    regCertificado(SolicitudId:string,certificado:string,prima:string,moneda:string,fecha_vigencia:string){
        return this.http.post(this.URL_API_MASIVOS + '/regCertificado', {SolicitudId,certificado,prima,moneda,fecha_vigencia}, {withCredentials:true});
    }

    getSucursal(){
        return this.http.get(this.URL_API_MASIVOS + '/getSucursal', {withCredentials:true});
    }
}
