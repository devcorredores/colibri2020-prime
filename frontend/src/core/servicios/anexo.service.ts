import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnexoService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/anexo`;
    
    constructor(
        private http: HttpClient,
    ) { }
  
  listaAtributosByIdPoliza(id_poliza: number,id_asegurado:number,id_anexo_poliza:number) {
    return this.http.post(this.URL_API_MASIVOS + '/AnexoAndDantoAnexoByIdPoliza',{id_poliza,id_asegurado,id_anexo_poliza}, {withCredentials:true}).toPromise();
  }
    
  findPlanesAnexoPoliza(id_poliza:number) {
      return this.http.get(this.URL_API_MASIVOS + '/findPlanesAnexoPoliza/' + id_poliza, {withCredentials:true});
  }
  
  findAnexoPoliza(id_poliza:number) {
      return this.http.get(this.URL_API_MASIVOS + '/findAnexoPoliza/' + id_poliza, {withCredentials:true});
  }
  
  findAnexoPolizaWithAsegurado(id_poliza:number, id_asegurado:number) {
      return this.http.get(this.URL_API_MASIVOS + '/findAnexoPolizaWithAsegurado/' + id_poliza + '/' + id_asegurado, {withCredentials:true});
  }

  findAllAnexoPolizaByIdPoliza(id_poliza:number) {
    return this.http.get(this.URL_API_MASIVOS + '/findAllAnexoPolizaByIdPoliza/' + id_poliza, {withCredentials:true});
  }
}
