import { Persona } from '../modelos/persona';
import { Injectable,Output,EventEmitter } from '@angular/core';
import {Router} from "@angular/router";
import {SessionStorageService} from "./sessionStorage.service";

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  isOpen = false;

  modalDisplay = false;

  persona:Persona;

  menu:any;
  item:any = {};

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  @Output() change2: EventEmitter<boolean> = new EventEmitter();

  @Output() change3: EventEmitter<Persona> = new EventEmitter();
  @Output() change4: EventEmitter<any> = new EventEmitter();

    constructor(
        private router:Router,
        private sessionStorageService:SessionStorageService,
    ) {}

  toggle() {
    this.isOpen = !this.isOpen;
    this.change.emit(this.isOpen);
  }

  abrirModal(){
    this.modalDisplay=true;
    this.change2.emit(this.modalDisplay);
  }

  asignarPersona(per:Persona){
    this.persona=per;
    this.change3.emit(this.persona);
  }

  obtenerPersona(){
    return this.persona;
  }

  guardarMenu(menu:any){
    this.menu=menu;
    this.change4.emit(this.menu);
  }

  ObtenerParametrosVista(id:any){
    this.buscaritems(this.menu,id);
    return(this.item);
  }

  activaRuteoMenu(id_vista:any,param_vista:any = null,param_ruteo:any = null,path:string = '',componentesInvisibles:any = null){
    this.buscaritems(this.menu,id_vista);
    let param:any = {};
    this.item.parametros=param;
    param.ComponentesInvisibles = componentesInvisibles;
    param.parametro_vista = param_vista;
    param.parametro_ruteo = param_ruteo;
    param.id_vista = id_vista;
    param.ruta = '';
    this.sessionStorageService.setItemSync('parametros',JSON.stringify(param));
    this.sessionStorageService.setItemSync('paramsDeleted',false);
    if (path) {
      this.router.navigate([`/AppMain/${path}`,{id_vista}])
    } else {
      this.item.command();
      this.item.parametros=null;
    }
  }

  buscaritems(menu:any,id:any){
      if (menu) {
        let item=menu.find(param=>param.id===id);
        if(item){
          this.item=item;
        }else{
          menu.forEach(element => {
            if(element.items){
              this.buscaritems(element.items,id);
            }
          });
        }
      }
  }
}
