import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdministracionDePermisosService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/admin-permisos`;

  constructor(private http: HttpClient) { }

  ListarMenu(id:any) {
    return this.http.post(this.URL_API_MASIVOS+'/ListarMenu',{id:id},{withCredentials:true});
  }
  getComponentes(idVista:number,idPerfil:number) {
    return this.http.get(this.URL_API_MASIVOS+'/getComponentes/'+idVista+'/'+idPerfil,{withCredentials:true});
  }

}
