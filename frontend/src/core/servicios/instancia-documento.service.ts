import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InstanciaDocumentoService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/instancia_documento`;
  URL_API: string = `${app.environment.URL_API}/seg-deudores`;

  constructor(private http: HttpClient) { } 
  
  findInstanciaNroDocumento(nroInstanciaDocumento:string, id_poliza:string) {
    return this.http.get(this.URL_API_MASIVOS + '/getByNroDocumento/' + nroInstanciaDocumento + '/' + id_poliza, {withCredentials:true});
  }

  findInstanciaNroDocumentoToEstado(idEstado:number, nroInstanciaDocumento:string, id_poliza:number[]) {
    return this.http.get(this.URL_API_MASIVOS + '/getByNroDocumentoToEstado/' + idEstado + '/' + nroInstanciaDocumento + '/' + id_poliza, {withCredentials:true});
  }
  findInstanciaIdToEstado(idEstado:number, id_instancia_poliza:number, id_poliza:number[]) {
    return this.http.get(this.URL_API_MASIVOS + '/getByIdInstanciaPolizaToEstado/' + idEstado + '/' + id_instancia_poliza+ '/' + id_poliza, {withCredentials:true});
  }
  findDeudorId(id:number) {
    return this.http.get(this.URL_API + '/find-deudor/' + id);
  }
}
