import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import * as app from '../../environments/environment';
import {Asegurado} from "../modelos/asegurado";
import {Persona} from "../modelos/persona";

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/persona`;

  constructor(private http: HttpClient) { }

  registro(username:string, password:string, nombres: string, email: string, estado: string, contexto: string) {
    return this.http.post(this.URL_API_MASIVOS + '/persona', {username, password, nombres, email, estado, contexto});
  }

  getDatosAdicionalesPersona(id_persona) {
    return this.http.get(this.URL_API_MASIVOS + '/GetDatosAdicionalesPersona/' + id_persona, {withCredentials:true});
  }

  FindPersonaSolicitudesByDocIdYPoliza(docId:number, docIdExt:number, idPoliza:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonaSolicitudesByDocIdYPoliza/' + docId + '/' + docIdExt + '/' + idPoliza,{withCredentials:true});
  }

  findPersonaByDocId(docId:number, docIdExt:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonaByDocId/' + docId + '/' + docIdExt,{withCredentials:true});
  }

  findPersonasAseguradasConAtributosByDocIdYPoliza(docId:string, idObjeto:number, idPoliza:any) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonasAseguradasConAtributosByDocIdYPoliza/' + docId + '/' + idObjeto + '/' + idPoliza,{withCredentials:true});
  }

  findPersonasAseguradasByDocIdYPoliza(docId:string, docIdExt:string, idObjeto:number, idPoliza:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonasAseguradasByDocIdYPoliza/' + docId + '/' + docIdExt + '/' + idObjeto + '/' + idPoliza,{withCredentials:true});
  }

  findAseguradoByDocId(docId:number, docIdExt:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindAseguradoByDocId/' + docId + '/' + docIdExt,{withCredentials:true});
  }

  findPersonaSolicitudConAtributosByDocIdYPoliza(docId:number, docIdExt:number, idPoliza:number, idObjeto:number, idInstanciaPoliza:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonaSolicitudConAtributosByDocIdYPoliza/' + docId + '/' + docIdExt + '/' + idPoliza + '/' + idObjeto + '/' + idInstanciaPoliza,{withCredentials:true});
  }

  findPersonaSolicitudByIdInstanciaPoliza(idObjeto:number,idInstanciaPoliza:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonaSolicitudByIdInstanciaPoliza/' + idObjeto + '/' + idInstanciaPoliza,{withCredentials:true});
  }

  getAtributosDatosComplementariosSuscripcion() {
    return this.http.get(this.URL_API_MASIVOS + '/GetAtributosDatosComplementariosSuscripcion', {withCredentials:true});
  }

  //AdicionarPersona(personaCompleto:any){
  //  return this.http.post(this.URL_API_MASIVOS + '/AdicionarPersona' ,personaCompleto, {withCredentials:true});
  //}

  crearNuevaSolicitud(asegurado:Asegurado){
    return this.http.post(this.URL_API_MASIVOS + '/CrearNuevaSolicitud' ,asegurado, {withCredentials:true});
  }

  actualizarSolicitud(asegurado:Asegurado) {
      return this.http.post(this.URL_API_MASIVOS + '/ActualizarSolicitud' ,asegurado, {withCredentials:true});
  }
  actualizarPersonaExt(idPersona:number,idInstanciaPoliza:number, docId:string, ext:string) {
      return this.http.post(this.URL_API_MASIVOS + '/actualizarPersonaExt/' + idInstanciaPoliza ,{idPersona,docId,ext}, {withCredentials:true});
  }
  actualizarSolicitudCambiandoEstado(asegurado:Asegurado) {
      return this.http.post(this.URL_API_MASIVOS + '/ActualizarSolicitudCambiandoEstado' ,asegurado, {withCredentials:true});
  }
  findClienteBeneficiarioByDocId(docId:number, docIdExt:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindClienteBeneficiarioByDocId/' + docId + '/' + docIdExt,{withCredentials:true});
  }
  AdicionarPersonaBeneficiario(id_asegurado: number, porcentaje: string, id_parentesco: string, descripcion_otro: string, persona_celular: string, tipo: string, estado: string, condicion: string, personaCompleto:any, nuevoRegistroConCI: string, displayBeneficiarioFound: any){
    return this.http.post(this.URL_API_MASIVOS + '/AdicionarPersonaBeneficiario' ,{id_asegurado, porcentaje, id_parentesco, descripcion_otro, persona_celular, tipo, estado, condicion, personaCompleto, nuevoRegistroConCI, displayBeneficiarioFound}, {withCredentials:true});
  }
}
