import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContextoService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/rol`;
  encryptSecretKey = '$ecret0';

  constructor(private http: HttpClient) { } 
  
  nuevoUsuarioContexto(id_rol:number, id_usuario:number){
    // const token = AccesoToken.getUsuariosStore().token;
    // const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.put(`${this.URL_API_MASIVOS}/${usuario.id}`,usuario, { headers: headersObject });
    return this.http.put(this.URL_API_MASIVOS+'/guardar_usuarioxrol', {id_rol, id_usuario}, {withCredentials:true});
  }
}
