import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Usuario } from '../modelos/usuario';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/usuario`;
  encryptSecretKey = '$ecret0';

  constructor(private http: HttpClient) { }

  // obtenerUsuarioByLogin(login: string) {
  //   const token = AccesoToken.getUsuariosStore().token;
  //   const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
  //   return this.http.get(`${this.URL_API_MASIVOS}/byLogin/${login}`, { headers: headersObject });
  // }

  obtenerUsuarios() {
    // const token = AccesoToken.getUsuariosStore().token;
    // const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.get(this.URL_API_MASIVOS, { headers: headersObject });
    return this.http.get(this.URL_API_MASIVOS+'/usuarios', {withCredentials:true});
  }
  obtenerUsuarioById(id: string) {
    return this.http.get(`${this.URL_API_MASIVOS}/findUsuarioById/${id}`, {withCredentials:true});
  }
  
  validaEmail(usuario_email: string) {
    return this.http.get(this.URL_API_MASIVOS+'/valida_email/'+ usuario_email, {withCredentials:true});
  }


  nuevoUsuario(usuario:any){
    return this.http.post(this.URL_API_MASIVOS+'/register', usuario, {withCredentials:true});
  }

  modificarUsuario(usuario:Usuario){
    // const token = AccesoToken.getUsuariosStore().token;
    // const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.put(`${this.URL_API_MASIVOS}/${usuario.id}`,usuario, { headers: headersObject });
    return this.http.put(this.URL_API_MASIVOS+'/modifica', usuario, {withCredentials:true});
 
  }

  findAllUsuariosWithRol(usuario_login) {
    return this.http.get(`${this.URL_API_MASIVOS}/findAllUsuariosWithRol`+'/'+ usuario_login, {withCredentials:true});
  }
  
  findUserById(userId:string) {
    return this.http.get(`${this.URL_API_MASIVOS}/findUserById`+'/'+ userId, {withCredentials:true});
  }



  // eliminarUsuario(id:Number){
  //   const token = AccesoToken.getUsuariosStore().token;
  //   const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
  //   return this.http.delete(`${this.URL_API_MASIVOS}/${id}`, { headers: headersObject });
  // }
  
  // setUsuarioStore(usuario:Usuario){
  //   //sessionStorageService.setItemSync('usuario-colibri', JSON.stringify(usuario));
  //   //sessionStorage.setItem('usuario-colibri', JSON.stringify(usuario));
  //   sessionStorage.setItem('usuario-colibri', this.encryptData(usuario));
  // }

  // getUsuariosStore(){
  //   //return sessionStorageService.getItemSync('usuario-colibri'));
  //   //return JSON.parse(sessionStorage.getItem('usuario-colibri'));
  //   return this.decryptData(sessionStorage.getItem('usuario-colibri'));
  // }

  // encryptData(data) {
  //   try {
  //     return CryptoJS.AES.encrypt(JSON.stringify(data), this.encryptSecretKey)+'';
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  // decryptData(data) {
  //   try {
  //     const bytes = CryptoJS.AES.decrypt(data, this.encryptSecretKey);
  //     if (bytes+'') {
  //       return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  //     }
  //     return data;
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }
}
