import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReporteService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/reporte`;

  //URL_API_MASIVOS: string = "http://localhost:5001/api-corredores-ecofuturo/usuario";

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {
  }

  OrdenPago(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/OrdenPago', {id, nombre_archivo}, {withCredentials: true});
  }

  OrdenPagoTarjetaV2(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/OrdenPagoTarCreV2', {id, nombre_archivo}, {withCredentials: true});
  }

  OrdenPagoEcoVida(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/OrdenPagoEcoVida', {id, nombre_archivo}, {withCredentials: true});
  }

  OrdenPagoEcoAccidentes(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/OrdenPagoEcoAccidentes', {id, nombre_archivo}, {withCredentials: true});
  }

  OrdenPagoEcoResguardo(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/OrdenPagoEcoResguardo', {id, nombre_archivo}, {withCredentials: true});
  }

  OrdenPagoEcoRiesgo(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/OrdenPagoEcoRiesgo', {id, nombre_archivo}, {withCredentials: true});
  }

  OrdenPagoEcoMedic(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/OrdenPagoEcoMedic', {id, nombre_archivo}, {withCredentials: true});
  }

  certificado_ecoaguinaldo(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/certificado_ecoaguinaldo', {id, nombre_archivo}, {withCredentials: true});
  }

  certificado_ecoriesgo(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/certificado_ecoriesgo', {id, nombre_archivo}, {withCredentials: true});
  }

  ReporteSolicitud(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteSolicitud', {id, nombre_archivo}, {withCredentials: true});
  }

  ReporteSolicitudEcoRiesgo(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteSolicitudEcoRiesgo', {id, nombre_archivo}, {withCredentials: true});
  }

  ReporteSolicitudEcoVida(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteSolicitudEcoVida', {id, nombre_archivo}, {withCredentials: true});
  }

  ReporteSolicitudEcoAccidentes(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteSolicitudEcoAccidentes', {
      id,
      nombre_archivo
    }, {withCredentials: true});
  }

  ReporteSolicitudEcoResguardo(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteSolicitudEcoResguardo', {
      id,
      nombre_archivo
    }, {withCredentials: true});
  }

  ReporteSolicitudEcoProteccion(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteSolicitudEcoProteccion', {
      id,
      nombre_archivo
    }, {withCredentials: true});
  }

  certificado_Ecomedic(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/certificado_Ecomedic', {id, nombre_archivo}, {withCredentials: true});
  }

  certificado_Ecomedic2(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteCertificadoEcoMedic2', {id, nombre_archivo}, {withCredentials: true});
  }

  ReporteSolicitudEcomedic(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteSolicitudEcomedic', {id, nombre_archivo}, {withCredentials: true});
  }

  ReporteSolicitudTarjeta(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteSolicitudTarjeta', {id, nombre_archivo}, {withCredentials: true});
  }

  ReporteCertificadoTarjeta(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteCertificadoTarjeta', {id, nombre_archivo}, {withCredentials: true});
  }

  ReporteCertificadoEcoVida(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteCertificadoEcoVida', {id, nombre_archivo}, {withCredentials: true});
  }

  ReporteCertificadoEcoAccidentes(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteCertificadoEcoAccidentes', {
      id,
      nombre_archivo,
      origin: location.origin
    }, {withCredentials: true});
  }

  ReporteCertificadoEcoResguardo(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteCertificadoEcoResguardo', {
      id,
      nombre_archivo,
      origin: location.origin
    }, {withCredentials: true});
  }

  ReporteCertificadoEcoProteccion(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteCertificadoEcoProteccion', {
      id,
      nombre_archivo
    }, {withCredentials: true});
  }

  ReporteCertificadoEcoMedic(id: any, nombre_archivo: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReporteCertificadoEcoMedic', {id, nombre_archivo}, {withCredentials: true});
  }

  cargarPagina(nombre_archivo: string) {
    if (nombre_archivo.indexOf('.pdf') < 0) {
      nombre_archivo += '.pdf';
    }
    let urlPdf = `${app.environment.URL_PUBLIC_PDF_MASIVOS}/${nombre_archivo}`;
    let innerHtml: any = this.sanitizer.bypassSecurityTrustHtml(`
    <iframe src="${urlPdf}" width="100%" height="100%" frameBorder="0"></iframe>
    `);
    let a = innerHtml.changingThisBreaksApplicationSecurity;
    window.open(urlPdf, "PDF", "width=1000,height=1000,");
    //ventana.document.open();
  }

  cargarUpload(nombre_archivo: string) {
    let urlPdf = `${app.environment.URL_PUBLIC_UPLOAD_MASIVOS}/${nombre_archivo}`;
    let innerHtml: any = this.sanitizer.bypassSecurityTrustHtml(`
    <iframe src="${urlPdf}" width="100%" height="100%" frameBorder="0"></iframe>
    `);
    let a = innerHtml.changingThisBreaksApplicationSecurity;
    window.open(urlPdf, "PDF", "width=1000,height=1000,");
  }

  ReportePlanPago(nro_certificado:any,id_instancia_poliza:any,nombre_archivo:any) {
    return this.http.post(this.URL_API_MASIVOS + '/ReportePlanPago', {nro_certificado,id_instancia_poliza, nombre_archivo}, {withCredentials:true});
  }
}
