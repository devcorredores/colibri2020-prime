import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlanPagoService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/plan_pago`;

  constructor(private http: HttpClient) { }

  GenerarPlanPagos(plan_pago:any) {
    return this.http.post(this.URL_API_MASIVOS + '/GenerarPlanPagos' ,plan_pago, {withCredentials:true});
  }

  GenerarPlanPagos2(plan_pago:any) {
    return this.http.post(this.URL_API_MASIVOS + '/GenerarPlanPagos' ,plan_pago, {withCredentials:true}).toPromise();
  }

  GenerarArchivo(data:any,id_poliza:any) {
    return this.http.post(this.URL_API_MASIVOS + '/GenerarArchivo' ,{data,id_poliza}, {withCredentials:true}).toPromise();
  }

  GenerarPagosProcedimiento(fecha:any,id_poliza:any, dias_habil_siguiente:number, id_calendario:number) {
    return this.http.post(this.URL_API_MASIVOS + '/GenerarPagosProcedimiento' ,{fecha,id_poliza,dias_habil_siguiente, id_calendario}, {withCredentials:true}).toPromise();
  }

  GenerarPagos(fecha:any,id_poliza:any) {
    return this.http.post(this.URL_API_MASIVOS + '/GenerarPagos' ,{fecha,id_poliza}, {withCredentials:true}).toPromise();
  }

  listaTodosSinPlanPagos() {
    return this.http.get(this.URL_API_MASIVOS + '/listaTodosSinPlanPagos' , {withCredentials:true});
  }


  listaTodosSinPlanPagosMigrados() {
    return this.http.get(this.URL_API_MASIVOS + '/listaTodosSinPlanPagosMigrados' , {withCredentials:true});
  }

  GetPlanPagoByNroCertAndIdPoliza(objeto:any){
    return this.http.post(this.URL_API_MASIVOS + '/GetPlanPagoByNroCertAndIdPoliza' ,objeto, {withCredentials:true}).toPromise();
  }

  GetPendientesByIdPolizaAndFechaAndDiasGracia(objeto:any){
    return this.http.post(this.URL_API_MASIVOS + '/GetPendientesByIdPolizaAndFechaAndDiasGracia' ,objeto, {withCredentials:true}).toPromise();
  }

  GetPlanPagoByNroCertAndIdPolizaAndInstanciaPoliza(objeto:any){
    return this.http.post(this.URL_API_MASIVOS + '/GetPlanPagoByNroCertAndIdPolizaAndInstanciaPoliza' ,objeto, {withCredentials:true}).toPromise();
  }

  listaCertificadosPorNroDocYIdPoliza(nro_certificado:any,id_poliza){
    return this.http.post(this.URL_API_MASIVOS + '/listaCertificadosPorNroDocYIdPoliza' ,{nro_certificado,id_poliza}, {withCredentials:true}).toPromise();
  }

}
