import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InstanciaPolizaService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/instancia_poliza`;

  constructor(private http: HttpClient) { } 
  
  GetAll(){
    return this.http.get(this.URL_API_MASIVOS+'/GetAll', {withCredentials:true});
  }

  GetAllByParametros(objeto : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByParametros',objeto, {withCredentials:true});
  }

  GetAllByParametrosEcoVida(objeto : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByParametrosEcoVida',objeto, {withCredentials:true});
  }

  GetAllByParametrosEcoAccidentes(objeto : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByParametrosEcoAccidentes',objeto, {withCredentials:true});
  }

  GetAllByParametrosDesgravamen(objeto : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByParametrosDesgravamen',objeto, {withCredentials:true});
  }

  GetAllByParametrosTarjetas(objeto : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByParametrosTarjetas',objeto, {withCredentials:true});
  }

  GetAllByIdsEcoAccidentes(objeto : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByIdsEcoAccidentes',objeto, {withCredentials:true});
  }

  listaAllByEstado(objeto : any){
    return this.http.post(this.URL_API_MASIVOS+'/listaAllByEstado',objeto, {withCredentials:true});
  }

  GetAllByIds(ids : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByIds',ids, {withCredentials:true});
  }

  GetAllByIdsEcoVida(ids : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByIdsEcoVida',ids, {withCredentials:true});
  }

  GetAllByIdsDesgravamen(ids : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByIdsDesgravamen',ids, {withCredentials:true});
  }

  GetAllByIdsTarjetas(ids : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByIdsTarjetas',ids, {withCredentials:true});
  }

  GetAllByIdsEcoMedic(ids : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByIdsEcoMedic',ids, {withCredentials:true});
  }

  GetById(id : any){
    return this.http.get(this.URL_API_MASIVOS+'/GetById/'+id, {withCredentials:true});
  }

  findInstanciaPolizaConAtributosEditablesYVisiblesByIdYPolizaId(idInstanciaPoliza:number,idPoliza:number,idEditable:number,idVisible:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindInstanciaPolizaConAtributosEditablesYVisiblesByIdYPolizaId/' + idInstanciaPoliza+ '/' + idPoliza +'/' + idEditable + '/' + idVisible,{withCredentials:true});
  }
  
  findInstanciaPolizaById(idInstanciaPoliza:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindInstanciaPolizaById/' + idInstanciaPoliza,{withCredentials:true});
  }

  findInstanciaPolizaConAtributosByIdYPolizaId(idInstanciaPoliza:number, idPoliza:number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindInstanciaPolizaConAtributosByIdYPolizaId/' + idInstanciaPoliza+ '/' + idPoliza,{withCredentials:true});
  }
  findSolicitudTransiciones(idInstanciaPoliza:number) {
    return this.http.get(this.URL_API_MASIVOS + '/findSolicitudTransiciones/' + idInstanciaPoliza,{withCredentials:true});
  }
  updateInstanciaToNextStatus(statusIds:number[], asegurado:any) {
    return this.http.post(this.URL_API_MASIVOS + '/UpdateInstanciaToNextStatus/' + statusIds, asegurado,{withCredentials:true});
  }

  GetAllByParametrosEcoMedic(objeto : any){
    return this.http.post(this.URL_API_MASIVOS+'/GetAllByParametrosEcoMedic',objeto, {withCredentials:true});
  }
  
  findPolizaNrosTransaccion(idPoliza, nroTransaccion) {
      return this.http.get(this.URL_API_MASIVOS + '/findPolizaNrosTransaccion/' + idPoliza + '/' + nroTransaccion,{withCredentials:true});
  }
  
  verifyCouldBeRenovated(idPoliza, idInstanciaPoliza) {
      return this.http.get(this.URL_API_MASIVOS + '/verifyCouldBeRenovated/' + idPoliza + '/'+ idInstanciaPoliza,{withCredentials:true});
  }
}
