import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import * as app from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TransicionService {

    URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/transicion`;
    encryptSecretKey = '$ecret0';

    constructor(private http: HttpClient) { }

}
