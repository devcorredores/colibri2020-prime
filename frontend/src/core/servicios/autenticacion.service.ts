import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/autenticacion`;
  encryptSecretKey = '$ecret0';

  constructor(private http: HttpClient) { } 
  
  obtenerAutenticacionUsuario(idUsuario: number) {
    return this.http.get(this.URL_API_MASIVOS + '/autenticacionusuario/' + idUsuario, {withCredentials:true});
  }

  obtenerAutenticacionById(id: string) {
    return this.http.get(this.URL_API_MASIVOS + '/byid/' + id, {withCredentials:true});
  }

  obtenerAutenticaciones() {
    return this.http.get(this.URL_API_MASIVOS, {withCredentials:true});
  }

  nuevoUsuarioAutenticacion(par_autenticacion_id:number, id_usuario:number){
    return this.http.post(this.URL_API_MASIVOS+'/guardar_autenticacionxusuario', {par_autenticacion_id, id_usuario}, {withCredentials:true});
  }
}
