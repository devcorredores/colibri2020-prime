import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ObjetoAtributoService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/objeto_atributo`;
  
  constructor(private http: HttpClient) { }
  
  getAllObjetoAtributosByIdObjeto(idObjeto: number) {
      return this.http.get(this.URL_API_MASIVOS + '/getAllObjetoAtributosByIdObjeto/' + idObjeto, {withCredentials:true});
  }
  
  
  
}
