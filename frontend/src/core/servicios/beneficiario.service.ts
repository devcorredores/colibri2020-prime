import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BeneficiarioService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/beneficiario`;

  constructor(private http: HttpClient) { }

  eliminarBeneficiario(id: any) {
    return this.http.delete(this.URL_API_MASIVOS + '/eliminar/' + id);
    // return this.http.delete(this.URL_API_MASIVOS + '/eliminar' ,id, {withCredentials:true});
  }

  findBeneficiario(id: any) {
    return this.http.get(this.URL_API_MASIVOS + '/' + id);
    // return this.http.delete(this.URL_API_MASIVOS + '/eliminar' ,id, {withCredentials:true});
  }

  findId_Beneficiario(id: any) {
    return this.http.get(this.URL_API_MASIVOS + '/byId_beneficiario/' + id);
    // return this.http.delete(this.URL_API_MASIVOS + '/eliminar' ,id, {withCredentials:true});
  }

  registro(username: string, password: string, nombres: string, email: string, estado: string, contexto: string) {
    return this.http.post(this.URL_API_MASIVOS + '/persona', { username, password, nombres, email, estado, contexto });
  }

  ModificarPersonaBeneficiario(id_beneficiario: string, porcentaje: string, id_parentesco: string, descripcion_otro: string, persona_celular: string, tipo: string, estado: string, condicion: string, personaCompleto: any) {
    return this.http.put(this.URL_API_MASIVOS + '/modificar', { id_beneficiario, porcentaje, id_parentesco, descripcion_otro, persona_celular, tipo, estado, condicion, personaCompleto }, { withCredentials: true });
  }

  getDatosAdicionalesPersona(id_persona) {
    return this.http.get(this.URL_API_MASIVOS + '/GetDatosAdicionalesPersona/' + id_persona, { withCredentials: true });
  }

  FindPersonaSolicitudesByDocIdYPoliza(docId: number, docIdExt: number, idPoliza: number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonaSolicitudesByDocIdYPoliza/' + docId + '/' + docIdExt + '/' + idPoliza, { withCredentials: true });
  }

  findPersonaByDocId(docId: number, docIdExt: number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonaByDocId/' + docId + '/' + docIdExt, { withCredentials: true });
  }

  findPersonasAseguradasConAtributosByDocIdYPoliza(docId: number, docIdExt: number, idPoliza: number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonasAseguradasConAtributosByDocIdYPoliza/' + docId + '/' + docIdExt + '/' + idPoliza, { withCredentials: true });
  }

  findAseguradoByDocId(docId: number, docIdExt: number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindAseguradoByDocId/' + docId + '/' + docIdExt, { withCredentials: true });
  }

  findPersonaSolicitudConAtributosByDocIdYPoliza(docId: number, docIdExt: number, idPoliza: number, idInstanciaPoliza: number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindPersonaSolicitudConAtributosByDocIdYPoliza/' + docId + '/' + docIdExt + '/' + idPoliza + '/' + idInstanciaPoliza, { withCredentials: true });
  }

  getAtributosDatosComplementariosSuscripcion() {
    return this.http.get(this.URL_API_MASIVOS + '/GetAtributosDatosComplementariosSuscripcion', { withCredentials: true });
  }

  //AdicionarPersona(personaCompleto:any){
  //  return this.http.post(this.URL_API_MASIVOS + '/AdicionarPersona' ,personaCompleto, {withCredentials:true});
  //}

  crearNuevaSolicitud(datosPersona: any) {
    return this.http.post(this.URL_API_MASIVOS + '/CrearNuevaSolicitud', datosPersona, { withCredentials: true });
  }

  actualizarSolicitud(datosPersona: any) {
    return this.http.post(this.URL_API_MASIVOS + '/ActualizarSolicitud', datosPersona, { withCredentials: true });
  }
  findClienteBeneficiarioByDocId(docId: number, docIdExt: number) {
    return this.http.get(this.URL_API_MASIVOS + '/FindClienteBeneficiarioByDocId/' + docId + '/' + docIdExt, { withCredentials: true });
  }
  AdicionarPersonaBeneficiario(id_asegurado: string, porcentaje: string, id_parentesco: string, descripcion_otro: string, telefono_celular: string, tipo: string, estado: string, personaCompleto: any) {
    return this.http.post(this.URL_API_MASIVOS + '/AdicionarPersonaBeneficiario', { id_asegurado, porcentaje, id_parentesco, descripcion_otro, telefono_celular, tipo, estado, personaCompleto }, { withCredentials: true });
  }
  migrarBeneficiariosDesgravamen(nroSolicitudSci:string, docId:string, docIdExt:string, idInstanciaPoliza:number) {
    return this.http.post(this.URL_API_MASIVOS + '/migrarBeneficiariosDesgravamen', {nroSolicitudSci , docId, docIdExt, idInstanciaPoliza},{ withCredentials: true });
  }
  verificaBeneficiariosDesgravamen(nroSolicitudSci:string, docId:string, docIdExt:string) {
    return this.http.post(this.URL_API_MASIVOS + '/verificaBeneficiariosDesgravamen',{nroSolicitudSci,docId,docIdExt},{ withCredentials: true });
  }
}
