import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ArchivoService {

  URL_API: string = `${environment.URL_API_MASIVOS}/archivo`;
  //URL_API: string = "http://localhost:5001/api-corredores-ecofuturo/usuario";
  rutaUpload: string = this.URL_API+'/upload';

  constructor(private http: HttpClient) { }

  getCustomer(doc_id:number,ext:number){
    return this.http.get(this.URL_API + '/getCustomer/'+doc_id+'/'+ext, {withCredentials:true});
  }
  delete(idInstanciaDocumento:number){
    return this.http.get(this.URL_API + '/delete/'+idInstanciaDocumento, {withCredentials:true});
  }

}
