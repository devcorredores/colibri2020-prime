import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PolizaService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/poliza`;
  
  constructor(private http: HttpClient) { }
  
  listaPolizas(idsPolizas:any = null) {
    return this.http.get(this.URL_API_MASIVOS + '/listaPolizas', {withCredentials:true});
  }
  
  getPolizaById(idPoliza:number) {
    return this.http.get(this.URL_API_MASIVOS + '/getPolizaById/' + idPoliza , {withCredentials:true});
  }
  getPolizaByIds(idPolizas:number[]) {
    return this.http.get(this.URL_API_MASIVOS + '/getPolizaByIds/'+idPolizas, {withCredentials:true});
  }
}
