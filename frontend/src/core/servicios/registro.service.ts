import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/usuario`;
  //URL_API_MASIVOS: string = "http://localhost:5001/api-corredores-ecofuturo/usuario";
  
  constructor(private http: HttpClient) { }

  registro(username:string, password:string, nombres: string, email: string, estado: string, contexto: string) {
    // const vtimeout = Number(app.settings.httpTimeout);
    // console.log(username+" "+password);
    return this.http.post(this.URL_API_MASIVOS + '/register', {username, password, nombres, email, estado, contexto});
  }

  cambiarContrasena(username:string, password:string, password2:string){
        return this.http.post(this.URL_API_MASIVOS + '/restore', {username, password, password2}, {withCredentials:true})
  } 
}
