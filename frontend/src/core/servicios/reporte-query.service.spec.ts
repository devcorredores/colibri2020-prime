import { TestBed } from '@angular/core/testing';

import { ReporteQueryService } from './reporte-query.service';

describe('ReporteQueryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReporteQueryService = TestBed.get(ReporteQueryService);
    expect(service).toBeTruthy();
  });
});
