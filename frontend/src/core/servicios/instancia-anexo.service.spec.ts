import { TestBed } from '@angular/core/testing';

import { InstanciaAnexoService } from './instancia-anexo.service';

describe('InstanciaAnexoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstanciaAnexoService = TestBed.get(InstanciaAnexoService);
    expect(service).toBeTruthy();
  });
});
