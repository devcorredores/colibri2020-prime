import { Usuario } from '../modelos/usuario';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/rol`;
  encryptSecretKey = '$ecret0';

  constructor(private http: HttpClient) { } 
  
  obtenerRolUsuario(idUsuario: number) {
    return this.http.get(this.URL_API_MASIVOS + '/rolusuario/' + idUsuario, {withCredentials:true});
  }

  obtenerRolesById(id: string) {
    return this.http.get(this.URL_API_MASIVOS + '/byid/' + id, {withCredentials:true});
  }

  obtenerRoles() {
    return this.http.get(this.URL_API_MASIVOS, {withCredentials:true});
  }

  nuevoUsuarioRol(id_rol:number, id_usuario:number){
    // const token = AccesoToken.getUsuariosStore().token;
    // const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.put(`${this.URL_API_MASIVOS}/${usuario.id}`,usuario, { headers: headersObject });
    return this.http.post(this.URL_API_MASIVOS+'/guardar_usuarioxrol', {id_rol, id_usuario}, {withCredentials:true});
  }

  ResgistrarEmparejamiento(usuarios:Usuario[]){
    return this.http.post(this.URL_API_MASIVOS+'/ResgistrarEmparejamiento', usuarios, {withCredentials:true});
  }

  eliminarRol_x_Perfil(rol:any){
    return this.http.post(this.URL_API_MASIVOS+'/eliminarRol_x_Perfil', rol, {withCredentials:true});
  }
}
