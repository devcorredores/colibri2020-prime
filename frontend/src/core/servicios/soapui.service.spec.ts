import { TestBed } from '@angular/core/testing';

import { SoapuiService } from './soapui.service';

describe('SoapuiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SoapuiService = TestBed.get(SoapuiService);
    expect(service).toBeTruthy();
  });
});
