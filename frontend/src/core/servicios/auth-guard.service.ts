import { LoginService } from './login.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor( private loginService : LoginService,private route : Router) { }
  canActivate(){
    if(this.loginService.isAuthenticated()){
      return true;
    }
    this.route.navigate(['login']);
    return false;
  }
}
