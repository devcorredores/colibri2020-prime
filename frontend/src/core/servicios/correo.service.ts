import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Correo } from '../modelos/correo';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CorreoService {

  // URL_API_MASIVOS = 'http://localhost:3000/api-corredores-ecofuturo/mail';

  URL_API_MASIVOS = `${app.environment.URL_API_MASIVOS}/sendmail`;

  constructor(private http: HttpClient) { }

  enviarEmail(correo: Correo) {
    // const token = AccesoToken.getUsuariosStore().token;
    // const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.post(this.URL_API_MASIVOS, correo, { headers: headersObject });
    return this.http.post(this.URL_API_MASIVOS, correo, {withCredentials:true});
  }

  // enviarEmailCambioEstado(mensaje: any) {
  //   const token = AccesoToken.getUsuariosStore().token;
  //   const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
  //   return this.http.post(`${this.URL_API_MASIVOS}/estado`, mensaje, { headers: headersObject });
  // }
  
  // EnvioCorreo(mensaje: any) {
  //   const token = AccesoToken.getUsuariosStore().token;
  //   const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
  //   return this.http.post(`${this.URL_API_MASIVOS}/Observaciones2`, mensaje, { headers: headersObject });
  // }
}
