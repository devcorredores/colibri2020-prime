import {Confirmation} from "primeng/api";

export declare class ConfirmService {
    private requireConfirmationSource;
    private acceptConfirmationSource;
    requireConfirmation$: import("rxjs").Observable<Confirmation>;
    accept: import("rxjs").Observable<Confirmation>;

    confirm(confirmation: Confirmation): this;

    onAccept(): void;


}
