import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Instancia_poliza_transicion} from "../modelos/instancia_poliza_transicion";
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InstanciaPolizaTransService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/instancia_poliza_transicion`;
  URL_API: string = `${app.environment.URL_API}/seg-deudores`;
  oldInstanciaPolizaTransiciones: Instancia_poliza_transicion[] = [];

  constructor(private http: HttpClient) { }

  GetAll(){
    return this.http.get(this.URL_API_MASIVOS+'/GetAll', {withCredentials:true});
  }

  guardarEstadoInstanciaPolizaTrans(id_instancia_poliza,id_poliza,par_estado_id,id_motivo, observacion,adicionado_por) {
    return this.http.post(this.URL_API_MASIVOS + '/register', {id_instancia_poliza,id_poliza,par_estado_id,id_motivo,observacion,adicionado_por}, {withCredentials:true} );
  }

  guardarNuevaObservacion(newObservationValue:string, idInstanciaPoliza:number, idEstado:number) {
    return this.http.post(this.URL_API_MASIVOS + '/registerObservacion', {newObservationValue, idInstanciaPoliza, idEstado}, {withCredentials:true} );
  }

  saveOrUpdateTransition(instanciaPolizaTransicions:Instancia_poliza_transicion[]) {
    return this.http.post(this.URL_API_MASIVOS + '/saveOrUpdateTransition', instanciaPolizaTransicions, {withCredentials:true} );
  }

  guardarEstadoDeudorTrans(Deu_Id,Deu_Incluido, Obs_Observacion, Obs_Usuario, Obs_IdSol) {
    return this.http.post(this.URL_API + '/update-deudor/' + Deu_Id, {Seg_Deudor:{Deu_Incluido},Seg_Observacion:{Obs_Observacion,Obs_Usuario,Obs_IdSol,Obs_FechaReg:new Date(),Obs_HoraReg:new Date().getHours()+':'+new Date().getMinutes()}});
  }
  getDeuId(Deu_Id) {
    return this.http.post(this.URL_API + '/get-deudor',Deu_Id);
  }
}
