import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { UsuariosService } from './usuarios.service';
import {Usuario} from "../modelos/usuario";
import {SessionStorageService} from "./sessionStorage.service";
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  usuario:Usuario;
  displaySinSucursalAgencia:Boolean = false;
  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/usuario`;

  loggedInUserInfo: {};

  constructor(
      private http: HttpClient,
      private usuarioService: UsuariosService,
      private sessionStorageService: SessionStorageService
  ) { }

  public isAuthenticated(): Boolean {
    let userData = this.sessionStorageService.getItemSync('userInfo');
    if (userData) {
      return true;
    }
    return false;
  }

  async setUserInfo(user) {
    let userStore: any = { rol: {}};
    userStore.id = user.id;
    userStore.usuario_login = user.usuario_login;
    userStore.usuario_nombre_completo = user.usuario_nombre_completo;
    userStore.usuario_email = user.usuario_email;
    userStore.pwd_change = user.pwd_change;
    userStore.par_local_id = user.par_local_id;
    // await this.usuarioService.obtenerUsuarioById(userStore.id).subscribe(async (resp) => {
    //   let response: any;
    //   response = resp;
    //   userStore.rol = response.data.usuario_x_rols[0].rol;
    //   sessionStorageService.setItemSync('userInfo', JSON.stringify(userStore));
    // });

    this.sessionStorageService.setItemSync('userInfo', JSON.stringify(user));
  }

  login(username: string, password: string) {
    // const vtimeout = Number(app.settings.httpTimeout);
    // console.log(username+" "+password);
    return this.http.post(this.URL_API_MASIVOS + '/authenticate', { username, password }, { withCredentials: true });
  }

  cerrarSesion() {
    return this.http.get(this.URL_API_MASIVOS + '/logout', { withCredentials: true }).toPromise()
  }

  prueba() {
    // const vtimeout = Number(app.settings.httpTimeout);
    return this.http.get(this.URL_API_MASIVOS + '/prueba', { withCredentials: true }).toPromise()
  }

  resetPassword(username: string, password: string, password2: string, pwdchange: number) {
      return this.http.post(this.URL_API_MASIVOS + '/restore', { username, password, password2, pwdchange }, { withCredentials: true });
  }

  resetPasswordFirst(username: string, password: string, password2: string, pwdchange: number, pwdold: string) {
      return this.http.post(this.URL_API_MASIVOS + '/restore_first', { username, password, password2, pwdchange, pwdold }, { withCredentials: true });
  }

  resetPasswordRequest(username: string, password: string, password2: string, pwdchange: number) {
    return this.http.post(this.URL_API_MASIVOS + '/restore_request', { username, password, password2, pwdchange }, { withCredentials: true });
  }

  resetPasswordOld(username: string, passwordold: string, password: string, password2: string, pwdchange: number) {
    return this.http.post(this.URL_API_MASIVOS + '/restore_required', { username, passwordold, password, password2, pwdchange }, { withCredentials: true });
  }
}
