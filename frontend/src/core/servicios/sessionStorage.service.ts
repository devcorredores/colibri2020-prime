import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as CryptoJS from 'crypto-js';

const KEY_ENCRYPT_SECRET = 'pr4bvrw3ghdpbpsxuhnxrhyc4u5q3pjx';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {

private cache: BehaviorSubject<any>[];

  constructor() {
    this.cache = Object.create(null);
  }

  setItem<T>(key: string, value: T): BehaviorSubject<T> {
    try {
      const serializeState = JSON.stringify(value);
      localStorage.setItem(key, serializeState);
      sessionStorage.setItem(key, serializeState);

      if (this.cache[key]) {
        this.cache[key].next(value);
        return this.cache[key];
      }
    } catch (err) {
      throw new Error(err);
    }
  }

  getItem<T>(key: string): BehaviorSubject<T> {
    try {
      if (this.cache[key]) {
        return this.cache[key];
      } else {
        let localValue = localStorage.getItem(key);
        let sessionValue = sessionStorage.getItem(key);
        if(sessionValue && localValue) {
          return this.cache[key] = new BehaviorSubject(
            JSON.parse(localStorage.getItem(key))
          );
        } else {
          return null;
        }
      }
    } catch (err) {
      throw new Error('ERROR!:' + err);
    }
  }

  setItemSync(key: string, value: any, encrypted: boolean = true) {
    if (encrypted) {
      value = this.encryptData(value);
    }
    localStorage.setItem(key, value);
    sessionStorage.setItem(key, value);

  }

  getItemSync(key: string, encrypted: boolean = true) {
    let localValue = localStorage.getItem(key);
    let sessionValue = sessionStorage.getItem(key);
    if (!localValue && !sessionValue) {
      return null;
    }
    if (localValue) {
      if (encrypted) {
        localValue = this.decryptData(localValue);
      }
      return localValue ? typeof localValue == 'object' ? localValue : typeof localValue == 'string' ? JSON.parse(localValue) : localValue : localValue;
    } else {
      return localValue;
    }
  }

  removeItem(key: string) {
    localStorage.removeItem(key);
    sessionStorage.removeItem(key);
    if (this.cache[key]) {
      // this.cache[key].next(undefined);
      delete this.cache[key];
    }
  }

  private encryptData(data) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(data), KEY_ENCRYPT_SECRET)+'';
    } catch (e) {
      // console.log(e);
    }
  }

  private decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, KEY_ENCRYPT_SECRET);
      if (bytes+'') {
        let decripted = bytes.toString(CryptoJS.enc.Utf8);
        let objDecriptedData = JSON.parse(decripted);
        return objDecriptedData ? objDecriptedData : decripted;
      }
      return data;
    } catch (e) {
      // console.log(e);
    }
  }

}
