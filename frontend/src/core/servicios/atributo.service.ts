import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AtributoService {
    
    URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/atributo`;
    
    constructor(
      private http: HttpClient,
  ) { }
  
  listaAtributosByIdPoliza(id: number) {
    return this.http.get(this.URL_API_MASIVOS + '/listaAtributosByIdPoliza/' + id, {withCredentials:true});
  }
  getAllAtributosByIdObjeto(id: number []) {
      return this.http.get(this.URL_API_MASIVOS + '/getAllAtributosByIdObjeto/' + id, {withCredentials:true});
  }
  getAllAtributosByIdObjetoEditablesYVisibles(idObjeto: number, idEditable:number, idVisible:number) {
      return this.http.get(this.URL_API_MASIVOS + '/getAllAtributosByIdObjetoEditablesYVisibles/' + idObjeto +'/' + idEditable + '/' + idVisible, {withCredentials:true});
  }
  
  getAllAtributosByObjetoIdInstanciaPoliza(idObjeto: number, idInstanciaPoliza:number) {
      return this.http.get(this.URL_API_MASIVOS + '/getAllAtributosByObjetoIdInstanciaPoliza/' + idObjeto +'/' + idInstanciaPoliza, {withCredentials:true});
  }
  
  getAllAtributosByObjeto(idObjeto: number) {
      return this.http.get(this.URL_API_MASIVOS + '/getAllAtributosByObjeto/' + idObjeto, {withCredentials:true});
  }
  
  getAllAtributosByObjetoIdInstanciaPolizaEditablesYVisibles(idObjeto: number, idInstanciaPoliza:number, idEditable:number, idVisible:number) {
      return this.http.get(this.URL_API_MASIVOS + '/getAllAtributosByObjetoIdInstanciaPolizaEditablesYVisibles/' + idObjeto +'/' + idInstanciaPoliza + '/' + idEditable + '/' + idVisible, {withCredentials:true});
  }
}
