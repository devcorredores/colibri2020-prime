import { Injectable,Output,EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Parametro } from '../modelos/parametro';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ParametrosService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/parametro`;
  encryptSecretKey = '$ecret0';
  parametros:Parametro[]=[];
  @Output() change: EventEmitter<Parametro[]> = new EventEmitter();
  constructor(
    private http: HttpClient
    ) { }

  obtenerParametros(id: string) {
    // const token = AccesoToken.getUsuariosStore().token;
    // const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.get(this.URL_API_MASIVOS, { headers: headersObject });
    return this.http.get(this.URL_API_MASIVOS + '/byid/' + id, {withCredentials:true});
  }

  obtenerAllParametro() {
    // const token = AccesoToken.getUsuariosStore().token;
    // const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.get(this.URL_API_MASIVOS, { headers: headersObject });
    return this.http.get(this.URL_API_MASIVOS, {withCredentials:true});
  }

  obtenerParamEstado(id: number) {
    // const token = AccesoToken.getUsuariosStore().token;
    // const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.get(this.URL_API_MASIVOS, { headers: headersObject });
    return this.http.get(this.URL_API_MASIVOS + '/byidparam/' + id+'', {withCredentials:true});
  }

  GetParametrosByIdDiccionario(id: number) {
    return this.http.get(this.URL_API_MASIVOS + '/GetParametrosByIdDiccionario/' + id+'', {withCredentials:true});
  }

  GetAllDiccionarios() {
    return this.http.get(this.URL_API_MASIVOS + '/GetAllDiccionarios', {withCredentials:true});
  }

  GetAllParametros() {
    return this.http.get(this.URL_API_MASIVOS + '/GetAllParametros', {withCredentials:true});
  }

  NewDiccionario(diccionario:any){
    return this.http.post(this.URL_API_MASIVOS + '/NewDiccionario',diccionario, {withCredentials:true});
  }

  NewParametro(parametro:any){
    return this.http.post(this.URL_API_MASIVOS + '/NewParametro',parametro, {withCredentials:true});
  }

  UpdateDiccionario(diccionario:any){
    return this.http.post(this.URL_API_MASIVOS + '/UpdateDiccionario',diccionario, {withCredentials:true});
  }
  
  UpdateParamtro(parametro:any){
    return this.http.post(this.URL_API_MASIVOS + '/UpdateParamtro', parametro, {withCredentials:true});
  }

  DeleteDiccionario(id:any){
    return this.http.delete(`${this.URL_API_MASIVOS}/EliminarDiccionario/${id}`, {withCredentials:true});
  }

  DeleteParametro(id:any){
    return this.http.delete(`${this.URL_API_MASIVOS}/EliminarParametro/${id}`, {withCredentials:true});
  }

  GetAllParametrosByIdDiccionarios(ids:number[]){
    return this.http.get(`${this.URL_API_MASIVOS}/GetAllParametrosByIdDiccionarios/${ids}`, {withCredentials:true});
  }
  GetAllEstadosDesgravamen(){
    return this.http.get(`${this.URL_API_MASIVOS}/GetAllEstadosDesgravamen`, {withCredentials:true});
  }

  GetAllPolizasDesgravamen(){
    return this.http.get(`${this.URL_API_MASIVOS}/GetAllPolizasDesgravamen`, {withCredentials:true});
  }

  GetAllParametrosByDiccionarioId(idDiccionarios: number[]) {
    return this.http.get(`${this.URL_API_MASIVOS}/GetAllParametrosByDiccionarioId/${idDiccionarios}`, {withCredentials:true});
  }

  AsignarParametros(parametros:Parametro[]){
    this.parametros=parametros;
    this.change.emit(this.parametros);
  }

  DevolverParametrosByIdDiccionarios(id_diccionario:number[]){
    return this.parametros.filter(parametro=>id_diccionario.find(id=> id+''===parametro.diccionario_id+''));
  }


}
