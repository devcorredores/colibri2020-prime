import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReporteQueryService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/reporte_query`;
  //URL_API_MASIVOS: string = "http://localhost:5001/api-corredores-ecofuturo/usuario";
  
  constructor(private http: HttpClient) { }

  GetWithParametrosByRol(id_rol:any){
        return this.http.get(this.URL_API_MASIVOS + '/GetWithParametrosByRol/'+id_rol, {withCredentials:true})
  }
  
  EjecutarQuery(reporte_query:any){
    return this.http.post(this.URL_API_MASIVOS + '/EjecutarQuery',reporte_query, {withCredentials:true})
  }

  ReporteCobertura(objeto:any){
    return this.http.post(this.URL_API_MASIVOS + '/ReporteCobertura',objeto, {withCredentials:true}).toPromise();
  }
    
    executeQueryCertificado(reporte_query:any){
        return this.http.post(this.URL_API_MASIVOS + '/executeQueryCertificado/',reporte_query, {withCredentials:true}).toPromise();
    }
    executeQueryPlanPago(reporte_query:any){
        return this.http.post(this.URL_API_MASIVOS + '/executeQueryPlanPago/',reporte_query, {withCredentials:true}).toPromise();
    }
    executeQueryCrediticios(reporte_query:any){
        return this.http.post(this.URL_API_MASIVOS + '/executeQueryCrediticios/',reporte_query, {withCredentials:true}).toPromise();
    }
    executeQuerySolicitud(reporte_query:any){
        return this.http.post(this.URL_API_MASIVOS + '/executeQuerySolicitud/',reporte_query, {withCredentials:true}).toPromise();
    }
    executeQueryOrdenPago(reporte_query:any){
        return this.http.post(this.URL_API_MASIVOS + '/executeQueryOrdenPago/',reporte_query, {withCredentials:true}).toPromise();
    }
    executeQueryAnulacion(reporte_query:any){
        return this.http.post(this.URL_API_MASIVOS + '/executeQueryAnulacion/',reporte_query, {withCredentials:true}).toPromise();
    }
    executeQueryCarta(reporte_query:any){
        return this.http.post(this.URL_API_MASIVOS + '/executeQueryCarta/',reporte_query, {withCredentials:true}).toPromise();
    }
}
