import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InstanciaAnexoService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/instancia_anexo_asegurado`;
  
  constructor(private http: HttpClient) { } 
  
  SaveOrUpdateInstancia_anexo_asegurado(instancia_anexo_asegurado: any) {
    return this.http.post(this.URL_API_MASIVOS + '/SaveOrUpdateInstancia_anexo_asegurado',instancia_anexo_asegurado, {withCredentials:true});
  }

  Eliminar(instancia_anexo_asegurado: any) {
    return this.http.post(this.URL_API_MASIVOS + '/Eliminar',instancia_anexo_asegurado, {withCredentials:true});
  }
}
