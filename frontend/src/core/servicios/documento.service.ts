import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentoService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/documento`;
    
    constructor(
      private http: HttpClient,
  ) { }
  
  listaDocumentosByIdPoliza(idPoliza: number) {
    return this.http.get(this.URL_API_MASIVOS + '/listaDocumentosByIdPoliza/' + idPoliza, {withCredentials:true});
  }
}
