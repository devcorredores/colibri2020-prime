import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { WsSoap } from '../modelos/ws-soap';
import * as app from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WsSoapService {

  URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/soap-ui`;

  constructor(private http: HttpClient) { }
  

  solicitarDatos(request:WsSoap){
    // const vtimeout = Number(app.settings.httpTimeout);
    // const token = AccesoToken.getUsuariosStore().token;
    // const headersObject = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.post(this.URL_API_MASIVOS,request, { headers: headersObject }).pipe(timeout(vtimeout));;
    return this.http.post(this.URL_API_MASIVOS,request, {withCredentials:true});
  }


}
