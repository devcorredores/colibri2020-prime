import { TestBed } from '@angular/core/testing';

import { PlanPagoService } from './plan-pago.service';

describe('PlanPagoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanPagoService = TestBed.get(PlanPagoService);
    expect(service).toBeTruthy();
  });
});
