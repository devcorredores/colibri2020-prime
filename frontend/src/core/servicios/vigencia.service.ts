import { Injectable } from '@angular/core';
import * as app from '../../environments/environment';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class VigenciaService {
    
    URL_API_MASIVOS: string = `${app.environment.URL_API_MASIVOS}/vigencia`;
    
    constructor(private http: HttpClient) {
    }
    
    verificaRenovandoSolicitud(idPoliza:number, idInstanciaPoliza:number, idEstado:number, gracia:number, manual:boolean, idSucursal:number = null, idAgencia:number= null) {
        if (idSucursal && idAgencia) {
            return this.http.get(this.URL_API_MASIVOS + '/verificaRenovandoSolicitud/' + idPoliza + '/' + idInstanciaPoliza + '/' + idEstado + '/' + gracia + '/' + manual + '/' + idSucursal + '/' + idAgencia, {withCredentials: true});
        } else {
            return this.http.get(this.URL_API_MASIVOS + '/verificaRenovandoSolicitud/' + idPoliza + '/' + idInstanciaPoliza + '/' + idEstado + '/' + gracia + '/' + manual, {withCredentials: true});
        }
    }
}
