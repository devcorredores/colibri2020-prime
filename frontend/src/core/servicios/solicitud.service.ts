import { Injectable } from "@angular/core";
import { Asegurado } from "../modelos/asegurado";
import { Anexo_poliza } from "../modelos/anexo_poliza";
import { ParametrosService } from "./parametro.service";
import { PolizaService } from "./poliza.service";
import { PersonaService } from "./persona.service";
import { AtributoService } from "./atributo.service";
import { UsuariosService } from "./usuarios.service";
import { ObjetoAtributoService } from "./objetoAtributo.service";
import { Message, MessageService, SelectItem } from "primeng/api";
import {Parametro, ParametroRuteo} from "../modelos/parametro";
import { Objeto_x_atributo } from "../modelos/objeto_x_atributo";
import { Atributo_instancia_poliza } from "../modelos/atributo_instancia_poliza";
import { Documento } from "../modelos/documento";
import { Objeto } from "../modelos/objeto";
import { Instancia_poliza_transicion } from "../modelos/instancia_poliza_transicion";
import { isArray, isObject, isString } from "util";
import { FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { DocumentoService } from "./documento.service";
import { InstanciaPolizaTransService } from "./instancia-poliza-trans.service";
import { persona_banco_account } from "../modelos/persona_banco_account";
import {
    Opcion,
    persona_banco_datos,
    Pregunta_cerrada,
    Respuesta
} from "../modelos/persona_banco_datos";
import { persona_banco_tarjeta_credito } from "../modelos/persona_banco_tarjeta_credito";
import { persona_banco_tarjeta_debito } from "../modelos/persona_banco_tarjeta_debito";
import { Poliza } from "../modelos/poliza";
import { MenuService } from "./menu.service";
import { Sucursal } from "../modelos/sucursal";
import { Usuario_Banco } from "../modelos/usuario_banco";
import { Agencia } from "../modelos/agencia";
import { Usuario } from "../modelos/usuario";
import { Rol } from "../modelos/rol";
import { Usuario_x_rol } from "../modelos/usuario_x_rol";
import { SoapuiService } from "./soapui.service";
import { RolesService } from "./rol.service";
import { Instancia_documento } from "../modelos/instancia_documento";
import { InstanciaPolizaService } from "./instancia-poliza.service";
import { Beneficiario } from "../modelos/beneficiario";
import { ReporteService } from "./reporte.service";
import {
    persona_banco_certificado,
    persona_banco_transaccion,
    persona_banco_transaccion_debito_csg
} from "../modelos/persona_banco_transaccion";
import { Router } from "@angular/router";
import { persona_banco_pep } from "../modelos/persona_banco_pep";
import { VigenciaService } from "./vigencia.service";
import { Anexo_asegurado } from "../modelos/anexo_asegurado";
import {persona_banco_datos_tarjeta} from "../modelos/persona_banco_datos_tarjeta";
import "../../../src/helpers/prototypes";
import {LoginService} from "./login.service";
import * as moment from "moment";
import {SessionStorageService} from "./sessionStorage.service";
import {BreadcrumbService} from "./breadcrumb.service";
import {Instancia_poliza} from "../modelos/instancia_poliza";
import {ReporteQueryService} from "./reporte-query.service";
import {persona_banco, persona_banco_solicitud} from "../modelos/persona_banco";
import { cuenta, persona_banco_tarjeta_debito_cuentas_vinculadas, TarjetaDebito} from "../modelos/persona_banco_tarjeta_debito_cuentas_vinculadas";
import {persona_banco_tarjeta_credito_cuentas_asociadas} from "../modelos/persona_banco_tarjeta_credito_cuentas_asociadas";
import {Perfil_x_Componente} from "../modelos/componente";
import {persona_banco_operacion} from "../modelos/persona_banco_operacion";
import {Upload} from "../modelos/upload";
import {persona_banco_tarjeta_ci_ext} from "../modelos/persona_banco_tarjeta_ci_ext";
import {BeneficiarioComponent} from "../componentes/beneficiario/beneficiario.component";
import {Perfil} from "../modelos/perfil";
import {Atributo_instancia_documento} from "../modelos/atributo_instancia_documento";
declare var $: any;
import {Util} from "../../helpers/util";
const util = new Util();

@Injectable({
    providedIn: "root",
})
export class SolicitudService {

  currentPerfil: Perfil;
  userInfo: Usuario;
    anexoEdades: number[] = [];
    usuarioAdicionado: Usuario;
    usuarioModificado: Usuario;
    isRenovated: boolean = false;
    instaciaRenovadaFromInstanciaId: number;
    usuarioModificadoHasRolCajero: boolean;
    usuarioAdicionadoHasRolCajero: boolean;
  persona_banco_beneficiario: persona_banco;
  persona_banco_datos_tarjeta: persona_banco_datos_tarjeta = new persona_banco_datos_tarjeta();
    product: string = "";
  nuevo: boolean = true;
  showOrdenPago = true;
    parametrosYAtributos: any = {};
    showCodAgenda = true;
    showOpcionDiabetes = true;
    showOpcionInsuficienciaRenal = true;
    showOpcionCovid19 = true;
    showOpcionSida = true;
    showOpcionEnfermedadesCorazon = true;
    showOpcionEnferemedadesCerebroVasculares = true;
    showOpcionEnfermedadesPulmonarCrónicaObstructiva = true;
    showOpcionSI = true;
    showOpcionNO = true;
    showOpcionCancer = true;
    showPregunta1 = true;
    showPregunta2 = true;
    showNroCuenta = true;
    showPagoEfectivo = true;
    showFormaPago = true;
    showFechaActivacion = true;
    showFinVigencia = true;
    showEstadoTarjeta = true;
    showTipoCredito = true;
    showCaedec = true;
    showLocalidad = true;
    showDepartamento = true;
    showCodSucursal = true;
    showTipoDocumento = true;
    showEmail = true;
    showEstadoCivil = true;
    showManejo = true;
    showMoneda = true;
    lookIntoIds = true;
    showDescCaedec = true;
    showTarjetaNro = true;
    showTarjetaNombre = true;
    showTarjetaValida = true;
    showModalidadPago = true;
  showLugarNacimiento = true;
  showIdTarjeta = true;
  showOperacionOficial = true;
  showTipoCuenta = true;
    showProductoAsociado = true;
    showTipoSeguro = true;
    showNroSolicitudSci= true;
    showNroCuotas = true;
    showMonto = true;
    showSolicitudEstadoSci = true;
    showSolicitudPlazoCredito = true;
    showSolicitudMoneda = true;
    showSolicitudFrecuenciaPlazo = true;
    showSolicitudPrimaTotal = true;
    showDescOcupacion = true;
    showZona = true;
    showNroDireccion = true;
    showRazonSocial = true;
    showNitCarnet = true;
    showTarjetaUltimosCuatroDigitos = true;
    showCuentaFechaExpiracion = true;
    showOcupacion = true;
    showPlan = true;
    showPlazo = true;
    showBtnImprimirCarta = false;
    showBtnImprimirDesistimiento = false;
    showSucursal = true;
    showAgencia = true;
    showUsuarioCargo = true;
    showPrima = true;
    showPrimaTotal = true;
    showTelefonoCelular = true;
    showCiudadNacimiento = true;
    showCondicionPep = true;
    showCargoEntidadPep = true;
    showPeriodoCargoPep = true;
    showDireccionLaboral = true;
    showNroTransaccion = true;
    showTransaccionMoneda = true;
    showTransaccionDebitoCSGEstado = true;
    showTransaccionCodigoVia = true;
    showTransaccionDetalle = true;
    showTransaccionImporte = true;
    showTransaccionEstado = true;
    showTransaccionFecha = true;
    showSecDatosTarjeta = true;
    showSecRegistroTitular = true;
    showSecFormaPago = true;
    showSecImpuestos = true;
    showSecCuestionario = false;
    showSecArchivos = true;
    showProvincia: Boolean = true;
    showDireccionDomicilio = true;
    showPaisNacimiento = true;
    showTelefonoDomicilio = true;
    showTelefonoTrabajo = true;
    showPaterno = true;
    showMaterno = true;
    showApCasada = true;
    showOperacionTipoCredito = true;
    showPrimerNombre = true;
    showSegundoNombre = true;
    showPrimerApellido = true;
    showSegundoApellido = true;
    showDocId = true;
    showDocIdComplemento = true;
    showDocIdExt = true;
    showSexoId = true;
    showFechaNac = true;
    showPlanPago = true;
  stopSavingByEdad: boolean = false;
  stopSavingByLastName: boolean = false;
  btnImprimirSolicitudEnabled = false;
  btnEnviarDocumentosEnabled = false;

    editCodAgenda = true;
    editPaterno = true;
    editMaterno = true;
    editPrimerNombre = true;
    editSegundoNombre = true;
    editPrimerApellido = true;
    editSegundoApellido = true;
    editDocId = true;
    editDocIdComplemento = true;
    editApCasada = true;
    editNroCuenta = true;
    editNroCuotas = true;
    editMonto = true;
  editSolicitudEstadoSci = true;
  editSolicitudPlazoCredito = true;
  editSolicitudPrimaTotal= true;
  editOperacionTipoCredito= true;
  editSolicitudMoneda = true;
    editDescOcupacion = true;
    editPagoEfectivo = true;
    editFormaPago = true;
    editCaedec = true;
    editLocalidad = true;
    editDepartamento = true;
    editCodSucursal = true;
    editTipoDocumento = true;
    editEmail = true;
    editEstadoCivil = true;
    editManejo = true;
    editMoneda = true;
    editDescCaedec = true;
    editTarjetaNro = true;
    editCondicionPep = true;
    editPeriodoCargoPep = true;
    editCargoEntidadPep = true;
    editDireccionLaboral = true;
    editTarjetaNombre = true;
    editTarjetaValida = true;
    editModalidadPago = true;
    editTipoCuenta = true;
    editTipoSeguro = true;
    editTarjetaEstado = true;
    editNroSolicitudSci = true;
    editProductoAsociado = true;
    editPrimaTotal = true;
    editZona = true;
  editProvincia: boolean = true;
  editNroDireccion = true;
    editDireccionDomicilio = true;
    editRazonSocial = true;
    editNitCarnet = true;
    editTarjetaUltimosCuatroDigitos = true;
    editCuentaFechaExpiracion = true;
    editTarjetaFechaActivacion = true;
    editTarjetaFechaFinVigencia = true;
    editOcupacion = true;
    editPlan = true;
    editPlazo = true;
    editSucursal = true;
    editAgencia = true;
    editPrima = true;
    editTelefonoCelular = true;
    editTelefonoDomicilio = true;
    editTelefonoTrabajo = true;
    editCiudadNacimiento = true;
    editLugarNacimiento= true;
    editPaisNacimiento = true;
    editSecDatosTarjeta = true;
    editSecImpuestos = true;
    editSecRegistroTitular = true;
    editSecFormaPago = true;
    editDocIdExt = true;
    editSexoId = true;
    editTarjetaTipo = true;
    editFechaNac = true;
    btnRenovarEstado: boolean = false;
    pagoACredito: boolean = false;
    scollapsedDatosTitular: boolean = false;

    ProcedenciaCI: SelectItem[] = [];
    SexosId: any[] = [];
    Sexos: SelectItem[] = [];
    SexosCod: SelectItem[] = [];
    Paises: SelectItem[] = [];
    Planes: SelectItem[] = [];
    planes: Anexo_poliza[] = [];
    anexoAsegurado: Anexo_asegurado;
    TiposDocumentos: SelectItem[] = [];
    NrosTarjetasDebitos: SelectItem[] = [];
    tarjetasDebitos: TarjetaDebito[] = [];
    Condiciones: SelectItem[] = [];
    FormasPago: SelectItem[] = [];
    CondicionesPep: SelectItem[] = [];
    Monedas: SelectItem[] = [];
    Tipo: SelectItem[] = [];
    Estado: SelectItem[] = [];
    Parentesco: SelectItem[] = [];
    EstadosInstaciaPoliza: SelectItem[] = [];
    EstadosDeudor: SelectItem[] = [];
    EstadosInstaciaPolizaAbr: SelectItem[] = [];
  EstadosDeudorAbr: SelectItem[] = [];
    Nacionalidades: SelectItem[] = [];
    estadosPoliza: Parametro[] = [];
    Periodicidad: SelectItem[] = [];
    TipoCuenta: SelectItem[] = [];
    ProductosAsociados: SelectItem[] = [];
    BeneficiarioCondiciones: SelectItem[] = [];
    BeneficiarioCondicionesAbreviacion: SelectItem[] = [];
  BeneficiarioCondicionesParametroCodParametroId: SelectItem[] = [];
  BeneficiarioCondicionesIdParametroDescripcion: any[] = [];
  Cuentas: SelectItem[] = [];
  TiposTarjeta: SelectItem[] = [];
  TiposTarjetaId: any[] = [];
  TiposTarjetaDescripcion:any[] = [];
  TiposTarjetaCod: any[] = [];
  EstadosTarjeta: SelectItem[] = [];
  EstadosTarjetaId: any[] = [];
  EstadosTarjetaDescripcion:any[] = [];
  EstadosTarjetaCod: any[] = [];
    SolicitudesSci: SelectItem[] = [];
    estadoIniciado: Parametro = new Parametro();
    estadoPorPagar: Parametro = new Parametro();
    estadoPorEmitir: Parametro = new Parametro();
    estadoSolicitado: Parametro = new Parametro();
    estadoEmitido: Parametro = new Parametro();
    estadoAnulado: Parametro;
    estadoRechazado: Parametro;
    estadoNoIncluido: Parametro;
    estadoSinVigencia: Parametro;
    estadoDesistido: Parametro;
    estadoCaducado: Parametro;
    estadoCobrado: Parametro;
    estadoPlataforma: Parametro;
    condicionSi: Parametro;
    condicionNo: Parametro;
    parMancomunada: Parametro;
    parIndividual: Parametro;
    parMensual: Parametro;
    parAnual: Parametro;
    parametroInvisible: Parametro;
    parametroNoEditable: Parametro;
    parametroEditable: Parametro;
    parametroVisible: Parametro;
    parametroNumeracionIndependiente: Parametro;
    parametroNumeracionUnificada: Parametro;
    parametroError: Parametro;
    parametroWarning: Parametro;
    parametroInfoWarning: Parametro;
    parametroInformation: Parametro;
    parametroSuccess: Parametro;
    parametroDocSolicitud: Parametro;
    parametroDocComprobante: Parametro;
    parametroDocCertificado: Parametro;
    parametroTipoSeguroEcoAccidente: Parametro;
    parametroTipoSeguroEcoResguardo: Parametro;
  parametroRechazado: Parametro;
  parametroIncluido: Parametro;
  parametroNoIncluido: Parametro;
    TiposDocumentosAbreviacion:any [] = [];
    TiposDocumentosCodId:any [] = [];
    TiposDocumentosParametroCod:any [] = [];
    TiposDocumentosId:any [] = [];
    NacionalidadesAbreviacion:any [] = [];
    NacionalidadesParametroCod:any [] = [];
    PaisesAbreviacion:any [] = [];
    PaisesParametroCod:any [] = [];
    PaisesId:any [] = [];
    EstadosInstaciaPolizaId:any [] = [];
    EstadosDeudorId:any [] = [];
    EstadosInstaciaPolizaAbreviacion:any [] = [];
    EstadosDeudorAbreviacion:any [] = [];
    EstadosDeudorParCodigo:any [] = [];
    SexosAbreviacion:any [] = [];
    ProcedenciaCIAbreviacion:any [] = [];
    SexosParametroCod:any [] = [];
    ProcedenciaCIParametroCod:any [] = [];
    ProcedenciaCIParametroCodDescripcion:any [] = [];
    ParentescoAbreviacion:any [] = [];
    CondicionesId:any [] = [];
    CondicionesIdDesc:any [] = [];
    CondicionesCod:any [] = [];
    FormasPagoId:any [] = [];
    FormasPagoIdDesc:any [] = [];
    FormasPagoCod:any [] = [];
    MonedasAbreviacion:any [] = [];
    MonedasParametroCod:any [] = [];
    MonedasDescripcion:any [] = [];
    MonedasDescripcionParametroCod:any [] = [];
    MonedasStrParametroCod:any [] = [];
    MonedasId:any [] = [];
    parametrosMonedas: Parametro[] = [];
    EstadoAbreviacion:any [] = [];
    TipoAbreviacion:any [] = [];
    PeriodicidadAbreviacion:any [] = [];
    ProductosAsociadosAbreviacion:any [] = [];
    ObservacionesId:any [] = [];
    estadosObservaciones:Parametro[] = [];
    SucursalesParametroCod:any [] = [];
    SucursalesParametroCodParametroId:any [] = [];
    AgenciasParametroCod:any [] = [];
    Sucursales: SelectItem[] = [];
    Agencias: SelectItem[] = [];
    PlanesParametroCod:any [] = [];

    msgs_error: Message[] = [];
    msgs_warn: Message[] = [];
    msgs_info_warn: Message[] = [];
    msgs_warn_error: Message[] = [];
    tran_info: Message[] = [];
    PeriodicidadId:any [] = [];
    TipoCuentaAbreviacion:any [] = [];

    ObjetoAtributos: Objeto_x_atributo[] = [];
    ObjetoAtributosDoc: Objeto_x_atributo[] = [];
    showImprimirSolicitud = true;
    showRefrescarInformacion = true;
    displayErrorPlanPago = true;
    idObjetoCurrent = 0;
  displayEdadIncorrecta: boolean = false;
  minlengthCelular:number = 8;
  minLengthMsgCelular:string = 'El ccelular debe tener al menos 8 digitos';

    atributoApellidoPaternoBeneficiario: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoApellidoMaternoBeneficiario: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoApellidoPaterno: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoDestinatarios: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoApellidoMaterno: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoPrimerNombre: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoSegundoNombre: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoSexo: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoFechaNacimiento: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoDocId: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoComplementoDocId: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoDocIdExt: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoApellidoCasada: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoDireccionDomicilio: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTelefonoDomicilio: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTelefonoTrabajo: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoPaisNacimiento: Atributo_instancia_poliza = new Atributo_instancia_poliza();

    atributoNroCuenta: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoZona: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoNroDireccion: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoEmail: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTipoDoc: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoUltimosCuatroDigitos: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoRazonSocial: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoArchivo: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoNitCarnet: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoCtaFechaExpiracion: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoPlan: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoPlazo: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoAgencia: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoUsuarioCargo: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoPrima: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTelefonoCelular: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoCiudadNacimiento: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoCondicionPep: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoCargoEntidadPep: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoPeriodoCargoPublico: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoDireccionLaboral: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTipoCuenta: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoProductoAsociado: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTipoSeguro: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoSolicitudSci: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoNroCuotas: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoDescOcupacion: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoMonto: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoSolicitudSciEstado: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoSolicitudPlazoCredito: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoSolicitudMoneda: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoSolicitudFrecuenciaPlazo: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoSolicitudPrimaTotal: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoNroTransaccion: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTransaccionMoneda: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTransaccionDebitoCSGEstado: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTransaccionCodigoVia: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTransaccionImporte: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTransaccionDetalle: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTransaccionEstado: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoFechaTransaccion: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoSucursal: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoOcupacion: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoModalidadPago: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoAmpConAmbMedGeneral: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoAmpSumAmbMedicamentos: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoAmpConAmbMedEspecializada: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoAmpSumExaLaboratorio: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoNroTarjeta: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoNombreTarjeta: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoCodAgenda: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoDebitoAutomatico: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoFormaPago: Atributo_instancia_poliza = new Atributo_instancia_poliza();
  atributoLugarNacimiento: Atributo_instancia_poliza = new Atributo_instancia_poliza();
  atributoTarjetaId: Atributo_instancia_poliza = new Atributo_instancia_poliza();
  atributoOperacionOficial: Atributo_instancia_poliza = new Atributo_instancia_poliza();
  atributoEstadoTarjeta: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoTipoCredito: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoFechaActivacion: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoFechaFinVigencia: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    atributoMoneda: Atributo_instancia_poliza = new Atributo_instancia_poliza();
  atributoTipoTarjeta: Atributo_instancia_poliza = new Atributo_instancia_poliza();
  atributoProvincia: Atributo_instancia_poliza = new Atributo_instancia_poliza();
  atributoDepartamento: Atributo_instancia_poliza = new Atributo_instancia_poliza();
  atributoLocalidad: Atributo_instancia_poliza = new Atributo_instancia_poliza();

    documentosPoliza: Documento[] = [];
    documentoSolicitud: Documento = new Documento();
    objetoAseguradoDatosComplementarios: Objeto = new Objeto();
    objetoDocumentoDatosComplementarios: Objeto = new Objeto();
    objetoDocSolicitudDatosComplementarios: Objeto = new Objeto();
    objetoDocCertificadoDatosComplementarios: Objeto = new Objeto();
    objetoDocComprobanteDatosComplementarios: Objeto = new Objeto();
    objetoDocDeclaracionDatosComplementarios: Objeto = new Objeto();
    ObjetoAseguradoAtributosFiltered: Objeto_x_atributo[] = [];
    ObjetoDocSolicitudAtributosFiltered: Objeto_x_atributo[] = [];
    ObjetoDocCertificadoAtributosFiltered: Objeto_x_atributo[] = [];
    ObjetoDocComprobanteAtributosFiltered: Objeto_x_atributo[] = [];
    documentoComprobante: Documento = new Documento();
    documentoCotizacion: Documento = new Documento();
    documentoCronogramaPago: Documento = new Documento();
    documentoCertificado: Documento = new Documento();

    componentesInvisibles: Perfil_x_Componente[] = [];
    parametros: Parametro[] = [];

    doc_id:string = '';
    extension:string = '';

    persona_banco_transaccion: persona_banco_transaccion = new persona_banco_transaccion();
    persona_banco_transaccion_debito_csg: persona_banco_transaccion_debito_csg = new persona_banco_transaccion_debito_csg();
    persona_banco_certificado: persona_banco_certificado = new persona_banco_certificado();
    persona_banco_account: persona_banco_account = new persona_banco_account();
    persona_banco: persona_banco = new persona_banco();
    persona_banco_bkp: persona_banco = new persona_banco();
    persona_banco_solicitud: persona_banco_solicitud = new persona_banco_solicitud();
    persona_banco_datos: persona_banco_datos = new persona_banco_datos();
    persona_banco_operacion: persona_banco_operacion = new persona_banco_operacion();
    persona_banco_operaciones: persona_banco_operacion [] = [];
    persona_banco_pep: persona_banco_pep = new persona_banco_pep();
    persona_banco_tarjeta_credito: persona_banco_tarjeta_credito = new persona_banco_tarjeta_credito();
    persona_banco_tarjeta_ci_ext: persona_banco_tarjeta_ci_ext = new persona_banco_tarjeta_ci_ext();
    persona_banco_tarjeta_debito: persona_banco_tarjeta_debito = new persona_banco_tarjeta_debito();
    persona_banco_tarjeta_debito_cuenta_vinculada: persona_banco_tarjeta_debito_cuentas_vinculadas = new persona_banco_tarjeta_debito_cuentas_vinculadas();
    persona_banco_tarjeta_credito_cuenta_asociada: persona_banco_tarjeta_credito_cuentas_asociadas = new persona_banco_tarjeta_credito_cuentas_asociadas();
    persona_banco_tarjeta_debito_cuentas_vinculadas: persona_banco_tarjeta_debito_cuentas_vinculadas[] = [];
    persona_banco_cuentas_vinculadas: persona_banco_tarjeta_debito_cuentas_vinculadas[] = [];
    persona_banco_tarjeta_credito_cuentas_asociadas: persona_banco_tarjeta_credito_cuentas_asociadas[] = [];
    persona_banco_tarjetas_debito: persona_banco_tarjeta_debito[] = [];
    usuario_banco: Usuario_Banco;

    polizaEcoAguinaldo: Poliza;
    id_poliza: number;
    polizaEcoPasanaku: Poliza;
    polizaEcoTarjetaCredito: Poliza;
    polizaEcoTarjetaDebito: Poliza;
    polizaEcoMedic: Poliza;
    polizaEcoMedicPlus: Poliza;
    polizaEcoAccidentes: Poliza;
    polizaEcoResguardo: Poliza;
    polizaEcoVida: Poliza;
    polizaEcoProteccion: Poliza;
    poliza: Poliza = new Poliza();
    polizas: Poliza[] = [];
    displayModalSolicitudExistenteMessage: string = "";
    displayDatosTitular: boolean = false;
    displayActualizacionExitoso: boolean = false;
    collapsedFormTitular: boolean = false;
    displayEmitirSolicitudDesdeCaja: boolean = false;
    displayValidacionAlInicio: boolean = false;
    displayFormTitular: boolean = true;
    displayActualizacionPersona: boolean = false;
    displayEmitirSolicitudSinNroTransaccion: boolean = false;
    displayEmitirSolicitudDesdeCajaError: boolean = false;
    displayMigrarBeneficiarios: boolean = false;
    displayBeneficiariosDuplicados: boolean = false;
  displayRegistrandoDuplicado: boolean = false;
    displayModalDatosTitular: boolean = false;
    collapsedDatosTitular: boolean = false;
    displayBusquedaCI: boolean = true;
    displayModalSolicitudExistente: boolean = false;
    displayModalSolicitudExistenteCrear: boolean = false;
    displayModalDosSolicitudesExistente: boolean = false;
    displayModalSolicitudExistenteConRenovacion: boolean = false;
    displayModalSolicitudExistenteAlertRenovacion: boolean = false;
    displayEnviarDocumento: boolean = false;
    displaySolicitudSciInvalida: boolean = false;
    displayClienteSinCuenta: boolean = false;
    displayValidacionApellidos: boolean = false;
    displayTarjetaInvalida: boolean = false;
    envioDestinatariosExitoso: boolean = false;
    displayModalFormTitular: boolean = false;
    displaySeguroDeRefrescar: boolean = false;
    esClienteBanco: boolean = false;
    conCreditoAsociado: boolean = false;
    tieneCuentaBanco: boolean = false;
    tieneTarjeta: boolean = false;
    buscando: boolean = false;
    editandoTitular: boolean = false;
    fromGestionSolicitudes: boolean = true;
    btnCartaEnabled: boolean = true;
    btnCartaDesistimiento: boolean = false;
    isInAltaSolicitud: boolean = false;
    // displayEdadIncorrecta: boolean = false;
    hasUniqueNroTran: boolean;
    btnRefrescarInformacion = false;
    btnRefrescarInformacionEnabled = false;
  showTipoTarjeta = true;

    persona_banco_accounts: persona_banco_account[] = [];
    usuario_banco_sucursales: Sucursal[] = [];
    usuario_banco_agencias: Agencia[] = [];

    usuarioLogin: Usuario = new Usuario();
    hasRolConsultaTarjetas = false;
    hasRolConsultaCajero = false;
    hasRolAdmin = false;
    hasRolCredito = false;
    hasRolPlataforma = false;
    hasRolCajero = false;
    rolAdminPermisos1: Rol;
    rolAdminPermisos2: Rol;
    rolConsulta: Rol;
    rolSupervisor: Rol;
    rolAdminUsuarios: Rol;
    rolOficialPlataforma: Rol;
    rolOficialTarjeta: Rol;
    rolOficialCredito: Rol;
    rolOficialCajero: Rol;
    displayValidacionConObservaciones: boolean = false;
    mostrarObservaciones: boolean = false;
    mostrarAdvertencias: boolean = false;
    aseguradoBeneficiarios: Beneficiario[] = [];
    asegurados: Asegurado[] = [];

    displayValidacionMessages: boolean = false;
    displayValidacionSinObservaciones: boolean = false;
    displayValidacionInfoWarnings: boolean = false;
    emitirInstanciaPoliza: boolean = false;
    userFormHasErrors: boolean = false;
    displayEmitirSolicitud: boolean = false;
    displayMontoInvalido: boolean = false;
    displayTransaccionInvalida: boolean = false;
    displayTransaccionInvalidaSinContinuar: boolean = false;
    displayNuevoClienteSinNroCuenta: boolean = false;

    envioDocumentos:any [] = [];

    msgTransaccionInvalida: string = "";
    msgTransaccionInfo: string = "";
    msgErrorValidacionDesdeCaja: string = "";
    msgMontoInvalido: string = "";
    msgSolicitudSciInvalida: string = "";
    displayTransaccionInfo: boolean = false;
    displayErrorValidacionDesdeCaja: boolean = false;
    isLoadingAgain: Boolean = false;
    displayCambioEstadoExitoso: boolean = false;
    displayCambioEstadoExitosoDesdeCaja: boolean = false;
    displayCambioEstadoExitosoOpcional: boolean = false;
    displayErrorRespuestaBanco: boolean = false;
    destinatariosCorreos: string = "";
    anexosPoliza: Anexo_poliza[] = [];
    anexoPoliza: Anexo_poliza;
    instanciaDocumentoSolicitud: Instancia_documento = new Instancia_documento();
    fileDocumentoSolicitud: Upload = new Upload();
    fileDocumentoCertificado: Upload = new Upload();
    instanciaDocumentoComprobante: Instancia_documento = new Instancia_documento();
    instanciaDocumentoCotizacion: Instancia_documento = new Instancia_documento();
    instanciaDocumentoCronogramaPagos: Instancia_documento = new Instancia_documento();
    instanciaDocumentoCertificado: Instancia_documento = new Instancia_documento();
    estadosUpdateSolicitud: number[] = [];
    displayCambioEstadoExitosoSinOpciones: boolean = false;
    displayCambioEstadoExitosoSinOpcionesDesdeCaja: boolean = false;
    msgCambioExitoso: string = "";
    msgCambioEstadoExitoso: string = "";
    userFormHasWarnings: boolean = false;
    userFormHasInfoWarnings: boolean = false;
    personaTitular: string = "La persona";
    atributoInstanciaPolizas: Atributo_instancia_poliza[] = [];
    asegurado: Asegurado = new Asegurado();
    disableToSave: boolean = false;
    beneficiarios: Beneficiario[] = [];
    edadAsegurado: number;
    msgText: string = "";
    oldAsegurado: Asegurado = new Asegurado();
    msgsFromSolicitud = "";
    fechaEdadMinima: Date = new Date();
    fechaEdadMaxima: Date = new Date();
    paramsLoaded: boolean = false;
    btnOrdenPagoEnabled: boolean = false;
    btnCrearEcoResguardoEnabled: boolean = false;
    btnCrearEcoAccidenteEnabled: boolean = false;
    btnPlanPagoEnabled: boolean = false;
    btnValidarYContinuarEnabled: boolean = false;
    costoSeguroEcopasanaku: number = 50;
    // costoSeguroEcoaguinaldo: number = 60;
    costoSeguroEcoaguinaldo: number = 90;
    costoSeguroEcovida: number = 100;
    costoSeguroEcoMedicB: number = 100;
    costoSeguroEcoRiesgo: number = 90;
    btnEmitirCertificadoEnabled: boolean = false;
    pagoDebitAutomatico: Parametro;
    pagoEfectivo: Parametro;
    displayRenovacionEstado: boolean = false;

  parametroSinExtension: Parametro;
    parametroAsegurado: Parametro;
    parametroDay: Parametro;
    parametroMonth: Parametro;
    parametroYear: Parametro;
  parametroPlanes: Parametro;
  parametroObjetoPoliza: Parametro;
  parametroCondicionadoParticular: Parametro;
  parametroObjetoDocumento: Parametro;
  parametroBeneficiarioPrimario: Parametro;
  parametroBeneficiarioContingente: Parametro;
  parametroOtroDestinoCreditos: Parametro;
  parametroEcoconsumoEcodisponible: Parametro;
  parametroTarjetaCredito: Parametro;
  parametroPagoContado: Parametro;
  parametroTarjetaPererdida: Parametro;
  parametroTarjetaInnominada: Parametro;
  parametroTarjetaBloqueada: Parametro;
  parametroTarjetaHabilitada: Parametro;
  parametroPagoCredito: Parametro;

    uploadedFiles:Upload[] = [];
    archivoSubido: boolean = false;
    solicitudAprobada: boolean = false;
    solicitudAjena: boolean = false;
    displayVerificaArchivo: boolean = false;
    stopSavingBySolicitud: boolean = false;

    id_beneficiario: string;
    siguientePaso: string;
    tipoProducto: string = '';
  edadMinimaYears: number = 18; // 18 Años;
  edadMaximaYears: number = 65; // 65 años;
  edadMinimaMonths: number = 18; // 18 Años;
  edadMaximaMonths: number = 65; // 65 años;
  edadMinimaDays: number = 18; // 18 Años;
  edadMaximaDays: number = 70; // 70 años;
  validandoContinuando: boolean = false;
  parametrosRuteo: ParametroRuteo = new ParametroRuteo();
  ruta: string;

  displayErrorMontoCuota: boolean = false;
  stopSavingByMontoCuota:boolean = false;
  stopSaving:boolean = false;
  displayNoPuedeCrearEcoResguardo: boolean = false;
  displayNoPuedeInstrumentarProducto: boolean = false;
  MotivosAnulacion: SelectItem[] = [];
  TiposCreditoDisponible: SelectItem[] = [];
    TiposCreditoConsumo: SelectItem[] = [];
  MotivosAnulacionAbreviacion: SelectItem[] = [];
    TiposCreditoConsumoAbreviacion: SelectItem[] = [];
    TiposCreditoDisponibleAbreviacion: SelectItem[] = [];
  documentoAnulacion: Documento = new Documento();
  MotivosAnulacionParametroCodParametroId: SelectItem[] = [];
    TiposCreditoConsumoParametroCodParametroId: SelectItem[] = [];
    TiposCreditoDisponibleParametroCodParametroId: SelectItem[] = [];
  paramsDeleted: any;
  numero_solicitudes: number;
  msgAlertRenew: string = "";
  msgRenew: string = "";
  lblButtonShowRequests: string = "";
  enableBtnRenuevaSolicitud: boolean = false;
  enableBtnNuevaSolicitud: boolean = false;
  strFechaFinVigencia: string = "";
  msgSolicitudExistente: string;
  displayClienteExistente: boolean = false;
  nombreServicioBanco:string = '';

  displaySolicitudTarjetaCreditoExistente: boolean = false;
  tarjetasAsegurar: any[];
  tarjetasAseguradas: any[];

  aseguradoEcoAccidente: Asegurado;
  aseguradoEcoresguardo: Asegurado;
  aseguradosEcoresguardo: Asegurado[];
  displayNuevoCliente: boolean = false;
  displayClienteSinCredito: boolean = false;
  TipoSeguros:SelectItem[] = [];

  constructor(
        private breadcrumbService: BreadcrumbService,
        private parametroService: ParametrosService,
        private polizaService: PolizaService,
        private router: Router,
        private personaService: PersonaService,
        private atributoService: AtributoService,
        private usuarioService: UsuariosService,
        private objetoAtributoService: ObjetoAtributoService,
        private documentoService: DocumentoService,
        private instanciaPolizaTransService: InstanciaPolizaTransService,
        private menuService: MenuService,
        private soapuiService: SoapuiService,
        private rolesService: RolesService,
        private instanciaPolizaService: InstanciaPolizaService,
        private usuariosService: UsuariosService,
        private reporteService: ReporteService,
        private service: MessageService,
        private loginService: LoginService,
        private vigenciaService: VigenciaService,
        private reporteQueryService: ReporteQueryService,
        private messageService: MessageService,
        private sessionStorageService: SessionStorageService
    ) {}

    enableUserform(userform:any) {
      for (var control in userform.controls) {
        if (
          control == "par_tipo_documento_id" ||
          control == "persona_direccion_domicilio" ||
          control == "persona_doc_id_comp" ||
          control == "par_pais_nacimiento_id" ||
          control == "persona_telefono_trabajo" ||
          control == "persona_telefono_domicilio" ||
          control == "par_debito_automatico_id" ||
          control == "submit" ||
          control == "par_moneda" ||
          control == "par_numero_cuenta"
        ) {
          if (userform.controls[control]) {
            userform.controls[control].enable();
          }
        }
      }
    }


  ValidarComponentesInvisible(id: any) {
    if (this.componentesInvisibles != null) {
      if (this.componentesInvisibles.find((params) => params.codigo == id && params.estado == "A")) {
        return true;
      } else {
        if (this.componentesInvisibles.find((params) => params.codigo == id && params.estado == "I")) {
          return false;
        } else {
          return true;
        }
      }
    }
  }

  async setComponentes(anexosPoliza: Anexo_poliza[], callback: Function = null) {
        this.poliza.anexo_polizas = anexosPoliza;
        this.poliza.entidad = this.asegurado.entidad;

        this.parametrosYAtributos.asegurado = this.asegurado;
        this.parametrosYAtributos.estadoIniciado = this.estadoIniciado;
        this.parametrosYAtributos.estadoSolicitado = this.estadoSolicitado;
        this.parametrosYAtributos.estadoEmitido = this.estadoEmitido;
        this.parametrosYAtributos.estadoAnulado = this.estadoAnulado;
        this.parametrosYAtributos.estadoRechazado = this.estadoRechazado;
        this.parametrosYAtributos.estadoNoIncluido = this.estadoNoIncluido;
        this.parametrosYAtributos.estadoSinVigencia = this.estadoSinVigencia;
        this.parametrosYAtributos.estadoDesistido = this.estadoDesistido;
        this.parametrosYAtributos.estadoCaducado = this.estadoCaducado;
        this.parametrosYAtributos.estadoCobrado = this.estadoCobrado;
        this.parametrosYAtributos.estadoPorPagar = this.estadoPorPagar;
        this.parametrosYAtributos.estadoPorEmitir = this.estadoPorEmitir;
        this.parametrosYAtributos.estadoPlataforma = this.estadoPlataforma;
        this.parametrosYAtributos.parametroError = this.parametroError;
        this.parametrosYAtributos.parametroWarning = this.parametroWarning;
        this.parametrosYAtributos.parametroInformation = this.parametroInformation;
        this.parametrosYAtributos.parametroSuccess = this.parametroSuccess;
        this.parametrosYAtributos.condicionSi = this.condicionSi;
        this.parametrosYAtributos.condicionNo = this.condicionNo;
        this.parametrosYAtributos.pagoDebitAutomatico = this.pagoDebitAutomatico;
        this.parametrosYAtributos.pagoEfectivo = this.pagoEfectivo;
        this.parametrosYAtributos.parMancomunada = this.parMancomunada;
        this.parametrosYAtributos.parIndividual = this.parIndividual;
        this.parametrosYAtributos.parMensual = this.parMensual;
        this.parametrosYAtributos.parAnual = this.parAnual;
        this.parametrosYAtributos.parametroEditable = this.parametroEditable;
        this.parametrosYAtributos.parametroVisible = this.parametroVisible;
        this.parametrosYAtributos.TiposDocumentos = this.TiposDocumentos;
        this.parametrosYAtributos.TiposDocumentosAbreviacion = this.TiposDocumentosAbreviacion;
        this.parametrosYAtributos.TiposDocumentosParametroCod = this.TiposDocumentosParametroCod;
        this.parametrosYAtributos.TiposDocumentosId = this.TiposDocumentosId;
        this.parametrosYAtributos.Nacionalidades = this.Nacionalidades;
        this.parametrosYAtributos.NacionalidadesAbreviacion = this.NacionalidadesAbreviacion;
        this.parametrosYAtributos.NacionalidadesParametroCod = this.NacionalidadesParametroCod;
        this.parametrosYAtributos.Paises = this.Paises;
        this.parametrosYAtributos.PaisesAbreviacion = this.PaisesAbreviacion;
        this.parametrosYAtributos.PaisesParametroCod = this.PaisesParametroCod;
        this.parametrosYAtributos.PaisesId = this.PaisesId;
        this.parametrosYAtributos.EstadosInstaciaPoliza = this.EstadosInstaciaPoliza;
        this.parametrosYAtributos.EstadosInstaciaPolizaId = this.EstadosInstaciaPolizaId;
        this.parametrosYAtributos.Sexos = this.Sexos;
        this.parametrosYAtributos.SexosCod = this.SexosCod;
        this.parametrosYAtributos.SexosAbreviacion = this.SexosAbreviacion;
        this.parametrosYAtributos.SexosParametroCod = this.SexosParametroCod;
        this.parametrosYAtributos.SexosId = this.SexosId;
        this.parametrosYAtributos.ProcedenciaCI = this.ProcedenciaCI;
        this.parametrosYAtributos.ProcedenciaCIAbreviacion = this.ProcedenciaCIAbreviacion;
        this.parametrosYAtributos.ProcedenciaCIParametroCod = this.ProcedenciaCIParametroCod;
        this.parametrosYAtributos.ProcedenciaCIParametroCodDescripcion = this.ProcedenciaCIParametroCodDescripcion;
        this.parametrosYAtributos.Parentesco = this.Parentesco;
        this.parametrosYAtributos.ParentescoAbreviacion = this.ParentescoAbreviacion;
        this.parametrosYAtributos.CondicionesPep = this.CondicionesPep;
        this.parametrosYAtributos.Condiciones = this.Condiciones;
        this.parametrosYAtributos.CondicionesPep = this.CondicionesPep;
        this.parametrosYAtributos.CondicionesId = this.CondicionesId;
        this.parametrosYAtributos.CondicionesIdDesc = this.CondicionesIdDesc;
        this.parametrosYAtributos.FormasPago = this.FormasPago;
        this.parametrosYAtributos.FormasPagoId = this.FormasPagoId;
        this.parametrosYAtributos.FormasPagoIdDesc = this.FormasPagoIdDesc;
        this.parametrosYAtributos.Monedas = this.Monedas;
        this.parametrosYAtributos.MonedasAbreviacion = this.MonedasAbreviacion;
        this.parametrosYAtributos.MonedasParametroCod = this.MonedasParametroCod;
        this.parametrosYAtributos.MonedasStrParametroCod = this.MonedasStrParametroCod;
        this.parametrosYAtributos.Estado = this.Estado;
        this.parametrosYAtributos.EstadoAbreviacion = this.EstadoAbreviacion;
        this.parametrosYAtributos.Tipo = this.Tipo;
        this.parametrosYAtributos.TipoAbreviacion = this.TipoAbreviacion;
        this.parametrosYAtributos.id_poliza = this.id_poliza;
        this.parametrosYAtributos.id_asegurado = this.asegurado.id;
        this.parametrosYAtributos.id_anexo_poliza = this.persona_banco_datos.plan ? this.persona_banco_datos.plan : null;
        this.parametrosYAtributos.persona_banco_pep = this.persona_banco_pep;
        this.parametrosYAtributos.componentes = this.componentesInvisibles;
        this.parametrosYAtributos.poliza = this.poliza;
        this.parametrosYAtributos.documentoSolicitud = this.documentoSolicitud;
        this.parametrosYAtributos.documentoCertificado = this.documentoCertificado;
        this.parametrosYAtributos.documentoComprobante = this.documentoComprobante;
        this.parametrosYAtributos.documentoCotizacion = this.documentoCotizacion;
        this.parametrosYAtributos.documentoCronogramaPago =           this.documentoCronogramaPago;
        this.parametrosYAtributos.objetoAseguradoDatosComplementarios = this.objetoAseguradoDatosComplementarios;
        this.parametrosYAtributos.objetoDocDeclaracionDatosComplementarios = this.objetoDocDeclaracionDatosComplementarios;
        this.parametrosYAtributos.ObjetoAseguradoAtributosFiltered = this.ObjetoAseguradoAtributosFiltered;
        this.parametrosYAtributos.persona_banco = this.persona_banco;
        this.parametrosYAtributos.persona_banco_accounts = this.persona_banco_accounts;
        this.parametrosYAtributos.persona_banco_account = this.persona_banco_account;
        this.parametrosYAtributos.instanciaDocumentoSolicitud = this.instanciaDocumentoSolicitud;
        this.parametrosYAtributos.instanciaDocumentoComprobante = this.instanciaDocumentoComprobante;
        this.parametrosYAtributos.instanciaDocumentoCotizacion = this.instanciaDocumentoCotizacion;
        this.parametrosYAtributos.instanciaDocumentoCronogramaPagos = this.instanciaDocumentoCronogramaPagos;
        this.parametrosYAtributos.instanciaDocumentoCertificado = this.instanciaDocumentoCertificado;
        this.parametrosYAtributos.estadosPoliza = this.estadosPoliza;
        this.parametrosYAtributos.estadosObservaciones = this.estadosObservaciones;
        this.parametrosYAtributos.ObservacionesId = this.ObservacionesId;
        this.parametrosYAtributos.usuarioLogin = this.usuarioLogin;
        this.parametrosYAtributos.hasRolConsultaTarjetas = this.hasRolConsultaTarjetas;
        this.parametrosYAtributos.hasRolConsultaCajero = this.hasRolConsultaCajero;
        this.parametrosYAtributos.hasRolAdmin = this.hasRolAdmin;

        if (typeof callback == "function") {
            callback();
        }
    }

    async setUsuarioRoles(callback: Function = null) {
        if (this.asegurado) {
            let respUsuarioAdicionado: any = await this.usuariosService
                .findUserById(this.asegurado.adicionado_por)
                .toPromise();
            let respUsuarioModificado: any = await this.usuariosService
                .findUserById(this.asegurado.modificado_por)
                .toPromise();
            if (respUsuarioAdicionado && respUsuarioAdicionado.data) {
                this.usuarioAdicionado = respUsuarioAdicionado.data;
                let rolCajeroUsuarioAdicionado =
                    this.usuarioAdicionado.usuarioRoles.find(
                        (param) => param.id == 13
                    );
                this.usuarioAdicionadoHasRolCajero = false;
                if (rolCajeroUsuarioAdicionado) {
                    this.usuarioAdicionadoHasRolCajero = true;
                }
            }
            if (respUsuarioAdicionado && respUsuarioAdicionado.data) {
                this.usuarioModificado = respUsuarioModificado.data;
                let rolCajeroUsuarioModificado =
                    this.usuarioModificado.usuarioRoles.find(
                        (param) => param.id == 13
                    );
                this.usuarioModificadoHasRolCajero = false;
                if (rolCajeroUsuarioModificado) {
                    this.usuarioModificadoHasRolCajero = true;
                }
            }
        }
        if (typeof callback == "function") {
            callback();
        }
    }

  async featureApPaternoMaternoCasada(userform:any) {
    if(this.persona_banco.paterno == '' && this.persona_banco.materno == '' && this.persona_banco.apcasada == ''){
      userform.controls['persona_primer_apellido'].setValidators([Validators.pattern("^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$")]);
      return false;
    }
    return true
  }

  async setBeneficiarios(callback: Function = null) {
        this.parametrosYAtributos.asegurado = this.asegurado;
        this.parametrosYAtributos.estadoIniciado = this.estadoIniciado;
        this.parametrosYAtributos.estadoSolicitado = this.estadoSolicitado;
        this.parametrosYAtributos.estadoEmitido = this.estadoEmitido;
        this.parametrosYAtributos.estadoAnulado = this.estadoAnulado;
        this.parametrosYAtributos.estadoRechazado = this.estadoRechazado;
        this.parametrosYAtributos.estadoNoIncluido = this.estadoNoIncluido;
        this.parametrosYAtributos.estadoSinVigencia = this.estadoSinVigencia;
        this.parametrosYAtributos.estadoDesistido = this.estadoDesistido;
        this.parametrosYAtributos.estadoCaducado = this.estadoCaducado;
        this.parametrosYAtributos.estadoCobrado = this.estadoCobrado;
        this.parametrosYAtributos.estadoPorPagar = this.estadoPorPagar;
        this.parametrosYAtributos.estadoPorEmitir = this.estadoPorEmitir;
        this.parametrosYAtributos.estadoPlataforma = this.estadoPlataforma;
        this.parametrosYAtributos.parametroError = this.parametroError;
        this.parametrosYAtributos.parametroWarning = this.parametroWarning;
        this.parametrosYAtributos.parametroInformation = this.parametroInformation;
        this.parametrosYAtributos.parametroSuccess = this.parametroSuccess;
        this.parametrosYAtributos.condicionSi = this.condicionSi;
        this.parametrosYAtributos.condicionNo = this.condicionNo;
        this.parametrosYAtributos.pagoEfectivo = this.pagoEfectivo;
        this.parametrosYAtributos.pagoDebitAutomatico = this.pagoDebitAutomatico;
        this.parametrosYAtributos.parMancomunada = this.parMancomunada;
        this.parametrosYAtributos.parIndividual = this.parIndividual;
        this.parametrosYAtributos.parMensual = this.parMensual;
        this.parametrosYAtributos.parAnual = this.parAnual;
        this.parametrosYAtributos.parametroEditable = this.parametroEditable;
        this.parametrosYAtributos.parametroVisible = this.parametroVisible;
        this.parametrosYAtributos.TiposDocumentos = this.TiposDocumentos;
        this.parametrosYAtributos.TiposDocumentosAbreviacion = this.TiposDocumentosAbreviacion;
        this.parametrosYAtributos.TiposDocumentosParametroCod = this.TiposDocumentosParametroCod;
        this.parametrosYAtributos.TiposDocumentosId = this.TiposDocumentosId;
        this.parametrosYAtributos.Nacionalidades = this.Nacionalidades;
        this.parametrosYAtributos.NacionalidadesAbreviacion = this.NacionalidadesAbreviacion;
        this.parametrosYAtributos.NacionalidadesParametroCod = this.NacionalidadesParametroCod;
        this.parametrosYAtributos.Paises = this.Paises;
        this.parametrosYAtributos.PaisesAbreviacion = this.PaisesAbreviacion;
        this.parametrosYAtributos.PaisesParametroCod = this.PaisesParametroCod;
        this.parametrosYAtributos.PaisesId = this.PaisesId;
        this.parametrosYAtributos.EstadosInstaciaPoliza = this.EstadosInstaciaPoliza;
        this.parametrosYAtributos.EstadosInstaciaPolizaId = this.EstadosInstaciaPolizaId;
        this.parametrosYAtributos.Sexos = this.Sexos;
        this.parametrosYAtributos.SexosCod = this.SexosCod;
        this.parametrosYAtributos.SexosAbreviacion = this.SexosAbreviacion;
        this.parametrosYAtributos.SexosParametroCod = this.SexosParametroCod;
        this.parametrosYAtributos.SexosId = this.SexosId;
        this.parametrosYAtributos.ProcedenciaCI = this.ProcedenciaCI;
        this.parametrosYAtributos.ProcedenciaCIAbreviacion = this.ProcedenciaCIAbreviacion;
        this.parametrosYAtributos.ProcedenciaCIParametroCod = this.ProcedenciaCIParametroCod;
        this.parametrosYAtributos.ProcedenciaCIParametroCodDescripcion = this.ProcedenciaCIParametroCodDescripcion;
        this.parametrosYAtributos.Parentesco = this.Parentesco;
        this.parametrosYAtributos.ParentescoAbreviacion = this.ParentescoAbreviacion;
        this.parametrosYAtributos.Condiciones = this.Condiciones;
        this.parametrosYAtributos.CondicionesPep = this.CondicionesPep;
        this.parametrosYAtributos.Condiciones = this.Condiciones;
        this.parametrosYAtributos.CondicionesId = this.CondicionesId;
        this.parametrosYAtributos.CondicionesIdDesc = this.CondicionesIdDesc;
        this.parametrosYAtributos.FormasPago = this.FormasPago;
        this.parametrosYAtributos.FormasPagoId = this.FormasPagoId;
        this.parametrosYAtributos.FormasPagoIdDesc = this.FormasPagoIdDesc;
        this.parametrosYAtributos.Monedas = this.Monedas;
        this.parametrosYAtributos.MonedasAbreviacion = this.MonedasAbreviacion;
        this.parametrosYAtributos.MonedasParametroCod = this.MonedasParametroCod;
        this.parametrosYAtributos.MonedasStrParametroCod = this.MonedasStrParametroCod;
        this.parametrosYAtributos.Estado = this.Estado;
        this.parametrosYAtributos.EstadoAbreviacion = this.EstadoAbreviacion;
        this.parametrosYAtributos.Tipo = this.Tipo;
        this.parametrosYAtributos.TipoAbreviacion = this.TipoAbreviacion;
        this.parametrosYAtributos.id_poliza = this.id_poliza;
        this.parametrosYAtributos.id_asegurado = this.asegurado.id;
        this.parametrosYAtributos.id_anexo_poliza = this.persona_banco_datos.plan ? this.persona_banco_datos.plan : null;
        this.parametrosYAtributos.componentes = this.componentesInvisibles;
        this.parametrosYAtributos.poliza = this.poliza;
        this.parametrosYAtributos.documentoSolicitud = this.documentoSolicitud;
        this.parametrosYAtributos.documentoCertificado = this.documentoCertificado;
        this.parametrosYAtributos.documentoComprobante = this.documentoComprobante;
        this.parametrosYAtributos.documentoCotizacion = this.documentoCotizacion;
        this.parametrosYAtributos.documentoCronogramaPago = this.documentoCronogramaPago;
        this.parametrosYAtributos.objetoAseguradoDatosComplementarios = this.objetoAseguradoDatosComplementarios;
        this.parametrosYAtributos.objetoDocDeclaracionDatosComplementarios = this.objetoDocDeclaracionDatosComplementarios;
        this.parametrosYAtributos.ObjetoAseguradoAtributosFiltered = this.ObjetoAseguradoAtributosFiltered;
        this.parametrosYAtributos.persona_banco = this.persona_banco;
        this.parametrosYAtributos.persona_banco_accounts = this.persona_banco_accounts;
        this.parametrosYAtributos.persona_banco_account = this.persona_banco_account;
        this.parametrosYAtributos.instanciaDocumentoSolicitud = this.instanciaDocumentoSolicitud;
        this.parametrosYAtributos.instanciaDocumentoComprobante = this.instanciaDocumentoComprobante;
        this.parametrosYAtributos.instanciaDocumentoCotizacion = this.instanciaDocumentoCotizacion;
        this.parametrosYAtributos.instanciaDocumentoCronogramaPagos = this.instanciaDocumentoCronogramaPagos;
        this.parametrosYAtributos.instanciaDocumentoCertificado = this.instanciaDocumentoCertificado;
        this.parametrosYAtributos.estadosPoliza = this.estadosPoliza;
        this.parametrosYAtributos.estadosObservaciones = this.estadosObservaciones;
        this.parametrosYAtributos.BeneficiarioCondiciones = this.BeneficiarioCondiciones;
        this.parametrosYAtributos.BeneficiarioCondicionesAbreviacion = this.BeneficiarioCondicionesAbreviacion;
        this.parametrosYAtributos.BeneficiarioCondicionesParametroCodParametroId = this.BeneficiarioCondicionesParametroCodParametroId;
        this.parametrosYAtributos.BeneficiarioCondicionesIdParametroDescripcion = this.BeneficiarioCondicionesIdParametroDescripcion;
        this.parametrosYAtributos.ObservacionesId = this.ObservacionesId;
        this.parametrosYAtributos.usuarioLogin = this.usuarioLogin;
        this.parametrosYAtributos.hasRolConsultaTarjetas = this.hasRolConsultaTarjetas;
        this.parametrosYAtributos.hasRolConsultaCajero = this.hasRolConsultaCajero;
        this.parametrosYAtributos.hasRolAdmin = this.hasRolAdmin;
        this.parametrosYAtributos.persona_banco_pep = this.persona_banco_pep;

        if (typeof callback == "function") {
            callback();
        }
    }

    async componentsBehavior(callback: Function = null) {
        // this.editCaedec = this.ValidarComponentesEditable();
        this.editLocalidad = this.ValidarComponentesEditable("EA_TitParLocalidad");
        // this.editEstadoCivil = this.ValidarComponentesEditable();
        // this.editCodSucursal = this.ValidarComponentesEditable();
        // this.editManejo = this.ValidarComponentesEditable();
        // this.editCodAgenda = this.ValidarComponentesEditable();
        // this.editSecDatosTarjeta = this.ValidarComponentesEditable();
        // this.editTarjetaUltimosCuatroDigitos = this.ValidarComponentesEditable();
        // this.editDescCaedec = this.ValidarComponentesEditable();
        this.editDepartamento = this.ValidarComponentesEditable("EA_TitParDepartamento");
        this.editTipoDocumento = this.ValidarComponentesEditable("EA_TitParTipoDocumentoId");
        this.editNroCuenta = this.ValidarComponentesEditable("EA_TitParNumeroCuenta");
        this.editNroCuotas = this.ValidarComponentesEditable("EA_TitNroCuotas");
        this.editMonto = this.ValidarComponentesEditable("EA_TitMonto");
        this.editFormaPago = this.ValidarComponentesEditable("EA_TitFormaPago");
        this.editSolicitudEstadoSci = this.ValidarComponentesEditable("EA_TitSolicitudEstadoSci");
        this.editSolicitudPlazoCredito = this.ValidarComponentesEditable("EA_TitSolicitudPlazoCredito");
        this.editSolicitudPrimaTotal = this.ValidarComponentesEditable("EA_TitSolicitudPrimaTotal");
        this.editOperacionTipoCredito = this.ValidarComponentesEditable("EA_TitOperacionTipoCredito");
        // this.editSolicitudPrimaTotal = true;
        this.editSolicitudMoneda = this.ValidarComponentesEditable("EA_TitSolicitudMoneda");
        this.editDescOcupacion = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitDescOcupacion") :false;
        this.editPagoEfectivo = this.ValidarComponentesEditable("EA_TitDebitoAutomatico");
        this.editMoneda = this.ValidarComponentesEditable("EA_TitParMoneda");
        this.editEmail = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaEmail") : true;
        this.editModalidadPago = this.ValidarComponentesEditable("EA_TitModalidadPago");
        this.editTipoCuenta = this.ValidarComponentesEditable("EA_TitTipoCuenta");
        this.editProductoAsociado = this.ValidarComponentesEditable("EA_TitProductoAsociado");
        // this.editPrimaTotal = this.ValidarComponentesEditable("EA_TitPrimaTotal");
        this.editPrimaTotal = true;
        this.editTipoSeguro = this.ValidarComponentesEditable("EA_TitTipoSeguro");
        this.editTarjetaEstado = this.ValidarComponentesEditable("EA_TitParTarjetaEstado");
        this.editNroSolicitudSci = this.ValidarComponentesEditable("EA_TitNroSolicitudSci");
        this.editZona = this.ValidarComponentesEditable("EA_TitZona");
        this.editNroDireccion = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitNroDireccion") : true;
        this.editDireccionDomicilio = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaDireccionDomicilio") : true;
        this.editRazonSocial = this.ValidarComponentesEditable("EA_TitRazonSocial");
        this.editNitCarnet = this.ValidarComponentesEditable("EA_TitNitCarnet");
        this.editCuentaFechaExpiracion = this.ValidarComponentesEditable("EA_TitFechaVencimientoCuenta");
        this.editTarjetaFechaActivacion = this.ValidarComponentesEditable("EA_TitTarjetaFechaActivacion");
        this.editTarjetaFechaFinVigencia = this.ValidarComponentesEditable("EA_TitTarjetaFechaFinVigencia");
        this.editOcupacion = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitOcupacion") : true;
        this.editPlan = this.ValidarComponentesEditable("EA_TitPlanes");
        this.editPlazo = this.ValidarComponentesEditable("EA_TitPlazo");
        this.editSucursal = this.ValidarComponentesEditable("EA_TitSucursal");
        this.editAgencia = this.ValidarComponentesEditable("EA_TitAgencia");
        // this.editPrima = this.ValidarComponentesEditable("EA_TitPrima");
        this.editPrima = this.ValidarComponentesEditable("EA_TitPrima");
        this.editTelefonoCelular = this.ValidarComponentesEditable("EA_TitPersonaTelefonoCelular");
        this.editTelefonoDomicilio = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaTelefonoDomicilio") : true;
        this.editTelefonoTrabajo = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaTelefonoTrabajo") : true;
        this.editCiudadNacimiento = this.ValidarComponentesEditable("EA_TitParCiudadNacimiento");
        this.editLugarNacimiento = this.ValidarComponentesEditable("EA_TitParLugarNacimiento");
        this.editProvincia = this.ValidarComponentesEditable("EA_TitParProvincia");
        this.editPaisNacimiento = this.ValidarComponentesEditable("EA_TitParPaisNacimiento");
        this.editPaterno = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPaterno") : true;
        this.editMaterno = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitMaterno") : true;
        this.editApCasada = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaApellidoCasada") : true;
        this.editPrimerNombre = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaPrimerNombre") : true;
        this.editSegundoNombre = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaSegundoNombre") : true;
        this.editPrimerApellido = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaPrimerApellido") : true;
        this.editSegundoApellido = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaSegundoApellido") : true;
        this.editDocId = this.ValidarComponentesEditable("EA_TitPersonaDocId");
        this.editDocIdComplemento = this.ValidarComponentesEditable("EA_TitPersonaDocIdComp");
        this.editDocIdExt = this.ValidarComponentesEditable("EA_TitPersonaDocIdExt");
        this.editSexoId = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitParSexoId") : true;
        this.editTarjetaTipo = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitParTarjetaTipo") : true;
        this.editFechaNac = this.esClienteBanco ? this.ValidarComponentesEditable("EA_TitPersonaFechaNacimiento") : true;
        this.editTarjetaNombre = this.ValidarComponentesEditable("EA_TitTarjetaNombre");
        this.editTarjetaValida = this.ValidarComponentesEditable("EA_TitTarjetaValida");
        this.editTarjetaUltimosCuatroDigitos = this.ValidarComponentesEditable("EA_TitTarjetaUltimosCuatroDigitos");
        this.editTarjetaNro = this.ValidarComponentesEditable("EA_TitTarjetaNro");
        this.editCondicionPep = this.ValidarComponentesEditable("EA_TitParCondicionPep");
        this.editPeriodoCargoPep = this.ValidarComponentesEditable("EA_TitParPeriodoCargoPep");
        this.editCargoEntidadPep = this.ValidarComponentesEditable("EA_TitCargoEntidadPep");
        this.editDireccionLaboral = this.ValidarComponentesEditable("EA_TitDireccionLaboral");

        this.editSecFormaPago = this.ValidarComponentesEditable("EA_SecFormaPago");
        this.editSecDatosTarjeta = this.ValidarComponentesEditable("EA_SecDatosTarjeta");
        this.editSecImpuestos = this.ValidarComponentesEditable("EA_SecImpuestos");
        this.editSecRegistroTitular = this.ValidarComponentesEditable("EA_SecRegistroTitular");

        // this.showCodAgenda = util.ValidarComponentesInvisible(this.componentesInvisibles,);
        // this.showCaedec = util.ValidarComponentesInvisible(this.componentesInvisibles,);
        // this.showLocalidad = util.ValidarComponentesInvisible(this.componentesInvisibles,);
        // this.showCodSucursal = util.ValidarComponentesInvisible(this.componentesInvisibles,);
        // this.showEstadoCivil = util.ValidarComponentesInvisible(this.componentesInvisibles,);
        // this.showManejo = util.ValidarComponentesInvisible(this.componentesInvisibles,);
        // this.showDescCaedec = util.ValidarComponentesInvisible(this.componentesInvisibles,);

        this.showPregunta1 = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitPregunta1');
        this.showPregunta2 = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitPregunta2');

        this.showOpcionCancer = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionCancer');
        this.showOpcionDiabetes = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionDiabetes');
        this.showOpcionInsuficienciaRenal = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionInsuficienciaRenal');
        this.showOpcionCovid19 = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionCovid19');
        this.showOpcionSida = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionSida');
        this.showOpcionEnfermedadesCorazon = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionEnfermedadesCorazon');
        this.showOpcionEnferemedadesCerebroVasculares = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionEnferemedadesCerebroVasculares');
        this.showOpcionEnfermedadesPulmonarCrónicaObstructiva = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionEnfermedadesPulmonarCrónicaObstructiva');
        this.showOpcionSI = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionSI');
        this.showOpcionNO = util.ValidarComponentesInvisible(this.componentesInvisibles, 'EA_TitOpcionNO');

      this.showDepartamento = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParDepartamento");
        this.showLocalidad = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParLocalidad");
        this.showSecFormaPago = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_SecFormaPago");
        this.showNroCuenta = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParNumeroCuenta");
        this.showPagoEfectivo = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitDebitoAutomatico");
        this.showTipoDocumento = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParTipoDocumentoId");
        this.showEmail = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaEmail");
        this.showMoneda = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParMoneda");
        this.showFormaPago = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitFormaPago");
        this.showTarjetaNro = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitTarjetaNro");
        this.showTarjetaNombre = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitTarjetaNombre");
        this.showTarjetaValida = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitTarjetaValida");
        this.showModalidadPago = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitModalidadPago");
        this.showTipoCuenta = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitTipoCuenta");
        this.showProductoAsociado = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitProductoAsociado");
        this.showTipoSeguro = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitTipoSeguro");
        this.showFechaActivacion = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitTarjetaFechaActivacion");
        this.showFinVigencia = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitTarjetaFinVigencia");
        this.showEstadoTarjeta = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParTarjetaEstado");
        this.showNroSolicitudSci = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitNroSolicitudSci");
        this.showNroCuotas = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitNroCuotas");
        this.showMonto = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitMonto");
        this.showSolicitudEstadoSci = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitSolicitudEstadoSci");
        this.showSolicitudPlazoCredito = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitSolicitudPlazoCredito");
        this.showSolicitudMoneda = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitSolicitudMoneda");
        this.showSolicitudFrecuenciaPlazo = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitSolicitudFrecuenciaPlazo");
        this.showSolicitudPrimaTotal = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitSolicitudPrimaTotal");
        this.showDescOcupacion = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitDescOcupacion");
        this.showZona = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitZona");
        this.showNroDireccion = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitNroDireccion");
        this.showRazonSocial = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitRazonSocial");
        this.showNitCarnet = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitNitCarnet");
        this.showTarjetaUltimosCuatroDigitos = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitTarjetaUltimosCuatroDigitos");
        this.showOcupacion = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitOcupacion");
        this.showPlan = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPlanes");
        this.showPlazo = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPlazo");
        this.showSucursal = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitSucursal");
        this.showAgencia = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitAgencia");
        this.showUsuarioCargo = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitUsuarioCargo");
        this.showPrima = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPrima");
        this.showPrimaTotal = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPrimaTotal");
        this.showTelefonoCelular = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaTelefonoCelular");
        this.showCiudadNacimiento = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParCiudadNacimiento");
        this.showLugarNacimiento = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParLugarNacimiento");
        this.showProvincia = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParProvincia");
        this.showCondicionPep = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitCondicionPep");
        this.showPeriodoCargoPep = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPeriodoCargoPep");
        this.showDireccionLaboral = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitDireccionLaboral");
        this.showCargoEntidadPep = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitCargoEntidadPep");
        this.showCuentaFechaExpiracion = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitFechaVencimientoCuenta");
        this.showDireccionDomicilio = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaDireccionDomicilio");
        this.showPaisNacimiento = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParPaisNacimiento");
        this.showTelefonoDomicilio = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaTelefonoDomicilio");
        this.showTelefonoTrabajo = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaTelefonoTrabajo");
        this.showPaterno = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPaterno");
        this.showMaterno = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitMaterno");
        this.showApCasada = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaApellidoCasada");
        this.showOperacionTipoCredito = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitOperacionTipoCredito");
        this.showPrimerNombre = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaPrimerNombre");
        this.showSegundoNombre = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaSegundoNombre");
        this.showPrimerApellido = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaPrimerApellido");
        this.showSegundoApellido = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaSegundoApellido");
        this.showDocId = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaDocId");
        this.showDocIdComplemento = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaDocIdComp");
        this.showDocIdExt = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaDocIdExt");
        this.showSexoId = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitParSexoId");
        this.showFechaNac = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_TitPersonaFechaNacimiento");
        this.showSecDatosTarjeta = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_SecDatosTarjeta");
        this.showSecRegistroTitular = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_SecRegistroTitular");
        this.showSecFormaPago = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_SecFormaPago");
        this.showSecImpuestos = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_SecImpuestos");
        this.showSecCuestionario = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_SecCuestionario");
        this.showSecArchivos = util.ValidarComponentesInvisible(this.componentesInvisibles, "EA_SecArchivos");

        if (!this.showSecDatosTarjeta) {
            this.showTarjetaNombre = false;
            this.showTarjetaNro = false;
            this.showTarjetaUltimosCuatroDigitos = false;
            this.showTarjetaValida = false;
        }
        if (!this.showSecFormaPago) {
            this.showNroCuenta = false;
            this.showMoneda = false;
            this.showPagoEfectivo = false;
            this.showModalidadPago = false;
            this.showTipoCuenta = false;
            this.showProductoAsociado = false;
            this.showTipoSeguro = false;
            this.showNroSolicitudSci = false;
            this.showNroCuotas = false;
            this.showMonto = false;
        }

        if (typeof callback == "function") {
            callback();
        }
    }

  async componentsBehaviorByRol(callback: Function = null) {
    this.showTarjetaNombre = this.ValidarComponentesInvisible("EA_TitTarjetaNombre");
    this.showTarjetaUltimosCuatroDigitos = this.ValidarComponentesInvisible("EA_TitTarjetaUltimosCuatroDigitos");
    this.showTarjetaValida = this.ValidarComponentesInvisible("EA_TitTarjetaValida");
    this.showModalidadPago = this.ValidarComponentesInvisible("EA_TitModalidadPago");
    this.showTipoTarjeta = this.ValidarComponentesInvisible("EA_TitTipoTarjeta");
    this.showAgencia = this.ValidarComponentesInvisible("EA_TitAgencia");
    this.showUsuarioCargo = this.ValidarComponentesInvisible("EA_TitUsuarioCargo");
    this.showSucursal = this.ValidarComponentesInvisible("EA_TitSucursal");
    this.showPagoEfectivo = this.ValidarComponentesInvisible("EA_TitDebitoAutomatico");
    this.showRazonSocial = this.ValidarComponentesInvisible("EA_TitRazonSocial");
    this.showTarjetaNro = this.ValidarComponentesInvisible("EA_TitTarjetaNro");
    this.showNitCarnet = this.ValidarComponentesInvisible("EA_TitNitCarnet");
    this.showNroCuenta = this.ValidarComponentesInvisible("EA_TitParNumeroCuenta");
    this.showMoneda = this.ValidarComponentesInvisible("EA_TitParMoneda");

    this.showSecDatosTarjeta = this.ValidarComponentesInvisible("EA_SecDatosTarjeta");
    this.showSecFormaPago = this.ValidarComponentesInvisible("EA_SecFormaPago");

    if (!this.showSecDatosTarjeta) {
      this.showTarjetaNombre = false;
      this.showTarjetaNro = false;
      this.showTarjetaUltimosCuatroDigitos = false;
      this.showTarjetaValida = false;
    }
    if (!this.showSecFormaPago) {
      this.showNroCuenta = false;
      this.showMoneda = false;
      this.showPagoEfectivo = false;
      this.showModalidadPago = false;
    }

    if (typeof callback == "function") {
      callback();
    }
  }

  async validarFechaNacimiento(userform:FormGroup, callback:any = null) {
    let fechaMin = this.getFechaMinimaSegunEdad();
    let fechaMax = this.getFechaMaximaSegunEdad();
    let edadYears, edadMonths, edadDays, edadMiliseconds;
    if (this.asegurado.entidad.persona.persona_fecha_nacimiento) {
      edadMiliseconds = new Date().getTime() - this.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
      edadYears = moment().diff(this.asegurado.entidad.persona.persona_fecha_nacimiento, "years");
      edadMonths = moment().diff(this.asegurado.entidad.persona.persona_fecha_nacimiento, "months");
      edadDays = moment().diff(this.asegurado.entidad.persona.persona_fecha_nacimiento, "days");
      if (fechaMin && fechaMax) {
        //let minTime = fechaMin.getTime();
        // 567648000000 = 18 años
        let edadMinimaMiliseconds, edadMaximaMiliseconds;
        switch (this.anexoAsegurado.id_edad_unidad) {
          case this.parametroYear.id:
            edadMinimaMiliseconds = this.edadMinimaYears * 31556900000;
            edadMaximaMiliseconds = this.edadMaximaYears * 31556900000;
            this.edadAsegurado = edadYears;
            break;
          case this.parametroMonth.id:
            edadMinimaMiliseconds = this.edadMinimaYears * 2629750000;
            edadMaximaMiliseconds = this.edadMaximaYears * 2629750000;
            this.edadAsegurado = edadYears;
            break;
          case this.parametroDay.id:
            edadMinimaMiliseconds = this.edadMinimaYears * 86400000;
            edadMaximaMiliseconds = this.edadMaximaYears * 86400000;
            this.edadAsegurado = edadYears;
            break;
        }
        switch (this.anexoAsegurado.id_edad_unidad) {
          case this.parametroYear.id:
            this.edadAsegurado = edadYears;
            if (edadYears < this.edadMinimaYears) {
              this.atributoFechaNacimiento.tipo_error = `La persona es menor de ${this.edadMinimaYears} ${this.anexoAsegurado.edad_unidad.parametro_descripcion}`;
              // this.isLoadingAgain = false;
              userform.controls["persona_fecha_nacimiento"].setErrors([Validators.min(fechaMin.getTime())]);
              this.stopSavingByEdad = true;
              if (!this.validandoContinuando) {
                this.displayEdadIncorrecta = true;
              }
              return "";
            } else if (edadYears >= this.edadMaximaYears) {
              this.atributoFechaNacimiento.tipo_error = `La edad de la persona es mayor o igual a ${this.edadMaximaYears} ${this.anexoAsegurado.edad_unidad.parametro_descripcion}`;
              // this.isLoadingAgain = false;
              userform.controls["persona_fecha_nacimiento"].setErrors([Validators.max(fechaMax.getTime())]);
              this.stopSavingByEdad = true;
              if (!this.validandoContinuando) {
                this.displayEdadIncorrecta = true;
              }
              return "";
            } else {
              userform.controls["persona_fecha_nacimiento"].clearAsyncValidators();
              userform.controls["persona_fecha_nacimiento"].setErrors(null);
              this.stopSavingByEdad = false;
            }
            break;
          case this.parametroMonth.id:
            this.edadAsegurado = edadMonths;
            if (edadMonths < this.edadMinimaMonths) {
              this.atributoFechaNacimiento.tipo_error = `La persona es menor de ${this.edadMinimaMonths} ${this.anexoAsegurado.edad_unidad.parametro_descripcion}`;
              // this.isLoadingAgain = false;
              userform.controls["persona_fecha_nacimiento"].setErrors([Validators.min(fechaMin.getTime())]);
              this.stopSavingByEdad = true;
              if (!this.validandoContinuando) {
                this.displayEdadIncorrecta = true;
              }
              return "";
            } else if (edadMonths >= this.edadMaximaMonths) {
              this.atributoFechaNacimiento.tipo_error = `La edad de la persona es mayor o igual a ${this.edadMaximaMonths} ${this.anexoAsegurado.edad_unidad.parametro_descripcion}`;
              // this.isLoadingAgain = false;
              userform.controls["persona_fecha_nacimiento"].setErrors([Validators.max(fechaMax.getTime())]);
              this.stopSavingByEdad = true;
              if (!this.validandoContinuando) {
                this.displayEdadIncorrecta = true;
              }
              return "";
            } else {
              userform.controls["persona_fecha_nacimiento"].clearAsyncValidators();
              userform.controls["persona_fecha_nacimiento"].setErrors(null);
              this.stopSavingByEdad = false;
            }
            break;
          case this.parametroDay.id:
            this.edadAsegurado = edadDays;
            if (edadDays < this.edadMinimaDays) {
              this.atributoFechaNacimiento.tipo_error = `La persona es menor de ${this.edadMinimaDays} ${this.anexoAsegurado.edad_unidad.parametro_descripcion}`;
              // this.isLoadingAgain = false;
              userform.controls["persona_fecha_nacimiento"].setErrors([Validators.min(fechaMin.getTime())]);
              this.stopSavingByEdad = true;
              if (!this.validandoContinuando) {
                this.displayEdadIncorrecta = true;
              }
              return "";
            } else if (edadDays >= this.edadMaximaDays) {
              this.atributoFechaNacimiento.tipo_error = `La edad de la persona es mayor o igual a ${this.edadMaximaDays} ${this.anexoAsegurado.edad_unidad.parametro_descripcion}`;
              // this.isLoadingAgain = false;
              userform.controls["persona_fecha_nacimiento"].setErrors([Validators.max(fechaMax.getTime())]);
              this.stopSavingByEdad = true;
              if (!this.validandoContinuando) {
                this.displayEdadIncorrecta = true;
              }
              return "";
            } else {
              userform.controls["persona_fecha_nacimiento"].clearAsyncValidators();
              userform.controls["persona_fecha_nacimiento"].setErrors(null);
              this.stopSavingByEdad = false;
            }
            break;
          default:
            this.atributoFechaNacimiento.tipo_error = `La póliza ${this.poliza.descripcion} no tiene resgitrado la unidad del rango de edad admitido, por favor contáctese con el administrador`;
            // this.isLoadingAgain = false;
            userform.controls[
              "persona_fecha_nacimiento"
              ].setErrors([
              Validators.max(fechaMax.getTime()),
              Validators.min(fechaMin.getTime()),
            ]);
            this.stopSavingByEdad = true;
            if (!this.validandoContinuando) {
              this.displayEdadIncorrecta = true;
            }
        }
      }
    }
    if (typeof callback == 'function') {
      callback();
    }
  }

  async validarMonto(callback) {
    let cuota = this.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.atributoNroCuotas.objeto_x_atributo.id_atributo);
    let monto = this.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.atributoMonto.objeto_x_atributo.id_atributo);
    let modalidad = this.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.atributoModalidadPago.objeto_x_atributo.id_atributo);
    if(modalidad && modalidad.valor == this.parMensual.id+'') {
      if(cuota.valor != '12' && monto.valor != '16') {
        this.displayErrorMontoCuota = true;
        this.stopSavingByMontoCuota = true;
      }
    } else if(modalidad && modalidad.valor == this.parAnual.id+'') {
      if(cuota.valor != '1' && monto.valor != '176') {
        this.displayErrorMontoCuota = true;
        this.stopSavingByMontoCuota = true;
      }
    }
    if (typeof callback == 'function' ) {
      callback();
    }
  }

    isNumber(param:any) {
        return util.isNumber(param);
    }

    ValidarComponentesEditable(id: any) {
        if (this.componentesInvisibles != null) {
        let componentesInvisibles = typeof this.componentesInvisibles == 'string' ? JSON.parse(this.componentesInvisibles) : this.componentesInvisibles;
        let componentesMerged = componentesInvisibles && Array.isArray(componentesInvisibles) ? componentesInvisibles.map(param => Object.assign(param,param.componente)) : [];
            if (componentesMerged && componentesMerged.length) {
              componentesInvisibles = componentesMerged;
            }
            if (componentesInvisibles.find((params) => params.codigo == id && params.estado == "NE")) {
              return false;
            } else {
              return true;
            }
        }
    }

    styledFieldsObject(form:FormGroup) {
      form.controls
    }

    styledFields(name: String,obj:any) {
      let classList = "md-inputfield";
      let keys;
      let itsBankField;
        if (obj) {
          keys = Object.keys(obj);
        } else {
          let objAllPersonaBanco = Object.assign(
            this.persona_banco_transaccion_debito_csg,
            this.persona_banco_transaccion,
            this.persona_banco_account,
            this.persona_banco,
            this.persona_banco_solicitud,
            this.persona_banco_operacion,
            this.persona_banco_pep,
            this.persona_banco_tarjeta_credito,
            this.persona_banco_tarjeta_debito,
            this.persona_banco_tarjeta_debito_cuenta_vinculada,
            this.persona_banco_tarjeta_credito_cuenta_asociada,
            this.persona_banco_tarjeta_debito_cuentas_vinculadas,
            this.persona_banco_cuentas_vinculadas,
            this.persona_banco_tarjeta_credito_cuentas_asociadas
          );
          keys = Object.keys(objAllPersonaBanco);
        }
        itsBankField = false;
        keys.forEach((key, index) => {
            if (key == name) {
                itsBankField = true;
            }
        });
        if (!itsBankField) {
            classList += " md-inputfield-fill";
        }
        return classList;
    }

  setSolicitudAsIniciado() {
    if (this.asegurado.instancia_poliza.id_estado == this.estadoIniciado.id) {
      this.btnEmitirCertificadoEnabled = false;
      this.btnImprimirSolicitudEnabled = false;
      if(this.atributoDebitoAutomatico.valor == this.condicionSi.id+'') {
        this.showOrdenPago = false;
      } else {
        this.showOrdenPago = false;
      }
      this.btnOrdenPago(false);
      if(this.hasRolConsultaTarjetas || this.hasRolConsultaCajero) {
        this.btnRefrescarInformacion = false;
      } else {
        this.btnRefrescarInformacion = true;
      }
    }
  }

    async getAllParametrosByIdDiccionario(idsObjetos: number[], idPoliza: any, ids: number[], callback: Function = null) {
        //if (!this.paramsLoaded) {
        this.paramsLoaded = true;
        await this.parametroService
            .GetAllParametrosByDiccionarioId(ids)
            .subscribe(async (res) => {
                    let response = res as { status: string; message: string; data: Parametro[]; };
                    this.parametros = response.data;
                    this.ProcedenciaCI = [];
                    this.Sexos = [];
                    this.SexosCod = [];
                    this.Sucursales = [];
                    this.Agencias = [];
                    this.Paises = [];
                    this.Planes = [];
                    this.TiposDocumentos = [];
                    this.Condiciones = [];
                    this.FormasPago = [];
                    this.BeneficiarioCondiciones = [];
                    this.BeneficiarioCondicionesAbreviacion = [];
                    this.BeneficiarioCondicionesParametroCodParametroId = [];
                    this.MotivosAnulacion = [];
                    this.MotivosAnulacionAbreviacion = [];
                    this.MotivosAnulacionParametroCodParametroId = [];
                    this.CondicionesPep = [];
                    this.Monedas = [];
                    this.Tipo = [];
                    this.Estado = [];
                    this.Parentesco = [];
                    this.EstadosInstaciaPoliza = [];
                    this.EstadosDeudor = [];
                    this.Nacionalidades = [];
                    this.estadosPoliza = [];
                    this.Periodicidad = [];
                    this.TipoCuenta = [];
                    this.TiposTarjeta = [];
                    this.EstadosTarjeta = [];
                    this.ProductosAsociados = [];
                    this.SolicitudesSci = [];
                    await this.ProcedenciaCI.push({
                        label: "Seleccione Procedencia",
                        value: null,
                    });
                    await this.Sexos.push({
                        label: "Seleccione Sexo",
                        value: null,
                    });
                    await this.SexosCod.push({
                        label: "Seleccione Sexo",
                        value: null,
                    });
                    await this.Sucursales.push({
                        label: "Seleccione una sucursal",
                        value: null,
                    });
                    await this.Agencias.push({
                        label: "Seleccione una agencia",
                        value: null,
                    });
                    await this.Paises.push({
                        label: "Seleccione Pais",
                        value: null,
                    });
                    await this.Planes.push({
                        label: "Seleccione un Plan",
                        value: null,
                    });
                    await this.TiposDocumentos.push({
                        label: "Seleccione Tipo de Documento",
                        value: null,
                    });
                    await this.Condiciones.push({
                        label: "Seleccione",
                        value: null,
                    });
                    await this.FormasPago.push({
                        label: "Seleccione",
                        value: null,
                    });
                    await this.CondicionesPep.push({
                        label: "Seleccione",
                        value: null,
                    });
                    await this.Monedas.push({
                        label: "Seleccione una moneda",
                        value: null,
                    });
                    await this.Tipo.push({
                        label: "Seleccione un tipo",
                        value: null,
                    });
                    await this.Estado.push({
                        label: "Seleccione un estado",
                        value: null,
                    });
                    await this.Parentesco.push({
                        label: "Seleccione un parentesco",
                        value: null,
                    });
                    await this.EstadosInstaciaPoliza.push({
                        label: "Seleccione un estado",
                        value: null,
                    });
                    await this.EstadosDeudor.push({
                        label: "Seleccione un estado",
                        value: null,
                    });
                    await this.Nacionalidades.push({
                        label: "Seleccione una nacionalidad",
                        value: null,
                    });
                    await this.Periodicidad.push({
                        label: "Seleccione una periodicidad",
                        value: null,
                    });
                    await this.TipoCuenta.push({
                        label: "Seleccione un tipo de cuenta",
                        value: null,
                    });
                    await this.TiposTarjeta.push({
                        label: "Seleccione un tipo de tarjeta",
                        value: null,
                    });
                    await this.EstadosTarjeta.push({
                        label: "Seleccione un estado de la tarjeta",
                        value: null,
                    });
                    await this.ProductosAsociados.push({
                        label: "Seleccione un producto Asociado",
                        value: null,
                    });
                    await this.MotivosAnulacion.push({
                        label: "Seleccione un motivo Asociado",
                        value: null,
                    });

                    if (response.data.length) {
                        this.parametros = response.data;
                        this.parametros.forEach((parametro: Parametro) => {
                                switch (parametro.id + "") {
                                    case "24":
                                        this.estadoIniciado = parametro;
                                        break;
                                    case "81":
                                        this.estadoSolicitado = parametro;
                                        break;
                                    case "59":
                                        this.estadoEmitido = parametro;
                                        break;
                                    case "60":
                                        this.estadoAnulado = parametro;
                                        break;
                                    case "350":
                                        this.estadoRechazado = parametro;
                                        break;
                                    case "352":
                                        this.estadoNoIncluido = parametro;
                                        break;
                                    case "282":
                                        this.estadoSinVigencia = parametro;
                                        break;
                                    case "62":
                                        this.estadoDesistido = parametro;
                                        break;
                                    case "63":
                                        this.estadoCobrado = parametro;
                                        break;
                                    case "65":
                                        this.estadoCaducado = parametro;
                                        break;
                                    case "238":
                                        this.estadoPorPagar = parametro;
                                        break;
                                    case "302":
                                        this.estadoPorEmitir = parametro;
                                        break;
                                    case "57":
                                        this.pagoEfectivo = parametro;
                                        this.condicionSi = parametro;
                                        break;
                                    case "58":
                                        this.pagoDebitAutomatico = parametro;
                                        this.condicionNo = parametro;
                                        break;
                                    case "247":
                                        this.parMancomunada = parametro;
                                        break;
                                    case "248":
                                        this.parIndividual = parametro;
                                        break;
                                    case "90":
                                        this.parMensual = parametro;
                                        break;
                                    case "93":
                                        this.parAnual = parametro;
                                        break;
                                    case "32":
                                        this.parametroInvisible = parametro;
                                        break;
                                    case "33":
                                        this.parametroNoEditable = parametro;
                                        break;
                                    case "71":
                                        this.parametroEditable = parametro;
                                        break;
                                    case "70":
                                        this.parametroVisible = parametro;
                                        break;
                                    case "88":
                                        this.parametroNumeracionIndependiente = parametro;
                                        break;
                                    case "89":
                                        this.parametroNumeracionUnificada = parametro;
                                        break;
                                    case "95":
                                        this.parametroError = parametro;
                                        break;
                                    case "96":
                                        this.parametroWarning = parametro;
                                        break;
                                    case "305":
                                        this.parametroInfoWarning = parametro;
                                        break;
                                    case "97":
                                        this.parametroInformation = parametro;
                                        break;
                                    case "98":
                                        this.parametroSuccess = parametro;
                                        break;
                                    case "279":
                                        this.parametroDocSolicitud = parametro;
                                        break;
                                    case "280":
                                        this.parametroDocComprobante = parametro;
                                        break;
                                    case "281":
                                        this.parametroDocCertificado = parametro;
                                        break;
                                    case "284":
                                        this.parametroSinExtension = parametro;
                                        break;
                                    case "289":
                                        this.parametroAsegurado = parametro;
                                        break;
                                    case "239":
                                        this.parametroDay = parametro;
                                        break;
                                    case "240":
                                        this.parametroMonth = parametro;
                                        break;
                                    case "241":
                                        this.parametroYear = parametro;
                                        break;
                                    case "110":
                                        this.parametroPlanes= parametro;
                                        break;
                                    case "298":
                                          this.parametroObjetoPoliza = parametro;
                                          break;
                                    case "288":
                                          this.parametroCondicionadoParticular = parametro;
                                          break;
                                    case "299":
                                          this.parametroObjetoDocumento = parametro;
                                          break;
                                    case "300":
                                          this.parametroBeneficiarioPrimario = parametro;
                                          break;
                                    case "301":
                                          this.parametroBeneficiarioContingente = parametro;
                                          break;
                                    case "295":
                                          this.parametroOtroDestinoCreditos = parametro;
                                          break;
                                    case "296":
                                          this.parametroEcoconsumoEcodisponible = parametro;
                                          break;
                                    case "297":
                                          this.parametroTarjetaCredito = parametro;
                                          break;
                                  case "303":
                                          this.parametroPagoCredito = parametro;
                                          break;
                                    case "304":
                                            this.parametroPagoContado = parametro;
                                            break;
                                    case "311": this.parametroTarjetaHabilitada = parametro;  break;
                                    case "312": this.parametroTarjetaPererdida = parametro;  break;
                                    case "315": this.parametroTarjetaInnominada = parametro;  break;
                                    case "316": this.parametroTarjetaBloqueada = parametro;  break;
                                    case "293": this.parametroTipoSeguroEcoAccidente = parametro;  break;
                                    case "294": this.parametroTipoSeguroEcoResguardo = parametro;  break;
                                    case "351": this.parametroIncluido = parametro;  break;
                                }
                                switch (parametro.diccionario_id + "") {
                                    case "1":
                                        this.TiposDocumentos.push({
                                            label: parametro.parametro_descripcion, value: parametro.id});
                                        this.TiposDocumentosAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        this.TiposDocumentosParametroCod[parametro.parametro_cod] = parametro.parametro_descripcion;
                                        this.TiposDocumentosId[parametro.id + ""] = parametro.parametro_descripcion;
                                        this.TiposDocumentosCodId[parametro.parametro_cod + ""] = parametro.id;
                                        break;
                                    case "2":
                                        this.Nacionalidades.push({label: parametro.parametro_descripcion, value: parametro.id});
                                        this.NacionalidadesAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        this.NacionalidadesParametroCod[parametro.parametro_cod] = parametro.id;
                                        break;
                                    case "3":
                                        this.Paises.push({ label: parametro.parametro_descripcion, value: parametro.id });
                                        this.PaisesAbreviacion[ parametro.parametro_abreviacion ] = parametro.id + "";
                                        this.PaisesParametroCod[ parametro.parametro_cod ] = parametro.parametro_abreviacion;
                                        this.PaisesId[parametro.id + ""] = parametro.parametro_descripcion;
                                        break;
                                    case "11":
                                        this.EstadosInstaciaPoliza.push({ label: parametro.parametro_descripcion, value: parametro.id + "" });
                                        this.EstadosInstaciaPolizaAbr.push({ label: parametro.parametro_descripcion, value: parametro.parametro_abreviacion + "" });
                                        this.EstadosInstaciaPolizaId[ parametro.id + "" ] = parametro.parametro_descripcion;
                                        this.EstadosInstaciaPolizaAbreviacion[ parametro.id + "" ] = parametro.parametro_abreviacion;
                                        this.estadosPoliza.push(parametro);
                                        break;
                                    case "70":
                                        this.EstadosDeudor.push({ label: parametro.parametro_descripcion, value: parametro.id + "" });
                                        this.EstadosDeudorAbr.push({ label: parametro.parametro_descripcion, value: parametro.parametro_abreviacion + "" });
                                        this.EstadosDeudorId[ parametro.id + "" ] = parametro.parametro_descripcion;
                                        this.EstadosDeudorParCodigo[ parametro.parametro_cod + "" ] = parametro.parametro_descripcion;
                                        this.EstadosDeudorAbreviacion[ parametro.id + "" ] = parametro.parametro_abreviacion;
                                        this.estadosPoliza.push(parametro);
                                      break;
                                    case "4":
                                        this.Sexos.push({ label: parametro.parametro_descripcion, value: parametro.id });
                                        this.SexosCod.push({ label: parametro.parametro_descripcion, value: parametro.parametro_abreviacion });
                                        this.SexosAbreviacion[ parametro.parametro_abreviacion ] = parametro.id + "";
                                        this.SexosParametroCod[ parametro.parametro_cod ] = parametro.id + "";
                                        this.SexosId[parametro.id + ""] = parametro.parametro_abreviacion + "";
                                        break;
                                    case "18":
                                        this.ProcedenciaCI.push({ label: parametro.parametro_descripcion, value: parametro.parametro_cod });
                                        this.ProcedenciaCIAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        this.ProcedenciaCIParametroCod[parametro.parametro_cod] = parametro.parametro_abreviacion;
                                        this.ProcedenciaCIParametroCodDescripcion[parametro.parametro_cod] = parametro.parametro_descripcion;
                                        // caso especial
                                        if (this.parametroSinExtension) {
                                            this.ProcedenciaCIAbreviacion["SE"] = this.parametroSinExtension.parametro_cod;
                                            this.ProcedenciaCIAbreviacion[""] = this.parametroSinExtension.parametro_cod;
                                        }
                                        break;
                                    case "20":
                                        this.Parentesco.push({ label: parametro.parametro_descripcion, value: parametro.id });
                                        this.ParentescoAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        break;
                                    case "21":
                                        this.Condiciones.push({ label: parametro.parametro_descripcion, value: parametro.id + "" });
                                        this.CondicionesPep.push({ label: parametro.parametro_abreviacion, value: parametro.id + "" });
                                        this.CondicionesId[parametro.id + ""] = parametro.parametro_abreviacion;
                                        this.CondicionesIdDesc[parametro.id + ""] = parametro.parametro_descripcion;
                                        this.CondicionesCod[parametro.parametro_cod + ""] = parametro.parametro_descripcion;
                                        break;
                                    case "59":
                                        this.FormasPago.push({label: parametro.parametro_descripcion,value: parametro.id + "",});
                                        this.FormasPagoId[parametro.id + ""] = parametro.parametro_abreviacion;
                                        this.FormasPagoIdDesc[parametro.id + ""] = parametro.parametro_descripcion;
                                        this.FormasPagoCod[parametro.parametro_cod + ""] = parametro.parametro_descripcion;
                                        break;
                                    case "22":
                                        this.Monedas.push({label: parametro.parametro_abreviacion, value: parseInt(parametro.parametro_cod)});
                                        this.MonedasAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        this.MonedasDescripcion[parametro.parametro_descripcion] = parametro.parametro_abreviacion;
                                        this.MonedasDescripcionParametroCod[parametro.parametro_descripcion.trim()] = parametro.parametro_cod;
                                        this.MonedasParametroCod[parametro.parametro_cod + ""] = parametro.parametro_abreviacion;
                                        this.MonedasStrParametroCod[parametro.parametro_cod] = parseInt(parametro.parametro_cod);
                                        this.MonedasId[parametro.id + ""] = parametro.parametro_abreviacion;
                                        this.parametrosMonedas.push(parametro);
                                        break;
                                    case "25":
                                        this.Estado.push({ label: parametro.parametro_descripcion, value: parametro.id });
                                        this.EstadoAbreviacion[ parametro.parametro_abreviacion ] = parametro.parametro_cod;
                                        break;
                                    case "29":
                                        this.Tipo.push({ label: parametro.parametro_descripcion, value: parametro.id });
                                        this.TipoAbreviacion[ parametro.parametro_abreviacion ] = parametro.parametro_cod;
                                        break;
                                    case "31":
                                        this.Periodicidad.push({label: parametro.parametro_descripcion,value: parametro.id,});
                                        this.PeriodicidadAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        this.PeriodicidadId[parametro.id+''] = parametro.parametro_abreviacion;
                                        break;
                                    case "32":
                                        this.ObservacionesId[parametro.id + ""] = parametro.parametro_descripcion;
                                        this.estadosObservaciones.push( parametro );
                                        break;
                                    case "33":
                                        this.TiposTarjeta.push({label: parametro.parametro_descripcion, value: parametro.id});
                                        this.TiposTarjetaCod[parametro.parametro_cod] = parametro.parametro_descripcion;
                                        this.TiposTarjetaId[parametro.id + ''] = parametro.parametro_descripcion;
                                        this.TiposTarjetaDescripcion[parametro.parametro_descripcion.toLowerCase().trim()] = parametro.id;
                                        break;
                                    case "61":
                                        this.EstadosTarjeta.push({label: parametro.parametro_descripcion, value: parametro.id});
                                        this.EstadosTarjetaCod[parametro.parametro_cod] = parametro.parametro_descripcion;
                                        this.EstadosTarjetaId[parametro.id + ''] = parametro.parametro_descripcion;
                                        this.EstadosTarjetaDescripcion[parametro.parametro_descripcion.toLowerCase().trim()] = parametro.id;
                                        break;
                                    // case "36":
                                    //     await this.Planes.push({ label: parametro.parametro_descripcion, value: parametro.id });
                                    //     break;
                                    case "38":
                                        this.Sucursales.push({ label: parametro.parametro_descripcion, value: parametro.parametro_cod });
                                        this.SucursalesParametroCod[ parametro.parametro_cod ] = parametro.parametro_descripcion;
                                        this.SucursalesParametroCodParametroId[ parametro.parametro_cod ] = parametro.id;
                                        break;
                                    case "40":
                                        this.Agencias.push({ label: parametro.parametro_descripcion, value: parametro.parametro_cod });
                                        this.AgenciasParametroCod[ parametro.parametro_cod ] = parametro.parametro_descripcion;
                                        break;
                                    case "44":
                                        this.TipoCuenta.push({ label: parametro.parametro_descripcion, value: parametro.id });
                                        this.TipoCuentaAbreviacion[ parametro.parametro_abreviacion ] = parametro.parametro_cod;
                                        break;
                                    case "56":
                                        this.ProductosAsociados.push({ label: parametro.parametro_descripcion, value: parametro.id });
                                        this.ProductosAsociadosAbreviacion[ parametro.parametro_abreviacion ] = parametro.parametro_cod;
                                        break;
                                    case "58":
                                        this.BeneficiarioCondiciones.push({label: parametro.parametro_descripcion, value: parametro.id});
                                        this.BeneficiarioCondicionesAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        this.BeneficiarioCondicionesParametroCodParametroId[parametro.parametro_cod] = parametro.id;
                                        this.BeneficiarioCondicionesIdParametroDescripcion[parametro.id] = parametro.parametro_descripcion;
                                        break;
                                    case "69":
                                        this.MotivosAnulacion.push({label: parametro.parametro_descripcion, value: parametro.id});
                                        this.MotivosAnulacionAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        this.MotivosAnulacionParametroCodParametroId[parametro.parametro_cod] = parametro.id;
                                        break;
                                    case "66":
                                        this.TiposCreditoConsumo.push({label: parametro.parametro_descripcion, value: parametro.parametro_cod});
                                        this.TiposCreditoConsumoAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        this.TiposCreditoConsumoParametroCodParametroId[parametro.parametro_cod] = parametro.id;
                                        break;
                                    case "67":
                                        this.TiposCreditoDisponible.push({label: parametro.parametro_descripcion, value: parametro.parametro_cod});
                                        this.TiposCreditoDisponibleAbreviacion[parametro.parametro_abreviacion] = parametro.parametro_cod;
                                        this.TiposCreditoDisponibleParametroCodParametroId[parametro.parametro_cod] = parametro.id;
                                        break;
                                }
                            }
                        );
                    }
                    if (idsObjetos && idsObjetos.length && idPoliza) {
                        if (Array.isArray(idPoliza) && idPoliza.length) {
                            if (!idPoliza.find(param => param == 'null')) {
                                await this.getAllDocumentos(idsObjetos, idPoliza, callback);
                            }
                        } else {
                            await this.getAllDocumentos(idsObjetos, idPoliza, callback);
                        }
                    } else {
                      if (typeof callback == 'function') {
                          callback();
                      }
                    }
                },
                (err) => {
                    if (
                        err.error.statusCode == 400 &&
                        err.error.message == "usuario no autentificado"
                    ) {
                        this.router.navigate([""]);
                    } else {
                        console.log(err);
                    }
                }
            );
        //} else {
        //}
    }

  validateForm(userform:FormGroup) {
    console.log(userform.valid);
    console.log(userform.controls);
    console.log(persona_banco_account);
    Object.values(userform.controls).forEach((value, index) => {
      console.log(
        Object.keys(userform.controls)[index],
        value.validator
      );
    });
    console.log(userform.getRawValue());
    console.log(this.ValidarComponentesInvisible("EA_TitParSexoId"));
    console.log(this.ValidarComponentesInvisible("EA_TitParNumeroCuenta"));
    console.log(this.ValidarComponentesInvisible("EA_TitParMoneda"));
    console.log(this.ValidarComponentesInvisible("EA_TitDebitoAutomatico"));
    console.log(this.ValidarComponentesInvisible("EA_TitTarjetaNro"));
    console.log(this.nuevo);
  }

  async setMsgsWarnsOrErrors(asegurado: Asegurado, severity: Parametro, tipoError: string, key: string = "") {
    if (tipoError) {
      const instanciaPolizaTrans = new Instancia_poliza_transicion();
      instanciaPolizaTrans.par_observacion_id = severity ? severity.id : null;
      instanciaPolizaTrans.par_estado_id = asegurado.instancia_poliza.id_estado;
      instanciaPolizaTrans.id_instancia_poliza = asegurado.instancia_poliza.id;
      instanciaPolizaTrans.observacion = tipoError;
      asegurado.instancia_poliza.instancia_poliza_transicions.push(instanciaPolizaTrans);
      if (severity) {
        if (severity.id == this.parametroError.id) {
          if (!this.msgs_error.find(param => param.detail == tipoError)) {
            this.msgs_error.push({
              severity: severity.parametro_cod,
              summary: severity.parametro_descripcion + ": ",
              detail: tipoError,
              key: key,
            });
          }
        } else {
          if (this.esClienteBanco) {
            if (severity.id == this.parametroInfoWarning.id) {
              if (!this.msgs_info_warn.find(param =>param.detail == tipoError)) {
                this.msgs_info_warn.push({
                  severity: severity.parametro_cod,
                  summary: severity.parametro_descripcion + ": ",
                  detail: tipoError,
                  key: key,
                });
              }
            } else if (severity.id == this.parametroWarning.id) {
              if (!this.msgs_warn.find(param => param.detail == tipoError)) {
                this.msgs_warn.push({
                  severity: severity.parametro_cod,
                  summary: severity.parametro_descripcion + ": ",
                  detail: tipoError,
                  key: key,
                });
              }
            }
          }
        }
      }
    }
  }

    async getAllAtributos(idsObjetos: number[], callback: Function = null) {
        idsObjetos.push(this.objetoAseguradoDatosComplementarios.id);
        if (this.ObjetoAtributosDoc.length == 0) {
            this.ObjetoAtributos = [];
            await this.atributoService
                .getAllAtributosByIdObjeto(idsObjetos)
                .subscribe(
                    async (res) => {
                        let response = res as {
                            status: string;
                            message: string;
                            data: Objeto_x_atributo[];
                        };
                        await response.data.forEach(
                            async (objetoAtributo: Objeto_x_atributo) => {
                                switch (objetoAtributo.id_objeto + "") {
                                    case "4":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "5":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "6":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "7":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "8":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "9":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "10":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "11":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "12":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "13":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "18":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "19":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "20":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "21":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "22":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "23":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "24":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "25":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "26":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "27":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "28":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "29":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "30":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "31":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                    case "32":
                                        this.ObjetoAtributos.push(objetoAtributo);
                                        break;
                                    case "33":
                                        this.ObjetoAtributosDoc.push(objetoAtributo);
                                        break;
                                }
                            }
                        );
                        if (typeof callback == "function") {
                            callback();
                        }
                    },
                    (err) => {
                        if (
                            err.error.statusCode == 400 &&
                            err.error.message == "usuario no autentificado"
                        ) {
                            window.location.replace("/");
                        } else {
                            console.log(err);
                        }
                    }
                );
        } else {
            if (typeof callback == "function") {
                callback();
            }
        }
    }


  async afterAlertEdad(userform:FormGroup) {
    userform.controls["persona_fecha_nacimiento"].setErrors(null);
    this.stopSavingByEdad = false;
  }

  async validarApellidos(userform:FormGroup, callback:any = null) {
    console.log(this.persona_banco);
    if (
      this.persona_banco.paterno == "" &&
      this.persona_banco.materno == ""
    ) {
      userform.controls["persona_primer_apellido"].setErrors([Validators.pattern("^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$"),]);
      userform.controls["persona_segundo_apellido"].setErrors([Validators.pattern("^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$"),]);
      this.atributoApellidoPaterno.requerido = true;
      // this.isLoadingAgain = false;
      this.stopSavingByLastName = true;
    } else {
      //this.onApellidoInput();
      this.stopSavingByLastName = false;
      this.displayValidacionApellidos = false;
    }
    console.log('apellido',this.stopSavingByLastName);
    if (typeof callback == 'function') {
      await callback();
    }
  }

  async setAtributoObservacion(asegurado: Asegurado, tipoError, valor, severity: Parametro, isShowingUp: Boolean = true, key: string = "") {
    if (isShowingUp && (valor == "0" || valor == "" || valor == undefined || valor == 'undefined' || valor == 'null' || valor == null)) {
      this.setMsgsWarnsOrErrors(asegurado, severity, tipoError, key);
    } else {
      let tipoErrorSinPrefijoTitularPersona = tipoError.split("La persona").join("").split("El titular").join("").trim();
      let oldTransicion;
      if (this.instanciaPolizaTransService.oldInstanciaPolizaTransiciones && this.instanciaPolizaTransService.oldInstanciaPolizaTransiciones.length) {
        oldTransicion = this.instanciaPolizaTransService.oldInstanciaPolizaTransiciones.find((params) => params.observacion && params.observacion.includes(tipoErrorSinPrefijoTitularPersona));
      }
      if (
          isObject(oldTransicion) &&
          isShowingUp &&
          (valor != "" || valor != undefined || valor != 'undefined' || valor != 'null' || valor != null)
      ) {
        this.setMsgsWarnsOrErrors(asegurado, this.parametroSuccess, tipoError, key);
      }
    }
  }

    async getAllDocumentos(idsObjetos: number[], id_poliza: any, callback: Function = null) {
        if (id_poliza != undefined) {
            await this.documentoService.listaDocumentosByIdPoliza(id_poliza).subscribe(async (res) => {
              let response = res as {
                status: string;
                message: string;
                data: Documento[];
              };
              this.documentosPoliza = response.data;
              for (let i = 0; i < this.documentosPoliza.length; i++) {
                let documentoPoliza = this.documentosPoliza[i];
                switch (documentoPoliza.id + "") {
                  case "1":
                    this.documentoSolicitud = documentoPoliza;
                    await this.documentoSolicitud.poliza.objetos.forEach(
                      async (objetoDocumento: Objeto) => {
                        switch (objetoDocumento.id + "") {
                          case "4":
                            this.objetoAseguradoDatosComplementarios =
                              objetoDocumento;
                            await this.atributoService.getAllAtributosByIdObjetoEditablesYVisibles(this.objetoAseguradoDatosComplementarios.id, this.parametroEditable.id, this.parametroVisible.id).subscribe(async (res) => {
                              let response =
                                res as {
                                status: string;
                                message: string;
                                data: Objeto_x_atributo[];
                              };
                              this.ObjetoAseguradoAtributosFiltered = response.data;
                            });
                            break;
                        }
                      });
                    break;
                    case "2":
                      this.documentoComprobante = documentoPoliza;
                      break;
                      case "3":
                        this.documentoCertificado = documentoPoliza;
                        break;
                        case "4":
                          this.documentoSolicitud = documentoPoliza;
                          await this.documentoSolicitud.poliza.objetos.forEach(async (objetoDocumento: Objeto) => {
                            switch (objetoDocumento.id + "") {
                              case "6":
                                this.objetoAseguradoDatosComplementarios = objetoDocumento;
                                await this.atributoService.getAllAtributosByIdObjetoEditablesYVisibles(this.objetoAseguradoDatosComplementarios.id, this.parametroEditable.id, this.parametroVisible.id).subscribe(async (res) => {
                                  let response =
                                    res as { status: string; message: string; data: Objeto_x_atributo[]; };
                                                                this.ObjetoAseguradoAtributosFiltered =
                                                                    response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "5":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "6":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                case "7":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "8":
                                                    this.objetoAseguradoDatosComplementarios =
                                                        objetoDocumento;
                                                    await this.atributoService
                                                        .getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this
                                                                .objetoAseguradoDatosComplementarios
                                                                .id,
                                                            this
                                                                .parametroEditable
                                                                .id,
                                                            this
                                                                .parametroVisible
                                                                .id
                                                        )
                                                        .subscribe(
                                                            async (res) => {
                                                                let response =
                                                                    res as {
                                                                        status: string;
                                                                        message: string;
                                                                        data: Objeto_x_atributo[];
                                                                    };
                                                                this.ObjetoAseguradoAtributosFiltered =
                                                                    response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "8":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "9":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                case "10":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "10":
                                                    this.objetoAseguradoDatosComplementarios =
                                                        objetoDocumento;
                                                    await this.atributoService
                                                        .getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this
                                                                .objetoAseguradoDatosComplementarios
                                                                .id,
                                                            this
                                                                .parametroEditable
                                                                .id,
                                                            this
                                                                .parametroVisible
                                                                .id
                                                        )
                                                        .subscribe(
                                                            async (res) => {
                                                                let response =
                                                                    res as {
                                                                        status: string;
                                                                        message: string;
                                                                        data: Objeto_x_atributo[];
                                                                    };
                                                                this.ObjetoAseguradoAtributosFiltered =
                                                                    response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "11":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "12":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                case "13":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "12":
                                                    this.objetoAseguradoDatosComplementarios =
                                                        objetoDocumento;
                                                    await this.atributoService
                                                        .getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this
                                                                .objetoAseguradoDatosComplementarios
                                                                .id,
                                                            this
                                                                .parametroEditable
                                                                .id,
                                                            this
                                                                .parametroVisible
                                                                .id
                                                        )
                                                        .subscribe(
                                                            async (res) => {
                                                                let response =
                                                                    res as {
                                                                        status: string;
                                                                        message: string;
                                                                        data: Objeto_x_atributo[];
                                                                    };
                                                                this.ObjetoAseguradoAtributosFiltered =
                                                                    response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "14":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "15":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                case "16":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "18":
                                                    this.objetoAseguradoDatosComplementarios =
                                                        objetoDocumento;
                                                    await this.atributoService
                                                        .getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this
                                                                .objetoAseguradoDatosComplementarios
                                                                .id,
                                                            this
                                                                .parametroEditable
                                                                .id,
                                                            this
                                                                .parametroVisible
                                                                .id
                                                        )
                                                        .subscribe(
                                                            async (res) => {
                                                                let response =
                                                                    res as {
                                                                        status: string;
                                                                        message: string;
                                                                        data: Objeto_x_atributo[];
                                                                    };
                                                                this.ObjetoAseguradoAtributosFiltered =
                                                                    response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "17":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "18":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                // case "24":
                                //     this.documentoCotizacion = documentoPoliza;
                                //     break;
                                // case "25":
                                //     this.documentoCronogramaPago = documentoPoliza;
                                //     break;
                                case "19":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "20":
                                                    this.objetoAseguradoDatosComplementarios =
                                                        objetoDocumento;
                                                    await this.atributoService
                                                        .getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this.objetoAseguradoDatosComplementarios.id,
                                                            this.parametroEditable.id,
                                                            this.parametroVisible.id
                                                        )
                                                        .subscribe(
                                                            async (res) => {
                                                                let response =
                                                                    res as {
                                                                        status: string;
                                                                        message: string;
                                                                        data: Objeto_x_atributo[];
                                                                    };
                                                                this.ObjetoAseguradoAtributosFiltered =
                                                                    response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "20":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "21":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                // case "22":
                                //     this.documentoCotizacion = documentoPoliza;
                                //     break;
                                // case "23":
                                //     this.documentoCronogramaPago = documentoPoliza;
                                //     break;
                                //
                                case "22":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "22":
                                                    this.objetoAseguradoDatosComplementarios =
                                                        objetoDocumento;
                                                    await this.atributoService
                                                        .getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this.objetoAseguradoDatosComplementarios.id,
                                                            this.parametroEditable.id,
                                                            this.parametroVisible.id
                                                        )
                                                        .subscribe(
                                                            async (res) => {
                                                                let response =
                                                                    res as {
                                                                        status: string;
                                                                        message: string;
                                                                        data: Objeto_x_atributo[];
                                                                    };
                                                                this.ObjetoAseguradoAtributosFiltered =
                                                                    response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "23":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "24":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                case "25":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "24":
                                                    this.objetoAseguradoDatosComplementarios =
                                                        objetoDocumento;
                                                    await this.atributoService
                                                        .getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this.objetoAseguradoDatosComplementarios.id,
                                                            this.parametroEditable.id,
                                                            this.parametroVisible.id
                                                        )
                                                        .subscribe(
                                                            async (res) => {
                                                                let response =
                                                                    res as {
                                                                        status: string;
                                                                        message: string;
                                                                        data: Objeto_x_atributo[];
                                                                    };
                                                                this.ObjetoAseguradoAtributosFiltered = response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "26":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "27":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                case "28":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "26":
                                                    this.objetoAseguradoDatosComplementarios = objetoDocumento;
                                                    await this.atributoService.getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this.objetoAseguradoDatosComplementarios.id,
                                                            this.parametroEditable.id,
                                                            this.parametroVisible.id
                                                        ).subscribe(
                                                            async (res) => {
                                                                let response = res as { status: string; message: string; data: Objeto_x_atributo[]; };
                                                                this.ObjetoAseguradoAtributosFiltered = response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "29":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "30":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                case "31":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "28":
                                                    this.objetoAseguradoDatosComplementarios = objetoDocumento;
                                                    await this.atributoService.getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this.objetoAseguradoDatosComplementarios.id,
                                                            this.parametroEditable.id,
                                                            this.parametroVisible.id
                                                        ).subscribe(
                                                            async (res) => {
                                                                let response = res as { status: string; message: string; data: Objeto_x_atributo[]; };
                                                                this.ObjetoAseguradoAtributosFiltered = response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "32":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "33":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                case "34":
                                    this.documentoSolicitud = documentoPoliza;
                                    await this.documentoSolicitud.poliza.objetos.forEach(
                                        async (objetoDocumento: Objeto) => {
                                            switch (objetoDocumento.id + "") {
                                                case "30":
                                                    this.objetoAseguradoDatosComplementarios = objetoDocumento;
                                                    await this.atributoService.getAllAtributosByIdObjetoEditablesYVisibles(
                                                            this.objetoAseguradoDatosComplementarios.id,
                                                            this.parametroEditable.id,
                                                            this.parametroVisible.id
                                                        ).subscribe(
                                                            async (res) => {
                                                                let response = res as { status: string; message: string; data: Objeto_x_atributo[]; };
                                                                this.ObjetoAseguradoAtributosFiltered = response.data;
                                                            }
                                                        );
                                                    break;
                                            }
                                        }
                                    );
                                    break;
                                case "35":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "36":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                                case "37":
                                    this.documentoAnulacion = documentoPoliza;
                                    break;
                                case "38":
                                    this.documentoAnulacion = documentoPoliza;
                                    break;
                                case "39":
                                    this.documentoSolicitud = documentoPoliza;
                                    for (let j = 0; j < this.documentoSolicitud.poliza.objetos.length; j++) {
                                      let objetoDocumento = this.documentoSolicitud.poliza.objetos[j];
                                      switch (objetoDocumento.id + "") {
                                        case "32":
                                          this.objetoAseguradoDatosComplementarios = objetoDocumento;
                                          await this.atributoService.getAllAtributosByIdObjetoEditablesYVisibles(
                                              this.objetoAseguradoDatosComplementarios.id,
                                              this.parametroEditable.id,
                                              this.parametroVisible.id
                                          ).subscribe(
                                              async (res) => {
                                                let response = res as { status: string; message: string; data: Objeto_x_atributo[]; };
                                                this.ObjetoAseguradoAtributosFiltered = response.data;
                                              }
                                          );
                                          break;
                                        case "33":
                                          this.objetoDocumentoDatosComplementarios = objetoDocumento;
                                          await this.atributoService.getAllAtributosByIdObjetoEditablesYVisibles(
                                              this.objetoDocumentoDatosComplementarios.id,
                                              this.parametroEditable.id,
                                              this.parametroVisible.id
                                          ).subscribe(
                                              async (res) => {
                                                let response = res as { status: string; message: string; data: Objeto_x_atributo[]; };
                                                this.ObjetoDocSolicitudAtributosFiltered = response.data;
                                              }
                                          );
                                          break;
                                      }
                                    }
                                    break;
                                case "40":
                                    this.documentoComprobante = documentoPoliza;
                                    break;
                                case "41":
                                    this.documentoCertificado = documentoPoliza;
                                    break;
                            }
                        }
                        await this.getAllAtributos(idsObjetos, callback);
                    },
                    (err) => {
                        if (
                            err.error.statusCode == 400 &&
                            err.error.message == "usuario no autentificado"
                        ) {
                            this.router.navigate([""]);
                        } else {
                            console.log(err);
                        }
                    }
                );
        }
    }

    async showFormValidation(productForm: FormGroup) {
        if (productForm) {
            Object.keys(productForm.controls).forEach((key) => {
                const controlErrors: ValidationErrors = productForm.get(key).errors;
                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach((keyError) => {
                        this.showMessages(key, controlErrors[keyError], productForm);
                    });
                } else {
                    this.showMessages(key, false, productForm);
                }
            });
        }
    }

    async updateEdad(event:any) {
        if (event.target.value) {
            let [day,month,year] = event.target.value.split('/');
            this.persona_banco.fecha_nacimiento = new Date(year,parseInt(month)-1,day);
            this.persona_banco.fecha_nacimiento_str = `${day}/${month}/${year}`;
            this.asegurado.entidad.persona.persona_fecha_nacimiento = this.persona_banco.fecha_nacimiento;
        }
    }

    showMessages(key: string, toShow: boolean, form: FormGroup) {
        switch (key) {
            case "persona_fecha_nacimiento":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco.fecha_nacimiento_str
                    );
                }
                this.atributoFechaNacimiento.requerido = toShow;
                break;
            case "par_ciudad_nacimiento_id":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.ciudad_nacimiento
                    );
                }
                this.atributoCiudadNacimiento.requerido = toShow;
                break;
            case "par_pais_nacimiento_id":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.pais_nacimiento
                    );
                }
                this.atributoPaisNacimiento.requerido = toShow;
                break;
            case "par_debito_automatico_id":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco.debito_automatico
                    );
                }
                this.atributoDebitoAutomatico.requerido = toShow;
                break;
            case "par_moneda":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_account.moneda
                    );
                }
                this.atributoMoneda.requerido = toShow;
                break;
            case "par_numero_cuenta":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_account.nrocuenta
                    );
                }
                this.atributoNroCuenta.requerido = toShow;
                break;
            case "par_sexo_id":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco.sexo);
                }
                this.atributoSexo.requerido = toShow;
                break;
            case "par_condicion_pep":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_pep.condicion
                    );
                }
                this.atributoCondicionPep.requerido = toShow;
                break;
            case "par_tipo_documento_id":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.TiposDocumentosCodId[
                            this.persona_banco_datos.tipo_doc
                        ]
                    );
                }
                this.atributoTipoDoc.requerido = toShow;
                break;
            case "persona_doc_id":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco.doc_id);
                }
                this.atributoDocId.requerido = toShow;
                break;
            case "persona_doc_id_ext":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.ProcedenciaCIAbreviacion[
                            this.persona_banco.extension
                        ]
                    );
                }
                this.atributoDocIdExt.requerido = toShow;
                break;
            case "persona_primer_apellido":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco.paterno);
                }
                this.atributoApellidoPaterno.requerido = toShow;
                break;
            case "persona_segundo_apellido":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco.materno);
                }
                this.atributoApellidoMaterno.requerido = toShow;
                break;
            case "persona_primer_nombre":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco.nombre);
                }
                this.atributoPrimerNombre.requerido = toShow;
                break;
            case "persona_segundo_nombre":
                if (!form.controls[key].value) {
                }
                this.atributoSegundoNombre.requerido = toShow;
                break;
            case "persona_apellido_casada":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco.apcasada);
                }
                this.atributoApellidoCasada.requerido = toShow;
                break;
            case "persona_direccion_domicilio":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco.direccion);
                }
                this.atributoDireccionDomicilio.requerido = toShow;
                break;
            case "persona_direcion_trabajo":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.direccion_laboral
                    );
                }
                this.atributoDireccionLaboral.requerido = toShow;
                break;
            case "persona_telefono_domicilio":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco.fono_domicilio
                    );
                }
                this.atributoTelefonoDomicilio.requerido = toShow;
                break;
            case "persona_telefono_trabajo":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco.fono_oficina
                    );
                }
                this.atributoTelefonoTrabajo.requerido = toShow;
                break;
            case "persona_telefono_celular":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.telefono_celular
                    );
                }
                this.atributoTelefonoCelular.requerido = toShow;
                break;
            // case 'persona_profesion':
            //     this.atributoOcupacion.requerido = toShow;
            //     break;
            // case 'par_nacionalidad_id':
            //     this.atributoPaisNacimiento.requerido = toShow;
            //     break;
            // case 'persona_origen':
            //     this.atributoTipoDoc.requerido = toShow;
            //     break;
            case "par_cuenta_expiracion":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_account ? this.persona_banco_account.fecha_expiracion : null
                    );
                }
                this.atributoCtaFechaExpiracion.requerido = toShow;
                break;
            case "par_ocupacion":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.ocupacion
                    );
                }
                this.atributoOcupacion.requerido = toShow;
                break;
            case "par_plan":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco_datos.plan);
                }
                this.atributoPlan.requerido = toShow;
                break;
            case "par_plazo":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco_datos.plazo);
                }
                this.atributoPlazo.requerido = toShow;
                break;
            case "par_codigo_agenda_id":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco.cod_agenda);
                }
                this.atributoCodAgenda.requerido = toShow;
                break;
            // case 'par_estado_civil_id':
            //     this.atributo.requerido = toShow;
            //     break;
            case "par_mail_id":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco.e_mail);
                }
                this.atributoEmail.requerido = toShow;
                break;
            // case 'par_caedec_id':
            //     this.atributoCa.requerido = toShow;
            //     break;
            // case 'par_localidad_id':
            //     this.atributo.requerido = toShow;
            //     break;
            // case 'par_departamento_id':
            //     this.atributoDe.requerido = toShow;
            //     break;
            case "par_sucursal":
                this.atributoSucursal.requerido = toShow;
                break;
            case "par_agencia":
                this.atributoAgencia.requerido = toShow;
                break;
            // case 'par_nro_sci':
            //     this.atributoTipoDoc.requerido = toShow;
            //     break;
            case "par_modalidad_pago":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.modalidad_pago
                    );
                }
                this.atributoModalidadPago.requerido = toShow;
                break;
            case "par_zona":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco_datos.zona);
                }
                this.atributoZona.requerido = toShow;
                break;
            case "par_nro_direccion":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.nro_direccion
                    );
                }
                this.atributoNroDireccion.requerido = toShow;
                break;
            case "par_razon_social":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.razon_social
                    );
                }
                this.atributoRazonSocial.requerido = toShow;
                break;
            case "par_archivo":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.ruta_archivo
                    );
                }
                this.atributoArchivo.requerido = toShow;
                break;
            case "par_nit_carnet":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.nit_carnet
                    );
                }
                this.atributoNitCarnet.requerido = toShow;
                break;
            case "par_cargo_pep":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_pep.cargo_entidad
                    );
                }
                this.atributoCargoEntidadPep.requerido = toShow;
                break;
            case "par_periodo_pep":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_pep.periodo_cargo_publico
                    );
                }
                this.atributoPeriodoCargoPublico.requerido = toShow;
                break;
            case "par_direccion_laboral":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.direccion_laboral
                    );
                }
                this.atributoDireccionLaboral.requerido = toShow;
                break;
            case "par_tipo_cuenta":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.tipo_cuenta
                    );
                }
                this.atributoTipoCuenta.requerido = toShow;
                break;
            case "par_producto_asociado":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.producto_asociado
                    );
                }
                this.atributoProductoAsociado.requerido = toShow;
                break;
            case "par_tarjeta_nro":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.nro_tarjeta
                    );
                }
                this.atributoProductoAsociado.requerido = toShow;
                break;
            case "par_nro_solicitud_sci":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_solicitud.solicitud_sci_selected
                    );
                }
                this.atributoSolicitudSci.requerido = toShow;
                break;
            case "par_nro_cuotas":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.nro_cuotas
                    );
                }
                this.atributoNroCuotas.requerido = toShow;
                break;
            case "par_desc_ocupacion":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(
                        this.persona_banco_datos.desc_ocupacion
                    );
                }
                this.atributoDescOcupacion.requerido = toShow;
                break;
            case "par_monto":
                if (!form.controls[key].value) {
                    form.controls[key].setValue(this.persona_banco_datos.monto);
                }
                this.atributoMonto.requerido = toShow;
                break;
          case "par_operacion_plazo":
            if (!form.controls[key].value) {
              if (this.persona_banco_operacion) {
                form.controls[key].setValue(this.persona_banco_operacion.Operacion_Plazo);
              }
            }
            this.atributoSolicitudPlazoCredito.requerido = toShow;
            break;
          case "par_operacion_tipo_credito":
            if (!form.controls[key].value) {
              if (this.persona_banco_operacion) {
                form.controls[key].setValue(this.persona_banco_operacion.Operacion_Tipo_Credito);
              }
            }
            this.atributoTipoCredito.requerido = toShow;
            break;
          case "par_prima":
            if (!form.controls[key].value) {
              if (this.poliza.anexo_poliza) {
                form.controls[key].setValue(this.poliza.anexo_poliza.monto_prima);
              }
            }
            this.atributoPrima.requerido = toShow;
            break;
          case "par_solicitud_prima_total":
            if (!form.controls[key].value) {
              form.controls[key].setValue(this.persona_banco_solicitud.solicitud_prima_total);
            }
            this.atributoSolicitudPrimaTotal.requerido = toShow;
            break;
          case "par_forma_pago":
            if (!form.controls[key].value) {
              form.controls[key].setValue(this.persona_banco_solicitud.solicitud_forma_pago);
            }
            this.atributoFormaPago.requerido = toShow;
            break;
        }
    }

    async constructComponent(callback:any = null) {
      // this.isLoadingAgain = true;
      this.setUserLogin(async () => {
        this.getAllRoles(async () => {
          this.paramsDeleted = this.sessionStorageService.getItemSync("paramsDeleted");
          this.parametrosRuteo = this.sessionStorageService.getItemSync('parametros') as ParametroRuteo;
          this.userInfo = this.sessionStorageService.getItemSync('userInfo') as Usuario;
          this.id_poliza = parseInt(this.parametrosRuteo.parametro_vista);
          this.ruta = this.parametrosRuteo ? this.parametrosRuteo.ruta : null;
          this.ruta = this.ruta ? this.ruta.replace('/Nueva Solicitud', '') : '';
          this.ruta = this.ruta ? this.ruta.replace('/Gestionar', '') : '';
          this.ruta = this.ruta ? this.ruta.replace('PRODUCTOS/', '') : '';
          if (this.parametrosRuteo.ComponentesInvisibles && typeof this.parametrosRuteo.ComponentesInvisibles == 'string') {
            if(util.isJson(this.parametrosRuteo.ComponentesInvisibles)) {
              this.componentesInvisibles = JSON.parse(this.parametrosRuteo.ComponentesInvisibles);
            } else {
              this.componentesInvisibles = this.parametrosRuteo.ComponentesInvisibles;
            }

          } else if(this.parametrosRuteo.ComponentesInvisibles && this.parametrosRuteo.ComponentesInvisibles.length) {
            this.componentesInvisibles = this.parametrosRuteo.ComponentesInvisibles;
          } else if(typeof this.parametrosRuteo.ComponentesInvisibles == 'object' && Object.keys(this.parametrosRuteo.ComponentesInvisibles).length) {
            this.componentesInvisibles = this.parametrosRuteo.ComponentesInvisibles;
          } else {
            this.componentesInvisibles = []
          }
          // this.ruta = this.parametrosRuteo.ruta;
          this.breadcrumbService.setItems([
            {label: this.ruta}
          ]);
          if(typeof callback == 'function') {
            await callback();
          }
        });
      });
    }

    setCurrentPerfil(idsPerfiles:number[]) {
      if (this.userInfo) {
        for (let i = 0; i < this.userInfo.usuarioRoles.length; i++) {
          let usuarioRol:Rol = this.userInfo.usuarioRoles[i];
          this.currentPerfil = usuarioRol.rolPerfiles.find(param => idsPerfiles.includes(parseInt(param.id+'')));
          if (this.currentPerfil) {
            break;
          }
        }
      }
    }

    showFormValidationRequired(productForm: FormGroup) {
        Object.keys(productForm.controls).forEach((key) => {
            switch (key) {
                case "persona_fecha_nacimiento":
                    this.atributoFechaNacimiento.requerido = productForm
                        .controls[key].errors
                        ? true
                        : false;
                    break;
                case "par_ciudad_nacimiento_id":
                    this.atributoCiudadNacimiento.requerido = productForm
                        .controls[key].errors
                        ? true
                        : false;
                    break;
                case "par_pais_nacimiento_id":
                    this.atributoPaisNacimiento.requerido = productForm
                        .controls[key].errors
                        ? true
                        : false;
                    break;
                case "par_debito_automatico_id":
                    this.atributoDebitoAutomatico.requerido = productForm
                        .controls[key].errors
                        ? true
                        : false;
                    break;
                case "par_moneda":
                    this.atributoMoneda.requerido = productForm.controls[key]
                        .errors
                        ? true
                        : false;
                    break;
                case "par_numero_cuenta":
                    this.atributoNroCuenta.requerido = productForm.controls[key]
                        .errors
                        ? true
                        : false;
                    break;
                case "par_sexo_id":
                    this.atributoSexo.requerido = productForm.controls[key]
                        .errors
                        ? true
                        : false;
                    break;
                case "par_condicion_pep":
                    this.atributoCondicionPep.requerido = productForm.controls[
                        key
                    ].errors
                        ? true
                        : false;
                    break;
                case "par_tipo_documento_id":
                    this.atributoTipoDoc.requerido = productForm.controls[key]
                        .errors
                        ? true
                        : false;
                    break;
            }
        });
    }

    getFormValidationErrors(productForm: FormGroup) {
        Object.keys(productForm.controls).forEach((key) => {
            const controlErrors: ValidationErrors = productForm.get(key).errors;
            if (controlErrors != null) {
                Object.keys(controlErrors).forEach((keyError) => {
                    console.log(
                        "Key control: " +
                            key +
                            ", keyError: " +
                            keyError +
                            ", err value: ",
                        controlErrors[keyError]
                    );
                });
            }
        });
    }

    setFeatureValidacionAlInicioPersonaBanco(ignored = []) {
      let objKeysPersonaBanco = Object.assign(this.persona_banco);
      if (objKeysPersonaBanco) {
        this.msgs_warn = this.msgs_warn.filter(param => objKeysPersonaBanco[param.key]);
        this.msgs_error = this.msgs_error.filter(param => objKeysPersonaBanco[param.key]);
        if (this.asegurado ) {
            if (this.asegurado.instancia_poliza.id == null) {
                let personaBancoKeys = Object.keys(objKeysPersonaBanco);
                personaBancoKeys = personaBancoKeys.filter(param =>
                    param != 'manejo' &&
                    param != 'producto_asociado' &&
                    param != 'Operacion_Tipo_Credito'
                )
                let warnings = this.msgs_warn;
                for (let i = 0; i < warnings.length; i++) {
                    let msgWarn = warnings[i];
                    if (msgWarn.key) {
                      let personaBancoKey = personaBancoKeys.find((param) => param == msgWarn.key);
                      if (personaBancoKey) {
                        if (!msgWarn.key) {
                            this.msgs_warn.splice(i,1);
                        }
                      } else {
                        this.msgs_warn.splice(i,1);
                      }
                    } else {
                      this.msgs_warn.splice(i,1);
                    }
                }
                this.msgs_warn = this.msgs_error.filter((param) => { return param !== undefined || param !== null });
                let errors = this.msgs_error;
                for (let i = 0; i < errors.length; i++) {
                    let msgErr = errors[i];
                    if(msgErr.key) {
                      let personaBancoKey = personaBancoKeys.find((param) => param == msgErr.key);
                      if (personaBancoKey) {
                        if (!msgErr.key) {
                          this.msgs_error.splice(i,1);
                        }
                      } else {
                        this.msgs_error.splice(i,1);
                      }
                    } else {
                      this.msgs_error.splice(i,1);
                    }
                }
                this.msgs_error = this.msgs_error.filter((param) => { return param !== undefined || param !== null});
                if (this.msgs_warn.length || this.msgs_error.length) {
                  this.msgText = `El Cliente tiene las siguientes advertencias provenientes del banco`;
                  this.displayValidacionAlInicio = true;
                }
            }
        }
      }
    }

    async getAllPolizas(idsPolizas = []) {
        await this.polizaService.listaPolizas(idsPolizas).subscribe(
            async (res) => {
                let response = res as {
                    status: string;
                    message: string;
                    data: Poliza[];
                };
                this.polizas = response.data;
                if (this.polizas.length) {
                     this.polizas.forEach((poliza: Poliza) => {
                        switch (poliza.id + "") {
                            case "1":
                                this.polizaEcoAguinaldo = poliza;
                                break;
                            case "2":
                                this.polizaEcoPasanaku = poliza;
                                break;
                            case "3":
                                this.polizaEcoTarjetaCredito = poliza;
                                break;
                            case "4":
                                this.polizaEcoTarjetaDebito = poliza;
                                break;
                            case "5":
                                this.polizaEcoMedic = poliza;
                                break;
                            case "6":
                                this.polizaEcoVida = poliza;
                                break;
                            case "7":
                                this.polizaEcoProteccion = poliza;
                                break;
                            case "8":
                                this.polizaEcoMedicPlus = poliza;
                                break;
                            case "10":
                                this.polizaEcoAccidentes = poliza;
                                break;
                            case "12":
                                this.polizaEcoResguardo = poliza;
                                break;
                             case "13":
                                this.polizaEcoResguardo = poliza;
                                break;
                             case "14":
                                this.polizaEcoResguardo = poliza;
                                break;
                        }
                    });
                }
            },
            (err) => {
                if (
                    err.error.statusCode == 400 &&
                    err.error.message == "usuario no autentificado"
                ) {
                    this.router.navigate([""]);
                } else {
                    console.log(err);
                }
            }
        );
    }

    cancelarSolicitud() {
        this.msgs_warn = [];
        this.msgs_error = [];
        this.displayModalSolicitudExistente = false;
        this.displayModalSolicitudExistenteConRenovacion = false;
        this.asegurado = new Asegurado();
        this.persona_banco = new persona_banco();
        this.persona_banco_datos = new persona_banco_datos();
        this.persona_banco_account = new persona_banco_account();
        this.persona_banco_accounts = [];
        this.persona_banco_pep = new persona_banco_pep();
    }

    disableForm(form: FormGroup) {
        for (var control in form.controls) {
            if (form.controls[control]) {
                form.controls[control].disable();
            }
        }
        setTimeout(() => {
            $(".ui-message").css("display", "none");
        }, 400);
    }

    changeView(userform: FormGroup, asegurado: Asegurado, filtros:any = null) {
        if (this.editandoTitular) {
            this.editandoTitular = false;
            this.displayModalDatosTitular = true;
            this.setAtributosToPersonaBanco(userform);
        } else if (this.buscando) {
            this.displayBusquedaCI = true;
            this.cancelarSolicitud();
            // this.asegurado = new Asegurado();
        } else {
            this.cancelarSolicitud();
            if (this.fromGestionSolicitudes) {
                //this.verGestionSolicitudes();
                if (this.id_poliza + "" == "1") {
                    this.menuService.activaRuteoMenu("9", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "2") {
                    this.menuService.activaRuteoMenu("11", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "3") {
                    this.menuService.activaRuteoMenu("16", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "4") {
                    this.menuService.activaRuteoMenu("23", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "5") {
                    this.menuService.activaRuteoMenu("33", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "6") {
                    this.menuService.activaRuteoMenu("47", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "7") {
                    this.menuService.activaRuteoMenu("50", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "8") {
                    this.menuService.activaRuteoMenu("55", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "10") {
                    this.menuService.activaRuteoMenu("68", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "12") {
                    this.menuService.activaRuteoMenu("68", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "11") {
                    this.menuService.activaRuteoMenu("72", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "13") {
                    this.menuService.activaRuteoMenu("84", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
                if (this.id_poliza + "" == "14") {
                    this.menuService.activaRuteoMenu("91", null, {id_instancia_poliza:asegurado.id_instancia_poliza, filtros});
                }
            } else {
                this.displayBusquedaCI = true;
            }
        }
    }

    verGestionSolicitudes(asegurados: Asegurado[], filtros: any = null) {
        this.displayModalSolicitudExistente = false;
        this.displayModalSolicitudExistenteConRenovacion = false;

        //let url = 'http://localhost:4200/#/AppMain/GestionEcoAguinaldo';
        //window.location.href = url;
        //this.router.navigate(['/AppMain/GestionEcoAguinaldo',{solicitudes:JSON.stringify(this.solicitudes)}]);
        let ids = Array.from(
            asegurados,
            (asegurado) => asegurado.id_instancia_poliza
        );
        let viewId;
        if (this.id_poliza) {
            switch (this.id_poliza + "") {
                case "1":
                    viewId = "9";
                    break;
                case "2":
                    viewId = "11";
                    break;
                case "3":
                    viewId = "16";
                    break;
                case "4":
                    viewId = "23";
                    break;
                case "5":
                    viewId = "33";
                    break;
                case "6":
                    viewId = "47";
                    break;
                case "7":
                    viewId = "50";
                    break;
                case "8":
                    viewId = "55";
                    break;
                case "10":
                    viewId = "68";
                    break;
                case "12":
                    viewId = "68";
                    break;
                case "11":
                    viewId = "72";
                    break;
                case "13":
                    viewId = "84";
                    break;
                case "14":
                    viewId = "91";
                    break;
            }
            if (!filtros) {
              filtros = {};
            }
            filtros.id_instancia_poliza = ids;
            this.menuService.activaRuteoMenu(viewId, this.id_poliza, {id_instancia_poliza:ids, filtros});
        }
    }

    async getDatosFromSucursalService(callback: Function = null) {
        await this.soapuiService.getSucursal().subscribe(
            async (res) => {
                this.Sucursales = [
                    { label: "Seleccione una sucursal", value: null },
                ];
                let response = res as {
                    status: string;
                    message: string;
                    data: any;
                };
                if (
                    isObject(response.data) &&
                    Object.keys(response.data).length
                ) {
                    this.usuario_banco_sucursales = [];
                    let keys = Object.keys(response.data.Sucursal);
                    let values = Object.values(response.data.Sucursal);
                    await keys.forEach((key) => {
                        this.Sucursales.push({
                            label: values[key].descripcion,
                            value: values[key].id,
                        });
                        this.usuario_banco_sucursales.push(values[key]);
                        this.usuarioLogin.usuario_banco.sucursal = values[key];
                    });
                } else {
                    this.usuario_banco_sucursales = [];
                    this.Sucursales = [];
                }
                if (typeof callback == "function") {
                    await callback();
                }
            },
            (err) => {
                if (
                    err.error.statusCode == 400 &&
                    err.error.message == "usuario no autentificado"
                ) {
                    this.router.navigate([""]);
                } else {
                    console.log(err);
                }
            }
        );
    }

    async getDatosFromAgenciaService(callback: Function = null) {
        if (this.usuarioLogin.usuario_banco) {
            await this.soapuiService
                .getAgencia(this.usuarioLogin.usuario_banco.us_sucursal)
                .subscribe(
                    async (res) => {
                        this.Agencias = [
                            { label: "Seleccione una agencia", value: null },
                        ];
                        let response = res as {
                            status: string;
                            message: string;
                            data: any;
                        };
                        if (
                            isObject(response.data) &&
                            Object.keys(response.data).length
                        ) {
                            this.usuario_banco_agencias = [];
                            let keys = Object.keys(response.data.Agencia);
                            let values = Object.values(response.data.Agencia);
                            await keys.forEach((key) => {
                                this.Agencias.push({
                                    label: values[key].descripcion,
                                    value: values[key].id,
                                });
                                this.usuario_banco_agencias.push(values[key]);
                                this.usuarioLogin.usuario_banco.agencia = values[key];
                            });
                        } else {
                            this.usuario_banco_agencias = [];
                            this.Agencias = [];
                        }
                        if (typeof callback == "function") {
                            await callback();
                        }
                    },
                    (err) => {
                        if (
                            err.error.statusCode == 400 &&
                            err.error.message == "usuario no autentificado"
                        ) {
                            this.router.navigate([""]);
                        } else {
                            console.log(err);
                        }
                    }
                );
        }
    }

  async setUserLogin(callback:any = null) {
    await this.getAllRoles(async () => {
      this.usuarioLogin = this.sessionStorageService.getItemSync("userInfo") as Usuario;

      if (this.usuarioLogin.usuarioRoles.find((params) => params.id == 5)) {
        this.hasRolCredito = true;
      } else {
        this.hasRolCredito = false;
      }
      if (this.usuarioLogin.usuarioRoles.find((params) => params.id == 9)) {
        this.hasRolPlataforma = true;
      } else {
        this.hasRolPlataforma = false;
      }
      if (this.usuarioLogin.usuarioRoles.find((params) => params.id == 12)) {
        this.hasRolAdmin = true;
      } else {
        this.hasRolAdmin = false;
      }
      if (this.usuarioLogin.usuarioRoles.find((params) => params.id == 13)) {
        this.hasRolCajero = true;
      } else {
        this.hasRolCajero = false;
      }
      if (this.usuarioLogin.usuarioRoles.find((params) => params.id == 11)) {
        this.hasRolConsultaTarjetas = true;
      } else {
        this.hasRolConsultaTarjetas = false;
      }
      //if(this.usuarioLogin.usuario_x_rols.find(params => params.id_rol == 13)) {
      //    this.hasRolConsultaCajero = true;
      //}
      if (typeof callback == 'function') {
        await callback()
      }
    });
  }
    async getComponentes(callback: Function = null) {

    }

  async setIdPoliza(parametro_vista,callback: Function = null) {
    if (parametro_vista !== "null") {
      this.id_poliza = parametro_vista;
      this.fromGestionSolicitudes = true;
    } else {
      this.fromGestionSolicitudes = false;
    }
    if (typeof callback == "function") {
      callback();
    }
  }

  iniciarSolicitud() {
    this.msgs_warn = [];
    this.msgs_error = [];
  }

  setFeaturesBeneficiarios(beneficiarioResp: any) {
    if (beneficiarioResp != undefined) {
      if (beneficiarioResp.length) {
        beneficiarioResp.forEach(async (beneficiario: any) => {
          beneficiario.persona_doc_id_ext = beneficiario.entidad ? beneficiario.entidad.persona.persona_doc_id_ext
            : beneficiario.persona_doc_id_ext ? beneficiario.persona_doc_id_ext : "";
          beneficiario.persona_primer_apellido = beneficiario.entidad ? beneficiario.entidad.persona.persona_primer_apellido
            : beneficiario.persona_primer_apellido ? beneficiario.persona_primer_apellido : "";
          beneficiario.persona_segundo_apellido = beneficiario.entidad ? beneficiario.entidad.persona.persona_segundo_apellido
            : beneficiario.persona_segundo_apellido ? beneficiario.persona_segundo_apellido : "";
        });
      }
    }
  }


  async getAllRoles(callback: Function = null) {
        await this.rolesService.obtenerRoles().subscribe(async (res) => {
          let response = res as {
            status: string;
            message: string;
            data: Rol[];
          };
          // this.isLoadingAgain = false;
          await response.data.forEach(async (rol: Rol) => {
            switch (rol.id + "") {
              case "2":
                this.rolAdminPermisos1 = rol;
                break;
              case "3":
                this.rolAdminPermisos2 = rol;
                break;
              case "4":
                this.rolConsulta = rol;
                break;
              case "5":
                this.rolOficialCredito = rol;
                break;
              case "6":
                this.rolSupervisor = rol;
                break;
              case "8":
                this.rolAdminUsuarios = rol;
                break;
              case "9":
                this.rolOficialPlataforma = rol;
                break;
              case "10":
                this.rolOficialTarjeta = rol;
                break;
              case "13":
                this.rolOficialCajero = rol;
                break;
              }
            });
            // await this.componentsBehavior();
            if (typeof callback == "function") {
              callback();
            }
          }, (err) => {
          if (
              err.error.statusCode == 400 &&
              err.error.message == "usuario no autentificado"
          ) {
            window.location.replace("/");
          } else {
            console.log(err);
          }
        });
    }

    setDatesOfAsegurado() {
        console.log('setDatesOfAsegurado',this.asegurado.entidad.persona.persona_fecha_nacimiento);
        if (this.asegurado.entidad.persona.persona_fecha_nacimiento) {
          if (this.persona_banco.fecha_nacimiento_str) {
            if (this.persona_banco.fecha_nacimiento_str.indexOf('/') >= 0 ){
              this.asegurado.entidad.persona.persona_fecha_nacimiento = moment(this.persona_banco.fecha_nacimiento_str,"DD/MM/YYYY").toDate();
            } else {
              this.asegurado.entidad.persona.persona_fecha_nacimiento = new Date(this.persona_banco.fecha_nacimiento_str);
            }
          }
          if (isString(this.asegurado.entidad.persona.persona_fecha_nacimiento)) {
            this.persona_banco.fecha_nacimiento_str = this.asegurado.entidad.persona.persona_fecha_nacimiento+'';
            if ((this.asegurado.entidad.persona.persona_fecha_nacimiento+'').indexOf('/') >= 0) {
              [this.persona_banco.anio_fechanac,this.persona_banco.mes_fechanac,this.persona_banco.dia_fechanac] = this.persona_banco.fecha_nacimiento_str.substring(0,10).split('/');
              if (this.persona_banco.anio_fechanac.length == 2) {
                [this.persona_banco.dia_fechanac,this.persona_banco.mes_fechanac,this.persona_banco.anio_fechanac] = this.persona_banco.fecha_nacimiento_str.substring(0,10).split('/');
              }
            } else if((this.asegurado.entidad.persona.persona_fecha_nacimiento+'').indexOf('-') >= 0) {
              [this.persona_banco.anio_fechanac,this.persona_banco.mes_fechanac,this.persona_banco.dia_fechanac] = this.persona_banco.fecha_nacimiento_str.substring(0,10).split('-');
            }
          } else if(typeof this.asegurado.entidad.persona.persona_fecha_nacimiento.getDate == 'function'){
            this.persona_banco.dia_fechanac = this.asegurado.entidad.persona.persona_fecha_nacimiento.getDate()+'';
            this.persona_banco.mes_fechanac = (this.asegurado.entidad.persona.persona_fecha_nacimiento.getMonth()+1)+'';
            this.persona_banco.anio_fechanac = this.asegurado.entidad.persona.persona_fecha_nacimiento.getFullYear()+'';
          }
        }
        console.log(this.persona_banco.anio_fechanac ,this.persona_banco.mes_fechanac , this.persona_banco.dia_fechanac);
        if (this.persona_banco.anio_fechanac && this.persona_banco.mes_fechanac && this.persona_banco.dia_fechanac) {
            this.persona_banco.fecha_nacimiento = new Date(parseInt(this.persona_banco.anio_fechanac),parseInt(this.persona_banco.mes_fechanac)-1,parseInt(this.persona_banco.dia_fechanac));
            this.persona_banco.fecha_nacimiento_str = this.persona_banco.fecha_nacimiento.getDate()+'/'+(this.persona_banco.fecha_nacimiento.getMonth()+1)+'/'+this.persona_banco.fecha_nacimiento.getFullYear();
            this.asegurado.entidad.persona.persona_fecha_nacimiento = this.persona_banco.fecha_nacimiento;
        } else if(this.persona_banco.fecha_nacimiento_str) {
            [this.persona_banco.dia_fechanac,this.persona_banco.mes_fechanac,this.persona_banco.anio_fechanac] = this.persona_banco.fecha_nacimiento_str.split('/');
            this.persona_banco.fecha_nacimiento = new Date(parseInt(this.persona_banco.anio_fechanac),parseInt(this.persona_banco.mes_fechanac)-1,parseInt(this.persona_banco.dia_fechanac));
            this.persona_banco.fecha_nacimiento_str = this.persona_banco.fecha_nacimiento.getDate()+'/'+(this.persona_banco.fecha_nacimiento.getMonth()+1)+'/'+this.persona_banco.fecha_nacimiento.getFullYear();
            this.asegurado.entidad.persona.persona_fecha_nacimiento = this.persona_banco.fecha_nacimiento;
        }
        console.log('fin setDates',this.persona_banco.fecha_nacimiento,this.persona_banco.fecha_nacimiento_str,this.asegurado.entidad.persona.persona_fecha_nacimiento)
        if (this.asegurado.instancia_poliza.instancia_poliza_transicions) {
            if (this.asegurado.instancia_poliza.instancia_poliza_transicions.length) {
                this.asegurado.instancia_poliza.instancia_poliza_transicions.forEach(
                    (transicion) => {
                        if (isString(transicion.createdAt) && isString(transicion.updatedAt)) {
                            transicion.createdAt = new Date(transicion.createdAt);
                            transicion.updatedAt = new Date(transicion.updatedAt);
                        }
                    }
                );
            }
        }
        this.asegurado.instancia_poliza.fecha_registro = this.asegurado.instancia_poliza.fecha_registro != null ? new Date(this.asegurado.instancia_poliza.fecha_registro+'') : new Date();
        if (this.asegurado.instancia_poliza.instancia_documentos.length) {
            this.asegurado.instancia_poliza.instancia_documentos.forEach(
                async (instancia_documento: Instancia_documento) => {
                    instancia_documento.fecha_inicio_vigencia = instancia_documento && instancia_documento.fecha_inicio_vigencia != null ? new Date(instancia_documento.fecha_inicio_vigencia+'') : null;
                    instancia_documento.fecha_fin_vigencia = instancia_documento && instancia_documento.fecha_fin_vigencia != null ? new Date(instancia_documento.fecha_fin_vigencia+'') : null;
                    instancia_documento.fecha_emision = instancia_documento && instancia_documento.fecha_emision != null ? new Date(instancia_documento.fecha_emision+'') : null;
                }
            );
        }
        return this.asegurado;
    }

    async setFromObjetoAtributoToAtributoInstanciaDocumento(callback: Function = null, idAtributosInclude = [], idAtributosExclude = []) {
      try {
        let titularOPersona = "";
        let ObjetoAtributos: Objeto_x_atributo[] = [];
        if (idAtributosInclude.length) {
          ObjetoAtributos = this.ObjetoAtributosDoc.filter((param: Objeto_x_atributo) => idAtributosInclude.includes(parseInt(param.id_atributo + "")));
        } else if (idAtributosExclude.length) {
          ObjetoAtributos = this.ObjetoAtributosDoc.filter((param: Objeto_x_atributo) => !idAtributosExclude.includes(parseInt(param.id_atributo + "")));
        } else {
          ObjetoAtributos = this.ObjetoAtributosDoc;
        }
        if (this.asegurado.instancia_poliza.id) {
          titularOPersona = `El titular `;
        } else {
          titularOPersona = `La persona `;
        }
        if (ObjetoAtributos) {

          for (let i = 0; i < ObjetoAtributos.length; i++) {
            let objetoAtributo = ObjetoAtributos[i];
            for (let j = 0; j < this.asegurado.instancia_poliza.instancia_documentos.length; j++) {
              let instanciaDocumento: Instancia_documento = this.asegurado.instancia_poliza.instancia_documentos[j];
              let atributosInstanciaDocumento = instanciaDocumento.atributo_instancia_documentos;
              let atributoInstanciaDocumentoFound = atributosInstanciaDocumento.find((param) => param.id_objeto_x_atributo == objetoAtributo.id);
              if (!atributoInstanciaDocumentoFound) {
                atributoInstanciaDocumentoFound = new Atributo_instancia_documento();
              }
              atributoInstanciaDocumentoFound.id_instancia_documento = instanciaDocumento.id;
              instanciaDocumento.atributo_instancia_documentos = [];
              for (let k = 0; k < this.persona_banco_datos.cuestionario.preguntas_cerradas.length; k++) {
                let preguntaCerrada = this.persona_banco_datos.cuestionario.preguntas_cerradas[k];
                if (objetoAtributo.id_atributo == 96) {
                  atributoInstanciaDocumentoFound.valor = preguntaCerrada && preguntaCerrada.respuestas && preguntaCerrada.respuestas.length ? JSON.stringify(preguntaCerrada.respuestas) : null;
                  atributoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaDocumentoFound.valor) {
                    atributoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la pregunta "Usted padece de alguna de las siguientes enfermedades" Contestada: ${atributoInstanciaDocumentoFound.valor}`;
                  } else {
                    atributoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la pregunta "Usted padece de alguna de las siguientes enfermedades" Contestada`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaDocumentoFound.tipo_error, atributoInstanciaDocumentoFound.valor, this.parametroWarning, this.showPregunta1, `pregunta_1`);
                }
                if (objetoAtributo.id_atributo == 105) {
                  atributoInstanciaDocumentoFound.valor = preguntaCerrada && preguntaCerrada.respuestas && preguntaCerrada.respuestas.length ? JSON.stringify(preguntaCerrada.respuestas) : null;
                  atributoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaDocumentoFound.valor) {
                    atributoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la pregunta "Practica algún deporte a nivel profesional o considerado de alto riesgo? (Alpinismo, automovilismo, etc)" Contestada: ${atributoInstanciaDocumentoFound.valor}`;
                  } else {
                    atributoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la pregunta "Practica algún deporte a nivel profesional o considerado de alto riesgo? (Alpinismo, automovilismo, etc)" Contestada`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaDocumentoFound.tipo_error, atributoInstanciaDocumentoFound.valor, this.parametroWarning, this.showPregunta2, `pregunta_2`);
                }

                instanciaDocumento.atributo_instancia_documentos.push(atributoInstanciaDocumentoFound);

                let atributoHijoInstanciaDocumentoFound = new Atributo_instancia_documento();
                atributoHijoInstanciaDocumentoFound.id_instancia_documento = instanciaDocumento.id;

                for (let l = 0; l < preguntaCerrada.opciones.length; l++) {
                  let preguntaCerradaOpcion = preguntaCerrada.opciones[l];
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 97)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion Cancer seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion cancer seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionCancer, `opcion_1`);
                  }
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 98)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion Diabetes seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion Diabetes seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionDiabetes, `opcion_2`);
                  }
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 99)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion Insuficiencia Renal seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion Insuficiencia Renal seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionInsuficienciaRenal, `opcion_3`);
                  }
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 100)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion COVID-19 seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion COVID-19 seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionCovid19, `opcion_4`);
                  }
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 101)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion Sida seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion Sida seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionSida, `opcion_5`);
                  }
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 102)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion Enfermedades del Corazon seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion Enfermedades del Corazon seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionEnfermedadesCorazon, `opcion_6`);
                  }
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 103)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion Enferemedades Cerebro Vasculares seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion Enferemedades Cerebro Vasculares seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionEnferemedadesCerebroVasculares, `opcion_7`);
                  }
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 104)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion Enfermedades Pulmonar Crónica Obstructiva seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion Enfermedades Pulmonar Crónica Obstructiva seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionEnfermedadesPulmonarCrónicaObstructiva, `opcion_8`);
                  }
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 106)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion SI seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion SI seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionSI, `opcion_1`);
                  }
                  if (objetoAtributo.atributo.atributoHijos.find(param => param.id == 107)) {
                    atributoHijoInstanciaDocumentoFound.valor = preguntaCerradaOpcion ? preguntaCerradaOpcion.value+'' : null;
                    atributoHijoInstanciaDocumentoFound.objeto_x_atributo = objetoAtributo;
                    atributoHijoInstanciaDocumentoFound.id_objeto_x_atributo = objetoAtributo.id;
                    if (atributoHijoInstanciaDocumentoFound.valor) {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `tiene la opcion NO seleccionada: ${atributoHijoInstanciaDocumentoFound.valor}`;
                    } else {
                      atributoHijoInstanciaDocumentoFound.tipo_error = titularOPersona + `no tiene la opcion NO seleccionada`;
                    }
                    await this.setAtributoObservacion(this.asegurado, atributoHijoInstanciaDocumentoFound.tipo_error, atributoHijoInstanciaDocumentoFound.valor, this.parametroWarning, this.showOpcionNO, `opcion_2`);
                  }
                  instanciaDocumento.atributo_instancia_documentos.push(atributoHijoInstanciaDocumentoFound);
                }
              }
            }
          }
        }
      } catch (e) {
        console.log(e)
      }
    }
    async setFromObjetoAtributoToAtributoInstanciaPoliza(callback: Function = null, idAtributosInclude = [], idAtributosExclude = []) {
        try {
          if (this.showPagoEfectivo) {
            if (this.atributoDebitoAutomatico && this.atributoDebitoAutomatico.valor == this.pagoEfectivo.id + "") {
              this.showNroCuenta = false;
              this.showMoneda = false;
            } else if(this.atributoDebitoAutomatico && this.atributoDebitoAutomatico.valor == this.pagoDebitAutomatico.id + ""){
              this.showNroCuenta = true;
              this.showMoneda = true;
            }
          }
          let ObjetoAtributos: Objeto_x_atributo[] = [];
          if (idAtributosInclude.length) {
            ObjetoAtributos = this.ObjetoAtributos.filter((param: Objeto_x_atributo) => idAtributosInclude.includes(parseInt(param.id_atributo + "")));
          } else if (idAtributosExclude.length) {
            ObjetoAtributos = this.ObjetoAtributos.filter((param: Objeto_x_atributo) => !idAtributosExclude.includes(parseInt(param.id_atributo + "")));
          } else {
            ObjetoAtributos = this.ObjetoAtributos;
          }
          // if (![this.estadoSolicitado.id,this.estadoPorPagar.id,this.estadoEmitido.id+''].includes(this.asegurado.instancia_poliza.id_estado)) {
          //     idAtributos = ["64", "65", "66", "67", "69"];
          //     this.ObjetoAtributos = this.ObjetoAtributos.filter((param: Objeto_x_atributo) => !idAtributos.includes(param.id_atributo + ""));
          // }
          let atributoInstanciaPolizas = this.asegurado.instancia_poliza.atributo_instancia_polizas;
          this.asegurado.instancia_poliza.atributo_instancia_polizas = [];
          let titularOPersona = "";
          if (this.asegurado.instancia_poliza.id) {
            titularOPersona = `El titular `;
          } else {
            titularOPersona = `La persona `;
          }
          for (let i = 0; i < ObjetoAtributos.length; i++) {
            let objetoAtributo = ObjetoAtributos[i];
            let atributoInstanciaPolizaFound = atributoInstanciaPolizas.find((param) => param.id_objeto_x_atributo == objetoAtributo.id);
            //let atributoInstanciaPolizaFoundIndex = atributoInstanciaPolizas.findIndex((param) => param.id_objeto_x_atributo == objetoAtributo.id);
            if (!atributoInstanciaPolizaFound) {
              atributoInstanciaPolizaFound = new Atributo_instancia_poliza();
            }
            if (atributoInstanciaPolizaFound.objeto_x_atributo && objetoAtributo.atributo) {
              atributoInstanciaPolizaFound.objeto_x_atributo.atributo.descripcion = objetoAtributo.atributo.descripcion;
            }
            atributoInstanciaPolizaFound.id_instancia_poliza = this.asegurado.instancia_poliza.id;
            if (objetoAtributo.atributo.id == 10) {
              atributoInstanciaPolizaFound.valor = this.persona_banco.cod_agenda ? this.persona_banco.cod_agenda+'' : null;
              atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
              atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
              if (atributoInstanciaPolizaFound.valor) {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un codigo de agenda: ${atributoInstanciaPolizaFound.valor}`;
              } else {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un codigo de agenda`;
              }
              await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showCodAgenda, `cod_agenda`);
            } else if (objetoAtributo.atributo.id == 5) {
              if (this.id_poliza == 4) {
                if (this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected && this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.nro_cuenta == `` && this.atributoNroCuenta.valor != ``) {
                  this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.nro_cuenta = this.atributoNroCuenta.valor;
                }
                atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected ? this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.nro_cuenta : '';
              } else {
                if (!this.persona_banco_account.nrocuenta && this.atributoNroCuenta.valor != ``) {
                  this.persona_banco_account.nrocuenta = this.atributoNroCuenta.valor;
                }
                atributoInstanciaPolizaFound.valor = this.persona_banco_account.nrocuenta ? this.persona_banco_account.nrocuenta : null;
              }
              atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
              atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
              this.atributoNroCuenta = atributoInstanciaPolizaFound;
              if (atributoInstanciaPolizaFound.valor) {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un numero de cuenta: ${atributoInstanciaPolizaFound.valor}`;
              } else {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un numero de cuenta`;
              }
              await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, ['10', '12'].includes(this.poliza.id + '') ? this.parametroWarning : this.parametroError, this.showNroCuenta, 'nrocuenta');
            } else if (objetoAtributo.atributo.id == 12) {
              if (!this.persona_banco.debito_automatico && this.atributoDebitoAutomatico.valor != ``) {
                this.persona_banco.debito_automatico = this.atributoDebitoAutomatico.valor;
              }
              atributoInstanciaPolizaFound.valor = this.persona_banco.debito_automatico ? this.persona_banco.debito_automatico : null;
              atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
              atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
              this.atributoDebitoAutomatico = atributoInstanciaPolizaFound;
              if (atributoInstanciaPolizaFound.valor) {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado si el debito sera automatico: ${this.CondicionesCod[atributoInstanciaPolizaFound.valor]}`;
              } else {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado si el debito sera automatico`;
              }
              await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showPagoEfectivo, 'debito_automatico');
            } else if (objetoAtributo.atributo.id == 14) {
              atributoInstanciaPolizaFound.valor = this.persona_banco.caedec ? this.persona_banco.caedec : null;
              atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
              atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
              if (atributoInstanciaPolizaFound.valor) {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un numero CAEDEC: ${atributoInstanciaPolizaFound.valor}`;
              } else {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un numero CAEDEC`;
              }
              await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showCaedec, `caedec`);
            } else if (objetoAtributo.atributo.id == 16) {
              atributoInstanciaPolizaFound.valor = this.persona_banco.localidad ? this.persona_banco.localidad : null;
              atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
              atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
              this.atributoLocalidad = atributoInstanciaPolizaFound;
              if (atributoInstanciaPolizaFound.valor) {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado una localidad: ${atributoInstanciaPolizaFound.valor}`;
              } else {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una localidad`;
              }
              await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showLocalidad, `localidad`);
            } else if (objetoAtributo.atributo.id == 17) {
              atributoInstanciaPolizaFound.valor = this.persona_banco.departamento ? this.persona_banco.departamento : null;
              atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
              atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
              this.atributoDepartamento = atributoInstanciaPolizaFound;
              if (atributoInstanciaPolizaFound.valor) {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un departamento: ${atributoInstanciaPolizaFound.valor}`;
              } else {
                atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un departamento`;
              }
              await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showDepartamento, `departamento`);
            } else if (objetoAtributo.atributo.id == 18) {
              atributoInstanciaPolizaFound.valor = this.persona_banco.cod_sucursal;
              atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
              atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
              if (atributoInstanciaPolizaFound.valor) {
                atributoInstanciaPolizaFound.tipo_error = `El titular tiene registrado un codigo de sucursal: ${atributoInstanciaPolizaFound.valor}`;
              } else {
                atributoInstanciaPolizaFound.tipo_error = `El titular no tiene registrado un codigo de sucursal`;
              }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showCodSucursal, `cod_sucursal`);
                } else if (objetoAtributo.atributo.id == 19) {
                  if (!this.persona_banco_datos.tipo_doc && this.atributoTipoDoc.valor != ``) {
                    this.persona_banco_datos.tipo_doc = this.atributoTipoDoc.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.tipo_doc ? this.persona_banco_datos.tipo_doc : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  this.atributoTipoDoc = atributoInstanciaPolizaFound;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado el tipo de documento utilizado: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado el tipo de documento utilizado`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTipoDocumento, 'tipo_doc');
                } else if (objetoAtributo.atributo.id == 4) {// if (this.persona_banco.e_mail == '' && this.atributoEmail.valor != '') {
                  //     this.persona_banco.e_mail = this.atributoEmail.valor;
                  // }
                  atributoInstanciaPolizaFound.valor = this.persona_banco.e_mail ? this.persona_banco.e_mail : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  this.atributoEmail = atributoInstanciaPolizaFound;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado su e-mail: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado su e-mail`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showEmail, `e_mail`);
                } else if (objetoAtributo.atributo.id == 3) {
                  atributoInstanciaPolizaFound.valor = this.persona_banco.est_civil ? this.persona_banco.est_civil : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado su estado civil: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error =
                        titularOPersona + `no tiene registrado su estado civil`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showEstadoCivil, `est_civil`);
                } else if (objetoAtributo.atributo.id == 20) {
                  atributoInstanciaPolizaFound.valor = this.persona_banco_account ? this.persona_banco_account.manejo + `` : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un codigo de manejo: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un codigo de manejo`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showManejo, `manejo`);
                } else if (objetoAtributo.atributo.id == 21) {
                  if (this.id_poliza == 4) {
                    if (this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected && this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.moneda == `` && this.atributoMoneda.valor != ``) {
                      this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.moneda = this.atributoMoneda.valor;
                    }
                    atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected ? this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.moneda + `` : '';
                  } else {
                    if (!this.persona_banco_account.moneda && this.atributoMoneda.valor != ``) {
                      this.persona_banco_account.moneda = this.atributoMoneda.valor;
                    }
                    atributoInstanciaPolizaFound.valor = this.persona_banco_account.moneda + ``;
                  }
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un tipo de moneda: ${this.MonedasParametroCod[atributoInstanciaPolizaFound.valor]}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un tipo de moneda`;
                  }
                  this.atributoMoneda = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, ['10', '12'].includes(this.poliza.id + '') ? this.parametroWarning : this.parametroError, this.showMoneda,`moneda`);
                } else if (objetoAtributo.atributo.id == 22) {
                  atributoInstanciaPolizaFound.valor = this.persona_banco.desc_caedec ? this.persona_banco.desc_caedec : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado una descripción en caedec: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una descripción en caedec`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showDescCaedec, `desc_caedec`);
                } else if (objetoAtributo.atributo.id == 23) {
                  if (this.id_poliza == 4) {
                    if (!this.persona_banco_tarjeta_debito_cuenta_vinculada.nro_tarjeta && this.atributoNroTarjeta.valor != ``) {
                      this.persona_banco_tarjeta_debito_cuenta_vinculada.nro_tarjeta = this.atributoMoneda.valor;
                    }
                    atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_debito_cuenta_vinculada.nro_tarjeta + ``;
                  } else {
                    if (!this.persona_banco_tarjeta_ci_ext.nrotarjeta && this.atributoNroTarjeta.valor != ``) {
                      this.persona_banco_tarjeta_ci_ext.nrotarjeta = this.atributoNroTarjeta.valor;
                    }
                    if (!this.persona_banco_datos.nro_tarjeta && this.atributoNroTarjeta.valor != ``) {
                      this.persona_banco_datos.nro_tarjeta = this.atributoNroTarjeta.valor;
                    }
                    atributoInstanciaPolizaFound.valor = this.persona_banco_datos.nro_tarjeta ? this.persona_banco_datos.nro_tarjeta : this.persona_banco_tarjeta_ci_ext.nrotarjeta ? this.persona_banco_tarjeta_ci_ext.nrotarjeta : '';
                  }
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  this.atributoNroTarjeta = atributoInstanciaPolizaFound;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un Nro de tarjeta: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un Nro de tarjeta`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTarjetaNro, 'nro_tarjeta');
                } else if (objetoAtributo.atributo.id == 24) {
                  atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_credito.tarjeta_nombre ? this.persona_banco_tarjeta_credito.tarjeta_nombre : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un nombre de tarjeta: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un nombre de tarjeta`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTarjetaNombre, 'tarjeta_nombre');
                } else if (objetoAtributo.atributo.id == 25) {
                  atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_credito.valida ? this.persona_banco_tarjeta_credito.valida : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado si su tarjeta es valida`;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTarjetaValida);
                  if (atributoInstanciaPolizaFound.valor == `N`) {
                    atributoInstanciaPolizaFound.tipo_error = `La tarjeta de la persona es invalida`;
                    await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTarjetaValida, 'valida');
                    await this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, atributoInstanciaPolizaFound.tipo_error);
                  }
                } else if (objetoAtributo.atributo.id == 28) {
                  if (!this.persona_banco_datos.modalidad_pago && this.atributoModalidadPago.valor != ``) {
                    this.persona_banco_datos.modalidad_pago = this.atributoModalidadPago.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.modalidad_pago ? this.persona_banco_datos.modalidad_pago : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una modalidad de pago`;
                  this.atributoModalidadPago = atributoInstanciaPolizaFound;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado una modalidad de pago: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una modalidad de pago`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showModalidadPago, 'modalidad_pago');
                } else if (objetoAtributo.atributo.id == 29) {
                  if (!this.persona_banco_datos.zona && this.atributoZona.valor != ``) {
                    this.persona_banco_datos.zona = this.atributoZona.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.zona ? this.persona_banco_datos.zona : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una zona`;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado una zona: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una zona`;
                  }
                  this.atributoZona = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showZona, 'zona');
                } else if (objetoAtributo.atributo.id == 30) {
                  if (!this.persona_banco_datos.nro_direccion && this.atributoNroDireccion.valor != ``) {
                    this.persona_banco_datos.nro_direccion = this.atributoNroDireccion.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.nro_direccion ? this.persona_banco_datos.nro_direccion : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  this.atributoNroDireccion = atributoInstanciaPolizaFound;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un Nro de direccion: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un Nro de direccion`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showNroDireccion, 'nro_direccion');
                } else if (objetoAtributo.atributo.id == 32) {
                  if (!this.persona_banco_datos.razon_social && this.atributoRazonSocial.valor != ``) {
                    this.persona_banco_datos.razon_social = this.atributoRazonSocial.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.razon_social ? this.persona_banco_datos.razon_social : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una razon social`;
                  this.atributoRazonSocial = atributoInstanciaPolizaFound;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado una razon socia ${atributoInstanciaPolizaFound.valor}l`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una razon social`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showRazonSocial, 'razon_social');
                } else if (objetoAtributo.atributo.id == 33) {
                  if (!this.persona_banco_datos.nit_carnet && this.atributoNitCarnet.valor != ``) {
                    this.persona_banco_datos.nit_carnet = this.atributoNitCarnet.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.nit_carnet ? this.persona_banco_datos.nit_carnet + '' : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  this.atributoNitCarnet = atributoInstanciaPolizaFound;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un Nit o Carnet: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un Nit o Carnet`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning,this.showNitCarnet, 'nit_carnet');
                } else if (objetoAtributo.atributo.id == 34) {
                  if (!this.persona_banco_tarjeta_debito.tarjeta_ultimos_cuatro_digitos && this.atributoUltimosCuatroDigitos.valor != ``) {
                    this.persona_banco_tarjeta_debito.tarjeta_ultimos_cuatro_digitos = this.atributoUltimosCuatroDigitos.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_debito.tarjeta_ultimos_cuatro_digitos ? this.persona_banco_tarjeta_debito.tarjeta_ultimos_cuatro_digitos + '' : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  this.atributoUltimosCuatroDigitos = atributoInstanciaPolizaFound;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado los ultimos cuatro digitos de su número de tarjeta: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado los ultimos cuatro digitos de su número de tarjeta`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showTarjetaUltimosCuatroDigitos, 'tarjeta_ultimos_cuatro_digitos');
                } else if (objetoAtributo.atributo.id == 35) {
                  if (!this.persona_banco_tarjeta_debito.fecha_expiracion && this.atributoCtaFechaExpiracion.valor != ``) {
                    this.persona_banco_tarjeta_debito.fecha_expiracion = new Date(this.atributoCtaFechaExpiracion.valor);
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_account.fecha_expiracion ? this.persona_banco_account.fecha_expiracion + `` : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado la fecha de expiración de su nro de cuenta: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado la fecha de expiración de su nro de cuenta`;
                  }
                  this.atributoCtaFechaExpiracion = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showCuentaFechaExpiracion, `fecha_expiracion`);
                } else if (objetoAtributo.atributo.id == 36) {
                  let key;
                  if (this.id_poliza == 4) {
                    if (!this.persona_banco_tarjeta_debito_cuenta_vinculada.tipo && this.atributoTipoTarjeta.valor != ``) {
                      this.persona_banco_tarjeta_debito_cuenta_vinculada.tipo = this.atributoTipoTarjeta.valor;
                    }
                    atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_debito_cuenta_vinculada.tipo ? this.persona_banco_tarjeta_debito_cuenta_vinculada.tipo + `` : null;
                    key = 'tipo';
                  } else {
                    if (!this.persona_banco_datos_tarjeta.tipo_tarjeta && this.atributoTipoTarjeta.valor != ``) {
                      this.persona_banco_datos_tarjeta.tipo_tarjeta = this.atributoTipoTarjeta.valor;
                    }
                    atributoInstanciaPolizaFound.valor = this.persona_banco_datos_tarjeta.tipo_tarjeta ? this.persona_banco_datos_tarjeta.tipo_tarjeta +'' : null;
                    key = 'tipo_tarjeta';
                  }
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado el tipo de tarjeta de debito: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado el tipo de tarjeta de debito`;
                  }
                  this.atributoTipoTarjeta = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showTipoTarjeta, key);
                } else if (objetoAtributo.atributo.id == 37) {
                  if (!this.persona_banco_datos.ocupacion && this.atributoOcupacion.valor != ``) {
                    this.persona_banco_datos.ocupacion = this.atributoOcupacion.valor;
                  }
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.ocupacion ? this.persona_banco_datos.ocupacion : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado una ocupación: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una ocupación`;
                  }
                  this.atributoOcupacion = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showOcupacion, 'ocupacion');
                } else if (objetoAtributo.atributo.id == 38) {
                  if (!this.persona_banco_datos.plan && this.atributoPlan.valor != ``) {
                    this.persona_banco_datos.plan = parseInt(this.atributoPlan.valor);
                  }
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (this.persona_banco_datos.plan) {
                    atributoInstanciaPolizaFound.valor = this.persona_banco_datos.plan ? this.persona_banco_datos.plan + `` : null;
                  }
                  this.atributoPlan = atributoInstanciaPolizaFound;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un plan: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un plan`;
                  }
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showPlan, 'plan');
                } else if (objetoAtributo.atributo.id == 58) {
                  if (!this.persona_banco_datos.plazo && this.atributoPlazo.valor != ``) {
                    this.persona_banco_datos.plazo = parseInt(this.atributoPlazo.valor);
                  }
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (this.persona_banco_datos.plazo) {
                    atributoInstanciaPolizaFound.valor = this.persona_banco_datos.plazo ? this.persona_banco_datos.plazo + `` : null;
                  }
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado un plazo: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un plazo`;
                  }
                  this.atributoPlazo = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showPlazo, 'plazo');
                } else if (objetoAtributo.atributo.id == 60) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  atributoInstanciaPolizaFound.tipo_error = `El usuario del banco no tiene registrado una sucursal`;
                  if (this.usuarioLogin.usuario_banco) {
                    if (!this.usuarioLogin.usuario_banco.us_sucursal && this.atributoSucursal.valor != ``) {
                      this.usuarioLogin.usuario_banco.us_sucursal = parseInt(this.atributoSucursal.valor);
                    }
                    if (!atributoInstanciaPolizaFound.valor && this.usuarioLogin.usuario_banco.us_sucursal) {
                      atributoInstanciaPolizaFound.valor = this.usuarioLogin.usuario_banco.us_sucursal ? this.usuarioLogin.usuario_banco.us_sucursal + `` : null;
                    }
                    if (!this.asegurado.instancia_poliza.id && this.usuarioLogin.usuario_banco.us_sucursal) {
                      atributoInstanciaPolizaFound.valor = this.usuarioLogin.usuario_banco.us_sucursal ? this.usuarioLogin.usuario_banco.us_sucursal + `` : null;
                    }
                    if (atributoInstanciaPolizaFound.valor) {
                      atributoInstanciaPolizaFound.tipo_error = `El usuario del banco tiene registrado una sucursal: ${this.SucursalesParametroCod[atributoInstanciaPolizaFound.valor]}`;
                    } else {
                      atributoInstanciaPolizaFound.tipo_error = `El usuario del banco no tiene registrado una sucursal`;
                    }
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `El usuario del banco no tiene registrado una sucursal`;
                  }
                  this.atributoSucursal = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showSucursal, 'us_sucursal');
                } else if (objetoAtributo.atributo.id == 59) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (this.usuarioLogin.usuario_banco) {
                    if (!this.usuarioLogin.usuario_banco.us_oficina && this.atributoAgencia.valor != ``) {
                      this.usuarioLogin.usuario_banco.us_oficina = parseInt(this.atributoAgencia.valor);
                    }
                    if (!atributoInstanciaPolizaFound.valor && this.usuarioLogin.usuario_banco.us_oficina) {
                      atributoInstanciaPolizaFound.valor = this.usuarioLogin.usuario_banco.us_oficina ? this.usuarioLogin.usuario_banco.us_oficina + `` : null;
                    }
                    if (!this.asegurado.instancia_poliza.id && this.usuarioLogin.usuario_banco.us_oficina) {
                      atributoInstanciaPolizaFound.valor = this.usuarioLogin.usuario_banco.us_oficina ? this.usuarioLogin.usuario_banco.us_oficina + `` : null;
                    }
                    if (atributoInstanciaPolizaFound.valor) {
                      atributoInstanciaPolizaFound.tipo_error = `El usuario del banco tiene registrado una agencia: ${this.AgenciasParametroCod[atributoInstanciaPolizaFound.valor]}`;
                    } else {
                      atributoInstanciaPolizaFound.tipo_error = `El usuario del banco no tiene registrado una agencia`;
                    }
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `El usuario del banco no tiene registrado una agencia`;
                  }
                  this.atributoAgencia = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showAgencia, 'us_oficina');
                } else if (objetoAtributo.atributo.id == 70) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (this.usuarioLogin.usuario_banco) {
                    if (!this.usuarioLogin.usuario_banco.us_cargo && this.atributoUsuarioCargo.valor != ``) {
                      this.usuarioLogin.usuario_banco.us_cargo = this.atributoUsuarioCargo.valor;
                    }
                    if (!atributoInstanciaPolizaFound.valor && this.usuarioLogin.usuario_banco.us_cargo) {
                      atributoInstanciaPolizaFound.valor = this.usuarioLogin.usuario_banco.us_cargo ? this.usuarioLogin.usuario_banco.us_cargo + `` : null;
                    }
                    if (!this.asegurado.instancia_poliza.id && this.usuarioLogin.usuario_banco.us_cargo) {
                      atributoInstanciaPolizaFound.valor = this.usuarioLogin.usuario_banco.us_cargo ? this.usuarioLogin.usuario_banco.us_cargo + `` : null;
                    }
                    if (atributoInstanciaPolizaFound.valor) {
                      atributoInstanciaPolizaFound.tipo_error = `El usuario del banco tiene registrado un cargo: ${atributoInstanciaPolizaFound.valor}`;
                    } else {
                      atributoInstanciaPolizaFound.tipo_error = `El usuario del banco no tiene registrado un cargo`;
                    }
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `El usuario del banco no tiene registrado un cargo`;
                  }
                  this.atributoUsuarioCargo = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showUsuarioCargo,'us_cargo');
                }
                  else if (objetoAtributo.atributo.id == 1) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_datos.prima && this.atributoPrima.valor != ``) {
                    this.persona_banco_datos.prima = parseInt(this.atributoPrima.valor);
                  }
                  if (this.persona_banco_datos.prima) {
                    atributoInstanciaPolizaFound.valor = this.persona_banco_datos.prima ? this.persona_banco_datos.prima + `` : this.poliza.anexo_poliza.monto_prima ? this.poliza.anexo_poliza.monto_prima+'' : null;
                  }
                  if (!atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.valor = this.poliza.anexo_poliza.monto_prima+'';
                  }
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado una prima: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una prima`;
                  }
                  this.atributoPrima = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showPrima, 'prima');
                }
                  else if (objetoAtributo.atributo.id == 62) {
                  if (!this.persona_banco_datos.telefono_celular && this.atributoTelefonoCelular.valor != ``) {
                    this.persona_banco_datos.telefono_celular = this.atributoTelefonoCelular.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.telefono_celular ? this.persona_banco_datos.telefono_celular + `` : null;
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = `La persona tiene registrado un telefono de celular: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `La persona no tiene registrado un telefono de celular`;
                  }
                  this.atributoTelefonoCelular = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showTelefonoCelular, 'telefono_celular');
                } else if (objetoAtributo.atributo.id == 63) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_datos.ciudad_nacimiento && this.atributoCiudadNacimiento.valor != ``) {
                    this.persona_banco_datos.ciudad_nacimiento = this.atributoCiudadNacimiento.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.ciudad_nacimiento ? this.persona_banco_datos.ciudad_nacimiento + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado una ciudad de nacimiento: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado una ciudad de nacimiento`;
                  }
                  this.atributoCiudadNacimiento = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showCiudadNacimiento, 'ciudad_nacimiento');
                } else if (objetoAtributo.atributo.id == 64) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_transaccion.nrotran && this.atributoNroTransaccion.valor) {
                    this.persona_banco_transaccion.nrotran = this.atributoNroTransaccion.valor;
                  } else if (!this.persona_banco_transaccion_debito_csg.nrotran && this.atributoNroTransaccion.valor) {
                    this.persona_banco_transaccion_debito_csg.nrotran = this.atributoNroTransaccion.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_transaccion.nrotran ? this.persona_banco_transaccion.nrotran + `` : this.persona_banco_transaccion_debito_csg.nrotran ? this.persona_banco_transaccion_debito_csg.nrotran + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = `El número de transacción es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `La transaccion no tiene registrado un nro de transacción`;
                  }
                  this.atributoNroTransaccion = atributoInstanciaPolizaFound;
                  if (this.persona_banco_transaccion.nrotran) {
                    if (!this.tran_info.find((param) => param.detail == this.persona_banco_transaccion.nrotran + ``)) {
                      this.tran_info.push({
                        severity: `success`,
                        summary: `Nro de transacción`,
                        detail: this.persona_banco_transaccion.nrotran + ``,
                      });
                    }
                  } else if (this.persona_banco_transaccion_debito_csg.nrotran) {
                    if (!this.tran_info.find((param) => param.detail == this.persona_banco_transaccion_debito_csg.nrotran + ``)) {
                      this.tran_info.push({
                        severity: `success`,
                        summary: `Nro de transacción`,
                        detail: this.persona_banco_transaccion_debito_csg.nrotran + ``,
                      });
                    }
                  }
                  if (
                      this.estadoPorPagar.id == this.asegurado.instancia_poliza.id_estado ||
                      this.estadoEmitido.id == this.asegurado.instancia_poliza.id_estado
                  ) {
                    if (this.persona_banco_transaccion.nrotran && this.persona_banco_transaccion.nrotran != ``) {
                      await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showNroTransaccion,'nrotran');
                    }
                  }
                } else if (objetoAtributo.atributo.id == 65) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_transaccion.fechatran && this.atributoFechaTransaccion.valor) {
                    this.persona_banco_transaccion.fechatran = this.atributoFechaTransaccion.valor;
                  } else if (!this.persona_banco_certificado.fecha_vigencia && this.atributoFechaTransaccion.valor) {
                    this.persona_banco_certificado.fecha_vigencia = this.atributoFechaTransaccion.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_transaccion.fechatran ? this.persona_banco_transaccion.fechatran + `` : this.persona_banco_certificado.fecha_vigencia ? this.persona_banco_certificado.fecha_vigencia + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = `La fecha de la transacción es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `La transaccion no tiene registrado una fecha de transacción`;
                  }
                  this.atributoFechaTransaccion = atributoInstanciaPolizaFound;
                  if (this.persona_banco_transaccion.fechatran) {
                    if (!this.tran_info.find((param) => param.detail == this.persona_banco_transaccion.fechatran + ``)) {
                      this.tran_info.push({
                        severity: `success`,
                        summary: `Fecha de la transaccion`,
                        detail: this.persona_banco_transaccion.fechatran + ``,
                      });
                    }
                  } else if (this.persona_banco_certificado.fecha_vigencia) {
                    if (!this.tran_info.find((param) => param.detail == this.persona_banco_certificado.fecha_vigencia + ``)) {
                      this.tran_info.push({
                        severity: `success`,
                        summary: `Fecha de la transaccion`,
                        detail: this.persona_banco_certificado.fecha_vigencia + ``,
                      });
                    }
                  }
                  if (
                      this.estadoPorPagar.id == this.asegurado.instancia_poliza.id_estado ||
                      this.estadoEmitido.id == this.asegurado.instancia_poliza.id_estado
                  ) {
                    await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTransaccionFecha, 'fechatran');
                  }
                } else if (objetoAtributo.atributo.id == 66) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_transaccion.importe && this.atributoTransaccionImporte.valor) {
                    this.persona_banco_transaccion.importe = this.atributoTransaccionImporte.valor;
                  } else if (!this.persona_banco_transaccion_debito_csg.importe && this.atributoTransaccionImporte.valor) {
                    this.persona_banco_transaccion_debito_csg.importe = this.atributoTransaccionImporte.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_transaccion.importe ? this.persona_banco_transaccion.importe + `` : this.persona_banco_transaccion.importe ? this.persona_banco_transaccion.importe + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = `El importe de la transaccion es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `La transaccion no tiene registrado un importe`;
                  }
                  this.atributoTransaccionImporte = atributoInstanciaPolizaFound;
                  if (this.persona_banco_transaccion.importe) {
                    let tranDetail = this.MonedasParametroCod[this.persona_banco_transaccion.moneda + ``] + ` ` + this.persona_banco_transaccion.importe + ``;
                    if (
                        !this.tran_info.find((param) => param.detail == tranDetail + ``)
                    ) {
                      this.tran_info.push({
                        severity: `success`,
                        summary: `Importe de la transaccion`,
                        detail: tranDetail,
                      });
                    }
                  }
                  if (this.estadoPorPagar.id == this.asegurado.instancia_poliza.id_estado || this.estadoEmitido.id == this.asegurado.instancia_poliza.id_estado) {
                    await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTransaccionImporte, 'importe');
                  }
                } else if (objetoAtributo.atributo.id == 69) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_transaccion.detalle && this.atributoTransaccionDetalle.valor) {
                    this.persona_banco_transaccion.detalle = this.atributoTransaccionDetalle.valor;
                  } else if (!this.persona_banco_transaccion_debito_csg.descripcion && this.atributoTransaccionDetalle.valor) {
                    this.persona_banco_transaccion_debito_csg.descripcion = this.atributoTransaccionDetalle.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_transaccion.detalle ? this.persona_banco_transaccion.detalle + `` : this.persona_banco_transaccion_debito_csg ? this.persona_banco_transaccion_debito_csg.descripcion + '': null ;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = `El detalle de la transaccion es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `La transaccion no tiene registrado un detalle de transacción`;
                  }
                  this.atributoTransaccionDetalle = atributoInstanciaPolizaFound;
                  if (
                      this.persona_banco_transaccion.detalle &&
                      this.persona_banco_transaccion.detalle != `undefined` &&
                      this.persona_banco_transaccion.detalle != `null`
                  ) {
                    if (!this.tran_info.find((param) => param.detail == this.persona_banco_transaccion.detalle + ``)) {
                      this.tran_info.push({
                        severity: `success`,
                        summary: `Detalle de la transaccion`,
                        detail: this.persona_banco_transaccion.detalle + ``,
                      });
                    }
                  }
                  if (
                      this.estadoPorPagar.id == this.asegurado.instancia_poliza.id_estado ||
                      this.estadoEmitido.id == this.asegurado.instancia_poliza.id_estado
                  ) {
                    await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showTransaccionDetalle, 'detalle');
                  }
                } else if (objetoAtributo.atributo.id == 96) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_transaccion_debito_csg.estado && this.atributoTransaccionEstado.valor) {
                    this.persona_banco_transaccion_debito_csg.estado = parseInt(this.atributoTransaccionEstado.valor+'');
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_transaccion_debito_csg.estado ? this.persona_banco_transaccion_debito_csg.estado + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = `El estado de la transaccion es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `La transaccion no tiene registrado un estado`;
                  }
                  this.atributoTransaccionEstado = atributoInstanciaPolizaFound;
                  if (this.persona_banco_transaccion_debito_csg.estado) {;
                    if (
                        !this.tran_info.find((param) => param.detail == this.persona_banco_transaccion_debito_csg.estado + ``)
                    ) {
                      this.tran_info.push({
                        severity: `success`,
                        summary: `Estado de la transaccion`,
                        detail: this.persona_banco_transaccion_debito_csg.estado+'',
                      });
                    }
                  }
                  if (this.estadoPorPagar.id == this.asegurado.instancia_poliza.id_estado || this.estadoEmitido.id == this.asegurado.instancia_poliza.id_estado) {
                    await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTransaccionEstado, 'estado');
                  }
                } else if (objetoAtributo.atributo.id == 67) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_transaccion.moneda && this.atributoTransaccionMoneda.valor) {
                    this.persona_banco_transaccion.moneda = this.atributoTransaccionMoneda.valor;
                  } else if (!this.persona_banco_transaccion_debito_csg.moneda && this.atributoTransaccionMoneda.valor) {
                    this.persona_banco_transaccion_debito_csg.moneda = this.atributoTransaccionMoneda.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_transaccion.moneda ? this.persona_banco_transaccion.moneda + `` : this.persona_banco_transaccion_debito_csg.moneda ? this.persona_banco_transaccion_debito_csg.moneda + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = `La moneda de la transaccion es: ${this.MonedasParametroCod[atributoInstanciaPolizaFound.valor]}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `La transaccion no tiene registrado un tipo de moneda`;
                  }
                  this.atributoTransaccionMoneda = atributoInstanciaPolizaFound;
                  // if(this.persona_banco_transaccion.moneda != undefined) {
                  // this.tran_info.push({ severity: 'success', summary: 'Moneda de la transaccion', detail: this.MonedasParametroCod[this.persona_banco_transaccion.moneda+'']});
                  // }
                  if (this.estadoPorPagar.id == this.asegurado.instancia_poliza.id_estado || this.estadoEmitido.id == this.asegurado.instancia_poliza.id_estado) {
                    await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTransaccionMoneda, 'moneda');
                  }
                } else if (objetoAtributo.atributo.id == 96) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_transaccion_debito_csg.estado && this.atributoTransaccionDebitoCSGEstado.valor) {
                    this.persona_banco_transaccion_debito_csg.estado = parseInt(this.atributoTransaccionDebitoCSGEstado.valor+'');
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_transaccion_debito_csg.estado ? this.persona_banco_transaccion_debito_csg.estado + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = `El estado de la transaccion es: ${this.MonedasParametroCod[atributoInstanciaPolizaFound.valor]}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `El estado de la transaccion no tiene registrado un tipo de moneda`;
                  }
                  this.atributoTransaccionDebitoCSGEstado = atributoInstanciaPolizaFound;
                  // if(this.persona_banco_transaccion.moneda != undefined) {
                  // this.tran_info.push({ severity: 'success', summary: 'Moneda de la transaccion', detail: this.MonedasParametroCod[this.persona_banco_transaccion.moneda+'']});
                  // }
                  if (this.estadoPorPagar.id == this.asegurado.instancia_poliza.id_estado || this.estadoEmitido.id == this.asegurado.instancia_poliza.id_estado ) {
                    await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTransaccionDebitoCSGEstado, 'moneda');
                  }
                } else if (objetoAtributo.atributo.id == 98) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_transaccion.codigo_via && this.atributoTransaccionCodigoVia.valor) {
                    this.persona_banco_transaccion.codigo_via = this.atributoTransaccionCodigoVia.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_transaccion.codigo_via ? this.persona_banco_transaccion.codigo_via + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = `La transacción tiene el codigo de via: ${this.MonedasParametroCod[atributoInstanciaPolizaFound.valor]}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = `La transacción no tiene registrado un codigo de via`;
                  }
                  this.atributoTransaccionCodigoVia = atributoInstanciaPolizaFound;
                  // if(this.persona_banco_transaccion.moneda != undefined) {
                  // this.tran_info.push({ severity: 'success', summary: 'Moneda de la transaccion', detail: this.MonedasParametroCod[this.persona_banco_transaccion.moneda+'']});
                  // }
                  if (this.estadoPorPagar.id == this.asegurado.instancia_poliza.id_estado || this.estadoEmitido.id == this.asegurado.instancia_poliza.id_estado ) {
                    await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTransaccionCodigoVia, 'codigo_via');
                  }
                } else if (objetoAtributo.atributo.id == 71) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_pep.condicion && this.atributoCondicionPep.valor != ``) {
                    this.persona_banco_pep.condicion = this.atributoCondicionPep.valor;
                  }
                  if (this.persona_banco_pep.condicion) {
                    atributoInstanciaPolizaFound.valor = this.persona_banco_pep.condicion + ``;
                  } else {
                    atributoInstanciaPolizaFound.valor = ``;
                  }
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `El cargo publico y/o politico jerarquico es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene un cargo publico y/o politico jerarquico`;
                  }
                  this.atributoCondicionPep = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showCondicionPep, 'condicion');
                } else if (objetoAtributo.atributo.id == 72) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_pep.cargo_entidad && this.atributoCargoEntidadPep.valor != ``) {
                    this.persona_banco_pep.cargo_entidad = this.atributoCargoEntidadPep.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_pep.cargo_entidad ? this.persona_banco_pep.cargo_entidad : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `El cargo/entidad pep es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un cargo/entidad pep`;
                  }
                  this.atributoCargoEntidadPep = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showCargoEntidadPep,'cargo_entidad');
                } else if (objetoAtributo.atributo.id == 73) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_pep.periodo_cargo_publico && this.atributoPeriodoCargoPublico.valor != ``) {
                    this.persona_banco_pep.periodo_cargo_publico = this.atributoPeriodoCargoPublico.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_pep.periodo_cargo_publico ? this.persona_banco_pep.periodo_cargo_publico : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `El periodo del cargo publico pep es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un periodo del cargo publico pep`;
                  }
                  this.atributoPeriodoCargoPublico = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showPeriodoCargoPep, 'periodo_cargo_publico');
                } else if (objetoAtributo.atributo.id == 74) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (this.persona_banco_datos.direccion_laboral == undefined && this.atributoDireccionLaboral.valor != ``) {
                    this.persona_banco_datos.direccion_laboral = this.atributoDireccionLaboral.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.direccion_laboral ? this.persona_banco_datos.direccion_laboral : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `La direccion laboral es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene una  direccion laboral`;
                  }
                  this.atributoDireccionLaboral = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showDireccionLaboral, 'direccion_laboral');
                } else if (objetoAtributo.atributo.id == 75) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_datos.tipo_cuenta && this.atributoTipoCuenta.valor != ``) {
                    this.persona_banco_datos.tipo_cuenta = this.atributoTipoCuenta.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.tipo_cuenta ? this.persona_banco_datos.tipo_cuenta : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `El tipo de cuenta es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene un tipo de cuenta`;
                  }
                  this.atributoTipoCuenta = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showTipoCuenta, 'tipo_cuenta');
                } else if (objetoAtributo.atributo.id == 80) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_datos.producto_asociado && this.atributoProductoAsociado.valor != ``) {
                    this.persona_banco_datos.producto_asociado = this.atributoProductoAsociado.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.producto_asociado ? this.persona_banco_datos.producto_asociado : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `El producto asociado es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene un producto asociado`;
                  }
                  this.atributoProductoAsociado = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showProductoAsociado, 'producto_asociado');
                } else if (objetoAtributo.atributo.id == 78) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_solicitud.solicitud_sci_selected && this.atributoSolicitudSci.valor != ``) {
                    this.persona_banco_solicitud.solicitud_sci_selected = this.atributoSolicitudSci.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_solicitud.solicitud_sci_selected ? this.persona_banco_solicitud.solicitud_sci_selected : null;
                  if (this.persona_banco_solicitud.solicitud_sci_selected && this.persona_banco_solicitud.solicitud_sci_selected.substring(0, 1) == '9') {
                    this.pagoACredito = true;
                    this.persona_banco_solicitud.solicitud_tipo = this.parametroTarjetaCredito.parametro_descripcion;
                    this.persona_banco.debito_automatico = this.pagoDebitAutomatico.id + '';
                    this.persona_banco_solicitud.solicitud_forma_pago = this.parametroPagoContado.id + '';
                  }
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `El nro de solicitud es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene un nro de solicitud sci`;
                  }
                  this.atributoSolicitudSci = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showNroSolicitudSci, 'solicitud_tipo');
                } else if (objetoAtributo.atributo.id == 76) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_datos.nro_cuotas && this.atributoNroCuotas.valor != ``) {
                    this.persona_banco_datos.tipo_cuenta = this.atributoNroCuotas.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.nro_cuotas ? this.persona_banco_datos.nro_cuotas + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `El Nro. de cuotas es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado un Nro. de cuotas`;
                  }
                  this.atributoNroCuotas = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showNroCuotas, 'nro_cuotas');
                } else if (objetoAtributo.atributo.id == 77) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_datos.desc_ocupacion && this.atributoDescOcupacion.valor != ``) {
                    this.persona_banco_datos.desc_ocupacion = this.atributoDescOcupacion.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.desc_ocupacion ? this.persona_banco_datos.desc_ocupacion + '' : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `La descripción de ocupación es: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado la descripción de su ocupación`;
                  }
                  this.atributoDescOcupacion = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showDescOcupacion, 'desc_ocupacion');
                } else if (objetoAtributo.atributo.id == 61) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_datos.monto && this.atributoMonto.valor != ``) {
                    this.persona_banco_datos.monto = parseInt(this.atributoMonto.valor);
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.monto ? this.persona_banco_datos.monto + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado el monto de la cuota: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado el monto de la cuota`;
                  }
                  this.atributoMonto = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showMonto, 'monto');
                } else if (objetoAtributo.atributo.id == 81) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_solicitud.solicitud_sci_estado && this.atributoSolicitudSciEstado.valor != ``) {
                    this.persona_banco_solicitud.solicitud_sci_estado = this.atributoSolicitudSciEstado.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_operacion.Operacion_Estado ? this.persona_banco_operacion.Operacion_Estado + `` : null;
                  if (this.persona_banco_operacion.Operacion_Estado && this.persona_banco_operacion.Operacion_Estado.includes('APROBAD')) {
                    this.solicitudAprobada = true;
                  } else {
                    this.solicitudAprobada = false;
                  }
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado el estado de la solicitud Sci: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado el estado de la solicitud Sci`;
                  }
                  this.atributoSolicitudSciEstado = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showSolicitudEstadoSci, 'solicitud_sci_estado');
                } else if (objetoAtributo.atributo.id == 82) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_operacion.Operacion_Plazo && this.atributoSolicitudPlazoCredito.valor != ``) {
                    this.persona_banco_operacion.Operacion_Plazo = parseInt(this.atributoSolicitudPlazoCredito.valor);
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_operacion.Operacion_Plazo ? this.persona_banco_operacion.Operacion_Plazo + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado el plazo del crédito: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado el plazo del crédito Sci`;
                  }
                  this.atributoSolicitudPlazoCredito = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showSolicitudPlazoCredito, 'Operacion_Plazo');
                } else if (objetoAtributo.atributo.id == 83) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_operacion.Operacion_Moneda && this.atributoSolicitudMoneda.valor != ``) {
                    this.persona_banco_operacion.Operacion_Moneda = this.atributoSolicitudMoneda.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_operacion.Operacion_Moneda ? this.persona_banco_operacion.Operacion_Moneda +'' : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado la moneda de la solicitud: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado la moneda de la solicitud`;
                  }
                  this.atributoSolicitudMoneda = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showSolicitudMoneda, 'Operacion_Moneda');
                } else if (objetoAtributo.atributo.id == 84) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_operacion.Operacion_Frecuencia && this.atributoSolicitudFrecuenciaPlazo.valor != ``) {
                    this.persona_banco_operacion.Operacion_Frecuencia = this.atributoSolicitudFrecuenciaPlazo.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_operacion.Operacion_Frecuencia ? this.persona_banco_operacion.Operacion_Frecuencia + '' : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado la frecuencia del plazo: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado la frecuencia del plazo`;
                  }
                  this.atributoSolicitudFrecuenciaPlazo = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showSolicitudFrecuenciaPlazo, 'Operacion_Frecuencia');
                } else if (objetoAtributo.atributo.id == 85) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_solicitud.solicitud_prima_total && this.atributoSolicitudPrimaTotal.valor != ``) {
                    this.persona_banco_solicitud.solicitud_prima_total = this.atributoSolicitudPrimaTotal.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_solicitud.solicitud_prima_total ? this.persona_banco_solicitud.solicitud_prima_total + `` : (this.persona_banco_operacion.Operacion_Plazo * this.poliza.anexo_poliza.monto_prima) + '';
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado la prima total: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado la prima total`;
                  }
                  this.atributoSolicitudPrimaTotal = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showSolicitudPrimaTotal, 'solicitud_prima_total');
                } else if (objetoAtributo.atributo.id == 86) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_solicitud.solicitud_forma_pago && this.atributoFormaPago.valor != ``) {
                    this.persona_banco_solicitud.solicitud_forma_pago = this.atributoFormaPago.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_solicitud.solicitud_forma_pago;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado la forma de pago: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado la forma de pago`;
                  }
                  this.atributoFormaPago = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showFormaPago, 'solicitud_forma_pago');
                } else if (objetoAtributo.atributo.id == 87) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_tarjeta_debito_cuenta_vinculada.fecha_activacion && this.atributoFormaPago.valor != ``) {
                    this.persona_banco_tarjeta_debito_cuenta_vinculada.fecha_activacion = new Date(this.atributoFechaActivacion.valor);
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_debito_cuenta_vinculada.fecha_activacion ? this.persona_banco_tarjeta_debito_cuenta_vinculada.fecha_activacion + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado la fecha de activacion de la tarjeta: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado la fecha de activacion de la tarjeta`;
                  }
                  this.atributoFechaActivacion = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showFechaActivacion, 'fecha_activacion');
                } else if (objetoAtributo.atributo.id == 88) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_tarjeta_debito_cuenta_vinculada.fin_vigencia && this.atributoFechaFinVigencia.valor != ``) {
                    this.persona_banco_tarjeta_debito_cuenta_vinculada.fin_vigencia = new Date(this.atributoFechaFinVigencia.valor);
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_debito_cuenta_vinculada.fin_vigencia ? this.persona_banco_tarjeta_debito_cuenta_vinculada.fin_vigencia + `` : null;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado el fin de vigencia de la tarjeta: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado el fin de vigencia de la tarjeta`;
                  }
                  this.atributoFechaFinVigencia = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showFinVigencia, 'fin_vigencia');
                } else if (objetoAtributo.atributo.id == 89) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_tarjeta_debito_cuenta_vinculada.estado_tarjeta && this.atributoEstadoTarjeta.valor != ``) {
                    this.persona_banco_tarjeta_debito_cuenta_vinculada.estado_tarjeta = this.atributoEstadoTarjeta.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_debito_cuenta_vinculada.estado_tarjeta;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado el estado de la tarjeta: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado el estado de la tarjeta`;
                  }
                  this.atributoEstadoTarjeta = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showEstadoTarjeta, 'estado_tarjeta');
                } else if (objetoAtributo.atributo.id == 90) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_operacion.Operacion_Tipo_Credito && this.atributoTipoCredito.valor != ``) {
                    this.persona_banco_operacion.Operacion_Tipo_Credito = this.atributoTipoCredito.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_operacion.Operacion_Tipo_Credito;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado el tipo de crédito: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado el tipo de crédito`;
                  }
                  this.atributoTipoCredito = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroError, this.showTipoCredito, 'Operacion_Tipo_Credito');
                } else if (objetoAtributo.atributo.id == 91) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_datos.provincia && this.atributoProvincia.valor != ``) {
                    this.persona_banco_datos.provincia = this.atributoProvincia.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.provincia;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado su provincia: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado su provincia`;
                  }
                  this.atributoProvincia = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showProvincia, 'provincia');
                } else if (objetoAtributo.atributo.id == 92) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_datos.lugar_nacimiento && this.atributoLugarNacimiento.valor != ``) {
                    this.persona_banco_datos.lugar_nacimiento = this.atributoLugarNacimiento.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_datos.lugar_nacimiento;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado su lugar de nacimiento: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado su lugar de nacimiento`;
                  }
                  this.atributoLugarNacimiento = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showLugarNacimiento, 'lugar_nacimiento');
                } else if (objetoAtributo.atributo.id == 93) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_tarjeta_ci_ext.id && this.atributoTarjetaId.valor != ``) {
                    this.persona_banco_tarjeta_ci_ext.id = this.atributoTarjetaId.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_tarjeta_ci_ext.id;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado su id de tarjeta: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado su id de tarjeta`;
                  }
                  this.atributoTarjetaId = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showIdTarjeta, 'id');
                } else if (objetoAtributo.atributo.id == 97) {
                  atributoInstanciaPolizaFound.objeto_x_atributo = objetoAtributo;
                  atributoInstanciaPolizaFound.id_objeto_x_atributo = objetoAtributo.id;
                  if (!this.persona_banco_operacion.Operacion_Usuario_Oficial && this.atributoOperacionOficial.valor != ``) {
                    this.persona_banco_operacion.Operacion_Usuario_Oficial = this.atributoOperacionOficial.valor;
                  }
                  atributoInstanciaPolizaFound.valor = this.persona_banco_operacion.Operacion_Usuario_Oficial;
                  if (atributoInstanciaPolizaFound.valor) {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `tiene registrado su operacion usuario oficial de credito: ${atributoInstanciaPolizaFound.valor}`;
                  } else {
                    atributoInstanciaPolizaFound.tipo_error = titularOPersona + `no tiene registrado su id de tarjeta`;
                  }
                  this.atributoOperacionOficial = atributoInstanciaPolizaFound;
                  await this.setAtributoObservacion(this.asegurado, atributoInstanciaPolizaFound.tipo_error, atributoInstanciaPolizaFound.valor, this.parametroWarning, this.showOperacionOficial, 'id');
                }
                //if (atributoInstanciaPolizaFoundIndex >= 0) {
                //    this.asegurado.instancia_poliza.atributo_instancia_polizas[atributoInstanciaPolizaFoundIndex] = atributoInstanciaPolizaFound;
                //} else {
                if (atributoInstanciaPolizaFound.id_objeto_x_atributo) {
                  this.asegurado.instancia_poliza.atributo_instancia_polizas.push(atributoInstanciaPolizaFound);
                }
                //}
                this.atributoInstanciaPolizas = this.asegurado.instancia_poliza.atributo_instancia_polizas;
            }
            if (typeof callback == "function") {
                callback();
            }
        } catch (e) {
            console.log(e);
        }
    }

    async fixFeatureFormAngular(form: FormGroup, key: string, idAtributo: number) {
        await this.asegurado.instancia_poliza.atributo_instancia_polizas.forEach(
            async (
                atributoInstanciaPoliza: Atributo_instancia_poliza,
                index
            ) => {
                switch (
                    atributoInstanciaPoliza.objeto_x_atributo.atributo.id + ""
                ) {
                    case idAtributo + "":
                        form.controls[key].setValue(
                            this.persona_banco_account.moneda
                        );
                        break;
                }
            }
        );
    }

    async setDocumentoCertificado() {
        this.instanciaDocumentoCertificado = new Instancia_documento();
        if (this.poliza.tipo_numeracion.id == this.parametroNumeracionUnificada.id) {
            this.instanciaDocumentoCertificado.nro_documento = this.instanciaDocumentoSolicitud.nro_documento;
        }
        this.instanciaDocumentoCertificado.fecha_emision = new Date();
        this.instanciaDocumentoCertificado.documento = this.documentoCertificado;
        if (this.asegurado.instancia_poliza.id_estado == this.estadoSolicitado.id) {
            if (this.atributoDebitoAutomatico.valor == this.condicionSi.id + "") {
                if (this.hasRolCajero || (this.hasRolPlataforma && this.atributoSolicitudSci && this.atributoSolicitudSci.valor.substring(0,1) == '9')) {
                    this.emitirInstanciaPoliza = true;
                } else {
                    this.emitirInstanciaPoliza = false;
                }
            } else {
                this.emitirInstanciaPoliza = true;
            }
        } else if (this.asegurado.instancia_poliza.id_estado == this.estadoPorPagar.id) {
            this.emitirInstanciaPoliza = true;
        } else if (this.asegurado.instancia_poliza.id_estado == this.estadoPorEmitir.id && this.hasRolPlataforma && this.atributoSolicitudSci && this.atributoSolicitudSci.valor.substring(0,1) == '9') {
            this.emitirInstanciaPoliza = true;
        }
    }

    getFechaMinimaSegunEdad() {
        var date = new Date();
        if (this.asegurado.entidad.persona.persona_fecha_nacimiento) {
            date.setDate(this.asegurado.entidad.persona.persona_fecha_nacimiento.getDate());
            date.setFullYear(this.asegurado.entidad.persona.persona_fecha_nacimiento.getFullYear() - 18);
            return date;
        } else {
            return null;
        }
    }
    getFechaMaximaSegunEdad() {
        var date = new Date();
        if (this.asegurado.entidad.persona.persona_fecha_nacimiento) {
            date.setDate(this.asegurado.entidad.persona.persona_fecha_nacimiento.getDate());
            date.setFullYear(this.asegurado.entidad.persona.persona_fecha_nacimiento.getFullYear() + 65);
            return date;
        } else {
            return null;
        }
    }


  async setAtributosToAsegurado(callback:Function = null) {
    if (this.asegurado) {
      this.msgs_warn = [];
      this.msgs_error = [];
      this.msgs_warn_error = [];
      this.msgs_info_warn = [];
      this.instanciaPolizaTransService.oldInstanciaPolizaTransiciones = this.asegurado.instancia_poliza.instancia_poliza_transicions;
      this.asegurado.instancia_poliza.instancia_poliza_transicions = [];
      if (this.persona_banco) {
          this.asegurado.entidad.persona.persona_doc_id = this.persona_banco.doc_id;
          this.asegurado.entidad.persona.persona_doc_id_ext = util.isNumber(this.persona_banco.extension) ? this.persona_banco.extension : this.ProcedenciaCIAbreviacion[this.persona_banco.extension];
          if(this.persona_banco.complemento != null) {
            this.asegurado.entidad.persona.persona_doc_id_comp = this.persona_banco.complemento;
          }
          if(this.persona_banco.nombre != null) {
            this.asegurado.entidad.persona.persona_primer_nombre = this.persona_banco.nombre;
          }
          if(this.persona_banco.paterno != null) {
            this.asegurado.entidad.persona.persona_primer_apellido = this.persona_banco.paterno;
          }
          if(this.persona_banco.materno != null) {
            this.asegurado.entidad.persona.persona_segundo_apellido = this.persona_banco.materno;
          }
          if(this.persona_banco.apcasada != null) {
            this.asegurado.entidad.persona.persona_apellido_casada = this.persona_banco.apcasada;
          }
          if(this.persona_banco.e_mail != null) {
            this.asegurado.entidad.persona.persona_email_personal= this.persona_banco.e_mail;
          }
          if(this.persona_banco.direccion != null) {
            this.asegurado.entidad.persona.persona_direccion_domicilio = this.persona_banco.direccion;
          }
          if(this.persona_banco.fono_domicilio != null) {
            this.asegurado.entidad.persona.persona_telefono_domicilio = this.persona_banco.fono_domicilio;
          }
          if(this.persona_banco.nro_celular != null) {
            this.asegurado.entidad.persona.persona_celular = this.persona_banco.nro_celular;
          }
          if(this.persona_banco.fono_oficina != null) {
            this.asegurado.entidad.persona.persona_telefono_trabajo = this.persona_banco.fono_oficina;
          }
          if(this.persona_banco.sexo != null) {
            this.asegurado.entidad.persona.par_sexo_id = this.SexosAbreviacion[this.persona_banco.sexo];
          }

          if(this.asegurado.entidad.persona.persona_primer_apellido == null) {
            this.asegurado.entidad.persona.persona_primer_apellido = '';
          }
          if(this.asegurado.entidad.persona.persona_segundo_apellido == null) {
            this.asegurado.entidad.persona.persona_segundo_apellido = '';
          }
          if(this.asegurado.entidad.persona.persona_primer_nombre == null) {
            this.asegurado.entidad.persona.persona_primer_nombre = '';
          }

          if(this.asegurado.instancia_poliza.id) {
            this.personaTitular = 'El titular';
          } else {
            this.personaTitular = 'La Persona';
          }

          if (this.persona_banco.doc_id != '' && this.persona_banco.extension != "") {
            this.TiposDocumentosId.forEach((label, index) => {
              if (label == this.persona_banco.doc_id) {
                this.asegurado.entidad.persona.par_tipo_documento_id = index+'';
              }
            });
            if (this.asegurado.entidad.persona.par_tipo_documento_id == '' || this.asegurado.entidad.persona.par_tipo_documento_id == undefined) {
              this.asegurado.entidad.persona.par_tipo_documento_id = '1';
            }
          }
          this.asegurado.entidad.persona.par_pais_nacimiento_id = this.persona_banco_datos.pais_nacimiento = "5";
          this.asegurado.entidad.persona.par_nacionalidad_id = this.NacionalidadesParametroCod["BO"];

          this.setDatesOfAsegurado();
          await this.setAtributoObservacion( this.asegurado,this.atributoComplementoDocId.tipo_error = this.personaTitular + ' no tiene registrado un complemento del documento de identidad', this.persona_banco.complemento,this.parametroWarning,true,'complemento');
          await this.setAtributoObservacion( this.asegurado,this.atributoDocId.tipo_error = this.personaTitular + ' no tiene registrado un número de documento', this.persona_banco.doc_id,this.parametroError,true,'doc_id');
          await this.setAtributoObservacion( this.asegurado,this.atributoDocIdExt.tipo_error = this.personaTitular + ' no tiene registrado una extencion del número de documento', this.persona_banco.extension,this.parametroError,true,'extension');
          await this.setAtributoObservacion( this.asegurado,this.atributoPrimerNombre.tipo_error = this.personaTitular + ' no tiene registrado un nombre', this.persona_banco.nombre,this.parametroWarning,true,'nombre');
          await this.setAtributoObservacion( this.asegurado,this.atributoApellidoPaterno.tipo_error = this.personaTitular + ' no tiene registrado un apellido paterno', this.persona_banco.paterno,this.parametroWarning,true,'paterno');
          await this.setAtributoObservacion( this.asegurado,this.atributoApellidoMaterno.tipo_error = this.personaTitular + ' no tiene registrado un apellido materno', this.persona_banco.materno,this.parametroWarning,true,'materno');
          await this.setAtributoObservacion( this.asegurado,this.atributoPaisNacimiento.tipo_error = this.personaTitular + ' no tiene registrado un pais de nacimiento', this.persona_banco_datos.pais_nacimiento,this.parametroWarning,true,'pais_nacimiento');
          await this.setAtributoObservacion( this.asegurado,this.atributoFechaNacimiento.tipo_error = this.personaTitular + ' no tiene registrado una fecha de nacimiento',this.persona_banco.fecha_nacimiento,this.parametroError,true,'fecha_nacimiento');
          await this.setAtributoObservacion( this.asegurado,this.atributoApellidoCasada.tipo_error = this.personaTitular + ' no tiene registrado un apellido de casada',this.persona_banco.apcasada,this.parametroWarning,true,'apcasada');
          await this.setAtributoObservacion( this.asegurado,this.atributoDireccionDomicilio.tipo_error = this.personaTitular + ' no tiene registrado una dirección',this.persona_banco.direccion,this.parametroWarning,true,'direccion');
          await this.setAtributoObservacion( this.asegurado,this.atributoTelefonoDomicilio.tipo_error = this.personaTitular + ' no tiene registrado un teléfono de domicilio',this.persona_banco.fono_domicilio,this.parametroWarning,true,'fono_domicilio');
          await this.setAtributoObservacion( this.asegurado,this.atributoTelefonoTrabajo.tipo_error = this.personaTitular + ' no tiene registrado un teléfono de trabajo',this.persona_banco.fono_oficina,this.parametroWarning,true,'fono_oficina');
          await this.setAtributoObservacion( this.asegurado,this.atributoTelefonoCelular.tipo_error = this.personaTitular + ' no tiene registrado un número de celular',this.persona_banco.nro_celular,this.parametroWarning,true,'nro_celular');
          await this.setAtributoObservacion( this.asegurado,this.atributoSexo.tipo_error = this.personaTitular + ' no tiene registrado su sexo',this.persona_banco.sexo,this.parametroWarning,true,'sexo');
          if(this.persona_banco_accounts.length == 0) {
            await this.setAtributoObservacion(this.asegurado, this.atributoNroCuenta.tipo_error = this.personaTitular  + ' no tiene registrado ningun número de cuenta',this.persona_banco_accounts.length,this.parametroWarning);
          }
          if (this.persona_banco.doc_id != '' && this.persona_banco.extension != "") {
            this.TiposDocumentosId.forEach((label, index) => {
              if (label == this.persona_banco.doc_id) {
                this.asegurado.entidad.persona.par_tipo_documento_id = index+'';
              }
            });
            if (this.asegurado.entidad.persona.par_tipo_documento_id == '' || this.asegurado.entidad.persona.par_tipo_documento_id == undefined) {
              this.asegurado.entidad.persona.par_tipo_documento_id = '1';
            }
          }
          this.asegurado.entidad.persona.par_pais_nacimiento_id = "5";
          this.asegurado.entidad.persona.par_nacionalidad_id = this.NacionalidadesParametroCod["BO"];
          await this.setFromObjetoAtributoToAtributoInstanciaPoliza();
          await this.setFromObjetoAtributoToAtributoInstanciaDocumento();
          this.asegurado.instancia_poliza.atributo_instancia_polizas_inter = [];
          await this.ObjetoAseguradoAtributosFiltered.forEach((objetoAtributo: Objeto_x_atributo, index) => {
            let atributoInstanciaPoliza:Atributo_instancia_poliza = this.asegurado.instancia_poliza.atributo_instancia_polizas.find(params => params.objeto_x_atributo.id_atributo == objetoAtributo.id_atributo);
            if(atributoInstanciaPoliza) {
              if (!atributoInstanciaPoliza.valor || atributoInstanciaPoliza.valor == 'undefined') {
                atributoInstanciaPoliza.valor = '';
              }
              this.asegurado.instancia_poliza.atributo_instancia_polizas_inter[index] = atributoInstanciaPoliza;
            }
          });

          let trancisiones:Instancia_poliza_transicion[] = this.asegurado.instancia_poliza.instancia_poliza_transicions;
          await this.instanciaPolizaTransService.oldInstanciaPolizaTransiciones.forEach((oldInstanciaPolizaTransicion:Instancia_poliza_transicion) => {
            if(oldInstanciaPolizaTransicion.par_estado_id == this.asegurado.instancia_poliza.id_estado) {
              trancisiones.forEach(async (newInstanciaPolizaTransaccion:Instancia_poliza_transicion) => {
                if(oldInstanciaPolizaTransicion.observacion == newInstanciaPolizaTransaccion.observacion) {
                  newInstanciaPolizaTransaccion.id = oldInstanciaPolizaTransicion.id;
                  newInstanciaPolizaTransaccion.createdAt = oldInstanciaPolizaTransicion.createdAt;
                  newInstanciaPolizaTransaccion.adicionado_por = oldInstanciaPolizaTransicion.adicionado_por;
                  newInstanciaPolizaTransaccion.modificado_por = oldInstanciaPolizaTransicion.modificado_por;
                  newInstanciaPolizaTransaccion.updatedAt = oldInstanciaPolizaTransicion.updatedAt;
                } else if(oldInstanciaPolizaTransicion.par_estado_id != this.asegurado.instancia_poliza.id_estado &&
                  oldInstanciaPolizaTransicion.observacion == newInstanciaPolizaTransaccion.observacion) {
                  this.asegurado.instancia_poliza.instancia_poliza_transicions.push(oldInstanciaPolizaTransicion);
                }
              })
            }
          });
          if(typeof callback == 'function') {
            await callback()
          }
      } else {
          if(typeof callback == 'function') {
              await callback()
          }
      }
    }
  }

  async setAnexos(asegurado: Asegurado, anexosPoliza: Anexo_poliza[], callback: Function = null) {
    this.poliza.anexo_polizas = anexosPoliza;
    this.poliza.entidad = asegurado.entidad;

    this.parametrosYAtributos.asegurado = asegurado;
    this.parametrosYAtributos.estadoIniciado = this.estadoIniciado;
    this.parametrosYAtributos.estadoSolicitado = this.estadoSolicitado;
    this.parametrosYAtributos.estadoEmitido = this.estadoEmitido;
    this.parametrosYAtributos.estadoAnulado = this.estadoAnulado;
    this.parametrosYAtributos.estadoSinVigencia = this.estadoSinVigencia;
    this.parametrosYAtributos.estadoDesistido = this.estadoDesistido;
    this.parametrosYAtributos.estadoCaducado = this.estadoCaducado;
    this.parametrosYAtributos.estadoCobrado = this.estadoCobrado;
    this.parametrosYAtributos.estadoPorPagar = this.estadoPorPagar;
    this.parametrosYAtributos.estadoPlataforma = this.estadoPlataforma;
    this.parametrosYAtributos.parametroError = this.parametroError;
    this.parametrosYAtributos.parametroWarning = this.parametroWarning;
    this.parametrosYAtributos.parametroInformation = this.parametroInformation;
    this.parametrosYAtributos.parametroSuccess = this.parametroSuccess;
    this.parametrosYAtributos.condicionSi = this.condicionSi;
    this.parametrosYAtributos.condicionNo = this.condicionNo;
    this.parametrosYAtributos.pagoDebitAutomatico = this.pagoDebitAutomatico;
    this.parametrosYAtributos.pagoEfectivo = this.pagoEfectivo;
    this.parametrosYAtributos.parMancomunada = this.parMancomunada;
    this.parametrosYAtributos.parIndividual = this.parIndividual;
    this.parametrosYAtributos.parMensual = this.parMensual;
    this.parametrosYAtributos.parAnual = this.parAnual;
    this.parametrosYAtributos.parametroEditable = this.parametroEditable;
    this.parametrosYAtributos.parametroVisible = this.parametroVisible;
    this.parametrosYAtributos.TiposDocumentos = this.TiposDocumentos;
    this.parametrosYAtributos.TiposDocumentosAbreviacion = this.TiposDocumentosAbreviacion;
    this.parametrosYAtributos.TiposDocumentosParametroCod = this.TiposDocumentosParametroCod;
    this.parametrosYAtributos.TiposDocumentosId = this.TiposDocumentosId;
    this.parametrosYAtributos.Nacionalidades = this.Nacionalidades;
    this.parametrosYAtributos.NacionalidadesAbreviacion = this.NacionalidadesAbreviacion;
    this.parametrosYAtributos.NacionalidadesParametroCod = this.NacionalidadesParametroCod;
    this.parametrosYAtributos.Paises = this.Paises;
    this.parametrosYAtributos.PaisesAbreviacion = this.PaisesAbreviacion;
    this.parametrosYAtributos.PaisesParametroCod = this.PaisesParametroCod;
    this.parametrosYAtributos.PaisesId = this.PaisesId;
    this.parametrosYAtributos.EstadosInstaciaPoliza = this.EstadosInstaciaPoliza;
    this.parametrosYAtributos.EstadosInstaciaPolizaId = this.EstadosInstaciaPolizaId;
    this.parametrosYAtributos.Sexos = this.Sexos;
    this.parametrosYAtributos.SexosCod = this.SexosCod;
    this.parametrosYAtributos.SexosAbreviacion = this.SexosAbreviacion;
    this.parametrosYAtributos.SexosParametroCod = this.SexosParametroCod;
    this.parametrosYAtributos.SexosId = this.SexosId;
    this.parametrosYAtributos.ProcedenciaCI = this.ProcedenciaCI;
    this.parametrosYAtributos.ProcedenciaCIAbreviacion = this.ProcedenciaCIAbreviacion;
    this.parametrosYAtributos.ProcedenciaCIParametroCod = this.ProcedenciaCIParametroCod;
    this.parametrosYAtributos.ProcedenciaCIParametroCodDescripcion = this.ProcedenciaCIParametroCodDescripcion;
    this.parametrosYAtributos.Parentesco = this.Parentesco;
    this.parametrosYAtributos.ParentescoAbreviacion = this.ParentescoAbreviacion;
    this.parametrosYAtributos.Condiciones = this.Condiciones;
    this.parametrosYAtributos.CondicionesPep = this.CondicionesPep;
    this.parametrosYAtributos.CondicionesId = this.CondicionesId;
    this.parametrosYAtributos.CondicionesIdDesc = this.CondicionesIdDesc;
    this.parametrosYAtributos.Monedas = this.Monedas;
    this.parametrosYAtributos.MonedasAbreviacion = this.MonedasAbreviacion;
    this.parametrosYAtributos.MonedasParametroCod = this.MonedasParametroCod;
    this.parametrosYAtributos.MonedasStrParametroCod = this.MonedasStrParametroCod;
    this.parametrosYAtributos.Estado = this.Estado;
    this.parametrosYAtributos.EstadoAbreviacion = this.EstadoAbreviacion;
    this.parametrosYAtributos.Tipo = this.Tipo;
    this.parametrosYAtributos.TipoAbreviacion = this.TipoAbreviacion;
    this.parametrosYAtributos.id_poliza = this.id_poliza;
    this.parametrosYAtributos.id_asegurado = asegurado.id;
    this.parametrosYAtributos.id_anexo_poliza = this.persona_banco_datos.plan ? this.persona_banco_datos.plan : null;
    this.parametrosYAtributos.persona_banco_pep = this.persona_banco_pep;
    this.parametrosYAtributos.componentes = this.componentesInvisibles;
    this.parametrosYAtributos.poliza = this.poliza;
    this.parametrosYAtributos.documentoSolicitud = this.documentoSolicitud;
    this.parametrosYAtributos.documentoCertificado = this.documentoCertificado;
    this.parametrosYAtributos.documentoComprobante = this.documentoComprobante;
    this.parametrosYAtributos.documentoCotizacion = this.documentoCotizacion;
    this.parametrosYAtributos.documentoCronogramaPago = this.documentoCronogramaPago;
    this.parametrosYAtributos.objetoAseguradoDatosComplementarios = this.objetoAseguradoDatosComplementarios;
    this.parametrosYAtributos.ObjetoAseguradoAtributosFiltered = this.ObjetoAseguradoAtributosFiltered;
    this.parametrosYAtributos.persona_banco = this.persona_banco;
    this.parametrosYAtributos.persona_banco_accounts = this.persona_banco_accounts;
    this.parametrosYAtributos.persona_banco_account = this.persona_banco_account;
    this.parametrosYAtributos.instanciaDocumentoSolicitud = this.instanciaDocumentoSolicitud;
    this.parametrosYAtributos.instanciaDocumentoComprobante = this.instanciaDocumentoComprobante;
    this.parametrosYAtributos.instanciaDocumentoCotizacion = this.instanciaDocumentoCotizacion;
    this.parametrosYAtributos.instanciaDocumentoCronogramaPagos = this.instanciaDocumentoCronogramaPagos;
    this.parametrosYAtributos.instanciaDocumentoCertificado = this.instanciaDocumentoCertificado;
    this.parametrosYAtributos.estadosPoliza = this.estadosPoliza;
    this.parametrosYAtributos.estadosObservaciones = this.estadosObservaciones;
    this.parametrosYAtributos.ObservacionesId = this.ObservacionesId;
    this.parametrosYAtributos.usuarioLogin = this.usuarioLogin;
    this.parametrosYAtributos.hasRolConsultaTarjetas = this.hasRolConsultaTarjetas;
    this.parametrosYAtributos.hasRolConsultaCajero = this.hasRolConsultaCajero;
    this.parametrosYAtributos.hasRolAdmin = this.hasRolAdmin;

    if (typeof callback == "function") {
      callback();
    }
  }

  async setAtributosDocumentosToPersonaBanco(
      callback: Function = null,
      idAtributosInclude = [],
      idAtributosExclude = []
  ) {
    if (this.asegurado) {
      for (let i = 0; i < this.asegurado.instancia_poliza.instancia_documentos.length; i++) {
        let instanciaDocumento = this.asegurado.instancia_poliza.instancia_documentos[i];
        let atributosInstanciaDocumento;
        if (idAtributosInclude.length) {
          atributosInstanciaDocumento = instanciaDocumento.atributo_instancia_documentos.filter((param) => idAtributosInclude.includes(parseInt(param.objeto_x_atributo.id_atributo + "")));
        } else {
          atributosInstanciaDocumento = instanciaDocumento.atributo_instancia_documentos;
        }
        for (let j = 0; j < atributosInstanciaDocumento.length; j++) {
          let atributoInstanciaDocumento:Atributo_instancia_documento = atributosInstanciaDocumento[i];
          if (atributoInstanciaDocumento.valor && util.isJson(atributoInstanciaDocumento.valor)) {
              let respuestas: Respuesta[] = JSON.parse(atributoInstanciaDocumento.valor);
              let valuesRespuestas = respuestas.map(param => {return param.value});
              if (this.persona_banco_datos.cuestionario.preguntas_cerradas && this.persona_banco_datos.cuestionario.preguntas_cerradas.length) {
                  for (let k = 0; k < this.persona_banco_datos.cuestionario.preguntas_cerradas.length; k++) {
                      let preguntaCerrada:Pregunta_cerrada = this.persona_banco_datos.cuestionario.preguntas_cerradas[k];
                      if (preguntaCerrada.opciones.find(param => valuesRespuestas.includes(param.value))) {
                        preguntaCerrada.respuestas = respuestas;
                      }
                  }
              }
          }
        }
      }
      if (typeof callback == 'function') {
          await callback();
      }
    }
  }

  async setAtributosToPersonaBanco(
        form: FormGroup = null,
        callback: Function = null,
        idAtributosInclude = [],
        idAtributosExclude = []
    ) {
      if (this.asegurado) {
          this.persona_banco = new persona_banco();
          this.persona_banco_account = new persona_banco_account();
          this.persona_banco_tarjeta_debito_cuenta_vinculada = new persona_banco_tarjeta_debito_cuentas_vinculadas();
          this.persona_banco_datos = new persona_banco_datos();
          this.persona_banco_tarjeta_credito = new persona_banco_tarjeta_credito();
          this.persona_banco_datos = new persona_banco_datos();
          this.persona_banco_transaccion = new persona_banco_transaccion();
          this.persona_banco_pep = new persona_banco_pep();
          this.persona_banco_solicitud = new persona_banco_solicitud();
          this.persona_banco_operacion = new persona_banco_operacion();
          this.persona_banco_tarjeta_ci_ext = new persona_banco_tarjeta_ci_ext();

            this.persona_banco.doc_id = this.asegurado.entidad.persona.persona_doc_id;
            this.persona_banco.complemento = this.asegurado.entidad.persona.persona_doc_id_comp;
            this.persona_banco.extension = util.isNumber(this.asegurado.entidad.persona.persona_doc_id_ext) ? this.asegurado.entidad.persona.persona_doc_id_ext : this.ProcedenciaCIAbreviacion[this.asegurado.entidad.persona.persona_doc_id_ext];
            this.persona_banco.paterno = this.asegurado.entidad.persona.persona_primer_apellido;
            this.persona_banco.materno = this.asegurado.entidad.persona.persona_segundo_apellido;
            this.persona_banco.nombre = this.asegurado.entidad.persona.persona_primer_nombre;
            this.persona_banco.apcasada = this.asegurado.entidad.persona.persona_apellido_casada;
            this.persona_banco.direccion = this.asegurado.entidad.persona.persona_direccion_domicilio;
            this.persona_banco.fecha_nacimiento = this.asegurado.entidad.persona.persona_fecha_nacimiento;
            this.persona_banco.fecha_nacimiento_str = this.asegurado.entidad.persona.persona_fecha_nacimiento+'';
            this.setDatesOfAsegurado();
            if (this.asegurado.entidad.persona.persona_fecha_nacimiento != null) {
                this.asegurado.entidad.persona.persona_fecha_nacimiento = new Date(this.asegurado.entidad.persona.persona_fecha_nacimiento+'');
            }
            this.asegurado.instancia_poliza.fecha_registro = new Date(this.asegurado.instancia_poliza.fecha_registro+'');
            this.persona_banco.fono_domicilio = this.asegurado.entidad.persona.persona_telefono_domicilio;
            this.persona_banco.fono_oficina = this.asegurado.entidad.persona.persona_telefono_trabajo;
            this.persona_banco.nro_celular = this.asegurado.entidad.persona.persona_celular;
            this.persona_banco.e_mail = this.asegurado.entidad.persona.persona_email_personal;
            this.persona_banco_datos.pais_nacimiento = this.asegurado.entidad.persona.par_pais_nacimiento_id;

            this.fechaEdadMinima = this.getFechaMinimaSegunEdad();
            this.fechaEdadMaxima = this.getFechaMaximaSegunEdad();

            this.instanciaDocumentoSolicitud = new Instancia_documento();
            this.instanciaDocumentoComprobante = new Instancia_documento();
            this.instanciaDocumentoCotizacion = new Instancia_documento();
            this.instanciaDocumentoCronogramaPagos = new Instancia_documento();
            this.instanciaDocumentoCertificado = new Instancia_documento();

            if (this.asegurado.entidad.persona.par_sexo_id) {
                this.persona_banco.sexo = this.SexosId[this.asegurado.entidad.persona.par_sexo_id + ""];
            }
            // await this.asegurado.instancia_poliza.instancia_documentos.forEach(async (instancia_documento: Instancia_documento) => {
            //   instancia_documento.atributo_instancia_documentos_inter = [];
            //   // TODO: Iterar instancia_documento.atributo_instancia_documentos
            // });

            this.asegurado.instancia_poliza.atributo_instancia_polizas_inter = [];
            this.asegurado.instancia_poliza.atributo_instancia_polizas.forEach((atributo_instancia_poliza: Atributo_instancia_poliza) => {
              if (atributo_instancia_poliza.objeto_x_atributo) {
                if (this.parametroVisible && atributo_instancia_poliza.objeto_x_atributo.par_comportamiento_interfaz_id == this.parametroVisible.id) {
                    if (!atributo_instancia_poliza.valor) {
                      atributo_instancia_poliza.valor = '';
                    }
                    this.asegurado.instancia_poliza.atributo_instancia_polizas_inter.push(atributo_instancia_poliza);
                }
              }
            });
            if (this.asegurado.instancia_poliza.instancia_documentos.length) {
              for (let i = 0; i < this.asegurado.instancia_poliza.instancia_documentos.length; i++) {
                let instancia_documento: Instancia_documento = this.asegurado.instancia_poliza.instancia_documentos[i];
                if (typeof instancia_documento.fecha_inicio_vigencia == "string") {
                  instancia_documento.fecha_inicio_vigencia = new Date(instancia_documento.fecha_inicio_vigencia);
                }
                if (typeof instancia_documento.fecha_fin_vigencia == "string") {
                  instancia_documento.fecha_fin_vigencia = new Date(instancia_documento.fecha_fin_vigencia);
                }
                if (typeof instancia_documento.fecha_emision == "string") {
                  instancia_documento.fecha_emision = new Date(instancia_documento.fecha_emision);
                }
                if (instancia_documento.id_documento == this.documentoSolicitud.id) {
                  this.instanciaDocumentoSolicitud = instancia_documento;
                  if (this.instanciaDocumentoSolicitud.nombre_archivo) {
                    this.fileDocumentoSolicitud = new Upload();
                    this.fileDocumentoSolicitud.path = this.documentoSolicitud.archivo.ubicacion_local+this.instanciaDocumentoSolicitud.nombre_archivo;
                    this.fileDocumentoSolicitud.tipoDocumento = this.documentoSolicitud.descripcion;
                    this.fileDocumentoSolicitud.idTipoDocumento = this.documentoSolicitud.id_tipo_documento;
                    this.fileDocumentoSolicitud.name = this.instanciaDocumentoSolicitud.nombre_archivo;
                    this.fileDocumentoSolicitud.newName = this.instanciaDocumentoSolicitud.nombre_archivo;
                    [this.fileDocumentoSolicitud.file, this.fileDocumentoSolicitud.ext] = this.instanciaDocumentoSolicitud.nombre_archivo.split('.');
                  }
                } else if (instancia_documento.id_documento == this.documentoComprobante.id) {
                  this.instanciaDocumentoComprobante = instancia_documento;
                } else if (instancia_documento.id_documento == this.documentoCotizacion.id) {
                  this.instanciaDocumentoCotizacion = instancia_documento;
                } else if (instancia_documento.id_documento == this.documentoCronogramaPago.id) {
                  this.instanciaDocumentoCronogramaPagos = instancia_documento;
                } else if (instancia_documento.id_documento == this.documentoCertificado.id) {
                  this.instanciaDocumentoCertificado = instancia_documento;
                  if (this.instanciaDocumentoCertificado.nombre_archivo) {
                    this.fileDocumentoCertificado = new Upload();
                    this.fileDocumentoCertificado.path = this.documentoCertificado.archivo.ubicacion_local+this.instanciaDocumentoCertificado.nombre_archivo;
                    this.fileDocumentoCertificado.tipoDocumento = this.documentoCertificado.descripcion;
                    this.fileDocumentoCertificado.idTipoDocumento = this.documentoCertificado.id_tipo_documento;
                    this.fileDocumentoCertificado.name = this.instanciaDocumentoCertificado.nombre_archivo;
                    this.fileDocumentoCertificado.newName = this.instanciaDocumentoCertificado.nombre_archivo;
                    [this.fileDocumentoCertificado.file, this.fileDocumentoCertificado.ext] = this.instanciaDocumentoCertificado.nombre_archivo.split('.');
                  }
                }
              }
            }
            let atributoInstanciaPolizas;
            if (idAtributosInclude.length) {
                atributoInstanciaPolizas = this.asegurado.instancia_poliza.atributo_instancia_polizas.filter((param) => idAtributosInclude.includes(parseInt(param.objeto_x_atributo.id_atributo + "")));
            } else {
                atributoInstanciaPolizas = this.asegurado.instancia_poliza.atributo_instancia_polizas;
            }
            this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected = new cuenta();
            for (let i = 0; i < atributoInstanciaPolizas.length; i++) {
                let atributoInstanciaPoliza = atributoInstanciaPolizas[i];
                if (atributoInstanciaPoliza.objeto_x_atributo) {
                    switch (atributoInstanciaPoliza.objeto_x_atributo.atributo.id + "") {
                        case "10":
                            if (this.persona_banco_tarjeta_debito_cuenta_vinculada && this.id_poliza == 4) {
                              this.persona_banco.cod_agenda = atributoInstanciaPoliza.valor == "" ? this.persona_banco.cod_agenda : atributoInstanciaPoliza.valor;
                            } else {
                              this.persona_banco_account.cod_agenda = atributoInstanciaPoliza.valor == "" ? this.persona_banco_account.cod_agenda : atributoInstanciaPoliza.valor;
                              this.persona_banco.cod_agenda = atributoInstanciaPoliza.valor == "" ? this.persona_banco.cod_agenda : atributoInstanciaPoliza.valor;
                            }
                            break;
                        case "5":
                            this.atributoNroCuenta = atributoInstanciaPoliza;
                            if (this.persona_banco_tarjeta_debito_cuenta_vinculada && this.id_poliza == 4) {
                              this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.nro_cuenta = atributoInstanciaPoliza.valor;
                            } else {
                              this.persona_banco_account.nrocuenta = atributoInstanciaPoliza.valor;
                            }
                            if(!this.Cuentas.find(param => param.value == this.atributoNroCuenta.valor)) {
                              this.Cuentas.push({value:this.atributoNroCuenta.valor, label:this.atributoNroCuenta.valor});
                            }
                            break;
                        case "12":
                            this.persona_banco.debito_automatico = atributoInstanciaPoliza.valor;
                            this.atributoDebitoAutomatico = atributoInstanciaPoliza;
                            break;
                        case "14":
                            this.persona_banco.caedec = atributoInstanciaPoliza.valor;
                            break;
                        case "16":
                            this.persona_banco.localidad = atributoInstanciaPoliza.valor;
                            break;
                        case "17":
                            this.persona_banco.departamento = atributoInstanciaPoliza.valor;
                            break;
                        case "18":
                            this.persona_banco.cod_sucursal = atributoInstanciaPoliza.valor;
                            break;
                        case "19":
                            this.persona_banco_datos.tipo_doc = atributoInstanciaPoliza.valor;
                            this.atributoTipoDoc = atributoInstanciaPoliza;
                            break;
                        case "4":
                            this.persona_banco.e_mail = atributoInstanciaPoliza.valor;
                            this.atributoEmail = atributoInstanciaPoliza;
                            break;
                        case "3":
                            this.persona_banco.est_civil = atributoInstanciaPoliza.valor;
                            break;
                        case "20":
                            if (this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected && this.id_poliza == 4) {
                                //this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.nro_cuenta = atributoInstanciaPoliza.valor;
                            } else {
                                this.persona_banco_account.manejo = atributoInstanciaPoliza.valor;
                            }
                            break;
                        case "21":
                            if (this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected && this.id_poliza == 4) {
                                this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.moneda = atributoInstanciaPoliza.valor;
                                if (form != null) {
                                    form.controls["par_moneda"].setValue(this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.moneda);
                                }
                            } else {
                                this.persona_banco_account.moneda = atributoInstanciaPoliza.valor;
                                if (form != null) {
                                    form.controls["par_moneda"].setValue(this.persona_banco_account.moneda);
                                }
                            }
                            this.atributoMoneda = atributoInstanciaPoliza;
                            break;
                        case "22":
                            this.persona_banco.desc_caedec = atributoInstanciaPoliza.valor;
                            break;
                        case "23":
                            if (this.id_poliza == 4) {
                              this.persona_banco_tarjeta_debito_cuenta_vinculada.nro_tarjeta = atributoInstanciaPoliza.valor;
                            } else {
                              this.persona_banco_datos.nro_tarjeta = atributoInstanciaPoliza.valor;
                              this.persona_banco_tarjeta_ci_ext.nrotarjeta = atributoInstanciaPoliza.valor;
                            }
                            this.atributoNroTarjeta = atributoInstanciaPoliza;
                            break;
                        case "24":
                            this.persona_banco_tarjeta_credito.tarjeta_nombre = atributoInstanciaPoliza.valor;
                            break;
                        case "25":
                            this.persona_banco_tarjeta_credito.valida = atributoInstanciaPoliza.valor;
                            break;
                        case "28":
                            this.persona_banco_datos.modalidad_pago = atributoInstanciaPoliza.valor;
                            this.atributoModalidadPago = atributoInstanciaPoliza;
                            break;
                        case "29":
                            this.persona_banco_datos.zona = atributoInstanciaPoliza.valor;
                            this.atributoZona = atributoInstanciaPoliza;
                            break;
                        case "30":
                            this.persona_banco_datos.nro_direccion = atributoInstanciaPoliza.valor;
                            this.atributoNroDireccion = atributoInstanciaPoliza;
                            break;
                        case "32":
                            this.persona_banco_datos.razon_social = atributoInstanciaPoliza.valor;
                            this.atributoRazonSocial = atributoInstanciaPoliza;
                            break;
                        case "33":
                            this.persona_banco_datos.nit_carnet = atributoInstanciaPoliza.valor;
                            this.atributoNitCarnet = atributoInstanciaPoliza;
                            break;
                        case "34":
                            this.persona_banco_tarjeta_debito.tarjeta_ultimos_cuatro_digitos = atributoInstanciaPoliza.valor;
                            this.atributoUltimosCuatroDigitos = atributoInstanciaPoliza;
                            break;
                        case "35":
                            if (this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected && this.id_poliza == 4) {

                            } else {
                                this.persona_banco_account.fecha_expiracion = new Date(atributoInstanciaPoliza.valor);
                            }
                            this.atributoCtaFechaExpiracion = atributoInstanciaPoliza;
                            break;
                        case "36":
                            if (this.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected && this.id_poliza == 4) {
                                this.persona_banco_tarjeta_debito_cuenta_vinculada.tipo = atributoInstanciaPoliza.valor;
                            } else {
                                this.persona_banco_datos_tarjeta.tipo_tarjeta = atributoInstanciaPoliza.valor;
                            }
                            this.atributoTipoTarjeta = atributoInstanciaPoliza;
                            break;
                        case "37":
                            this.persona_banco_datos.ocupacion = atributoInstanciaPoliza.valor;
                            this.atributoOcupacion = atributoInstanciaPoliza;
                            break;
                        case "38":
                            this.persona_banco_datos.plan = parseInt(atributoInstanciaPoliza.valor);
                            this.atributoPlan = atributoInstanciaPoliza;
                            break;
                        case "58":
                            this.persona_banco_datos.plazo = parseInt(atributoInstanciaPoliza.valor);
                            this.atributoPlazo = atributoInstanciaPoliza;
                            break;
                        case "60":
                            if (this.usuarioLogin.usuario_banco) {
                                this.usuarioLogin.usuario_banco.us_sucursal = parseInt(atributoInstanciaPoliza.valor);
                                this.atributoSucursal = atributoInstanciaPoliza;
                            } else {
                                this.atributoSucursal = atributoInstanciaPoliza;
                            }
                            break;
                        case "59":
                            if (this.usuarioLogin.usuario_banco) {
                                this.usuarioLogin.usuario_banco.us_oficina = parseInt(atributoInstanciaPoliza.valor);
                                this.atributoAgencia = atributoInstanciaPoliza;
                            } else {
                                this.atributoAgencia = atributoInstanciaPoliza;
                            }
                            break;
                        case "70":
                            if (this.usuarioLogin.usuario_banco) {
                                this.usuarioLogin.usuario_banco.us_cargo = atributoInstanciaPoliza.valor;
                                this.atributoUsuarioCargo = atributoInstanciaPoliza;
                            } else {
                                this.atributoUsuarioCargo = atributoInstanciaPoliza;
                            }
                            break;
                        case "1":
                            this.persona_banco_datos.prima = parseInt(atributoInstanciaPoliza.valor);
                            this.atributoPrima = atributoInstanciaPoliza;
                            break;
                        case "62":
                            this.persona_banco_datos.telefono_celular = atributoInstanciaPoliza.valor;
                            this.atributoTelefonoCelular = atributoInstanciaPoliza;
                            break;
                        case "63":
                            this.persona_banco_datos.ciudad_nacimiento = atributoInstanciaPoliza.valor;
                            this.atributoCiudadNacimiento = atributoInstanciaPoliza;
                            break;
                        case "64":
                            this.persona_banco_transaccion.nrotran = atributoInstanciaPoliza.valor;
                            this.persona_banco_transaccion_debito_csg.nrotran = atributoInstanciaPoliza.valor;
                            this.atributoNroTransaccion = atributoInstanciaPoliza;
                            break;
                        case "65":
                            this.persona_banco_transaccion.fechatran = atributoInstanciaPoliza.valor;
                            this.atributoFechaTransaccion = atributoInstanciaPoliza;
                            break;
                        case "66":
                            this.persona_banco_transaccion.importe = atributoInstanciaPoliza.valor;
                            this.persona_banco_transaccion_debito_csg.importe = atributoInstanciaPoliza.valor;
                            this.atributoTransaccionImporte = atributoInstanciaPoliza;
                            break;
                        case "69":
                            this.persona_banco_transaccion.detalle = atributoInstanciaPoliza.valor;
                            this.persona_banco_transaccion_debito_csg.descripcion = atributoInstanciaPoliza.valor;
                            this.atributoTransaccionDetalle = atributoInstanciaPoliza;
                            break;
                        case "67":
                            this.persona_banco_transaccion.moneda = atributoInstanciaPoliza.valor;
                            this.persona_banco_transaccion_debito_csg.moneda = atributoInstanciaPoliza.valor;
                            this.atributoTransaccionMoneda = atributoInstanciaPoliza;
                            break;
                        case "96":
                            this.persona_banco_transaccion_debito_csg.estado = atributoInstanciaPoliza.valor;
                            this.atributoTransaccionDebitoCSGEstado = atributoInstanciaPoliza;
                            break;
                        case "98":
                            this.persona_banco_transaccion.codigo_via = atributoInstanciaPoliza.valor;
                            this.atributoTransaccionCodigoVia = atributoInstanciaPoliza;
                            break;
                        case "71":
                            this.persona_banco_pep.condicion = atributoInstanciaPoliza.valor;
                            this.atributoCondicionPep = atributoInstanciaPoliza;
                            break;
                        case "72":
                            this.persona_banco_pep.cargo_entidad = atributoInstanciaPoliza.valor;
                            this.atributoCargoEntidadPep = atributoInstanciaPoliza;
                            break;
                        case "73":
                            this.persona_banco_pep.periodo_cargo_publico = atributoInstanciaPoliza.valor;
                            this.atributoPeriodoCargoPublico = atributoInstanciaPoliza;
                            break;
                        case "74":
                            this.persona_banco_datos.direccion_laboral = atributoInstanciaPoliza.valor;
                            this.atributoDireccionLaboral = atributoInstanciaPoliza;
                            break;
                        case "75":
                            this.persona_banco_datos.tipo_cuenta = atributoInstanciaPoliza.valor;
                            this.atributoTipoCuenta = atributoInstanciaPoliza;
                            break;
                        case "80":
                            this.persona_banco_datos.producto_asociado = atributoInstanciaPoliza.valor;
                            this.atributoProductoAsociado = atributoInstanciaPoliza;
                            break;
                        case "76":
                            this.persona_banco_datos.nro_cuotas = parseInt(atributoInstanciaPoliza.valor);
                            this.atributoNroCuotas = atributoInstanciaPoliza;
                            break;
                        case "77":
                            this.persona_banco_datos.desc_ocupacion = atributoInstanciaPoliza.valor;
                            this.atributoDescOcupacion = atributoInstanciaPoliza;
                            break;
                        case "78":
                            this.persona_banco_solicitud.solicitud_sci_selected = atributoInstanciaPoliza.valor;
                            this.persona_banco_operacion.Operacion_Solicitud = atributoInstanciaPoliza.valor;
                            this.atributoSolicitudSci = atributoInstanciaPoliza;
                            if(!this.SolicitudesSci.find(param => param.value == this.atributoSolicitudSci.valor)) {
                              this.SolicitudesSci.push({value:this.atributoSolicitudSci.valor, label:this.atributoSolicitudSci.valor});
                            }
                            if (this.atributoSolicitudSci && this.atributoSolicitudSci.valor && this.atributoSolicitudSci.valor.substring(0,1) == '9') {
                              this.pagoACredito = true;
                            } else {
                              this.pagoACredito = false;
                            }
                            break;
                        case "61":
                            this.persona_banco_datos.monto = parseInt(atributoInstanciaPoliza.valor);
                            this.atributoMonto = atributoInstanciaPoliza;
                            break;
                        case "81":
                            this.persona_banco_solicitud.solicitud_sci_estado = atributoInstanciaPoliza.valor;
                            this.persona_banco_operacion.Operacion_Estado = atributoInstanciaPoliza.valor;
                            this.atributoSolicitudSciEstado = atributoInstanciaPoliza;
                            if (this.persona_banco_operacion.Operacion_Estado && this.persona_banco_operacion.Operacion_Estado.includes('APROBAD')) {
                              this.solicitudAprobada = true;
                            } else {
                              this.solicitudAprobada = false;
                            }
                            break;
                        case `82`:
                          this.persona_banco_operacion.Operacion_Plazo= atributoInstanciaPoliza.valor;
                          this.persona_banco_solicitud.solicitud_plazo_credito= atributoInstanciaPoliza.valor;
                          this.atributoSolicitudPlazoCredito = atributoInstanciaPoliza;
                          break;
                        case `83`:
                          this.persona_banco_operacion.Operacion_Moneda= atributoInstanciaPoliza.valor;
                          this.persona_banco_solicitud.solicitud_moneda= atributoInstanciaPoliza.valor;
                          this.atributoSolicitudMoneda = atributoInstanciaPoliza;
                          break;
                        case `84`:
                          this.persona_banco_operacion.Operacion_Frecuencia = atributoInstanciaPoliza.valor;
                          this.atributoSolicitudFrecuenciaPlazo = atributoInstanciaPoliza;
                          break;
                        case `85`:
                          this.persona_banco_solicitud.solicitud_prima_total = atributoInstanciaPoliza.valor;
                          this.atributoSolicitudPrimaTotal = atributoInstanciaPoliza;
                          break;
                        case `86`:
                          this.persona_banco_solicitud.solicitud_forma_pago = atributoInstanciaPoliza.valor;
                          this.atributoFormaPago = atributoInstanciaPoliza;
                          break;
                        case `87`:
                          this.persona_banco_tarjeta_debito_cuenta_vinculada.fecha_activacion = typeof atributoInstanciaPoliza.valor == 'string' ? new Date(atributoInstanciaPoliza.valor) : atributoInstanciaPoliza.valor;
                          this.atributoFechaActivacion = atributoInstanciaPoliza;
                          break;
                        case `88`:
                          this.persona_banco_tarjeta_debito_cuenta_vinculada.fin_vigencia = typeof atributoInstanciaPoliza.valor == 'string' ? new Date(atributoInstanciaPoliza.valor) : atributoInstanciaPoliza.valor;
                          this.atributoFechaFinVigencia = atributoInstanciaPoliza;
                          break;
                        case `89`:
                          this.persona_banco_tarjeta_debito_cuenta_vinculada.estado_tarjeta = atributoInstanciaPoliza.valor;
                          this.atributoEstadoTarjeta = atributoInstanciaPoliza;
                          break;
                        case `90`:
                          this.persona_banco_operacion.Operacion_Tipo_Credito = atributoInstanciaPoliza.valor;
                          this.atributoTipoCredito= atributoInstanciaPoliza;
                          break;
                        case `91`:
                          this.persona_banco_datos.provincia = atributoInstanciaPoliza.valor;
                          this.atributoProvincia = atributoInstanciaPoliza;
                          break;
                        case `92`:
                          this.persona_banco_datos.lugar_nacimiento = atributoInstanciaPoliza.valor;
                          this.atributoLugarNacimiento = atributoInstanciaPoliza;
                          break;
                        case `93`:
                          this.persona_banco_tarjeta_ci_ext.id = atributoInstanciaPoliza.valor;
                          this.atributoTarjetaId = atributoInstanciaPoliza;
                          break;
                    }
                }
            }
        }
        // else {
        //     this.id_this.asegurado = "";
        // }
        if (typeof callback == "function") {
            callback();
        }
    }

    async btnOrdenPago(isEnabled: boolean) {
        if (isEnabled) {
            if (this.condicionNo && this.condicionNo.id) {
                if (this.atributoDebitoAutomatico.valor == this.condicionNo.id + "") {
                    this.btnOrdenPagoEnabled = false;
                    this.showOrdenPago = false;
                    this.showPlanPago = true;
                    this.btnPlanPagoEnabled = true;
                } else if (
                    this.atributoDebitoAutomatico.valor == this.condicionSi.id + "") {
                    this.btnPlanPagoEnabled = false;
                    this.btnOrdenPagoEnabled = true;
                } else {
                    //this.showOrdenPago = false;
                }
            } else {
                this.btnPlanPagoEnabled = false;
                this.btnOrdenPagoEnabled = false;
            }
        } else {
            this.btnPlanPagoEnabled = false;
            this.btnOrdenPagoEnabled = false;
        }
      if ([10,12].includes(this.poliza.id)){
        this.showOrdenPago = false;
      }
    }


  async validarFechaMinima(userform) {
    let fechaMin = this.getFechaMinimaSegunEdad();
    let diff = new Date().getTime() - this.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
    let yearsMiliseconds = 5.676e+11 // 18 años;
    if( diff < fechaMin.getTime()) {
      userform.controls['persona_fecha_nacimiento'].setErrors([Validators.min(fechaMin.getTime())]);
      this.atributoFechaNacimiento.tipo_error = 'La persona no es mayor de 18 años';
      this.atributoApellidoPaterno.requerido = true;
      // this.isLoadingAgain = false;
      this.stopSaving = true;
    } else {
      userform.controls['persona_fecha_nacimiento'].clearAsyncValidators();
      userform.controls['persona_fecha_nacimiento'].setErrors(null);
      this.stopSaving = false;
    }
  }

  async validarFechaMaxima(userform) {
    let fechaMax = this.getFechaMaximaSegunEdad();
    let diff = new Date().getTime() - this.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
    let yearsMiliseconds = 2.05e+12 // 65 años;
    if( diff > fechaMax.getTime()) {
      userform.controls['persona_fecha_nacimiento'].setErrors([Validators.max(fechaMax.getTime())]);
      this.atributoFechaNacimiento.tipo_error = 'La persona es mayor de 65 años';
      this.atributoApellidoPaterno.requerido = true;
      // this.isLoadingAgain = false;
      this.stopSaving = true;
    } else {
      userform.controls['persona_fecha_nacimiento'].clearAsyncValidators();
      userform.controls['persona_fecha_nacimiento'].setErrors(null);
      this.stopSaving = false;
    }
  }

    async validarArchivos() {
      let validaciones = [], validados = [], validacion;
      validaciones[1] = "No se subio ningun archivo de solicitud";
      validaciones[2] = "No se subio ningun archivo de emisión ";

        let respValidacionArchivos = [];

        // 1: No Existe un documento de solicitud subido
        // 2: No Existe un documento de certificado subido

        /* Valida que haya Archivos */
      if ([this.estadoSolicitado.id].includes(this.asegurado.instancia_poliza.id_estado)) {
        if (!this.instanciaDocumentoSolicitud.nombre_archivo) {
          respValidacionArchivos.push(1)
        } else if (!this.instanciaDocumentoCertificado.nombre_archivo) {
          respValidacionArchivos.push(2)
        }
      }

        if (isArray(respValidacionArchivos) && respValidacionArchivos.length) {
          respValidacionArchivos.forEach((code) => {
            switch (code + "") {
              case "1":
                validacion = validaciones[1];
                if (
                  !this.msgs_error.find((param) => param == validacion) &&
                  !this.msgs_error.find((param) => param.detail == validacion)
                ) {
                  this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                }
                break;
            }
          });
        } else {
          this.archivoSubido = true;
        }
    }

  ValidarPlanPago(instancia_poliza:Instancia_poliza) {
    if (instancia_poliza.plan_pagos) {
      return true;
    } else {
      return false;
    }
  }

    async validarBeneficiarios(componenteBeneficiario: BeneficiarioComponent) {
        let validaciones = [], validados = [], validacion;
        validaciones[1] = "El porcentaje de los beneficiarios no es igual a 100%.";
        validaciones[2] = "Uno de los beneficiarios introducido es el mismo titular";
        validaciones[3] = "Existen dos beneficiarios con el mismo número de carnet y extención";
        validaciones[4] = "Se verificó que existen dos conyugues";
        validaciones[5] = "No se encontro ningun beneficiario registrado.";
        validaciones[6] = "El parentesco conviviente esta duplicados, por favor verifica los datos.";
        validaciones[7] = "El parentesco conyugue esta duplicado, por favor verifica los datos.";
        validaciones[8] = "El parentesco hijo esta duplicado, por favor verifica los datos.";
        validaciones[9] = "El parentesco hermano esta duplicado, por favor verifica los datos.";
        validaciones[10] = "El parentesco madre esta duplicado, por favor verifica los datos.";
        validaciones[11] = "El parentesco padre esta duplicado, por favor verifica los datos.";
        validaciones[12] = "El parentesco otro parentesco esta duplicado, por favor verifica los datos.";
        validaciones[13] = "Debe existir al menos un beneficiario primario";
        validaciones[14] = "Debe existir al menos un beneficiario contingente";
        validaciones[15] = "El porcentaje asignado a los beneficiarios primarios no es igual a 100%, favor revise los datos en la seccion beneficiarios.";
        validaciones[16] = "El porcentaje asignado a los beneficiarios primarios no es igual a 100%, favor revise los datos en la seccion beneficiarios.";
        validaciones[17] = "El porcentaje asignado a los beneficiarios contingentes no es igual a 100%, favor revise los datos en la seccion beneficiarios.";
        validaciones[18] = "El porcentaje asignado a los beneficiarios contingentes no es igual a 100%, favor revise los datos en la seccion beneficiarios.";
        if (componenteBeneficiario) {
            let respValidacionBeneficiarios = componenteBeneficiario.validacionGeneralBeneficiarios();
            // 1: Porcentaje total no coincide con 100
            // 2: Beneficiario es titular
            // 3: Beneficiario repetidos en CI y EXT_CI
            // 4: Valida q no hay dos conyugues
            // 5: Sin beneficiarios

            if (isArray(respValidacionBeneficiarios)) {
                respValidacionBeneficiarios.forEach((code) => {
                  switch (code + "") {
                    case "1":
                      validacion = validaciones[1];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "2":
                      validacion = validaciones[2];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "3":
                      validacion = validaciones[3];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "4":
                      validacion = validaciones[4];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "5":
                      validacion = validaciones[5];
                      if (
                          !this.msgs_error.find((param) => param == validacion) &&
                          !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      if (
                        !this.msgs_info_warn.find((param) => param == validacion) &&
                        !this.msgs_info_warn.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroInfoWarning, validacion);
                      }
                      break;
                    case "6":
                      validacion = validaciones[6];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "7":
                      validacion = validaciones[7];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "8":
                      validacion = validaciones[8];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "9":
                      validacion = validaciones[9];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "10":
                      validacion = validaciones[10];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "11":
                      validacion = validaciones[11];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "12":
                      validacion = validaciones[12];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "13":
                      validacion = validaciones[13];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "14":
                      validacion = validaciones[14];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "15":
                      validacion = validaciones[15];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "16":
                      validacion = validaciones[16];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "17":
                      validacion = validaciones[17];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                    case "18":
                      validacion = validaciones[18];
                      if (
                        !this.msgs_error.find((param) => param == validacion) &&
                        !this.msgs_error.find((param) => param.detail == validacion)
                      ) {
                        this.setMsgsWarnsOrErrors(this.asegurado, this.parametroError, validacion);
                      }
                      break;
                  }
                });
            }
        }
    }

  getLasSolicitudVigente() {
    let finVigenciaDates = [];
    let finVigenciaInstancias = [];
    for (let i = 0; i < this.asegurados.length; i++) {
      let asegurado = this.asegurados[i];
      for (let j = 0; j < asegurado.instancia_poliza.instancia_documentos.length; j++) {
        let instanciaDocumento = asegurado.instancia_poliza.instancia_documentos[j];
        finVigenciaDates.push(new Date(instanciaDocumento.fecha_fin_vigencia + ''));
        finVigenciaInstancias.push(instanciaDocumento.id_instancia_poliza);
      }
    }
    let maxFinVigenciaDate = new Date(Math.max.apply(null, finVigenciaDates));
    let indexFinVigenciaDate = finVigenciaDates.findIndex((param) => param+'' == maxFinVigenciaDate+'');
    let idInstanciaFinVigencia = finVigenciaInstancias[indexFinVigenciaDate];
    let lastFinVigenciaSolicitud = this.asegurados.find((param) => param.instancia_poliza.id == idInstanciaFinVigencia);
    return [idInstanciaFinVigencia, lastFinVigenciaSolicitud];
  }

  async afterSearch(callback:any = null) {
    this.numero_solicitudes = this.asegurados.length;
    this.asegurado = this.asegurados[this.asegurados.length - 1];
    this.asegurado.instancia_poliza = new Instancia_poliza();
    this.setDatesOfAsegurado();
    this.asegurado.instancia_poliza.atributo_instancia_polizas_inter = [];
    await this.asegurado.instancia_poliza.atributo_instancia_polizas.forEach(async (atributo_instancia_poliza: Atributo_instancia_poliza) => {
      if (atributo_instancia_poliza.objeto_x_atributo.par_comportamiento_interfaz.id == this.parametroVisible.id) {
        this.asegurado.instancia_poliza.atributo_instancia_polizas_inter.push(atributo_instancia_poliza);
      }
    });
    if ([3, 13].includes(parseInt(this.id_poliza+''))) {
      this.displaySolicitudTarjetaCreditoExistente = true;
    } else {
      if (this.tarjetasAsegurar && this.tarjetasAsegurar.length > 1) {
        this.displayModalSolicitudExistenteCrear = true;
      } else {
        this.displayModalSolicitudExistente = true;
      }
    }
    if (this.numero_solicitudes > 1) {
      this.msgSolicitudExistente = "Se encontró (" + this.numero_solicitudes + ") solicitudes anteriores para el cliente, correspondiente a " + this.poliza.descripcion;
    } else {
      this.msgSolicitudExistente = "Se encontró (" + this.numero_solicitudes + ") solicitud anterior para el CI: " +
          this.asegurado.entidad.persona.persona_doc_id + " " +
          this.ProcedenciaCIParametroCod[this.asegurado.entidad.persona.persona_doc_id_ext + ''] + ", correspondiente a " + this.poliza.descripcion;
    }
    if (this.displayModalFormTitular) {
      this.displayModalFormTitular = false;
    }
    if (typeof callback == 'function') {
      callback()
    }
  }

  async afterSearchWithLimits(search, userform, callbackNewAsegurado:any = null, callbackAbrirModalRegistroCliente:any = null, callbackReiniciarEcoAccidente:any = null, callback:any = null) {
    this.aseguradosEcoresguardo = this.asegurados.filter(param => param.instancia_poliza.id_poliza == 12);
    this.aseguradoEcoAccidente = this.asegurados.find(param => param.instancia_poliza.id_poliza == 10);
    this.btnCrearEcoResguardoEnabled = true;
    this.numero_solicitudes = this.asegurados.length;
    this.displayModalSolicitudExistente = search;
    this.asegurado = this.aseguradoEcoAccidente ? this.aseguradoEcoAccidente : this.asegurados[0];
    if (search) {
      if (this.aseguradoEcoAccidente) {
        this.msgSolicitudExistente = `Se encontró (${this.numero_solicitudes}) solicitud(es) de seguro para el cliente: ${this.aseguradoEcoAccidente.entidad.persona.persona_primer_apellido} ${this.aseguradoEcoAccidente.entidad.persona.persona_segundo_apellido} ${this.aseguradoEcoAccidente.entidad.persona.persona_primer_nombre} ${this.aseguradoEcoAccidente.entidad.persona.persona_segundo_nombre}<br><br>`;
        for (let i = 0; i < this.asegurados.length; i++) {
          this.asegurado = this.asegurados[i];
          this.msgSolicitudExistente += `${i + 1}: ${this.asegurado.instancia_poliza.poliza.descripcion} de fecha: ${moment(this.asegurado.instancia_poliza.fecha_registro).format('DD/MM/YYYY')}<br> en estado ${this.EstadosInstaciaPolizaId[this.asegurado.instancia_poliza.id_estado]}<br>`;
        }
        if (this.aseguradosEcoresguardo.length >= 2) {
          this.msgSolicitudExistente += `<br> Ya no es posible instrumentar mas seguros para el cliente, si desea puede ver el detalle de los certificados registrados en el sistema.`;
          this.btnCrearEcoResguardoEnabled = false;
          this.displayModalSolicitudExistente = true;
        } else {
          if (this.aseguradoEcoAccidente.instancia_poliza.id_estado+'' == this.estadoEmitido.id+'' ||
              (this.aseguradoEcoAccidente.instancia_poliza.id_estado+'' == this.estadoPorEmitir.id+'' &&
                  new Date(this.aseguradoEcoAccidente.instancia_poliza.updatedAt).getDate() == new Date().getDate() &&
                  new Date(this.aseguradoEcoAccidente.instancia_poliza.updatedAt).getMonth() == new Date().getMonth() &&
                  new Date(this.aseguradoEcoAccidente.instancia_poliza.updatedAt).getFullYear() == new Date().getFullYear()) ||
              (this.aseguradoEcoAccidente.instancia_poliza.id_estado+'' == this.estadoPorPagar.id+'' &&
                  new Date(this.aseguradoEcoAccidente.instancia_poliza.updatedAt).getDate() == new Date().getDate() &&
                  new Date(this.aseguradoEcoAccidente.instancia_poliza.updatedAt).getMonth() == new Date().getMonth() &&
                  new Date(this.aseguradoEcoAccidente.instancia_poliza.updatedAt).getFullYear() == new Date().getFullYear())
          ) {
            if (this.aseguradosEcoresguardo.length) {
              if (this.aseguradosEcoresguardo.find(param =>
                  param.instancia_poliza.id_estado == this.estadoEmitido.id ||
                  (param.instancia_poliza.id_estado == this.estadoPorEmitir.id &&
                      new Date(param.instancia_poliza.updatedAt).getDate() == new Date().getDate() &&
                      new Date(param.instancia_poliza.updatedAt).getMonth() == new Date().getMonth() &&
                      new Date(param.instancia_poliza.updatedAt).getFullYear() == new Date().getFullYear()) ||
                  (param.instancia_poliza.id_estado == this.estadoPorPagar.id &&
                      new Date(param.instancia_poliza.updatedAt).getDate() == new Date().getDate() &&
                      new Date(param.instancia_poliza.updatedAt).getMonth() == new Date().getMonth() &&
                      new Date(param.instancia_poliza.updatedAt).getFullYear() == new Date().getFullYear())
              )) {
                  this.msgSolicitudExistente += `<br> Si el cliente lo desea, puede instrumentarse adicionalmente el seguro EcoResguardo o puede ver el detalle de los certificados registrados en el sistema.`;
                  this.parametrosRuteo.openForm = true;
                  this.btnCrearEcoResguardoEnabled = true;
                  if (this.displayModalFormTitular) {
                      this.displayModalFormTitular = false;
                  }
                  if (typeof callbackNewAsegurado == 'function') {
                      callbackNewAsegurado();
                  }
              } else {
                  this.msgSolicitudExistente += `<br>Antes de continuar debe resolver los casos que se encuentran pendientes de emisión`;
                  this.btnCrearEcoResguardoEnabled = false;
              }
            } else {
              this.msgSolicitudExistente += `<br> Si el cliente lo desea, puede instrumentarse adicionalmente el seguro EcoResguardo o puede ver el detalle de los certificados registrados en el sistema.`;
              this.btnCrearEcoResguardoEnabled = true;
              if (this.displayModalFormTitular) {
                this.displayModalFormTitular = false;
              }
              if (typeof callbackNewAsegurado == 'function') {
                callbackNewAsegurado();
              }
            }
          } else {
            this.msgSolicitudExistente += `<br>Antes de continuar debe resolver los casos que se encuentran pendientes de emisión`;
            this.btnCrearEcoResguardoEnabled = false;
          }
        }
      } else {
        if (this.aseguradosEcoresguardo.length >= 1) {
          this.aseguradoEcoresguardo = this.aseguradosEcoresguardo[0];
          this.msgSolicitudExistente = `Se encontró ${this.aseguradosEcoresguardo.length} seguro(s) para para el cliente: ${this.aseguradoEcoresguardo.entidad.persona.persona_primer_apellido} ${this.aseguradoEcoresguardo.entidad.persona.persona_segundo_apellido} ${this.aseguradoEcoresguardo.entidad.persona.persona_primer_nombre} ${this.aseguradoEcoresguardo.entidad.persona.persona_segundo_nombre}<br><br>`;
          for (let i = 0; i < this.asegurados.length; i++) {
            this.asegurado = this.asegurados[i];
            this.msgSolicitudExistente += `${i + 1}: ${this.asegurado.instancia_poliza.poliza.descripcion} de fecha: ${moment(this.asegurado.instancia_poliza.fecha_registro).format('DD/MM/YYYY')}<br> en estado ${this.EstadosInstaciaPolizaId[this.asegurado.instancia_poliza.id_estado]}<br>`;
          }
          if (this.aseguradosEcoresguardo.find(param => [this.estadoIniciado.id,this.estadoSolicitado.id,this.estadoPorEmitir.id,this.estadoPorPagar.id].includes(param.instancia_poliza.id_estado))) {
            this.msgSolicitudExistente += `<br>Antes de continuar debe resolver los casos que se encuentran pendientes de emisión`;
            this.btnCrearEcoAccidenteEnabled = false;
            this.btnCrearEcoResguardoEnabled = false;
          } else {
            this.msgSolicitudExistente += `<br> Si el cliente lo desea, puede instrumentarse adicionalmente el seguro EcoAccidente o puede ver el detalle de los certificados registrados en el sistema.`;
            this.btnCrearEcoAccidenteEnabled = true;
            this.btnCrearEcoResguardoEnabled = false;
          }
          this.displayModalSolicitudExistente = true;
        }
      }
    } else {
      if (this.numero_solicitudes > 1) {
        this.msgSolicitudExistente = `Se encontraron (${this.numero_solicitudes}) solicitud(es) de seguro para el cliente: ${this.asegurado.entidad.persona.persona_primer_apellido} ${this.asegurado.entidad.persona.persona_segundo_apellido} ${this.asegurado.entidad.persona.persona_primer_nombre} ${this.asegurado.entidad.persona.persona_segundo_nombre}<br><br>`;
        for (let i = 0; i < this.asegurados.length; i++) {
          let asegurado = this.asegurados[i];
          if(asegurado.instancia_poliza) {
            this.msgSolicitudExistente += `${i + 1}: Correspondiente al producto ${asegurado.instancia_poliza.poliza.descripcion} de: ${moment(asegurado.instancia_poliza.fecha_registro).format('DD/MM/YYYY')} en estado (${this.EstadosInstaciaPolizaId[asegurado.instancia_poliza.id_estado]})<br>`;
          }
        }
      } else {
        this.msgSolicitudExistente = `Se encontró (${this.numero_solicitudes}) solicitud correspondiente al producto ${this.poliza.descripcion} para el cliente: ${this.asegurado.entidad.persona.persona_primer_apellido} ${this.asegurado.entidad.persona.persona_primer_nombre}`;
      }
      this.asegurado = this.asegurados.find(param => [10,12].includes(parseInt(param.instancia_poliza.id_poliza+'')));
      if(this.asegurado) {
        this.asegurado.instancia_poliza.instancia_poliza_transicions = [];
        this.asegurado.instancia_poliza.atributo_instancia_polizas_inter = [];
        this.asegurado.instancia_poliza.atributo_instancia_polizas = this.asegurado.instancia_poliza.atributo_instancia_polizas.filter(param => ![64,65,66,67,69].includes(parseInt(param.objeto_x_atributo.id_atributo+'')));
        // this.setAtributosToPersonaBanco(userform);
      }
      // this.setAtributosToAsegurado();
      if (!this.esClienteBanco) {
        this.displayNuevoCliente = true;
      } else {
        if (!this.conCreditoAsociado) {
          this.displayClienteSinCredito = true;
        } else {
          if (typeof callbackAbrirModalRegistroCliente == 'function') {
            callbackAbrirModalRegistroCliente()
          }
        }
      }
    }
    if (typeof callback == 'function') {
      callback()
    }
  }

  async afterSearchWithRenovation(persona_doc_id:string, persona_doc_id_ext:string, isBankClient:any, callback:any = null) {
    this.numero_solicitudes = this.asegurados.length;
    let [idInstanciaFinVigencia, lastFinVigenciaSolicitud,] = this.getLasSolicitudVigente();
    this.asegurado = lastFinVigenciaSolicitud;
    this.isRenovated = lastFinVigenciaSolicitud.instancia_poliza.id == lastFinVigenciaSolicitud.instancia_poliza.id_instancia_renovada ? false : true;
    this.instaciaRenovadaFromInstanciaId = lastFinVigenciaSolicitud.instancia_poliza.id == lastFinVigenciaSolicitud.instancia_poliza.id_instancia_renovada ? null : lastFinVigenciaSolicitud.instancia_poliza.id_instancia_renovada;
    let lastFinVigenciaSolicitudCertificado = lastFinVigenciaSolicitud.instancia_poliza.instancia_documentos.find((param) => param.id_documento + "" == this.documentoCertificado.id + "");
    if (lastFinVigenciaSolicitudCertificado) {
      let lastFinVigenciaSolicitudCertificadoDateEnd = typeof lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia == "string" ? moment(new Date(lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia)).format("DD/MM/YYYY") : moment(lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia).format("DD/MM/YYYY");
      lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia = typeof lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia == "string" ? new Date(lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia) : lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia;
      let momMaxDate = moment(lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia).add(this.poliza.dias_espera_renovacion, "days");
      let momMinDate = moment(lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia).subtract(this.poliza.dias_anticipado_renovacion, "days");
      this.msgAlertRenew = "<br>";
      let dateMin = momMinDate.toDate();
      let dateMax = momMaxDate.toDate();
      let dateToday = new Date();
      let momLastFinVigenciaSolicitudCertificadoLessDaysWait = moment(lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia).subtract(this.poliza.dias_espera_renovacion, "days");
      let dateLastFinVigenciaSolicitudCertificadoLessDaysWait = momLastFinVigenciaSolicitudCertificadoLessDaysWait.toDate();
      let momLastFinVigenciaSolicitudCertificadoPlussDaysWait = moment(lastFinVigenciaSolicitudCertificado.fecha_fin_vigencia).add(this.poliza.dias_anticipado_renovacion, "days");
      let dateLastFinVigenciaSolicitudCertificadoPlussDaysWait = momLastFinVigenciaSolicitudCertificadoPlussDaysWait.toDate();
      this.msgRenew = `Se encontró, para el cliente ${lastFinVigenciaSolicitud.entidad.persona.persona_primer_apellido} ${lastFinVigenciaSolicitud.entidad.persona.persona_segundo_apellido} ${lastFinVigenciaSolicitud.entidad.persona.persona_primer_nombre},
                                el certificado Nro.: ${lastFinVigenciaSolicitudCertificado.nro_documento},
                                con fecha de fin de vigencia: ${lastFinVigenciaSolicitudCertificadoDateEnd}.<br>
                                La renovación de este certificado puede ser instrumentada hasta el ${momMaxDate.format("DD/MM/YYYY")}.<br><br>
                                Usted puede RENOVAR el Certificado o VER el detalle de los certificados que fueron encontrados.`;
      if (dateToday.getTime() < dateLastFinVigenciaSolicitudCertificadoLessDaysWait.getTime()) {
        this.msgAlertRenew += `
                                Se encontró para el cliente: ${lastFinVigenciaSolicitud.entidad.persona.persona_primer_apellido} ${lastFinVigenciaSolicitud.entidad.persona.persona_segundo_apellido} ${lastFinVigenciaSolicitud.entidad.persona.persona_primer_nombre}
                                , el Certificado Nro.: ${lastFinVigenciaSolicitudCertificado.nro_documento},
                                cuya vigencia concluirá el ${lastFinVigenciaSolicitudCertificadoDateEnd},
                                por lo que aún no es posible renovar el mismo o generar un nuevo certificado.<br><br>`;
        this.msgAlertRenew += `
                                La renovación de este certificado será posible realizarla a partir del ${momMinDate.format(
            "DD/MM/YYYY"
        )}
                                hasta el ${momMaxDate.format("DD/MM/YYYY")}
                                (${this.poliza.dias_anticipado_renovacion} dias antes ó ${this.poliza.dias_espera_renovacion}
                                dias despues de la fecha del fin de la vigencia del certificado). Si desea puede VER los certificados que fueron encontrados`;
        this.lblButtonShowRequests = "Ver Certificados encontrados";
        this.enableBtnRenuevaSolicitud = false;
        this.enableBtnNuevaSolicitud = false;
      } else if (dateToday.getTime() > dateLastFinVigenciaSolicitudCertificadoPlussDaysWait.getTime()) {
        this.msgAlertRenew += `
                                Se encontró para el cliente: ${lastFinVigenciaSolicitud.entidad.persona.persona_primer_apellido} ${lastFinVigenciaSolicitud.entidad.persona.persona_segundo_apellido} ${lastFinVigenciaSolicitud.entidad.persona.persona_primer_nombre},
                                el Certificado Nro.: ${lastFinVigenciaSolicitudCertificado.nro_documento},
                                cuya vigencia concluyó el ${lastFinVigenciaSolicitudCertificadoDateEnd},
                                por lo que ya no es posible realizar la renovación del mismo.<br><br>`;
        this.msgAlertRenew += `
                                La renovación de este certificado podía ser realizada desde el ${momMinDate.format(
            "DD/MM/YYYY"
        )} hasta el ${momMaxDate.format("DD/MM/YYYY")}.
                                Si desea puede CREAR una nueva solicitud o VER los certificados que fueron encontrados`;
        this.lblButtonShowRequests = "Ver Certificados encontrados";
        this.enableBtnNuevaSolicitud = true;
        this.enableBtnRenuevaSolicitud = false;
      } else {
        this.msgAlertRenew = this.msgRenew;
        this.enableBtnNuevaSolicitud = false;
        this.enableBtnRenuevaSolicitud = true;
        this.lblButtonShowRequests = "Ver Solicitudes encontradas";
      }

      this.asegurado = lastFinVigenciaSolicitud;
      await this.setAtributosToPersonaBanco();
      await this.setDatesOfAsegurado();
      if ([this.estadoEmitido.id + "", this.estadoSinVigencia.id + "",].includes(lastFinVigenciaSolicitud.instancia_poliza.id_estado + "")) {
        let solicitudesEmitidas = this.asegurados.filter((param) => param.instancia_poliza.id_estado + "" == this.estadoEmitido.id + "" && param.instancia_poliza.id == idInstanciaFinVigencia);
        if (solicitudesEmitidas) {
          for (let i = 0; i < solicitudesEmitidas.length; i++) {
            let solicitudEmitida = solicitudesEmitidas[i];
            let instanciaDocumentoCertificado = solicitudEmitida.instancia_poliza.instancia_documentos.find((param) => param.id_documento + "" == this.documentoCertificado.id + "");
            if (typeof instanciaDocumentoCertificado.fecha_fin_vigencia == "string") {
              instanciaDocumentoCertificado.fecha_fin_vigencia = new Date(instanciaDocumentoCertificado.fecha_fin_vigencia);
            }
            if (instanciaDocumentoCertificado.fecha_fin_vigencia) {
              this.strFechaFinVigencia += ` (${i + 1}) ` + instanciaDocumentoCertificado.fecha_fin_vigencia.getDate().pad(2) + "/" + (instanciaDocumentoCertificado.fecha_fin_vigencia.getMonth() + 1).pad(2) + "/" + instanciaDocumentoCertificado.fecha_fin_vigencia.getFullYear();
            }
          }
        }
        let respCouldBeRenovated: any = await this.instanciaPolizaService.verifyCouldBeRenovated(this.asegurado.instancia_poliza.id_poliza, this.asegurado.instancia_poliza.id).toPromise();
        if (respCouldBeRenovated.data) {
          this.displayModalSolicitudExistenteConRenovacion = true;
        } else {
          // this.displayModalSolicitudExistenteMessage = respCouldBeRenovated.message;
          this.displayModalSolicitudExistenteAlertRenovacion = true;
        }
        if (this.asegurados.find((param) => param.instancia_poliza.id_estado == this.estadoIniciado.id ) ||
            this.asegurados.find((param) => param.instancia_poliza.id_estado == this.estadoSolicitado.id )
        ) {
          this.btnRenovarEstado = false;
        } else {
          this.btnRenovarEstado = true;
        }
      } else if (
          [this.estadoEmitido.id+''].includes(lastFinVigenciaSolicitud.instancia_poliza.id_estado+'')
      ) {
        this.displayModalSolicitudExistenteAlertRenovacion = true;
      } else {
        this.displayModalSolicitudExistente = true;
      }
    } else {
      this.displayModalSolicitudExistente = true;
    }
    let solicitudEstado = this.EstadosInstaciaPolizaId[this.asegurado.instancia_poliza.id_estado];
    if (this.numero_solicitudes > 1) {
      this.msgSolicitudExistente = `Se encontró una solicitud  en estado ${solicitudEstado}, correspondiente al producto ${this.poliza.descripcion} para el cliente: ${this.asegurado.entidad.persona.persona_primer_apellido} ${this.asegurado.entidad.persona.persona_primer_nombre}`;
    } else {
      this.msgSolicitudExistente = `Se encontró una solicitud  en estado ${solicitudEstado}, correspondiente al producto ${this.poliza.descripcion} para el cliente: ${this.asegurado.entidad.persona.persona_primer_apellido} ${this.asegurado.entidad.persona.persona_primer_nombre}`;
    }
    if (this.displayModalFormTitular) {
      this.displayModalFormTitular = false;
    }
    // this.asegurado.instancia_poliza = new Instancia_poliza();
    // this.asegurado.instancia_poliza.instancia_poliza_transicions = [];
    if (isBankClient) {
      this.displayClienteExistente = false;
    }
    this.isLoadingAgain = false;
    if (typeof callback == 'function') {
      await callback();
    }
  }

  async validarWarnsOrErrors(callback: Function = null) {
    if (this.msgs_error.length) {
      this.userFormHasErrors = await this.msgs_error.find(param => param.severity == 'error') ? true : false;
    } else {
      this.userFormHasErrors = false;
    }
    if (this.msgs_warn.length) {
      this.userFormHasWarnings = await this.msgs_warn.find(param => param.severity == 'warn') ? true : false;
    } else {
      this.userFormHasWarnings = false;
    }
    if (this.msgs_info_warn.length) {
      this.userFormHasInfoWarnings = await this.msgs_info_warn.find(param => param.severity == 'warn') ? true : false;
    } else {
      this.userFormHasInfoWarnings = false;
    }
    this.msgs_warn_error = this.msgs_error.concat(this.msgs_warn);
    if (typeof callback == 'function') {
      callback();
    }
  }

  ImprimirSolicitudEcoAguinaldo(id_instancia_poliza:any = null) {
        let nombre_archivo = "SoliEcoAgui" + this.asegurado.instancia_poliza.id;
        this.isLoadingAgain = true;
        this.reporteService.ReporteSolicitud(this.asegurado.instancia_poliza.id, nombre_archivo).subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        this.msgsFromSolicitud = response.message;
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error(
                        "ERROR llamando servicio ImprimirSolicitud:",
                        err
                    )
                }
            );
    }

    ImprimirSolicitudEcoRiesgo(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "SoliEcoRiesgo" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.ReporteSolicitudEcoRiesgo(id_instancia_poliza, nombre_archivo).subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        this.msgsFromSolicitud = response.message;
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error(
                        "ERROR llamando servicio ImprimirSolicitud:",
                        err
                    )
                }
            );
    }

    ImprimirSolicitudEcovida(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "SoliEcoVida" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.ReporteSolicitudEcoVida(
                id_instancia_poliza,
                nombre_archivo
        ).subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        this.msgsFromSolicitud = response.message;
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error(
                        "ERROR llamando servicio ImprimirSolicitud:",
                        err
                    )
                }
            );
    }

    ShowUploadedFile(nombre_archivo:string) {
      this.reporteService.cargarUpload(nombre_archivo);
      this.isLoadingAgain = false;
    }

    ImprimirSolicitudEcoAccidentes(instancia_poliza_id:number = null, printFileUploaded:boolean = false) {
      instancia_poliza_id = instancia_poliza_id ? instancia_poliza_id : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "SoliEcoAccidentes" + instancia_poliza_id;
        this.isLoadingAgain = true;
        if (printFileUploaded) {
          this.reporteService.cargarPagina(this.instanciaDocumentoSolicitud.nombre_archivo);
          this.isLoadingAgain = false;
        } else {
          this.reporteService.ReporteSolicitudEcoAccidentes(instancia_poliza_id, nombre_archivo).subscribe((res) => {
                      let response = res as {
                          status: string;
                          message: string;
                          data: Poliza[];
                      };
                      this.isLoadingAgain = false;
                      if (response.status == "ERROR") {
                          this.msgsFromSolicitud = response.message;
                          this.displayValidacionMessages = true;
                      } else {
                          this.reporteService.cargarPagina(nombre_archivo);
                      }
                  },
                  (err) => {
                      this.isLoadingAgain = false;
                      console.error("ERROR llamando servicio ImprimirSolicitud:", err)
          });
        }
    }

    ImprimirSolicitudEcoResguardo(instancia_poliza_id:number = null, printFileUploaded:boolean = false) {
      instancia_poliza_id = instancia_poliza_id ? instancia_poliza_id : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "SoliEcoResguardo" + instancia_poliza_id;
        this.isLoadingAgain = true;
        if (printFileUploaded) {
          this.reporteService.cargarPagina(this.instanciaDocumentoSolicitud.nombre_archivo);
          this.isLoadingAgain = false;
        } else {
          this.reporteService.ReporteSolicitudEcoResguardo(instancia_poliza_id, nombre_archivo).subscribe((res) => {
                      let response = res as {
                          status: string;
                          message: string;
                          data: Poliza[];
                      };
                      this.isLoadingAgain = false;
                      if (response.status == "ERROR") {
                          this.msgsFromSolicitud = response.message;
                          this.displayValidacionMessages = true;
                      } else {
                          this.reporteService.cargarPagina(nombre_archivo);
                      }
                  },
                  (err) => {
                      this.isLoadingAgain = false;
                      console.error("ERROR llamando servicio ImprimirSolicitud:", err)
          });
        }
    }

    ImprimirSolicitudEcoMedicB(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "SoliEcoMedicB" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.ReporteSolicitudEcomedic(
                id_instancia_poliza,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: persona_banco_tarjeta_credito;
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        this.msgsFromSolicitud = response.message;
                        this.service.add({
                            key: "tst",
                            severity: "error",
                            summary: "Error Al generar el PDF",
                            detail: response.message,
                        });
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error("ERROR Generar el reporte:", err);
                }
            );
    }

    ImprimirSolicitudEcoProteccion(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "SoliEcoProt" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.ReporteSolicitudEcoProteccion(id_instancia_poliza, nombre_archivo)
            .subscribe((res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        this.msgsFromSolicitud = response.message;
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error(
                        "ERROR llamando servicio ImprimirSolicitud:",
                        err
                    )
                }
            );
    }

    async ImprimirCertificado2(id_instancia_poliza:any = null,id_poliza:any = null,nombre_archivo:any = null, id_documento_version:any =null) {
      this.isLoadingAgain = true;
      let response:any = await this.reporteQueryService.executeQueryCertificado({nombre_archivo,id_poliza,id_instancia_poliza,id_documento_version,reporte_query_parametros:[{nombre:'@param1',tipo:'int', valor:id_instancia_poliza}]});
      this.isLoadingAgain = false;
      if (response.status !== "ERROR") {
        this.reporteService.cargarPagina(nombre_archivo);
      } else {
        this.service.add({
          key: "tst",
          severity: "error",
          summary: "Error Al generar el PDF",
          detail: response.message,
        });
        if (
          response.status == 400 &&
          response.message == "usuario no autentificado"
        ) {
          window.location.replace("/");
        }
      }
    }

    ImprimirCertificado(id_instancia_poliza:any = null,idPoliza:any = null, id_documento_version:any = null) {
      this.isLoadingAgain = true;
        switch (idPoliza+'') {
          case '1':
            this.ImprimirCertificadoEcoAguinaldo(id_instancia_poliza);
            break;
          case '2':
            this.ImprimirCertificadoEcoAguinaldo(id_instancia_poliza);
            break;
          case '3':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'Cert1TarCre'+id_instancia_poliza);
            break;
          case '4':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'CertTarDeb'+id_instancia_poliza);
            break;
          case '6':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'CertEcoVida'+id_instancia_poliza);
            break;
          case '7':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'CertEcoProteccion'+id_instancia_poliza);
            break;
          case '8':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'CertEcoMedicV'+id_instancia_poliza);
            break;
          case '10':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'CertEcoAccidente'+id_instancia_poliza);
            break;
          case '11':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'CertEcoRiesgo'+id_instancia_poliza);
            break;
          case '12':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'CertEcoResguardo'+id_instancia_poliza);
            break;
          case '13':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'Cert2TarCre'+id_instancia_poliza);
            break;
          case '14':
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'CertEcoEstudio'+id_instancia_poliza);
            break;
          case null:
            this.ImprimirCertificado2(id_instancia_poliza,idPoliza,'CertCrediticio'+id_instancia_poliza,id_documento_version);
            break;
        }
    }

    ImprimirPlanPago(id_instancia_poliza:any = null,idPoliza:any = null) {
      this.isLoadingAgain = true;
        switch (idPoliza+'') {
          case '7':
            this.ImprimirPlanPagoEcoProteccion(id_instancia_poliza);
            break;
          case '8':
            this.ImprimirPlanPagoEcoMedicB(id_instancia_poliza);
            break;
          case '14':
            this.ImprimirPlanPagoEcoEstudio(id_instancia_poliza);
            break;
          case '4':
            this.ImprimirPlanPagoTarjetas(id_instancia_poliza);
            break;
        }
    }

    ImprimirAnulacion(id_instancia_poliza:any = null,idPoliza:any = null) {
      this.isLoadingAgain = true;
      idPoliza = idPoliza ? idPoliza : this.poliza.id;
      switch (idPoliza + '') {
        case '10':
          this.ImprimirAnulacion2(id_instancia_poliza, idPoliza, 'AnulacionEcoAccidente' + id_instancia_poliza);
          break;
        case '12':
          this.ImprimirAnulacion2(id_instancia_poliza, idPoliza, 'AnulacionEcoResguardo' + id_instancia_poliza);
          break;
        case 'null':
          this.ImprimirAnulacion2(id_instancia_poliza, null, 'AnulacionDesgravamen' + id_instancia_poliza);
          break;
      }
    }

    ImprimirCarta(id_instancia_poliza:any = null,idPoliza:any = null) {
      this.isLoadingAgain = true;
      idPoliza = idPoliza ? idPoliza : this.id_poliza;
      switch (idPoliza +'') {
        case '10':
          this.ImprimirCarta2(id_instancia_poliza, idPoliza, 'CartaAutorizacionEcoAccidente' + id_instancia_poliza);
          break;
        case '12':
          this.ImprimirCarta2(id_instancia_poliza, idPoliza, 'CartaAutorizacionEcoResguardo' + id_instancia_poliza);
          break;
      }
    }

    ImprimirOrden(id_instancia_poliza:any = null,idPoliza:any = null) {
      this.isLoadingAgain = true;
      idPoliza = idPoliza ? idPoliza : this.poliza.id;
      switch (idPoliza +'') {
        case '1':
          this.ImprimirOrdenPago2(id_instancia_poliza,idPoliza,'OrdenPagoEcoAgui'+id_instancia_poliza);
          break;
        case '2':
          this.ImprimirOrdenPago2(id_instancia_poliza,idPoliza,'OrdenPagoEcoResguardo'+id_instancia_poliza);
          break;
        case '6':
          this.ImprimirOrdenPago2(id_instancia_poliza,idPoliza,'OrdenPagoEcoVida'+id_instancia_poliza);
          break;
        case '8':
          this.ImprimirOrdenPago2(id_instancia_poliza,idPoliza,'OrdenPagoEcoMedicV'+id_instancia_poliza);
          break;
        case '14':
          this.ImprimirOrdenPago2(id_instancia_poliza,idPoliza,'OrdenPagoEcoEstudio'+id_instancia_poliza);
          break;
        case '10':
          this.ImprimirOrdenPago2(id_instancia_poliza,idPoliza,'OrdenPagoEcoAccidentes'+id_instancia_poliza);
          break;
        case '11':
          this.ImprimirOrdenPago2(id_instancia_poliza,idPoliza,'OrdenPagoEcoRiesgo'+id_instancia_poliza);
          break;
        case '12':
          this.ImprimirOrdenPago2(id_instancia_poliza,idPoliza,'OrdenPagoEcoResguardo'+id_instancia_poliza);
          break;
      }
    }

  ImprimirSolicitud(id_instancia_poliza:any = null, idPoliza:any = null, id_documento_version: any = null, nro_solicitud: any = null) {
    this.isLoadingAgain = true;
    switch (idPoliza+'') {
      case '1':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'SolEcoAgui'+id_instancia_poliza);
        break;
      case '2':
        this.ImprimirSolicitudEcoAguinaldo(id_instancia_poliza);
        break;
      case '3':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'Sol1TarCre'+id_instancia_poliza);
        break;
      case '4':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'SolTarDeb'+id_instancia_poliza);
        break;
      case '6':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'SolEcoVida'+id_instancia_poliza);
        break;
      case '7':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'SolEcoProteccion'+id_instancia_poliza);
        break;
      case '8':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'SolEcoMedicV'+id_instancia_poliza);
        break;
      case '10':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'SolEcoAccidentes'+id_instancia_poliza,null,nro_solicitud);
        break;
      case '11':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'SolEcoRiesgo'+id_instancia_poliza);
        break;
      case '12':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'SolEcoResguardo'+id_instancia_poliza,null,nro_solicitud);
        break;
      case '13':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'Sol2TarCre'+id_instancia_poliza);
        break;
      case '14':
        this.ImprimirSolicitud2(id_instancia_poliza,idPoliza,'SolEcoEstudio'+id_instancia_poliza);
        break;
      case null:
        this.ImprimirDocumentosCrediticios(id_instancia_poliza,id_documento_version);
        break;
    }
  }

  ImprimirCertificadoEcoAguinaldo(id_instancia_poliza:any = null) {
        let nombre_archivo = "CertEcoAgui" + this.asegurado.instancia_poliza.id;
        this.isLoadingAgain = true;
        this.reporteService.certificado_ecoaguinaldo(this.asegurado.instancia_poliza.id, nombre_archivo)
            .subscribe((res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        this.msgsFromSolicitud = response.message;
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error(
                        "ERROR llamando servicio ImprimirCertificado:",
                        err
                    )
                }
            );
    }

    ImprimirCertificadoEcoRiesgo(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "CertEcoRiesgo" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService
            .certificado_ecoriesgo(
                id_instancia_poliza,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        this.msgsFromSolicitud = response.message;
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error(
                        "ERROR llamando servicio ImprimirCertificado:",
                        err
                    )
                }
            );
    }

    ImprimirCertificadoEcoProteccion(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "CertEcoProt" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService
            .ReporteCertificadoEcoProteccion(
                id_instancia_poliza,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        this.msgsFromSolicitud = response.message;
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error("ERROR llamando servicio ImprimirCertificadoEcoProteccion:", err);
                }
            );
    }

    ImprimirCertificadoEcoVida(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "CertEcoVida" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.ReporteCertificadoEcoVida(
                id_instancia_poliza,
                nombre_archivo
        ).subscribe((res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        this.msgsFromSolicitud = response.message;
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },(err) => {
                    this.isLoadingAgain = false;
                    console.error("ERROR llamando servicio ImprimirCertificadoEcoVida:",err)
                }
        );
    }

    ImprimirCertificadoEcoAccidentes(instancia_poliza_id:number =null, printFileUploaded:boolean = false) {
      instancia_poliza_id = instancia_poliza_id ? instancia_poliza_id : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "CertEcoAccidentes" + instancia_poliza_id;
        this.isLoadingAgain = true;
        if (printFileUploaded) {
          this.reporteService.cargarPagina(this.instanciaDocumentoSolicitud.nombre_archivo);
          this.isLoadingAgain = false;
        } else {
          this.reporteService.ReporteCertificadoEcoAccidentes(instancia_poliza_id, nombre_archivo).subscribe(
                  (res) => {
                      let response = res as {
                          status: string;
                          message: string;
                          data: Poliza[];
                      };
                      this.isLoadingAgain = false;
                      if (response.status == "ERROR") {
                          this.msgsFromSolicitud = response.message;
                          this.displayValidacionMessages = true;
                      } else {
                          this.reporteService.cargarPagina(nombre_archivo);
                      }
                  },
                  (err) => {
                      this.isLoadingAgain = false;
                      console.error("ERROR llamando servicio ImprimirCertificadoEcoAccidentes:", err)
                  });
        }
    }

    ImprimirCertificadoEcoResguardo(instancia_poliza_id:number =null, printFileUploaded:boolean = false) {
      instancia_poliza_id = instancia_poliza_id ? instancia_poliza_id : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "CertEcoResguardo" + instancia_poliza_id;
        this.isLoadingAgain = true;
        if (printFileUploaded) {
          this.reporteService.cargarPagina(this.instanciaDocumentoSolicitud.nombre_archivo);
          this.isLoadingAgain = false;
        } else {
          this.reporteService.ReporteCertificadoEcoResguardo(instancia_poliza_id, nombre_archivo).subscribe(
                  (res) => {
                      let response = res as {
                          status: string;
                          message: string;
                          data: Poliza[];
                      };
                      this.isLoadingAgain = false;
                      if (response.status == "ERROR") {
                          this.msgsFromSolicitud = response.message;
                          this.displayValidacionMessages = true;
                      } else {
                          this.reporteService.cargarPagina(nombre_archivo);
                      }
                  },
                  (err) => {
                      this.isLoadingAgain = false;
                      console.error("ERROR llamando servicio ImprimirCertificadoEcoResguardo:", err)
                  });
        }
    }

    ImprimirCertificadoEcoMedicB(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "CertEcoMedic" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.certificado_Ecomedic2(id_instancia_poliza, nombre_archivo).subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: persona_banco_tarjeta_credito;
                    };
                    this.isLoadingAgain = false;
                    if (response.status !== "ERROR") {
                        this.reporteService.cargarPagina(nombre_archivo);
                    } else {
                        this.service.add({
                            key: "tst",
                            severity: "error",
                            summary: "Error Al generar el PDF",
                            detail: response.message,
                        });
                    }
                },
                (err) => {
                    console.error("ERROR Generar el reporte:", err)
                    this.isLoadingAgain = false;
                }
            );
    }

    OpenEnvioDeDocumento() {
        this.displayEnviarDocumento = true;
        if (
            !this.destinatariosCorreos.includes(
                this.asegurado.entidad.persona.persona_email_personal
            )
        ) {
            this.destinatariosCorreos += this.validarEmail(
                this.asegurado.entidad.persona.persona_email_personal
            )
                ? this.asegurado.entidad.persona.persona_email_personal
                : "";
        }
    }

  ImprimirSolicitudTarjeta(id_instancia_poliza:any = null) {
    id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
    let nombre_archivo = "SoliTarjeta" + id_instancia_poliza;
    this.reporteService.ReporteSolicitudTarjeta(
      id_instancia_poliza,
        nombre_archivo
      )
      .subscribe(
        (res) => {
          let response = res as {
            status: string;
            message: string;
            data: persona_banco_tarjeta_credito;
          };
          this.isLoadingAgain = false;
          if (response.status !== "ERROR") {
            this.reporteService.cargarPagina(nombre_archivo);
          } else {
            this.service.add({
              key: "tst",
              severity: "error",
              summary: "Error Al generar el PDF",
              detail: response.message,
            });
          }
        },
        (err) => console.error("ERROR Generar el reporte:", err)
      );
  }

  ImprimirCertificadoTarjeta(id_instancia_poliza:any = null) {
    id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
    let nombre_archivo = "CertTarjeta" + id_instancia_poliza;
    this.reporteService.ReporteCertificadoTarjeta(
        id_instancia_poliza,
        nombre_archivo
      )
      .subscribe(
        (res) => {
          let response = res as {
            status: string;
            message: string;
            data: persona_banco_tarjeta_credito;
          };
          this.isLoadingAgain = false;
          if (response.status !== "ERROR") {
            this.reporteService.cargarPagina(nombre_archivo);
          } else {
            this.service.add({
              key: "tst",
              severity: "error",
              summary: "Error Al generar el PDF",
              detail: response.message,
            });
          }
        },
        (err) => console.error("ERROR Generar el reporte:", err)
      );
  }

  ImprimirOrdenEcoAguinaldo(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "OrdenPago" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.OrdenPago(id_instancia_poliza, nombre_archivo)
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud =
                                "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) =>
                    console.error("ERROR llamando servicio ImprimirOrden:", err)
            );
    }

    ImprimirOrdenTarCreV2(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "OrdenPagoTarCreV2" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.OrdenPagoTarjetaV2(id_instancia_poliza, nombre_archivo)
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud =
                                "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) =>
                    console.error("ERROR llamando servicio ImprimirOrden:", err)
            );
    }

    ImprimirOrdenEcoVida(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "OrdenPagoEcoVida" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.OrdenPagoEcoVida(
                id_instancia_poliza,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud =
                                "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    console.error("ERROR llamando servicio ImprimirOrden:", err);
                    this.isLoadingAgain = false;
                }
            );
    }

    ImprimirOrdenEcoAccidentes(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "OrdenPagoEcoAccidentes" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.OrdenPagoEcoAccidentes(
                id_instancia_poliza,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud =
                                "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    console.error("ERROR llamando servicio ImprimirOrden:", err);
                    this.isLoadingAgain = false;
                }
            );
    }

    ImprimirOrdenEcoResguardo(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "OrdenPagoEcoResguardo" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.OrdenPagoEcoResguardo(
                id_instancia_poliza,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud = "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    console.error("ERROR llamando servicio ImprimirOrden:", err);
                    this.isLoadingAgain = false;
                }
            );
    }

    ImprimirOrdenEcoRiesgo(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "OrdenPagoEcoRiesgo" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.OrdenPagoEcoRiesgo(id_instancia_poliza, nombre_archivo)
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud = "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error("ERROR llamando servicio ImprimirOrden:", err);
                }
            );
    }

    ImprimirOrdenEcoMedicB(id_instancia_poliza = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "OrdenPagoEcoMedicV" + id_instancia_poliza;
        this.isLoadingAgain = true;
        this.reporteService.OrdenPagoEcoMedic(id_instancia_poliza, nombre_archivo).subscribe((res) => {
                this.isLoadingAgain = false;
                this.reporteService.cargarPagina(nombre_archivo);
            },
                (err) => {
                    this.isLoadingAgain = false;
                    console.error(
                        "ERROR llamando servicio elminacion de beneficiario:",
                        err
                    )
                }
            );
    }

    ImprimirPlanPagoEcoMedicB(id_instancia_poliza:any = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "PlanPagoEcoMedicB" + id_instancia_poliza;
        this.isLoadingAgain = true;
        let instanciaCertificado: Instancia_documento = this.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.documentoCertificado.id);
        this.reporteService.ReportePlanPago(
                instanciaCertificado.nro_documento,
                id_instancia_poliza,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud =
                                "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) =>
        {
            this.isLoadingAgain = false;
        console.error("ERROR llamando servicio ImprimirOrden:", err)
        }
            );
    }

    ImprimirPlanPagoEcoEstudio(id_instancia_poliza:any = null) {
        id_instancia_poliza = id_instancia_poliza ? id_instancia_poliza : this.asegurado.instancia_poliza.id;
        let nombre_archivo = "PlanPagoEcoEstudio" + id_instancia_poliza;
        this.isLoadingAgain = true;
        let instanciaCertificado: Instancia_documento = this.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.documentoCertificado.id);
        this.reporteService.ReportePlanPago(
                instanciaCertificado.nro_documento,
                id_instancia_poliza,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud =
                                "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) =>
        {
            this.isLoadingAgain = false;
        console.error("ERROR llamando servicio ImprimirOrden:", err)
        }
            );
    }

  ImprimirSolicitudEcoMedic(id: any) {
    let nombre_archivo = 'SoliEcoMedic' + id;
    this.isLoadingAgain = true;
    this.reporteService.ReporteSolicitudEcomedic(id, nombre_archivo).subscribe(res => {
      this.isLoadingAgain = false;
      let response = res as { status: string, message: string, data: Poliza[] };
      if (response.status == 'ERROR') {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No pudo imprimir el documento, verifique los datos de la solicitud' });
      } else {
        this.reporteService.cargarPagina(nombre_archivo);
      }
    }, err => {
      this.isLoadingAgain = false;
      console.error("ERROR llamando servicio ImprimirSolicitud:", err);
    });
  }

  ImprimirCertificadoEcoMedic(id: any) {
    let nombre_archivo = 'CertEcoMedic' + id;
    this.isLoadingAgain = true;
    this.reporteService.certificado_Ecomedic2(id, nombre_archivo).subscribe(res => {
      let response = res as { status: string, message: string, data: Poliza[] };
      this.isLoadingAgain = false;
      if (response.status == 'ERROR') {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No pudo imprimir el documento, verifique los datos de la solicitud' });
      } else {
        this.reporteService.cargarPagina(nombre_archivo);
      }
    }, err => {
      this.isLoadingAgain = false;
      console.error("ERROR llamando servicio ImprimirCertificadoEcoVida:", err);
    });
  }

    ImprimirPlanPagoEcoProteccion(id_instancia_poliza:any = null) {
        let nombre_archivo = "PlanPagoEcoProteccion" + this.asegurado.instancia_poliza.id;
        this.isLoadingAgain = true;
        let instanciaCertificado: Instancia_documento = this.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.documentoCertificado.id);
        this.reporteService.ReportePlanPago(
                instanciaCertificado.nro_documento,
                this.asegurado.instancia_poliza.id,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud =
                                "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) =>
                {
                    this.isLoadingAgain = false;
                    console.error("ERROR llamando servicio ImprimirOrden:", err)
                });
    }

    ImprimirPlanPagoTarjetas(id_instancia_poliza:any = null) {
        let nombre_archivo = "PlanPagoTarjeta" + this.asegurado.instancia_poliza.id;
        this.isLoadingAgain = true;
        let instanciaCertificado: Instancia_documento = this.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.documentoCertificado.id);
        this.reporteService.ReportePlanPago(
                instanciaCertificado.nro_documento,
                this.asegurado.instancia_poliza.id,
                nombre_archivo
            )
            .subscribe(
                (res) => {
                    let response = res as {
                        status: string;
                        message: string;
                        data: Poliza[];
                    };
                    this.isLoadingAgain = false;
                    if (response.status == "ERROR") {
                        if (typeof response.message == "object") {
                            this.msgsFromSolicitud =
                                "Hubo un error, el comprobante de pago no pudo generarse debido a que falta el certificado de cobertura";
                        } else {
                            this.msgsFromSolicitud = response.message;
                        }
                        this.displayValidacionMessages = true;
                    } else {
                        this.reporteService.cargarPagina(nombre_archivo);
                    }
                },
                (err) =>
                {
                    this.isLoadingAgain = false;
                    console.error("ERROR llamando servicio ImprimirOrden:", err)
                });
    }
    async ImprimirSolicitud2(id_instancia_poliza:any = null,id_poliza:any = null,nombre_archivo:any = null,id_documento_version:any = null,nro_solicitud:string = null) {
        this.isLoadingAgain = true;
        let response:any = await this.reporteQueryService.executeQuerySolicitud({nombre_archivo,id_poliza,id_instancia_poliza,id_documento_version,reporte_query_parametros:[{nombre:'@param1',tipo:'int', valor:id_instancia_poliza},{nombre:'@param2',tipo:'string', valor:nro_solicitud}]});
        this.isLoadingAgain = false;
        if (response.status !== "ERROR") {
            this.reporteService.cargarPagina(nombre_archivo);
        } else {
            this.service.add({
                key: "tst",
                severity: "error",
                summary: "Error Al generar el PDF",
                detail: response.message,
            });
            if (
                response.status == 400 &&
                response.message == "usuario no autentificado"
            ) {
                window.location.replace("/");
            }
        }
    }

    async ImprimirDocumentosCrediticios(id_deudor:any = null,id_documento_version:any = null) {
        this.isLoadingAgain = true;
        //let nombre_archivo = 'crediticios'+id_deudor;
        let response:any = await this.reporteQueryService.executeQueryCrediticios({id_deudor,id_documento_version,reporte_query_parametros:[{nombre:'@param1',tipo:'int', valor:id_deudor}]});
        this.isLoadingAgain = false;
        if (response.status !== "ERROR") {
            this.reporteService.cargarPagina(response.data.base);
        } else {
            this.service.add({
                key: "tst",
                severity: "error",
                summary: "Error Al generar el PDF",
                detail: response.message,
            });
            if (
                response.status == 400 &&
                response.message == "usuario no autentificado"
            ) {
                window.location.replace("/");
            }
        }
    }
    async ImprimirOrdenPago2(id_instancia_poliza:any = null,id_poliza:any = null,nombre_archivo:any = null) {
        this.isLoadingAgain = true;
        let response:any = await this.reporteQueryService.executeQueryOrdenPago({nombre_archivo,id_poliza,id_instancia_poliza,reporte_query_parametros:[{nombre:'@param1',tipo:'int', valor:id_instancia_poliza}]});
        this.isLoadingAgain = false;
        if (response.status !== "ERROR") {
            this.reporteService.cargarPagina(nombre_archivo);
        } else {
            this.service.add({
                key: "tst",
                severity: "error",
                summary: "Error Al generar el PDF",
                detail: response.message,
            });
            if (
                response.status == 400 &&
                response.message == "usuario no autentificado"
            ) {
                window.location.replace("/");
            }
        }
    }

    async ImprimirCarta2(id_instancia_poliza:any = null,id_poliza:any = null,nombre_archivo:any = null) {
        this.isLoadingAgain = true;
        let response:any = await this.reporteQueryService.executeQueryCarta({nombre_archivo,id_poliza,id_instancia_poliza,reporte_query_parametros:[{nombre:'@param1',tipo:'int', valor:id_instancia_poliza}]});
        this.isLoadingAgain = false;
        if (response.status !== "ERROR") {
            this.reporteService.cargarPagina(nombre_archivo);
        } else {
            this.service.add({
                key: "tst",
                severity: "error",
                summary: "Error Al generar el PDF",
                detail: response.message,
            });
            if (
                response.status == 400 &&
                response.message == "usuario no autentificado"
            ) {
                window.location.replace("/");
            }
        }
    }

    async ImprimirAnulacion2(id_instancia_poliza:any = null,id_poliza:any = null,nombre_archivo:any = null) {
        this.isLoadingAgain = true;
        let response:any = await this.reporteQueryService.executeQueryAnulacion({nombre_archivo,id_poliza,id_instancia_poliza,reporte_query_parametros:[{nombre:'@param1',tipo:'int', valor:id_instancia_poliza}]});
        this.isLoadingAgain = false;
        if (response.status !== "ERROR") {
            this.reporteService.cargarPagina(nombre_archivo);
        } else {
            this.service.add({
                key: "tst",
                severity: "error",
                summary: "Error Al generar el PDF",
                detail: response.message,
            });
            if (
                response.status == 400 &&
                response.message == "usuario no autentificado"
            ) {
                window.location.replace("/");
            }
        }
    }

    validarEmail(valor: string) {
        valor = valor.trim();
        let emailRegex =
            /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/i;
        if (emailRegex.test(valor)) {
            return true;
        } else {
            return false;
        }
    }
    intVal(term) {
        return parseInt(term);
    }

    editClienteBanco() {
      this.editPrimerApellido = false;
      this.editPaterno = false;
      this.editSegundoApellido= false;
      this.editMaterno= false;
      this.editPrimerNombre = false;
      this.editSegundoNombre = false;
      this.editApCasada = false;
      this.editFechaNac = false;
      this.editCiudadNacimiento = false;
      this.editPaisNacimiento = false;
      this.editDireccionDomicilio = false;
      this.editDireccionLaboral = false;
      this.editZona = false;
      this.editNroDireccion = false;
      this.editTelefonoDomicilio = false;
      this.editTelefonoCelular = false;
      this.editTelefonoTrabajo = false;
      this.editOcupacion = false;
      this.editEmail = false;
      this.editDescOcupacion = false;
    }

    editNoClienteBanco() {
      this.editPrimerApellido = true;
      this.editPaterno = true;
      this.editSegundoApellido= true;
      this.editMaterno= true;
      this.editPrimerNombre = true;
      this.editSegundoNombre = true;
      this.editApCasada = true;
      this.editFechaNac = true;
      this.editCiudadNacimiento = true;
      this.editPaisNacimiento = true;
      this.editDireccionDomicilio = true;
      this.editDireccionLaboral = true;
      this.editZona = true;
      this.editNroDireccion = true;
      this.editTelefonoDomicilio = true;
      this.editTelefonoCelular = true;
      this.editTelefonoTrabajo = true;
      this.editOcupacion = true;
      this.editEmail = true;
      this.editDescOcupacion = true;
    }
    async validaUnicoNroTransaccion(idPoliza: number, nroTransaccion: string) {
        let respInstanciaPoliza: any = await this.instanciaPolizaService
            .findPolizaNrosTransaccion(idPoliza, nroTransaccion)
            .toPromise();
        if (
            respInstanciaPoliza &&
            respInstanciaPoliza.data &&
            respInstanciaPoliza.data.length
        ) {
            return false;
        }
        return true;
    }

    async featureOnErrRelogin(err:any) {
      let userInfo:any = this.sessionStorageService.getItemSync('userInfo');
      if (err.error.statusCode == 400 && err.error.message == "usuario no autentificado") {
        if (userInfo && Object.keys(userInfo).length) {
          if (userInfo.userName && userInfo.password) {
            this.loginService.login(userInfo.userName, userInfo.password).subscribe(async res => {
              let user = res['user'];
              this.loginService.setUserInfo(user);
              let roles = user.rol;
              // this.router.navigate(['/AppMain']);
              if (roles.find(param => param.id == 8)) {
                window.location.replace("/administracion");
              } else if (roles.find(param => param.id == 5)) {
                window.location.replace("/crediticios");
              } else {
                window.location.replace("/masivos");
              }
              this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Order submitted' });
            }, err => {
              if (err.error.message) {
                this.messageService.clear();
                this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: err.error.message.message, detail: '' });
              } else {
                this.messageService.clear();
                this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: err.message, detail: '' });
              }
            });
          } else {
            this.messageService.clear();
            this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Usuario no autenticado', detail: 'Por favor vuelva a iniciar sesion' });
          }
        } else {
          window.location.replace("/login");
        }
      } else {
        console.log(err);
      }
    }


}
