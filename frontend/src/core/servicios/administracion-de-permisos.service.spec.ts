import { TestBed } from '@angular/core/testing';

import { AdministracionDePermisosService } from './administracion-de-permisos.service';

describe('AdministracionDePermisosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdministracionDePermisosService = TestBed.get(AdministracionDePermisosService);
    expect(service).toBeTruthy();
  });
});
