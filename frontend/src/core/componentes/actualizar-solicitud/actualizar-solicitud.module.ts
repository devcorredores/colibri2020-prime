import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ActualizarSolicitudComponent} from "./actualizar-solicitud.component";
import {ButtonModule, CalendarModule, DialogModule, FieldsetModule, MessagesModule} from "primeng";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    ActualizarSolicitudComponent
  ],
  imports: [
    CommonModule,

    DialogModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    ButtonModule,
    FieldsetModule,
    MessagesModule
  ],
  exports: [
    ActualizarSolicitudComponent,

    DialogModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    MessagesModule
  ]
})
export class ActualizarSolicitudModule { }
