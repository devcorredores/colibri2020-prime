import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import {BeneficiarioComponent} from "../beneficiario/beneficiario.component";
import {Asegurado} from "../../modelos/asegurado";
import {InstanciaPolizaService} from "../../servicios/instancia-poliza.service";
import {SolicitudService} from "../../servicios/solicitud.service";
import {PolizaService} from "../../servicios/poliza.service";
import {SoapuiService} from "../../servicios/soapui.service";
import {PersonaService} from "../../servicios/persona.service";
import {MenuService} from "../../servicios/menu.service";
import {UsuariosService} from "../../servicios/usuarios.service";
import {
    persona_banco_certificado,
    persona_banco_transaccion,
    persona_banco_transaccion_debito_csg
} from "../../modelos/persona_banco_transaccion";
import {Poliza} from "../../modelos/poliza";
import {Instancia_documento} from "../../modelos/instancia_documento";
import {Instancia_poliza_transicion} from "../../modelos/instancia_poliza_transicion";
import {Atributo_instancia_poliza} from "../../modelos/atributo_instancia_poliza";
import * as moment from "moment";
import {parse} from "@angular/compiler/src/render3/view/style_parser";
import {environment} from "../../../environments/environment";

@Component({
    selector: "app-actualizar-solicitud",
    templateUrl: "./actualizar-solicitud.component.html",
    styleUrls: ["./actualizar-solicitud.component.css"],
})
export class ActualizarSolicitudComponent implements OnInit {
    @ViewChild('componenteBeneficiario', { static: false })
    private componenteBeneficiario: BeneficiarioComponent;
    @Output() setAsegurado = new EventEmitter<Asegurado>();
    color = "primary";
    mode = "indeterminate";
    value = 1;
    fechaEmisionForm: FormGroup;
    showBeneficiarios: boolean = false;
    msgFechaEmision: String = "";
    msgTransaccionInvalida: String = "";
    continuarSinValidarNroTransaccion: boolean = false;
    yesterday: Date = new Date();

    constructor(
        private instanciaPolizaService: InstanciaPolizaService,
        public solicitudService: SolicitudService,
        private polizaService: PolizaService,
        private soapuiService: SoapuiService,
        private personaService: PersonaService,
        private menuService: MenuService,
        private usuariosService: UsuariosService,
        private fb: FormBuilder
    ) {
        if (!solicitudService.isInAltaSolicitud) {
            this.solicitudService.isLoadingAgain = false;
        }
        this.setFormEmitir();
        this.msgFechaEmision =
            "La fecha de emisión del certificado de seguro sera la siguiente:";
    }

    ngOnInit() {
        if (!this.solicitudService.isInAltaSolicitud) {
            this.solicitudService.isLoadingAgain = false;
        }
        this.solicitudService.persona_banco_transaccion = new persona_banco_transaccion();
        this.setFormEmitir();
    }
    setFormEmitir() {
        this.fechaEmisionForm = this.fb.group({
            fecha_registro: new FormControl("", Validators.required),
            nro_transaccion: new FormControl("", Validators.required),
        });
    }
    async cambiarSolicitudEstado() {
        this.solicitudService.persona_banco_transaccion.nrotran = '';
        this.solicitudService.persona_banco_transaccion.detalle = '';
        this.solicitudService.persona_banco_transaccion.importe = '';
        this.solicitudService.persona_banco_transaccion.moneda = '';
        this.solicitudService.persona_banco_transaccion.fechatran = '';
        this.solicitudService.setAtributosToPersonaBanco();
        return new Promise(async (resolve) => {
            this.setFormEmitir();
            this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
            this.solicitudService.tran_info = [];
            this.solicitudService.persona_banco_transaccion = new persona_banco_transaccion();
            if (Object.keys(this.solicitudService.asegurado).length) {
                if (this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.length) {
                    this.solicitudService.msgs_warn = [];
                    this.solicitudService.msgs_error = [];
                    this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
                    this.solicitudService.isLoadingAgain = true;
                    let idObjeto = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas[0].objeto_x_atributo.id_objeto;
                    let idPoliza = this.solicitudService.asegurado.instancia_poliza.id_poliza;
                    // await this.solicitudService.setBeneficiarios();
                    // this.componenteBeneficiario.ngOnInit();
                    await this.solicitudService.setDatesOfAsegurado();
                    await this.solicitudService.setUserLogin();
                    // await this.solicitudService.validarBeneficiarios(this.componenteBeneficiario);
                    await this.polizaService.getPolizaById(idPoliza).subscribe(async (res) => {
                            let response = res as {
                                status: string;
                                message: string;
                                data: Poliza;
                            };
                            this.solicitudService.poliza = response.data as Poliza;
                            await this.solicitudService.getAllParametrosByIdDiccionario(
                                [idObjeto],
                                this.solicitudService.asegurado.instancia_poliza.id_poliza, [11, 17, 21, 30, 31, 32, 22], async () => {
                                    let atributosToUpdate = [64, 65, 66, 67, 69];
                                    await this.solicitudService.setAtributosToPersonaBanco();
                                    await this.solicitudService.setFromObjetoAtributoToAtributoInstanciaPoliza(null, [], atributosToUpdate);
                                    this.solicitudService.displayValidacionSinObservaciones = false;
                                    if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                                        this.solicitudService.instanciaDocumentoComprobante = new Instancia_documento();
                                        this.solicitudService.instanciaDocumentoComprobante.documento = this.solicitudService.documentoComprobante;
                                        if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                                            this.solicitudService.instanciaDocumentoComprobante.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
                                        }
                                        if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.solicitudService.instanciaDocumentoComprobante.id_documento)) {
                                            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoComprobante);
                                        }
                                        this.solicitudService.emitirInstanciaPoliza = false;
                                    } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                                        await this.solicitudService.setDocumentoCertificado();
                                    } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                                        await this.solicitudService.setDocumentoCertificado();
                                    } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                                        await this.solicitudService.setDocumentoCertificado();
                                    } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
                                        this.solicitudService.emitirInstanciaPoliza = true;
                                    }
                                    if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoEfectivo.id + "") {
                                        if (this.solicitudService.hasRolCajero) {
                                            this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
                                        } else {
                                            if ([10,12].includes(parseInt(this.solicitudService.id_poliza+'')) && this.solicitudService.hasRolPlataforma) {
                                                this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
                                            }
                                            // this.solicitudService.msgErrorValidacionDesdeCaja = 'Esta intentando emitir el certificado de cobertura desde caja, para ello debe tener el rol cajero, favor contactarse con el area de Sistemas';
                                            // this.solicitudService.displayErrorValidacionDesdeCaja = true;
                                        }
                                    } else if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoDebitAutomatico.id + "") {
                                        if ([10,12].includes(parseInt(this.solicitudService.id_poliza+'')) && this.solicitudService.hasRolPlataforma && this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id && this.solicitudService.hasRolPlataforma && this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9') {
                                            this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
                                        } else {
                                            this.solicitudService.displayEmitirSolicitud = true;
                                        }
                                    } else {
                                        // this.solicitudService.msgErrorValidacionDesdeCaja = 'Hubo un error en la validacion, porfavor verifica los datos para continuar, caso contrario favor contactarse con el area de Sistemas';
                                        // this.solicitudService.displayErrorValidacionDesdeCaja = true;
                                    }
                                    //this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
                                    this.solicitudService.isLoadingAgain = false;
                                    resolve(true);
                                }
                            );
                        });
                } else {
                    this.solicitudService.displayEmitirSolicitudDesdeCajaError = true;
                    resolve(false);
                }
            } else {
                this.solicitudService.isLoadingAgain = false;
            }
        });
    }

    async updateInstanciaPoliza() {
        return new Promise(async (resolve) => {
            this.solicitudService.poliza.anexo_poliza = this.solicitudService.poliza.anexo_polizas.find(param => param.id_tipo == 288);
            await this.solicitudService.setUserLogin();
            if (this.solicitudService.asegurado) {
                this.solicitudService.isLoadingAgain = true;
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoDebitAutomatico.id + "") {
                    await this.justUpdateInstanciaPoliza();
                } else {
                    this.solicitudService.hasUniqueNroTran = await this.solicitudService.validaUnicoNroTransaccion(this.solicitudService.asegurado.instancia_poliza.id_poliza, this.solicitudService.persona_banco_transaccion.nrotran);
                    if (this.solicitudService.hasUniqueNroTran && this.solicitudService.poliza && this.solicitudService.poliza.anexo_poliza) {
                        if ([10,12].includes(parseInt(this.solicitudService.poliza.id+''))) {
                            await  this.executeValidaTran(resolve, async () => {
                                await  this.executeValidaTranDebitoCsg(resolve)
                            });
                        } else {
                            await this.executeValidaTran(resolve);
                        }
                    } else {
                        this.solicitudService.msgTransaccionInvalida = "El número de transaccion introducido fue encontrado en una anterior solicitud, por favor verifica el número de transaccion: " + this.solicitudService.persona_banco_transaccion.nrotran + " contra el sistema del banco";
                        this.solicitudService.displayTransaccionInvalidaSinContinuar = true;
                        this.solicitudService.isLoadingAgain = false;
                        resolve(false);
                    }
                }
            } else {
                // this.solicitudService.msgErrorValidacionDesdeCaja = 'Hubo un error en la validacion, porfavor verifica los datos para continuar, caso contrario favor contactarse con el area de Sistemas';
                // this.solicitudService.displayErrorValidacionDesdeCaja = true;
            }
        });
    }

    async executeValidaTran(resolve:any = null, validaTranDebitoCsg:any = null) {
        try {
            this.soapuiService.validaTran(this.solicitudService.persona_banco_transaccion.nrotran).subscribe(async (res) => {
                const response = res as { status: string; message: string; data: persona_banco_transaccion; };
                if (response.data && Object.keys(response.data).length) {
                    this.solicitudService.persona_banco_transaccion = response.data;
                    this.solicitudService.atributoFechaTransaccion.valor = this.solicitudService.persona_banco_transaccion.fechatran;
                    this.solicitudService.atributoTransaccionImporte.valor = this.solicitudService.persona_banco_transaccion.importe;
                    this.solicitudService.atributoTransaccionDetalle.valor = this.solicitudService.persona_banco_transaccion.detalle;
                    this.solicitudService.atributoNroTransaccion.valor = this.solicitudService.persona_banco_transaccion.nrotran;
                    this.solicitudService.atributoTransaccionMoneda.valor = this.solicitudService.persona_banco_transaccion.moneda;

                    const fechaTran = new Date(this.solicitudService.persona_banco_transaccion.fechatran);
                    this.solicitudService.persona_banco_transaccion.fechatran = fechaTran.getDate() + "/" + (fechaTran.getMonth() + 1) + "/" + fechaTran.getFullYear();

                    if ([10,12].includes(parseInt(this.solicitudService.poliza.id+''))) {
                        if (parseInt(this.solicitudService.persona_banco_transaccion.codigo_via) == parseInt(this.solicitudService.poliza.anexo_poliza.codigo)) {
                            if (parseInt(this.solicitudService.persona_banco_transaccion.importe) == parseInt(this.solicitudService.atributoSolicitudPrimaTotal.valor)) {
                                const atributosToUpdate = [64, 65, 66, 67, 69, 98];
                                await this.solicitudService.setFromObjetoAtributoToAtributoInstanciaPoliza(null, atributosToUpdate);
                                this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.filter((param) => atributosToUpdate.includes(parseInt(param.objeto_x_atributo.id_atributo + "")) && param.valor != "");
                                await this.justUpdateInstanciaPoliza();
                            } else {
                                this.solicitudService.msgMontoInvalido = 'El importe pagado ' + this.solicitudService.persona_banco_transaccion.importe + " bs. no coincide con el costo del seguro (" + this.solicitudService.atributoSolicitudPrimaTotal.valor + '.00 bs.), por favor verifica el monto cobrado al cliente.';
                                this.solicitudService.displayMontoInvalido = true;
                                this.solicitudService.isLoadingAgain = false;
                                resolve(false);
                            }
                        } else {
                            this.solicitudService.msgMontoInvalido = `El Nro. de transaccion introducido corresponde a otro seguro por lo que no es posible habilitar la emision para el seguro ${this.solicitudService.poliza.descripcion}, favor revisa el nro de transaccion introducido.`;
                            this.solicitudService.displayMontoInvalido = true;
                            this.solicitudService.isLoadingAgain = false;
                            resolve(false);
                        }
                    } else {
                        if (parseInt(this.solicitudService.persona_banco_transaccion.importe) == this.solicitudService.poliza.anexo_poliza.monto_prima) {
                            const atributosToUpdate = [64, 65, 66, 67, 69, 98];
                            await this.solicitudService.setFromObjetoAtributoToAtributoInstanciaPoliza(null, atributosToUpdate);
                            this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.filter((param) => atributosToUpdate.includes(parseInt(param.objeto_x_atributo.id_atributo + "")) && param.valor != "");
                            await this.justUpdateInstanciaPoliza();
                        } else {
                            this.solicitudService.msgMontoInvalido = 'El importe pagado ' + this.solicitudService.persona_banco_transaccion.importe + ' bs. no coincide con el costo del seguro (' + this.solicitudService.poliza.anexo_poliza.monto_prima + '.00 bs.), por favor verifica el monto cobrado al cliente.';
                            this.solicitudService.displayMontoInvalido = true;
                            this.solicitudService.isLoadingAgain = false;
                            resolve(false);
                        }
                    }
                } else {
                    this.solicitudService.msgTransaccionInvalida = 'No fue posible validar la transaccion Nro: ' + this.solicitudService.persona_banco_transaccion.nrotran + ' contra el sistema del banco';
                    this.solicitudService.isLoadingAgain = false;
                    if ([10,12].includes(parseInt(this.solicitudService.poliza.id+''))) {
                        if (typeof validaTranDebitoCsg == 'function') {
                            await validaTranDebitoCsg()
                        }
                    } else {
                        this.solicitudService.displayTransaccionInvalidaSinContinuar = true;
                        resolve(false);
                    }
                }},() => {
                this.solicitudService.nombreServicioBanco = 'validaTran';
                this.solicitudService.displayErrorRespuestaBanco = true;
            });
        } catch (e) {
            console.log(e);
        }
    }

    async executeValidaTranDebitoCsg(resolve:any) {
        try {
            const attNroSolicitudCredito = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == 78);
            const attMontoCredito = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == 85);
            const attMontoMonedaCredito = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == 83);
            this.solicitudService.persona_banco_transaccion_debito_csg.nrotran = this.solicitudService.persona_banco_transaccion.nrotran;
            this.solicitudService.persona_banco_transaccion_debito_csg.nroSolicitud = attNroSolicitudCredito.valor +'';
            this.solicitudService.persona_banco_transaccion_debito_csg.importe = attMontoCredito.valor;
            this.solicitudService.persona_banco_transaccion_debito_csg.moneda = this.solicitudService.MonedasDescripcionParametroCod[attMontoMonedaCredito.valor.trim()];
            this.soapuiService.validaTranDebitoCsg(
                this.solicitudService.persona_banco_transaccion_debito_csg.nrotran,
                this.solicitudService.persona_banco_transaccion_debito_csg.nroSolicitud,
                this.solicitudService.persona_banco_transaccion_debito_csg.importe,
                this.solicitudService.persona_banco_transaccion_debito_csg.moneda
            ).subscribe(async (res) => {
                const response = res as { status: string; message: string; data: persona_banco_transaccion_debito_csg; };
                if (response.data && Object.keys(response.data).length) {
                    this.solicitudService.persona_banco_transaccion_debito_csg.estado = response.data.estado;
                    this.solicitudService.persona_banco_transaccion_debito_csg.descripcion = response.data.descripcion;
                    if (this.solicitudService.persona_banco_transaccion_debito_csg.estado && this.solicitudService.persona_banco_transaccion_debito_csg.estado == 0) {
                        const atributosToUpdate = [64, 65, 66, 67, 69, 96, 98];
                        await this.solicitudService.setFromObjetoAtributoToAtributoInstanciaPoliza(null, atributosToUpdate);
                        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.filter((param) => atributosToUpdate.includes(parseInt(param.objeto_x_atributo.id_atributo + '')) && param.valor != '');
                        await this.justUpdateInstanciaPoliza();
                    } else {
                        this.solicitudService.msgTransaccionInvalida = 'No fue posible validar la transaccion Nro: ' + this.solicitudService.persona_banco_transaccion.nrotran + ' contra el sistema del banco';
                        this.solicitudService.displayTransaccionInvalidaSinContinuar = true;
                        this.solicitudService.isLoadingAgain = false;
                        resolve(false);
                    }
                } else {
                    this.solicitudService.msgTransaccionInvalida = 'No fue posible validar la transaccion Nro: ' + this.solicitudService.persona_banco_transaccion.nrotran + ' contra el sistema del banco, verifica los datos del seguro';
                    this.solicitudService.displayTransaccionInvalidaSinContinuar = true;
                    this.solicitudService.isLoadingAgain = false;
                    resolve(false);
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    async justUpdateInstanciaPoliza() {
        await this.solicitudService.setUserLogin();
        if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
            if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.solicitudService.instanciaDocumentoCertificado.documento.id)) {
                this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoCertificado);
            }
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id && this.solicitudService.hasRolCajero) {
            if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.solicitudService.instanciaDocumentoCertificado.documento.id)) {
                this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoCertificado);
            }
        } else if (this.solicitudService.atributoTipoCredito && this.solicitudService.atributoTipoCredito.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9') {
            if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.solicitudService.instanciaDocumentoCertificado.documento.id)) {
                this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoCertificado);
            }
        }
        this.solicitudService.isLoadingAgain = true;
        if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoDebitAutomatico.id + "") {
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                if ([10,12].includes(parseInt(this.solicitudService.asegurado.instancia_poliza.id_poliza +''))) {
                    if (this.solicitudService.atributoTipoCredito && this.solicitudService.atributoTipoCredito.valor &&
                        this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9') {
                        this.solicitudService.estadosUpdateSolicitud = [
                            this.solicitudService.estadoIniciado.id,
                            this.solicitudService.estadoSolicitado.id,
                            this.solicitudService.estadoPorEmitir.id,
                            this.solicitudService.estadoEmitido.id
                        ]
                    } else {
                        this.solicitudService.estadosUpdateSolicitud = [
                            this.solicitudService.estadoIniciado.id,
                            this.solicitudService.estadoSolicitado.id,
                            this.solicitudService.estadoPorEmitir.id,
                            this.solicitudService.estadoPorPagar.id,
                            this.solicitudService.estadoEmitido.id
                        ]
                    }
                } else {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorPagar.id,
                        this.solicitudService.estadoEmitido.id,
                    ];
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9') {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorEmitir.id,
                        this.solicitudService.estadoEmitido.id,
                    ];
                } else {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorPagar.id,
                        this.solicitudService.estadoEmitido.id,
                    ];
                }
            } else {
                this.solicitudService.estadosUpdateSolicitud = [
                    this.solicitudService.estadoIniciado.id,
                    this.solicitudService.estadoSolicitado.id,
                    this.solicitudService.estadoEmitido.id,
                ];
            }
        } else {
            if (this.solicitudService.hasRolCajero) {
                if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorPagar.id,
                        this.solicitudService.estadoEmitido.id,
                    ];
                } else {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoEmitido.id,
                    ];
                }
            } else {
                if (this.solicitudService.atributoTipoCredito && this.solicitudService.atributoTipoCredito.valor &&
                    (this.solicitudService.TiposCreditoConsumo.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                        this.solicitudService.TiposCreditoDisponible.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                        this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9')
                ) {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorEmitir.id,
                        this.solicitudService.estadoEmitido.id,
                    ];
                } else {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorPagar.id,
                        this.solicitudService.estadoEmitido.id,
                    ];
                }
            }
        }
        this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
        const fechaEmision = this.solicitudService.instanciaDocumentoCertificado.fecha_emision;
        let transicionEmitidoFecha = new Instancia_poliza_transicion();
        transicionEmitidoFecha.observacion = this.msgFechaEmision + ' ' + fechaEmision.getDate() + '/' + (fechaEmision.getMonth() + 1) + '/' + fechaEmision.getFullYear();
        transicionEmitidoFecha.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
        transicionEmitidoFecha.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
        transicionEmitidoFecha.par_observacion_id = this.solicitudService.parametroSuccess.id;
        this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionEmitidoFecha);

        if (this.solicitudService.msgTransaccionInvalida) {
            let transicionEmitidoTranNroInvalido = new Instancia_poliza_transicion();
            transicionEmitidoTranNroInvalido.observacion = this.solicitudService.msgTransaccionInvalida;
            transicionEmitidoTranNroInvalido.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            transicionEmitidoTranNroInvalido.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            transicionEmitidoTranNroInvalido.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionEmitidoTranNroInvalido);
        }
        this.instanciaPolizaService.updateInstanciaToNextStatus(this.solicitudService.estadosUpdateSolicitud, this.solicitudService.asegurado)
            .subscribe(async (res) => {
                const response = res as { status: string; message: string; data: Asegurado; };
                this.solicitudService.asegurado = response.data;
                const docSolicitud = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(param => param.id_documento == this.solicitudService.documentoSolicitud.id);
                const docCertificado = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(param => param.id_documento == this.solicitudService.documentoCertificado.id);
                const nroSolicitudCredito = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == 78);
                this.solicitudService.persona_banco_certificado.certificado = docCertificado ? docCertificado.nro_documento : '';
                this.solicitudService.persona_banco_certificado.IdSolicitud = nroSolicitudCredito ? nroSolicitudCredito.valor : '';
                this.solicitudService.persona_banco_certificado.prima = this.solicitudService.persona_banco_transaccion.importe ? this.solicitudService.persona_banco_transaccion.importe : this.solicitudService.persona_banco_transaccion_debito_csg.importe;
                this.solicitudService.persona_banco_certificado.moneda = this.solicitudService.persona_banco_transaccion.moneda ? this.solicitudService.persona_banco_transaccion.moneda : this.solicitudService.persona_banco_transaccion_debito_csg.moneda;
                this.solicitudService.persona_banco_certificado.fecha_vigencia = moment(docCertificado.fecha_fin_vigencia).format('YYYY-MM-DD');
                this.solicitudService.tran_info = [];
                let atributosToUpdate = [64, 65, 66, 67, 69, 98];
                if ([10,12].includes(parseInt(this.solicitudService.poliza.id+''))) {
                    atributosToUpdate.push(96);
                    // if(environment.production) {
                        await this.soapuiService.regCertificado(
                            this.solicitudService.persona_banco_certificado.IdSolicitud,
                            this.solicitudService.persona_banco_certificado.certificado,
                            this.solicitudService.persona_banco_certificado.prima,
                            this.solicitudService.persona_banco_certificado.moneda,
                            this.solicitudService.persona_banco_certificado.fecha_vigencia
                        ).subscribe(async (res) => {
                            let response = res as { status: string; message: string; data: persona_banco_certificado; };
                            if (response.data && Object.keys(response.data).length) {
                                this.solicitudService.persona_banco_certificado.estado = response.data.estado;
                                this.solicitudService.persona_banco_certificado.descripcion = response.data.descripcion;
                                if (this.solicitudService.persona_banco_certificado.estado && this.solicitudService.persona_banco_certificado.estado == 0) {
                                    let transicionRegCertificado = new Instancia_poliza_transicion();
                                    transicionRegCertificado.observacion = `Informacion enviada al Banco, estado: ${this.solicitudService.persona_banco_certificado.estado}, descripcion: ${this.solicitudService.persona_banco_certificado.descripcion}`;
                                    transicionRegCertificado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
                                    transicionRegCertificado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
                                    transicionRegCertificado.par_observacion_id = this.solicitudService.parametroSuccess.id;
                                    this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionRegCertificado);
                                }
                            }
                        });
                    // }
                }
                await this.solicitudService.setFromObjetoAtributoToAtributoInstanciaPoliza(async () => {
                        await this.solicitudService.setUsuarioRoles(async () => {
                            let atributo_instancia_polizas: Atributo_instancia_poliza[] = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas;
                            this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas = [];
                            for (let i = 0; i < atributo_instancia_polizas.length; i++) {
                                let atributo_instancia_poliza = atributo_instancia_polizas[i];
                                if (atributosToUpdate.find((param) => param == atributo_instancia_poliza.objeto_x_atributo.id_atributo)) {
                                    await this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.push(atributo_instancia_poliza);
                                }
                            }
                            await this.personaService.actualizarSolicitud(this.solicitudService.asegurado).subscribe(async (res) => {
                                await this.solicitudService.setAtributosToPersonaBanco(null, null, atributosToUpdate);
                                await this.solicitudService.setFromObjetoAtributoToAtributoInstanciaPoliza(() => {
                                    this.solicitudService.setDatesOfAsegurado();
                                    for (let i = 0; i < this.solicitudService.asegurado.instancia_poliza.instancia_documentos.length; i++) {
                                        let instancia_documento = this.solicitudService.asegurado.instancia_poliza.instancia_documentos[i];
                                        if (instancia_documento.id_documento == this.solicitudService.documentoSolicitud.id) {
                                            this.solicitudService.instanciaDocumentoSolicitud = instancia_documento;
                                        } else if (instancia_documento.id_documento == this.solicitudService.documentoComprobante.id) {
                                            this.solicitudService.instanciaDocumentoComprobante = instancia_documento;
                                        } else if (instancia_documento.id_documento == this.solicitudService.documentoCertificado.id) {
                                            this.solicitudService.instanciaDocumentoCertificado = instancia_documento;
                                        }
                                    }
                                    this.solicitudService.setBeneficiarios();
                                    if (this.solicitudService.asegurado.instancia_poliza.id_estado + "" == this.solicitudService.estadoSolicitado.id + "") {
                                        this.solicitudService.msgCambioExitoso = "Se ha habilitado la posibilidad de imprimir la solicitud Nro: " + this.solicitudService.persona_banco_transaccion.nrotran + ",  y el comprobante de pago Nro:" + this.solicitudService.instanciaDocumentoComprobante.nro_documento + "";
                                    } else if (this.solicitudService.asegurado.instancia_poliza.id_estado + "" == this.solicitudService.estadoPorPagar.id + "") {
                                        this.solicitudService.msgCambioExitoso = "El cliente debe aproximarse a caja para hacer el pago de la prima y obtener su certificado de cobertura.";
                                    } else if (
                                        this.solicitudService.asegurado.instancia_poliza.id_estado + "" == this.solicitudService.estadoEmitido.id + "") {
                                        this.solicitudService.btnEmitirCertificadoEnabled = true;
                                        this.solicitudService.msgCambioExitoso = "¿Desea imprimir el certificado de cobertura Nro:" + this.solicitudService.instanciaDocumentoCertificado.nro_documento + "" + ".";
                                    }
                                    this.solicitudService.displayEmitirSolicitudDesdeCaja = false;
                                    if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id + "") {
                                        this.solicitudService.msgTransaccionInfo = "La transacción Nro: " + this.solicitudService.persona_banco_transaccion.nrotran + " fue registrada correctamente";
                                        this.solicitudService.displayTransaccionInfo = true;
                                    }
                                    this.solicitudService.isLoadingAgain = false;
                                    if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
                                        this.solicitudService.btnValidarYContinuarEnabled = false;
                                    }
                                    // if (!this.solicitudService.isInAltaSolicitud) {
                                        //     this.solicitudService.verGestionSolicitudes(this.solicitudService.asegurados);
                                        // } else {
                                        //
                                        // }
                                    },
                                    //atributosToUpdate
                                );
                            });
                            //}
                        });
                    },
                    //atributosToUpdate
                );
            });
    }

    sendAsegurado() {
        this.setAsegurado.emit(this.solicitudService.asegurado);
    }
}
