import {Component, Input, OnInit} from '@angular/core';
import {Message} from "primeng/api";
import {Upload} from "../../modelos/upload";
import {ArchivoService} from "../../servicios/archivo.service";
import {SolicitudService} from "../../servicios/solicitud.service";
import {InstanciaDocumentoService} from "../../servicios/instancia-documento.service";
import {Instancia_documento} from "../../modelos/instancia_documento";

@Component({
  selector: 'app-archivos',
  templateUrl: './archivos.component.html',
  styleUrls: ['./archivos.component.scss']
})
export class ArchivosComponent implements OnInit {

  msgs: Message[];
  origin:string;
  
  @Input() uploadedFiles: Upload[] = [];
  @Input() uploadedFile: Upload = null;

  constructor(
    public archivoService: ArchivoService,
    public solicitudService: SolicitudService,
    public instanciaDocumentoService: InstanciaDocumentoService
  ) {
    // this.breadcrumbService.setItems([
    //   {label: 'Components'},
    //   {label: 'File Upload', routerLink: ['/file']}
    // ]);
  }

  ngOnInit() {
    this.origin = window.location.origin;
    if (this.solicitudService.fileDocumentoSolicitud && this.solicitudService.fileDocumentoSolicitud.path) {
      this.uploadedFiles.push(this.solicitudService.fileDocumentoSolicitud);
    }
    if (this.solicitudService.fileDocumentoCertificado && this.solicitudService.fileDocumentoCertificado.path) {
      this.uploadedFiles.push(this.solicitudService.fileDocumentoCertificado);
    }
    for (let i = 0; i < this.uploadedFiles.length; i++) {
      let uploadedFile = this.uploadedFiles[i];
      if (uploadedFile.name && uploadedFile.path) {
        if (uploadedFile.name.indexOf('.pdf') >= 0) {
          uploadedFile.source = `/assets/pdf_logo.jpeg`;
        } else {
          uploadedFile.source = uploadedFile.path;
        }
      } else {
        delete this.uploadedFiles[i];
      }
    }
  }

  showFile(event) {
    console.log(event);
    if (event.image.path) {
      window.open(event.image.path, '_blank');
    } else {
      window.open(`/upload/doc_${this.solicitudService.instanciaDocumentoSolicitud.id}_${event.image.name}`,'_blank');
    }

  }
  onUpload(events) {
    let newName;
    if (this.solicitudService.asegurado.instancia_poliza.id) {
      this.msgs = [];
      for (let i = 0; i < events.files.length; i++) {
        let file = events.files[i];
          if (file.name) {
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
              newName = `doc_${this.solicitudService.instanciaDocumentoSolicitud.id}_`+file.name;
              file.newName = newName;
              [file.file,file.ext] = newName.split('.');
              this.uploadedFile = file;
              this.uploadedFile.path = this.solicitudService.documentoSolicitud.archivo.ubicacion_local;
              this.uploadedFile.pathName = this.solicitudService.documentoSolicitud.archivo.ubicacion_local+'/'+this.uploadedFile.newName;
              this.uploadedFile.tipoDocumento = this.solicitudService.documentoSolicitud.descripcion;
              this.uploadedFile.idTipoDocumento = this.solicitudService.documentoSolicitud.id_tipo_documento;
              if (file.name.indexOf('.pdf') >= 0) {
                this.uploadedFile.source = `/assets/pdf_logo.jpeg`;
              } else {
                this.uploadedFile.source = this.solicitudService.documentoSolicitud.archivo.ubicacion_local+'/'+this.uploadedFile.newName;
              }
              this.solicitudService.instanciaDocumentoSolicitud.nombre_archivo = file.newName;
              this.solicitudService.fileDocumentoSolicitud = file;
              this.uploadedFiles[0] = this.uploadedFile;
              this.msgs.push({severity: 'info', summary: 'Documento Solicitud Subido', detail: ''});
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
              newName = `doc_${this.solicitudService.instanciaDocumentoCertificado.id}_`+file.name;
              file.newName = newName;
              [file.file,file.ext] = newName.split('.');
              this.uploadedFile = file;
              this.uploadedFile.path = this.solicitudService.documentoCertificado.archivo.ubicacion_local;
              this.uploadedFile.pathName = this.solicitudService.documentoCertificado.archivo.ubicacion_local+'/'+this.uploadedFile.newName;
              this.uploadedFile.tipoDocumento = this.solicitudService.documentoCertificado.descripcion;
              this.uploadedFile.idTipoDocumento = this.solicitudService.documentoCertificado.id_tipo_documento;
              if (file.name.indexOf('.pdf') >= 0) {
                this.uploadedFile.source = `/assets/pdf_logo.jpeg`;
              } else {
                this.uploadedFile.source = this.solicitudService.documentoCertificado.archivo.ubicacion_local+'/'+this.uploadedFile.newName;
              }
              this.solicitudService.instanciaDocumentoCertificado.nombre_archivo = file.newName;
              this.solicitudService.fileDocumentoCertificado = file;
              this.uploadedFiles[1] = this.uploadedFile;
              this.msgs.push({severity: 'info', summary: 'Documento Certificado Subido', detail: ''});
            }
          }
      }

      this.solicitudService.archivoSubido = true;
    }
  }

  async deleteFile(idInstanciaDocumento:number) {
    await this.archivoService.delete(idInstanciaDocumento).subscribe(res => {
      let response = res as { status: string, message: string, data: Instancia_documento };
      let deleteDocument = this.uploadedFiles.find(param => param.idTipoDocumento == this.solicitudService.documentoSolicitud.id_tipo_documento);
      let deleteDocumentIndex = this.uploadedFiles.findIndex(param => param.idTipoDocumento == this.solicitudService.documentoSolicitud.id_tipo_documento);
      delete this.uploadedFiles[deleteDocumentIndex];
      this.solicitudService.instanciaDocumentoSolicitud = response.data;
      this.solicitudService.instanciaDocumentoSolicitud.fecha_emision = new Date(this.solicitudService.instanciaDocumentoSolicitud.fecha_emision+'');
      this.solicitudService.instanciaDocumentoSolicitud.fecha_fin_vigencia = new Date(this.solicitudService.instanciaDocumentoSolicitud.fecha_fin_vigencia+'');
      this.solicitudService.instanciaDocumentoSolicitud.fecha_inicio_vigencia = new Date(this.solicitudService.instanciaDocumentoSolicitud.fecha_inicio_vigencia+'');
    });
  }

  changeFile(idInstanciaPoliza:number) {

  }

  validacionGeneralArchivos() {

  }
}
