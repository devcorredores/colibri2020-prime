import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FieldsetModule, FileUploadModule} from "primeng";
import {ArchivosComponent} from "./archivos.component";
import {ToastrModule} from "ngx-toastr";

@NgModule({
  declarations: [
    ArchivosComponent
  ],
  imports: [
    CommonModule,
    FileUploadModule,
    FieldsetModule,
    ToastrModule.forRoot(),
  ],
  exports: [
    FileUploadModule,
    ArchivosComponent,
    FieldsetModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class ArchivosModule { }
