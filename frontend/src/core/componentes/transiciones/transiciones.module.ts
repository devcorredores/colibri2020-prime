import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TransicionesComponent} from "./transiciones.component";
import {DialogModule, TabViewModule} from "primeng";

@NgModule({
  declarations: [
    TransicionesComponent
  ],
  imports: [
    CommonModule,

    DialogModule,
    TabViewModule
  ],
  exports: [
    TransicionesComponent,

    DialogModule,
    TabViewModule
  ]
})
export class TransicionesModule { }
