import {AfterViewChecked, AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {isObject, isString} from "util";
import {Router} from "@angular/router";
import * as moment from 'moment';
import {Instancia_poliza_transicion} from "../../modelos/instancia_poliza_transicion";
import {Parametro} from "../../modelos/parametro";
import {Usuario} from "../../modelos/usuario";
import {Asegurado} from "../../modelos/asegurado";
import {InstanciaPolizaService} from "../../servicios/instancia-poliza.service";
import {SolicitudService} from "../../servicios/solicitud.service";
import {Instancia_poliza} from "../../modelos/instancia_poliza";

@Component({
  selector: 'app-transiciones',
  templateUrl: './transiciones.component.html',
  styleUrls: ['./transiciones.component.scss']
})

export class TransicionesComponent implements OnInit {

    @Input() parametros_atributos: any;
    color = 'primary';
    mode = 'indeterminate';
    value = 1;
    updatedDay: number;
    updatedDateString: String;
    updatedDayName: String;
    weekdays = new Array(7);
    yearMonths = new Array(12);
    actualDate:Date = new Date();
    layout = 'moody';
    transicionesEstadosPoliza:Instancia_poliza_transicion[];
    estadosPoliza:Parametro[] = [];
    usuarioLogin:Usuario;
    fechaActual:Date = new Date();
    instanciaPolizaTransicions:Instancia_poliza_transicion[] = [];
    ObservacionesId:Parametro[];
    parametroSuccess:Parametro;
    estadoPolizaUsuario:Usuario[] = [];
    isLoadingAgain = false;
    parametroWarning:Parametro;
    parametroError:Parametro;
    estadoAnulado:Parametro;
    estadoDesistido:Parametro;
    estadoCaducado:Parametro;
    estadoSinVigencia:Parametro;
    estadoIniciado:Parametro;
    estadoSolicitado:Parametro;
    estadoEmitido:Parametro;
    estadoPorPagar:Parametro;
    parametroInformation:Parametro;
    transicionActive: boolean;
    asegurado:Asegurado;
    transicionClick: boolean;
    strDateStartInstanciaPoliza:string;
    dateStartInstanciaPoliza:Date;

    constructor(
        private router: Router,
        private instanciaPolizaService: InstanciaPolizaService,
        private solicitudService: SolicitudService,
    ) {
        this.yearMonths = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ];
        this.weekdays = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
    }
    
    ngOnInit() {
        if(this.parametros_atributos != undefined && this.parametros_atributos.asegurado != undefined) {
            
            this.ObservacionesId = this.parametros_atributos.ObservacionesId;
            this.instanciaPolizaTransicions = this.parametros_atributos.asegurado.instancia_poliza.instancia_poliza_transicions;
            this.usuarioLogin = this.parametros_atributos.usuarioLogin;
            this.asegurado = this.parametros_atributos.asegurado;
            this.parametroSuccess = this.parametros_atributos.parametroSuccess;
            this.estadoAnulado = this.parametros_atributos.estadoAnulado;
            this.estadoDesistido = this.parametros_atributos.estadoDesistido;
            this.estadoCaducado = this.parametros_atributos.estadoCaducado;
            this.estadoSinVigencia = this.parametros_atributos.estadoSinVigencia;
            this.estadoSolicitado = this.parametros_atributos.estadoSolicitado;
            this.estadoIniciado = this.parametros_atributos.estadoIniciado;
            this.estadoEmitido = this.parametros_atributos.estadoEmitido;
            this.estadoPorPagar= this.parametros_atributos.estadoPorPagar;
            this.parametroWarning = this.parametros_atributos.parametroWarning;
            this.parametroError = this.parametros_atributos.parametroError;
            this.parametroInformation = this.parametros_atributos.parametroInformation;
            this.parametroSuccess = this.parametros_atributos.parametroSuccess;
            this.dateStartInstanciaPoliza = new Date(this.solicitudService.asegurado.instancia_poliza.createdAt);
            this.strDateStartInstanciaPoliza = this.dateStartInstanciaPoliza.getDate()+'/'+(this.dateStartInstanciaPoliza.getMonth()+1)+'/'+this.dateStartInstanciaPoliza.getFullYear()
            if(this.instanciaPolizaTransicions != undefined) {
                this.instanciaPolizaTransicions.forEach(async (instancia_poliza_trancision: Instancia_poliza_transicion) => {
                    if (isString(instancia_poliza_trancision.updatedAt)) {
                        instancia_poliza_trancision.updatedAt = new Date(instancia_poliza_trancision.updatedAt);
                    }
                    if (isString(instancia_poliza_trancision.createdAt)) {
                        instancia_poliza_trancision.createdAt = new Date(instancia_poliza_trancision.createdAt);
                    }
                });
                this.estadosPoliza.forEach((estadoInstanciaPoliza: Parametro, index) => {
                    if(estadoInstanciaPoliza.orden != null) {
                        if (isString(estadoInstanciaPoliza.updatedAt)) {
                            estadoInstanciaPoliza.updatedAt = new Date(estadoInstanciaPoliza.updatedAt);
                        }
                        if (isString(estadoInstanciaPoliza.createdAt)) {
                            estadoInstanciaPoliza.createdAt = new Date(estadoInstanciaPoliza.createdAt);
                        }
                    }
                });
                this.transicionesEstadosPoliza = [];
                this.estadosPoliza.forEach((estadoInstanciaPoliza: Parametro) => {
                    let instanciaPolizaTransicion:Instancia_poliza_transicion = this.instanciaPolizaTransicions.find(params => params.par_estado_id == estadoInstanciaPoliza.id);
                    if(instanciaPolizaTransicion) {
                        instanciaPolizaTransicion.modificado_por = this.usuarioLogin.id+'';
                        instanciaPolizaTransicion.adicionado_por = this.usuarioLogin.id+'';
                        this.transicionesEstadosPoliza.push(instanciaPolizaTransicion);
                    }
                });
            }
        }
    }
    
    onTransicionButtonClick(event) {
        this.transicionActive = !this.transicionActive;
        if(this.transicionActive) {
            this.isLoadingAgain = true;
            this.instanciaPolizaService.findSolicitudTransiciones(this.asegurado.instancia_poliza.id).subscribe(async res => {
                let resp = res as { status: string, message: string, data: Instancia_poliza };
                this.asegurado.instancia_poliza.instancia_poliza_transicions = resp.data.instancia_poliza_transicions;
                this.estadosPoliza = [];
                let oneTrancisionForIniciado, oneTrancisionForSolicitado, oneTrancisionForPorPagar, oneTrancisionForEmitido, oneTrancisionForAnulado, oneTrancisionForDesistido, oneTrancisionForCaducado, oneTrancisionForSinVigencia;
                if(this.asegurado.instancia_poliza.instancia_poliza_transicions && this.asegurado.instancia_poliza.instancia_poliza_transicions.length) {
                    oneTrancisionForIniciado = this.estadoIniciado ? this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == this.estadoIniciado.id) : null;
                    oneTrancisionForSolicitado = this.estadoSolicitado ? this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == this.estadoSolicitado.id) : null;
                    oneTrancisionForPorPagar = this.estadoPorPagar ? this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == this.estadoPorPagar.id) : null;
                    oneTrancisionForEmitido = this.estadoEmitido ? this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == this.estadoEmitido.id) : null;
                    oneTrancisionForAnulado = this.estadoAnulado ? this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == this.estadoAnulado.id) : null;
                    oneTrancisionForDesistido = this.estadoDesistido ? this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == this.estadoDesistido.id) : null;
                    oneTrancisionForCaducado = this.estadoCaducado ? this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == this.estadoCaducado.id) : null;
                    oneTrancisionForSinVigencia = this.estadoSinVigencia ? this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == this.estadoSinVigencia.id) : null;
                }
                
                if (isObject(oneTrancisionForIniciado)) {
                    this.estadosPoliza.push(this.estadoIniciado);
                }
                if (isObject(oneTrancisionForSolicitado)) {
                    this.estadosPoliza.push(this.estadoSolicitado);
                }
                if (isObject(oneTrancisionForPorPagar)) {
                    this.estadosPoliza.push(this.estadoPorPagar);
                }
                if (isObject(oneTrancisionForEmitido)) {
                    this.estadosPoliza.push(this.estadoEmitido);
                }
                if (isObject(oneTrancisionForAnulado)) {
                    this.estadosPoliza.push(this.estadoAnulado);
                }
                if (isObject(oneTrancisionForDesistido)) {
                    this.estadosPoliza.push(this.estadoDesistido);
                }
                if (isObject(oneTrancisionForCaducado)) {
                    this.estadosPoliza.push(this.estadoCaducado);
                }
                if (isObject(oneTrancisionForSinVigencia)) {
                    this.estadosPoliza.push(this.estadoSinVigencia);
                }
    
                this.ngOnInit();
                this.isLoadingAgain = false;
                this.transicionClick = true;
            },err =>{
                if(err.error.statusCode===400 && err.error.message==="usuario no autentificado"){
                    this.router.navigate(['']);
                }else{
                    console.log(err);
                }
            });
        }
        event.preventDefault();
    }
    
    PadLeft (value, length) {
        return (value+''.length < length) ? this.PadLeft("0" + value, length) :
            value;
    }
    
    setDateTime (dateTime) {
        return moment(dateTime).add('hour',4).format('DD/MM/YYYY hh:mm');
    }
    
    onTransicionCloseClick(event) {
        this.transicionActive = false;
        event.preventDefault();
    }

    onTransicionClick(event) {
        this.transicionClick = true;
    }
}
