import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Message, SelectItem } from 'primeng/primeng';
import {Beneficiario} from "../../modelos/beneficiario";
import {Persona} from "../../modelos/persona";
import {Parametro} from "../../modelos/parametro";
import {Asegurado} from "../../modelos/asegurado";
import {persona_banco} from "../../modelos/persona_banco";
import {Atributo_instancia_poliza} from "../../modelos/atributo_instancia_poliza";
import {SoapuiService} from "../../servicios/soapui.service";
import {PersonaService} from "../../servicios/persona.service";
import {BeneficiarioService} from "../../servicios/beneficiario.service";
import {SolicitudService} from "../../servicios/solicitud.service";
import {SegSolicitudes} from "../../modelos/desgravamen/seg_solicitudes";

@Component({
  selector: 'app-beneficiario',
  templateUrl: './beneficiario.component.html',
  styleUrls: ['./beneficiario.component.css']
})
export class BeneficiarioComponent implements OnInit {

  @Input() parametros_beneficiario: any;
  @Output() setBeneficiarios = new EventEmitter<Beneficiario[]>();

  beneficiario: Persona = new Persona();
  userform2: FormGroup;
  userform3: FormGroup;
  editandoBeneficiario: boolean = false;
  displayBeneficiario: boolean = false;
  displayBeneficiarioDouble: boolean = false;
  displayBusquedaCI: boolean = false;
  displayBeneficiariosCambio: boolean = false;
  displayEliminarBeneficiario: boolean = false;
  displayEliminarBeneficiarioOK: boolean = false;
  displayDatosTitular: boolean = false;
  addingBeneficiario: boolean = false;
  isLoadingAgainBeneficiarios: Boolean = false;
  displayBeneficiariosExcedidos: boolean = false;
  displayBeneficiariosTitular: boolean = false;
  displayBeneficiariosNotFound: boolean = false;
  displayErrorBusquedaBeneficiario: boolean = false;
  displayUsoCINuevo: boolean = false;
  displayBeneficiarioFound: boolean = false;
  displayId: boolean = false;
  displayBeneficiarioDuplicado: boolean = false;
  displayGuardado: boolean = false;
  ValidacionesMensajes: Message[] = [];
  // nuevo: boolean = true;

  // TODO: deberiamos evitar usuar estos tipos de variables por que provocan incoherencias,
  //  podemos reemplazar estas variables can las que ya estan establecidas.
  benAux: Beneficiario;
  public id_beneficiarioAux;
  currentBeneficiario:Beneficiario;
  numeroBeneficiarios = 5;
  nuevoRegistroConCI = "0";
  // ---

  Sexos: SelectItem[];
  Tipo: SelectItem[];
  Estado: SelectItem[];
  Parentesco: SelectItem[];
  BeneficiarioCondiciones: SelectItem[];
  Condiciones: SelectItem[];
  ProcedenciaCI: SelectItem[];
  ProcedenciaCIAbreviacion: any[];
  BeneficiarioCondicionesIdParametroDescripcion: any[];
  estadoIniciado: Parametro;
  estadoEmitido: Parametro;
  estadoAnulado: Parametro;
  Beneficiarios: Persona[];
  BeneficiariosAux: Beneficiario[] = [];
  asegurado: Asegurado;
  parentescoAux: Parametro;
  doc_id: string;
  ext: string;
  persona_banco: persona_banco;
  persona_banco_beneficiario: persona_banco;
  docId: string;
  docIdExt: string;
  hasRolConsultaTarjetas: boolean;
  hasRolAdmin: boolean = false;
  public enableAdicionarBeneficiario: boolean = false;
  stopSaving: boolean = false;
  atributoNroSolicitud: Atributo_instancia_poliza;
  constructor(
    private fb: FormBuilder,
    private soapuiService: SoapuiService,
    private personaService: PersonaService,
    private beneficiarioService: BeneficiarioService,
    public solicitudService: SolicitudService
  ) { }


  // ngAfterViewChecked() {
  //   setTimeout(_ => {
  //     if (this.hasRolConsultaTarjetas) {
  //       this.enableAdicionarBeneficiario = true;
  //     }
  //     if(this.asegurado && this.estadoIniciado) {
  //         if(this.asegurado.instancia_poliza.id_estado == this.estadoIniciado.id) {
  //             this.enableAdicionarBeneficiario = false;
  //         } else {
  //             this.enableAdicionarBeneficiario = true;
  //         }
  //     }
  //   });
  // }

  ngOnInit() {
    this.ValidacionesMensajes=[];
    this.ValidacionesMensajes.push({ key: '3', severity: 'warn', detail: "Los apellidos del Beneficiario son requeridos, es obligatorio introducir al menos uno de ellos (paterno o materno)" });
    this.setParametrosFromSolicitud();
    this.beneficiario = new Persona();
    this.userform2 = this.fb.group({
      'id': new FormControl('', []),
      'persona_primer_apellido': new FormControl('', [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")]),
      'persona_segundo_apellido': new FormControl('', [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")]),
      'persona_primer_nombre': new FormControl('', Validators.required),
      'porcentaje': new FormControl('', Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])),
      'id_parentesco': new FormControl('', Validators.required),
      'tipo': new FormControl('', Validators.required),
      'condicion': new FormControl('', this.Condiciones && this.Condiciones.length ? [Validators.required] : []),
      'estado': new FormControl('', Validators.required),
      'descripcion_otro': new FormControl(''),
      'persona_celular': new FormControl('', Validators.required),
      'sexo': new FormControl('', Validators.required)
    });
    //this.solicitudService.getFormValidationErrors(this.userform2);

    this.userform3 = this.fb.group({
      'doc_id': new FormControl('', Validators.required),
      'ext': new FormControl('', Validators.required),
    });

    if (this.asegurado) {
      this.setBeneficiarioOnInit();
      // let resultados = this.validacionGeneralBeneficiarios();
    }

    if (this.hasRolConsultaTarjetas) {
      this.enableAdicionarBeneficiario = true;
    } else {
      this.enableAdicionarBeneficiario = false;
    }
    if (this.asegurado && this.estadoIniciado) {
      if (this.asegurado.instancia_poliza.id_estado == this.estadoIniciado.id) {
        this.enableAdicionarBeneficiario = false;
      } else {
        this.enableAdicionarBeneficiario = true;
      }
    }
    // if ((this.asegurado.instancia_poliza.id_estado == this.estadoEmitido.id+'') || (this.asegurado.instancia_poliza.id_estado == this.estadoAnulado.id+'')) {
    //     this.enableAdicionarBeneficiario = true;
    // }
  }

  async onApellidoInput() {
    this.userform2.controls['persona_primer_apellido'].clearAsyncValidators();
    this.userform2.controls['persona_primer_apellido'].setErrors(null);
    this.userform2.controls['persona_segundo_apellido'].clearAsyncValidators();
    this.userform2.controls['persona_segundo_apellido'].setErrors(null);
    this.userform2.controls['persona_primer_apellido'].setValidators([Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")]);
    this.userform2.controls['persona_segundo_apellido'].setValidators([Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")]);
  }

  async validarApellidos() {
    if (this.beneficiario.persona_primer_apellido == '' && this.beneficiario.persona_segundo_apellido == '') {
      this.userform2.controls['persona_primer_apellido'].setErrors([Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")]);
      this.userform2.controls['persona_segundo_apellido'].setErrors([Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")]);
      this.solicitudService.isLoadingAgain = false;
      this.stopSaving = true;
    } else {
      //this.onApellidoInput();
      this.stopSaving = false;
    }
  }

  async setBeneficiarioOnInit() {
    if (this.asegurado.beneficiarios != undefined) {
      this.BeneficiariosAux = [];
      for (let index = 0; index < this.asegurado.beneficiarios.length; index++) {
        var pBenAux = JSON.stringify(this.asegurado.beneficiarios[index]);
        var pBeneficiario = JSON.parse(pBenAux);
        var persona_doc_id_extAux, estadoAux, condicionAux, parentescoAux: any;

        if (pBeneficiario.persona_doc_id_ext) {
          if (this.ProcedenciaCI.find(params => params.value == pBeneficiario.persona_doc_id_ext+'')) {
            persona_doc_id_extAux = this.ProcedenciaCI.find(params => params.value == pBeneficiario.persona_doc_id_ext+'').label;
          }
        } else {
          persona_doc_id_extAux = null;
        }

        if (this.Estado.find(params => params.value == pBeneficiario.estado, 10)) {
          estadoAux = this.Estado.find(params => params.value == pBeneficiario.estado, 10).label;
        }
        if (this.Condiciones && this.Condiciones.find(params => params.value == pBeneficiario.condicion, 10)) {
          condicionAux = this.Condiciones.find(params => params.value == pBeneficiario.condicion, 10).label;
        }
        if (this.Parentesco.find(params => params.value == pBeneficiario.id_parentesco)) {
          parentescoAux = this.Parentesco.find(params => params.value == pBeneficiario.id_parentesco).label;
        }

        if (pBeneficiario.entidad != undefined) {
          this.BeneficiariosAux.push({
            id: pBeneficiario.id ? pBeneficiario.id : null,
            persona_doc_id: pBeneficiario.persona_doc_id ? pBeneficiario.persona_doc_id+'' : null,
            persona_doc_id_ext: persona_doc_id_extAux,
            persona_primer_apellido: pBeneficiario.entidad.persona.persona_primer_apellido,
            persona_segundo_apellido: pBeneficiario.entidad.persona.persona_segundo_apellido,
            persona_primer_nombre: pBeneficiario.entidad.persona.persona_primer_nombre,
            persona_celular: pBeneficiario.entidad.persona.persona_celular,
            id_beneficiario: pBeneficiario.id,
            par_sexo_id: pBeneficiario.entidad.persona.par_sexo_id,
            id_asegurado: this.asegurado.beneficiarios[index].id_asegurado,
            porcentaje: this.asegurado.beneficiarios[index].porcentaje,
            parentesco: parentescoAux,
            descripcion_otro: this.asegurado.beneficiarios[index].descripcion_otro ? this.asegurado.beneficiarios[index].descripcion_otro : pBeneficiario.descripcion_otro ? pBeneficiario.descripcion_otro : '',
            tipo: this.asegurado.beneficiarios[index].tipo,
            estado: estadoAux,
            condicion: condicionAux,
            id_parentesco:pBeneficiario.id_parentesco,
            fecha_declaracion: this.asegurado.beneficiarios[index].fecha_declaracion
          });
        } else if (pBeneficiario) {
          this.BeneficiariosAux.push({
            id: pBeneficiario.id ? pBeneficiario.id : null,
            persona_doc_id: pBeneficiario.persona_doc_id ? pBeneficiario.persona_doc_id+'' : null,
            persona_doc_id_ext: persona_doc_id_extAux,
            persona_primer_apellido: pBeneficiario.persona_primer_apellido,
            persona_segundo_apellido: pBeneficiario.persona_segundo_apellido,
            persona_primer_nombre: pBeneficiario.persona_primer_nombre,
            persona_celular: pBeneficiario.persona_celular,
            id_beneficiario: pBeneficiario.id_beneficiario,
            id_asegurado: this.asegurado.beneficiarios[index].id_asegurado,
            porcentaje: this.asegurado.beneficiarios[index].porcentaje,
            parentesco: parentescoAux,
            descripcion_otro: this.asegurado.beneficiarios[index].descripcion_otro ? this.asegurado.beneficiarios[index].descripcion_otro : pBeneficiario.descripcion_otro ? pBeneficiario.descripcion_otro : '',
            tipo: this.asegurado.beneficiarios[index].tipo,
            estado: estadoAux,
            condicion: condicionAux,
            id_parentesco:pBeneficiario.id_parentesco,
            fecha_declaracion: this.asegurado.beneficiarios[index].fecha_declaracion,
            par_sexo_id: pBeneficiario.par_sexo_id
          });
        }
      }
    }
  }

  setParametrosFromSolicitud() {
    this.Sexos = this.parametros_beneficiario.Sexos;
    this.Tipo = this.parametros_beneficiario.Tipo;
    this.Estado = this.parametros_beneficiario.Estado;
    this.Parentesco = this.parametros_beneficiario.Parentesco;
    this.Condiciones = this.parametros_beneficiario.BeneficiarioCondiciones;
    this.BeneficiarioCondiciones = this.parametros_beneficiario.BeneficiarioCondiciones;
    this.ProcedenciaCI = this.parametros_beneficiario.ProcedenciaCI;
    this.ProcedenciaCIAbreviacion = this.parametros_beneficiario.ProcedenciaCIAbreviacion;
    this.BeneficiarioCondicionesIdParametroDescripcion = this.parametros_beneficiario.BeneficiarioCondicionesIdParametroDescripcion;
    this.estadoIniciado = this.parametros_beneficiario.estadoIniciado;
    this.estadoEmitido = this.parametros_beneficiario.estadoEmitido;
    this.estadoAnulado = this.parametros_beneficiario.estadoAnulado;
    this.asegurado = this.parametros_beneficiario.asegurado;
    // this.BeneficiariosAux = this.parametros_beneficiario.BeneficiariosAux;
    this.parentescoAux = this.parametros_beneficiario.parentescoAux;
    this.doc_id = this.parametros_beneficiario.doc_id;
    this.ext = this.parametros_beneficiario.ext;
    this.persona_banco = this.parametros_beneficiario.persona_banco;
    this.persona_banco_beneficiario = this.parametros_beneficiario.persona_banco_beneficiario;
    this.docId = this.parametros_beneficiario.docId;
    this.docIdExt = this.parametros_beneficiario.docIdExt;
    this.hasRolConsultaTarjetas = this.parametros_beneficiario.hasRolConsultaTarjetas;
    this.hasRolAdmin = this.parametros_beneficiario.hasRolAdmin;
  }

  sendBeneficiarios() {
    this.setBeneficiarios.emit(this.BeneficiariosAux)
  }

  registrarNuevoConCI() {
    this.displayBeneficiariosNotFound = false;
    this.displayUsoCINuevo = true;
    // this.nuevoRegistroConCI = "1";
  }

  registrarNuevoSinCI() {
    this.displayBeneficiariosNotFound = false;
    this.displayUsoCINuevo = false;
    // this.nuevoRegistroConCI = "0";
    this.userform3.reset();
  }

  BuscarClienteBeneficiario() {
    this.displayBeneficiarioFound = false;
    this.displayBeneficiariosNotFound = false;
    this.userform2.reset();
    this.userform2.controls['tipo'].setValue("86");
    this.userform2.controls['estado'].setValue("83");
    this.userform2.controls['condicion'].setValue("300");
    if (this.doc_id && this.ext) {
        this.soapuiService.getCustomer(this.doc_id, this.ext).subscribe(res => {
          let response = res as { status: string, message: string, data: persona_banco };
          if (response.data != undefined) {
            this.displayBeneficiarioFound = true;
            this.persona_banco_beneficiario = response.data;
            this.beneficiario = new Persona();
            this.beneficiario.persona_primer_apellido = this.persona_banco_beneficiario.paterno;
            this.beneficiario.persona_segundo_apellido = this.persona_banco_beneficiario.materno;
            this.beneficiario.persona_primer_nombre = this.persona_banco_beneficiario.nombre;
            this.beneficiario.persona_celular = this.persona_banco_beneficiario.nro_celular;
            this.beneficiario.persona_doc_id = this.persona_banco_beneficiario.doc_id;
            this.beneficiario.persona_doc_id_comp = this.persona_banco_beneficiario.complemento == "" ? "0" : this.persona_banco_beneficiario.complemento;
            this.beneficiario.persona_doc_id_ext = this.ProcedenciaCIAbreviacion[this.persona_banco_beneficiario.extension];
            this.beneficiario.persona_fecha_nacimiento = new Date(this.persona_banco_beneficiario.mes_fechanac + '/' + this.persona_banco_beneficiario.dia_fechanac + '/' + this.persona_banco_beneficiario.anio_fechanac);
            this.beneficiario.persona_direccion_domicilio = this.persona_banco_beneficiario.direccion;
            this.beneficiario.par_sexo_id = this.persona_banco_beneficiario.sexo ? this.Sexos.find(params => params.label.substring(0, 1) == this.persona_banco_beneficiario.sexo).value : null;
          } else {
            this.displayBeneficiariosNotFound = true;
          }
        });
    } else {
        this.displayErrorBusquedaBeneficiario = true;
    }

    // this.cols = [
    //   { field: 'id_objeto_x_atributo', header: 'ID' },
    //   { field: 'valor', header: 'VALOR' }
    // ];
  }

  AbrirVentanaBeneficiarioVerificandoDesgravamen() {
    this.setParametrosFromSolicitud();
    this.addingBeneficiario = true;
    this.atributoNroSolicitud = this.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo && param.objeto_x_atributo.atributo.id == 78);
    if (this.atributoNroSolicitud && [10,12].includes(parseInt(this.asegurado.instancia_poliza.id_poliza+'')) && !this.BeneficiariosAux.length) {
      this.beneficiarioService.verificaBeneficiariosDesgravamen(this.atributoNroSolicitud.valor,this.asegurado.entidad.persona.persona_doc_id+'',this.asegurado.entidad.persona.persona_doc_id_ext).subscribe(res => {
        let response = res as { status: string, message: string, data: SegSolicitudes };
        if (response.data && response.data.sol_deudor && response.data.sol_deudor.length && response.data.sol_deudor.find(param => param.deu_beneficiarios.length > 0)) {
          let duplicados = [];
          for (let i = 0; i < response.data.sol_deudor.length; i++) {
            let solDeudor = response.data.sol_deudor[i];
            for (let j = 0; j < solDeudor.deu_beneficiarios.length; j++) {
              let solDeudorBeneficiario = solDeudor.deu_beneficiarios[j];
              let nombreBeneficiario = solDeudorBeneficiario.Ben_Nombre+' '+solDeudorBeneficiario.Ben_Paterno+' '+solDeudorBeneficiario.Ben_Materno;
              if (duplicados.find(param => param == nombreBeneficiario)) {
                duplicados.push(nombreBeneficiario);
              }
            }
          }
          if (duplicados.length > 1) {
            this.solicitudService.displayBeneficiariosDuplicados = true;
          } else {
            this.solicitudService.displayMigrarBeneficiarios = true;
          }
        } else {
          this.solicitudService.displayMigrarBeneficiarios = false;
          let beneficiario = new Persona();
          this.displayBeneficiarioFound = false;
          this.displayBeneficiario = true;
          this.displayDatosTitular = true;
          this.isLoadingAgainBeneficiarios = false;
          this.userform3.reset();
          this.userform2.reset();
          this.userform2.controls['tipo'].setValue("86");
          this.userform2.controls['estado'].setValue("83");
          this.userform2.controls['condicion'].setValue("300");
        }
      },(error) => {
        let response = error as { status: string, message: string};
        this.solicitudService.displayMigrarBeneficiarios = false;
        let beneficiario = new Persona();
        this.displayBeneficiarioFound = false;
        this.displayBeneficiario = true;
        this.displayDatosTitular = true;
        this.isLoadingAgainBeneficiarios = false;
        this.userform3.reset();
        this.userform2.reset();
        this.userform2.controls['tipo'].setValue("86");
        this.userform2.controls['estado'].setValue("83");
        this.userform2.controls['condicion'].setValue("300");
      });
    } else {
      let beneficiario = new Persona();
      this.displayBeneficiarioFound = false;
      this.displayBeneficiario = true;
      this.displayDatosTitular = true;
      this.isLoadingAgainBeneficiarios = false;
      this.userform3.reset();
      this.userform2.reset();
      this.userform2.controls['tipo'].setValue("86");
      this.userform2.controls['estado'].setValue("83");
      this.userform2.controls['condicion'].setValue("300");
    }
  }

  AbrirVentanaBeneficiario() {
    this.setParametrosFromSolicitud();
    this.solicitudService.displayMigrarBeneficiarios = false;
    let beneficiario = new Persona();
    this.displayBeneficiarioFound = false;
    this.displayBeneficiario = true;
    this.displayDatosTitular = true;
    this.isLoadingAgainBeneficiarios = false;
    this.addingBeneficiario = true;
    this.userform3.reset();
    this.userform2.reset();
    this.userform2.controls['tipo'].setValue("86");
    this.userform2.controls['estado'].setValue("83");
    this.userform2.controls['condicion'].setValue("300");

  }



  migrarBeneficiarios() {
    this.solicitudService.isLoadingAgain = true;
    this.beneficiarioService.migrarBeneficiariosDesgravamen(this.atributoNroSolicitud.valor,this.asegurado.entidad.persona.persona_doc_id+'',this.asegurado.entidad.persona.persona_doc_id_ext,this.asegurado.id_instancia_poliza).subscribe(res => {
      let response = res as { status: string, message: string, data: Asegurado };
      this.solicitudService.isLoadingAgain = false;
      this.solicitudService.displayMigrarBeneficiarios = false;
      this.asegurado.beneficiarios = response.data.beneficiarios ? response.data.beneficiarios : [];
      if (this.asegurado) {
        this.setBeneficiarioOnInit();
      }
    });
  }

  confirmado() {
    this.displayGuardado = false;
    // this.displayBeneficiario = false;
  }

  validacionGeneralBeneficiarios(): number[] {
    /*
      Resultados validacion general de usuarios en array
      vacio: Se pasaron todas las validaciones
      1: Porcentaje total no coincide con 100
      2: Beneficiario es titular
      3: Beneficiario repetidos en CI y EXT_CI
      4: Existen conyugues duplicados
      5: No Existen beneficiarios
      6: Existen convivientes duplicados
    */

    // var resultadoValida:  number[];
    var resultadoValida = [];

    /* Valida que haya beneficiarios */
    if (this.asegurado.beneficiarios && this.asegurado.beneficiarios.length) {
      /* Validar total porcentaje */
      var porcentajeTotal: number = 0;
      let beneficiariosPrimarios = this.asegurado.beneficiarios.filter(param => param.condicion == this.solicitudService.parametroBeneficiarioPrimario.id+'' || param.condicion == this.solicitudService.parametroBeneficiarioPrimario.parametro_descripcion);
      let beneficiariosContingentes = this.asegurado.beneficiarios.filter(param => param.condicion == this.solicitudService.parametroBeneficiarioContingente.id+'' || param.condicion == this.solicitudService.parametroBeneficiarioContingente.parametro_descripcion);
      this.asegurado.beneficiarios.forEach(element => {
        porcentajeTotal = porcentajeTotal + parseInt(element.porcentaje, 10)
      });
      beneficiariosPrimarios.forEach(element => {
        porcentajeTotal = porcentajeTotal + parseInt(element.porcentaje, 10)
      });
      if ((beneficiariosPrimarios && beneficiariosPrimarios.length) || (beneficiariosContingentes && beneficiariosContingentes.length)) {
        let benPorcentajesPrimarios, benPorcentajesContingentes,benPorcentajeTotalPrimarios,benPorcentajeTotalContingentes;
        if ((beneficiariosPrimarios && beneficiariosPrimarios.length)) {
          benPorcentajesPrimarios = beneficiariosPrimarios.map(param => parseInt(param.porcentaje));
          benPorcentajeTotalPrimarios = benPorcentajesPrimarios.reduce((accumulator, curr) => accumulator + curr);
        }
        if ((beneficiariosContingentes && beneficiariosContingentes.length)) {
          benPorcentajesContingentes = beneficiariosContingentes.map(param => parseInt(param.porcentaje));
          benPorcentajeTotalContingentes = benPorcentajesContingentes.reduce((accumulator, curr) => accumulator + curr);
        }
        if (benPorcentajeTotalPrimarios && benPorcentajeTotalPrimarios != 100) {
          resultadoValida.push(15)
        }
        if (benPorcentajeTotalContingentes) {
          if (benPorcentajeTotalContingentes != 100) {
            resultadoValida.push(17)
          }
        }
      } else {
        if (porcentajeTotal != 100) {
          resultadoValida.push(1)
        }
      }

      /* Validar beneficiario es titular */
      this.asegurado.beneficiarios.forEach(element => {
        let procedenciaAux;
        if (this.ProcedenciaCI.find(params => params.label == element.persona_doc_id_ext)) {
          procedenciaAux = this.ProcedenciaCI.find(params => params.label == element.persona_doc_id_ext).value;
        }
        if ((this.asegurado.entidad.persona.persona_doc_id == element.persona_doc_id) && (this.asegurado.entidad.persona.persona_doc_id_ext == procedenciaAux)) {
          resultadoValida.push(2);
        }
      });

      /* Validar beneficiario duplicado */
      this.asegurado.beneficiarios.forEach(element => {
        this.asegurado.beneficiarios.forEach(element2 => {
          if ((element.persona_primer_nombre != element2.persona_primer_nombre) && (element.persona_primer_apellido != element2.persona_primer_apellido) && (element.persona_segundo_apellido != element2.persona_segundo_apellido)) {
            if ((parseInt(element.persona_doc_id, 10) == parseInt(element2.persona_doc_id, 10)) && (element.persona_doc_id_ext == element2.persona_doc_id_ext)) {
              resultadoValida.push(3);
            }
          }
        });
      });

      /* Validar conyugue duplicado */
      this.asegurado.beneficiarios.forEach(element => {
        let parentescoValue;
        if (this.Parentesco.find(params => params.label == element.parentesco)) {
          parentescoValue = parseInt(this.Parentesco.find(params => params.label == element.parentesco).value, 10);
        }
        if (parentescoValue == 49) {
          this.asegurado.beneficiarios.forEach(element2 => {
            if ((element.persona_primer_nombre != element2.persona_primer_nombre) && (element.persona_primer_apellido != element2.persona_primer_apellido) && (element.persona_segundo_apellido != element2.persona_segundo_apellido)) {
              let parentesco2Value;
              if (this.Parentesco.find(params => params.label == element2.parentesco)) {
                parentesco2Value = parseInt(this.Parentesco.find(params => params.label == element2.parentesco).value, 10);
              }
              if (parentesco2Value == 49) {
                resultadoValida.push(4);
              }
            }
          });
        }
      });

      /* Validar conviviente duplicado */
      let benConvivientes = this.asegurado.beneficiarios.filter(param => param.id_parentesco == 48);
      if (benConvivientes.length > 1) {
        resultadoValida.push(6);
      }
      let benConyugue = this.asegurado.beneficiarios.filter(param => param.id_parentesco == 49);
      if (benConyugue.length > 1) {
        resultadoValida.push(7);
      }
      // let benHijos = this.asegurado.beneficiarios.filter(param => param.id_parentesco == 50);
      // if (benHijos.length > 1) {
      //   resultadoValida.push(8);
      // }
      // let benHermano = this.asegurado.beneficiarios.filter(param => param.id_parentesco == 52);
      // if (benHermano.length > 1) {
      //   resultadoValida.push(9);
      // }
      let benMadre = this.asegurado.beneficiarios.filter(param => param.id_parentesco == 53);
      if (benMadre.length > 1) {
        resultadoValida.push(10);
      }
      let benPadre = this.asegurado.beneficiarios.filter(param => param.id_parentesco == 55);
      if (benPadre.length > 1) {
        resultadoValida.push(11);
      }
      // let benOtroParentesco = this.asegurado.beneficiarios.filter(param => param.id_parentesco == 56);
      // if (benOtroParentesco.length > 1) {
      //   resultadoValida.push(12);
      // }
      /* Validar Beneficiarios Primarios y Contingentes */

      if([10,12].includes(parseInt(this.solicitudService.poliza.id+''))) {
        let benPrimarios = this.asegurado.beneficiarios.filter(param => param.condicion == this.solicitudService.parametroBeneficiarioPrimario.id+'' || param.condicion == this.solicitudService.parametroBeneficiarioPrimario.parametro_descripcion);
        if (benPrimarios.length < 1) {
          resultadoValida.push(13);
        }
        let benContingentes = this.asegurado.beneficiarios.filter(param => param.condicion == this.solicitudService.parametroBeneficiarioContingente.id+'' || param.condicion == this.solicitudService.parametroBeneficiarioContingente.parametro_descripcion);
        if (benContingentes.length < 1) {
          // resultadoValida.push(14);
        }
        if (benPrimarios.length) {
          let initialValuePrimarios = 0;
          let procentajesPrimarios = benPrimarios.map(param => parseInt(param.porcentaje));
          let totalPrimarios = procentajesPrimarios.reduce((previousValue, currentValue) => previousValue + currentValue, initialValuePrimarios);
          if (totalPrimarios > 100) {
            resultadoValida.push(15);
          }
          if (totalPrimarios < 100) {
            resultadoValida.push(16);
          }
        }
        if (benContingentes.length) {
          let initialValueContingentes = 0;
          let procentajesContingentes = benContingentes.map(param => parseInt(param.porcentaje));
          let totalContingentes = procentajesContingentes.reduce((previousValue, currentValue) => previousValue + currentValue, initialValueContingentes);
          if (totalContingentes > 100) {
            resultadoValida.push(17);
          }
          if (totalContingentes < 100) {
            resultadoValida.push(18);
          }
        }
      } else {

      }
    } else {
      resultadoValida.push(5)
    }
    // return resultadoValida;
    // let distintosResultadoValida = [...new Set(resultadoValida)]
    let distintosResultadoValida = resultadoValida.filter((el, index) => resultadoValida.indexOf(el) === index)
    return distintosResultadoValida;
  }

  async guardarPersonaBeneficiario() {
    this.displayBeneficiario = false;
    let post2 = this.userform2.getRawValue();
    let post3 = this.userform3.getRawValue();
    let toContinue = false;
    if (this.addingBeneficiario) {
      if (this.BeneficiariosAux.find(param =>
          param.persona_primer_nombre == post2.persona_primer_nombre &&
          param.persona_primer_apellido == post2.persona_primer_apellido &&
          param.persona_segundo_apellido == post2.persona_segundo_apellido
      )) {
        this.solicitudService.displayRegistrandoDuplicado = true;
        toContinue = false;
      } else {
        toContinue = true;
      }
    } else {
      toContinue = true;
    }
    if (toContinue) {
      var porcentaje = post2.porcentaje;
      var id_parentesco = post2.id_parentesco;
      var id_tipo = post2.tipo;
      var par_sexo_id = post2.sexo;
      var id_estado = post2.estado;
      var id_condicion = post2.condicion;
      var tipo = id_tipo && this.Tipo.length ? this.Tipo.find(params => params.value == id_tipo).label : '';
      var estado = id_estado && this.Estado.length ? this.Estado.find(params => params.value == id_estado).label : '';
      var condicion = id_condicion && this.Condiciones && this.Condiciones.length ? this.Condiciones.find(params => params.value == id_condicion).label : '';
      var persona_celular = post2.persona_celular;
      var parentesco = id_parentesco && this.Parentesco.length ? this.Parentesco.find(params => params.value == id_parentesco).label : '';
      var descripcion_otro = '';
      if (id_parentesco == '56') {
        descripcion_otro = post2.descripcion_otro;
        parentesco = parentesco + '-' + descripcion_otro;
      }
      var id_asegurado = this.asegurado.id;
      var persona_primer_apellido = post2.persona_primer_apellido;
      var persona_segundo_apellido = post2.persona_segundo_apellido;
      var persona_primer_nombre = post2.persona_primer_nombre;
      var persona_doc_id = post3.doc_id;
      var id_persona_doc_id_ext = post3.ext;
      var persona_doc_id_ext = id_persona_doc_id_ext && this.ProcedenciaCI.length ? this.ProcedenciaCI.find(params => params.value == id_persona_doc_id_ext).label : '';

      // var beneficiarioValida = this.BeneficiariosAux.find(params => params.persona_primer_apellido === persona_primer_apellido && params.persona_segundo_apellido === persona_segundo_apellido && params.persona_primer_nombre === persona_primer_nombre)
      // if (beneficiarioValida == undefined) {
      // let post3 = this.userform3.getRawValue();
      // if (((post3.doc_id == null) || (post3.ext == null)) && (!this.displayBeneficiarioFound)) {
      if (!this.displayBeneficiarioFound) {
        for (var clave in this.beneficiario) {
          this.beneficiario[clave] = null;
        }

        if (this.displayUsoCINuevo) {
          this.nuevoRegistroConCI = "1";
          this.beneficiario.persona_doc_id = persona_doc_id;
          this.beneficiario.persona_doc_id_ext = id_persona_doc_id_ext;
        } else {
          this.nuevoRegistroConCI = "0";
        }

        this.beneficiario.persona_primer_nombre = persona_primer_nombre;
        this.beneficiario.persona_primer_apellido = persona_primer_apellido;
        this.beneficiario.persona_segundo_apellido = persona_segundo_apellido;
        this.beneficiario.par_sexo_id= par_sexo_id;
        this.beneficiario.persona_celular= persona_celular;
      }

      if ((this.asegurado.entidad.persona.persona_doc_id == this.beneficiario.persona_doc_id) && (this.asegurado.entidad.persona.persona_doc_id_ext == this.beneficiario.persona_doc_id_ext)) {
        this.displayBeneficiariosTitular = true;
      } else {

        this.solicitudService.isLoadingAgain = true;
        await this.validarApellidos();
        if (!this.stopSaving) {
          if (!this.editandoBeneficiario) {
            if (this.BeneficiariosAux.length < this.numeroBeneficiarios) {
              this.personaService.AdicionarPersonaBeneficiario(
                  this.asegurado.id,
                  porcentaje,
                  id_parentesco,
                  descripcion_otro,
                  persona_celular,
                  id_tipo,
                  id_estado,
                  id_condicion,
                  this.beneficiario,
                  this.nuevoRegistroConCI,
                  this.displayBeneficiarioFound
              ).subscribe(async res => {
                this.solicitudService.isLoadingAgain = false;
                let response = await res as { status: 'OK', message: string, data: any };
                var id_beneficiario = parseInt(response.data.id, 10);
                var fecha_declaracion = response.data.fecha_declaracion;
                this.BeneficiariosAux.push({
                  persona_doc_id,
                  persona_doc_id_ext,
                  id_beneficiario,
                  id_asegurado,
                  porcentaje,
                  parentesco,
                  descripcion_otro,
                  persona_primer_apellido,
                  persona_segundo_apellido,
                  persona_primer_nombre,
                  persona_celular,
                  tipo,
                  estado,
                  condicion,
                  fecha_declaracion,
                  id_parentesco,
                  par_sexo_id
                });
                this.displayBeneficiario = false;
                this.displayGuardado = true;
                this.sendBeneficiarios();
              });
            } else {
              this.displayBeneficiariosExcedidos = true;
            }
          } else {
            // if (this.displayUsoCINuevo) {
            // this.nuevoRegistroConCI = "1";
            this.beneficiario.persona_doc_id = persona_doc_id;
            // this.beneficiario.persona_doc_id_ext = id_persona_doc_id_ext;
            this.beneficiario.persona_doc_id_ext = id_persona_doc_id_ext;
            // } else {
            //   this.nuevoRegistroConCI = "0";
            // }
            this.beneficiarioService.ModificarPersonaBeneficiario(this.id_beneficiarioAux, porcentaje, id_parentesco, descripcion_otro, persona_celular, id_tipo, id_estado, id_condicion, this.beneficiario).subscribe(res => {
              let response = res as { status: 'OK', message: string, data: any };
              this.solicitudService.isLoadingAgain = false;
              for (let index = 0; index < this.BeneficiariosAux.length; index++) {
                if (this.BeneficiariosAux[index].id_beneficiario == this.id_beneficiarioAux) {
                  this.BeneficiariosAux[index].parentesco = parentesco;
                  this.BeneficiariosAux[index].persona_doc_id = post3.doc_id;
                  this.BeneficiariosAux[index].persona_doc_id_ext = this.ProcedenciaCI.find(params => params.value == post3.ext).label;
                  this.BeneficiariosAux[index].tipo = tipo;
                  this.BeneficiariosAux[index].estado = estado;
                  this.BeneficiariosAux[index].condicion = condicion;
                  this.BeneficiariosAux[index].id = post2.id;
                  this.BeneficiariosAux[index].id_parentesco = id_parentesco;
                  this.BeneficiariosAux[index].persona_celular = persona_celular;
                  this.BeneficiariosAux[index].porcentaje = porcentaje;
                  this.BeneficiariosAux[index].persona_primer_apellido = post2.persona_primer_apellido;
                  this.BeneficiariosAux[index].persona_segundo_apellido = post2.persona_segundo_apellido;
                  this.BeneficiariosAux[index].persona_primer_nombre = post2.persona_primer_nombre;
                  this.BeneficiariosAux[index].par_sexo_id = post2.sexo;
                  this.BeneficiariosAux[index].descripcion_otro = post2.descripcion_otro;
                  this.BeneficiariosAux[index].persona_celular = post2.persona_celular;
                  this.sendBeneficiarios();
                }
              }
              this.displayBeneficiario = false;
              this.displayGuardado = true;
            })
          }
        }
      }
      // } else {
      //   this.displayBeneficiarioDouble = true;
      // }
      this.editandoBeneficiario = false;
      this.userform2.reset();
      this.userform3.reset();
      //this.sendBeneficiarios();
    }
  }

  guardarBeneficiario() {
    this.Beneficiarios.push(this.beneficiario);
    this.displayBeneficiario = false;
  }

  editarBeneficiario(ben: Beneficiario) {
      this.addingBeneficiario = false;
      if (!this.solicitudService.asegurado.beneficiarios.length) {
          this.solicitudService.asegurado.beneficiarios = this.solicitudService.beneficiarios;
      }
      if (this.BeneficiariosAux.length) {
          if (this.solicitudService.asegurado.beneficiarios.length) {
              for (let i = 0 ; i < this.solicitudService.asegurado.beneficiarios.length ; i++) {
                  let beneficiario:any = this.solicitudService.asegurado.beneficiarios[i];
                  if (beneficiario.entidad && beneficiario.entidad.persona) {
                      let beneficiarioFound = this.BeneficiariosAux.find(param =>
                          param.persona_primer_nombre == beneficiario.entidad.persona.persona_primer_nombre &&
                          param.persona_primer_apellido == beneficiario.entidad.persona.persona_primer_apellido &&
                          param.persona_segundo_apellido == beneficiario.entidad.persona.persona_segundo_apellido &&
                          param.condicion == this.BeneficiarioCondicionesIdParametroDescripcion[beneficiario.condicion] &&
                          param.id == beneficiario.id
                      );
                      let beneficiarioFoundIndex = this.BeneficiariosAux.findIndex(param =>
                          param.persona_primer_nombre == beneficiario.entidad.persona.persona_primer_nombre &&
                          param.persona_primer_apellido == beneficiario.entidad.persona.persona_primer_apellido &&
                          param.persona_segundo_apellido == beneficiario.entidad.persona.persona_segundo_apellido &&
                          param.condicion == this.BeneficiarioCondicionesIdParametroDescripcion[beneficiario.condicion] &&
                          param.id == beneficiario.id
                      );
                      if (beneficiarioFound) {
                        beneficiarioFound.id_beneficiario = beneficiario.id;
                        beneficiarioFound.id_parentesco = beneficiario.id_parentesco;
                        beneficiarioFound.descripcion_otro = beneficiario.descripcion_otro;
                        beneficiarioFound.id_asegurado = beneficiario.id_asegurado;
                        beneficiarioFound.porcentaje = beneficiario.porcentaje;
                        this.BeneficiariosAux[beneficiarioFoundIndex] = beneficiarioFound;
                      }
                  }
              }
          }
      } else {
          if (this.solicitudService.asegurado.beneficiarios.length) {
              for (let i = 0 ; i < this.solicitudService.asegurado.beneficiarios.length ; i++) {
                  let beneficiario:any = this.solicitudService.asegurado.beneficiarios[i];
                  if (beneficiario.entidad && beneficiario.entidad.persona) {
                    this.BeneficiariosAux.push(beneficiario.entidad.persona);
                  }
              }
          }
      }
      if (this.asegurado.instancia_poliza.id_estado == this.estadoIniciado.id) {
      this.editandoBeneficiario = true;
      this.displayBeneficiarioFound = false;
      this.currentBeneficiario = this.BeneficiariosAux.find(params =>
          params.persona_primer_apellido == ben.persona_primer_apellido &&
          params.persona_segundo_apellido == ben.persona_segundo_apellido &&
          params.persona_primer_nombre == ben.persona_primer_nombre &&
          params.condicion == ben.condicion &&
          params.id == ben.id
      );
      this.id_beneficiarioAux = this.currentBeneficiario.id_beneficiario;
      let porcentajeAux = this.currentBeneficiario.porcentaje;
      let parentescoAux = this.currentBeneficiario.parentesco;
      let estadoAux = this.currentBeneficiario.estado;
      let condicionAux = this.currentBeneficiario.condicion;
      let persona_celularAux = this.currentBeneficiario.persona_celular;
      let parentescoAux2 = this.Parentesco.find(params => params.label == parentescoAux) ? this.Parentesco.find(params => params.label == parentescoAux).value : null;
      let estadoAux2 = estadoAux ? this.Estado.find(params => params.label == estadoAux).value : '';
      let condicionAux2 = condicionAux && this.Condiciones ? this.Condiciones.find(params => params.label == condicionAux).value : '';
      this.userform2.controls['persona_primer_apellido'].setValue(this.currentBeneficiario.persona_primer_apellido);
      this.userform2.controls['persona_segundo_apellido'].setValue(this.currentBeneficiario.persona_segundo_apellido);
      this.userform2.controls['persona_primer_nombre'].setValue(this.currentBeneficiario.persona_primer_nombre);
      this.userform2.controls['persona_celular'].setValue(this.currentBeneficiario.persona_celular);
      this.userform2.controls['sexo'].setValue(this.currentBeneficiario.par_sexo_id);
      this.userform2.controls['porcentaje'].setValue(porcentajeAux);
      let otroParentesco = parentescoAux ? parentescoAux.split('-')[1] : '';
      if (parentescoAux2) {
          this.userform2.controls['id_parentesco'].setValue(parentescoAux2);
      } else {
          this.userform2.controls['id_parentesco'].setValue('56');
      }
      this.userform2.controls['descripcion_otro'].setValue(this.currentBeneficiario.descripcion_otro);
      this.userform2.controls['estado'].setValue(estadoAux2);
      this.userform2.controls['condicion'].setValue(condicionAux2);
      this.userform2.controls['tipo'].setValue(86);
      let beneficiarioAux = this.BeneficiariosAux.find(params =>
          params.persona_primer_apellido == ben.persona_primer_apellido &&
          params.persona_segundo_apellido == ben.persona_segundo_apellido &&
          params.persona_primer_nombre == ben.persona_primer_nombre &&
          params.condicion == ben.condicion &&
          params.id == ben.id
      );
      if (beneficiarioAux) {
        let persona_doc_id = beneficiarioAux.persona_doc_id;
        let persona_doc_id_extAux = beneficiarioAux.persona_doc_id_ext;
        let persona_doc_id_ext;
        if (this.ProcedenciaCI.find(params => params.label == persona_doc_id_extAux)) {
          persona_doc_id_ext = this.ProcedenciaCI.find(params => params.label == beneficiarioAux.persona_doc_id_ext).value;
        }
        if (this.userform3) {
            if (persona_doc_id) {
              this.userform3.controls['doc_id'].setValue(persona_doc_id);
              this.userform3.controls['ext'].setValue(persona_doc_id_ext);
            } else {
                if (typeof this.userform3.reset == 'function') {
                    this.userform3.reset();
                }
            }
        }
      }
      this.beneficiarioService.findBeneficiario(this.id_beneficiarioAux).subscribe(res => {
        let response = res as { status: string, message: string, data: Persona };
        this.beneficiario = response.data;
        // this.VendedorForm.controls['nombres'].setValue(response.body.data.nombres);
        // this.BeneficiariosAux.splice(this.BeneficiariosAux.indexOf(ben), 1);
        this.userform2.controls['persona_celular'].setValue(persona_celularAux);
      },
        err => console.error("ERROR llamando servicio informacion de beneficiario:", err));
      this.displayBeneficiario = true;
    }
    else {
      this.displayBeneficiariosCambio = true;
    }
  }

  eliminarBeneficiario() {
    // this.Beneficiarios.splice(this.Beneficiarios.indexOf(per), 1);
    if (this.asegurado.instancia_poliza.id_estado == this.estadoIniciado.id) {
      this.displayEliminarBeneficiario = false;
      // var id_beneficiario = this.BeneficiariosAux.find(params => params.persona_primer_apellido === ben.persona_primer_apellido && params.persona_segundo_apellido === ben.persona_segundo_apellido && params.persona_primer_nombre === ben.persona_primer_nombre).id_beneficiario;
      var beneficiarioAux = this.BeneficiariosAux.find(params =>
          params.persona_primer_apellido === this.benAux.persona_primer_apellido &&
          params.persona_segundo_apellido === this.benAux.persona_segundo_apellido &&
          params.persona_primer_nombre === this.benAux.persona_primer_nombre &&
          params.condicion === this.benAux.condicion &&
          params.id === this.benAux.id
      );
      if (beneficiarioAux) {
        this.beneficiarioService.eliminarBeneficiario(beneficiarioAux.id_beneficiario).subscribe(res => {
          this.BeneficiariosAux.splice(this.BeneficiariosAux.indexOf(this.benAux), 1);
          let benIndexDelete = this.asegurado.beneficiarios.findIndex(param => param.id == this.benAux.id);
          delete this.asegurado.beneficiarios[benIndexDelete];
        },
          err => console.error("ERROR llamando servicio elminacion de beneficiario:", err));
      }
    } else {
      this.displayBeneficiariosCambio = true;
    }
  }

  eliminarBeneficiarioConfirmacion(ben: Beneficiario) {
    this.benAux = ben;
    this.displayEliminarBeneficiario = true;
    this.stopSaving=true;
  }

  validacionApellidos(): boolean {
      if (this.beneficiario) {
          if (this.beneficiario.persona_primer_apellido || this.beneficiario.persona_segundo_apellido || this.beneficiario.persona_primer_nombre) {
            if (!this.beneficiario.persona_primer_apellido){
              this.beneficiario.persona_primer_apellido='';
            }
            if (!this.beneficiario.persona_segundo_apellido){
              this.beneficiario.persona_segundo_apellido='';
            }
              if ((this.beneficiario.persona_primer_apellido + this.beneficiario.persona_segundo_apellido).trim() === '') {
                return true;
              } else {
                return false;
              }
          }
      } else {
          this.displayErrorBusquedaBeneficiario = true;
      }

  }

  devuelveValorParentesco(id: any) {
    let parametro = this.Parentesco.find(param=>param.value+''===id+'')
    if (parametro) {
      return parametro.label;
    } else {
      return '';
    }
  }

  devuelveValorSexo(id: any) {
    let parametro = this.Sexos.find(param=>param.value+''===id+'')
    if (parametro) {
      return parametro.label;
    } else {
      return '';
    }
  }
}

