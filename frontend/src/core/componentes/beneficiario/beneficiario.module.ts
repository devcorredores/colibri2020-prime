import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BeneficiarioComponent} from "./beneficiario.component";
import {
  BreadcrumbModule, ButtonModule,
  DataViewModule,
  DialogModule,
  DropdownModule,
  FieldsetModule, InputTextModule, MessagesModule,
  PanelModule,
  TableModule,
  ToastModule
} from "primeng";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    BeneficiarioComponent
  ],
  imports: [
    CommonModule,

    FormsModule,
    DialogModule,
    DropdownModule,
    DataViewModule,
    InputTextModule,
    ButtonModule,
    FieldsetModule,

    ReactiveFormsModule
  ],
  exports: [
    BeneficiarioComponent,

    ButtonModule,
    TableModule,
    FormsModule,
    DialogModule,
    InputTextModule,
    DropdownModule,
    DataViewModule,
    FieldsetModule,

    ReactiveFormsModule
  ]
})
export class BeneficiarioModule { }
