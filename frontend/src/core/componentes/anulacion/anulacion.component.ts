import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators, ValidationErrors} from '@angular/forms';
import {SelectItem} from 'primeng/primeng';
import {Router} from '@angular/router';
import "../../../../src/helpers/prototypes";
import * as moment from "moment";
import {Asegurado} from "../../modelos/asegurado";
import {Instancia_poliza_transicion} from "../../modelos/instancia_poliza_transicion";
import {Instancia_documento} from "../../modelos/instancia_documento";
import {Atributo_instancia_documento} from "../../modelos/atributo_instancia_documento";
import {InstanciaDocumentoService} from "../../servicios/instancia-documento.service";
import {InstanciaPolizaTransService} from "../../servicios/instancia-poliza-trans.service";
import {SolicitudService} from "../../servicios/solicitud.service";
import {MenuService} from "../../servicios/menu.service";
import {SessionStorageService} from "../../servicios/sessionStorage.service";
import {BreadcrumbService} from "../../servicios/breadcrumb.service";
import {SegDeudores} from "../../modelos/desgravamen/seg_deudores";
import {SegObservaciones} from "../../modelos/desgravamen/seg_observaciones";
import {Parametro} from "../../modelos/parametro";

@Component({
    selector: 'app-anulacion',
    templateUrl: './anulacion.component.html',
    styleUrls: ['./anulacion.component.css']
})

export class AnulacionComponent implements OnInit {

    motivoCambioDes: string = '';
    motivoCambio: string = '';
    desLabel: string = '';
    estadoActual: string = '';
    userform2: FormGroup;
    estadoLabel: string = '';
    userform3: FormGroup;
    displayNotfound: boolean = false;
    displaySolicitudProcesada: boolean = false;
    displaySolicitudProcesadaConImpresion: boolean = false;
    displayFoundSol: boolean = false;
    displayNotPosible: boolean = false;
    disabledFound: boolean = true;
    displayConfirmar: boolean = false;
    displayGuardado: boolean = false;
    asegurado_nombre1: string;
    asegurado_nombre2: string;
    asegurado_apellido1: string;
    asegurado_apellido2: string;
    fecha_inicio_vigencia_solicitud: string = '';
    fecha_fin_vigencia_solicitud: string = '';
    fecha_inicio_vigencia_certificado: string = '';
    fecha_fin_vigencia_certificado: string = '';
    fecha_emision_solicitud: string = '';
    fecha_emision_certificado: string = '';
    nro_certificado: string;
    Estados: SelectItem[];
    par_estado_id: number = null;
    par_estado: Parametro;
    par_estado_abreviacion: string = null;
    idsPoliza: any[] = [];
    parametrosRuteo: any;
    id_instancia_poliza: number;
    id_deudor: number;
    nro_solicitud: string;
    id_estado: number;
    estado: string;
    producto: string;
    id_instancia_polizaTxt: string;
    par_observacion_id: number = 97;
    currentEstado: SelectItem;
    showBtnGuardar: boolean = false;
    showBtnImprimirAnulacion: boolean = false;
    ruta;
    asegurado: Asegurado;
    deudor: SegDeudores;
    proceder: boolean = false;
    displayPreventMessage: boolean = false;
    showImpresora: boolean = false;
    enableImprimir: boolean = false;
    preventMessage: string = ''
    fecha_anulado: string;
    observacion: string = '';
    motivo: string = '';
    id_motivo: string = '';
    instanciaPolizaTrancisionAnulado: Instancia_poliza_transicion = null;
    deudorTrancisionAnulado: SegObservaciones = null;
    instanciaDocumentoAnulacion: Instancia_documento = null;
    atributoInstanciaDocumentoMotivo: Atributo_instancia_documento = null;
    id_poliza: number = null;
    idInstanciaPoliza: number = null;
    idDeudor: number = null;
    nroSolicitud: string = '';
    nroCertificado: string = '';
    showIdMotivo: boolean = false;

    constructor(
        private fb: FormBuilder,
        private instanciaDocumentoService: InstanciaDocumentoService,
        private instanciaPolizaTransService: InstanciaPolizaTransService,
        private router: Router,
        public solicitudService: SolicitudService,
        public menuService: MenuService,
        private sessionStorageService: SessionStorageService,
        public breadcrumbService: BreadcrumbService
    ) {
    }

    ngOnInit() {
        this.displayNotfound = false;
        this.disabledFound = false;
        this.Estados = [];
        this.parametrosRuteo = this.sessionStorageService.getItemSync('parametros');
        this.idsPoliza = this.parametrosRuteo.parametro_vista ? this.parametrosRuteo.parametro_vista.split(',') : this.parametrosRuteo.parametro_vista;
        this.idsPoliza = this.idsPoliza ? this.idsPoliza.map((param) => { return parseInt(param + '') }) : ['null'];
        this.showIdMotivo = this.idsPoliza.find(param => [10, 12].includes(param)) ? true : false;
        this.userform2 = this.fb.group({
            'id_motivo': new FormControl('', this.showIdMotivo ? [Validators.required] : []),
            'observacion': new FormControl('', [Validators.required, Validators.minLength(30)])
        });
        this.userform3 = this.fb.group({
            'numSolicitud': new FormControl('', ['null'].find(param => this.idsPoliza.includes(param + '')) ? [] : [Validators.required]),
            'id_deudor': new FormControl('', ['null'].find(param => this.idsPoliza.includes(param + '')) ? [Validators.required] : []),
            // 'id_instancia_poliza': new FormControl('', [10, 12].find(param => this.idsPoliza.includes(param)) ? [Validators.required] : []),
            'par_estado_id': new FormControl('', [Validators.required])
        });
        this.solicitudService.getAllParametrosByIdDiccionario([], this.idsPoliza, [11, 17, 69, 70], () => {
            this.solicitudService.setUserLogin(() => {
                if (['null'].find(param => this.idsPoliza.includes(param + ''))) {
                    if (this.solicitudService.usuarioLogin.usuarioRoles.find(param => param.id == 15)) {
                        this.Estados = this.solicitudService.EstadosDeudor.filter(param => ['350', '352', 'null'].includes(param.value + ''));
                    } else {
                        this.Estados = this.solicitudService.EstadosDeudor.filter(param => ['350', 'null'].includes(param.value + ''));
                    }
                } else {
                    if (this.solicitudService.usuarioLogin.usuarioRoles.find(param => [15,5].includes(parseInt(param.id+'')))) {
                        this.Estados = this.solicitudService.EstadosInstaciaPoliza.filter(param => ['60', '62', 'null'].includes(param.value + ''));
                    } else {
                        this.Estados = this.solicitudService.EstadosInstaciaPoliza.filter(param => ['60', 'null'].includes(param.value + ''));
                    }
                }
            });
        });

        this.ruta = this.parametrosRuteo.ruta;
        this.ruta = this.parametrosRuteo.ruta;
        this.ruta = this.ruta ? this.ruta.replace('/Anular', '') : '';
        this.ruta = this.ruta ? this.ruta.replace('PRODUCTOS/', '') : '';
        this.solicitudService.product = this.ruta;

        this.breadcrumbService.setItems([
            {label: this.ruta}
        ]);
    }

    showPopUpEstado() {

    }

    getFormValidationErrors(productForm: FormGroup) {
        Object.keys(productForm.controls).forEach(key => {
            const controlErrors: ValidationErrors = productForm.get(key).errors;
            if (controlErrors != null) {
                Object.keys(controlErrors).forEach(keyError => {
                    console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                });
            }
        });
    }

    guardarNuevaObservacion(newObservationValue:string, idInstanciaPoliza:number, idEstado:number) {
        this.instanciaPolizaTransService.guardarNuevaObservacion(newObservationValue, idInstanciaPoliza, idEstado).subscribe(res => {
            const response = res as { status: string, message: string, data: Instancia_poliza_transicion };
            this.observacion = response.data.observacion;
        })
    }

    BuscarSolicitud() {
        this.displayNotfound = false;
        this.disabledFound = true;
        this.displayFoundSol = false;
        this.asegurado_nombre1 = null;
        this.showImpresora = ['null', '10', '12'].includes(this.id_poliza + '') ? true : false;
        this.enableImprimir = false;
        this.par_estado = this.solicitudService.estadosPoliza.find(param => param.id == this.par_estado_id);
        if (this.idsPoliza.includes(10) || this.idsPoliza.includes(12)) {
            // this.instanciaDocumentoService.findInstanciaIdToEstado(this.par_estado_id, this.id_instancia_poliza, this.idsPoliza).subscribe(res => {
            this.instanciaDocumentoService.findInstanciaNroDocumentoToEstado(this.par_estado_id, this.nro_solicitud, this.idsPoliza).subscribe(res => {
                let response = res as { status: string, message: string, data: Asegurado };
                if (response.data && Object.keys(response.data).length) {
                    this.asegurado = response.data;
                    this.id_instancia_poliza = this.asegurado.id_instancia_poliza;
                    this.setAsegurado();
                    if (this.par_estado_id == this.solicitudService.estadoDesistido.id && this.asegurado.instancia_poliza.id_estado != this.solicitudService.estadoEmitido.id) {
                        this.displayNotPosible = true;
                    } else {
                        this.displayFoundSol = true;
                        this.enableImprimir = [this.solicitudService.estadoDesistido.id, this.solicitudService.estadoRechazado.id].includes(this.asegurado.instancia_poliza.id_estado);
                    }
                } else {
                    this.displayNotfound = true;
                    this.displaySolicitudProcesadaConImpresion = false;
                }
            }, err => {
                this.displayNotfound = true;
            });
        } else if (this.idsPoliza.includes('null')) {
            this.par_estado_abreviacion = this.solicitudService.EstadosDeudorAbreviacion[this.par_estado_id];
            this.instanciaDocumentoService.findDeudorId(this.id_deudor).subscribe(res => {
                let response = res as { status: string, message: string, data: SegDeudores };
                if (response.data && Object.keys(response.data).length) {
                    this.deudor = response.data;
                    this.enableImprimir = [this.solicitudService.estadoDesistido.id, this.solicitudService.estadoRechazado.id].includes(this.asegurado.instancia_poliza.id_estado);
                    this.displayFoundSol = true;
                    this.setDeudor();
                } else {
                    this.displayNotfound = true;
                    this.displaySolicitudProcesadaConImpresion = false;
                }
            }, err => {
                this.displayNotfound = true;
            });
        } else if (this.nro_solicitud) {
            this.instanciaDocumentoService.findInstanciaNroDocumentoToEstado(this.par_estado_id, this.nro_solicitud, this.idsPoliza).subscribe(res => {
                let response = res as { status: string, message: string, data: Asegurado };
                if (response.data && Object.keys(response.data).length) {
                    this.asegurado = response.data;
                    this.id_instancia_poliza = this.asegurado.id_instancia_poliza;
                    this.setAsegurado();
                    if (this.par_estado_id == this.solicitudService.estadoDesistido.id && this.asegurado.instancia_poliza.id_estado != this.solicitudService.estadoEmitido.id) {
                        this.displayNotPosible = true;
                    } else {
                        this.displayFoundSol = true;
                        this.enableImprimir = [this.solicitudService.estadoDesistido.id, this.solicitudService.estadoRechazado.id].includes(this.asegurado.instancia_poliza.id_estado);
                    }
                } else {
                    this.displayNotfound = true;
                    this.displaySolicitudProcesada = false;
                }
            }, err => {
                this.displayNotfound = true;
            });
        }
    }

    validateForm() {
        this.solicitudService.getFormValidationErrors(this.userform3)
    }

    selectCurrentStado(event) {
        this.currentEstado = this.Estados.find(param => param.value == this.par_estado_id);
        this.estadoLabel = this.currentEstado.value == 60 ? 'anuló' : this.currentEstado.value == 350 ? 'rechazó' : this.currentEstado.value == 352 ? 'no se incluyó' : 'desistió';
        this.desLabel = this.currentEstado.value == 60 ? 'anulado' : this.currentEstado.value == 350 ? 'rechazado' : this.currentEstado.value == 352 ? 'no fue incluido' : 'desistido';
        this.estadoActual = this.currentEstado.value == 60 ? 'Anulado' : this.currentEstado.value == 350 ? 'Rechazado' : this.currentEstado.value == 352 ? 'No Incluido' : 'Desistido';
    }

    cambiarSiEstadoSolicitud() {
        this.displayConfirmar = true;
    }

    Anular() {
        this.displayConfirmar = false;
        this.displayGuardado = false;
        this.solicitudService.isLoadingAgain = true;
        this.par_estado_id = 60;
        if (this.proceder) {
            this.instanciaPolizaTransService.guardarEstadoInstanciaPolizaTrans(this.asegurado.id_instancia_poliza, this.id_poliza, this.par_estado_id, this.id_motivo, this.observacion, this.solicitudService.usuarioLogin.id).subscribe(res => {
                let response = res as { status: string, message: string, data: Asegurado };
                this.solicitudService.isLoadingAgain = false;
                this.asegurado = response.data;
                this.id_instancia_poliza = this.asegurado.id_instancia_poliza;
                this.enableImprimir = [this.solicitudService.estadoDesistido.id, this.solicitudService.estadoRechazado.id].includes(this.asegurado.instancia_poliza.id_estado);
                this.setAsegurado();
                this.displayGuardado = true;
                // this.CancelarAnulacion();
            }, err => console.error("ERROR llamando servicio:", err.message));
        }
    }

    Desistir() {
        this.displayConfirmar = false;
        this.displayGuardado = false;
        this.solicitudService.isLoadingAgain = true;
        this.par_estado_id = 62;
        if (this.proceder) {
            this.instanciaPolizaTransService.guardarEstadoInstanciaPolizaTrans(this.asegurado.id_instancia_poliza, this.id_poliza, this.par_estado_id, this.id_motivo, this.observacion, this.solicitudService.usuarioLogin.id).subscribe(res => {
                let response = res as { status: string, message: string, data: Asegurado };
                this.solicitudService.isLoadingAgain = false;
                this.asegurado = response.data;
                this.id_instancia_poliza = this.asegurado.id_instancia_poliza;
                this.enableImprimir = [this.solicitudService.estadoDesistido.id, this.solicitudService.estadoRechazado.id].includes(this.asegurado.instancia_poliza.id_estado);
                this.setAsegurado();
                this.displayGuardado = true;
            }, err => console.error("ERROR llamando servicio:", err.message));
        }
    }

    Rechazar() {
        this.displayConfirmar = false;
        this.displayGuardado = false;
        this.solicitudService.isLoadingAgain = true;
        this.par_estado_id = this.solicitudService.parametroRechazado.id;
        if (this.proceder) {
            this.observacion = !this.observacion.includes(this.par_estado_id + '') ? this.par_estado_id + '|' + this.observacion : this.observacion;
            this.instanciaPolizaTransService.guardarEstadoDeudorTrans(this.deudor.Deu_Id, this.par_estado_abreviacion, this.observacion, this.solicitudService.usuarioLogin.id, this.deudor.deu_solicitud.Sol_IdSol).subscribe(res => {
                let response = res as { status: string, message: string, data: SegDeudores };
                this.solicitudService.isLoadingAgain = false;
                this.deudor = response.data;
                this.enableImprimir = true;
                this.setDeudor();
                this.displayGuardado = true;
            }, err => {
                console.error("ERROR llamando servicio:", err.message)
            });
        }
    }

    NoIncluir() {
        this.displayConfirmar = false;
        this.displayGuardado = false;
        this.par_estado_id = this.solicitudService.parametroNoIncluido.id;
        this.solicitudService.isLoadingAgain = true;
        if (this.proceder) {
            this.observacion = !this.observacion.includes(this.par_estado_id + '') ? this.par_estado_id + '|' + this.observacion : this.observacion;
            this.instanciaPolizaTransService.guardarEstadoDeudorTrans(this.deudor.Deu_Id, this.par_estado_abreviacion, this.observacion, this.solicitudService.usuarioLogin.id, this.deudor.deu_solicitud.Sol_IdSol).subscribe(res => {
                let response = res as { status: string, message: string, data: SegDeudores };
                this.solicitudService.isLoadingAgain = false;
                this.enableImprimir = true;
                this.deudor = response.data;
                this.setDeudor();
                this.displayGuardado = true;
            }, err => console.error("ERROR llamando servicio:", err.message));
        }
    }

    confirmado() {
        //  let userInfo = this.sessionStorageService.getItemSync('userInfo');
        // if (userInfo.rol.find(param => param.id == 8)) {
        //          window.location.replace("/administracion");
        //        } else if (userInfo.rol.find(param => param.id == 5)) {
        //          window.location.replace("/crediticios");
        //        } else if (userInfo.rol.find(param => param.id == 20)) {
        //          window.location.replace("/consultas");
        //        } else {
        //          window.location.replace("/masivos");
        //        }
        //       this.router.navigate(['/']);
    }

    CancelarAnulacion() {
        // this.userform3.reset();
        // this.userform2.reset();
        // this.disabledFound = true;
    }

    setAsegurado() {
        if (this.asegurado && this.asegurado.instancia_poliza) {
            let now = moment();
            let fechaRegistro = moment(this.asegurado.instancia_poliza.fecha_registro);
            let fechaEmision;
            let instanciaDocumentoCertificado = this.asegurado.instancia_poliza.instancia_documentos.find(param => param.documento.id_tipo_documento == 281);
            if (instanciaDocumentoCertificado) {
                fechaEmision = moment(instanciaDocumentoCertificado.fecha_emision);
            }
            if (this.par_estado_id == 60) {
                if (fechaEmision && this.asegurado.instancia_poliza.id_estado == 59) {
                    if ((fechaEmision.date() == now.date() && fechaEmision.month() == now.month() && fechaEmision.year() == now.year())) {
                        this.proceder = true;
                    } else {
                        if (this.solicitudService.usuarioLogin.usuarioRoles.find(param => param.id == 15)) {
                            this.proceder = true;
                        } else {
                            this.proceder = false;
                            this.preventMessage = `La solicitud fue emitida en fecha: ${moment(fechaEmision).format('DD/MM/YYYY hh:mm')}, por lo que ahora solo podra ser cambiada de estado en dicha fecha.`;
                            this.displayPreventMessage = true;
                        }
                    }
                } else {
                    this.proceder = true;
                }

                if ((fechaRegistro.date() == now.date() && fechaRegistro.month() == now.month() && fechaRegistro.year() == now.year())) {
                    this.proceder = true;
                } else if (this.asegurado.instancia_poliza.id_estado != 59) {
                    this.proceder = true;
                } else {
                    this.proceder = false;
                    this.preventMessage = `Ya no es posible Anular la solicitud su fecha de emision es: ${fechaEmision ? moment(fechaEmision).format('DD/MM/YYYY') : ''}, solo se puede anular el dia de la emision`;
                    this.displayPreventMessage = true;
                }
            } else if (this.par_estado_id == 62) {
                if ([10,12].includes(parseInt(this.asegurado.instancia_poliza.id_poliza+''))) {
                    if (this.asegurado.instancia_poliza.id_estado == 59) {
                        this.proceder = true;
                    } else {
                        this.proceder = false;
                    }
                } else {
                    this.proceder = true;
                }
            }
            if ([10, 12].includes(parseInt(this.asegurado.instancia_poliza.id_poliza + '')) && this.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoDesistido.id) {
                this.enableImprimir = true;
            }
            if ([10, 12].includes(parseInt(this.asegurado.instancia_poliza.id_poliza + ''))) {
                this.showBtnImprimirAnulacion = true;
            } else {
                this.showBtnImprimirAnulacion = false;
            }
            if (this.asegurado.instancia_poliza.id_estado == 60) {
                this.proceder = false;
                this.instanciaPolizaTrancisionAnulado = this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == 60);
                this.preventMessage = `La solicitud fue actualizada exitosamente al estado ${this.asegurado.instancia_poliza.estado.parametro_descripcion} en fecha: ${moment(this.asegurado.instancia_poliza.updatedAt).format('DD/MM/YYYY hh:mm')}, por lo que ahora no será posible realizar acciones sobre esta solicitud`;
                this.displayPreventMessage = true;
            } else if (this.asegurado.instancia_poliza.id_estado == 62) {
                this.proceder = false;
                this.instanciaDocumentoAnulacion = this.asegurado.instancia_poliza.instancia_documentos.find(param => param.documento.id_tipo_documento == 348);
                this.instanciaPolizaTrancisionAnulado = this.asegurado.instancia_poliza.instancia_poliza_transicions.find(param => param.par_estado_id == 62);
                if (this.instanciaDocumentoAnulacion) {
                    this.preventMessage = `La solicitud fue actualizada exitosamente al estado: ${this.asegurado.instancia_poliza.estado.parametro_descripcion}, en fecha: ${moment(this.instanciaDocumentoAnulacion.fecha_emision).format('DD/MM/YYYY hh:mm')}, por lo que ahora no será posible realizar acciones sobre esta solicitud <br>
                                           antes de entregar el anexo de desistimiento debe primero hacer firmar la carta de desistimiento, desea imprimir los documentos de desistimiento?`;
                    this.fecha_anulado = moment(this.instanciaDocumentoAnulacion.createdAt + '').format('DD/MM/YYYY hh:mm');
                } else {
                    this.preventMessage = `La solicitud fue actualizada exitosamente al estado ${this.asegurado.instancia_poliza.estado.parametro_descripcion} en fecha: ${moment(this.asegurado.instancia_poliza.updatedAt).format('DD/MM/YYYY hh:mm')}, por lo que ahora no será posible realizar acciones sobre esta solicitud`;
                }
                this.displayPreventMessage = true;
            }
            this.id_estado = parseInt(this.asegurado.instancia_poliza.id_estado + '');
            let instanciasDocumentos = this.asegurado.instancia_poliza.instancia_documentos;
            if (instanciasDocumentos) {
                for (let i = 0; i < instanciasDocumentos.length; i++) {
                    let instanciasDocumento = instanciasDocumentos[i];
                    if (instanciasDocumento.atributo_instancia_documentos && instanciasDocumento.atributo_instancia_documentos.length) {
                        this.atributoInstanciaDocumentoMotivo = instanciasDocumento.atributo_instancia_documentos.find(param => param.objeto_x_atributo && parseInt(param.objeto_x_atributo.id_atributo + '') == 94);
                    }
                    switch (parseInt(instanciasDocumento.documento.id_tipo_documento + '')) {
                        case 279:
                            this.solicitudService.instanciaDocumentoSolicitud = instanciasDocumento;
                            this.fecha_inicio_vigencia_solicitud = instanciasDocumento.fecha_inicio_vigencia && instanciasDocumento.fecha_inicio_vigencia ? moment(instanciasDocumento.fecha_inicio_vigencia).format('DD/MM/YYYY hh:mm') : null;
                            this.fecha_fin_vigencia_solicitud = instanciasDocumento.fecha_fin_vigencia && instanciasDocumento.fecha_fin_vigencia ? moment(instanciasDocumento.fecha_fin_vigencia).format('DD/MM/YYYY hh:mm') : null;
                            this.nroSolicitud = instanciasDocumento.nro_documento ? instanciasDocumento.nro_documento : null;
                            this.fecha_emision_solicitud = instanciasDocumento.fecha_emision ? moment(instanciasDocumento.fecha_emision).format('DD/MM/YYYY hh:mm') : null;
                            this.solicitudService.instanciaDocumentoSolicitud.nro_documento = instanciasDocumento.nro_documento;
                            break;
                        case 281:
                            this.solicitudService.instanciaDocumentoSolicitud = instanciasDocumento;
                            this.fecha_inicio_vigencia_certificado = instanciasDocumento.fecha_inicio_vigencia ? moment(instanciasDocumento.fecha_inicio_vigencia).format('DD/MM/YYYY hh:mm') : null;
                            this.fecha_fin_vigencia_certificado = instanciasDocumento.fecha_fin_vigencia ? moment(instanciasDocumento.fecha_fin_vigencia).format('DD/MM/YYYY hh:mm') : null;
                            this.nroCertificado = instanciasDocumento.nro_documento ? instanciasDocumento.nro_documento : null;
                            this.fecha_emision_certificado = instanciasDocumento.fecha_emision ? moment(instanciasDocumento.fecha_emision).format('DD/MM/YYYY hh:mm') : null;
                            this.solicitudService.instanciaDocumentoCertificado.nro_documento = instanciasDocumento.nro_documento;
                            break;
                    }
                }
            }
            this.asegurado_nombre1 = this.asegurado.entidad.persona.persona_primer_nombre ? this.asegurado.entidad.persona.persona_primer_nombre : '';
            this.asegurado_nombre2 = this.asegurado.entidad.persona.persona_segundo_nombre ? this.asegurado.entidad.persona.persona_segundo_nombre : '';
            this.asegurado_apellido1 = this.asegurado.entidad.persona.persona_primer_apellido ? this.asegurado.entidad.persona.persona_primer_apellido : '';
            this.asegurado_apellido2 = this.asegurado.entidad.persona.persona_segundo_apellido ? this.asegurado.entidad.persona.persona_segundo_apellido : '';
            this.idInstanciaPoliza = this.asegurado.instancia_poliza.id;
            this.id_poliza = this.asegurado.instancia_poliza.id_poliza;
            this.estado = this.asegurado.instancia_poliza.estado.parametro_descripcion;
            this.observacion = this.instanciaPolizaTrancisionAnulado ? this.instanciaPolizaTrancisionAnulado.observacion : '';
            this.motivo = this.atributoInstanciaDocumentoMotivo ? this.atributoInstanciaDocumentoMotivo.par_motivo[0] ? this.atributoInstanciaDocumentoMotivo.par_motivo[0].parametro_descripcion : '' : '';
            this.producto = this.asegurado.instancia_poliza.poliza ? this.asegurado.instancia_poliza.poliza.descripcion : '';
            this.showBtnGuardar = true;
            this.displayNotfound = false;
            this.disabledFound = false;
            this.solicitudService.isLoadingAgain = false;
        }
    }

    setDeudor() {
        if (this.deudor) {
            let now = moment();
            let fechaRegistro = moment(this.deudor.Deu_FecRegCli);
            let fechaEmision = moment(this.deudor.deu_solicitud.Sol_FechaSol);
            if (this.par_estado_id == this.solicitudService.parametroNoIncluido.id) {
                if (fechaEmision && this.deudor.Deu_Incluido == 'S') {
                    if ((fechaEmision.date() == now.date() && fechaEmision.month() == now.month() && fechaEmision.year() == now.year())) {
                        this.proceder = true;
                    } else {
                        if (this.solicitudService.usuarioLogin.usuarioRoles.find(param => param.id == 15)) {
                            this.proceder = true;
                        } else {
                            this.proceder = false;
                            this.preventMessage = `La solicitud fue emitida en fecha: ${moment(fechaEmision).format('DD/MM/YYYY hh:mm')}, por lo que solo podra ser cambiada de estado en dicha fecha.`;
                            this.displayPreventMessage = true;
                        }
                    }
                } else {
                    this.proceder = false;
                }
            } else if (this.par_estado_id == this.solicitudService.parametroRechazado.id) {
                this.proceder = true;
            }
            this.showBtnImprimirAnulacion = true;
            this.asegurado_nombre1 = this.deudor.Deu_Nombre ? this.deudor.Deu_Nombre : '';
            this.asegurado_nombre2 = '';
            this.asegurado_apellido1 = this.deudor.Deu_Paterno ? this.deudor.Deu_Paterno : '';
            this.asegurado_apellido2 = this.deudor.Deu_Materno ? this.deudor.Deu_Materno : '';
            this.idDeudor = this.deudor.Deu_Id;
            this.fecha_inicio_vigencia_certificado = this.deudor.deu_solicitud.Sol_EstadoSol.includes('A') ? this.deudor.deu_solicitud.Sol_FechaSol : null;
            this.fecha_fin_vigencia_certificado = null;
            this.fecha_inicio_vigencia_solicitud = this.deudor.deu_solicitud.Sol_FechaSol;
            this.fecha_fin_vigencia_solicitud = null;
            this.estado = this.solicitudService.EstadosDeudorParCodigo[this.deudor.Deu_Incluido];
            this.observacion = this.deudorTrancisionAnulado ? this.deudorTrancisionAnulado.Obs_Observacion : '';
            this.showBtnGuardar = true;
            this.displayNotfound = false;
            this.disabledFound = false;

            if ([this.solicitudService.parametroRechazado.parametro_cod, this.solicitudService.parametroNoIncluido.parametro_cod].includes(this.deudor.Deu_Incluido)) {
                this.proceder = false;
                this.deudorTrancisionAnulado = this.deudor.deu_solicitud.sol_observaciones.find(param => [this.solicitudService.parametroRechazado.id, this.solicitudService.parametroNoIncluido.id].includes(parseInt(param.Obs_Observacion.split('|')[0] + '')));
                if (this.deudorTrancisionAnulado) {
                    this.preventMessage = `La solicitud fue actualizada Exitosamente al estado ${this.estado}, en fecha: ${moment(this.deudorTrancisionAnulado.Obs_FechaReg).format('DD/MM/YYYY hh:mm')}, por lo que ahora no será posible realizar acciones sobre esta solicitud`;
                    this.displayPreventMessage = true;
                }
            }
            this.id_estado = parseInt(this.par_estado_id + '');
            if (['null'].includes(this.id_poliza + '') && this.id_estado == this.solicitudService.estadoDesistido.id) {
                this.enableImprimir = true;
            }
        }
    }
}
