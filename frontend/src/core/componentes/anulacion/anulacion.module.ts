import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AnulacionComponent} from "./anulacion.component";
import {
  ButtonModule,
  DialogModule,
  DropdownModule, FieldsetModule,
  InputTextModule, MessageModule,
  MessagesModule, OverlayPanelModule, PanelModule, TabViewModule,
} from "primeng";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
      AnulacionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    DialogModule,
    InputTextModule,
    MessagesModule,
    MessageModule,
    FieldsetModule,
    DropdownModule,
    PanelModule,
    OverlayPanelModule,
    TabViewModule,
    ButtonModule
  ],

  exports: [
    AnulacionComponent,

    DropdownModule,
    InputTextModule,
    MessagesModule,
    MessageModule,
    FieldsetModule,
    DropdownModule,
    PanelModule,
    OverlayPanelModule,
    TabViewModule,
    ButtonModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AnulacionModule { }
