import * as moment from 'moment';

export class Util {

    calendario_es = {
        firstDayOfWeek: 0,
        dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
        dayNamesMin: ["Do","Lu","Ma","Mie","Ju","Vi","Sa"],
        monthNames: [ "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre" ],
        monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
        today: 'Hoy',
        clear: 'Limpiar',
        dateFormat: 'mm/dd/yy',
        weekHeader: 'Wk'
    };

    calendario_en = {
        firstDayOfWeek: 0,
        dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        dayNamesMin: ["Su","Mo","Tu","We","Th","Fr","Sa"],
        monthNames: [ "January","February","March","April","May","June","July","August","September","October","November","December" ],
        monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
        today: 'Today',
        clear: 'Clear',
        dateFormat: 'mm/dd/yy',
        weekHeader: 'Wk'
    };

    public isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    public ValidarComponentesInvisible(componentesInvisibles:any,id: any) {
        if(componentesInvisibles) {
            if (componentesInvisibles.find(params => params.codigo === id && params.estado === 'A')) {
                return true;
            } else {
                if (componentesInvisibles.find(params => params.codigo === id && params.estado === 'I')) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return true;
        }
    }

    public ValidarComponentesEditable(componentesInvisibles,id: any) {
        if(componentesInvisibles != null) {
            if (componentesInvisibles.find(params => params.codigo === id && params.estado === 'NE')) {
                return true;
            } else {
                return false;
            }
        }
    }

    public isNumber(n) {
        return !isNaN(parseFloat(n)) && !isNaN(n - 0)
    }

    public getEdad(dateString:string) {
        let hoy = new Date()
        let fechaNacimiento = new Date(dateString)
        let edad = hoy.getFullYear() - fechaNacimiento.getFullYear()
        let diferenciaMeses = hoy.getMonth() - fechaNacimiento.getMonth()
        if (
          diferenciaMeses < 0 ||
          (diferenciaMeses === 0 && hoy.getDate() < fechaNacimiento.getDate())
        ) {
          edad--
        }
        return edad
    }

    public formatoFecha = (param:string) => {
        return moment(param).add('hour',4).format('DD/MM/YYYY');
      }

      public formatoFechaQuery = (param:string) => {
          return moment(param).format('DDMMYYYY');
      }
}
