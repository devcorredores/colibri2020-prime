export const environment = {
  production: true,
  version: "1.0.0",
  URL: 'https://seguroscolibri.com:7000',

  // desgravamen

  SOCKET_ENDPOINT: 'https://seguroscolibri.com:7004',
  URL_API: 'https://seguroscolibri.com:7004/api-seguros',
  URL_PUBLIC_PDF: 'https://seguroscolibri.com:7004/pdfs',
  URL_PUBLIC_FILES: 'https://seguroscolibri.com:7004/files',
  URL_PUBLIC_VIEWER: 'https://seguroscolibri.com:7004/viewer',

  // masivos

  URL_API_MASIVOS: "https://seguroscolibri.com:7000/api-corredores-ecofuturo",
  URL_PUBLIC_PDF_MASIVOS: "https://seguroscolibri.com:7000/pdf",
  URL_PUBLIC_UPLOAD_MASIVOS: "https://seguroscolibri.com:7000/upload",
  URL_PUBLIC_FILES_MASIVOS: 'https://seguroscolibri.com:7000/files',
  URL_PUBLIC_VIEWER_MASIVOS: "https://seguroscolibri.com:7000/viewer",
  SOCKET_ENDPOINT_MASIVOS: 'https://seguroscolibri.com:7000',
};
