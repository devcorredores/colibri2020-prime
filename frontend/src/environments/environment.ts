// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  version: "1.0.0",
  URL: 'http://localhost:7001',

  // desgravamen

  URL_API: 'http://localhost:4005/api-seguros',
  URL_PUBLIC_PDF: 'http://localhost:4005/pdfs',
  URL_PUBLIC_FILES: 'http://localhost:4005/files',
  URL_PUBLIC_VIEWER: 'http://localhost:4005/viewer',
  SOCKET_ENDPOINT: 'http://localhost:4005',

  // masivos

  URL_API_CREDITICIOS: "http://localhost:7001/api-corredores-ecofuturo",
  URL_API_MASIVOS: "http://localhost:7001/api-corredores-ecofuturo",
  URL_PUBLIC_PDF_MASIVOS: "http://localhost:7001/pdf",
  URL_PUBLIC_UPLOAD_MASIVOS: "http://localhost:7001/upload",
  URL_PUBLIC_FILES_MASIVOS: 'http://localhost:7001/files',
  URL_PUBLIC_VIEWER_MASIVOS: "http://localhost:7001/viewer",
  SOCKET_ENDPOINT_MASIVOS: 'http://localhost:7001',
};

/*

  SOCKET_ENDPOINT: 'https://corredoresecofuturo.com.bo:4004',
  URL_API: 'https://corredoresecofuturo.com.bo:4004/api-seguros',
  URL_PUBLIC_PDF: 'https://corredoresecofuturo.com.bo:4004/pdfs',
  URL_PUBLIC_FILES: 'http://corredoresecofuturo.com.bo:4004/files',
  URL_PUBLIC_VIEWER: 'https://corredoresecofuturo.com.bo:4004/viewer'

  SOCKET_ENDPOINT: 'http://localhost:4005',
  URL_API: 'http://localhost:4005/api-seguros',
  URL_PUBLIC_PDF: 'http://localhost:4005/pdfs',
  URL_PUBLIC_FILES: 'http://localhost:4005/files',
  URL_PUBLIC_VIEWER: 'http://localhost:4005/viewer',

*/

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 * 
 * 
 * 
 * 
    "bn-ng-idle": "^1.0.1",
    "file-saver": "^2.0.5",
    "html-to-pdfmake": "^2.1.7",
    "jspdf": "^2.3.1",
    "pdfmake": "^0.1.71",
    "socket.io-client": "^4.1.3",
    "xlsx": "^0.17.0",
    "ngx-toastr": "^13.1.0",
    "rxjs": "~6.5.4",
    "sweetalert2": "^10.10.4",
    "tslib": "^1.10.0",
    "w3-css": "^4.1.0",
    "zone.js": "~0.10.2"
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
