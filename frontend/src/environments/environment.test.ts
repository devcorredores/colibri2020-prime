// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  version: "1.0.0",
  URL: 'https://seguroscolibri.com:4000',

  // desgravamen

  URL_API: 'https://seguroscolibri.com:4004/api-seguros',
  URL_PUBLIC_PDF: 'https://seguroscolibri.com:4004/pdfs',
  URL_PUBLIC_FILES: 'https://seguroscolibri.com:4004/files',
  URL_PUBLIC_VIEWER: 'https://seguroscolibri.com:4004/viewer',
  SOCKET_ENDPOINT: 'https://seguroscolibri.com:4004',

  // masivos

  URL_API_MASIVOS: "https://seguroscolibri.com:4000/api-corredores-ecofuturo",
  URL_PUBLIC_PDF_MASIVOS: "https://seguroscolibri.com:4000/pdf",
  URL_PUBLIC_UPLOAD_MASIVOS: "https://seguroscolibri.com:4000/upload",
  URL_PUBLIC_FILES_MASIVOS: 'https://seguroscolibri.com:4000/files',
  URL_PUBLIC_VIEWER_MASIVOS: "https://seguroscolibri.com:4000/viewer",
  SOCKET_ENDPOINT_MASIVOS: 'https://seguroscolibri.com:4000',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
