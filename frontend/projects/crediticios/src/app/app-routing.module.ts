import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AltaEcoAccidentesComponent} from "./eco-accidentes/alta-eco-accidentes/alta-eco-accidentes.component";
import {GestionEcoAccidentesComponent} from "./eco-accidentes/gestion-eco-accidentes/gestion-eco-accidentes.component";
import {GestionDesgravamenComponent} from "./desgravamen/gestion-desgravamen/gestion-desgravamen.component";
import {AnulacionComponent} from "../../../../src/core/componentes/anulacion/anulacion.component";


const routes: Routes = [
  { path: 'AltaEcoAccidente', component:  AltaEcoAccidentesComponent},
  { path: 'AltaEcoResguardo', component:  AltaEcoAccidentesComponent},
  { path: 'AnularEcoAccidente', component:  AnulacionComponent},
  { path: 'GestionSolicitudesEcoAccidente', component:  GestionEcoAccidentesComponent},
  { path: 'GestionDesgravamen', component:  GestionDesgravamenComponent},
  { path: 'AnularDesgravamen', component:  AnulacionComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
