import {Component, OnInit, ViewChild} from '@angular/core';
import {Componente} from "../../../../../../src/core/modelos/componente";
import {Parametro, PolizasDesgravamen} from "../../../../../../src/core/modelos/parametro";
import {Instancia_poliza} from "../../../../../../src/core/modelos/instancia_poliza";
import {Poliza} from "../../../../../../src/core/modelos/poliza";
import {Asegurado} from "../../../../../../src/core/modelos/asegurado";
import {MessageService, SelectItem, SortEvent} from "primeng";
import {Util} from "../../../../../../src/helpers/util";
import {ActivatedRoute, Router} from "@angular/router";
import {BreadcrumbService} from "../../../../../../src/core/servicios/breadcrumb.service";
import {ParametrosService} from "../../../../../../src/core/servicios/parametro.service";
import {InstanciaPolizaService} from "../../../../../../src/core/servicios/instancia-poliza.service";
import {MenuService} from "../../../../../../src/core/servicios/menu.service";
import {ReporteService} from "../../../../../../src/core/servicios/reporte.service";
import {PersonaService} from "../../../../../../src/core/servicios/persona.service";
import {SolicitudService} from "../../../../../../src/core/servicios/solicitud.service";
import {SessionStorageService} from "../../../../../../src/core/servicios/sessionStorage.service";
import {PlanPagoService} from "../../../../../../src/core/servicios/plan-pago.service";
import {FilterUtils} from "primeng/utils";
import {Plan_pago} from "../../../../../../src/core/modelos/plan_pago";
import * as xlsx from 'xlsx';
import * as FileSaver from 'file-saver';
import {SegSolicitudes} from "../../../../../../src/core/modelos/desgravamen/seg_solicitudes";
import {Usuario} from "../../../../../../src/core/modelos/usuario";
import {ActualizarSolicitudComponent} from "../../../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component";
import {SegDeudores} from "../../../../../../src/core/modelos/desgravamen/seg_deudores";
declare var $:any;

@Component({
  selector: 'app-gestion-desgravamen',
  templateUrl: './gestion-desgravamen.component.html',
  styleUrls: ['./gestion-desgravamen.component.css']
})
export class GestionDesgravamenComponent implements OnInit {


  color = 'primary';
  mode = 'indeterminate';
  value = 1;
  componentesInvisibles: Componente[];
  componentesEditables: Componente[];
  ruta: string;
  parametros: Parametro[] = [];
  instancias_polizas: Instancia_poliza[] = [];
  instancias: any[] = [];
  //poliza = new Poliza();
  segSolicitudes: SegSolicitudes[] = [];
  segDeudores: SegDeudores[] = [];
  cols: any;
  colsAseguradoraLbcSuscriptor: any;
  colsAseguradoraAlianza: any;
  objeto: {
    agencia_required: boolean,
    sucursal_required: boolean,
    estado: string,
    poliza: string,
    agencia: string,
    sucursal: string,
    usuario_login: string,
    persona_primer_apellido: string,
    persona_segundo_apellido: string,
    persona_primer_nombre: string,
    persona_doc_id: string,
    persona_doc_id_ext: string,
    id_poliza: number,
    campo_fecha: string,
    fecha_min: Date,
    fecha_max: Date,
    tipo_cobertura: string,
    tipo_seguro: string,
    id_deu: string,
    nro_documento: string,
    tipo_credito: string,
    usuarioLogin:any
    first:number,
    rows:number
  } = {
    agencia_required: true,
    sucursal_required: true,
    estado: null,
    poliza: null,
    agencia: null,
    sucursal: null,
    usuario_login: '',
    persona_primer_apellido: '',
    persona_segundo_apellido: '',
    persona_primer_nombre: '',
    persona_doc_id: '',
    persona_doc_id_ext: '',
    id_poliza: null,
    campo_fecha: null,
    fecha_min: null,
    fecha_max: null,
    tipo_cobertura: '',
    tipo_seguro: '',
    id_deu: '',
    nro_documento: '',
    tipo_credito: '',
    usuarioLogin:null,
    first:0,
    rows:10
  };
  solicituddes: Asegurado[];
  showFiltros: boolean = true;
  FiltrosFechas: SelectItem[] = [];
  ProcedenciaCI: SelectItem[] = [];
  Agencias: SelectItem[] = [];
  paramAgencias: Parametro[] = [];
  paramSucursales: Parametro[] = [];
  Sucursales: SelectItem[] = [];
  Estados: SelectItem[] = [];
  EstadosAll: any[] = [];
  Polizas: SelectItem[] = [];
  EstadosFiltered: SelectItem[] = [];
  TiposCobertura: SelectItem[] = [];
  TiposSeguro: SelectItem[] = [];
  id_poliza;
  parametrosRuteo: any;
  es: any;
  util = new Util();
  usuarioLogin: Usuario;
  btnEditar: boolean = false;
  btnSoporte: boolean = false;
  btnVer1: boolean = false;
  btnVer2: boolean = false;
  btnEmitir: boolean = false;
  index_instancia_poliza: number;
  exportColumns: any[];
  @ViewChild('componenteActualizarSolicitud', { static: false }) private componenteActualizarSolicitud: ActualizarSolicitudComponent;
  disableFieldUsuario: boolean = false;
  disableFieldAgencia: boolean = false;
  disableFieldSucursal: boolean = false;
  disableTipoCobertura: boolean = false;
  disableTipoSeguro: boolean = false;
  displayExedioResultados: boolean = false;
  PanelFiltrosColapsed: boolean = false;
  showMoreResultsMessage: boolean = false;
  esAseguradora: boolean = false;
  esAseguradoraAlianza: boolean = false;
  esAseguradoraLbcSuscriptor: boolean = false;
  edadClass: string;
  rowsPerPage: number = 0;
  numRecords: number = 0;
  totalRows:number = 0;

  constructor(
      private params: ActivatedRoute, private breadcrumbService: BreadcrumbService,
      private parametrosService: ParametrosService,
      private instanciaPolizaService: InstanciaPolizaService,
      private router: Router,
      private menuService: MenuService,
      private reporteService: ReporteService,
      private service: MessageService,
      private personaService: PersonaService,
      public solicitudService: SolicitudService,
      public sessionStorageService: SessionStorageService,
      private planPagoService: PlanPagoService
  ) {
    let userInfo:any = this.sessionStorageService.getItemSync('userInfo');
    let parametros:any = this.sessionStorageService.getItemSync('parametros');
    this.usuarioLogin = userInfo;
    this.parametrosRuteo = parametros;
    this.componentesInvisibles = this.parametrosRuteo.ComponentesInvisibles ? JSON.parse(this.parametrosRuteo.ComponentesInvisibles) : this.parametrosRuteo.ComponentesInvisibles;
    this.ruta = this.parametrosRuteo.ruta;
    this.breadcrumbService.setItems([
      { label: this.ruta }
    ]);
    this.cols = [
      { field: 'Nro', header: 'Nro', width: '5%' },
      { field: 'Nro_Sol', header: 'Nro. Sol.', width: '5%' },
      { field: 'id_deu', header: 'Id Deudor', width: '5%' },
      { field: 'Deu_Poliza', header: 'Poliza', width: '5%' },
      { field: 'ci', header: 'CI', width: '7%' },
      { field: 'Sucursal', header: 'Sucursal', width: '8%' },
      { field: 'Agencia', header: 'Agencia', width: '10%' },
      { field: 'Tipo_Cobertura', header: 'Tipo Cobertura', width: '6%' },
      { field: 'Tipo_Seguro', header: 'Tipo Cartera', width: '6%' },
      { field: 'Estado', header: 'Estado Sol.', width: '6%' },
      { field: 'Incluido', header: 'Incluido', width: '5%' },
      { field: 'Fecha_Solicitud', header: 'Fecha Solicitud', width: '9%' },
      { field: 'Fecha_Resolucion', header: 'Fecha Resolucion', width: '9%' },
      { field: 'Asegurado', header: 'Asegurado', width: '15%' },
      { field: 'usuario', header: 'usuario', width: '8%' },
      { field: null, header: 'Acciones', width: '10%' }
    ];

    this.colsAseguradoraLbcSuscriptor = [
      { field: 'Nro_Sol', header: 'Nro. Sol.', width: '5%' },
      { field: 'id_deu', header: 'Id Deudor', width: '5%' },
      { field: 'Fecha_Solicitud', header: 'Fecha Solicitud', width: '6%' },
      { field: 'Fecha_Resolucion', header: 'Fecha Resolucion', width: '6%' },
      { field: 'Deu_Poliza', header: 'Poliza', width: '5%' },
      { field: 'ci', header: 'CI', width: '7%' },
      { field: 'Asegurado', header: 'Asegurado', width: '6%' },
      { field: 'Edad', header: 'Edad', width: '4%' },
      { field: 'Sol_MonedaSol', header: 'Moneda', width: '5%' },
      { field: 'Sol_MontoSol', header: 'Monto Solicitado', width: '5%' },
      { field: 'Estado', header: 'Estado Sol.', width: '6%' },
      { field: 'Incluido', header: 'Incluido', width: '4%' },
      { field: 'Deu_Imc', header: 'IMC', width: '7%' },
      { field: 'Deu_MontoActAcumVerifSum', header: 'Saldo', width: '6%' },
      { field: 'Deu_MontoSolAcumVerifSum', header: 'Monto a Evaluar', width: '8%' },
      { field: 'Deu_RespPositivas', header: 'Resp. Positivas', width: '6%' },
      { field: 'usuario', header: 'usuario', width: '6%' },
      { field: null, header: 'Acciones', width: '6%' },
    ];

    this.colsAseguradoraAlianza = [
      { field: 'Nro_Sol', header: 'Nro. Sol.', width: '5%' },
      { field: 'id_deu', header: 'Id Deudor', width: '5%' },
      { field: 'Fecha_Solicitud', header: 'Fecha Solicitud', width: '6%' },
      { field: 'Fecha_Resolucion', header: 'Fecha Resolucion', width: '6%' },
      { field: 'Deu_Poliza', header: 'Poliza', width: '5%' },
      { field: 'ci', header: 'CI', width: '7%' },
      { field: 'Asegurado', header: 'Asegurado', width: '6%' },
      { field: 'Edad', header: 'Edad', width: '4%' },
      { field: 'Sol_MonedaSol', header: 'Moneda', width: '5%' },
      { field: 'Sol_MontoSol', header: 'Monto Solicitado', width: '5%' },
      { field: 'Estado', header: 'Estado Sol.', width: '6%' },
      { field: 'Incluido', header: 'Incluido', width: '4%' },
      { field: 'Deu_FactorK', header: 'Factor K', width: '7%' },
      { field: 'Deu_MontoActAcumVerifSum', header: 'Saldo', width: '6%' },
      { field: 'Deu_MontoSolAcumVerifSum', header: 'Monto a Evaluar', width: '8%' },
      { field: 'Deu_RespPositivas', header: 'Resp. Positivas', width: '6%' },
      { field: 'usuario', header: 'usuario', width: '6%' },
      { field: null, header: 'Acciones', width: '6%' },
    ];

    FilterUtils['custom'] = (value, filter): boolean => {
      if (filter === undefined || filter === null || filter.trim() === '') {
        return true;
      }

      if (value === undefined || value === null) {
        return false;
      }

      return parseInt(filter) > value;
    }
  }

  ValidarComponentesInvisible(id: any) {
    if (this.componentesInvisibles.find(params => params.codigo === id && params.estado === 'I')) {
      return false;
    } else {
      return true;
    }
  }

  getIconEditAdd(instancia) {
    if (instancia.Estado) {
      if (this.usuarioLogin.usuarioRoles.find(param => [20,21,22,23,24,25].includes(parseInt(param.id+'')))) {
        return 'remove-red-eye';
      } else {
        return ['A','C'].includes(instancia.Estado.substring(0,1)) ? 'remove-red-eye' : 'edit'
      }
    }
  };

  getWordEditAdd(instancia) {
    if (instancia.Estado) {
      if (this.usuarioLogin.usuarioRoles.find(param => [20,21,22,23,24,25].includes(parseInt(param.id+'')))) {
        return 'Ver';
      } else {
        return ['A','C'].includes(instancia.Estado.substring(0,1)) ? 'Ver' : 'Editar'
      }
    }
  }
  showBtnImprimir(seg_deudor:SegDeudores) {
    let estadosAprobados = this.EstadosAll.filter(param => ['A'].includes(param.Est_Codigo.substring(0,1)));
    if(seg_deudor.id_documento_version &&
        estadosAprobados.find(param => param.Est_Codigo.substring(0,1) == seg_deudor.deu_solicitud.Sol_EstadoSol.substring(0,1)) &&
        seg_deudor.Deu_Incluido.toUpperCase() == 'S'
    ) {
      return true;
    } else {
      return false;
    }
  }
  setEdadClass(instancia) {
    return parseInt(instancia['Edad']) < 71 ? 'resp-info' : 'resp-danger';
  }

  ngAfterViewChecked() {
    /*if (this.solicitudService.asegurado.instancia_poliza.id_estado + '' === '59') {
      if (this.poliza.instancia_polizas.length > 0 && (this.index_instancia_poliza || this.index_instancia_poliza===0)) {
        this.poliza.instancia_polizas[this.index_instancia_poliza].id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
        this.instancias[this.index_instancia_poliza].id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
        this.instancias[this.index_instancia_poliza]['Estado'] = this.devuelveValorParametro(this.instancias[this.index_instancia_poliza].id_estado);
      }
    }*/
  }

  ValidarComponentesEditable(id: any) {
    if (this.componentesEditables.find(params => params.codigo === id && params.estado === 'NE')) {
      return true;
    } else {
      return false;
    }
  }

  async ngOnInit() {
    this.solicitudService.constructComponent(() => {
      this.breadcrumbService.setItems([
        {label: this.solicitudService.ruta}
      ]);
      this.objeto = this.solicitudService.parametrosRuteo.parametro_ruteo && this.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? this.solicitudService.parametrosRuteo.parametro_ruteo.filtros : this.objeto;
      this.objeto.id_poliza = parseInt(this.solicitudService.parametrosRuteo.parametro_vista+'') ? parseInt(this.solicitudService.parametrosRuteo.parametro_vista+'') : this.solicitudService.id_poliza;
      this.solicitudService.product = this.solicitudService.ruta;
      this.solicitudService.isInAltaSolicitud = false;
      this.solicitudService.isLoadingAgain = false;
      this.GetAllParametrosByIdDiccionarios(() => {
        if (this.parametrosRuteo.parametro_ruteo !== null) {
          this.solicituddes = this.parametrosRuteo.parametro_ruteo;
          this.listaAllByIds();
          this.showFiltros = false;
        } else {
          this.solicitudService.isLoadingAgain = false;
        }

        if (this.parametrosRuteo.parametro_vista !== null) {
          this.id_poliza = this.parametrosRuteo.parametro_vista;
        }

        if (this.usuarioLogin) {
          //this.disableFieldUsuario = true;
          if (this.usuarioLogin.usuario_banco) {
            this.disableFieldSucursal = true;
            this.disableFieldAgencia = true;
          }
          // 5   Oficial de Credito
          if (this.usuarioLogin.usuarioRoles.find(param => param.id == 5)) {
            this.objeto.usuario_login = this.usuarioLogin.usuario_login;
            this.objeto.sucursal = this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_sucursal+'' : '';
            //this.objeto.agencia = this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_oficina+'' : '';
            this.disableFieldUsuario = true;
            this.disableFieldSucursal = true;
            this.disableFieldAgencia = true;
            this.esAseguradora = false;
            this.esAseguradoraLbcSuscriptor = false;
            this.esAseguradoraAlianza = false;
          }
          // 20	aseguradora	Aseguradora Alianza
          if (this.usuarioLogin.usuarioRoles.find(param => param.id == 20)) {
            this.disableFieldUsuario = false;
            this.disableFieldSucursal = false;
            this.disableFieldAgencia = false;
            this.disableTipoSeguro = false;
            this.disableTipoCobertura = false;
            this.esAseguradora = true;
            this.esAseguradoraAlianza = true;
            this.esAseguradoraLbcSuscriptor = false;
            // this.objeto.tipo_seguro = this.TiposSeguro[1].value;
            // this.objeto.tipo_cobertura = this.TiposCobertura[2].value;
          }
          // 21	aseguradora_no_licitada	LBC
          if (this.usuarioLogin.usuarioRoles.find(param => param.id == 21)) {
            this.disableFieldUsuario = false;
            this.disableFieldSucursal = false;
            this.disableFieldAgencia = false;
            this.disableTipoSeguro = false;
            this.disableTipoCobertura = false;
            this.esAseguradora = true;
            this.esAseguradoraLbcSuscriptor = true;
            this.esAseguradoraAlianza = false;
          }
          // 22	corredora_desgravamen	Corredora Desgravamen
          if (this.usuarioLogin.usuarioRoles.find(param => param.id == 22)) {
            this.disableFieldUsuario = false;
            this.disableFieldSucursal = false;
            this.disableFieldAgencia = false;
            this.disableTipoSeguro = false;
            this.disableTipoCobertura = false;
            this.esAseguradora = false;
            this.esAseguradoraLbcSuscriptor = false;
            this.esAseguradoraAlianza = false;
          }
          // 23	supervisor_ofcredito	Supervisor General
          if (this.usuarioLogin.usuarioRoles.find(param => param.id == 23)) {
            this.disableFieldUsuario = false;
            this.disableFieldSucursal = false;
            this.disableFieldAgencia = false;
            this.esAseguradora = false;
            this.esAseguradoraLbcSuscriptor = false;
            this.esAseguradoraAlianza = false;
          }
          // 24	supervisor_ofcredito_sucursal	Supervisor Sucursal
          if (this.usuarioLogin.usuarioRoles.find(param => param.id == 24)) {
            this.objeto.sucursal = this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_sucursal+'' : '';
            this.disableFieldUsuario = false;
            this.disableFieldSucursal = true;
            this.disableFieldAgencia = false;
            this.esAseguradora = false;
            this.esAseguradoraLbcSuscriptor = false;
            this.esAseguradoraAlianza = false;
          }
          // 25	supervisor_ofcredito_agencia	Supervisor Agencia
          if (this.usuarioLogin.usuarioRoles.find(param => param.id == 25)) {
            this.objeto.sucursal = this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_sucursal+'' : '';
            this.objeto.agencia = this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_oficina+'' : '';
            this.disableFieldUsuario = false;
            this.disableFieldSucursal = true;
            this.disableFieldAgencia = true;
            this.esAseguradora = false;
            this.esAseguradoraLbcSuscriptor = false;
            this.esAseguradoraAlianza = false;
          }
          // 26	aseguradora LBC Aseguradora LBC Suscriptor
          if (this.usuarioLogin.usuarioRoles.find(param => param.id == 26)) {
            this.disableFieldUsuario = false;
            this.disableFieldSucursal = false;
            this.disableFieldAgencia = false;
            this.disableTipoSeguro = false;
            this.disableTipoCobertura = false;
            this.esAseguradora = true;
            this.esAseguradoraLbcSuscriptor = true;
            this.esAseguradoraAlianza = false;
          }
        }
      });
    });
  }

  listaAll(dt: any, callback:any = null) {
    if (dt) {
      dt.reset();
    }
    setTimeout(() => {
      if ($(window).width() < 720) {
        $('.ui-table').css('overflow','scroll');
        $('.ui-table table').css('width','400%');
      }
    },2000);
    this.instancias = [];
    if (!this.objeto.agencia) {
      this.objeto.agencia_required = false;
    }
    if (!this.objeto.sucursal_required) {
      this.objeto.sucursal_required = false;
    }
    this.solicitudService.isLoadingAgain = true;
    if (this.objeto.campo_fecha && this.objeto.fecha_max) {
      this.objeto.fecha_max.setDate(this.objeto.fecha_max.getDate() + 1);
      this.objeto.fecha_max.setMinutes(this.objeto.fecha_max.getMinutes() - 1);
    }
    this.objeto.usuarioLogin = this.usuarioLogin;
    if (this.totalRows && this.totalRows == this.objeto.first) {
      this.objeto.first = this.objeto.first-this.objeto.rows
    }
    this.instanciaPolizaService.GetAllByParametrosDesgravamen(this.objeto).subscribe(res => {
      let response = res as { status: string, message: string, data: SegDeudores[], recordsTotal:number, recordsOffset:number, recordsFiltered:number};
      this.totalRows = response.recordsTotal;
      this.objeto.first = response.recordsOffset;
      this.objeto.rows = response.recordsFiltered;
      this.segDeudores = response.data;
      if (this.segDeudores.length >= 300) {
        this.showMoreResultsMessage = true;
      } else {
        this.showMoreResultsMessage = false;
      }
      this.PanelFiltrosColapsed = false;
      if (this.segDeudores && this.segDeudores.length) {
        if (this.segDeudores.length === 0) {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
        } else if (this.segDeudores.length >= 300 && !this.usuarioLogin.usuarioRoles.find(param => param.id == 5)) {
          this.displayExedioResultados = true;
        }
        this.segDeudores = this.getCars(this.segDeudores);
        let lastEmptyRows = this.totalRows%this.objeto.rows;
        let lastFilledRows = this.objeto.rows-lastEmptyRows;
        let rowsAfter = this.totalRows-lastEmptyRows;
        for (let i = 0; i < rowsAfter; i++) {
          this.segDeudores.push(new SegDeudores());
          this.segDeudores[i].Nro = i+(this.objeto.first+1);
        }
        this.rowsPerPage = this.objeto.first+this.objeto.rows;
        if (this.totalRows-this.objeto.first == lastFilledRows) {
          this.numRecords = lastFilledRows
        } else {
          this.numRecords = this.objeto.rows
        }
      } else {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
      }
      this.solicitudService.isLoadingAgain = false;
      if (typeof callback == 'function') {
        callback();
      }
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  listaAllByIds() {
    this.instanciaPolizaService.GetAllByIdsDesgravamen({ ids: this.solicituddes }).subscribe(res => {
      let response = res as { status: string, message: string, data: SegDeudores[] };
      this.segDeudores = response.data;
      if (this.segDeudores !== undefined) {
        if (this.segDeudores.length === 0) {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
        }
        this.segDeudores = this.getCars(this.segDeudores);
      } else {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
      }
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  devuelveValorParametro(id: any) {
    let parametro = this.parametros.find(parametro => parametro.id === id);
    if (parametro) {
      return parametro.parametro_descripcion;
    } else {
      return '';
    }
  }

  devuelveValorParametroByCod(id: any) {
    let parametro = this.parametros.find(parametro => parametro.parametro_cod === id);
    if (parametro) {
      return parametro.parametro_descripcion;
    } else {
      return '';
    }
  }

  devuelveValorAgenciaSucursal(agencia: any) {
    if (agencia) {
      let parametro = this.parametros.find(parametro => parametro.parametro_cod === agencia.valor);
      if (parametro) {
        return parametro.parametro_descripcion;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  async GetAllParametrosByIdDiccionarios(callback:any) {
    //ids ponemos los id de los diccionarios que necesitemos
    await this.parametrosService.GetAllEstadosDesgravamen().subscribe(res => {
      let response = res as { status: string, message: string, data: any };
      this.Estados.push({ label: "Seleccione Estado", value: null });
      this.EstadosFiltered.push({ label: "Seleccione Estado", value: null });
      let estados = response.data.estados;
      this.EstadosAll = response.data.estadosAll;
      if(estados) {
        for (let i = 0; i < estados.length; i++) {
          let estado = estados[i];
          if(this.usuarioLogin.usuarioRoles.find(param => param.id == 5)) {
            // 5    Oficial de Credito
            this.Estados.push({ label: estado.Est_Grupo, value: estado.Est_Codigo });
          } else if(this.usuarioLogin.usuarioRoles.find(param => param.id == 20)) {
            // 20	aseguradora_no_licitada	Aseguradora No Licitada Alianza
            if (!['P'].includes(estado.Est_Codigo && estado.Est_Codigo.toUpperCase())) {
              this.Estados.push({ label: estado.Est_Grupo, value: estado.Est_Codigo });
            }
          } else if(this.usuarioLogin.usuarioRoles.find(param => param.id == 21)) {
            // 21	aseguradora_no_licitada	Aseguradora No Licitada Boliviana Ciacruz
            if (!['P'].includes(estado.Est_Codigo && estado.Est_Codigo.toUpperCase())) {
              this.Estados.push({ label: estado.Est_Grupo, value: estado.Est_Codigo });
            }
          } else if(this.usuarioLogin.usuarioRoles.find(param => param.id == 22)) {
            // 22	corredora_desgravamen	Corredora Desgravamen
            this.Estados.push({ label: estado.Est_Grupo, value: estado.Est_Codigo });
          } else if(this.usuarioLogin.usuarioRoles.find(param => param.id == 23)) {
            // 23	supervisor_ofcredito	Supervisor General
            this.Estados.push({ label: estado.Est_Grupo, value: estado.Est_Codigo });
          } else if(this.usuarioLogin.usuarioRoles.find(param => param.id == 24)) {
            // 24	supervisor_ofcredito_sucursal	Supervisor Sucursal
            this.Estados.push({ label: estado.Est_Grupo, value: estado.Est_Codigo });
          } else if(this.usuarioLogin.usuarioRoles.find(param => param.id == 25)) {
            // 25	supervisor_ofcredito_agencia	Supervisor Agencia
            this.Estados.push({ label: estado.Est_Grupo, value: estado.Est_Codigo });
          } else if(this.usuarioLogin.usuarioRoles.find(param => param.id == 26)) {
            // 26	supervisor_ofcredito	Supervisor General Suscriptor
            if (['A','C','D','O','R'].includes(estado.Est_Codigo && estado.Est_Codigo.toUpperCase().substring(0,1))) {
              this.Estados.push({ label: estado.Est_Grupo, value: estado.Est_Codigo });
            }
          }
        }
      }
    });
    await this.parametrosService.GetAllPolizasDesgravamen().subscribe(res => {
      let response = res as { status: string, message: string, data: PolizasDesgravamen[] };
      this.Polizas.push({ label: "Seleccione Poliza", value: null });
      if(response.data) {
        for (let i = 0; i < response.data.length; i++) {
          let polizaDesgravamen = response.data[i];
          // 5    Oficial de Credito
          if(this.usuarioLogin.usuarioRoles.find(param => param.id == 5)) {
            // if (['1/2018','2/2018','80268'].includes(polizaDesgravamen.Deu_Poliza)){
            this.Polizas.push({ label: polizaDesgravamen.Deu_Poliza, value: polizaDesgravamen.Deu_Poliza });
            // }
          }
          // 20	aseguradora_no_licitada	Aseguradora No Licitada Alianza
          if(this.usuarioLogin.usuarioRoles.find(param => param.id == 20)) {
            if (['1/2018','2/2018','80268'].includes(polizaDesgravamen.Deu_Poliza)){
              this.Polizas.push({ label: polizaDesgravamen.Deu_Poliza, value: polizaDesgravamen.Deu_Poliza });
            }
          }
          // 21	aseguradora_no_licitada	Aseguradora No Licitada Boliviana Ciacruz
          if(this.usuarioLogin.usuarioRoles.find(param => param.id == 21)) {
            if (['20002','20007','20001','20006'].includes(polizaDesgravamen.Deu_Poliza)){
              this.Polizas.push({ label: polizaDesgravamen.Deu_Poliza, value: polizaDesgravamen.Deu_Poliza });
            }
          }
          // 22	corredora_desgravamen	Corredora Desgravamen
          if(this.usuarioLogin.usuarioRoles.find(param => param.id == 22)) {
            if (['1/2018','2/2018','80268','20002','20007','20001','20006'].includes(polizaDesgravamen.Deu_Poliza)){
              this.Polizas.push({ label: polizaDesgravamen.Deu_Poliza, value: polizaDesgravamen.Deu_Poliza });
            }
          }
          // 23	supervisor_ofcredito	Supervisor General
          if(this.usuarioLogin.usuarioRoles.find(param => param.id == 23)) {
            // if (['1/2018','2/2018','3/2018'].includes(polizaDesgravamen.Deu_Poliza)){
            this.Polizas.push({ label: polizaDesgravamen.Deu_Poliza, value: polizaDesgravamen.Deu_Poliza });
            // }
          }
          // 24	supervisor_ofcredito_sucursal	Supervisor Sucursal
          if(this.usuarioLogin.usuarioRoles.find(param => param.id == 24)) {
            // if (['1/2018','2/2018','3/2018'].includes(polizaDesgravamen.Deu_Poliza)){
            this.Polizas.push({ label: polizaDesgravamen.Deu_Poliza, value: polizaDesgravamen.Deu_Poliza });
            // }
          }
          // 25	supervisor_ofcredito_agencia	Supervisor Agencia
          if(this.usuarioLogin.usuarioRoles.find(param => param.id == 25)) {
            // if (['1/2018','2/2018','3/2018'].includes(polizaDesgravamen.Deu_Poliza)){
            this.Polizas.push({ label: polizaDesgravamen.Deu_Poliza, value: polizaDesgravamen.Deu_Poliza });
            // }
          }
          // 26	aseguradora_no_licitada	Aseguradora No Licitada Boliviana Ciacruz Suscriptor
          if(this.usuarioLogin.usuarioRoles.find(param => param.id == 26)) {
            // if (['1/2018','2/2018','3/2018'].includes(polizaDesgravamen.Deu_Poliza)){
            this.Polizas.push({ label: polizaDesgravamen.Deu_Poliza, value: polizaDesgravamen.Deu_Poliza });
            // }
          }
        }
      }
    });

    let ids = [11, 1, 18, 38, 40, 54,62,63,64];
    await this.parametrosService.GetAllParametrosByIdDiccionarios(ids).subscribe(res => {
      let response = res as { status: string, message: string, data: Parametro[] };
      this.parametros = response.data;
      this.ProcedenciaCI.push({ label: "Seleccione Procedencia", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "18").forEach(element => {
        this.ProcedenciaCI.push({ label: element.parametro_descripcion, value: element.parametro_abreviacion });
      });

      this.FiltrosFechas.push({ label: "Seleccione un tipo de fecha", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "64").forEach(element => {
        this.FiltrosFechas.push({ label: element.parametro_descripcion, value: element.parametro_cod })});

      this.Agencias.push({ label: "Seleccione Agencia", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "40").forEach(element => {
        this.Agencias.push({ label: element.parametro_descripcion, value: element.parametro_cod });
        this.paramAgencias.push(element);
      });
      this.Sucursales.push({ label: "Seleccione Sucursal", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "38").forEach(element => {
        this.Sucursales.push({ label: element.parametro_descripcion, value: element.parametro_cod });
        this.paramSucursales.push(element);
      });

      this.TiposCobertura.push({ label: "Seleccione Tipo Cobertura", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "62").forEach(element => {
        this.TiposCobertura.push({ label: element.parametro_descripcion, value: element.parametro_cod });
      });
      this.TiposSeguro.push({ label: "Seleccione Tipo Cartera", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "63").forEach(element => {
        this.TiposSeguro.push({ label: element.parametro_descripcion, value: element.parametro_cod });
      });
      if(this.usuarioLogin) {
        if (this.usuarioLogin.usuario_banco) {
        //   if (this.usuarioLogin.usuario_banco.us_sucursal) {
        //     this.objeto.sucursal = this.usuarioLogin.usuario_banco.us_sucursal + '';
        //   }
        //   if (this.usuarioLogin.usuario_banco.us_oficina) {
        //     this.objeto.agencia = this.usuarioLogin.usuario_banco.us_oficina + '';
        //   }
        }
      } else {
        this.router.navigate(['login']);
      }
      if (typeof callback == 'function') {
        callback();
      }
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  listaAllById() {

  }

  EditarPoliza(id: any) {
    this.btnEditar = true;
    this.btnSoporte = true;
    this.btnVer1 = true;
    sessionStorage.setItem("paramsDeleted", '');
    if (this.id_poliza === '6') {
      this.menuService.activaRuteoMenu('46', 6, {id_instancia_poliza:id}, 'AltaEcoVida', this.solicitudService.componentesInvisibles);
    }
    if (this.id_poliza === '7') {
      this.menuService.activaRuteoMenu('49', 7, {id_instancia_poliza:id}, 'AltaEcoProteccion', this.solicitudService.componentesInvisibles);
    }
    //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);
  }

  VerPoliza(id: any) {
    sessionStorage.setItem("paramsDeleted", '');
    this.btnVer2 = true;
    if (this.id_poliza + '' === '6') {
      this.menuService.activaRuteoMenu('52', 6, {id_instancia_poliza:id}, 'AltaEcoVida', this.solicitudService.componentesInvisibles);
    }
    if (this.id_poliza + '' === '7') {
      this.menuService.activaRuteoMenu('53', 7, {id_instancia_poliza:id}, 'AltaEcoProteccion', this.solicitudService.componentesInvisibles);
    }
    //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);
  }

  ValidarEstados(id_estado: any, estados: any[]) {
    if (estados.find(param => param == '*')) {
      return true;
    } else {
      if (estados.includes(parseInt(id_estado + ''))) {
        return true;
      } else {
        return false;
      }
    }
  }
  verifyRol(estadosAvailable:any[], rolsAvailable:any[], instanciaPoliza:Instancia_poliza) {
    let includesRol = false;
    if(!this.usuarioLogin) {
      this.usuarioLogin = this.sessionStorageService.getItemSync('userInfo') as Usuario;
    }
    if (this.usuarioLogin) {
      for (let i = 0; i < this.usuarioLogin.usuarioRoles.length; i++) {
        let rol = this.usuarioLogin.usuarioRoles[i];
        if (rolsAvailable.find(param => param == '*')) {
          includesRol = true;
          break;
        } else {
          if (rolsAvailable.includes(parseInt(rol.id+''))) {
            includesRol = true;
          }
        }
      }
      if (this.ValidarEstados(instanciaPoliza.id_estado,estadosAvailable) && includesRol) {
        return true;
      }
      return false;
    } else {
      this.router.navigate(['login']);
    }
  }

  ValidarPlanPago() {
    if (this.solicitudService.asegurado.instancia_poliza.planPago && Object.keys(this.solicitudService.asegurado.instancia_poliza.planPago)) {
      return true;
    } else {
      return false;
    }
  }

  editSolicitudDesgravamen(id:number) {
    this.router.navigate(['/AppMain/pnetseg/seguros/seguro-form',{ id }]);
  }

  soporteSolicitudDesgravamen(id:number) {
    this.router.navigate(['/AppMain/pnetseg/seguros/soporte-solicitud',{ id }]);
  }

  buscarObjetoAsegurado(id_instancia_poliza: any, index: number) {
    this.index_instancia_poliza = index;
    let id_objeto = 0
    if (this.id_poliza + '' === '6') {
      id_objeto = 18;
    }
    if (this.id_poliza + '' === '7') {
      id_objeto = 20;
    }
    this.btnEmitir = true;
    this.solicitudService.isLoadingAgain = true;
    this.personaService.findPersonaSolicitudByIdInstanciaPoliza(id_objeto, id_instancia_poliza).subscribe(async res => {
      let response = res as { status: string, message: string, data: Asegurado };
      this.solicitudService.asegurado = response.data;
      await this.componenteActualizarSolicitud.cambiarSolicitudEstado().then(respo => {
        /*let ins = this.instancias_polizas.find(param => param.id + '' === id_instancia_poliza + '');
        if (ins) {
            ins.id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
        }*/
        this.solicitudService.isLoadingAgain = false;
      });
      this.instancias_polizas = [];
      this.btnEmitir = false;
      this.instancias = [];
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });

  }

  limpiar() {
    this.segDeudores = [];
    let rolOficialCredito = this.usuarioLogin.usuarioRoles.find(param => param.id == 5);
    this.objeto = {
      agencia_required: true,
      sucursal_required: true,
      estado: null,
      poliza: null,
      agencia: this.usuarioLogin ? this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_oficina+'' : '' : '' ,
      sucursal: this.usuarioLogin ? this.usuarioLogin.usuario_banco ? this.usuarioLogin.usuario_banco.us_sucursal+'' : '' : '' ,
      usuario_login: this.usuarioLogin ? rolOficialCredito ? this.usuarioLogin.usuario_login : '' :'',
      persona_primer_apellido: '',
      persona_segundo_apellido: '',
      persona_primer_nombre: '',
      persona_doc_id: '',
      persona_doc_id_ext: '',
      id_poliza: null,
      campo_fecha: null,
      fecha_min: null,
      fecha_max: null,
      tipo_cobertura: '',
      tipo_seguro: '',
      id_deu: '',
      nro_documento: '',
      tipo_credito: '',
      usuarioLogin:this.usuarioLogin ? this.usuarioLogin : null,
      first:0,
      rows:10
    };
  }

  onPageAction(event,dt) {
    console.log(event)
    this.objeto.first = event.first;
    this.objeto.rows = event.rows;
    this.listaAll(dt);
  }

  filterAgencies(event) {
    if (event.value) {
      let value = event.value;
      let sucursalPadre = this.paramSucursales.find(param => param.parametro_cod == value);
      if (sucursalPadre) {
        let filteredAgencias = this.paramAgencias.filter(param => param.id_padre+'' == sucursalPadre.id+'');
        this.Agencias = filteredAgencias.map(param => {return {value: param.parametro_cod, label: param.parametro_descripcion}})
        this.Agencias.unshift({ label: "Seleccione Agencia", value: null });
      }
    }
  }

  selectAllLikeEstados(event) {
    if (event.value) {
      let value = event.value;
      // if (value.indexOf(',') >= 0) {
      //     let filteredEstados = this.Estados.filter(param => value.split(',').includes(param.value));
      //     let strFilterEstado = filteredEstados.map(param => {return param.value});
      //     this.objeto.estado = strFilterEstado.join(',');
      // } else {
      // }
      this.objeto.estado = value;
    }
  }

  obtenerUltimoDocumento(documentos: any[]) {
    let nro_solicitud = "";
    documentos.forEach(documento => {
      nro_solicitud = documento.nro_documento;
    });
    return nro_solicitud;
  }

  ImprimirSolicitud(id: any) {
    let nombre_archivo = 'SoliEcoAgui' + id;
    this.solicitudService.isLoadingAgain = true;
    this.reporteService.ReporteSolicitud(id, nombre_archivo).subscribe(res => {
      let response = res as { status: string, message: string, data: Poliza[] };
      this.solicitudService.isLoadingAgain = false;
      if (response.status == 'ERROR') {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No pudo imprimir el documento, verifique los datos de la solicitud' });
      } else {
        this.reporteService.cargarPagina(nombre_archivo);
      }
    }, err => {
      this.solicitudService.isLoadingAgain = false;
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  ImprimirCertificado(id: any) {
    let nombre_archivo = 'CertEcoAgui' + id;
    this.solicitudService.isLoadingAgain = true;
    this.reporteService.certificado_ecoaguinaldo(id, nombre_archivo).subscribe(res => {
      let response = res as { status: string, message: string, data: Poliza[] };
      this.solicitudService.isLoadingAgain = false;
      if (response.status == 'ERROR') {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No pudo imprimir el documento, verifique los datos de la solicitud' });
      } else {
        this.reporteService.cargarPagina(nombre_archivo);
      }
    }, err => {
      this.solicitudService.isLoadingAgain = false;
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  exportExcel(dt: any) {
    this.objeto.rows = this.totalRows;
    this.objeto.first = 0;
    this.listaAll(dt, () => {
      let segDudores = null;
      if (dt.filteredValue !== null) {
        segDudores = dt.filteredValue;
      } else {
        segDudores = this.segDeudores;
      }
      let excelDataSegDudores = this.getCarsExcel(segDudores);
      const worksheet = xlsx.utils.json_to_sheet(excelDataSegDudores);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "primengTable");
    })
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  getCars(aData: any) {
    let segDeudores = [];
    let c = 0, d = 0;
    for (let i = 0 ; i < aData.length ; i++) {
      let data = aData[i];
      c++;
      let solicitudEstado = this.EstadosAll.find(param => param.Est_Codigo && param.Est_Codigo == data.deu_solicitud.Sol_EstadoSol);
      let objeto = data;
      objeto['Nro'] = c;
      objeto['Nro_Sol'] = data.deu_solicitud.Sol_NumSol ? data.deu_solicitud.Sol_NumSol : '';
      objeto['id_sol'] = data.deu_solicitud.Sol_IdSol;
      objeto['id_deu'] = data.Deu_Id;
      objeto['Edad'] = data.Deu_Edad;
      objeto['Talla'] = data.Deu_Talla;
      objeto['Peso'] = data.Deu_Peso;
      objeto['id_documento_version'] = data.id_documento_version;
      objeto['Estado_Cod'] = data.deu_solicitud.Sol_EstadoSol;
      objeto['Sol_MontoSolSum'] = data.deu_solicitud.Sol_MontoSolSum;
      objeto['Sol_MontoSol'] = data.deu_solicitud.Sol_MontoSol;
      objeto['Sol_Deu_MontoSolAcumSum'] = data.deu_solicitud.Sol_Deu_MontoSolAcumSum;
      objeto['Deu_Imc'] = data.Deu_Imc;
      objeto['Deu_MontoActAcumVerifSum'] = data.Deu_MontoActAcumVerifSum;
      objeto['Deu_MontoSolAcumVerifSum'] = data.Deu_MontoSolAcumVerifSum;
      objeto['Deu_FactorK'] = data.Deu_FactorK;
      objeto['Deu_RespPositivas'] = data.Deu_RespPositivas;
      objeto['Deu_K'] = data.Deu_K;
      objeto['Deu_MontoSolAcumVerifSum'] = data.Deu_MontoSolAcumVerifSum;
      objeto['Deu_FactorImc'] = data.Deu_FactorImc;
      objeto['Sol_MonedaSol'] = data.deu_solicitud.Sol_MonedaSol;
      objeto['Poliza'] = data.Deu_Poliza ? data.Deu_Poliza : '';
      objeto['CI'] = data ? data.Deu_NumDoc ? data.Deu_NumDoc + (data.Deu_ExtDoc ? ' '+data.Deu_ExtDoc : '') : '' : '';
      objeto['Sucursal'] = data.deu_solicitud.Sol_Sucursal;
      objeto['Agencia'] = data.deu_solicitud.Sol_Agencia;
      objeto['Tipo_Cobertura'] = data.Deu_cobertura;
      objeto['Tipo_Seguro'] = data.deu_solicitud.Sol_TipoSeg;
      objeto['Estado'] = solicitudEstado ? solicitudEstado.Est_Descripcion : '';
      objeto['Incluido'] = data.Deu_Incluido;
      objeto['Fecha_Solicitud'] = data.deu_solicitud.Sol_FechaSol ? this.util.formatoFecha(data.deu_solicitud.Sol_FechaSol + '') : null;
      objeto['Fecha_Resolucion'] = data ? data.Deu_FechaResolucion ? this.util.formatoFecha(data.Deu_FechaResolucion + '') : null : null;
      objeto['Asegurado'] = data ? data.Deu_Nombre+' '+data.Deu_Paterno+' '+data.Deu_Materno : '';
      objeto['usuario'] = data.deu_solicitud.Sol_CodOficial;
      segDeudores.push(objeto);
    }
    return segDeudores;
  }

  getCarsExcel(ins_poli: any) {
    let segDeudores = [];
    let c = 0;
    for (let data of ins_poli) {
      c++;
      let objeto = {};

      objeto['Nro'] = data.Nro;
      // objeto['id_sol'] = instancia.id_sol;
      objeto['id_deu'] = data.Deu_Id;
      // objeto['id_documento_version'] = instancia.id_documento_version;
      objeto['Nro_Sol'] = data.Nro_Sol;
      objeto['Poliza'] = data.Poliza;
      objeto['CI'] = data.CI;
      objeto['Sucursal'] = data.Sucursal;
      objeto['Agencia'] = data.Agencia;
      objeto['Tipo_Cobertura'] = data.Tipo_Cobertura;
      objeto['Tipo_Seguro'] = data.Tipo_Seguro;
      objeto['Estado'] = data.Estado;
      objeto['Fecha_Solicitud'] = data.Fecha_Solicitud;
      objeto['Fecha_Resolucion'] = data.Fecha_Resolucion;
      objeto['Asegurado'] = data.Asegurado;
      objeto['usuario'] = data.usuario;
      segDeudores.push(objeto);
    }

    return segDeudores;
  }

  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];
      let result = null;

      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

      return (event.order * result);
    });
  }

  // exportPdf() {
  //       const doc = new jsPDF.default(0, 0);
  //       doc.autoTable(this.exportColumns, this.poliza.instancia_polizas);
  //       doc.save('primengTable.pdf');
  // }

  ImprimirSolicitudEcovida(id: any) {
    let nombre_archivo = 'SoliEcoVida' + id;
    this.solicitudService.isLoadingAgain = true;
    if (this.id_poliza + '' === '6') {
      this.reporteService.ReporteSolicitudEcoVida(id, nombre_archivo).subscribe(res => {
        let response = res as { status: string, message: string, data: Poliza[] };
        this.solicitudService.isLoadingAgain = false;
        if (response.status == 'ERROR') {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No pudo imprimir el documento, verifique los datos de la solicitud' });
        } else {
          this.reporteService.cargarPagina(nombre_archivo);
        }
      }, err => {
        this.solicitudService.isLoadingAgain = false;
        console.error("ERROR llamando servicio ImprimirSolicitud:", err);
      });
    }
    if (this.id_poliza + '' === '7') {
      this.reporteService.ReporteSolicitudEcoProteccion(id, nombre_archivo).subscribe(res => {
        let response = res as { status: string, message: string, data: Poliza[] };
        this.solicitudService.isLoadingAgain = false;
        if (response.status == 'ERROR') {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No pudo imprimir el documento, verifique los datos de la solicitud' });
        } else {
          this.reporteService.cargarPagina(nombre_archivo);
        }
      }, err => {
        this.solicitudService.isLoadingAgain = false;
        console.error("ERROR llamando servicio ImprimirSolicitud:", err);
      });
    }
  }

  ImprimirCertificadoEcoVida(id: any) {
    let nombre_archivo = 'CertEcoVida' + id;
    this.solicitudService.isLoadingAgain = true;
    if (this.id_poliza + '' === '6') {
      this.reporteService.ReporteCertificadoEcoVida(id, nombre_archivo).subscribe(res => {
        let response = res as { status: string, message: string, data: Poliza[] };
        this.solicitudService.isLoadingAgain = false;
        if (response.status == 'ERROR') {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No pudo imprimir el documento, verifique los datos de la solicitud' });
        } else {
          this.reporteService.cargarPagina(nombre_archivo);
        }
      }, err => {
        this.solicitudService.isLoadingAgain = false;
        console.error("ERROR llamando servicio ImprimirCertificadoEcoVida:", err);
      });
    }
    if (this.id_poliza + '' === '7') {
      this.reporteService.ReporteCertificadoEcoProteccion(id, nombre_archivo).subscribe(res => {
        let response = res as { status: string, message: string, data: Poliza[] };
        this.solicitudService.isLoadingAgain = false;
        if (response.status == 'ERROR') {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No pudo imprimir el documento, verifique los datos de la solicitud' });
        } else {
          this.reporteService.cargarPagina(nombre_archivo);
        }
      }, err => {
        this.solicitudService.isLoadingAgain = false;
        console.error("ERROR llamando servicio ImprimirCertificadoEcoVida:", err);
      });
    }
  }

  generarplanpago(id: any) {
    let pp = new Plan_pago();
    pp.id_instancia_poliza = id;
    pp.total_prima = 192;
    pp.interes = 0;
    pp.plazo_anos = 1;
    pp.periodicidad_anual = 12;
    pp.prepagable_postpagable = 1;
    pp.fecha_inicio = new Date();
    pp.adicionado_por = '2';
    pp.modificado_por = '2';
    this.planPagoService.GenerarPlanPagos(pp).subscribe(res => {
      let response = res as { status: string, message: string, data: any[] };
      if (response.status == 'ERROR') {

      } else {

      }
    }, err => console.error("ERROR llamando servicio plan de pagos:", err));
  }

  async GeneraAllPlanPagos(id: any) {
    await this.planPagoService.listaTodosSinPlanPagos().subscribe(async res => {
      let response = res as { status: string, message: string, data: any[] };
      for (let i = 0; i < response.data.length; i++) {
        let pp = new Plan_pago();
        pp.id_instancia_poliza = response.data[i].id;
        pp.total_prima = parseInt(response.data[i].monto) * parseInt(response.data[i].plazo);
        pp.interes = 0;
        pp.plazo_anos = 1;
        pp.periodicidad_anual = parseInt(response.data[i].monto);
        pp.prepagable_postpagable = 1;
        pp.fecha_inicio = response.data[i].fecha_emision;
        pp.adicionado_por = '4';
        pp.modificado_por = '4';
        await this.planPagoService.GenerarPlanPagos2(pp).then(res => {
          let response = res as { status: string, message: string, data: any[] };
          if (response.status == 'ERROR') {

          } else {

          }
        }, err => console.error("ERROR llamando servicio plan de pagos:", err));
      }
    }, err => console.error("ERROR llamando servicio plan de pagos:", err));
  }

  verPlanPagos(ins_pol: any) {
    this.menuService.activaRuteoMenu('66', null, {id_instancia_poliza:ins_pol.id});
  }
}
