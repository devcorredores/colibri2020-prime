import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionDesgravamenComponent } from './gestion-desgravamen.component';

describe('GestionDesgravamenComponent', () => {
  let component: GestionDesgravamenComponent;
  let fixture: ComponentFixture<GestionDesgravamenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionDesgravamenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionDesgravamenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
