import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AltaEcoAccidentesComponent} from "./eco-accidentes/alta-eco-accidentes/alta-eco-accidentes.component";
import {GestionEcoAccidentesComponent} from "./eco-accidentes/gestion-eco-accidentes/gestion-eco-accidentes.component";
import {GestionDesgravamenComponent} from "./desgravamen/gestion-desgravamen/gestion-desgravamen.component";
import {AnulacionModule} from "../../../../src/core/componentes/anulacion/anulacion.module";
import {
  BreadcrumbModule,
  DialogModule, DropdownModule,
  FieldsetModule, MessageService,
  MessagesModule,
  PanelModule,
  TableModule,
  ToastModule
} from "primeng";
import {ArchivosModule} from "../../../../src/core/componentes/archivos/archivos.module";
import {BeneficiarioModule} from "../../../../src/core/componentes/beneficiario/beneficiario.module";
import {TransicionesModule} from "../../../../src/core/componentes/transiciones/transiciones.module";
import {ActualizarSolicitudModule} from "../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.module";
import {BreadcrumbService} from "../../../../src/core/servicios/breadcrumb.service";
import {HttpClientModule} from "@angular/common/http";
import {LoaderService} from "../../../../src/core/servicios/loader.service";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    AltaEcoAccidentesComponent,
    GestionEcoAccidentesComponent,
    GestionDesgravamenComponent,
  ],
  imports: [
    CommonModule,
      FormsModule,
    // BrowserModule,
    AppRoutingModule,
    AnulacionModule,
    TransicionesModule,
    BeneficiarioModule,
    ArchivosModule,
    HttpClientModule,

    ActualizarSolicitudModule,

    DialogModule,
    ToastModule,
    TableModule,
    PanelModule,
    MessagesModule,
    FieldsetModule,
    DropdownModule,
    BreadcrumbModule,
  ],
  providers: [
    BreadcrumbService,
    LoaderService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
