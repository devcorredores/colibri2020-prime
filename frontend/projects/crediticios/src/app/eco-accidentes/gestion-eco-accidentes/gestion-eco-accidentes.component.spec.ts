import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionEcoAccidentesComponent } from './gestion-eco-vida.component';

describe('GestionEcoVidaComponent', () => {
  let component: GestionEcoAccidentesComponent;
  let fixture: ComponentFixture<GestionEcoAccidentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionEcoAccidentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionEcoAccidentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
