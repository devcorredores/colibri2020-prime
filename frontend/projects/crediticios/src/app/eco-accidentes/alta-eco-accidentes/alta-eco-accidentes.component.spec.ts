import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoAccidentesComponent } from './alta-eco-accidentes.component';

describe('AltaEcoProteccionNuevoComponent', () => {
  let component: AltaEcoAccidentesComponent;
  let fixture: ComponentFixture<AltaEcoAccidentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoAccidentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoAccidentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
