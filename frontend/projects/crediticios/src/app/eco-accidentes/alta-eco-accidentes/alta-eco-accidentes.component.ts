import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {Message, MessageService, SelectItem} from "primeng/api";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {DomSanitizer} from "@angular/platform-browser";
import {isObject} from "util";
import "../../../../../../src/helpers/prototypes";
import {MenuItem} from "primeng/primeng";
import * as moment from 'moment';
import {Anexo_poliza} from "../../../../../../src/core/modelos/anexo_poliza";
import {Beneficiario} from "../../../../../../src/core/modelos/beneficiario";
import {Parametro, ParametroRuteo} from "../../../../../../src/core/modelos/parametro";
import {Persona} from "../../../../../../src/core/modelos/persona";
import {Atributo} from "../../../../../../src/core/modelos/atributo";
import {persona_banco, persona_banco_solicitud} from "../../../../../../src/core/modelos/persona_banco";
import {persona_banco_tarjeta_debito} from "../../../../../../src/core/modelos/persona_banco_tarjeta_debito";
import {Sucursal} from "../../../../../../src/core/modelos/sucursal";
import {Agencia} from "../../../../../../src/core/modelos/agencia";
import {BreadcrumbService} from "../../../../../../src/core/servicios/breadcrumb.service";
import {ParametrosService} from "../../../../../../src/core/servicios/parametro.service";
import {AnexoService} from "../../../../../../src/core/servicios/anexo.service";
import {PersonaService} from "../../../../../../src/core/servicios/persona.service";
import {BeneficiarioService} from "../../../../../../src/core/servicios/beneficiario.service";
import {SoapuiService} from "../../../../../../src/core/servicios/soapui.service";
import {AtributoService} from "../../../../../../src/core/servicios/atributo.service";
import {ObjetoAtributoService} from "../../../../../../src/core/servicios/objetoAtributo.service";
import {DocumentoService} from "../../../../../../src/core/servicios/documento.service";
import {PolizaService} from "../../../../../../src/core/servicios/poliza.service";
import {InstanciaPolizaService} from "../../../../../../src/core/servicios/instancia-poliza.service";
import {ReporteService} from "../../../../../../src/core/servicios/reporte.service";
import {RolesService} from "../../../../../../src/core/servicios/rol.service";
import {UsuariosService} from "../../../../../../src/core/servicios/usuarios.service";
import {ContextoService} from "../../../../../../src/core/servicios/contexto.service";
import {InstanciaPolizaTransService} from "../../../../../../src/core/servicios/instancia-poliza-trans.service";
import {SolicitudService} from "../../../../../../src/core/servicios/solicitud.service";
import {PlanPagoService} from "../../../../../../src/core/servicios/plan-pago.service"
import {SessionStorageService} from "../../../../../../src/core/servicios/sessionStorage.service";
import {AdministracionDePermisosService} from "../../../../../../src/core/servicios/administracion-de-permisos.service";
import {Asegurado} from "../../../../../../src/core/modelos/asegurado";
import {Instancia_documento} from "../../../../../../src/core/modelos/instancia_documento";
import {Perfil_x_Componente} from "../../../../../../src/core/modelos/componente";
import {Poliza} from "../../../../../../src/core/modelos/poliza";
import {Instancia_poliza_transicion} from "../../../../../../src/core/modelos/instancia_poliza_transicion";
import {persona_banco_operacion} from "../../../../../../src/core/modelos/persona_banco_operacion";
import {persona_banco_account} from "../../../../../../src/core/modelos/persona_banco_account";
import {persona_banco_pep} from "../../../../../../src/core/modelos/persona_banco_pep";
import {persona_banco_datos} from "../../../../../../src/core/modelos/persona_banco_datos";
import {Instancia_poliza} from "../../../../../../src/core/modelos/instancia_poliza";
import {Atributo_instancia_poliza} from "../../../../../../src/core/modelos/atributo_instancia_poliza";
import {Plan_pago} from "../../../../../../src/core/modelos/plan_pago";
import {Upload} from "../../../../../../src/core/modelos/upload";
import {ArchivoService} from "../../../../../../src/core/servicios/archivo.service";
import {TransicionesComponent} from "../../../../../../src/core/componentes/transiciones/transiciones.component";
import {BeneficiarioComponent} from "../../../../../../src/core/componentes/beneficiario/beneficiario.component";
import {ArchivosComponent} from "../../../../../../src/core/componentes/archivos/archivos.component";
import {Util} from "../../../../../../src/helpers/util";
import {Rol} from "../../../../../../src/core/modelos/rol";
import {ActualizarSolicitudComponent} from "../../../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component";

declare var $: any;

@Component({
    selector: 'app-alta-eco-accidentes',
    templateUrl: './alta-eco-accidentes.component.html',
    styleUrls: ['./alta-eco-accidentes.component.css']
})
export class AltaEcoAccidentesComponent implements OnInit {

    @ViewChild('componentTransiciones', {static: false}) private transicionesComponent: TransicionesComponent;
    @ViewChild("componenteBeneficiario", {static: false}) private componenteBeneficiario: BeneficiarioComponent;
    @ViewChild("componenteArchivo", {static: false}) private componenteArchivo: ArchivosComponent;
    @ViewChild("componenteActualizarSolicitud", {static: false}) public componenteActualizarSolicitud: ActualizarSolicitudComponent;
    // @ViewChild("componenteAnexo", {static:false}) private componenteAnexo:AnexoComponent;

    color = 'primary';
    mode = 'indeterminate';
    value = 1;
    show = false;
    ruta: string;
    yesterday: Date = new Date();
    displayFormTitular: boolean = true;
    displayDatosTitular: boolean = false;
    displayErrorMontoCuota: boolean = false;
    displayErrorPlanPago: boolean = false;
    displayTarjetaInvalida: boolean = false;
    displayTarjetaInvalidaContinuar: boolean = false;
    displayBeneficiariosExcedidos: boolean = false;
    displayBeneficiariosTitular: boolean = false;
    displayBeneficiariosNotFound: boolean = false;
    displayBeneficiarioFound: boolean = false;
    collapsedFormTitular: boolean = false;
    collapsedDatosTitular: boolean = false;
    displayClienteNoExiste: boolean = false;
    displayEdadIncorrecta: boolean = false;
    displaySolicitudDuplicado: boolean = false;
    displayActualizacionExitoso: boolean = false;
    displayIntroduscaNroTarjeta: boolean = false;
    displayNroTarjetaSinCuentas: boolean = false;
    displayNroCuentasActualizados: boolean = false;
    // beneficiario: Persona = new Persona();
    beneficiario: Persona = new Persona();
    anexosPlanesPoliza: Anexo_poliza[] = [];
    anexoPoliza: Anexo_poliza;
    Beneficiarios: Persona[] = [];
    BeneficiariosAux: Beneficiario[] = [];
    benAux: Beneficiario;
    id_beneficiarioAux;
    // parentescoAux: string;
    parentescoAux: Parametro;
    numeroBeneficiarios = 2;
    docId: string = null;
    minLengthMsg: string = '';
    minLengthMsgCelular: string = '';
    minlength: number = 0;
    minlengthCelular: number = 0;
    docIdExt: number = null;
    display: boolean = false;
    displayBeneficiario: boolean = false;
    displayBeneficiarioDouble: boolean = false;
    displayBeneficiariosCambio: boolean = false;
    showEnviarDocumentos = true;
    displayEliminarBeneficiario: boolean = false;
    displayEliminarBeneficiarioOK: boolean = false;
    //  solicitudes:Asegurado[] = [];
    parametroMoneda: Parametro;
    SucursalesId: any[] = [];
    AgenciasId: any[] = [];
    MonedasParametroDescripcion: any[] = [];
    parametros: Parametro[] = [];
    userform: FormGroup;
    fechaEmisionForm: FormGroup;
    envioDocumentosForm: FormGroup;
    formBusqueda: FormGroup;
    userform2: FormGroup;
    userform3: FormGroup;
    responsiveOptions: any;
    nuevo: boolean = true;
    atributo: Atributo[];
    // atribut_instancia_poliza:Atributo_instancia_poliza[];
    // dato_complementario: [];
    //selected_atributo_instancia_poliza:Atributo_instancia_poliza;
    atributo_instancia_poliza_selected: any[] = [];
    cols: any[];
    doc_id: number = 0;
    ext: number = 0;
    persona_banco_beneficiario: persona_banco;
    //  titular: Persona = new Persona;
    // atributo_instancia_poliza_cuenta: Atributo_instancia_poliza = new Atributo_instancia_poliza();
    persona_banco_tarjetas_debito: persona_banco_tarjeta_debito[] = [];
    plan: Anexo_poliza = new Anexo_poliza();
    usuario_banco_sucursal: Sucursal;
    usuario_banco_agencia: Agencia;
    events: Subscription[] = [];
    messages: string[] = [];
    maxLengthTarjeta: number = 1;
    guardandoTitular: boolean = false;
    editandoBeneficiario: boolean = false;
    fromSearch: boolean = false;
    id_instancia_poliza: number;
    id_estado: string;
    id_asegurado: string;
    totalPorcentaje = 0;
    persona: Persona;
    numero_solicitudes: number;

    showErrors: boolean = false;
    messaje: string = "";
    withErrorsOrWarnings: boolean = false;

    // benificiarioAux: Beneficiario[] = [];
    // btnValidarYContinuarEnabled = false;
    // btnOrdenPagoEnabled = false;
    btnEnviarDocumentosEnabled = false;
    btnTarjetaDebitoCuentasEnabled = true;
    showValidarYContinuar = true;
    showEmitirCertificado = true;
    // msgText: string;
    validarTarjeta: Boolean = false;
    reloadBeneficiarioComponent = true;
    util = new Util();
    apellidos: string[] = [];
    // edadMinimaYears: number = 18 // 18 Años;
    // edadMaximaYears: number = 65// 65 años;
    idsPerfiles: number[] = [];
    stopSaving: boolean = false;
    stopSending: boolean = false;
    fechaEdadMinima: Date = new Date();
    stopSavingByEdad: boolean = false;
    stopSavingByLastName: boolean = false;
    stopSavingBySolicitud: boolean = false;
    stopSavingByMontoCuota: boolean = false;
    validandoContinuando: boolean = false;
    msgs: Message[];
    splitButtonItems: MenuItem[] = [
        {label: 'Update', icon: 'ui-icon-update'},
        {label: 'Delete', icon: 'ui-icon-close'},
        {label: 'Home', icon: 'ui-icon-home', url: 'http://www.primefaces.org/primeng'}
    ];
    search: boolean = false;
    aseguradoWithDiferentDocIdExt: Asegurado;
    solicitudesObservaciones:{solicitudAprobada:boolean, solicitudAjena:boolean, solicitudSelected:string}[] = []
    reloadData: boolean = false;

    constructor(
        private params: ActivatedRoute,
        private breadcrumbService: BreadcrumbService,
        private changeDetection: ChangeDetectorRef,
        private parametroService: ParametrosService,
        private anexoService: AnexoService,
        private fb: FormBuilder,
        private personaService: PersonaService,
        private beneficiarioService: BeneficiarioService,
        private soapuiService: SoapuiService,
        private atributoService: AtributoService,
        private objetoAtributoService: ObjetoAtributoService,
        private documentoService: DocumentoService,
        private polizaService: PolizaService,
        private router: Router,
        private instanciaPolizaService: InstanciaPolizaService,
        private messageService: MessageService,
        private sanitizer: DomSanitizer,
        private reporteService: ReporteService,
        private rolesService: RolesService,
        private service: MessageService,
        private usuarioService: UsuariosService,
        private contextoService: ContextoService,
        private instanciaPolizaTransService: InstanciaPolizaTransService,
        private cdRef: ChangeDetectorRef,
        public solicitudService: SolicitudService,
        private planPagoService: PlanPagoService,
        public archivoService: ArchivoService,
        private sessionStorageService: SessionStorageService,
        public adminPermisosService: AdministracionDePermisosService,
    ) {}

    beforeToggleDatosTitular(reload = false) {
        this.sessionStorageService.setItemSync("paramsDeleted", 'true');
        this.solicitudService.displayModalDatosTitular = false;
        this.solicitudService.parametrosRuteo.openForm = false;
        this.solicitudService.changeView(this.userform, this.solicitudService.asegurado);
        this.onInit(10, null, null, false);
    }

    beforeToggleFormTitular() {
        this.solicitudService.displayModalFormTitular = false;
        this.solicitudService.parametrosRuteo.openForm = false;
        this.solicitudService.changeView(this.userform, this.solicitudService.asegurado);
        this.onInit(10, this.solicitudService.doc_id, parseInt(this.solicitudService.extension+''), false);
    }

    async setFeaturesBeneficiarios(beneficiarioResp: any) {
        if (beneficiarioResp != undefined) {
            if (beneficiarioResp.length) {
                beneficiarioResp.forEach(async (beneficiario: any) => {
                    beneficiario.persona_doc_id_ext = beneficiario.entidad != undefined ? beneficiario.entidad.persona.persona_doc_id_ext : (beneficiario.persona_doc_id_ext != undefined ? beneficiario.persona_doc_id_ext : '');
                    beneficiario.persona_primer_apellido = beneficiario.entidad != undefined ? beneficiario.entidad.persona.persona_primer_apellido : (beneficiario.persona_primer_apellido != undefined ? beneficiario.persona_primer_apellido : '');
                    beneficiario.persona_segundo_apellido = beneficiario.entidad != undefined ? beneficiario.entidad.persona.persona_segundo_apellido : (beneficiario.persona_segundo_apellido != undefined ? beneficiario.persona_segundo_apellido : '');
                });
            }
        }
    }

    ngOnInit() {
        this.onInit();
        this.reloadData = true;
        this.solicitudService.parametrosRuteo.openForm = false;
    }

    onInit(id_poliza: number = null, docId: string = null, docIdExt: number = null, search:boolean = true, updatePersona:boolean = true) {
        this.solicitudService.product = this.solicitudService.ruta;
        this.solicitudService.persona_banco_datos = new persona_banco_datos();
        this.solicitudService.persona_banco = new persona_banco();
        this.solicitudService.persona_banco_account = new persona_banco_account();
        this.solicitudService.persona_banco_solicitud = new persona_banco_solicitud();
        this.solicitudService.persona_banco_operacion = new persona_banco_operacion();
        this.solicitudService.persona_banco_pep = new persona_banco_pep();
        this.solicitudService.fileDocumentoSolicitud = new Upload();
        this.solicitudService.fileDocumentoCertificado = new Upload();
        this.solicitudService.isInAltaSolicitud = true;
        this.solicitudService.solicitudAprobada = false;
        this.solicitudService.paramsLoaded = false;
        this.solicitudService.isLoadingAgain = true;
        this.solicitudService.displayBusquedaCI = true;
        this.solicitudService.destinatariosCorreos = '';
        this.solicitudService.paramsLoaded = false;
        this.solicitudService.displayModalDatosTitular = false;
        this.solicitudService.parametrosRuteo.parametro_vista = id_poliza+'';
        this.solicitudService.constructComponent(async () => {
            this.getPoliza(async () => {
                let paramsDeleted = this.sessionStorageService.getItemSync("paramsDeleted");
                this.solicitudService.TipoSeguros = [{label: this.solicitudService.poliza.descripcion, value: this.solicitudService.poliza.id}];
                this.setUserForm();
                if (
                    !paramsDeleted &&
                    this.solicitudService.parametrosRuteo.parametro_ruteo !== null &&
                    this.solicitudService.parametrosRuteo.parametro_ruteo !== "undefined" &&
                    this.solicitudService.parametrosRuteo.parametro_ruteo !== "{}" &&
                    Object.keys(this.solicitudService.parametrosRuteo.parametro_ruteo).length
                ) {
                    this.solicitudService.isLoadingAgain = true;
                    this.solicitudService.editandoTitular = false;
                    this.solicitudService.displayBusquedaCI = false;
                    this.solicitudService.displayModalFormTitular = false;
                    this.id_instancia_poliza = this.solicitudService.parametrosRuteo.parametro_ruteo ? this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza ? this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza : null : null;
                    // this.iniciarSolicitud();
                    this.stateButtonsFlow();
                    this.personaService.findPersonaSolicitudByIdInstanciaPoliza(this.solicitudService.objetoAseguradoDatosComplementarios.id, this.id_instancia_poliza).subscribe( async (res) => {
                        let resp = res as { status: string, message: string, data: Asegurado };
                        if (Object.keys(resp.data)) {
                            this.solicitudService.asegurado = resp.data;
                            // await this.solicitudService.setDatesOfAsegurado();
                            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
                            // this.solicitudService.setAtributosToPersonaBanco(this.userform, async () => {
                                //this.solicitudService.setAtributosToAsegurado();
                            await this.initInstanciaPoliza(async () => {
                                if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                                    if (this.reloadData && this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                                        this.reloadData = false;
                                        await this.getDatosFromCustomerService(this.solicitudService.asegurado.entidad.persona.persona_doc_id, this.solicitudService.asegurado.entidad.persona.persona_doc_id_ext+'', null);
                                    }
                                }
                            })
                            // })
                        } else {
                            this.displayClienteNoExiste = true;
                            this.stateButtonsFlow();
                        }
                    }, err => {
                        if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                            this.router.navigate(['login']);
                        } else {
                            console.log(err);
                        }
                    });
                } else {
                    if (this.solicitudService.parametrosRuteo.openForm) {
                        this.BuscarCliente(docId,docIdExt,search,updatePersona)
                    } else {
                        this.solicitudService.isLoadingAgain = false;
                        this.solicitudService.displayModalFormTitular = false;
                        this.solicitudService.displayBusquedaCI = true;
                    }
                }
            });
        });
    }

    async initInstanciaPoliza(callback:any = null) {
        this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
        this.solicitudService.product = this.solicitudService.asegurado.instancia_poliza.poliza.descripcion;
        this.solicitudService.id_poliza = this.solicitudService.asegurado.instancia_poliza.poliza.id;
        // await this.solicitudService.setDatesOfAsegurado();
        this.solicitudService.setAtributosToPersonaBanco(this.userform, async res => {
            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach((instancia_documento: Instancia_documento) => {
                if (instancia_documento.id_documento == this.solicitudService.documentoSolicitud.id) {
                    this.solicitudService.instanciaDocumentoSolicitud = instancia_documento;
                } else if (instancia_documento.id_documento == this.solicitudService.documentoComprobante.id) {
                    this.solicitudService.instanciaDocumentoComprobante = instancia_documento;
                } else if (instancia_documento.id_documento == this.solicitudService.documentoCertificado.id) {
                    this.solicitudService.instanciaDocumentoCertificado = instancia_documento;
                }
            });
            await this.solicitudService.setAtributosToPersonaBanco(this.userform, async () => {
                this.solicitudService.persona_banco.extension = this.solicitudService.asegurado.entidad.persona.persona_doc_id_ext+'';
                await this.stateButtonsFlow();
            });
            this.setPago(this.solicitudService.atributoDebitoAutomatico.valor);
            //this.setMonto(this.solicitudService.persona_banco_datos.modalidad_pago);
            this.setNroCuotas(this.solicitudService.persona_banco_datos.nro_cuotas);
            this.solicitudService.setBeneficiarios();
            this.solicitudService.displayModalDatosTitular = true;
            this.displayDatosTitular = true;
            this.solicitudService.isLoadingAgain = false;
            if (this.componenteBeneficiario != undefined) {
                this.componenteBeneficiario.ngOnInit();
            }
            if (this.transicionesComponent != undefined) {
                this.transicionesComponent.ngOnInit();
            }
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                await this.getDatosFromAccountService();
            }
            this.stateButtonsFlow();
            if(typeof callback == 'function') {
                await callback();
            }
        });
    }
    async setUserForm() {
        this.solicitudService.isLoadingAgain = true;
        if (this.solicitudService.id_poliza == 10) {
            this.idsPerfiles = [27,33];
        } else if (this.solicitudService.id_poliza == 12) {
            this.idsPerfiles = [34,35];
        }
        this.solicitudService.setCurrentPerfil(this.idsPerfiles);
        this.adminPermisosService.getComponentes(this.solicitudService.parametrosRuteo.id_vista, this.solicitudService.currentPerfil.id).subscribe(async (res) => {
            let response = res as { status: string; message: string; data: Perfil_x_Componente[]; };
            this.solicitudService.isLoadingAgain = false;
            this.solicitudService.componentesInvisibles = response.data;
            await this.solicitudService.componentsBehavior(res => {
                this.minlengthCelular = 8;
                this.minLengthMsg = "Debe contener " + this.minlength + " digitos";
                this.minLengthMsgCelular = "Debe contener " + this.minlengthCelular + " digitos";
                this.breadcrumbService.setItems([
                    {label: 'Productos'},
                    {label: 'Eco Accidentes', routerLink: ['/file']}
                ]);
                this.formBusqueda = this.fb.group({
                    'persona_doc_id': new FormControl('', [Validators.required]),
                    'persona_doc_id_ext': new FormControl('', Validators.required),
                });
                this.envioDocumentosForm = this.fb.group({
                    'destinatarios_correos': new FormControl('', [Validators.required]),
                    'envio_documentos': new FormControl('', Validators.required),
                });
                this.fechaEmisionForm = this.fb.group({
                    'fecha_registro': new FormControl('', Validators.required),
                    'nro_transaccion': new FormControl('', Validators.required)
                });
                this.userform = this.fb.group({
                    'persona_doc_id': new FormControl('', [Validators.required]),
                    'persona_doc_id_ext': new FormControl('', Validators.required),
                    //'persona_doc_id_comp': new FormControl(''),
                    'persona_primer_apellido': new FormControl('', this.solicitudService.showPrimerApellido ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    'persona_segundo_apellido': new FormControl('', this.solicitudService.showSegundoApellido ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    'persona_primer_nombre': new FormControl('', this.solicitudService.showPrimerNombre ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    'persona_segundo_nombre': new FormControl('', this.solicitudService.showSegundoNombre ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    'persona_apellido_casada': new FormControl('', Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")),
                    'persona_direccion_domicilio': new FormControl(''),
                    'persona_direcion_trabajo': new FormControl(''),
                    'persona_fecha_nacimiento': new FormControl('', [Validators.required]),
                    // 'persona_celular': new FormControl(''),
                    'persona_telefono_domicilio': new FormControl(this.solicitudService.showTelefonoDomicilio ? [Validators.minLength(8), Validators.maxLength(8), Validators.pattern("^[0-9]*$")] : []),
                    'persona_telefono_trabajo': new FormControl(this.solicitudService.showTelefonoTrabajo ? [Validators.minLength(8), Validators.maxLength(8), Validators.pattern("^[0-9]*$")] : []),
                    'persona_telefono_celular': new FormControl('', this.solicitudService.showTelefonoCelular ? [Validators.minLength(8), Validators.maxLength(8), Validators.pattern("^[0-9]*$")] : []),
                    //'persona_profesion': new FormControl(''),
                    'par_tipo_documento_id': new FormControl('', this.solicitudService.showTipoDocumento ? [Validators.required] : []),
                    // 'par_nacionalidad_id': new FormControl(4),
                    'par_pais_nacimiento_id': new FormControl('', []),
                    'par_ciudad_nacimiento_id': new FormControl('', this.solicitudService.showCiudadNacimiento ? [] : []),
                    'par_provincia_id': new FormControl('', this.solicitudService.showProvincia ? [] : []),
                    'par_departamento_id': new FormControl('', this.solicitudService.showDepartamento ? [] : []),
                    'par_lugar_nacimiento_id': new FormControl('', this.solicitudService.showLugarNacimiento ? [] : []),
                    'par_sexo_id': new FormControl('', [Validators.required]),
                    //'persona_origen': new FormControl(''),
                    'par_numero_cuenta': new FormControl('', this.solicitudService.showNroCuenta ? [] : []),
                    'par_cuenta_expiracion': new FormControl('', this.solicitudService.showCuentaFechaExpiracion ? [] : []),
                    'par_ocupacion': new FormControl('', this.solicitudService.showOcupacion ? [] : []),
                    // 'par_plan': new FormControl('', this.solicitudService.showPlan ? [Validators.required] : []),
                    // 'par_plazo': new FormControl('', this.solicitudService.showPlazo ? [Validators.required, Validators.min(1)] : []),
                    'par_codigo_agenda_id': new FormControl(''),
                    //'par_estado_civil_id': new FormControl(''),
                    'par_mail_id': new FormControl('', this.solicitudService.showEmail ? [] : []),
                    //'par_caedec_id': new FormControl(''),
                    'par_localidad_id': new FormControl('', this.solicitudService.showLocalidad ? [] : []),
                    'par_sucursal': new FormControl('', this.solicitudService.showSucursal ? [] : []),
                    'par_agencia': new FormControl('', this.solicitudService.showAgencia ? [] : []),
                    //'par_prima': new FormControl('', this.solicitudService.showPrima ? [this.solicitudService.atributoPrima.requerido = Validators.required] : []),
                    //'par_nro_sci': new FormControl(''),
                    'par_modalidad_pago': new FormControl(''),
                    'par_zona': new FormControl(''),
                    'par_nro_direccion': new FormControl(''),
                    'par_razon_social': new FormControl(''),
                    'par_nit_carnet': new FormControl(''),
                    'par_moneda': new FormControl('', this.solicitudService.showMoneda ? [] : []),
                    'par_debito_automatico_id': new FormControl('', this.solicitudService.showPagoEfectivo ? [Validators.required] : []),
                    //'par_condicion_pep': new FormControl('', this.solicitudService.showCondicionPep ? [] : []),
                    //'par_cargo_pep': new FormControl('', this.solicitudService.showCargoEntidadPep ? [] : []),
                    //'par_periodo_pep': new FormControl('', this.solicitudService.showPeriodoCargoPep ? [] : []),
                    'par_direccion_laboral': new FormControl('', this.solicitudService.showDireccionLaboral ? [] : []),
                    //'par_tipo_cuenta': new FormControl('', this.solicitudService.showTipoCuenta ? [] : []),
                    //'par_nro_cuotas': new FormControl('', this.solicitudService.showNroCuotas? [] : []),
                    //'par_monto': new FormControl('', this.solicitudService.showMonto? [] : []),
                    'par_forma_pago': new FormControl('', this.solicitudService.showFormaPago ? [Validators.required] : []),
                    'par_desc_ocupacion': new FormControl('', this.solicitudService.showDescOcupacion ? [] : []),
                    'par_producto_asociado': new FormControl('', this.solicitudService.showProductoAsociado ? [] : []),
                    'par_tipo_seguro': new FormControl('', this.solicitudService.showTipoSeguro ? [Validators.required] : []),
                    'par_nro_solicitud_sci': new FormControl('', this.solicitudService.showNroSolicitudSci ? [Validators.required] : []),
                    'par_operacion_plazo': new FormControl('', this.solicitudService.showSolicitudPlazoCredito ? [Validators.required] : []),
                    'par_operacion_tipo_credito': new FormControl('', this.solicitudService.showTipoCredito ? [Validators.required] : []),
                    'par_prima': new FormControl('', this.solicitudService.showPrima ? [] : []),
                    'par_solicitud_prima_total': new FormControl('', this.solicitudService.showSolicitudPrimaTotal ? [] : []),
                });
                this.solicitudService.getFormValidationErrors(this.userform);
                this.solicitudService.showFormValidation(this.userform);
                this.solicitudService.setFeatureValidacionAlInicioPersonaBanco();
            });
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    async featureApPaternoMaternoCasada() {
        if (this.solicitudService.persona_banco.paterno == '' && this.solicitudService.persona_banco.materno == '' && this.solicitudService.persona_banco.apcasada == '') {
            this.userform.controls['persona_primer_apellido'].setValidators([Validators.pattern("^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$")]);
            return false;
        }
        return true
    }

    async componentsBehaviorOnInit(callback: Function = null) {
        this.solicitudService.showTarjetaNombre = false;
        this.solicitudService.showTarjetaValida = false;
        this.solicitudService.showModalidadPago = false;
        this.solicitudService.showPagoEfectivo = false;
        this.solicitudService.showRazonSocial = false;
        this.solicitudService.showTarjetaNro = false;
        this.solicitudService.showNitCarnet = false;
        this.solicitudService.showTarjetaUltimosCuatroDigitos = false;
        this.solicitudService.showNroCuenta = false;
        this.solicitudService.showCuentaFechaExpiracion = false;
        this.solicitudService.showOcupacion = false;
        this.solicitudService.showPlan = false;
        this.solicitudService.showPlazo = false;
        this.solicitudService.showAgencia = false;
        this.solicitudService.showUsuarioCargo = false;
        this.solicitudService.showPrima = true;
        this.solicitudService.showSucursal = false;
        this.solicitudService.showMoneda = false;
        this.solicitudService.showCaedec = true;
        this.solicitudService.showLocalidad = true;
        this.solicitudService.showDepartamento = true;
        this.solicitudService.showCodSucursal = true;
        this.solicitudService.showTipoDocumento = true;
        this.solicitudService.showEmail = true;
        this.solicitudService.showEstadoCivil = true;
        this.solicitudService.showManejo = true;
        this.solicitudService.showCodAgenda = true;
        this.solicitudService.showDescCaedec = true;
        this.solicitudService.showZona = false;
        this.solicitudService.showNroDireccion = false;
        if (typeof callback == 'function') {
            callback();
        }
    }

    async getPoliza(callback:Function = null) {
        await this.solicitudService.setIdPoliza(this.solicitudService.parametrosRuteo.parametro_vista);
        await this.polizaService.getPolizaById(this.solicitudService.id_poliza).subscribe(async res => {
            let response = res as { status: string, message: string, data: Poliza };
            this.solicitudService.poliza = response.data;
            await this.solicitudService.getAllParametrosByIdDiccionario([32,33],
                this.solicitudService.poliza.id, [1, 2, 3, 4, 11, 17, 18, 20, 21, 22, 25, 29, 30, 31, 32, 36, 38, 40, 52, 34, 58, 41, 56, 59, 66, 67],async () => {
                    if (this.solicitudService.poliza.anexo_polizas && this.solicitudService.poliza.anexo_polizas.length) {
                        this.solicitudService.poliza.anexo_poliza = this.solicitudService.poliza.anexo_polizas.find(param => param.id_tipo == 288 );
                        this.solicitudService.anexoPoliza = this.solicitudService.poliza.anexo_polizas.find(param => param.id_tipo == 288 );
                        this.solicitudService.anexoAsegurado = this.solicitudService.anexoPoliza.anexo_asegurados.find(param => param.id_tipo == this.solicitudService.parametroAsegurado.id);
                        if(this.solicitudService.anexoAsegurado) {
                            this.solicitudService.isRenovated = this.solicitudService.asegurado.instancia_poliza.id == this.solicitudService.asegurado.instancia_poliza.id_instancia_renovada ? false : true;
                            switch (this.solicitudService.anexoAsegurado.id_edad_unidad) {
                                case this.solicitudService.parametroYear.id:
                                    this.solicitudService.edadMinimaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                                case this.solicitudService.parametroMonth.id:
                                    this.solicitudService.edadMinimaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                                case this.solicitudService.parametroDay.id:
                                    this.solicitudService.edadMinimaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                            }
                        }
                    }
                    if(typeof callback == 'function') {
                        await callback();
                    }
                });
        },err =>{
            if(err.error.statusCode===400 && err.error.message==="usuario no autentificado"){
                this.router.navigate(['']);
            }else{
                console.log(err);
            }
        });
    }

    cambiarSolicitudEstado() {
        this.solicitudService.displayValidacionSinObservaciones = false;
        if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
            this.solicitudService.emitirInstanciaPoliza = false;
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
            this.solicitudService.instanciaDocumentoComprobante = new Instancia_documento();
            this.solicitudService.instanciaDocumentoComprobante.documento = this.solicitudService.documentoComprobante;
            if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                this.solicitudService.instanciaDocumentoComprobante.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
            }
            if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(param => param.id_documento == this.solicitudService.instanciaDocumentoComprobante.id_documento)) {
                this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoComprobante);
            }
            this.solicitudService.emitirInstanciaPoliza = false;
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
            if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoDebitAutomatico.id+'') {
                this.solicitudService.instanciaDocumentoCertificado = new Instancia_documento();
                if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                    this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
                }
                this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
                this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
                this.solicitudService.emitirInstanciaPoliza = true;
            } else if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9') {
                this.solicitudService.instanciaDocumentoCertificado = new Instancia_documento();
                if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                    this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
                }
                this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
                this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
                this.solicitudService.emitirInstanciaPoliza = true;
            } else {
                this.solicitudService.emitirInstanciaPoliza = false;
            }
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
            let transicionSolicitado = new Instancia_poliza_transicion();
            transicionSolicitado.observacion = this.solicitudService.msgText;
            transicionSolicitado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            transicionSolicitado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            transicionSolicitado.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionSolicitado);
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
            this.solicitudService.instanciaDocumentoCertificado = new Instancia_documento();
            // if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
            //     this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
            // }
            // this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
            // this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
            this.solicitudService.emitirInstanciaPoliza = true;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
            let transicionSolicitado = new Instancia_poliza_transicion();
            transicionSolicitado.observacion = this.solicitudService.msgText;
            transicionSolicitado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            transicionSolicitado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            transicionSolicitado.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionSolicitado);
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
            this.solicitudService.instanciaDocumentoCertificado = new Instancia_documento();
            // if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
            //     this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
            // }
            // this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
            // this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
            this.solicitudService.emitirInstanciaPoliza = true;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
            let transicionSolicitado = new Instancia_poliza_transicion();
            transicionSolicitado.observacion = this.solicitudService.msgText;
            transicionSolicitado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            transicionSolicitado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            transicionSolicitado.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionSolicitado);
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
            this.solicitudService.emitirInstanciaPoliza = true;
        }
        if (!this.solicitudService.userFormHasErrors && !this.solicitudService.emitirInstanciaPoliza) {
            this.updateInstanciaPoliza();
        } else if (!this.solicitudService.userFormHasErrors && this.solicitudService.emitirInstanciaPoliza) {
            if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                this.solicitudService.displayEmitirSolicitud = false;
                this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
            } else {
                if (this.solicitudService.atributoSolicitudSci &&
                    this.solicitudService.TiposCreditoConsumo.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                    this.solicitudService.TiposCreditoDisponible.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                    (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9')
                ) {
                    this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
                } else {
                    this.solicitudService.displayEmitirSolicitud = true;
                }
            }
        }
    }

    async updateInstanciaPoliza() {
        if (parseInt(this.solicitudService.atributoDebitoAutomatico.valor) == this.solicitudService.condicionNo.id &&
            this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id &&
            !this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(param => param.id_documento == this.solicitudService.instanciaDocumentoCertificado.id_documento)
        ) {
            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoCertificado);
        }
        this.solicitudService.isLoadingAgain = true;
        if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoEfectivo.id+'') {
            // antiguamente solo EcoResguardo 12
            if ([10,12].includes(parseInt(this.solicitudService.asegurado.instancia_poliza.id_poliza+''))) {
                if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9') {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorEmitir.id,
                        this.solicitudService.estadoEmitido.id
                    ]
                } else {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorEmitir.id,
                        this.solicitudService.estadoPorPagar.id,
                        this.solicitudService.estadoEmitido.id
                    ]
                }
            }
            // else {
            //     if (this.solicitudService.TiposCreditoConsumo.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
            //         this.solicitudService.TiposCreditoDisponible.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
            //         this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9'
            //     ) {
            //         this.solicitudService.estadosUpdateSolicitud = [
            //             this.solicitudService.estadoIniciado.id,
            //             this.solicitudService.estadoSolicitado.id,
            //             this.solicitudService.estadoPorEmitir.id,
            //             this.solicitudService.estadoEmitido.id
            //         ]
            //     } else {
            //         this.solicitudService.estadosUpdateSolicitud = [
            //             this.solicitudService.estadoIniciado.id,
            //             this.solicitudService.estadoSolicitado.id,
            //             this.solicitudService.estadoPorEmitir.id,
            //             this.solicitudService.estadoPorPagar.id,
            //             this.solicitudService.estadoEmitido.id
            //         ]
            //     }
            // }
        } else {
            this.solicitudService.estadosUpdateSolicitud = [
                this.solicitudService.estadoIniciado.id,
                this.solicitudService.estadoSolicitado.id,
                this.solicitudService.estadoPorEmitir.id,
                this.solicitudService.estadoEmitido.id
            ]
        }
        this.instanciaPolizaService.updateInstanciaToNextStatus(this.solicitudService.estadosUpdateSolicitud, this.solicitudService.asegurado).subscribe(async res => {
            let response = res as { status: string, message: string, data: Asegurado };
            this.solicitudService.asegurado = response.data;
            this.stateButtonsFlow();
            this.solicitudService.setDatesOfAsegurado();
            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach((instancia_documento: Instancia_documento) => {
                if (instancia_documento.id_documento == this.solicitudService.documentoSolicitud.id) {
                    this.solicitudService.instanciaDocumentoSolicitud = instancia_documento;
                } else if (instancia_documento.id_documento == this.solicitudService.documentoComprobante.id) {
                    this.solicitudService.instanciaDocumentoComprobante = instancia_documento;
                } else if (instancia_documento.id_documento == this.solicitudService.documentoCertificado.id) {
                    this.solicitudService.instanciaDocumentoCertificado = instancia_documento;
                }
            });
            this.solicitudService.setBeneficiarios();
            this.transicionesComponent.ngOnInit();
            this.componenteBeneficiario.ngOnInit();
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoEfectivo.id +'') {
                    this.solicitudService.msgCambioExitoso = `La solicitud fue actualizada exitosamente a la siguiente etapa ${this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado]} <br><br> En esta etapa debe imprimir la Solicitud del Seguro y hacerla firmar por el cliente, este documento debe digitalizarlo y subirlo al sistema, en la sección "documentos Adjuntos"`;
                } else {
                    this.solicitudService.msgCambioExitoso = `La solicitud fue actualizada exitosamente a la siguiente etapa ${this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado]} <br><br> En esta etapa debe imprimir la Solicitud del Seguro y hacerla firmar por el cliente, este documento debe digitalizarlo y subirlo al sistema, en la sección "documentos Adjuntos"`;
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                this.solicitudService.msgCambioExitoso = 'El cliente debe aproximarse a caja para hacer el pago de la prima y habilitar la emision del certificado de cobertura.';
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                this.solicitudService.msgCambioExitoso = 'El cliente puede aproximarse a plataforma para habilitar la emision del certificado de cobertura.';
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
                this.solicitudService.msgCambioExitoso = `La solicitud fue actualizada exitósamente al siguiente estado ${this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado]}. ¿Desea imprimir el certificado de cobertura Nro: ${this.solicitudService.instanciaDocumentoCertificado.nro_documento+''}.`;
            } else {
                this.solicitudService.msgCambioExitoso = 'La solicitud fue actualizada exitósamente al siguiente estado. ' + this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado];
            }
            //await this.setPlanPago(() => {
            this.solicitudService.displayEmitirSolicitud = false;
            this.solicitudService.displayEmitirSolicitudDesdeCaja = false;
            this.solicitudService.displayCambioEstadoExitoso = true;
            //});
            this.solicitudService.isLoadingAgain = false;
            this.validandoContinuando = false;
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    async setPlanPago(callback: Function = null) {
        if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
            this.solicitudService.isLoadingAgain = true;

            let documentoCertificado = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(param => param.id_documento == this.solicitudService.documentoCertificado.id);
            let atributoCuota = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.solicitudService.atributoNroCuotas.objeto_x_atributo.id_atributo);
            let atributoMonto = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.solicitudService.atributoMonto.objeto_x_atributo.id_atributo);
            let atributoModalidad = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.solicitudService.atributoModalidadPago.objeto_x_atributo.id_atributo);
            let atributoMoneda = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.solicitudService.atributoMoneda.objeto_x_atributo.id_atributo);

            let planPago: Plan_pago = new Plan_pago();

            if (documentoCertificado && documentoCertificado.fecha_emision) {

                planPago.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
                planPago.total_prima = atributoCuota && atributoMonto ? parseInt(atributoCuota.valor) * parseInt(atributoMonto.valor) : 0;
                planPago.interes = 0;
                planPago.id_moneda = parseInt(atributoMoneda.valor);
                planPago.plazo_anos = 1;
                planPago.periodicidad_anual = atributoModalidad && atributoModalidad.valor == this.solicitudService.parMensual.id+'' ? 12 : atributoModalidad.valor == this.solicitudService.parAnual.id+'' ? 1 : 0;
                planPago.prepagable_postpagable = 1;
                planPago.fecha_inicio = documentoCertificado.fecha_emision;
                planPago.adicionado_por = this.solicitudService.asegurado.instancia_poliza.adicionada_por;
                planPago.modificado_por = this.solicitudService.asegurado.instancia_poliza.modificada_por;

                this.planPagoService.GenerarPlanPagos(planPago).subscribe(res => {
                    let response = res as { status: string, message: string, data: any[] };
                    if (response.status == 'ERROR') {
                        this.displayErrorPlanPago = true;
                    }
                    if (typeof callback == 'function') {
                        callback();
                    }
                    this.solicitudService.isLoadingAgain = false;
                });
            } else {
                this.displayErrorPlanPago = true;
            }
        }
    }

    onUpload(event) {
        for (const file of event.files) {
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                this.solicitudService.fileDocumentoSolicitud = file;
            }
            this.solicitudService.uploadedFiles = [this.solicitudService.fileDocumentoSolicitud];
        }
        this.msgs = [];
        this.msgs.push({severity: 'info', summary: 'File Uploaded', detail: ''});
    }

    async validarYContinuar() {
        this.solicitudService.msgs_warn = [];
        this.solicitudService.msgs_error = [];
        this.solicitudService.msgs_info_warn = [];
        this.solicitudService.userFormHasErrors = false;
        this.solicitudService.userFormHasWarnings = false;
        this.validandoContinuando = true;
        await this.solicitudService.setAtributosToAsegurado(async () => {
            await this.solicitudService.setBeneficiarios();
            await this.stateButtonsFlow();
            if (this.transicionesComponent != undefined) {
                this.transicionesComponent.ngOnInit();
            }
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                this.solicitudService.mostrarObservaciones = false;
                this.solicitudService.mostrarAdvertencias = true;
                if (this.solicitudService.msgs_warn.length || this.solicitudService.msgs_info_warn.length) {
                    this.solicitudService.msgText = `No tiene observaciones, puede continuar con la instrumentación del seguro. <br><br> Tenga en cuenta que la solicitud de crédito seleccionada para la instrumentacion del seguro es ${this.solicitudService.atributoSolicitudSci.valor}, cuyo plazo es: ${this.solicitudService.atributoSolicitudPlazoCredito.valor} meses`;
                } else {
                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.'
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                this.solicitudService.mostrarObservaciones = false;
                if (this.solicitudService.msgs_warn.length) {
                    this.solicitudService.msgText = `No tiene observaciones, puede continuar con la instrumentación del seguro.`;
                } else {
                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.'
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                this.solicitudService.mostrarObservaciones = false;
                if (this.solicitudService.msgs_warn.length) {
                    this.solicitudService.msgText = `No tiene observaciones, puede continuar con la instrumentación del seguro.`;
                } else {
                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.'
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                this.solicitudService.mostrarObservaciones = false;
                if (this.solicitudService.msgs_warn.length) {
                    this.solicitudService.msgText = `No tiene observaciones, puede continuar con la instrumentación del seguro. <br><br> Tenga en cuenta que a partir de la siguiente etapa ya no podrá modificar los datos que fueron introducidos.`;
                } else {
                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.'
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                this.solicitudService.mostrarObservaciones = false;
                if (this.solicitudService.msgs_warn.length) {
                    this.solicitudService.msgText = `No tiene observaciones, puede continuar con la instrumentación del seguro. <br><br> Tenga en cuenta que a partir de la siguiente etapa ya no podrá modificar los datos que fueron introducidos.`;
                } else {
                    this.solicitudService.msgText = 'No tiene observaciones ni advertencias, por lo que se pasará a la siguiente etapa.'
                }
            } else {
                this.solicitudService.mostrarObservaciones = false;
                this.solicitudService.msgText = 'No tiene observaciones ni advertencias,  por lo que se la pasará a la siguiente etapa.';
            }

            if (this.solicitudService.userFormHasErrors) {
                this.withErrorsOrWarnings = true;
            } else if (this.solicitudService.userFormHasWarnings) {
                this.withErrorsOrWarnings = false;
            }

            if (this.solicitudService.userFormHasErrors) {
                this.solicitudService.displayValidacionConObservaciones = true;
                this.solicitudService.displayValidacionSinObservaciones = false;
            } else if (this.solicitudService.userFormHasWarnings) {
                this.solicitudService.displayValidacionConObservaciones = false;
                this.solicitudService.displayValidacionSinObservaciones = true;
            } else if (this.solicitudService.userFormHasInfoWarnings) {
                this.solicitudService.displayValidacionConObservaciones = false;
                this.solicitudService.displayValidacionInfoWarnings = true;
            } else {
                this.solicitudService.displayValidacionConObservaciones = false;
                this.solicitudService.displayValidacionSinObservaciones = true;
            }
        });
    }

    validateForm() {
        console.log(this.solicitudService.getFormValidationErrors(this.userform));
        console.log('atributoCondicionPep', this.solicitudService.atributoCondicionPep.requerido);
    }

    disableForm(form: FormGroup) {
        for (var control in form.controls) {
            if (form.controls[control]) {
                form.controls[control].disable();
            }
        }
        setTimeout(() => {
            $('.ui-message').css('display', 'none')
        }, 200);
    }

    enableUserform() {
        for (var control in this.userform.controls) {
            if (
                control == 'par_tipo_documento_id' ||
                control == 'persona_direccion_domicilio' ||
                control == 'persona_doc_id_comp' ||
                control == 'par_pais_nacimiento_id' ||
                control == 'persona_telefono_trabajo' ||
                control == 'persona_telefono_domicilio' ||
                control == 'par_debito_automatico_id' ||
                control == 'submit' ||
                control == 'par_moneda' ||
                control == 'par_numero_cuenta'
            ) {
                if (this.userform.controls[control]) {
                    this.userform.controls[control].enable();
                }
            }
        }
    }

    // setSolicitudAsIniciado() {
    //     if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
    //         this.solicitudService.btnEmitirCertificadoEnabled = false;
    //         this.btnEnviarDocumentosEnabled = false;
    //         this.solicitudService.btnImprimirSolicitudEnabled = false;
    //         if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
    //             this.solicitudService.showOrdenPago = false;
    //         } else {
    //             this.solicitudService.showOrdenPago = false;
    //         }
    //         this.solicitudService.btnOrdenPago(false);
    //         if (this.solicitudService.hasRolConsultaTarjetas || this.solicitudService.hasRolConsultaCajero) {
    //             this.btnRefrescarInformacion = false;
    //         } else {
    //             this.btnRefrescarInformacion = true;
    //         }
    //     }
    // }

    stateButtonsFlow() {
        this.solicitudService.showFormValidation(this.userform);
        this.solicitudService.validarBeneficiarios(this.componenteBeneficiario);
        this.solicitudService.validarArchivos();
        this.solicitudService.validarWarnsOrErrors();
        // this.solicitudService.setFeatureValidacionAlInicioPersonaBanco();
        this.solicitudService.btnValidarYContinuarEnabled = true;
        this.solicitudService.btnRefrescarInformacion = true;
        this.btnTarjetaDebitoCuentasEnabled = true;
        this.solicitudService.showOrdenPago = false;
        this.solicitudService.btnOrdenPago(false);
        if (this.solicitudService.asegurado.instancia_poliza != null &&
            this.solicitudService.atributoDebitoAutomatico != undefined &&
            this.solicitudService.condicionSi != undefined &&
            this.solicitudService.condicionNo != undefined
        ) {
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? true : false;
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = false;
                }
                this.solicitudService.showBtnImprimirCarta = false;
                this.solicitudService.btnCartaEnabled = false;
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = true;
                this.solicitudService.btnRefrescarInformacion = true;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                this.btnEnviarDocumentosEnabled = false;
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                    this.solicitudService.showOrdenPago = false;
                } else {
                    this.solicitudService.showOrdenPago = false;
                }
                this.solicitudService.btnOrdenPago(false);
                // if (this.solicitudService.hasRolConsultaTarjetas || this.solicitudService.hasRolConsultaCajero) {
                //     this.btnRefrescarInformacion = false;
                // } else {
                // }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor) {
                    if (this.solicitudService.TiposCreditoConsumo.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                        this.solicitudService.TiposCreditoDisponible.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                        (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9')
                    ) {
                        this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                        this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
                        this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? true : false;
                    } else {
                        this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                        this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
                        this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? true : false;
                    }
                } else {
                    this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                    this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
                    this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? true : false;
                }
                this.solicitudService.showBtnImprimirCarta = false;
                this.solicitudService.btnCartaEnabled = false;
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                this.btnEnviarDocumentosEnabled = true;
                this.solicitudService.btnRefrescarInformacion = false;
                this.solicitudService.btnImprimirSolicitudEnabled = true;
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                    this.solicitudService.showOrdenPago = false;
                } else {
                    this.solicitudService.showOrdenPago = false;
                }
                this.solicitudService.btnOrdenPago(false);
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                if(this.solicitudService.hasRolPlataforma) {
                    this.solicitudService.btnOrdenPago(true);
                    if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.pagoEfectivo.id+'') {
                        this.solicitudService.showOrdenPago = true;
                    } else {
                        this.solicitudService.showOrdenPago = false;
                    }
                }
                this.solicitudService.btnOrdenPagoEnabled = false;
                if (this.solicitudService.TiposCreditoConsumo.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                    this.solicitudService.TiposCreditoDisponible.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                    (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9')
                ) {
                    this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                    this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? false : true;
                    this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? true : false;
                    if(this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9') {
                        this.solicitudService.btnOrdenPago(false);
                        this.solicitudService.showOrdenPago = false;
                    }
                    this.solicitudService.showBtnImprimirCarta = this.solicitudService.hasRolPlataforma ? true : false;
                    this.solicitudService.btnCartaEnabled = this.solicitudService.hasRolPlataforma ? true : false;
                } else {
                    this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                    this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? false : true;
                    this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? true : false;
                    this.solicitudService.showBtnImprimirCarta = false;
                    this.solicitudService.btnCartaEnabled = false;
                }
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                // this.solicitudService.btnCartaEnabled = true;
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                this.btnEnviarDocumentosEnabled = true;
                this.solicitudService.btnRefrescarInformacion = false;
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? true : false;
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                this.solicitudService.btnRefrescarInformacion = false;
                this.btnEnviarDocumentosEnabled = true;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                    this.solicitudService.showOrdenPago = false;
                } else {
                    this.solicitudService.showOrdenPago = false;
                }
                this.solicitudService.showOrdenPago = this.solicitudService.hasRolPlataforma ? true : false;
                this.solicitudService.btnOrdenPagoEnabled = this.solicitudService.hasRolPlataforma ? true : false;
                this.solicitudService.btnOrdenPago(true);
            }
            /*else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorEmitir.id) {
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCredito ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolCajero ? false : true;
                this.solicitudService.btnValidarYContinuarEnabled = this.solicitudService.hasRolPlataforma ? true : false;
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                this.solicitudService.btnRefrescarInformacion = false;
                this.btnEnviarDocumentosEnabled = true;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                this.solicitudService.btnOrdenPago(false);
            }*/
            else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = true;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                if (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor) {
                    if (this.solicitudService.TiposCreditoConsumo.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                        this.solicitudService.TiposCreditoDisponible.find(param => this.solicitudService.atributoTipoCredito.valor.includes(param.value)) ||
                        (this.solicitudService.atributoSolicitudSci && this.solicitudService.atributoSolicitudSci.valor && this.solicitudService.atributoSolicitudSci.valor.substring(0,1) == '9')
                    ) {
                        this.solicitudService.showBtnImprimirCarta = this.solicitudService.hasRolPlataforma || this.solicitudService.hasRolCredito ? true : false;
                        this.solicitudService.btnCartaEnabled = this.solicitudService.hasRolPlataforma || this.solicitudService.hasRolCredito ? true : false;
                    } else {
                        this.solicitudService.showBtnImprimirCarta = false;
                        this.solicitudService.btnCartaEnabled = false;
                    }
                } else {
                    this.solicitudService.showBtnImprimirCarta = false;
                    this.solicitudService.btnCartaEnabled = false;
                }
                this.btnEnviarDocumentosEnabled = true;
                this.solicitudService.btnOrdenPago(true);
                this.solicitudService.btnValidarYContinuarEnabled = false;
                this.solicitudService.btnRefrescarInformacion = false;
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoAnulado.id ) {
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                this.solicitudService.showBtnImprimirDesistimiento = false;
                this.solicitudService.btnCartaDesistimiento = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                this.btnEnviarDocumentosEnabled = false;
                this.solicitudService.btnOrdenPago(false);
                this.solicitudService.btnValidarYContinuarEnabled = false;
                this.solicitudService.btnRefrescarInformacion = false;
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoDesistido.id) {
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.showBtnImprimirDesistimiento = true;
                this.solicitudService.btnCartaDesistimiento = true;
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                this.btnEnviarDocumentosEnabled = false;
                this.solicitudService.btnOrdenPago(false);
                this.solicitudService.btnValidarYContinuarEnabled = false;
                this.solicitudService.btnRefrescarInformacion = false;
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSinVigencia.id) {
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.showBtnImprimirDesistimiento = false;
                this.solicitudService.btnCartaDesistimiento = false;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                this.btnEnviarDocumentosEnabled = false;
                this.solicitudService.btnOrdenPago(false);
                this.solicitudService.btnValidarYContinuarEnabled = false;
                this.solicitudService.btnRefrescarInformacion = false;
            } else {
                this.solicitudService.btnOrdenPago(false);
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnRefrescarInformacionEnabled = false;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
            }
        }
    }

    async refrescarInformacion(callback:any = null) {
        this.solicitudService.isLoadingAgain = true;
        this.solicitudService.editandoTitular = true;
        this.getDatosFromCustomerService(this.solicitudService.persona_banco.doc_id, this.solicitudService.persona_banco.extension, async res => {
            if (this.solicitudService.persona_banco_solicitud && this.solicitudService.persona_banco_solicitud.solicitudes_sci.length) {
                this.solicitudService.SolicitudesSci = [];
                this.solicitudService.SolicitudesSci.push({label: "Seleccione una solicitud", value: null});
                for (let i = 0; i < this.solicitudService.persona_banco_solicitud.solicitudes_sci.length; i++) {
                    let persona_banco_solicitud_sci = this.solicitudService.persona_banco_solicitud.solicitudes_sci[i];
                    await this.solicitudService.SolicitudesSci.push({
                        label: persona_banco_solicitud_sci,
                        value: persona_banco_solicitud_sci,
                    });
                }
            } else {
                this.solicitudService.persona_banco_solicitud = new persona_banco_solicitud();
            }
            await this.solicitudService.setAtributosToAsegurado(async => {
                this.stateButtonsFlow();
                this.solicitudService.persona_banco.debito_automatico = this.solicitudService.atributoDebitoAutomatico.valor;
                this.solicitudService.persona_banco_account.moneda = this.solicitudService.atributoMoneda.valor;
                this.solicitudService.persona_banco_datos.nro_tarjeta = this.solicitudService.atributoNroTarjeta.valor;
                this.solicitudService.persona_banco_account.nrocuenta = this.solicitudService.atributoNroCuenta.valor;
                this.solicitudService.persona_banco_datos.zona = this.solicitudService.atributoZona.valor;
                this.solicitudService.persona_banco_datos.nro_direccion = this.solicitudService.atributoNroDireccion.valor;
                this.solicitudService.persona_banco.e_mail = this.solicitudService.atributoEmail.valor;
                this.solicitudService.persona_banco_tarjeta_debito.tarjeta_ultimos_cuatro_digitos = this.solicitudService.atributoUltimosCuatroDigitos.valor;
                this.solicitudService.persona_banco_datos.nit_carnet = this.solicitudService.atributoNitCarnet.valor;
                this.solicitudService.persona_banco_datos.razon_social = this.solicitudService.atributoRazonSocial.valor;
                this.solicitudService.persona_banco_datos.ocupacion = this.solicitudService.atributoOcupacion.valor;
                this.solicitudService.persona_banco_datos.tipo_doc = this.solicitudService.atributoTipoDoc.valor;
                this.solicitudService.persona_banco_datos.amp_con_amb_med_general = this.solicitudService.atributoAmpConAmbMedGeneral.valor;
                this.solicitudService.persona_banco_datos.amp_con_amb_med_especializada = this.solicitudService.atributoAmpConAmbMedEspecializada.valor;
                this.solicitudService.persona_banco_datos.amp_sum_amb_medicamentos = this.solicitudService.atributoAmpSumAmbMedicamentos.valor;
                this.solicitudService.persona_banco_datos.amp_sum_exa_laboratorio = this.solicitudService.atributoAmpSumExaLaboratorio.valor;
                this.solicitudService.persona_banco_datos.plan = parseInt(this.solicitudService.atributoPlan.valor);
                this.solicitudService.persona_banco_transaccion.moneda = this.solicitudService.atributoTransaccionMoneda.valor;
                this.solicitudService.persona_banco_transaccion.detalle = this.solicitudService.atributoTransaccionDetalle.valor;
                this.solicitudService.persona_banco_transaccion.importe = this.solicitudService.atributoTransaccionImporte.valor;
                this.solicitudService.persona_banco_transaccion.fechatran = this.solicitudService.atributoFechaTransaccion.valor;
                this.solicitudService.persona_banco_solicitud.solicitud_sci_selected = this.solicitudService.atributoSolicitudSci.valor;
                this.solicitudService.persona_banco_operacion.Operacion_Tipo_Credito = this.solicitudService.atributoTipoCredito.valor;
                this.solicitudService.persona_banco_operacion.Operacion_Plazo = parseInt(this.solicitudService.atributoSolicitudPlazoCredito.valor+'');
                this.solicitudService.persona_banco_solicitud.solicitud_prima_total = this.solicitudService.atributoSolicitudPrimaTotal.valor;
                this.solicitudService.persona_banco_solicitud.solicitud_forma_pago = this.solicitudService.atributoFormaPago.valor;
                this.solicitudService.persona_banco_datos.plazo = parseInt(this.solicitudService.atributoPlazo.valor);
                if (this.solicitudService.usuario_banco) {
                    this.solicitudService.usuario_banco.us_sucursal = parseInt(this.solicitudService.atributoSucursal.valor);
                    this.solicitudService.usuario_banco.us_oficina = parseInt(this.solicitudService.atributoAgencia.valor);
                    this.solicitudService.usuario_banco.us_cargo = this.solicitudService.atributoUsuarioCargo.valor;
                }
                this.solicitudService.persona_banco_datos.modalidad_pago = this.solicitudService.atributoModalidadPago.valor;
                this.solicitudService.persona_banco_datos.prima = parseInt(this.solicitudService.atributoPrima.valor);
                this.solicitudService.persona_banco_datos.telefono_celular = this.solicitudService.atributoTelefonoCelular.valor;
                this.solicitudService.persona_banco_datos.ciudad_nacimiento = this.solicitudService.atributoCiudadNacimiento.valor;
                this.solicitudService.persona_banco_datos.direccion_laboral = this.solicitudService.atributoDireccionLaboral.valor;
                this.solicitudService.persona_banco_pep.condicion = this.solicitudService.atributoCondicionPep.valor;
                this.solicitudService.persona_banco_pep.cargo_entidad = this.solicitudService.atributoCargoEntidadPep.valor;
                this.solicitudService.persona_banco_pep.periodo_cargo_publico = this.solicitudService.atributoPeriodoCargoPublico.valor;
                this.solicitudService.persona_banco_tarjeta_debito.fecha_expiracion = new Date(this.solicitudService.atributoCtaFechaExpiracion.valor);
                this.solicitudService.aseguradoBeneficiarios = this.solicitudService.asegurado.beneficiarios;
                // this.solicitudService.setBeneficiarios();
                // this.validarTarjetaYGuardarSolicitud(() => {
                this.solicitudService.asegurado.beneficiarios = this.solicitudService.aseguradoBeneficiarios;
                this.solicitudService.setBeneficiarios();
                this.displayActualizacionExitoso = true;
                this.solicitudService.isLoadingAgain = false;
                // });
                this.guardarSolicitudValidando();
            });
        })
    }

    async getDatosFromSolicitudPrimeraEtapaService(solicitud_sci_selected:string, callback: Function = null) {
            this.solicitudService.isLoadingAgain = true;
            if (solicitud_sci_selected && solicitud_sci_selected.substring(0, 1) == '9') {
                this.solicitudService.persona_banco.debito_automatico = this.solicitudService.pagoDebitAutomatico.id + '';
                this.solicitudService.pagoACredito = true;
                this.solicitudService.editPagoEfectivo = false;
            } else {
                this.solicitudService.pagoACredito = false;
                this.solicitudService.persona_banco.debito_automatico = this.solicitudService.pagoEfectivo.id + '';
            }
            this.solicitudService.persona_banco_solicitud.solicitud_forma_pago = this.solicitudService.parametroPagoContado.id + '';
            if (solicitud_sci_selected) {
                await this.soapuiService.getSolicitudPrimeraEtapa(solicitud_sci_selected).subscribe(async res => {
                    this.solicitudService.isLoadingAgain = false;
                    let response = res as { status: string, message: string, data: persona_banco_operacion };
                    if (isObject(response.data) && Object.keys(response.data).length) {
                        this.solicitudService.persona_banco_operacion = response.data;
                        this.solicitudService.persona_banco_operaciones.push(this.solicitudService.persona_banco_operacion);
                            this.solicitudService.persona_banco_solicitud.solicitud_prima_total = (this.solicitudService.poliza.anexo_poliza.monto_prima * (this.solicitudService.persona_banco_operacion.Operacion_Plazo > 60 ? 60 : this.solicitudService.persona_banco_operacion.Operacion_Plazo)) + '';
                            this.solicitudService.setAtributosToAsegurado();
                            if (this.solicitudService.persona_banco_operacion.Operacion_Estado && this.solicitudService.persona_banco_operacion.Operacion_Estado.includes('APROBAD')) {
                                this.solicitudService.isLoadingAgain = false;
                                this.solicitudService.stopSavingBySolicitud = false;
                                this.solicitudService.solicitudAprobada = true;
                                this.solicitudService.solicitudAjena = false;
                            } else {
                                let validacion = this.solicitudService.msgSolicitudSciInvalida = `Advertencia: El Nro de solicitud de crédito seleccionado: ${solicitud_sci_selected}, no fue aprobado`;
                                this.solicitudService.solicitudAjena = false;
                                if (
                                    !this.solicitudService.msgs_error.find((param) => param.detail == validacion) &&
                                    this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id
                                ) {
                                    this.solicitudService.setMsgsWarnsOrErrors(this.solicitudService.asegurado, this.solicitudService.parametroError, validacion);
                                }
                                this.solicitudService.solicitudAprobada = false;
                                this.solicitudService.stopSavingBySolicitud = true;
                                this.solicitudService.displaySolicitudSciInvalida = false;
                            }
                            if(this.solicitudService.persona_banco_operacion.Operacion_Usuario_Oficial && this.solicitudService.persona_banco_operacion.Operacion_Usuario_Oficial.includes(this.solicitudService.usuarioLogin.usuario_login)) {
                                this.solicitudService.solicitudAjena = false;
                                this.solicitudService.displaySolicitudSciInvalida = false;
                            } else {
                                this.solicitudService.solicitudAjena = true;
                                let validacion = this.solicitudService.msgSolicitudSciInvalida = `Advertencia: El Nro de solicitud de crédito seleccionado: ${solicitud_sci_selected}, pertenece al usuario ${this.solicitudService.persona_banco_operacion.Operacion_Usuario_Oficial}, su usuario actual es: ${this.solicitudService.usuarioLogin.usuario_login}`;
                                if (
                                    !this.solicitudService.msgs_error.find((param) => param.detail == validacion) &&
                                    this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id
                                ) {
                                    this.solicitudService.setMsgsWarnsOrErrors(this.solicitudService.asegurado, this.solicitudService.parametroError, validacion);
                                }
                                this.solicitudService.displaySolicitudSciInvalida = true;
                            }
                            this.solicitudService.disableToSave = false;
                        if (typeof callback == 'function') {
                            await callback(this.solicitudService.solicitudAprobada, this.solicitudService.solicitudAjena, solicitud_sci_selected);
                        }
                    } else {
                        this.solicitudService.solicitudAprobada = false;
                        this.solicitudService.stopSavingBySolicitud = true;
                        this.solicitudService.displaySolicitudSciInvalida = true;
                        this.solicitudService.persona_banco_operacion = new persona_banco_operacion();
                        this.solicitudService.msgSolicitudSciInvalida = 'Existen problemas de comunicacion con el sistema del banco, por favor comunicate con el administrador del sistema';
                        if (typeof callback == 'function') {
                            await callback(this.solicitudService.solicitudAprobada);
                        }
                    }
                },err =>{
                    if(err.error.statusCode===400 && err.error.message==="usuario no autentificado"){
                        this.router.navigate(['']);
                    } else {
                        this.solicitudService.nombreServicioBanco = 'getSolicitudPrimeraEtapa';
                        this.solicitudService.displayErrorRespuestaBanco = true;
                    }
                });
            } else {
                this.solicitudService.isLoadingAgain = false;
                if (typeof callback == 'function') {
                    await callback(this.solicitudService.solicitudAprobada);
                }
            }
    }

    async validaDatosFromSolicitudPrimeraEtapaServiceSelected(callback: Function = null) {
        let toContinue = false;
        this.solicitudesObservaciones = [];
        this.solicitudService.SolicitudesSci = [];
        this.solicitudService.SolicitudesSci.push({label: "Seleccione una solicitud", value: null});
        for (let i = 0; i < this.solicitudService.persona_banco_solicitud.solicitudes_sci.length; i++) {
            let solicitudSci = this.solicitudService.persona_banco_solicitud.solicitudes_sci[i];
            this.solicitudService.SolicitudesSci.push({label: solicitudSci, value: solicitudSci});
            if (this.solicitudService.persona_banco_solicitud.solicitud_sci_selected && this.solicitudService.persona_banco_solicitud.solicitud_sci_selected.substring(0, 1) == '9') {
                this.solicitudService.persona_banco.debito_automatico = this.solicitudService.pagoDebitAutomatico.id + '';
                this.solicitudService.pagoACredito = true;
                this.solicitudService.editPagoEfectivo = false;
            } else {
                this.solicitudService.persona_banco_solicitud.solicitud_sci_selected = this.solicitudService.persona_banco_solicitud.solicitud_sci_selected ? this.solicitudService.persona_banco_solicitud.solicitud_sci_selected : solicitudSci;
                this.solicitudService.pagoACredito = false;
                this.solicitudService.persona_banco.debito_automatico = this.solicitudService.pagoEfectivo.id + '';
            }
            this.solicitudService.persona_banco_solicitud.solicitud_forma_pago = this.solicitudService.parametroPagoContado.id + '';
            // await this.getDatosFromSolicitudPrimeraEtapaService(solicitudSci, async (solicitudAprobada, solicitudAjena, solicitudSci) => {
            //     this.solicitudesObservaciones.push({solicitudAprobada, solicitudAjena, solicitudSelected:this.solicitudService.persona_banco_solicitud.solicitud_sci_selected});
            //     if (typeof callback == 'function') {
            //         await callback(toContinue, this.solicitudesObservaciones, i == this.solicitudService.persona_banco_solicitud.solicitudes_sci.length-1);
            //     }
            // });
            if (typeof callback == 'function') {
                await callback(toContinue, this.solicitudesObservaciones, i == this.solicitudService.persona_banco_solicitud.solicitudes_sci.length-1);
            }
        }
    }

    async getDatosFromCustomerService(docId:string = null, extension:string = null, callback: Function = null) {
        this.solicitudService.esClienteBanco = false;
        await this.soapuiService.getCustomerSol(
            this.solicitudService.doc_id ? this.solicitudService.doc_id : docId,
            this.solicitudService.extension ? parseInt(this.solicitudService.extension) : parseInt(extension)
        ).subscribe(async res => {
            let response = res as { status: string, message: string, data: any };
            if (response.data && Object.keys(response.data).length) {

                this.solicitudService.persona_banco = this.solicitudService.persona_banco_bkp = response.data.general;
                this.solicitudService.persona_banco.extension = this.solicitudService.persona_banco.extension == '' ? '12' : this.solicitudService.persona_banco.extension
                this.solicitudService.persona_banco_datos = new persona_banco_datos();
                this.solicitudService.persona_banco_solicitud = new persona_banco_solicitud();
                this.solicitudService.persona_banco_operacion = new persona_banco_operacion();
                this.solicitudService.persona_banco_solicitud = response.data.solicitud;
                this.solicitudService.setAtributosToAsegurado(async () => {
                    if (this.solicitudService.persona_banco && isObject(this.solicitudService.persona_banco) && Object.keys(this.solicitudService.persona_banco).length) {
                        this.solicitudService.showMoneda = false;
                        this.solicitudService.showNroCuenta = false;
                        this.solicitudService.showTipoCuenta = false;
                        this.solicitudService.esClienteBanco = true;
                        if (this.solicitudService.persona_banco_solicitud && this.solicitudService.persona_banco_solicitud.solicitudes_sci.length) {
                            await this.validaDatosFromSolicitudPrimeraEtapaServiceSelected(async (toContinue, solicitudesObservaciones, loopFinish) => {
                                if (loopFinish) {
                                    if (solicitudesObservaciones.find(param => param.solicitudAjena == true)){
                                        toContinue = false
                                    } else {
                                        toContinue = true
                                    }
                                    if (this.solicitudService.persona_banco_operaciones.find(param => param.Operacion_Usuario_Oficial != this.solicitudService.userInfo.usuario_login)){
                                        toContinue = false
                                    } else {
                                        toContinue = true
                                    }
                                    let personaBancoOperacion = this.solicitudService.persona_banco_operaciones.find(param => param.Operacion_Solicitud == this.solicitudService.persona_banco_solicitud.solicitud_sci_selected);
                                    this.solicitudService.persona_banco_operacion = personaBancoOperacion ? personaBancoOperacion : this.solicitudService.persona_banco_operacion;
                                    this.solicitudService.conCreditoAsociado = true;
                                    this.solicitudService.persona_banco.debito_automatico = '';
                                    if ([10, 12].includes(parseInt(this.solicitudService.id_poliza+''))) {
                                        this.solicitudService.persona_banco_accounts = [];
                                        this.solicitudService.Cuentas = [];
                                        this.solicitudService.persona_banco_account = new persona_banco_account();
                                        if (typeof callback == 'function') {
                                            await callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado);
                                        }
                                    } else {
                                        if (typeof callback == 'function') {
                                            await callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado);
                                        }
                                    }
                                }
                            });
                        } else {
                            this.solicitudService.conCreditoAsociado = false;
                            if (typeof callback == 'function') {
                                await callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado);
                            }
                            this.solicitudService.isLoadingAgain = false;
                        }
                    } else {
                        if (typeof callback == 'function') {
                            await callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado);
                        }
                        this.solicitudService.isLoadingAgain = false;
                    }
                });
            } else {
                this.solicitudService.nombreServicioBanco = 'getCustomerSol';
                this.solicitudService.displayErrorRespuestaBanco = true;
                this.solicitudService.isLoadingAgain = false;
            }
        },err =>{
            if(err.error.statusCode===400 && err.error.message==="usuario no autentificado"){
                this.router.navigate(['']);
            } else {
                this.solicitudService.nombreServicioBanco = 'getCustomerSol';
                this.solicitudService.displayErrorRespuestaBanco = true;
            }
        });
    }

    async getDatosFromAccountService(callback: Function = null) {
        this.solicitudService.isLoadingAgain = true;
        await this.soapuiService.getAccount(this.solicitudService.persona_banco.cod_agenda).subscribe(async res => {
            this.solicitudService.Cuentas = [{label: "Seleccione Nro de Cuenta", value: null}];
            let response = res as { status: string, message: string, data: any };
            this.solicitudService.isLoadingAgain = false;
            if (isObject(response.data) && Object.keys(response.data).length) {
                this.solicitudService.persona_banco_accounts = [];
                if (response.data.nrocuenta != undefined) {
                    //this.solicitudService.persona_banco_account = response.data;
                    this.solicitudService.persona_banco_accounts.push(response.data);
                    this.solicitudService.Cuentas.push({
                        label: response.data.nrocuenta + ' ' + this.solicitudService.MonedasParametroCod[response.data.moneda+''],
                        value: response.data.nrocuenta
                    });
                } else {
                    let keys = Object.keys(response.data);
                    let values = Object.values(response.data);
                    await keys.forEach((key) => {
                        this.solicitudService.Cuentas.push({
                            label: values[key].nrocuenta + ' ' + this.solicitudService.MonedasParametroCod[values[key].moneda],
                            value: values[key].nrocuenta
                        });
                        this.solicitudService.persona_banco_accounts.push(values[key]);
                    });
                }
                if (this.solicitudService.Cuentas.length) {
                    this.solicitudService.esClienteBanco = true;
                    this.solicitudService.tieneCuentaBanco = true;
                } else {
                    this.solicitudService.esClienteBanco = false;
                    this.solicitudService.tieneCuentaBanco = false;
                    this.solicitudService.displayClienteSinCuenta = true;
                }
            } else {
                this.solicitudService.persona_banco_accounts = [];
                this.solicitudService.Cuentas = [];
            }
            this.solicitudService.persona_banco_account = new persona_banco_account();
            if (typeof callback == 'function') {
                await callback(this.solicitudService.esClienteBanco, this.solicitudService.conCreditoAsociado, this.solicitudService.tieneCuentaBanco);
            }
        },err =>{
            if(err.error.statusCode===400 && err.error.message==="usuario no autentificado"){
                this.router.navigate(['']);
            } else {
                this.solicitudService.nombreServicioBanco = 'getAccount';
                this.solicitudService.displayErrorRespuestaBanco = true;
            }
        });
    }

    async getDatosFromTarjetaDebitoCuentaService(callback: Function = null) {
        if (this.solicitudService.persona_banco_datos.nro_tarjeta) {
            this.solicitudService.isLoadingAgain = true;
            await this.soapuiService.getTarjetaDebitoCuentas(this.solicitudService.persona_banco_datos.nro_tarjeta+'').subscribe(async res => {
                this.solicitudService.isLoadingAgain = false;
                this.persona_banco_tarjetas_debito = [];
                let response = res as { status: string, message: string, data: any };
                if (isObject(response.data) && Object.keys(response.data).length) {
                    if (response.data.Cuentas != undefined) {
                        this.solicitudService.persona_banco_accounts = [];
                        if (!this.guardandoTitular && !this.solicitudService.editandoTitular) {
                            this.displayNroCuentasActualizados = true;
                        }
                        if (response.data.Cuentas.nro_cuenta != undefined) {
                            this.solicitudService.Cuentas = [{label: "Seleccione Nro de Cuenta", value: null}];
                            this.persona_banco_tarjetas_debito.push(response.data.Cuentas);
                            this.parametroMoneda = this.solicitudService.parametrosMonedas.find(params => params.parametro_descripcion == response.data.Cuentas.moneda.trim());
                            const personaBancoAccount: persona_banco_account = new persona_banco_account();
                            personaBancoAccount.nrocuenta = response.data.Cuentas.nro_cuenta;
                            personaBancoAccount.moneda = this.parametroMoneda.parametro_cod;
                            personaBancoAccount.fecha_expiracion = new Date(response.data.Cuentas.fecha_expiracion);
                            personaBancoAccount.cod_agenda = this.solicitudService.persona_banco.cod_agenda;
                            this.solicitudService.persona_banco_accounts.push(personaBancoAccount);
                            this.solicitudService.persona_banco_account.fecha_expiracion = new Date(response.data.Cuentas.fecha_expiracion);
                            this.solicitudService.Cuentas.push({
                                label: personaBancoAccount.nrocuenta + ' ' + this.solicitudService.MonedasParametroCod[personaBancoAccount.moneda+''],
                                value: personaBancoAccount.nrocuenta
                            });
                        } else {
                            this.solicitudService.Cuentas = [{label: "Seleccione Nro de Cuenta", value: null}];
                            let keys = Object.keys(response.data.Cuentas);
                            let values = Object.values(response.data.Cuentas);
                            values.forEach((value: persona_banco_tarjeta_debito) => {
                                this.persona_banco_tarjetas_debito.push(value);
                                this.parametroMoneda = this.solicitudService.parametrosMonedas.find(params => params.parametro_descripcion == value.moneda.trim());
                                const personaBancoAccount: persona_banco_account = new persona_banco_account();
                                personaBancoAccount.nrocuenta = value.nro_cuenta;
                                personaBancoAccount.moneda = this.parametroMoneda.parametro_cod;
                                personaBancoAccount.fecha_expiracion = new Date(value.fecha_expiracion + '');
                                personaBancoAccount.cod_agenda = this.solicitudService.persona_banco.cod_agenda;
                                this.solicitudService.persona_banco_accounts.push(personaBancoAccount);
                                this.solicitudService.persona_banco_account.fecha_expiracion = new Date(value.fecha_expiracion + '');
                                this.solicitudService.Cuentas.push({
                                    label: personaBancoAccount.nrocuenta + ' ' + this.solicitudService.MonedasParametroCod[personaBancoAccount.moneda+''],
                                    value: personaBancoAccount.nrocuenta
                                });
                            });
                        }
                    }
                } else {
                    if (this.solicitudService.persona_banco.cod_agenda != '' && this.solicitudService.persona_banco.cod_agenda != undefined && this.solicitudService.persona_banco.cod_agenda != "undefined") {
                        await this.getDatosFromAccountService(res => {
                            this.persona_banco_tarjetas_debito = [];
                            this.displayNroTarjetaSinCuentas = true;
                            if (typeof callback == 'function') {
                                callback();
                            }
                        });
                    } else {
                        this.solicitudService.persona_banco_accounts = [];
                        this.solicitudService.Cuentas = [];
                        this.solicitudService.persona_banco_account = new persona_banco_account();
                        if (typeof callback == 'function') {
                            await callback();
                        }
                    }
                }
                if (typeof callback == 'function') {
                    await callback();
                }
            },err =>{
                if(err.error.statusCode===400 && err.error.message==="usuario no autentificado"){
                    this.router.navigate(['']);
                } else {
                    this.solicitudService.nombreServicioBanco = 'getTarjetaDebitoCuentas';
                    this.solicitudService.displayErrorRespuestaBanco = true;
                }
            });
        } else {
            this.displayIntroduscaNroTarjeta = true;
        }
    }

    async setAnexoPoliza() {
        await this.anexoService.findAnexoPoliza(this.solicitudService.poliza.id).subscribe(async res => {
            let response = res as { status: string, message: string, data: Anexo_poliza[] };
            this.solicitudService.anexosPoliza = response.data;
            this.solicitudService.anexosPoliza.forEach((anexoPoliza: Anexo_poliza) => {
                this.solicitudService.Planes.push({
                    label: anexoPoliza.descripcion,
                    value: parseInt(anexoPoliza.id+'')
                });
                this.solicitudService.planes.push(anexoPoliza);
                this.solicitudService.PlanesParametroCod[anexoPoliza.id+''] = anexoPoliza.descripcion;
            })
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    async setAnexoPolizaPlanes() {
        await this.anexoService.findPlanesAnexoPoliza(this.solicitudService.poliza.id).subscribe(async res => {
            let response = res as { status: string, message: string, data: Anexo_poliza[] };
            this.solicitudService.anexosPoliza = response.data;
            this.anexosPlanesPoliza = this.solicitudService.anexosPoliza.filter(params => params.id_tipo == 110);
            this.anexosPlanesPoliza.forEach((anexoPoliza: Anexo_poliza) => {
                this.solicitudService.Planes.push({label: anexoPoliza.descripcion, value: anexoPoliza.id});
                this.solicitudService.planes.push(anexoPoliza);
                this.solicitudService.PlanesParametroCod[anexoPoliza.id+''] = anexoPoliza.descripcion;
            })
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    async setAnexoPolizaWithAsegurado() {
        await this.anexoService.findAnexoPolizaWithAsegurado(this.solicitudService.poliza.id, this.solicitudService.asegurado.id).subscribe(async res => {
            let response = res as { status: string, message: string, data: Anexo_poliza[] };
            this.solicitudService.anexosPoliza = response.data;
            if (this.solicitudService.anexosPoliza.length) {
                this.anexosPlanesPoliza = this.solicitudService.anexosPoliza.filter(params => params.id_tipo == 110);
                this.anexosPlanesPoliza.forEach((anexoPoliza: Anexo_poliza) => {
                    this.solicitudService.Planes.push({label: anexoPoliza.descripcion, value: anexoPoliza.id});
                    this.solicitudService.planes.push(anexoPoliza);
                    this.solicitudService.PlanesParametroCod[anexoPoliza.id+''] = anexoPoliza.descripcion;
                })
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    async setAnexoPolizaWithAseguradoAnexo() {
        if (this.solicitudService.persona_banco_datos.plan != 0) {
            await this.anexoService.listaAtributosByIdPoliza(this.solicitudService.poliza.id, this.solicitudService.asegurado.id, this.solicitudService.persona_banco_datos.plan)
                .then(async res => {
                    let response = res as { status: string, message: string, data: Anexo_poliza[] };
                    this.solicitudService.anexosPoliza = response.data;
                    if (this.solicitudService.anexosPoliza.length) {
                        this.anexosPlanesPoliza = this.solicitudService.anexosPoliza.filter(params => params.id_tipo === 110);
                        this.anexosPlanesPoliza.forEach(async (anexoPoliza: Anexo_poliza) => {
                            await this.solicitudService.Planes.push({
                                label: anexoPoliza.descripcion,
                                value: anexoPoliza.id
                            });
                        })
                    }
                });
        }
    }

    async BuscarCliente(docId: string = null, docIdExt: number = null, search:boolean = true, updatePersona:boolean = true) {
        this.search = search;
        this.solicitudService.buscando = true;
        this.solicitudService.persona_banco = new persona_banco();
        this.solicitudService.persona_banco_datos = new persona_banco_datos();
        this.solicitudService.persona_banco_account = new persona_banco_account();
        this.solicitudService.persona_banco_solicitud = new persona_banco_solicitud();
        this.solicitudService.persona_banco_operacion = new persona_banco_operacion();
        this.solicitudService.persona_banco_pep = new persona_banco_pep();
        this.solicitudService.asegurado = new Asegurado();
        this.solicitudService.setUserLogin();
        this.solicitudService.editandoTitular = false;
        this.solicitudService.isLoadingAgain = true;
        if (docId && docIdExt) {
            this.solicitudService.extension = docIdExt+'';
            this.solicitudService.doc_id = docId;
        }
        if (this.solicitudService.doc_id && this.solicitudService.extension) {
            await this.getDatosFromCustomerService(this.solicitudService.doc_id, this.solicitudService.extension, async (isBankClient, conCreditoAsociado) => {
                let persona_doc_id = this.solicitudService.doc_id ? this.solicitudService.doc_id.trim() : this.solicitudService.doc_id;
                let persona_doc_id_ext = this.solicitudService.extension;
                if (this.solicitudService.persona_banco && isObject(this.solicitudService.persona_banco) && Object.keys(this.solicitudService.persona_banco).length) {
                    await this.personaService.findPersonasAseguradasConAtributosByDocIdYPoliza(persona_doc_id,this.solicitudService.objetoAseguradoDatosComplementarios.id,[10,12]).subscribe(async (res) => {
                        // await this.setAnexoPolizaPlanes();
                        let response = res as { status: string, message: string, data: Asegurado[] };
                        this.solicitudService.asegurados = response.data;
                        // if (this.solicitudService.asegurados.length) {
                            if (this.solicitudService.asegurados.length) {
                                this.aseguradoWithDiferentDocIdExt = this.solicitudService.asegurados.find(param => param.entidad.persona.persona_doc_id_ext+'' != persona_doc_id_ext+'');
                                if (this.aseguradoWithDiferentDocIdExt) {
                                    if (updatePersona) {
                                        // this.solicitudService.displayActualizacionPersona = true;
                                        await this.cambiarPersonaExtension(this.aseguradoWithDiferentDocIdExt.entidad.persona.id, this.aseguradoWithDiferentDocIdExt.id_instancia_poliza, this.solicitudService.doc_id, this.solicitudService.extension);
                                    } else {
                                        await this.afterSearch();
                                    }
                                    this.solicitudService.isLoadingAgain = false;
                                } else {
                                    await this.solicitudService.afterSearchWithLimits(this.search,this.userform, async () => {
                                        await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                                    }, async () => {
                                        this.abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext);
                                    }, async () => {
                                        this.reiniciarEcoAccidentes()
                                    }, async () => {
                                        this.solicitudService.isLoadingAgain = false;
                                        await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                                        this.stateButtonsFlow();
                                    });
                                }
                            } else {
                                // await this.abrirModalRegistroCliente();
                                // this.solicitudService.asegurado.instancia_poliza = new Instancia_poliza();
                                // this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
                                if (!isBankClient) {
                                    this.solicitudService.displayNuevoCliente = true;
                                } else {
                                    if (!conCreditoAsociado) {
                                        this.solicitudService.displayClienteSinCredito = true;
                                    } else {
                                        await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                                        this.abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext);
                                    }
                                }
                                this.solicitudService.isLoadingAgain = false;
                                this.stateButtonsFlow();
                            }
                        // }
                        // else {
                        //     await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                        //     if (!isBankClient) {
                        //         this.solicitudService.displayNuevoCliente = true;
                        //     } else {
                        //         if (!conCreditoAsociado) {
                        //             this.solicitudService.displayClienteSinCredito = true;
                        //         } else {
                        //             this.abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext);
                        //         }
                        //     }
                        //     this.solicitudService.isLoadingAgain = false;
                        //     this.stateButtonsFlow();
                        // }
                        await this.solicitudService.setAtributosToAsegurado(async () => {
                            this.stateButtonsFlow();
                        });
                    }, err => {
                        if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                            this.router.navigate(['login']);
                        } else {
                            console.log(err);
                        }
                    });
                } else {
                    this.solicitudService.persona_banco_account = new persona_banco_account();
                    this.solicitudService.persona_banco_datos = new persona_banco_datos();
                    this.solicitudService.persona_banco = new persona_banco();
                    this.solicitudService.displayNuevoCliente = true;
                    this.solicitudService.isLoadingAgain = false;
                    // await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                    this.solicitudService.displayClienteSinCredito = true;
                    this.stateButtonsFlow();
                }
            });
        } else {
            this.solicitudService.displayNuevoCliente = true;
            this.solicitudService.isLoadingAgain = false;
        }
        this.cols = [];
    }

    async newAsegurado(persona_doc_id, persona_doc_id_ext) {
        let oldAsegurado: Asegurado = new Asegurado();
        if (isObject(this.solicitudService.asegurado)) {
            oldAsegurado = this.solicitudService.asegurado;
            if (oldAsegurado.instancia_poliza.atributo_instancia_polizas && oldAsegurado.instancia_poliza.atributo_instancia_polizas.length) {
                oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter = oldAsegurado.instancia_poliza.atributo_instancia_polizas.filter(param => param.objeto_x_atributo.par_comportamiento_interfaz_id == this.solicitudService.parametroVisible.id);
            }
            if (oldAsegurado.instancia_poliza.atributo_instancia_polizas) {
                oldAsegurado.instancia_poliza.atributo_instancia_polizas.forEach(async (atributo_instancia_poliza: Atributo_instancia_poliza) => {
                    atributo_instancia_poliza.valor = '';
                });
            }
            if (oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter) {
                oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter.forEach(async (atributo_instancia_poliza: Atributo_instancia_poliza) => {
                    atributo_instancia_poliza.valor = '';
                });
            }
        }
        oldAsegurado.entidad.persona.persona_doc_id = persona_doc_id;
        oldAsegurado.entidad.persona.persona_doc_id_ext = persona_doc_id_ext;

        this.solicitudService.asegurado.instancia_poliza = new Instancia_poliza();
        this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
        await this.solicitudService.setDatesOfAsegurado();
        this.solicitudService.persona_banco.doc_id = this.solicitudService.doc_id ? this.solicitudService.doc_id.trim() : this.solicitudService.doc_id;
        this.solicitudService.persona_banco.extension = this.solicitudService.extension;
        this.solicitudService.asegurado = new Asegurado();
        this.BeneficiariosAux = [];
        //this.solicitudService.usuario_banco = this.solicitudService.usuarioLogin.usuario_banco;
        this.solicitudService.asegurado.instancia_poliza.id_anexo_poliza = this.solicitudService.poliza.anexo_poliza.id;
        this.solicitudService.asegurado.instancia_poliza.estado = this.solicitudService.estadoIniciado;
        this.solicitudService.asegurado.instancia_poliza.poliza = this.solicitudService.poliza;
        this.solicitudService.asegurado.instancia_poliza.id_estado = this.solicitudService.estadoIniciado.id;
        this.solicitudService.asegurado.instancia_poliza.id_poliza = this.solicitudService.poliza.id;
        this.solicitudService.instanciaDocumentoSolicitud = new Instancia_documento();
        this.solicitudService.instanciaDocumentoSolicitud.documento = this.solicitudService.documentoSolicitud;
        if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(param => param.id_documento == this.solicitudService.instanciaDocumentoSolicitud.id_documento)) {
            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoSolicitud);
        }
        this.solicitudService.asegurado.entidad = oldAsegurado.entidad;
        this.solicitudService.persona_banco_datos.ocupacion = this.solicitudService.persona_banco.caedec + ' - ' + this.solicitudService.persona_banco.desc_caedec;
        this.solicitudService.persona_banco_datos.modalidad_pago = this.solicitudService.Periodicidad[2].value+'';
        this.solicitudService.persona_banco_datos.plazo = 12;
        this.solicitudService.persona_banco_datos.ciudad_nacimiento = '';
        this.solicitudService.persona_banco_datos.lugar_nacimiento = '';
        this.solicitudService.persona_banco_datos.provincia = '';
        this.solicitudService.persona_banco_datos.desc_ocupacion = '';
        this.solicitudService.asegurado.entidad.persona.par_pais_nacimiento_id = "5";
        this.solicitudService.persona_banco_datos.tipo_doc = 'CI';

        this.solicitudService.persona_banco_solicitud.solicitud_sci_selected = null;
        this.solicitudService.persona_banco_solicitud.solicitud_forma_pago = null;
        this.solicitudService.persona_banco_solicitud.solicitud_prima_total = '';
        this.solicitudService.persona_banco_operacion.Operacion_Plazo = 0;
        this.solicitudService.persona_banco_operacion.Operacion_Tipo_Credito = '';

        this.solicitudService.persona_banco.debito_automatico = null;
        this.solicitudService.persona_banco.debito_automatico = this.solicitudService.Condiciones[1].value;
        this.solicitudService.persona_banco_datos.razon_social = this.solicitudService.persona_banco.paterno + ' ' + this.solicitudService.persona_banco.nombre;
        this.solicitudService.persona_banco_datos.nit_carnet = this.solicitudService.persona_banco.doc_id+'';
        this.solicitudService.atributoNroCuenta = new Atributo_instancia_poliza();
        this.solicitudService.atributoNroDireccion = new Atributo_instancia_poliza();
        this.solicitudService.atributoEmail = new Atributo_instancia_poliza();
        this.solicitudService.atributoUltimosCuatroDigitos = new Atributo_instancia_poliza();
        this.solicitudService.atributoRazonSocial = new Atributo_instancia_poliza();
        this.solicitudService.atributoOcupacion = new Atributo_instancia_poliza();
        this.solicitudService.atributoAmpConAmbMedGeneral = new Atributo_instancia_poliza();
        this.solicitudService.atributoAmpConAmbMedEspecializada = new Atributo_instancia_poliza();
        this.solicitudService.atributoAmpSumAmbMedicamentos = new Atributo_instancia_poliza();
        this.solicitudService.atributoAmpSumExaLaboratorio = new Atributo_instancia_poliza();
        this.solicitudService.atributoNitCarnet = new Atributo_instancia_poliza();
        this.solicitudService.atributoCtaFechaExpiracion = new Atributo_instancia_poliza();
        this.solicitudService.atributoPlan = new Atributo_instancia_poliza();
        this.solicitudService.atributoNroTransaccion = new Atributo_instancia_poliza();
        this.solicitudService.atributoTransaccionImporte = new Atributo_instancia_poliza();
        this.solicitudService.atributoTransaccionDetalle = new Atributo_instancia_poliza();
        this.solicitudService.atributoFechaTransaccion = new Atributo_instancia_poliza();
        this.solicitudService.atributoTransaccionMoneda = new Atributo_instancia_poliza();
        this.solicitudService.atributoPlazo = new Atributo_instancia_poliza();
        this.solicitudService.atributoAgencia = new Atributo_instancia_poliza();
        this.solicitudService.atributoPrima = new Atributo_instancia_poliza();
        this.solicitudService.atributoTelefonoCelular = new Atributo_instancia_poliza();
        this.solicitudService.atributoCiudadNacimiento = new Atributo_instancia_poliza();
        this.solicitudService.atributoSucursal = new Atributo_instancia_poliza();
        this.solicitudService.atributoUsuarioCargo = new Atributo_instancia_poliza();
        this.solicitudService.atributoZona = new Atributo_instancia_poliza();
        this.solicitudService.atributoDebitoAutomatico = new Atributo_instancia_poliza();
        this.solicitudService.atributoFormaPago = new Atributo_instancia_poliza();
        this.solicitudService.aseguradoBeneficiarios = [];
        this.solicitudService.atributoMoneda = new Atributo_instancia_poliza();
        this.solicitudService.atributoNroTarjeta = new Atributo_instancia_poliza();
        this.solicitudService.atributoSolicitudPrimaTotal = new Atributo_instancia_poliza();
        this.solicitudService.atributoSolicitudFrecuenciaPlazo = new Atributo_instancia_poliza();
        this.solicitudService.atributoSolicitudMoneda = new Atributo_instancia_poliza();
        this.solicitudService.atributoSolicitudSciEstado = new Atributo_instancia_poliza();
        this.solicitudService.atributoSolicitudSci = new Atributo_instancia_poliza();
        this.solicitudService.atributoTipoSeguro = new Atributo_instancia_poliza();
        this.solicitudService.atributoSolicitudPlazoCredito = new Atributo_instancia_poliza();
        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas = oldAsegurado.instancia_poliza.atributo_instancia_polizas;
        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter = oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter;
        await this.componentsBehaviorOnInit();
        await this.solicitudService.setAtributosToAsegurado(async () => {
            this.stateButtonsFlow();
            // await this.solicitudService.componentsBehavior();
            await this.solicitudService.setBeneficiarios();
            this.setPago(this.solicitudService.persona_banco.debito_automatico);
            //this.setMonto(this.solicitudService.persona_banco_datos.modalidad_pago);
            this.setNroCuotas(this.solicitudService.persona_banco_datos.nro_cuotas);
            if (this.transicionesComponent != undefined) {
                this.transicionesComponent.ngOnInit();
            }
        });
    }

    selectNroCuenta(event) {
        if (event.value) {
            let value = event.value;
            if ([10,12].includes(parseInt(this.solicitudService.poliza.id + ''))) {
                this.solicitudService.showNroCuenta = false;
                this.solicitudService.showMoneda = false;
            } else {
                if (this.solicitudService.persona_banco_accounts.length) {
                    this.solicitudService.showNroCuenta = true;
                    this.solicitudService.showMoneda = true;
                    this.solicitudService.persona_banco_account = this.solicitudService.persona_banco_accounts.find(params => params.nrocuenta === value);
                    if (this.solicitudService.persona_banco_account) {
                        this.userform.controls['par_moneda'].setValue(this.solicitudService.persona_banco_account.moneda);
                        this.userform.controls['par_numero_cuenta'].setValue(this.solicitudService.persona_banco_account.nrocuenta);
                    }
                }
                // this.solicitudService.setAtributosToAsegurado();
            }
        }
    }

    async afterSearch() {
        await this.solicitudService.afterSearchWithLimits(this.search,this.userform, async () => {
            await this.newAsegurado(this.solicitudService.doc_id, this.solicitudService.extension);
        }, async () => {
            this.abrirModalRegistroCliente(this.solicitudService.doc_id, this.solicitudService.extension);
        }, async () => {
            this.solicitudService.isLoadingAgain = false;
            await this.newAsegurado(this.solicitudService.doc_id, this.solicitudService.extension);
            this.stateButtonsFlow();
        });
    }

    async cambiarPersonaExtension(idPerson: number, idInstanciaPoliza: number, docId:string, ext:string) {
        this.solicitudService.isLoadingAgain = true;
        this.personaService.actualizarPersonaExt(idPerson, idInstanciaPoliza, docId, ext).subscribe((resp) => {
            let response = resp as {
                status: string;
                message: string;
                data: { persona:Persona, instancia_documentos:Instancia_documento[]};
            };
            if (isObject(response.data) && Object.keys(response.data).length) {
                this.solicitudService.isLoadingAgain = false;
                this.solicitudService.asegurado.entidad.persona = response.data.persona;
                this.solicitudService.displayActualizacionPersona = false;
                this.afterSearch();
            }
        });
    }

    async selectPagoEfectivo(event) {
        if (event.value) {
            let value = event.value;
            await this.getDatosFromAccountService(async () => {
                await this.setPago(value);
                //await this.setMonto(this.solicitudService.persona_banco_datos.modalidad_pago);
            });
            // this.solicitudService.setAtributosToAsegurado();
        }
    }

    async setPago(value) {
        if (this.userform) {
            if (this.solicitudService.pagoEfectivo.id == value) {
                this.solicitudService.showTipoCuenta = false;
                this.solicitudService.showNroCuenta = false;
                this.solicitudService.showMoneda = false;
                this.userform.controls['par_numero_cuenta'].clearValidators();
                this.userform.controls['par_moneda'].clearValidators();
                this.userform.controls['par_numero_cuenta'].setValue('');
                this.userform.controls['par_moneda'].setValue('');
            } else if ([10,12].includes(parseInt(this.solicitudService.poliza.id + ''))) {
                this.userform.controls['par_numero_cuenta'].clearValidators();
                this.userform.controls['par_moneda'].clearValidators();
                this.solicitudService.showMoneda = false;
                this.solicitudService.showNroCuenta = false;
                this.solicitudService.showTipoCuenta = false;
            } else {
                this.userform.controls['par_numero_cuenta'].setValidators(Validators.required);
                this.userform.controls['par_moneda'].setValidators(Validators.required);
                this.solicitudService.showMoneda = true;
                this.solicitudService.showNroCuenta = true;
                this.solicitudService.showTipoCuenta = false;
            }
        }
    }

    async setNroCuotas(event) {
        let value;
        if (event.value) {
            value = event.value;
        } else {
            value = event;
        }
        if (parseInt(value)) {
            if (this.solicitudService.persona_banco_datos.nro_cuotas == 1) {
                this.solicitudService.persona_banco_datos.monto = parseInt('176') / parseInt(value+'');
            } else {
                this.solicitudService.persona_banco_datos.monto = parseInt('192') / parseInt(value+'');
            }
        } else {
            this.solicitudService.persona_banco_datos.monto = 0;
        }
    }

    selectPlan(event) {
        if (event.value) {
            let value = event.value;
            if (this.solicitudService.Planes.length) {
                this.plan = this.solicitudService.planes.find(params => params.id === value);
                if (this.plan) {
                    this.solicitudService.persona_banco_datos.plan = this.plan.id;
                    this.userform.controls['par_prima'].setValue(this.plan.monto_prima);
                }
            }
        }
    }

    async abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext) {
        // await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
        this.stateButtonsFlow();
        this.solicitudService.displayModalFormTitular = true;
        this.solicitudService.displayBusquedaCI = false;
        this.collapsedFormTitular = false;
        this.solicitudService.setFromObjetoAtributoToAtributoInstanciaPoliza(() => {
            this.solicitudService.setFeatureValidacionAlInicioPersonaBanco();
        });
        await this.solicitudService.validarWarnsOrErrors(() => {
            if (this.solicitudService.userFormHasErrors && this.solicitudService.userFormHasWarnings) {
                this.solicitudService.msgText = 'La información proporcionada por el sistema del banco tiene las siguientes advertencias y observaciones que deben ser resueltas,';
            } else if (this.solicitudService.userFormHasWarnings) {
                this.solicitudService.msgText = 'La información proporcionada por el sistema del banco tiene las siguientes advertencias,';
            }
        });
        this.enableUserform();
    }

    iniciarSolicitud() {
        this.solicitudService.msgs_warn = [];
        this.solicitudService.msgs_error = [];

    }

    cancelarSolicitud() {
        this.iniciarSolicitud();
        this.solicitudService.displayModalSolicitudExistente = false;
        this.solicitudService.asegurado = new Asegurado();
        this.solicitudService.persona_banco = new persona_banco();
        this.solicitudService.persona_banco_datos = new persona_banco_datos();
        this.solicitudService.persona_banco_account = new persona_banco_account();
        this.solicitudService.persona_banco_accounts = [];
        this.solicitudService.persona_banco_pep = new persona_banco_pep();
    }

    crearEcoResguardo() {
        this.solicitudService.parametrosRuteo = this.sessionStorageService.getItemSync('parametros') as ParametroRuteo;
        this.solicitudService.id_poliza = 12;
        this.solicitudService.parametrosRuteo.id_vista = 76;
        this.solicitudService.parametrosRuteo.parametro_vista = this.solicitudService.id_poliza+'';
        this.solicitudService.parametrosRuteo.openForm = true;
        this.sessionStorageService.setItemSync('parametros', this.solicitudService.parametrosRuteo);
        let docId = this.solicitudService.doc_id ? this.solicitudService.doc_id.trim() : this.solicitudService.doc_id;
        let docIdExt = parseInt(this.solicitudService.extension+'');
        this.solicitudService.displayModalSolicitudExistente = false;
        this.solicitudService.isLoadingAgain = true;
        this.onInit(this.solicitudService.id_poliza,docId,docIdExt,false,false);
    }

    reiniciarEcoAccidentes() {
        this.solicitudService.parametrosRuteo = this.sessionStorageService.getItemSync('parametros') as ParametroRuteo;
        this.solicitudService.id_poliza = 10;
        this.solicitudService.parametrosRuteo.id = 67;
        this.solicitudService.parametrosRuteo.parametro_vista = this.solicitudService.id_poliza+'';
        this.solicitudService.parametrosRuteo.openForm = true;
        this.sessionStorageService.setItemSync('parametros', this.solicitudService.parametrosRuteo);
        let docId = this.solicitudService.doc_id ? this.solicitudService.doc_id.trim() : this.solicitudService.doc_id;
        let docIdExt = parseInt(this.solicitudService.extension+'');
        this.solicitudService.displayModalSolicitudExistente = false;
        this.onInit(this.solicitudService.id_poliza,docId,docIdExt,false,false);
    }

    abrirVentanaTitular() {
        this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_apellido_casada = "";
        this.stateButtonsFlow();
        this.solicitudService.displayModalFormTitular = true;
        this.displayFormTitular = true;
    }

    abrirVentanaRegistroNuevoTitular() {
        this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_apellido_casada = "";
        this.displayFormTitular = true;
        this.displayDatosTitular = true;
        this.solicitudService.isLoadingAgain = false;
        this.stateButtonsFlow();
        this.solicitudService.displayModalFormTitular = true;
    }

    onRowSelectDato_complementario(even: any) {

    }

    async onApellidoInput() {
        this.userform.controls['persona_primer_apellido'].clearAsyncValidators();
        this.userform.controls['persona_primer_apellido'].setErrors(null);
        this.userform.controls['persona_segundo_apellido'].clearAsyncValidators();
        this.userform.controls['persona_segundo_apellido'].setErrors(null);
    }

    async validarApellidos(callback: any = null) {
        if (this.solicitudService.persona_banco.paterno == '' && this.solicitudService.persona_banco.materno == '' && this.solicitudService.persona_banco.apcasada == '') {
            this.userform.controls['persona_primer_apellido'].setErrors([Validators.pattern("^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$")]);
            this.userform.controls['persona_segundo_apellido'].setErrors([Validators.pattern("^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$")]);
            this.solicitudService.atributoApellidoPaterno.requerido = true;
            this.solicitudService.isLoadingAgain = false;
            this.stopSavingByLastName = true;
        } else {
            //this.onApellidoInput();
            this.stopSavingByLastName = false;
        }
        if (typeof callback == 'function') {
            await callback();
        }
    }

    async EnviarDocumentosADestinatariosEcoProteccion() {
        await this.validarCorreos();
        if (!this.stopSending) {
            this.solicitudService.envioDestinatariosExitoso = true;
        }
    }

    async validarCorreos() {
        if (this.solicitudService.destinatariosCorreos != '') {
            let correos = this.solicitudService.destinatariosCorreos.split(',');
            let correosValidos = true;
            correos.forEach(correo => {
                if (!this.solicitudService.validarEmail(correo)) {
                    correosValidos = false;
                }
            });
            if (!correosValidos) {
                this.envioDocumentosForm.controls['destinatarios_correos'].setErrors([Validators.email]);
                this.solicitudService.atributoDestinatarios.requerido = true;
                this.solicitudService.atributoFechaNacimiento.tipo_error = 'Uno de los correos no es valido, por favor revisalos';
                this.solicitudService.isLoadingAgain = false;
                this.stopSending = true;
            } else {
                this.stopSending = false;
            }
        } else {
            this.envioDocumentosForm.controls['destinatarios_correos'].clearAsyncValidators();
            this.envioDocumentosForm.controls['destinatarios_correos'].setErrors(null);
            this.stopSending = false;
        }
    }

    async afterAlertEdad() {
        this.userform.controls['persona_fecha_nacimiento'].setErrors(null);
        this.stopSavingByEdad = false;
    }

    async validarFechaNacimiento(callback: any = null) {
        let fechaMin = this.solicitudService.getFechaMinimaSegunEdad();
        let fechaMax = this.solicitudService.getFechaMaximaSegunEdad();
        let edad;
        if (this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento) {
            edad = new Date().getTime() - this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
            if (fechaMin && fechaMax) {
                //let minTime = fechaMin.getTime();
                // 567648000000 = 18 años
                let edadMinimaMiliseconds = this.solicitudService.edadMinimaYears * 31539999999.9988899;
                let edadMaximaMiliseconds = this.solicitudService.edadMaximaYears * 31539999999.9988899;
                if (edad < edadMinimaMiliseconds) {
                    this.solicitudService.atributoFechaNacimiento.tipo_error = 'La persona no es mayor de 18 años';
                    this.solicitudService.isLoadingAgain = false;
                    this.userform.controls['persona_fecha_nacimiento'].setErrors([Validators.min(fechaMin.getTime())]);
                    this.stopSavingByEdad = true;
                    if (!this.validandoContinuando) {
                        this.displayEdadIncorrecta = true;
                    }
                    return '';

                    // 2049840000000 = 65
                } else if (edad > edadMaximaMiliseconds) {
                    this.solicitudService.atributoFechaNacimiento.tipo_error = 'La persona es mayor de 65 años';
                    this.solicitudService.isLoadingAgain = false;
                    this.userform.controls['persona_fecha_nacimiento'].setErrors([Validators.max(fechaMax.getTime())]);
                    this.stopSavingByEdad = true;
                    if (!this.validandoContinuando) {
                        this.displayEdadIncorrecta = true;
                    }
                    return '';
                } else {
                    this.userform.controls['persona_fecha_nacimiento'].clearAsyncValidators();
                    this.userform.controls['persona_fecha_nacimiento'].setErrors(null);
                    this.stopSavingByEdad = false;
                }
            }
        }
        if (typeof callback == 'function') {
            await callback()
        }
    }


    async validarFechaMinima() {
        let fechaMin = this.solicitudService.getFechaMinimaSegunEdad();
        let diff = new Date().getTime() - this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
        let yearsMiliseconds = 5.676e+11 // 18 años;
        if (diff < fechaMin.getTime()) {
            this.userform.controls['persona_fecha_nacimiento'].setErrors([Validators.min(fechaMin.getTime())]);
            this.solicitudService.atributoFechaNacimiento.tipo_error = 'La persona no es mayor de 18 años';
            this.solicitudService.atributoApellidoPaterno.requerido = true;
            this.solicitudService.isLoadingAgain = false;
            this.stopSaving = true;
        } else {
            this.userform.controls['persona_fecha_nacimiento'].clearAsyncValidators();
            this.userform.controls['persona_fecha_nacimiento'].setErrors(null);
            this.stopSaving = false;
        }
    }

    async validarFechaMaxima() {
        let fechaMax = this.solicitudService.getFechaMaximaSegunEdad();
        let diff = new Date().getTime() - this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
        let yearsMiliseconds = 2.05e+12 // 65 años;
        if (diff > fechaMax.getTime()) {
            this.userform.controls['persona_fecha_nacimiento'].setErrors([Validators.max(fechaMax.getTime())]);
            this.solicitudService.atributoFechaNacimiento.tipo_error = 'La persona es mayor de 65 años';
            this.solicitudService.atributoApellidoPaterno.requerido = true;
            this.solicitudService.isLoadingAgain = false;
            this.stopSaving = true;
        } else {
            this.userform.controls['persona_fecha_nacimiento'].clearAsyncValidators();
            this.userform.controls['persona_fecha_nacimiento'].setErrors(null);
            this.stopSaving = false;
        }
    }

    async guardarSolicitudValidando(callback:any = null) {
        await this.solicitudService.setAtributosToAsegurado(async () => {
            this.stateButtonsFlow();
            await this.validarApellidos(() => {
                this.validarFechaNacimiento(() => {
                    this.guardarSolicitudPersona(callback)
                })
            });
        });

    }

    async guardarSolicitudPersona(callback:any = null) {
        if (!this.stopSaving && !this.stopSavingByEdad && !this.stopSavingByLastName && !this.stopSavingByMontoCuota) {
            this.solicitudService.isLoadingAgain = true;
            this.stateButtonsFlow();
            if (this.solicitudService.editandoTitular) {
                this.solicitudService.editandoTitular = false;
                this.personaService.actualizarSolicitud(this.solicitudService.asegurado).subscribe(async res => {
                    let response = res as { status: string, message: string, data: Asegurado };
                    this.solicitudService.buscando = false;
                    this.solicitudService.asegurado = response.data;
                    // this.setFeaturesBeneficiarios(this.solicitudService.asegurado.beneficiarios);
                    this.solicitudService.persona_banco_account = new persona_banco_account();
                    this.solicitudService.persona_banco = new persona_banco();
                    this.solicitudService.persona_banco_datos = new persona_banco_datos();
                    await this.stateButtonsFlow();
                    await this.solicitudService.setDatesOfAsegurado();
                    await this.solicitudService.setAtributosToPersonaBanco(this.userform, () => {
                        this.solicitudService.persona_banco.extension = this.solicitudService.asegurado.entidad.persona.persona_doc_id_ext+'';
                    });
                    await this.solicitudService.setBeneficiarios();
                    this.componenteBeneficiario.ngOnInit();
                    this.solicitudService.isLoadingAgain = false;
                    if(typeof callback == 'function') {
                        await callback();
                    }
                }, err => {
                    if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        this.router.navigate(['login']);
                    } else {
                        console.log(err);
                    }
                })
            } else {
                this.personaService.crearNuevaSolicitud(this.solicitudService.asegurado).subscribe(async res => {
                    let response = res as { status: string, message: string, data: Asegurado };
                    this.solicitudService.buscando = false;
                    this.solicitudService.asegurado = response.data;
                    // this.setFeaturesBeneficiarios(this.solicitudService.asegurado.beneficiarios);
                    this.solicitudService.persona_banco_account = new persona_banco_account();
                    this.solicitudService.persona_banco = new persona_banco();
                    this.solicitudService.persona_banco_datos = new persona_banco_datos();
                    await this.stateButtonsFlow();
                    await this.solicitudService.setDatesOfAsegurado();
                    await this.solicitudService.setAtributosToPersonaBanco(this.userform, () => {
                        this.solicitudService.persona_banco.extension = this.solicitudService.asegurado.entidad.persona.persona_doc_id_ext+'';
                    });
                    await this.solicitudService.setBeneficiarios();
                    // await this.setSolicitudAsIniciado();
                    this.componenteBeneficiario.ngOnInit();
                    this.solicitudService.isLoadingAgain = false;
                    if(typeof callback == 'function') {
                        await callback();
                    }
                }, err => {
                    if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        this.router.navigate(['login']);
                    } else {
                        console.log(err);
                    }
                });
            }
            this.displayDatosTitular = true;
            this.collapsedDatosTitular = false;
            this.solicitudService.displayModalDatosTitular = true;
            this.solicitudService.displayModalFormTitular = false;
        }
    }

    async onAcceptValidationInit() {
        this.solicitudService.displayValidacionAlInicio = false;
        if (this.userform.controls['par_debito_automatico_id'].value == null || this.userform.controls['par_debito_automatico_id'].value == '') {
            this.solicitudService.showNroCuenta = false;
            this.solicitudService.showMoneda = false;
            this.solicitudService.showTipoCuenta = false;
        }
        if (this.solicitudService.persona_banco_accounts.length == 0) {
            //this.cancelarSolicitud();
            this.stateButtonsFlow();
            this.solicitudService.displayModalFormTitular = true;
            //this.solicitudService.displayBusquedaCI = true;
        }
    }

    async editarTitular() {
        if ((this.solicitudService.asegurado.instancia_poliza.id_estado != this.solicitudService.estadoIniciado.id ||
            this.solicitudService.hasRolCajero) &&
            this.solicitudService.asegurado.instancia_poliza.id_estado != null) {
            this.solicitudService.disableForm(this.userform);
            this.solicitudService.disableToSave = true;
            this.stateButtonsFlow();
        }
        await this.solicitudService.setAtributosToPersonaBanco(this.userform,() => {
            this.solicitudService.persona_banco.extension = this.solicitudService.asegurado.entidad.persona.persona_doc_id_ext+'';
        });
        this.stateButtonsFlow();
        this.solicitudService.setDatesOfAsegurado();
        this.solicitudService.editandoTitular = true;
        this.solicitudService.displayModalFormTitular = true;
        this.solicitudService.displayModalDatosTitular = false;
        this.collapsedFormTitular = false;
    }
}
