import { PlanPagoComponent } from './componentes/administracion/archivos/plan-pago/plan-pago.component';
import { ControlCobroComponent } from './componentes/administracion/archivos/control-cobro/control-cobro.component';
import { PendientesComponent } from './componentes/administracion/archivos/pendientes/pendientes.component';
import { CargadoComponent } from './componentes/administracion/archivos/cargado/cargado.component';
import { GeneracionComponent } from './componentes/administracion/archivos/generacion/generacion.component';
import { ReportesComponent } from './componentes/genericos/reportes/reportes.component';
import { AnexoPruebaComponent } from './componentes/genericos/anexo-prueba/anexo-prueba.component';
import { AltaEcoMedicComponent } from './componentes/eco-medic/alta-eco-medic/alta-eco-medic.component';
import { AdministracionRolesPerfilesComponent } from './componentes/administracion/administracion-roles-perfiles/administracion-roles-perfiles.component';
import { AltaEcoAguinaldoComponent } from './componentes/eco-aguinaldo/alta-eco-aguinaldo/alta-eco-aguinaldo.component';
import { ParametrosComponent } from './componentes/administracion/parametros/parametros.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AppMainComponent } from './app.main.component';
import { Dashboard2Component } from './componentes/dashboard2/dashboard2.component';
import { AppLoginComponent } from './pages/app.login.component';
import {GestionEcoAguinaldoComponent} from "./componentes/eco-aguinaldo/gestion-eco-aguinaldo/gestion-eco-aguinaldo.component";
import {AltaEcoTarjetaComponent} from "./componentes/eco-tarjeta/alta-eco-tarjeta/alta-eco-tarjeta.component";
import {GestionEcoTarjetaComponent} from "./componentes/eco-tarjeta/gestion-eco-tarjeta/gestion-eco-tarjeta.component";
import { GestionEcoMedicComponent } from './componentes/eco-medic/gestion-eco-medic/gestion-eco-medic.component';
import {AltaEcoVidaComponent} from "./componentes/eco-vida/alta-eco-vida/alta-eco-vida.component";
import {AltaEcoProteccionComponent} from "./componentes/eco-proteccion/alta-eco-proteccion/alta-eco-proteccion.component";
import {GestionEcoVidaComponent} from "./componentes/eco-vida/gestion-eco-vida/gestion-eco-vida.component";
import {AltaEcoMedicPlusComponent} from "./componentes/eco-medic-plus/alta-eco-medic-plus/alta-eco-medic-plus.component";
import {UsuariosListaComponent} from "./componentes/usuarios-lista/usuarios-lista.component";
import {AltaEcoProteccionNuevoComponent} from "./componentes/eco-proteccion-nuevo/alta-eco-proteccion-nuevo/alta-eco-proteccion-nuevo.component";
import {AltaEcoRiesgoComponent} from "./componentes/eco-riesgo/alta-eco-riesgo/alta-eco-riesgo.component";
import {AuthGuardService} from "../../../../src/core/servicios/auth-guard.service";
import {AnulacionComponent} from "../../../../src/core/componentes/anulacion/anulacion.component";
import {AltaEcoEstudioComponent} from "./componentes/eco-estudio/alta-eco-estudio/alta-eco-estudio.component";


export const routes: Routes = [
    { path: '', component: AppLoginComponent},
    { path: 'AppMain', component: AppMainComponent, canActivate : [AuthGuardService],
        children: [
            { path: '', component: Dashboard2Component },
            { path: 'usuario', component: UsuariosListaComponent},
            { path: 'parametros', component:  ParametrosComponent},
            { path: 'AltaEcoAguinaldo', component:  AltaEcoAguinaldoComponent},
            { path: 'GestionEcoAguinaldo', component:  GestionEcoAguinaldoComponent},
            { path: 'GestionEcoVida', component:  GestionEcoVidaComponent},
            { path: 'AnularEcoAguinaldo', component:  AnulacionComponent},
            { path: 'AltaEcoPasanacu', component:  AltaEcoAguinaldoComponent},
            { path: 'AltaEcoVida', component:  AltaEcoVidaComponent},
            { path: 'AltaEcoProteccion', component:  AltaEcoProteccionComponent},
            { path: 'GestionEcoPasanacu', component:  GestionEcoAguinaldoComponent},
            { path: 'AnularEcoPasanacu', component:  AnulacionComponent},
            { path: 'AnularEcoVida', component:  AnulacionComponent},
            { path: 'AnularEcoProteccion', component:  AnulacionComponent},
            { path: 'AltaTarjetaDebito', component:  AltaEcoTarjetaComponent},
            { path: 'AnularTarjetaDebito', component:  AnulacionComponent},
            { path: 'AnularTarjetaCredito', component:  AnulacionComponent},
            { path: 'AltaTarjetaCredito', component:  AltaEcoTarjetaComponent},
            { path: 'GestionSolicitudesTarjetas', component:  GestionEcoTarjetaComponent},
            { path: 'GestionSolicitudesTarjetasDebito', component:  GestionEcoTarjetaComponent},
            { path: 'EmitirTarjetaCredito', component:  GestionEcoTarjetaComponent},
            { path: 'EmitirTarjetaDebito', component:  GestionEcoTarjetaComponent},
            { path: 'AdminRolesPerfiles', component:  AdministracionRolesPerfilesComponent},
            { path: 'AltaEcoMedic', component:  AltaEcoMedicComponent},
            { path: 'AltaEcoMedicPlus', component:  AltaEcoMedicPlusComponent},
            { path: 'AltaEcoEstudio', component:  AltaEcoEstudioComponent},
            { path: 'AnularEcoMedic', component:  AnulacionComponent},
            { path: 'AnularEcoMedicPlus', component:  AnulacionComponent},
            { path: 'AnularEcoEstudio', component:  AnulacionComponent},
            { path: 'GestionSolicitudesEcoMedic', component:  GestionEcoMedicComponent},
            { path: 'GestionSolicitudesEcoEstudio', component:  GestionEcoMedicComponent},
            { path: 'GestionSolicitudesEcoVida', component:  GestionEcoVidaComponent},
            { path: 'GestionSolicitudesEcoProteccion', component:  GestionEcoVidaComponent},
            { path: 'AnexoPrueba', component:  AnexoPruebaComponent},
            { path: 'Reportes', component:  ReportesComponent},
            { path: 'Generacion', component:  GeneracionComponent},
            { path: 'Carga', component:  CargadoComponent},
            { path: 'Pendientes', component:  PendientesComponent},
            { path: 'ControlCobro', component:  ControlCobroComponent},
            { path: 'PlanPago', component:  PlanPagoComponent},
            { path: 'AltaEcoProteccionNew', component:  AltaEcoProteccionNuevoComponent},
            { path: 'GestionSolicitudesEcoProteccionNew', component:  GestionEcoVidaComponent},
            { path: 'AltaEcoRiesgo', component:  AltaEcoRiesgoComponent},
            { path: 'GestionSolicitudesEcoRiesgo', component:  GestionEcoAguinaldoComponent},
            { path: 'AnularEcoAguinaldo', component:  AnulacionComponent},
            { path: 'AnularEcoRiesgo', component:  AnulacionComponent},
            { path: 'GestionTarjetaCredito', component:  GestionEcoTarjetaComponent},
            { path: 'AnularTarjetaCredito', component:  AnulacionComponent},
            { path: 'AltaTarjetaCredito', component:  AltaEcoTarjetaComponent},

            // {
            //     path : 'pnetseg',
            //     loadChildren : () => import('./pnetseg/pnetseg.module').then(m=>m.PnetsegModule)
            // },
            {
                path : 'pnetseg',
                loadChildren : () => import('../../../../src/app/app.module').then(m=>m.AppModule)
            },
            {
                path : 'crediticios',
                loadChildren : () => import('../../../crediticios/src/app/app.module').then(m=>m.AppModule)
            },
        ]
    },


    /*{path: 'error', component: AppErrorComponent},
    {path: 'accessdenied', component: AppAccessdeniedComponent},
    {path: '404', component: AppNotfoundComponent},*/
    {path: 'login', component: AppLoginComponent}
    /*{ path: 'cambiapwd', component: CambiapwdComponent },
    {path: '**', redirectTo: '/404'}*/,


];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'});
