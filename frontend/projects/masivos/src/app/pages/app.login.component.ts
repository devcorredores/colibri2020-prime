import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms'
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as app from '../../../../../src/environments/environment';
import { MessageService } from "primeng/api";
import {LoginService} from "../../../../../src/core/servicios/login.service";
import {CorreoService} from "../../../../../src/core/servicios/correo.service";
import {SessionStorageService} from "../../../../../src/core/servicios/sessionStorage.service";
import {UsuariosService} from "../../../../../src/core/servicios/usuarios.service";
import {Usuario} from "../../../../../src/core/modelos/usuario";

@Component({
  selector: 'app-login',
  templateUrl: './app.login.component.html',
  providers: [MessageService]
})
export class AppLoginComponent {

  formulario: boolean = true;
  userformPwd: FormGroup;
  displayPwd: boolean = false;
  formLogin: FormGroup;
  userinfo: Usuario;

  constructor(private router: Router,
    public loginService: LoginService,
    private messageService: MessageService,
    private correoService: CorreoService,
    private sessionStorageService: SessionStorageService,
    private usuarioService: UsuariosService,
    private route: ActivatedRoute,
    private fb: FormBuilder ) {
    this.formLogin = this.fb.group({
      'username': new FormControl("", Validators.compose([Validators.required])),
      'password': new FormControl("", Validators.compose([Validators.required])),
    });
  }

  ngOnInit() {
    this.loginService.usuario = new Usuario();
    this.userinfo = this.sessionStorageService.getItemSync('userInfo') as Usuario;
    if (this.userinfo) {
      setTimeout(() => {
        this.messageService.clear();
        this.messageService.add({ key: 'session', sticky: true, severity: 'warn', summary: 'Verificamos que tiene el sistema Colibrí abierto en otra pestaña del navegador. Si desea iniciar sesión en esta pestaña, deberá cerrar las anteriores.\n' +
              'Cerrar sesiones anteriores?', detail: '' });},1000);
      }
    this.userformPwd = this.fb.group({
      'email': new FormControl('', Validators.compose([Validators.required, Validators.email]))
    });
    let close = this.cerrarSesion;
    let sessionStorageService = this.sessionStorageService;
    window.addEventListener('beforeunload', (event) => {
        close();
    });
  }

  async CerrarSesionIniciando() {
    localStorage.removeItem('userInfo');
    await this.loginService.cerrarSesion();
    this.loginService.usuario = null;
    await this.Session()
  }

  async cerrarSesion() {
    localStorage.removeItem('userInfo');
    await this.loginService.cerrarSesion();
    this.router.navigate(['login']);
  }

  async Session() {
    //await this.cerrarSesion();
    this.userinfo = this.sessionStorageService.getItemSync('userInfo') as Usuario;
    if (this.userinfo) {
      this.messageService.clear();
      this.messageService.add({ key: 'session', sticky: true, severity: 'warn', summary: 'Verificamos que tiene el sistema Colibrí abierto en otra pestaña del navegador. Si desea iniciar sesión en esta pestaña, deberá cerrar las anteriores.\n' +
            'Cerrar sesiones anteriores?', detail: '' });
      //this.router.navigate(['/AppMain']);
    } else {
        this.loginService.login(this.loginService.usuario.usuario_login, this.loginService.usuario.usuario_password).subscribe(async res => {
          this.userinfo = res['user'] as Usuario;
          if ([357,11].includes(parseInt(this.userinfo.autenticacion_usuario.par_autenticacion_id+''))) {
            if (this.userinfo.usuario_banco && this.userinfo.usuario_banco.us_sucursal && this.userinfo.usuario_banco.us_oficina) {
              this.userinfo = res['user'] as Usuario;
              this.formulario = false;
              this.loginService.setUserInfo(this.userinfo);
              this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Order submitted' });
              //await this.delay(2000);
              this.router.navigate(['/AppMain']);
            } else {
              this.messageService.add({ key: 'usuarioBanco', sticky: true, severity: 'warn', summary: `No se pudo identificar la sucursal o agencia de su usuario: ${this.userinfo.usuario_login}, por favor conactese con soporte del banco.`, detail: '' });
            }
          } else {
            this.userinfo = res['user'] as Usuario;
            this.formulario = false;
            this.loginService.setUserInfo(this.userinfo);
            this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Order submitted' });
            //await this.delay(2000);
            this.router.navigate(['/AppMain']);
          }
          }, err => {
            if (err.error.message) {
              this.messageService.clear();
              this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: err.error.message.message, detail: '' });
            } else {
              this.messageService.clear();
              this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: err.message, detail: '' });
            }
        });
    }
  }

  showMessage() {
    this.messageService.add({severity:'info', summary: 'Record is added successully', detail:'record added'});
  }

  onReject(key) {
    this.messageService.clear(key);
    this.loginService.usuario=new Usuario();
    //this.router.navigate(['/login']);
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  solicitaResetPassword() {
    this.displayPwd = true;
  }

  solCambiarPassword(emailUsuario: string) {
    let response: { status: string, message: string, data: Usuario };
    this.usuarioService.validaEmail(emailUsuario).subscribe(async res => {
      response = res as any;
      if (response.message === 'Email registrado') {
        response = res as { status: string, message: string, data: any };
        if (response.data.par_local_id) {
          if (response.data.par_local_id == 25) {
            const mailOptions = {
              "para": emailUsuario,
              "asunto": "Solicitud cambio de Contraseña Colibri-2020",
              "mensaje": `Señores <br>Corrredores de Seguro Ecofuturo: <br><br> Mediante la presente solicito se pueda realizar el cambio de contraseña para el usuario del sistema Colibri 2020 <b> ${response.data.usuario_login} </b> asociado al email ${emailUsuario}.`
            }
            this.correoService.enviarEmail(mailOptions).subscribe(resp => {
              this.messageService.clear();
              this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Contraseña', detail: `Su solicitud fue enviada a Kerkus Corredores de Seguros, con el Asunto Solicitud cambio de Contraseña para el usuario asociado a ${emailUsuario}` });
            });
          } else if (response.data.par_local_id == 26) {
            const mailOptions = {
              "para": emailUsuario,
              "asunto": "Cambio contraseña via enlace Colibri-2020",
              "mensaje": `Señor <br> ${response.data.usuario_login} <br> Usuario Colibri2020 : <br><br>Para proceder a cambiar su contraseña por favor haga click en el siguiente <a href='${app.environment.URL}/#/cambiapwd?username=${response.data.usuario_login}'> enlace </a>. <br><br>Corrredores de Seguro Ecofuturo`
              // "mensaje": `Señor <br> ${response.data.usuario_login} <br> Usuario Colibri2020 : <br><br>Para proceder a cambiar su contraseña por favor haga click en el siguiente <a href='http://localhost:4200/#/cambiapwd?username=${response.data.usuario_login}'> enlace </a>. <br><br>Corrredores de Seguro Ecofuturo`
              // "mensaje": `Señor <br> Usuario Colibri2020: <br><br>Para cambiar su contraseña por favor haga click en el siguiente <a href='http://localhost:4200/#/AppMain/usuario;ComponentesInvisibles=%5B%5D;ruta=%2FAppMain%2Fusuario'> enlace </a>. <br><br>Corrredores de Seguro Ecofuturo`
            }
            this.correoService.enviarEmail(mailOptions).subscribe(resp => {
              response = resp as any;
              this.messageService.clear();
              this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Contraseña', detail: `Su solicitud fue enviada a Kerkus Corredores de Seguros, con el Asunto Solicitud cambio de Contraseña para el usuario asociado a ${emailUsuario}` });
            });
          } else {
            this.messageService.clear();
            this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Contraseña', detail: `El usuario del correo ${emailUsuario} debe resetar su contraseña a traves de su plataforma personal` });
          }
        } else {
          this.messageService.clear();
          this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Contraseña', detail: `El usuario del correo ${emailUsuario} debe resetar su contraseña a traves de su plataforma personal` });
        }
        this.displayPwd = false;
      } else if (response.message === 'Email no registrado') {
        this.messageService.clear();
        this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Contraseña', detail: `Su solicitud fue enviada a Kerkus Corredores de seguros, con el Asunto Solicitud cambio de Contraseña para el usuario asociado a ${emailUsuario}` });
        // this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Validacion email', detail: `Email ${emailUsuario} no registrado ` });
      }
    },
      err => console.error("ERROR llamando servicio:", err));
  }

  // solCambiarPasswordLink(emailUsuario: string) {
  //   var response: { status: string, message: string, data: {} };
  //   const mailOptions = {
  //     "para": emailUsuario,
  //     "asunto": "Cambio contraseña via enlace Colibri-2020",
  //     "mensaje": `Señor <br> Usuario Colibri2020: <br><br>Para cambiar su contraseña por favor haga click en el siguiente <a href='http://localhost:4200/#/cambiapwd?username=aaaa'> enlace </a>. <br><br>Corrredores de Seguro Ecofuturo`
  //     // "mensaje": `Señor <br> Usuario Colibri2020: <br><br>Para cambiar su contraseña por favor haga click en el siguiente <a href='http://localhost:4200/#/AppMain/usuario;ComponentesInvisibles=%5B%5D;ruta=%2FAppMain%2Fusuario'> enlace </a>. <br><br>Corrredores de Seguro Ecofuturo`
  //   }
  //   this.correoService.enviarEmail(mailOptions).subscribe(resp => {
  //     response = resp as any;
  //     this.messageService.clear();
  //     this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Contraseña', detail: `Su solicitud fue enviada a kerkus Corredores de Seguros, con el Asunto Solicitud cambio de Contraseña para el usuario asociado a ${emailUsuario}` });
  //   });
  //   this.displayPwd = false;
  // }
}
