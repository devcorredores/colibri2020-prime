import { PendientesComponent } from './componentes/administracion/archivos/pendientes/pendientes.component';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {AppRoutes} from './app.routes';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CalendarModule } from 'primeng/calendar';
import { ChartModule } from 'primeng/chart';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DataViewModule } from 'primeng/dataview';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { MenuModule } from 'primeng/menu';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PanelModule } from 'primeng/panel';
import { PickListModule } from 'primeng/picklist';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { TooltipModule } from 'primeng/tooltip';
import {MessageService} from "primeng/api";
import { InputTextModule } from 'primeng/inputtext';
import { AppComponent } from './app.component';
import { AppMainComponent } from './app.main.component';
import { AppConfigComponent } from './app.config.component';
import { AppNotfoundComponent } from './pages/app.notfound.component';
import { AppErrorComponent } from './pages/app.error.component';
import { AppAccessdeniedComponent } from './pages/app.accessdenied.component';
import { AppLoginComponent } from './pages/app.login.component';
import {AppMenuComponent, AppSubMenuComponent} from './app.menu.component';
import {AppBreadcrumbComponent} from './app.breadcrumb.component';
import {AppTopBarComponent} from './app.topbar.component';
import {AppFooterComponent} from './app.footer.component';
import {DashboardComponent} from './demo/view/dashboard.component';
import {CarService} from './demo/service/carservice';
import {CountryService} from './demo/service/countryservice';
import {EventService} from './demo/service/eventservice';
import {NodeService} from './demo/service/nodeservice';
import { LoginComponent } from './componentes/login/login.component';
import { Dashboard2Component } from './componentes/dashboard2/dashboard2.component';
import { TablaComponent } from './componentes/tabla/tabla.component';
import { UsuariosListaComponent } from './componentes/usuarios-lista/usuarios-lista.component';
import { ParametrosComponent } from './componentes/administracion/parametros/parametros.component';
import { CambiapwdComponent } from './componentes/cambiapwd/cambiapwd.component';
import { AltaEcoAguinaldoComponent } from './componentes/eco-aguinaldo/alta-eco-aguinaldo/alta-eco-aguinaldo.component';
import { GestionEcoAguinaldoComponent } from './componentes/eco-aguinaldo/gestion-eco-aguinaldo/gestion-eco-aguinaldo.component';
import {LoaderInterceptor} from "./interceptors/loader.interceptor";
import { AltaEcoTarjetaComponent } from './componentes/eco-tarjeta/alta-eco-tarjeta/alta-eco-tarjeta.component';
import { GestionEcoTarjetaComponent } from './componentes/eco-tarjeta/gestion-eco-tarjeta/gestion-eco-tarjeta.component';
import { AltaEcoMedicComponent } from './componentes/eco-medic/alta-eco-medic/alta-eco-medic.component';
import { GestionEcoMedicComponent } from './componentes/eco-medic/gestion-eco-medic/gestion-eco-medic.component';
import { AnexoComponent } from './componentes/genericos/anexo/anexo.component';
import { AnexoPruebaComponent } from './componentes/genericos/anexo-prueba/anexo-prueba.component';
import { ReportesComponent } from './componentes/genericos/reportes/reportes.component';
import { AltaEcoVidaComponent } from './componentes/eco-vida/alta-eco-vida/alta-eco-vida.component';
import { GestionEcoVidaComponent } from './componentes/eco-vida/gestion-eco-vida/gestion-eco-vida.component';
import { AltaEcoProteccionComponent } from './componentes/eco-proteccion/alta-eco-proteccion/alta-eco-proteccion.component';
import { AltaEcoMedicPlusComponent } from './componentes/eco-medic-plus/alta-eco-medic-plus/alta-eco-medic-plus.component';
import { GeneracionComponent } from './componentes/administracion/archivos/generacion/generacion.component';
import { CargadoComponent } from './componentes/administracion/archivos/cargado/cargado.component';
import { ControlCobroComponent } from './componentes/administracion/archivos/control-cobro/control-cobro.component';
import { PlanPagoComponent } from './componentes/administracion/archivos/plan-pago/plan-pago.component';
import { AltaEcoProteccionNuevoComponent } from './componentes/eco-proteccion-nuevo/alta-eco-proteccion-nuevo/alta-eco-proteccion-nuevo.component';
import { AltaEcoRiesgoComponent } from './componentes/eco-riesgo/alta-eco-riesgo/alta-eco-riesgo.component';
import {AdministracionRolesPerfilesComponent} from "./componentes/administracion/administracion-roles-perfiles/administracion-roles-perfiles.component";
import {ToastrModule} from "ngx-toastr";
import {RouterModule} from "@angular/router";
import {BreadcrumbService} from "../../../../src/core/servicios/breadcrumb.service";
import {AdministracionDePermisosService} from "../../../../src/core/servicios/administracion-de-permisos.service";
import {LoaderService} from "../../../../src/core/servicios/loader.service";
import {AnulacionModule} from "../../../../src/core/componentes/anulacion/anulacion.module";
import {BeneficiarioModule} from "../../../../src/core/componentes/beneficiario/beneficiario.module";
import {TransicionesModule} from "../../../../src/core/componentes/transiciones/transiciones.module";
import {ActualizarSolicitudModule} from "../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.module";
import {AltaEcoEstudioComponent} from "./componentes/eco-estudio/alta-eco-estudio/alta-eco-estudio.component";
import {MultiSelectModule} from "primeng";

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        RouterModule,
        AppRoutes,
        HttpClientModule,
         BrowserAnimationsModule,
         AutoCompleteModule,
         CalendarModule,
         ChartModule,
         CheckboxModule,
        MultiSelectModule,
        ConfirmDialogModule,
         DataViewModule,
         DialogModule,
         DropdownModule,
         FieldsetModule,
         FullCalendarModule,
         InputTextModule,
         MenuModule,
         MessageModule,
         MessagesModule,
        OverlayPanelModule,
         PanelModule,
         PickListModule,
         ScrollPanelModule,
         TableModule,
         TabViewModule,
         ToastModule,
         TooltipModule,
        FormsModule,
        ToastrModule.forRoot(),
        FieldsetModule,
        AnulacionModule,
        BeneficiarioModule,
        TransicionesModule,
        ActualizarSolicitudModule
    ],
    declarations: [
        AppComponent,
        AppMainComponent,
        AppMenuComponent,
        AppConfigComponent,
        AppSubMenuComponent,
        AppBreadcrumbComponent,
        AppTopBarComponent,
        AppFooterComponent,
        DashboardComponent,
        AppNotfoundComponent,
        AppErrorComponent,
        AppAccessdeniedComponent,
        LoginComponent,
        TablaComponent,
        Dashboard2Component,
        AppLoginComponent,
        UsuariosListaComponent,
        ParametrosComponent,
        CambiapwdComponent,
        AltaEcoAguinaldoComponent,
        GestionEcoAguinaldoComponent,
        AltaEcoTarjetaComponent,
        GestionEcoTarjetaComponent,
        AdministracionRolesPerfilesComponent,
        AltaEcoMedicComponent,
        GestionEcoMedicComponent,
        AnexoComponent,
        AnexoPruebaComponent,
        ReportesComponent,
        AltaEcoVidaComponent,
        GestionEcoVidaComponent,
        AltaEcoProteccionComponent,
        AltaEcoMedicPlusComponent,
        GeneracionComponent,
        CargadoComponent,
        PendientesComponent,
        ControlCobroComponent,
        PlanPagoComponent,
        AltaEcoProteccionNuevoComponent,
        AltaEcoRiesgoComponent,
        AltaEcoEstudioComponent
    ],
    providers: [
        LoaderService,MessageService,ToastrModule,
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
        CarService, CountryService, EventService, NodeService, BreadcrumbService, AdministracionDePermisosService
    ],
    bootstrap: [AppComponent],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }