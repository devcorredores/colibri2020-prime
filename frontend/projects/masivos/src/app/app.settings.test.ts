export const settings = {
    production: false,
    version: "1.0.0",
    URL: 'https://corredoresecofuturo.com.bo:4000',
    URL_API_MASIVOS: "https://corredoresecofuturo.com.bo:4000/api-corredores-ecofuturo",
    URL_PUBLIC_PDF_MASIVOS: "https://corredoresecofuturo.com.bo:4000/pdf",
    URL_PUBLIC_VIEWER_MASIVOS: "https://corredoresecofuturo.com.bo:4000/viewer"
};
