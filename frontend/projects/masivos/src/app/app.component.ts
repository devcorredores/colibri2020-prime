import {AfterViewChecked, Component} from '@angular/core';
import * as $ from 'jquery';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})

export class AppComponent implements AfterViewChecked{

    bloqueo_navegador=true;
    constructor(){
        let a=this.getBrowserInfo();
        console.log("Entrando a las validaciones");
        if(a){
            let A=a.substring(0,2);
                if(A==='IE'){
                    this.bloqueo_navegador=false;
                }
        }
        if(!this.bloqueo_navegador){
            alert("utilice otro navegador que no sea Internet Explorer");
        }
    }

    ngAfterViewChecked() {
        $('form input').each((index,input)=>{if($(input).attr('readonly')=='readonly') {$($('form input')[index]).addClass('input-disabled')} if($(input).prop('disabled')){$($('form input')[index]).removeClass('input-disabled')}});
        $('p-dropdown').each((index,dropdownt)=>{$(dropdownt).removeClass('ng-untouched');$(dropdownt).addClass('ng-touched ui-inputwrapper-focus');});
        //$('.ui-widget-overlay').each((index,overlay) => {index == 0 ? $(overlay).addClass('display-block') : $(overlay).addClass('display-none')});
    }

    getBrowserInfo() {
        var ua= navigator.userAgent, tem, 
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if(M[1]=== 'Chrome'){
            tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
            if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    };
}
