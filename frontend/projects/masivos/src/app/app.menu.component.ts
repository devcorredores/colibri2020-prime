import { BreadcrumbService } from '../../../../src/core/servicios/breadcrumb.service';
import { MenuService } from '../../../../src/core/servicios/menu.service';
import { User } from '../../../../src/core/modelos/user';
import { AdministracionDePermisosService } from '../../../../src/core/servicios/administracion-de-permisos.service';
import {Component, Input, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {trigger, state, style, transition, animate} from '@angular/animations';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {AppMainComponent} from './app.main.component';
import {SolicitudService} from "../../../../src/core/servicios/solicitud.service";
import {SessionStorageService} from "../../../../src/core/servicios/sessionStorage.service";
import {MenuItem, ScrollPanel} from "primeng";
import {Usuario} from "../../../../src/core/modelos/usuario";

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})

export class AppMenuComponent implements OnInit, AfterViewInit {

    @Input() reset: boolean;

    model: any[];
    model2: any[];

    @ViewChild('scrollPanel', { static: true }) layoutMenuScrollerViewChild: ScrollPanel;

    constructor(public app: AppMainComponent,
                private administracionDePermisosService:AdministracionDePermisosService,
                private router : Router,
                private menuService:MenuService,
                private sessionStorageService:SessionStorageService,
                private breadcrumbService : BreadcrumbService,
                public solicitudService:SolicitudService

    ) {}

    ngAfterViewInit() {
      setTimeout(() => {this.layoutMenuScrollerViewChild.moveBar(); }, 100);
    }

    ngOnInit() {
        let user = this.sessionStorageService.getItemSync('userInfo') as Usuario;
        this.administracionDePermisosService.ListarMenu(user.id).subscribe(res => {
            let response = res as { status: string, message: string, data: any };
            this.model2=response.data;
            this.cargarFuncionalidades();     
            this.model=this.model2;
            this.menuService.guardarMenu(this.model);
        }, err => {
            // if(err.error.message){
            //   this.messageService.clear();
            //   this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:err.error.message.message, detail:''});
            // }else{
            //   this.messageService.clear();
            //   this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:err.message, detail:''});
            // }
        });
        // this.model = [
        //     {label: 'Dashboard', icon: 'dashboard', routerLink: ['/']},
        //     {
        //         label: 'Components', icon: 'list', badge: '2', badgeStyleClass: 'orange-badge',
        //         items: [
        //             {label: 'Sample Page', icon: 'desktop_mac', routerLink: ['/sample']},
        //             {label: 'Forms', icon: 'input', routerLink: ['/forms']},
        //             {label: 'Data', icon: 'grid_on', routerLink: ['/data']},
        //             {label: 'Panels', icon: 'content_paste', routerLink: ['/panels']},
        //             {label: 'Overlays', icon: 'content_copy', routerLink: ['/overlays']},
        //             {label: 'Menus', icon: 'menu', routerLink: ['/menus']},
        //             {label: 'Messages', icon: 'message', routerLink: ['/messages']},
        //             {label: 'Charts', icon: 'insert_chart', routerLink: ['/charts']},
        //             {label: 'File', icon: 'attach_file', routerLink: ['/file']},
        //             {label: 'Misc', icon: 'toys', routerLink: ['/misc']}
        //         ]
        //     },
        //     {
        //         label: 'Template Pages', icon: 'get_app',
        //         items: [
        //             {label: 'Empty Page', icon: 'hourglass_empty', routerLink: ['/empty']},
        //             {label: 'Landing Page', icon: 'flight_land', url: 'assets/pages/landing.html', target: '_blank'},
        //             {label: 'Login Page', icon: 'verified_user', routerLink: ['/login'], target: '_blank'},
        //             {label: 'Error Page', icon: 'error', routerLink: ['/error'], target: '_blank'},
        //             {label: '404 Page', icon: 'error_outline', routerLink: ['/404'], target: '_blank'},
        //             {label: 'Access Denied Page', icon: 'security', routerLink: ['/accessdenied'], target: '_blank'}
        //         ]
        //     },
        //     {
        //         label: 'Menu Hierarchy', icon: 'menu',
        //         items: [
        //             {
        //                 label: 'Submenu 1', icon: 'subject',
        //                 items: [
        //                     {
        //                         label: 'Submenu 1.1', icon: 'subject',
        //                         items: [
        //                             {label: 'Submenu 1.1.1', icon: 'subject'},
        //                             {label: 'Submenu 1.1.2', icon: 'subject'},
        //                             {label: 'Submenu 1.1.3', icon: 'subject'},
        //                         ]
        //                     },
        //                     {
        //                         label: 'Submenu 1.2', icon: 'subject',
        //                         items: [
        //                             {label: 'Submenu 1.2.1', icon: 'subject'},
        //                             {label: 'Submenu 1.2.2', icon: 'subject'}
        //                         ]
        //                     },
        //                 ]
        //             },
        //             {
        //                 label: 'Submenu 2', icon: 'subject',
        //                 items: [
        //                     {
        //                         label: 'Submenu 2.1', icon: 'subject',
        //                         items: [
        //                             {label: 'Submenu 2.1.1', icon: 'subject'},
        //                             {label: 'Submenu 2.1.2', icon: 'subject'},
        //                             {label: 'Submenu 2.1.3', icon: 'subject'}
        //                         ]
        //                     },
        //                     {
        //                         label: 'Submenu 2.2', icon: 'subject',
        //                         items: [
        //                             {label: 'Submenu 2.2.1', icon: 'subject'},
        //                             {label: 'Submenu 2.2.2', icon: 'subject'}
        //                         ]
        //                     },
        //                 ]
        //             }
        //         ]
        //     },
        //     {label: 'Utils', icon: 'build', routerLink: ['/utils']},
        //     {label: 'Docs', icon: 'find_in_page', routerLink: ['/documentation']}
        // ];
    }

    cargarFuncionalidades(){
        this.model2.forEach(element => {
              this.cargarFuncionalidadesHijos(element);
          });
    }
    
    cargarFuncionalidadesHijos(menu){
        if(menu.ruta){
            if(menu.ruta!==''){
                menu.parametros2=menu.codigo;
                menu.parametros=null;
                menu.parametros_vista={};
                menu.command = () => {
                                        this.sessionStorageService.setItemSync('paramsDeleted',true);
                                        let parametros={
                                            id_vista:menu.id,
                                            parametro_ruteo:menu.parametros && menu.parametros.parametro_ruteo ? menu.parametros.parametro_ruteo : null,
                                            parametro_vista:menu.codigo,
                                            ComponentesInvisibles:JSON.stringify(menu.componentes),
                                            ruta:menu.localizacion
                                        };
                                        this.sessionStorageService.setItemSync('parametros', JSON.stringify(parametros));
                                        if(menu.parametros_vista===null){
                                            menu.parametros_vista={};
                                        }
                                        menu.parametros_vista.id=menu.id;
                                        this.router.navigate([menu.ruta,menu.parametros_vista]);
                                        this.breadcrumbService.setItems([
                                            {label: menu.localizacion}
                                          ]);
                                    }
            }    
        }
        if(menu.items){
            menu.items.forEach(element => {
                this.cargarFuncionalidadesHijos(element);
            });
        }
    }
}

@Component({
    /* tslint:disable:component-selector */
    selector: '[app-submenu]',
    /* tslint:enable:component-selector */
    template: `
        <ng-template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
            <li [ngClass]="{'active-menuitem': isActive(i)}" [class]="child.badgeStyleClass" *ngIf="child.visible && child.estado!=='IN'">
                <a [href]="child.url||'#'" (click)="itemClick($event,child,i)" *ngIf="!child.routerLink"
                   [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
                   (mouseenter)="onMouseEnter(i)" class="ripplelink">
                    <i class="material-icons">{{child.icon}}</i>
                    <span class="menuitem-text">{{child.label}}</span>
                    <i class="material-icons layout-submenu-toggler" *ngIf="child.items">keyboard_arrow_down</i>
                    <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                </a>

                <a (click)="itemClick($event,child,i)" *ngIf="child.routerLink"
                   [routerLink]="child.routerLink" routerLinkActive="active-menuitem-routerlink"
                   [routerLinkActiveOptions]="{exact: true}" [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
                   (mouseenter)="onMouseEnter(i)" class="ripplelink">
                    <i class="material-icons">{{child.icon}}</i>
                    <span class="menuitem-text">{{child.label}}</span>
                    <i class="material-icons layout-submenu-toggler" *ngIf="child.items">>keyboard_arrow_down</i>
                    <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                </a>
                <ul app-submenu [item]="child" *ngIf="child.items && isActive(i)" [visible]="isActive(i)" [reset]="reset"
                    [parentActive]="isActive(i)" [@children]="(app.isHorizontal())&&root ? isActive(i) ?
                    'visible' : 'hidden' : isActive(i) ? 'visibleAnimated' : 'hiddenAnimated'"></ul>
            </li>
        </ng-template>
    `,
    animations: [
        trigger('children', [
            state('void', style({
                height: '0px'
            })),
            state('hiddenAnimated', style({
                height: '0px'
            })),
            state('visibleAnimated', style({
                height: '*'
            })),
            state('visible', style({
                height: '*',
                'z-index': 100
            })),
            state('hidden', style({
                height: '0px',
                'z-index': '*'
            })),
            transition('visibleAnimated => hiddenAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hiddenAnimated => visibleAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('void => visibleAnimated, visibleAnimated => void',
                animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppSubMenuComponent {

    @Input() item: MenuItem;

    @Input() root: boolean;

    @Input() visible: boolean;

    _reset: boolean;

    _parentActive: boolean;

    activeIndex: number;

    constructor(public app: AppMainComponent, public router: Router, public location: Location, public appMenu: AppMenuComponent) {}

    itemClick(event: Event, item: MenuItem, index: number) {
        if (this.root) {
            this.app.menuHoverActive = !this.app.menuHoverActive;
            event.preventDefault();
        }

        // avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }

        // activate current item and deactivate active sibling if any
        if (item.routerLink || item.items || item.command || item.url) {
            this.activeIndex = (this.activeIndex as number === index) ? -1 : index;
        }

        // execute command
        if (item.command) {
            item.command({originalEvent: event, item});
        }

        // prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
          setTimeout(() => {
            this.appMenu.layoutMenuScrollerViewChild.moveBar();
          }, 450);

          event.preventDefault();
        }

        // hide menu
        if (!item.items) {
            if (this.app.isMobile()) {
                this.app.sidebarActive = false;
                this.app.mobileMenuActive = false;
            }

            if (this.app.isHorizontal()) {
                this.app.resetMenu = true;
            } else {
                this.app.resetMenu = false;
            }

            this.app.menuHoverActive = !this.app.menuHoverActive;
        }
    }

    onMouseEnter(index: number) {
        if (this.root && this.app.menuHoverActive && this.app.isHorizontal()
            && !this.app.isMobile() && !this.app.isTablet()) {
            this.activeIndex = index;
        }
    }

    isActive(index: number): boolean {
        return this.activeIndex === index;
    }

    @Input() get reset(): boolean {
        return this._reset;
    }

    set reset(val: boolean) {
        this._reset = val;

        if (this._reset && (this.app.isHorizontal() || this.app.isOverlay())) {
            this.activeIndex = null;
        }
    }

    @Input() get parentActive(): boolean {
        return this._parentActive;
    }

    set parentActive(val: boolean) {
        this._parentActive = val;

        if (!this._parentActive) {
            this.activeIndex = null;
        }
    }
}
