import {Component} from '@angular/core';

@Component({
    selector: 'app-footer',
    template: `
        <div class="layout-footer clearfix">
                <img alt="logo-colored" style="width:50px; height: 50px" src="assets/layout/images/logoCorredora-light.png" />
            <span class="footer-text-right">
                <span class="material-icons">copyright</span>
                <span>Todos los derechos reservados</span>
            </span>
        </div>
    `
})
export class AppFooterComponent {

}
