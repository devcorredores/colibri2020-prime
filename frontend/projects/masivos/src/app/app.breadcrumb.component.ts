import { Router } from '@angular/router';
import { Component, OnDestroy } from '@angular/core';
import { BreadcrumbService } from '../../../../src/core/servicios/breadcrumb.service';
import { Subscription } from 'rxjs';
import { MenuItem } from 'primeng';
import {SessionStorageService} from "../../../../src/core/servicios/sessionStorage.service";

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './app.breadcrumb.component.html'
})
export class AppBreadcrumbComponent implements OnDestroy {

    subscription: Subscription;

    items: MenuItem[];

    constructor(
        public breadcrumbService: BreadcrumbService,
        private router:Router,
        private sessionStorageService:SessionStorageService
    ) {
        this.subscription = breadcrumbService.itemsHandler.subscribe(response => {
            this.items = response;
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    MenuCerrar(){
        if(this.router.url==='/AppMain'){
            return false;
        }else{
            return true;
        }
    }

    salir(){
        this.router.navigate(['/AppMain']);
        this.sessionStorageService.removeItem('parametros');
        this.sessionStorageService.setItemSync('paramsDeleted', true);
    }
}
