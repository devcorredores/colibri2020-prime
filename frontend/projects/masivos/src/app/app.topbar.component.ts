import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms'
import { Usuario } from '../../../../src/core/modelos/usuario';
import { Router } from '@angular/router';
import { LoginService } from '../../../../src/core/servicios/login.service';
import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { MessageService } from 'primeng/api';
import { AppMainComponent } from './app.main.component';
import * as app from '../../../../src/environments/environment';
import {SolicitudService} from "../../../../src/core/servicios/solicitud.service";
import {SessionStorageService} from "../../../../src/core/servicios/sessionStorage.service";

@Component({
  selector: 'app-topbar',
  templateUrl: './app.topbar.component.html',
  providers: [MessageService]
})
export class AppTopBarComponent implements OnInit, OnChanges {
  //usuario: Usuario;
  @Input() usuario: Usuario;
  userformPwd: FormGroup;
  userformChangePwd: FormGroup;
  displayPwd: boolean = false;
  displayChangePwd: boolean = false;
  versionApp: string;
  parametrosRuteo: any;
  ruta;
  roles:any[] = [];
  strRoles:string;
  interval:any;
  constructor(
    public app: AppMainComponent,
    private loginService: LoginService,
    private router: Router,
    private messageService: MessageService,
    private fb: FormBuilder,
    private sessionStorageService: SessionStorageService,
    public solicitudService:SolicitudService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.usuario && changes.usuario.currentValue) {
      this.usuario = changes.usuario.currentValue;
    }
  }

  ngOnInit() {
    this.versionApp = "v. " +  app.environment.version;
    this.usuario = this.sessionStorageService.getItemSync('userInfo') as Usuario;
    if (this.usuario.par_local_id != null) {
      if (this.usuario.pwd_change) {
        this.displayChangePwd = true;
      }
    }
    this.roles = this.usuario && this.usuario.usuarioRoles ? this.usuario.usuarioRoles : [];
    if (this.roles && this.roles.length) {
        this.strRoles = '(';
        for (let  i = 0 ; i < this.roles.length ; i++) {
            let rol:any = this.roles[i];
            if (i == this.roles.length-1) {
                this.strRoles += rol.descripcion ? rol.descripcion : '';
            } else {
                this.strRoles += rol.descripcion ? rol.descripcion+', ' : '';
            }
        }
        this.strRoles += ')';
    }
    this.userformPwd = this.fb.group({
      'passwordold': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
      'password2': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)]))
    });

    this.userformChangePwd = this.fb.group({
      'passwordold': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
      'password2': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)]))
    });
    let router = this.router;
    let sessionStorageService = this.sessionStorageService;
    this.interval = setInterval(function() {
      let userInfo = localStorage.getItem('userInfo');
      if (!userInfo) {
          router.navigate(['login']);
      }
    }, 1000);
  }

  cerrarSesion() {
    localStorage.removeItem('userInfo');
    sessionStorage.removeItem('userInfo');
    this.loginService.usuario = new Usuario();
    this.loginService.cerrarSesion().then(async (response) => {
      this.router.navigate(['']);
    });
  }
  //
  // ngOnDestroy() {
  //   this.cerrarSesion();
  // }

  resetPassword() {
    this.displayPwd = true;
  }

  cambiarPassword(pwd1: string, pwd2: string) {
    if (pwd1 === pwd2) {
      let response: { status: string, message: string, data: any };
      this.loginService.resetPassword(this.usuario.usuario_login, pwd1, pwd2, 0).subscribe(res => {
        response = res as { status: string, message: string, data: any };
        if (response.message === 'Contraseña actualizada.') {
          this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Contraseña actualizada' });
          this.displayPwd = false;
        } else {
          this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Error' });
        }
      });
    } else {
      this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Los passwords deben ser iguales' });
    }
  }

  cambiarPasswordOld(pwdold: string, pwd1: string, pwd2: string) {
    if (pwd1 === pwd2) {
      let response: { status: string, message: string, data: any };
      this.loginService.resetPasswordFirst(this.usuario.usuario_login, pwd1, pwd2, 0, pwdold).subscribe(res => {
        response = res as { status: string, message: string, data: any };
        if (response.message === 'Contraseña anterior invalida') {
          this.messageService.add({ key: 'c1', sticky: true, severity: 'warn', summary: 'Actualizacion de contraseña', detail: 'La contraseña anterior no es válida' });
        } else if (response.message === 'Contraseña actualizada.') {
          this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Actualizacion de contraseña', detail: 'Se actualizo la contraseña exitosamente' });
        } else {
          this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Error al actualizar contraseña', detail: 'Error, no se actualizo contraseña' });
        }
      });
    } else {
      this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Los passwords deben ser iguales' });
    }
  }

  onReject() {
    this.messageService.clear('c');
    // this.router.navigate(['/login']);
  }

  onConfirm() {
    this.displayPwd = false;
    this.messageService.clear('c');
    this.router.navigate(['/login']);
  }

  onConfirm1() {
    // this.displayPwd = false;
    this.messageService.clear('c1');
    // this.router.navigate(['/login']);
  }

  onReject1() {
    this.messageService.clear('c');
    // this.router.navigate(['/login']);
  }

}
