import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CambiapwdComponent } from './cambiapwd.component';

describe('CambiapwdComponent', () => {
  let component: CambiapwdComponent;
  let fixture: ComponentFixture<CambiapwdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambiapwdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CambiapwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
