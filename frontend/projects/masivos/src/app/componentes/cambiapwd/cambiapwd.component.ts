import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Usuario } from '../../../../../../src/core/modelos/usuario';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms'
import { LoginService } from '../../../../../../src/core/servicios/login.service';
import { CorreoService } from '../../../../../../src/core/servicios/correo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cambiapwd',
  templateUrl: './cambiapwd.component.html',
  styleUrls: ['./cambiapwd.component.css'],
  providers: [MessageService]
})
export class CambiapwdComponent implements OnInit {
  userformPwd: FormGroup;
  displayPwd: boolean = false;
  usuario = new Usuario();
  urlTree: any;
  username: any;
  type: any;
  agent: any;

  constructor(private router: Router,
    private loginService: LoginService,
    private messageService: MessageService,
    private correoService: CorreoService,
    private fb: FormBuilder
  ) {
    this.urlTree = this.router.parseUrl(this.router.url);
    this.username = this.urlTree.queryParams['username'];
    // this.type = this.urlTree.queryParams['type'];
    // this.agent = this.urlTree.queryParams['agent'];
  }

  ngOnInit() {
    if (this.username != 'guest') {
      this.displayPwd = true;
      this.userformPwd = this.fb.group({
        'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
        'password2': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)]))
      });
      this.Session();
    }
  }

  async Session() {
    this.loginService.login("guest", "%}N[qN>*B(r=>MeM").subscribe(async res => {
      await this.delay(1500);
      this.loginService.setUserInfo(res['user']);
      // this.router.navigate(['/AppMain']);
      // this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Order submitted' });
    }, err => {
      if (err.error.message) {
        this.messageService.clear();
        this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: err.error.message.message, detail: '' });
      } else {
        this.messageService.clear();
        this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: err.message, detail: '' });
      }
    });
  }

  ngOnDestroy() {
    this.cerrarSesion();
  }

  cerrarSesion() {
    localStorage.removeItem('userInfo');
    this.loginService.cerrarSesion().then(async (response) => {
      this.router.navigate(['']);
    });
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  onReject() {
    this.messageService.clear('c');
  }

  onConfirm() {
    this.displayPwd = false;
    this.messageService.clear('c');
    this.messageService.add({ severity: 'success', summary: 'Gestion usuarios', detail: 'Proceso exitoso' });
  }

  cambiarPassword(pwd1: string, pwd2: string) {
    if (pwd1 === pwd2) {
      let response: { status: string, message: string, data: any };
      this.loginService.resetPasswordRequest(this.username, pwd1, pwd2, 0).subscribe(res => {
        response = res as { status: string, message: string, data: any };
        if (response.message === 'Contraseña actualizada.') {
          this.messageService.clear();
          this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Contraseña', detail: 'Contraseña cambiada exitosamente' });
          this.router.navigate(['/login']);
          // var mailOptionsPwd = {
          //   "para": this.usuario.usuario_email,
          //   "asunto": "Cambio de Contraseña Colibri-2020",
          //   "mensaje": `Señor <br>${this.usuario.usuario_login}: <br><br> Mediante la presente informarle que se ha cambiado exitosamente la contraseña a <u><b>${pwd1}</b></u> segun requerimiento de ${this.usuario.usuario_email}.<br><br> La cual debe ser cambiada por usted la siguiente vez que ingrese a <a href='https://www.corredoresecofuturo.com:7000'> Colibri2020 </a>`
          // }
          // this.correoService.enviarEmail(mailOptionsPwd).subscribe(resp => {
          //   response = resp as any;
          // });
          // this.displayPwd = false;

        } else {
          this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Error' });
        }
      });
    }
    else {
      this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Los passwords deben ser iguales' });
    }
  }
}

