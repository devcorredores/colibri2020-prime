import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoRiesgoComponent } from './alta-eco-riesgo.component';

describe('AltaEcoRiesgoComponent', () => {
  let component: AltaEcoRiesgoComponent;
  let fixture: ComponentFixture<AltaEcoRiesgoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoRiesgoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoRiesgoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
