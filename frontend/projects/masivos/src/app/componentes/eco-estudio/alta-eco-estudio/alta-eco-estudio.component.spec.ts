import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoEstudioComponent } from './alta-eco-estudio.component';

describe('AltaEcoEstudioComponent', () => {
  let component: AltaEcoEstudioComponent;
  let fixture: ComponentFixture<AltaEcoEstudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoEstudioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoEstudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
