import { persona_banco } from '../../../../../../../src/core/modelos/persona_banco';
import { SoapuiService } from '../../../../../../../src/core/servicios/soapui.service';
import { Parametro } from '../../../../../../../src/core/modelos/parametro';
import { Poliza } from '../../../../../../../src/core/modelos/poliza';
import { SelectItem, MessageService, ConfirmationService, Message } from 'primeng';
import { ParametrosService } from '../../../../../../../src/core/servicios/parametro.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { InstanciaAnexoService } from '../../../../../../../src/core/servicios/instancia-anexo.service';
import { BreadcrumbService } from '../../../../../../../src/core/servicios/breadcrumb.service';
import { ActivatedRoute } from '@angular/router';
import { MenuService } from '../../../../../../../src/core/servicios/menu.service';
import { AnexoService } from '../../../../../../../src/core/servicios/anexo.service';
import { Component, OnInit, Input } from '@angular/core';
import { Instancia_anexo_asegurado } from '../../../../../../../src/core/modelos/instancia_anexo_asegurado';
import {SessionStorageService} from "../../../../../../../src/core/servicios/sessionStorage.service";
import {Util} from "../../../../../../../src/helpers/util";

@Component({
  selector: 'app-anexo',
  templateUrl: './anexo.component.html',
  styleUrls: ['./anexo.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class AnexoComponent implements OnInit {

  @Input() parametrosAnexo: any;

  poliza: Poliza = new Poliza();
  vista: any;
  componentesInvisibles: any;
  variv = 'ejemplo';
  display: boolean = false;
  instancia_anexo_asegurado: Instancia_anexo_asegurado = new Instancia_anexo_asegurado();
  instancia_anexo_banco: Instancia_anexo_asegurado = new Instancia_anexo_asegurado();
  instancia_anexo_asegurado_anterior: Instancia_anexo_asegurado = new Instancia_anexo_asegurado();
  userform: FormGroup;
  userform2: FormGroup;
  parametros: Parametro[] = [];
  Parentescos: SelectItem[] = [];
  Extensiones: SelectItem[] = [];
  TiposDocumentos: SelectItem[] = [];
  Generos: SelectItem[] = [];
  ids_diccionarios: number[] = [];
  objVal = {};
  userInfo;
  index_anexo_poliza: number = -1;
  index_anexo_asegurado: number = -1;
  index_instancia_anexo_asegurado = -1;
  util = new Util();
  displayConfirmation = false;
  theHtmlString: string = '';
  doc_id: number;
  ext: number;
  displayAceptarClienteBanco = false;
  displayBuscardorBanco = false;
  displaySeleccion = false;
  msgs: Message[] = [];
  ValidacionesMensajes: Message[] = [];
  ValidacionesMensajesOtrosCambos: Message[] = [];
  displayValidaciones = false;
  nuevo = true;
  displayBeneficiariosNotFound = false;
  botonDelete: boolean = true;
  msgs_error: Message[] = [];
  msgs_warn: Message[] = [];
  displaySalir: boolean = false;
  nopreguntar: boolean = true;
  botonGuardar = true;
  DatosVisible = false;
  mensaje = "";
  displayCarnetExistente = false;
  validar = true;
  validar2 = true;
  RegistrarSinBanco = false;
  mensajeTooltip = "No hay mensaje";
  CarnetObligario = false;
  ExtensionObligario = true;

  constructor(private instanciaAnexoService: InstanciaAnexoService,
    private anexoService: AnexoService,
    private menuService: MenuService,
    private params: ActivatedRoute,
    private breadcrumbService: BreadcrumbService,
    private parametrosService: ParametrosService,
    private fb: FormBuilder,
    private service: MessageService,
    private soapuiService: SoapuiService,
    private sessionStorageService: SessionStorageService,
    private confirmationService: ConfirmationService) {
  }

  async ngOnInit() {
    this.userInfo = this.sessionStorageService.getItemSync('userInfo');
    this.ids_diccionarios = [20, 18, 1, 4];

    this.userform2 = this.fb.group({
      'Nro. Carnet': new FormControl(''),
      'Extencion de Carnet': new FormControl('')
    });

    this.objVal = {
      'Apellido Paterno': new FormControl(''),
      'Apellido Materno': new FormControl(''),
      'Primer Nombre': new FormControl(''),
      'Segundo Nombre': new FormControl(''),
      'Parentesco': new FormControl(''),
      'Fecha de Nacimiento': new FormControl(''),
      'Nro. Carnet': new FormControl(''),
      'Extencion de Carnet': new FormControl(''),
      'Tipo de documento': new FormControl(''),
      'Genero': new FormControl(''),
      'Descripcion de la relacion': new FormControl(''),
      'Telefono de domicilio': new FormControl('')
    }

    this.userform = this.fb.group(this.objVal);
    if (this.parametrosAnexo.id_anexo_poliza !== null && this.parametrosAnexo.id_asegurado > 0) {
      await this.listaAtributosByIdPoliza(this.parametrosAnexo.id_poliza, this.parametrosAnexo.id_asegurado, this.parametrosAnexo.id_anexo_poliza);
    }

    this.userform.valueChanges.subscribe(
      data => {
        if(this.parametrosAnexo.id_anexo_poliza !== null && this.parametrosAnexo.id_asegurado > 0){
          this.validarDatos();
        }
      });

    /*this.userform2.controls['Nro. Carnet'].valueChanges.subscribe(
      data => {
        this.ValidaCarnetUnicos();
      });*/

    this.poliza.anexo_polizas.forEach(anexo_poliza => {
      anexo_poliza.anexo_asegurados.forEach(anexo_asegurado => {
        anexo_asegurado.instancia_anexo_asegurados.forEach((instancia_anexo_asegurado, index) => {
          this.instancia_anexo_asegurado = instancia_anexo_asegurado;
        });
      });
    });

  }

  async listaAtributosByIdPoliza(id_poliza: number, id_asegurado: number, id_anexo_poliza: number) {
    await this.anexoService.listaAtributosByIdPoliza(id_poliza, id_asegurado, id_anexo_poliza).then(res => {
      let response = res as { status: string, message: string, data: Poliza };
      this.poliza = response.data;
      this.poliza.objetos.forEach(objeto => {
        objeto.objeto_x_atributos.forEach(objeto_x_atributo => {
          if (objeto_x_atributo.atributo.id_diccionario !== null) {
            this.ids_diccionarios.push(parseInt(objeto_x_atributo.atributo.id_diccionario + ''));
          }
        });
      });
      this.GetAllParametrosByIdDiccionarios(this.ids_diccionarios);
      this.GeneraJsonFormValidation();
      this.poliza.anexo_polizas.forEach(anexo_poliza => {
        anexo_poliza.anexo_asegurados.forEach(anexo_asegurado => {
          anexo_asegurado.instancia_anexo_asegurados.forEach(instancia_anexo_asegurado => {
            instancia_anexo_asegurado.entidad.persona.persona_fecha_nacimiento = new Date(instancia_anexo_asegurado.entidad.persona.persona_fecha_nacimiento);
            instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext = instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext+'';
            instancia_anexo_asegurado.observaciones = [];
          });
        });
      });
    });
  }

  GuardarAnexo() {
    this.botonGuardar = false;
    this.instancia_anexo_asegurado.id_asegurado = this.parametrosAnexo.id_asegurado;
    this.instancia_anexo_asegurado.id_anexo_asegurado = this.poliza.anexo_polizas[0].anexo_asegurados[0].id;
    this.TodasLasValidaciones();
    this.instanciaAnexoService.SaveOrUpdateInstancia_anexo_asegurado({ instancia_anexo_asegurado: this.instancia_anexo_asegurado, id_usuario: this.userInfo.id }).subscribe(res => {
      let response = res as { status: string, message: string, data: Instancia_anexo_asegurado };
      if (response.status === 'OK') {
        if (!this.poliza.anexo_polizas[this.index_anexo_poliza].anexo_asegurados[this.index_anexo_asegurado].instancia_anexo_asegurados.find(param => param.id + '' === response.data.id + '')) {
          this.poliza.anexo_polizas[this.index_anexo_poliza].anexo_asegurados[this.index_anexo_asegurado].instancia_anexo_asegurados.push(response.data);
        }
        this.display = false;
        this.nopreguntar = false;
        this.botonGuardar = true;
        this.service.add({ key: 'tst', severity: 'success', summary: 'Exitoso', detail: 'Se Registro Correctamente' });
      } else {
        this.service.add({ key: 'tst', severity: 'error', summary: 'Error', detail: 'No se ha podido realizar el registro' });
        this.botonGuardar = true;
      }
    });
  }

  GetAllParametrosByIdDiccionarios(ids: number[]) {
    //ids ponemos los id de los diccionarios que necesitemos
    this.parametrosService.GetAllParametrosByIdDiccionarios(ids).subscribe(res => {
      let response = res as { status: string, message: string, data: Parametro[] };
      this.parametros = response.data;
      this.Parentescos.push({ label: "Seleccione Parentesco", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "20").forEach(element => {
        this.Parentescos.push({ label: element.parametro_descripcion, value: element.id });
      });
      if (this.parametrosAnexo.id_poliza+''==='5'){
        this.Parentescos=this.Parentescos.filter(param=>param.value+''!=='56');
      }
      this.Extensiones.push({ label: "Seleccione Extension", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "18").forEach(element => {
        this.Extensiones.push({ label: element.parametro_abreviacion, value: parseInt(element.parametro_cod) });
      });
      this.TiposDocumentos.push({ label: "Seleccione Tipo de documento", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "1").forEach(element => {
        this.TiposDocumentos.push({ label: element.parametro_descripcion, value: element.id });
      });
      this.Generos.push({ label: "Seleccione Genero", value: null });
      this.parametros.filter(param => param.diccionario_id + '' === "4").forEach(element => {
        this.Generos.push({ label: element.parametro_descripcion, value: element.id });
      });

      this.poliza.objetos.forEach(objeto => {
        objeto.objeto_x_atributos.forEach(objeto_x_atributo => {
          if (objeto_x_atributo.atributo.id_diccionario !== null) {
            let SelItArray: SelectItem[] = [];
            SelItArray.push({ label: "Seleccione " + objeto_x_atributo.atributo.diccionario.diccionario_descripcion, value: '' });
            this.parametros.filter(param => param.diccionario_id + '' === objeto_x_atributo.atributo.id_diccionario + '').forEach(element => {
              SelItArray.push({ label: element.parametro_descripcion, value: element.id });
            });
            objeto_x_atributo.atributo.diccionarios = SelItArray;
          }
        });
      });
    });
  }

  GeneraJsonFormValidation() {
    this.poliza.objetos.forEach(objeto => {
      objeto.objeto_x_atributos.forEach(objeto_x_atributo => {
        //this.objVal[objeto_x_atributo.atributo.descripcion]=
        if (objeto_x_atributo.obligatorio) {
          //this.objVal[objeto_x_atributo.atributo.descripcion]=new FormControl('', Validators.required);
        }
      });
    });
    this.poliza.anexo_polizas.forEach(anexo_poliza => {
      anexo_poliza.anexo_asegurados.forEach(anexo_asegurado => {
        anexo_asegurado.dato_anexo_asegurados.forEach(dato_anexo => {
          if (dato_anexo.obligatorio) {
            this.objVal[dato_anexo.descripcion] = new FormControl('', Validators.required);
          }
          if (dato_anexo.descripcion === 'Nro. Carnet') {
            this.CarnetObligario = dato_anexo.obligatorio;
          }
          if (dato_anexo.descripcion === 'Extencion de Carnet') {
            this.ExtensionObligario = dato_anexo.obligatorio;
          }
        });
      });
    });
    this.userform = this.fb.group(this.objVal);
  }

  NuevoAnexo(i: number, j: number) {
    this.validar = false;
    this.validar2 = false;
    this.nopreguntar = true;
    this.index_anexo_poliza = i;
    this.index_anexo_asegurado = j;
    this.instancia_anexo_asegurado = new Instancia_anexo_asegurado();
    this.userform.reset();
    this.display = true;
    this.nuevo = true;
    this.doc_id = null;
    this.ext = null;
    this.instancia_anexo_asegurado.observaciones = [];
    this.RegistrarSinBanco = false;
    this.DatosVisible = false;
  }

  EditarAnexo(ane_ins: Instancia_anexo_asegurado, index_anexo_poliza: number, index_anexo_asegurado: number, index_instancia_anexo_asegurado: number) {
    this.index_anexo_poliza = index_anexo_poliza;
    this.index_anexo_asegurado = index_anexo_asegurado;
    this.index_instancia_anexo_asegurado = index_instancia_anexo_asegurado;
    this.instancia_anexo_asegurado.entidad.persona.persona_fecha_nacimiento = new Date(this.instancia_anexo_asegurado.entidad.persona.persona_fecha_nacimiento);
    this.instancia_anexo_asegurado = ane_ins;
    this.instancia_anexo_asegurado_anterior = JSON.parse(JSON.stringify(ane_ins));
    this.display = true;
    this.nuevo = false;
    this.nopreguntar = true;
    this.DatosVisible = true;
  }

  devolverDescripcion(id: any): string {
    if (id && this.parametros.length > 0) {
      if (this.parametros.find(param => param.id + '' === id + '')) {
        return this.parametros.find(param => param.id + '' === id + '').parametro_descripcion;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  devolverAbreviacion(id: any): string {
    if (id && this.parametros.length > 0) {
      if (this.parametros.find(param => param.id + '' === id + '')) {
        return this.parametros.find(param => param.id + '' === id + '').parametro_abreviacion;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  validarDatos() {
    this.msgs_error = [];
    this.msgs_warn = [];
    this.instancia_anexo_asegurado.observaciones = [];
    for (let nameControl in this.userform.controls) {
      if (this.userform.controls[nameControl].errors !== null) {
        for (let error in this.userform.controls[nameControl].errors) {
          if (error === 'required') {
            this.instancia_anexo_asegurado.observaciones.push({ key: '1', severity: 'error', detail: '(' + this.poliza.anexo_polizas[0].anexo_asegurados[0].parametro.parametro_descripcion + ') El campo ' + nameControl + ' es requerido' });
            this.msgs_error.push({ key: '1', severity: 'error', detail: 'El campo ' + nameControl + ' es requerido' });
          }
        }
      } else {

        if (((this.userform.controls[nameControl].value + '').trim() === '' || this.userform.controls[nameControl].value === null) && nameControl !== 'Descripcion de la relacion') {
          this.instancia_anexo_asegurado.observaciones.push({ key: '2', severity: 'warn', detail: '(' + this.poliza.anexo_polizas[0].anexo_asegurados[0].parametro.parametro_descripcion + ') El campo ' + nameControl + ' esta vacio' });
          this.msgs_warn.push({ key: '2', severity: 'warn', detail: 'El campo ' + nameControl + ' esta vacio' });
        }
      }
    }

    let carnetRepetido = this.ValidaCarnetUnicos();

    if (carnetRepetido !== "" && carnetRepetido) {
      this.instancia_anexo_asegurado.observaciones.push({ key: '3', severity: 'error', detail: carnetRepetido });
      //this.msgs_error.push({ key: '3', severity: 'error', detail: carnetRepetido });
    }

    this.ValidacionesMensajes = [];
    this.instancia_anexo_asegurado.observaciones.filter(param => param.key === '3').forEach(observacion => {
      this.ValidacionesMensajes.push(observacion);
    });

    let conyugue = this.ValidarSoloUNConyugeOconviviente();

    if (conyugue !== "" && conyugue) {
      this.instancia_anexo_asegurado.observaciones.push({ key: '4', severity: 'error', detail: conyugue });
    }

    let ValEdad = this.ValidacionEdadesPorRelacionEcoMedic();

    if (ValEdad !== "" && ValEdad) {
      this.instancia_anexo_asegurado.observaciones.push({ key: '4', severity: 'error', detail: ValEdad });
    }

    this.ValidacionesMensajesOtrosCambos = [];
    this.instancia_anexo_asegurado.observaciones.filter(param => param.key === '4').forEach(observacion => {
      this.ValidacionesMensajesOtrosCambos.push(observacion);
    });

    this.mensajeTooltip = "";
    let countObs = 0;
    this.instancia_anexo_asegurado.observaciones.filter(param => param.severity === 'error').forEach(observacion => {
      countObs++;
    });
    if (countObs > 0) {
      this.mensajeTooltip = "Registrado con Errores";
    } else {
      this.mensajeTooltip = "Registrado correctamente";
    }
  }

  ValidarFormulario(obsers: Message[]): boolean {
    if (obsers) {
      if (obsers.filter(param => param.severity === 'error').length > 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  Salir() {
    if ((this.instancia_anexo_asegurado.entidad.persona.persona_doc_id + '').trim() === ''
      || this.instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext === null
      || this.instancia_anexo_asegurado.entidad.persona.persona_doc_id === null
      || this.instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext === '') {
      this.userform.reset();
      this.display = false;
      this.displaySalir = false;
    } else {
      this.validarDatos();
      this.displayValidaciones = true;
      this.display = false;
      this.displaySalir = false;
    }
  }

  SalirSinValidar(i: number, j: number) {
    if (!this.displayValidaciones && this.nopreguntar) {
      if (((this.instancia_anexo_asegurado.entidad.persona.persona_doc_id + '').trim() === ''
        || this.instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext === null
        || this.instancia_anexo_asegurado.entidad.persona.persona_doc_id === null
        || this.instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext === '')
        && !this.RegistrarSinBanco) {
        this.userform.reset();
        this.display = false;
      } else {
        this.displaySalir = true;
        this.display = false;
      }
    }
  }

  GeneraMensaje(): string {
    let html = "";

    if (this.instancia_anexo_asegurado.observaciones.length > 0) {
      html = html + "<h5><b>Existen las siguientes observaciones deseas GUARDAR?:<br /></b></h5>";
      html = html + "<table class='table' style='text-size=10'><thead><tr><th>Obligatorias</th></tr></thead><tbody><tr><td>";
      html = html + '<style>li span { position: relative; left: 0px; }.contenedor-arbol, .contenedor-arbol ul, .contenedor-arbol li {position: relative;}.contenedor-arbol ul {list-style: none;}.contenedor-arbol li::before, .contenedor-arbol li::after {content: "";position: absolute;left: -12px; }.contenedor-arbol li::before { border-top: 2px solid green;top: 9px; width: 8px;height: 0}.contenedor-arbol li::after {border-left: 2px solid brown;height: 100%; width: 0px;top: 2px;}.contenedor-arbol ul > li:last-child::after { height: 8px;} </style>';
      html = html + '<div align="left" class="contenedor-arbol">';
      html = html + "<ul><li>Obligatorios<ul>";
      this.instancia_anexo_asegurado.observaciones.filter(param => param.severity === 'error').forEach(element => {
        html = html + "<li>" + element.detail + "<span class='fa fa-exclamation-circle' style='color:red;'></span></li>";
      });
      html = html + "</ul></li></ul>";
      html = html + "<ul><li>Advertencias<ul>";
      this.instancia_anexo_asegurado.observaciones.filter(param => param.severity === 'warning').forEach(element => {
        html = html + "<li>" + element.detail + "<span class='fa fa-exclamation-circle' style='color:red;'></span></li>";
      });
      html = html + "</ul></li></ul>";
      html = html + "</div>";
      //html=html+"</div>"
      html = html + "</td></tr></tbody></html>";
    }
    return html;
  }

  Aceptar() {
    this.GuardarAnexo();
    this.display = false;
    this.displayValidaciones = false;
    this.displaySalir = false;
  }

  Cancelar() {
    if (!this.nuevo) {
      this.poliza.anexo_polizas[this.index_anexo_poliza].anexo_asegurados[this.index_anexo_asegurado].instancia_anexo_asegurados[this.index_instancia_anexo_asegurado] = this.instancia_anexo_asegurado_anterior;
    }
    this.displayValidaciones = false;
    this.display = false;
    this.displaySalir = false;
  }

  BusacarClienteBanco() {
    this.soapuiService.getCustomer(this.instancia_anexo_asegurado.entidad.persona.persona_doc_id, this.instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext).subscribe(res => {
      let response = res as { status: string, message: string, data: persona_banco };
      if (response.data != undefined) {
        this.DatosVisible = true;
        this.instancia_anexo_asegurado = new Instancia_anexo_asegurado();
        this.instancia_anexo_asegurado.entidad.persona.persona_primer_apellido = response.data.paterno;
        this.instancia_anexo_asegurado.entidad.persona.persona_segundo_apellido = response.data.materno;
        this.instancia_anexo_asegurado.entidad.persona.persona_primer_nombre = response.data.nombre;
        this.instancia_anexo_asegurado.entidad.persona.persona_apellido_casada = response.data.apcasada;
        this.instancia_anexo_asegurado.entidad.persona.persona_doc_id = response.data.doc_id;
        this.instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext = this.parametros.find(param => param.parametro_abreviacion + '' === response.data.extension + '' && param.diccionario_id + '' === '18').parametro_cod + '';
        this.instancia_anexo_asegurado.entidad.persona.par_sexo_id = this.parametros.find(param => param.parametro_abreviacion + '' === response.data.sexo + '' && param.diccionario_id + '' === '4').id + '';
        this.instancia_anexo_asegurado.entidad.persona.persona_telefono_domicilio = response.data.fono_domicilio;
        this.instancia_anexo_asegurado.entidad.persona.persona_telefono_trabajo = response.data.fono_oficina;
        this.instancia_anexo_asegurado.entidad.persona.persona_fecha_nacimiento = new Date(response.data.anio_fechanac + '/' + response.data.mes_fechanac + '/' + response.data.dia_fechanac);
        this.instancia_anexo_asegurado.entidad.persona.persona_celular = response.data.nro_celular;
        this.instancia_anexo_asegurado.entidad.persona.par_tipo_documento_id = response.data.tipo_doc;
        this.instancia_anexo_asegurado.entidad.persona.persona_email_personal = response.data.e_mail;
        this.instancia_anexo_asegurado.entidad.persona.persona_telefono_domicilio = response.data.fono_domicilio;
      } else {
        this.displayBeneficiariosNotFound = true;
        this.display = false;
        this.nopreguntar = false;
      }
    });
  }

  EditarClienteBanco() {
    this.instancia_anexo_asegurado = this.instancia_anexo_banco;
    this.displayConfirmation = false;
    this.display = true;
  }

  NoAceptarCliente() {
    this.displayConfirmation = false;
  }

  ConfirmacionAceptacionClienteBanco() {
    this.BusacarClienteBanco();
    this.displayConfirmation = true;
    this.displayBuscardorBanco = false;
  }

  AbrirdisplayBuscardorBanco() {
    this.displaySeleccion = false;
    this.displayBuscardorBanco = true;
  }

  velidarAllAnexoAsegurado() {
    let obs: Message[] = [];
    this.msgs = [];
    this.poliza.anexo_polizas.forEach(anexo_poliza => {
      anexo_poliza.anexo_asegurados.forEach(anexo_asegurado => {
        if (anexo_asegurado.nro_obligatorios <= anexo_asegurado.instancia_anexo_asegurados.length) {
          anexo_asegurado.instancia_anexo_asegurados.forEach(instancia_anexo_asegurado => {
            if (instancia_anexo_asegurado.observaciones) {
              instancia_anexo_asegurado.observaciones.filter(ins => ins.severity === 'error').forEach(observacion => {
                obs.push(observacion);
              });
            }
          });
          if (obs.length > 0) {
            this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Tiene observaciones en ' + anexo_asegurado.parametro.parametro_descripcion });
          }
        } else {
          this.msgs.push({ severity: 'warn', summary: 'Advertencia', detail: 'La poliza requiere al menos ' + anexo_asegurado.nro_obligatorios + ' ' + anexo_asegurado.parametro.parametro_descripcion });
        }

      });
    });
    return true;
  }

  getValidacionAnexos(): Message[] {
    let mensajes: Message[] = [];
    this.poliza.anexo_polizas[0].anexo_asegurados[0].instancia_anexo_asegurados.forEach(instancia_ase => {
      instancia_ase.observaciones.forEach(observacion => {
        mensajes.push(observacion);
      });
    });
    this.poliza.anexo_polizas[0].anexo_asegurados.forEach(anexo_asegurado => {
      if (anexo_asegurado.nro_obligatorios > anexo_asegurado.instancia_anexo_asegurados.length) {
        mensajes.push({ key: '2', severity: 'error', detail: 'La poliza requiere al menos ' + anexo_asegurado.nro_obligatorios + ' ' + anexo_asegurado.parametro.parametro_descripcion });
      }
    });

    return mensajes;
  }

  Eliminar(instancia_anexo_asegurado: Instancia_anexo_asegurado, i: number, j: number, k: number) {
    this.botonDelete = false;
    this.index_anexo_poliza = i;
    this.index_anexo_asegurado = j;
    this.index_instancia_anexo_asegurado = k;
    this.instanciaAnexoService.Eliminar({ instancia_anexo_asegurado: instancia_anexo_asegurado, id_usuario: this.userInfo.id }).subscribe(res => {
      let response = res as { status: string, message: string, data: Instancia_anexo_asegurado };
      if (response.status === 'OK') {
        this.poliza.anexo_polizas[this.index_anexo_poliza].anexo_asegurados[this.index_anexo_asegurado].instancia_anexo_asegurados.splice(this.index_instancia_anexo_asegurado, 1);
        this.service.add({ key: 'tst', severity: 'success', summary: 'Exitoso', detail: 'Se Elimino Correctamente' });
      } else {
        this.service.add({ key: 'tst', severity: 'error', summary: 'Error', detail: 'No se pudo eliminar el Registro' });
      }
      this.botonDelete = true;
    });
  }

  registrarNuevoConCI() {
    this.displayBeneficiariosNotFound = false;
    this.display = true;
    this.DatosVisible = true;
    this.RegistrarSinBanco = true;
  }

  registrarNuevoSinCI() {
    this.displayBeneficiariosNotFound = false;
    this.display = false;
    this.DatosVisible = true;
    this.userform.reset();
  }

  ExistenAtributos_x_obejtos(): boolean {
    let sum = 0;
    this.poliza.objetos.forEach(objeto => {
      sum = sum + objeto.objeto_x_atributos.length;
    });
    if (sum === 0) {
      return false;
    } else {
      return true;
    }
  }

  ValidaCarnetUnicos(): string {
    if (this.validar) {
      let countCi = 0;
      let nombres = "";
      this.poliza.anexo_polizas.forEach(anexo_poliza => {
        anexo_poliza.anexo_asegurados.forEach(anexo_asegurado => {
          anexo_asegurado.instancia_anexo_asegurados.forEach(instancia_anexo => {
            if (instancia_anexo.entidad.persona.persona_doc_id + '' === this.userform2.controls['Nro. Carnet'].value + ''
              && instancia_anexo.entidad.persona.persona_doc_id_ext + '' === this.userform2.controls['Extencion de Carnet'].value + ''
              && instancia_anexo.id + '' !== this.instancia_anexo_asegurado.id + '') {
              countCi++;
              nombres = instancia_anexo.entidad.persona.persona_primer_nombre + ' ' + instancia_anexo.entidad.persona.persona_primer_apellido + ' ' + instancia_anexo.entidad.persona.persona_segundo_apellido
            }
          });
        });
      });

      if (this.parametrosAnexo.asegurado !== null) {
        if (this.parametrosAnexo.asegurado.entidad.persona.persona_doc_id + '' === this.instancia_anexo_asegurado.entidad.persona.persona_doc_id + ''
          && this.parametrosAnexo.asegurado.entidad.persona.persona_doc_id_ext + '' === this.instancia_anexo_asegurado.entidad.persona.persona_doc_id_ext + '') {
          countCi++;
          nombres = this.parametrosAnexo.asegurado.entidad.persona.persona_primer_nombre + ' ' + this.parametrosAnexo.asegurado.entidad.persona.persona_primer_apellido + ' ' + this.parametrosAnexo.asegurado.entidad.persona.persona_segundo_apellido
        }
      }
      if (countCi > 0) {
        this.mensaje = 'El numero numero de carnet y la extención ya esta siendo usado por ' + nombres + ", utilice otro carnet";
        return this.mensaje;
      }
    } else {
      this.validar = true;
      return "";
    }
  }


  UtilizarOtroCarnet() {
    this.displayCarnetExistente = false;
    this.nopreguntar = true;
    this.display = true;
  }

  TodasLasValidaciones() {
    this.ValidaCarnetUnicos();
    this.ValidarSoloUNConyugeOconviviente();
  }

  NumeroErroresByKey(key: string): number {
    return this.instancia_anexo_asegurado.observaciones.filter(param => param.key === key).length;
  }

  ValidarSoloUNConyugeOconviviente(): string {
    let nombres = "";
    if (this.validar2) {
      this.poliza.anexo_polizas.forEach(anexo_poliza => {
        anexo_poliza.anexo_asegurados.forEach(anexo_asegurado => {
          anexo_asegurado.instancia_anexo_asegurados.forEach(instancia_anexo => {
            if ((instancia_anexo.id_parentesco + '' === '48' || instancia_anexo.id_parentesco + '' === '49')
              && instancia_anexo.id + '' !== this.instancia_anexo_asegurado.id + ''
              && (this.userform.controls['Parentesco'].value + '' === '48' || this.userform.controls['Parentesco'].value + '' === '49')) {
              nombres = instancia_anexo.entidad.persona.persona_primer_nombre + ' ' + instancia_anexo.entidad.persona.persona_primer_apellido + ' ' + instancia_anexo.entidad.persona.persona_segundo_apellido
            }
          });
        });
      });
      if (nombres !== "") {
        nombres = 'Ya tiene a ' + nombres + " como conyugue o conviviente, seleccione otro parentesco";
      }
    } else {
      this.validar2 = true;
    }
    return nombres;
  }

  ValidacionEdadesPorRelacionEcoMedic(): string {
    let mensaje = "";
    let primerRelaciones = ['48', '48', '53', '55'];
    let segundaRelaciones = ['50'];
    if (this.validar2) {
      let edad = this.util.getEdad(this.userform.controls['Fecha de Nacimiento'].value);
      let paraentesco = primerRelaciones.find(param => param === this.userform.controls['Parentesco'].value + '');
      if (paraentesco
        && (edad < 18 || edad > 64)) {
        mensaje = "Para relacion " + this.devolverDescripcion(this.userform.controls['Parentesco'].value) + 'la edad tiene que estar entre 18 y 64';
      }

      let paraentesco2 = segundaRelaciones.find(param => param === this.userform.controls['Parentesco'].value + '');
      if (paraentesco2
        && (edad < 0 || edad > 24)) {
        mensaje = "Para relacion " + this.devolverDescripcion(this.userform.controls['Parentesco'].value) + ' la edad tiene que estar entre 0 y 18';
      }
    } else {
      this.validar2 = true;
    }
    return mensaje;
  }

  confirm(i: number, j: number) {
    this.index_anexo_poliza = i;
    this.index_anexo_asegurado = j;
    if (this.CarnetObligario && this.ExtensionObligario) {
      this.confirmationService.confirm({
        message: 'Usted cuenta con el Numero de Carnet y la Extención?',
        accept: () => {
          this.NuevoAnexo(i, j);
        },
        acceptLabel: 'SI',
        rejectLabel: 'NO',
        reject: () => {
          this.display = true;
          this.nuevo = true;
          this.nopreguntar = true;
          this.DatosVisible = true;
          this.instancia_anexo_asegurado = new Instancia_anexo_asegurado();
          this.RegistrarSinBanco = true
          this.userform.reset();
        }
      });
    }else{
      this.NuevoAnexo(i, j);
    }
  }
}
