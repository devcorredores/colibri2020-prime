import { SolicitudService } from '../../../../../../../src/core/servicios/solicitud.service';
import { MessageService, SortEvent } from 'primeng/api';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Parametro } from '../../../../../../../src/core/modelos/parametro';
import { SelectItem } from 'primeng';
import { ParametrosService } from '../../../../../../../src/core/servicios/parametro.service';
import { Router } from '@angular/router';
import { Reporte_query } from '../../../../../../../src/core/modelos/reporte_query';
import { ReporteQueryService } from '../../../../../../../src/core/servicios/reporte-query.service';
import { Component, OnInit } from '@angular/core';
import * as xlsx from 'xlsx';
import {SessionStorageService} from "../../../../../../../src/core/servicios/sessionStorage.service";
import {FilterUtils} from "primeng/utils";
import {Util} from "../../../../../../../src/helpers/util";
import * as moment from 'moment';
import * as FileSaver from 'file-saver';
import {Usuario} from "../../../../../../../src/core/modelos/usuario";
declare var $:any

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css'],
  providers: [MessageService]
})
export class ReportesComponent implements OnInit {

  userInfo:Usuario;
  reportes: Reporte_query[];
  exportColumns: any[];
  exportColumns2: any[];
  cols: any;
  cols2: any;
  reporteSelect: Reporte_query = new Reporte_query();
  util = new Util();
  resultado_query: any[] = [];
  ids_diccionarios: number[] = [];
  parametros: Parametro[] = [];
  userform: FormGroup;
  objVal = {};
  btnEjecuta = false;
  a: boolean = false;
  b: boolean = false;
  c: boolean = false;
  onePastMonth: Date;
  twoPastMonth: Date;
  today: Date;
  lastMonth: Date;

  constructor(private reporteQueryService: ReporteQueryService
            , private router: Router
            , private parametrosService: ParametrosService
            , private fb: FormBuilder
            , private service: MessageService
            , private sessionStorageService: SessionStorageService
            , public solicitudService: SolicitudService) {
    this.userInfo = sessionStorageService.getItemSync('userInfo');
    this.cols = [
      { field: 'poliza', header: 'Poliza', width: '20%' },
      { field: 'descripcion', header: 'Reporte', width: '70%' },
      { field: null, header: 'Cargar Reporte' }
    ];

    FilterUtils['custom'] = (value, filter): boolean => {
      if (filter === undefined || filter === null || filter.trim() === '') {
        return true;
      }

      if (value === undefined || value === null) {
        return false;
      }

      return parseInt(filter) > value;
    }
  }

  ngOnInit() {
    this.today = moment(new Date()).toDate();
    this.onePastMonth = moment(new Date()).add(-1,'months').toDate();
    this.twoPastMonth = moment(new Date()).add(-2,'months').toDate();
    this.lastMonth = moment(new Date()).add(-2,'months').set('date',1).toDate();
    setTimeout(() => {
      if($('.layout-wrapper').hasClass('layout-wrapper-static')) {
        $('.ui-table').addClass('ui-table-fixed-opened-sidebar');
        $('.ui-table').removeClass('ui-table-fixed-closed-sidebar');
      } else {
        $('.ui-table').addClass('ui-table-fixed-closed-sidebar');
        $('.ui-table').removeClass('ui-table-fixed-opened-sidebar');
      }
      $('.sidebar-anchor').click(() => {
        if($('.layout-wrapper').hasClass('layout-wrapper-static')) {
          $('.ui-table').addClass('ui-table-fixed-opened-sidebar');
          $('.ui-table').removeClass('ui-table-fixed-closed-sidebar');
        } else {
          $('.ui-table').addClass('ui-table-fixed-closed-sidebar');
          $('.ui-table').removeClass('ui-table-fixed-opened-sidebar');
        }
      });
    },1000);
    this.ListarReportes();
  }

  ListarReportes() {
    let usuarioRoles = this.userInfo.usuarioRoles.map(param => {return param.id});
    this.reporteQueryService.GetWithParametrosByRol(usuarioRoles).subscribe(async res => {
      let response = res as { status: string, message: string, data: Reporte_query[] };
      this.reportes = response.data;
      this.ids_diccionarios = [];
      this.reportes.forEach(reporte => {
        reporte.reporte_query_parametros.forEach(param => {
          if (param.id_diccionario) {
            this.ids_diccionarios.push(param.id_diccionario);
          }
          param.valor = '';
        });
      });

      this.GetAllParametrosByIdDiccionarios();
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  GenerarReporte(reporte: Reporte_query,pf1:any,pf2:any,pf3:any) {
    pf1.collapsed=true;
    pf2.collapsed=false;
    pf3.collapsed=true;
    this.objVal = {};
    reporte.reporte_query_parametros.forEach(param => {
      this.objVal[param.descripcion] = new FormControl('', [Validators.required]);
    });
    this.userform = this.fb.group(this.objVal);
    this.reporteSelect = reporte;
    this.userform.reset();
  }

  EjecutarQuery(reporte: Reporte_query,pf1:any,pf2:any,pf3:any,dt2:any) {
    dt2.reset();
    pf1.collapsed=true;
    pf2.collapsed=true;
    pf3.collapsed=false;
    this.resultado_query = [];
    this.btnEjecuta = true;
    this.solicitudService.isLoadingAgain = true;
    this.reporteQueryService.EjecutarQuery(reporte).subscribe(async res => {
      this.solicitudService.isLoadingAgain = false;
      this.btnEjecuta = false;
      let response = res as { status: string, message: string, data: any };
      this.resultado_query = response.data;
      if (this.resultado_query.length === 0) {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
      }
      this.cols2 = [];
      for (let atributo in this.resultado_query[0]) {
        this.cols2.push({ field: atributo, header: atributo, width: '20%' });
      }
      let width = $(window).width()*1/16 + 8;
      $('.reporte-resultados').find('.ui-table').css('width',`${width}em`);
      $(window).on('resize', () => {
          width = $(window).width()*1/16 + 8;
          $('.reporte-resultados').find('.ui-table').css('width',`${width}em`);
      });
    }, err => {
      this.btnEjecuta = false;
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  exportExcel(dt: any) {
    let name=this.reporteSelect.poliza.descripcion;
    
    this.reporteSelect.reporte_query_parametros.forEach(element=>{
      if(element.tipo==='date'){
        //@ts-ignore
        name=name+'@'+this.util.formatoFecha(element.valor.getFullYear()+'-'+(element.valor.getMonth()+1)+'-'+element.valor.getDate());
      }else{
        name=name+'@'+element.valor;
      }
    });
    let instan = null
    if (dt.filteredValue !== null && dt.filteredValue !== undefined) {
      instan = dt.filteredValue;
    } else {
      instan = this.resultado_query;
    }
    let instancia_poliza_excel = this.getCars(instan);
      const worksheet = xlsx.utils.json_to_sheet(instancia_poliza_excel);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, name);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName +'@'+ new Date().getTime() + EXCEL_EXTENSION);
  }

  getCars(ins_poli: any) {
    let instancias = [];
    for (let instancia of ins_poli) {
      instancias.push(instancia);
    }
    return instancias;
  }

  GetAllParametrosByIdDiccionarios() {
    //ids ponemos los id de los diccionarios que necesitemos
    if (this.ids_diccionarios.length > 0) {
      this.parametrosService.GetAllParametrosByIdDiccionarios(this.ids_diccionarios).subscribe(res => {
        let response = res as { status: string, message: string, data: Parametro[] };
        this.parametros = response.data;
        this.reportes.forEach(reporte => {
          reporte.reporte_query_parametros.forEach(reporte_query_parametro => {
            if (reporte_query_parametro.id_diccionario) {
              let SelItArray: SelectItem[] = [];
              SelItArray.push({ label: "Seleccione " + reporte_query_parametro.diccionario.diccionario_descripcion, value: '' });
              this.parametros.filter(param => param.diccionario_id + '' === reporte_query_parametro.id_diccionario + '').forEach(element => {
                SelItArray.push({ label: element.parametro_descripcion, value: element.id });
              });
              reporte_query_parametro.diccionarios = SelItArray;
            }
          });
        })
      }, err => {
        if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
        } else {
          console.log(err);
        }
      });
    }
  }

  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];
      let result = null;

      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

      return (event.order * result);
    });
  }

  CerrarAll(pf: any, pf2: any, pf3: any,nro:number) {

    if (pf.collapsed === false && nro === 1) {
      pf2.collapsed=true;
      pf3.collapsed=true;
    }
    if (pf2.collapsed === false && nro === 2) {
      pf.collapsed=true;
      pf3.collapsed=true;
    }
    if (pf3.collapsed === false && nro === 3) {
      pf.collapsed=true;
      pf2.collapsed=true;
    }

  }

}

