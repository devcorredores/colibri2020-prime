import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnexoPruebaComponent } from './anexo-prueba.component';

describe('AnexoPruebaComponent', () => {
  let component: AnexoPruebaComponent;
  let fixture: ComponentFixture<AnexoPruebaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnexoPruebaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnexoPruebaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
