import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoMedicPlusComponent } from './alta-eco-medic-plus.component';

describe('AltaEcoMedicPlusComponent', () => {
  let component: AltaEcoMedicPlusComponent;
  let fixture: ComponentFixture<AltaEcoMedicPlusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoMedicPlusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoMedicPlusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
