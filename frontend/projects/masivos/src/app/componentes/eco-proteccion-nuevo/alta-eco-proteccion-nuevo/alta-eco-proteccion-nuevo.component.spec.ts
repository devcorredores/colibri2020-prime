import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoProteccionNuevoComponent } from './alta-eco-proteccion-nuevo.component';

describe('AltaEcoProteccionNuevoComponent', () => {
  let component: AltaEcoProteccionNuevoComponent;
  let fixture: ComponentFixture<AltaEcoProteccionNuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoProteccionNuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoProteccionNuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
