import { Message } from 'primeng/api';
import { ConfirmationService, MessageService } from 'primeng';
import { RolesService } from '../../../../../../../src/core/servicios/rol.service';
import { Usuario } from '../../../../../../../src/core/modelos/usuario';
import { UsuariosService } from '../../../../../../../src/core/servicios/usuarios.service';
import { Rol } from '../../../../../../../src/core/modelos/rol';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-administracion-roles-perfiles',
  templateUrl: './administracion-roles-perfiles.component.html',
  styleUrls: ['./administracion-roles-perfiles.component.css'],
  providers: [ConfirmationService,MessageService]
})
export class AdministracionRolesPerfilesComponent implements OnInit {

  roles:Rol[]=[];
  rolesSelect:Rol[]=[];
  usuarios:Usuario[]=[];
  usuariosRegistrar:Usuario[]=[];
  filteredUsuariosMultiple: any[];
  msgs: Message[] = [];

  constructor(private rolesService:RolesService,private usuariosService:UsuariosService,private confirmationService: ConfirmationService,private service: MessageService) { }

  ngOnInit() {
    this.listarRoles();
  }

  listarRoles(){
    this.rolesService.obtenerRoles().subscribe(res => {
      let response = res as { status: string, message: string, data: Rol[] };
      this.roles=response.data;
    },
      err => console.error("ERROR llamando servicio informacion de Rol:", err.message));
  }

  filterCountryMultiple(event) {
    let usuario_login = event.query;    
    this.usuariosService.findAllUsuariosWithRol(usuario_login).subscribe(res => {
      let response = res as { status: string, message: string, data: Usuario[] };
      this.filteredUsuariosMultiple=response.data;
    },
      err => console.error("ERROR llamando servicio informacion de usuarios:", err.message));
    
  }

  filterCountry(query, countries: any[]):any[] {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered : any[] = [];
    for(let i = 0; i < countries.length; i++) {
        let country = countries[i];
        if (country.descripcion.toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(country);
        }
    }
    return filtered;
  }

  obtenerDatos(user:Usuario):string{
    let cadena='';
    cadena=user.usuario_login+` - email(${user.usuario_email}) `;
    user.usuarioRoles.forEach(rol=>{
      rol.des_aux='<a role="button" (click)="eliminarRol_x_Perfil()">ssshhhh</a>';
    });
    if(user.usuarioRoles.length>0){
      let descripciones = Array.from(user.usuarioRoles, rol => rol.descripcion+rol.des_aux);
      cadena=cadena+` - roles(${descripciones.join(',')})`;
    }
    cadena="<button (click)='new()'>ddddssssss</button>";
    return cadena;
  }

  emparejarUsuarioConRol(usuarios:Usuario[],roles:Rol[]){
     let usuario=new Usuario(); 
     usuarios.forEach(usuario=>{
      usuario.roles_adicionar= [];
        roles.forEach(rol=>{
          if(!usuario.usuarioRoles.find(userrol=>userrol.id===rol.id)){
            usuario.roles_adicionar.push(rol);
          }
        });
     });
  }

  GenerarMensaje(usuarios:Usuario[]):string{
      let mensaje:string;
      mensaje='<table>';
      let sw=0;
      usuarios.forEach(user=>{
        if(user.roles_adicionar.length>0){
          sw=1;
          user.roles_adicionar.forEach(rol=>{
            mensaje=mensaje+'<tr><td>'+user.usuario_login+'</td><td>=> '+rol.descripcion+'</td></tr>';
          });
        }
      });
      mensaje=mensaje+'</table>';
      if(sw===0){
        mensaje='No hay nada que emparejar';
      }
      if(sw===1){
        this.usuariosRegistrar=usuarios.filter(usuario=>usuario.roles_adicionar.length>0);
      }
      return mensaje;
  }

  RegistrarEmparejamiento(){
    this.rolesService.ResgistrarEmparejamiento(this.usuariosRegistrar).subscribe(res => {
      let response = res as { status: string, message: string, data: Rol[] };
      this.showSuccessViaToast();
      this.usuarios.forEach(user=>{
        if(this.usuariosRegistrar.find(elem=>elem.id===user.id)){
          this.usuarios.splice(this.usuarios.indexOf(user),1);
        }
      });
      this.usuariosRegistrar=[];
    },
      err => console.error("ERROR llamando servicio informacion de Rol:", err.message));
  }

  confirm() {
    this.emparejarUsuarioConRol(this.usuarios,this.rolesSelect);
    let mensaje=this.GenerarMensaje(this.usuarios);
    if(this.usuariosRegistrar.length>0){
      this.confirmationService.confirm({
          message: 'Desea emparejar a los siguientes usuarios con los siguienes roles?<br>'+mensaje,
          accept: () => {
            this.RegistrarEmparejamiento();
        }
      });
    }else{
      this.showWarnViaToast();
    }
  }

  showWarnViaToast() {
    this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No hay nada que emparejar' });
  }

  showSuccessViaToast() {
      this.service.add({ key: 'tst', severity: 'success', summary: 'Exitoso', detail: 'Se realizo el emparejamiento correctamente' });
  }

  eliminarRol_x_Perfil(id_usuario:string,id_rol:string,index_user:number,index_rol:number){
    console.log("ddkkdk");
    this.rolesService.eliminarRol_x_Perfil({id_usuario,id_rol}).subscribe(res => {
      let response = res as { status: string, message: string, data: Rol[] };
      this.service.add({ key: 'tst', severity: 'success', summary: 'Exitoso', detail: 'Se elimino el rol correctamente' });
      this.usuarios[index_user].usuarioRoles.splice(index_rol,1);
    },
      err => console.error("ERROR llamando servicio informacion de Rol:", err.message));
  }

  confimarEliminar(id_usuario:string,id_rol:string,index_user:number,index_rol:number){
    this.confirmationService.confirm({
      message: 'Estas seguro de eliminar este Rol para este Usuario?',
      accept: () => {
        this.eliminarRol_x_Perfil(id_usuario,id_rol,index_user,index_rol);
    }
  });
  }
}
