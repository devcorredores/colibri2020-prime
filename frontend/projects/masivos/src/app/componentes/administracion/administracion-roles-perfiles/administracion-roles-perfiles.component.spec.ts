import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracionRolesPerfilesComponent } from './administracion-roles-perfiles.component';

describe('AdministracionRolesPerfilesComponent', () => {
  let component: AdministracionRolesPerfilesComponent;
  let fixture: ComponentFixture<AdministracionRolesPerfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministracionRolesPerfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracionRolesPerfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
