import { Plan_pago } from '../../../../../../../src/core/modelos/plan_pago';
import { PlanPagoService } from '../../../../../../../src/core/servicios/plan-pago.service';
import { Diccionario } from '../../../../../../../src/core/modelos/diccionario';
import { Parametro } from '../../../../../../../src/core/modelos/parametro';
import { BreadcrumbService } from '../../../../../../../src/core/servicios/breadcrumb.service';
import { Componente } from '../../../../../../../src/core/modelos/componente';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms'
import { MessageService } from 'primeng/api';
import { ParametrosService } from '../../../../../../../src/core/servicios/parametro.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-parametros',
  templateUrl: './parametros.component.html',
  styleUrls: ['./parametros.component.css'],
  providers: [MessageService]
})
export class ParametrosComponent implements OnInit {

  ruta:string;
  componentesInvisibles: Componente[];
  diccionarios:Diccionario[]=[];
  parametros:Parametro[]=[];
  diccionarioSelect:Diccionario;
  diccionario:Diccionario=new Diccionario();
  parametro:Parametro=new Parametro();
  parametroSelect:Parametro=new Parametro();
  cols: any[];
  cols2: any[];
  diccionarioform: FormGroup;
  parametroform: FormGroup;
  nuevo: boolean = true;
  nuevoParametro: boolean = true;
  display: boolean = false;
  displayParametro:boolean = false;
  btnPP: boolean =true;
  numero_inicial=0;
  numero_final=0;
  numero_inicial2=0;
  numero_final2=0;

  ValidarComponentesInvisible(id:any){
    if(this.componentesInvisibles.find(params=>params.codigo===id && params.estado==='I')){
      return false;
    }else{
      return true;
    }
  }

  ValidarComponentesEditable(id:any){
    if(this.componentesInvisibles.find(params=>params.codigo===id && params.estado==='NE')){
      return true;
    }else{
      return false;
    }
  }

  constructor(
    private params: ActivatedRoute,private breadcrumbService: BreadcrumbService,
    private parametroService: ParametrosService,
    private messageService: MessageService,
    private planPagoService: PlanPagoService,
    private fb: FormBuilder
  ) {
    this.params.paramMap.subscribe(params => {
      this.componentesInvisibles = JSON.parse(params.get('ComponentesInvisibles'));
      this.ruta=params.get('ruta');
    });

    this.breadcrumbService.setItems([
      {label: this.ruta}
    ]);
  }

  ngOnInit() {
    this.cols = [
      { field: 'diccionario_codigo', header: 'CODIGO' },
      { field: 'diccionario_descripcion', header: 'DESCRIPCIÓN' }
    ];

    this.cols2 = [
      { field: 'parametro_cod', header: 'CODIGO' },
      { field: 'parametro_descripcion', header: 'DESCRIPCIÓN' },
      { field: 'parametro_abreviacion', header: 'ABREVIACIÓN' }
    ];

    this.diccionarioform = this.fb.group({
      'diccionario_codigo': new FormControl('', Validators.required),
      'diccionario_descripcion': new FormControl('', Validators.required)
    });

    this.parametroform = this.fb.group({
      'parametro_cod': new FormControl('', Validators.required),
      'parametro_descripcion': new FormControl('', Validators.required),
      'parametro_abreviacion': new FormControl('', Validators.required)
    });

    this.GetAllDiccionarios();
  }

  showDialog() {
    this.nuevo = true;
    this.diccionario = new Diccionario();
    this.display = true;
    this.diccionarioform.reset();
  }

  showDialogParametro() {
    this.nuevoParametro = true;
    this.parametro = new Parametro();
    this.displayParametro = true;
    this.parametroform.reset();
  }

  GetAllDiccionarios(){
    this.parametroService.GetAllDiccionarios().subscribe(res=>{
        let response = res as { status: string, message: string, data: Diccionario[] };
        this.diccionarios=response.data;
    });
  }

  onRowSelect(event) {
    this.diccionario = event.data as Diccionario;
    this.nuevo = false;
    this.display = true;
    this.parametroService.GetParametrosByIdDiccionario(this.diccionario.id).subscribe(res=>{
      let response = res as { status: string, message: string, data: Parametro[] };
      this.parametros=response.data;
  });
  }

  onRowSelectParametro(event) {
    this.parametro = event.data as Parametro;
    this.nuevoParametro = false;
    this.displayParametro = true;
  }

  async onSubmit() {
      let response: { status: string, message: string, data: Diccionario };
      if (this.nuevo) {
        this.parametroService.NewDiccionario(this.diccionario).subscribe(res => {
            response = res as { status: string, message: string, data: Diccionario };
           if(response.status==="OK"){ 
              this.messageService.add({ key: 'tst', sticky: true, severity: 'success', summary: 'Gestion de diccionario', detail: 'Registro de diccionario creado' });
              this.diccionarios.push(response.data);
              this.display = false;
            } else {
              this.messageService.add({ key: 'tst', sticky: true, severity: 'error', summary: 'Gestion de diccionario', detail: 'Error al crear diccionario' });
            }
        });
      } else {
        this.parametroService.UpdateDiccionario(this.diccionario).subscribe(res => {
          response = res as { status: string, message: string, data: any };
          if (response.status === 'OK') {
            this.messageService.add({ key: 'tst', sticky: true, severity: 'success', summary: 'Gestion de Diccionario', detail: 'Registro de diccionario editado' });
            this.display = false;
          } else {
            this.messageService.add({ key: 'tst', sticky: true, severity: 'error', summary: 'Gestion de Diccionario', detail: 'Error al editar diccionario' });
          }
        })
      }
      //this.diccionario = new Diccionario();
      this.nuevo = true;
  }

  async onSubmitParametro() {
    let response: { status: string, message: string, data: Parametro };
    if (this.nuevoParametro) {
      this.parametro.diccionario_id=this.diccionario.id;
      let parametro=this.parametro;
      this.parametroService.NewParametro(parametro).subscribe(res => {
          response = res as { status: string, message: string, data: Parametro };
         if(response.status==="OK"){ 
            this.messageService.add({ key: 'tst', sticky: true, severity: 'success', summary: 'Gestion de parametros', detail: 'Registro de parametro creado' });
            this.parametros.push(response.data);
            this.displayParametro = false;
          } else {
            this.messageService.add({ key: 'tst', sticky: true, severity: 'error', summary: 'Gestion de parametros', detail: 'Error al crear parametro' });
          }
      });
    } else {
      let parametro=this.parametro;
      this.parametroService.UpdateParamtro(parametro).subscribe(res => {
        response = res as { status: string, message: string, data: any };
        if (response.status === 'OK') {
          this.messageService.add({ key: 'tst', sticky: true, severity: 'success', summary: 'Gestion de Parametro', detail: 'Registro de Parametro editado' });
          this.displayParametro = false;
        } else {
          this.messageService.add({ key: 'tst', sticky: true, severity: 'error', summary: 'Gestion de Parametro', detail: 'Error al editar Parametro' });
        }
      })

    }
    //this.parametro = new Parametro();
    this.nuevoParametro = true;
}

delete(){
  let id=this.diccionario.id;
  this.parametroService.DeleteDiccionario(id).subscribe(res => {
     let response = res as { status: string, message: string, data: Parametro };
     if(response.status==="OK"){ 
        this.messageService.add({ key: 'tst', sticky: true, severity: 'success', summary: 'Gestion de Diccionario', detail: 'Registro de Diccionario eliminado' });
        this.removeItemFromArr(this.diccionarios,this.diccionario);
        this.display = false;
      } else {
        this.messageService.add({ key: 'tst', sticky: true, severity: 'error', summary: 'Gestion de Diccionario', detail: 'Error al eliminar Diccionario' });
      }
  });
}

deleteParametro(){
  let id=this.parametro.id;
  this.parametroService.DeleteParametro(id).subscribe(res => {
     let response = res as { status: string, message: string, data: Parametro };
     if(response.status==="OK"){ 
        this.messageService.add({ key: 'tst', sticky: true, severity: 'success', summary: 'Gestion de parametros', detail: 'Registro de parametro eliminado' });
        this.removeItemFromArr(this.parametros,this.parametro);
        this.displayParametro = false;
      } else {
        this.messageService.add({ key: 'tst', sticky: true, severity: 'error', summary: 'Gestion de parametros', detail: 'Error al eliminar parametro' });
      }
  });
}

removeItemFromArr ( arr:any, item:any ) {
  var i = arr.indexOf( item );

  if ( i !== -1 ) {
      arr.splice( i, 1 );
  }
}

async GeneraAllPlanPagos() {
  this.btnPP=false;
  await this.planPagoService.listaTodosSinPlanPagos().subscribe(async res => {
    let response = res as { status: string, message: string, data: any[] };
    this.numero_inicial=response.data.length;
    for (let i = 0; i < response.data.length; i++) {
      this.numero_final=i+1;
      let pp = new Plan_pago();
      pp.id_instancia_poliza = response.data[i].id;
      pp.total_prima = parseInt(response.data[i].monto)*parseInt(response.data[i].plazo);
      pp.interes = 0;
      pp.plazo_anos = 1;
      pp.periodicidad_anual = parseInt(response.data[i].plazo);
      pp.prepagable_postpagable = 1;
      pp.fecha_inicio = response.data[i].fecha_emision;
      pp.adicionado_por = '4';
      pp.modificado_por = '4';
      pp.id_moneda = 68;
      await this.planPagoService.GenerarPlanPagos2(pp).then(res => {
        let response = res as { status: string, message: string, data: any[] };
        if (response.status == 'ERROR') {

        } else {

        }
      }, err => console.error("ERROR llamando servicio plan de pagos:", err));
    }
  }, err => console.error("ERROR llamando servicio plan de pagos:", err));
}

async GeneraAllPlanPagosMigrados() {
  this.btnPP=false;
  await this.planPagoService.listaTodosSinPlanPagosMigrados().subscribe(async res => {
    let response = res as { status: string, message: string, data: any[] };
    this.numero_inicial2=response.data.length;
    for (let i = 0; i < response.data.length; i++) {
      this.numero_final2=i+1;
      let pp = new Plan_pago();
      pp.id_instancia_poliza = response.data[i].id;
      pp.total_prima = parseInt(response.data[i].monto)*parseInt(response.data[i].plazo);
      pp.interes = 0;
      pp.plazo_anos = 1;
      pp.periodicidad_anual = parseInt(response.data[i].plazo);
      pp.prepagable_postpagable = 1;
      pp.fecha_inicio =new Date(response.data[i].fecha_emision);
      var ultimoDia = new Date(pp.fecha_inicio.getFullYear(), pp.fecha_inicio.getMonth() + 1, 0);
      pp.fecha_inicio=ultimoDia;
      pp.adicionado_por = '4';
      pp.modificado_por = '4';
      pp.id_moneda = 68;
      await this.planPagoService.GenerarPlanPagos2(pp).then(res => {
        let response = res as { status: string, message: string, data: any[] };
        if (response.status == 'ERROR') {

        } else {

        }
      }, err => console.error("ERROR llamando servicio plan de pagos:", err));
    }
  }, err => console.error("ERROR llamando servicio plan de pagos:", err));
}

onReject(){

}

}
