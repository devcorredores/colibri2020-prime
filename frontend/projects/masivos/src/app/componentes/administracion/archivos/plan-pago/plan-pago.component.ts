import { ReporteService } from '../../../../../../../../src/core/servicios/reporte.service';
import { MenuService } from '../../../../../../../../src/core/servicios/menu.service';
import { SolicitudService } from '../../../../../../../../src/core/servicios/solicitud.service';
import { PlanPagoService } from '../../../../../../../../src/core/servicios/plan-pago.service';
import { Poliza } from '../../../../../../../../src/core/modelos/poliza';
import { SelectItem, MessageService } from 'primeng';
import { PolizaService } from '../../../../../../../../src/core/servicios/poliza.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {SessionStorageService} from "../../../../../../../../src/core/servicios/sessionStorage.service";

@Component({
  selector: 'app-plan-pago',
  templateUrl: './plan-pago.component.html',
  styleUrls: ['./plan-pago.component.css'],
  providers: [MessageService]
})
export class PlanPagoComponent implements OnInit {

  nro_certificado:string='';
  id_instancia_poliza:number;
  ListaPolizas: SelectItem[] = [];
  polizas: Poliza[] = [];
  cols: any[];
  milimizado: boolean=false;
  objeto:{nro_certificado:string,id_poliza:number}={nro_certificado:'',id_poliza:null};
  plan_pago:{estado_instancia:string,total_prima:number,moneda:string,interes:number,plazo_anos:number,periodicidad_anual:number,prepagable_postpagable:number,fecha_inicio:string,poliza:string,asegurado:string,nro_certificado:string,fecha_emision:string,fecha_inicio_vigencia:string,fecha_fin_vigencia:string,estado_plan:string,total_calcelado:number,total_pendiente:number,nro_cuotas_canceladas:number,nro_cuotas_pendientes:number}={estado_instancia:'',total_prima:0,moneda:'',interes:0,plazo_anos:0,periodicidad_anual:0,prepagable_postpagable:0,fecha_inicio:'',poliza:'',asegurado:'',nro_certificado:'',fecha_inicio_vigencia:'',fecha_fin_vigencia:'',estado_plan:'',fecha_emision:'',total_calcelado:0,total_pendiente:0,nro_cuotas_canceladas:0,nro_cuotas_pendientes:0};
  plan_pagos:{estado_instancia:string,nro_cuota_prog:number,fecha_couta_prog:Date,pago_cuota_prog:number,pago_prima_acumulado:number,prima_pendiente:number,FechaPagoEjec:Date,estado:string,estado_det:string,total_prima:number,moneda:string,interes:number,plazo_anos:number,periodicidad_anual:number,prepagable_postpagable:number,fecha_inicio:string,poliza:string,asegurado:string,nro_certificado:string,fecha_emision:string,fecha_inicio_vigencia:string,fecha_fin_vigencia:string,estado_plan:string}[]=[];
  certificados:{nro_documento:string,id:number,estado:string,fecha_inicio_vigencia:string,fecha_fin_vigencia:string,fecha_emision:string,RENOVADA:string}[]=[];
  vista:any;
  parametrosRuteo: any;
  select


  constructor(private polizaService: PolizaService,
    private router: Router,
    private planPagoService:PlanPagoService,
    private service: MessageService,
    public solicitudService: SolicitudService,
    private menuService:MenuService,
    private params: ActivatedRoute,
    private sessionStorageService: SessionStorageService,
    private reporteService:ReporteService) {
      this.vista = this.menuService.ObtenerParametrosVista(this.params.snapshot.paramMap.get('id'));
    }

  ngOnInit() {
    this.cols = [
      { field: 'nro_cuota_prog', header: 'Nro de cuota' },
      { field: 'fecha_couta_prog', header: 'Fecha Pago progamada' },
      { field: 'pago_cuota_prog', header: 'Monto  a pagar' },
      { field: 'fecha_limite', header: 'Fecha limite para pago' },
      { field: 'pago_prima_acumulado', header: 'Pago acumulado' },
      { field: 'prima_pendiente', header: 'Pago pendiente' },
      { field: 'FechaPagoEjec', header: 'Fecha cobro' },
      { field: 'estado', header: 'ESTADO' },
      { field: 'estado_det', header: 'OBSERVACIONES' },
      { field: '', header: '',width:'5%' }
    ];

    this.cargarPolizas();
    this.parametrosRuteo = this.sessionStorageService.getItemSync('parametros');
    if(this.parametrosRuteo.parametro_ruteo!==null){
        this.generarPlanPagosByCertAndIdinstancia(this.parametrosRuteo.parametro_ruteo);
        this.nro_certificado=this.parametrosRuteo.parametro_ruteo.nro_certificado;
        this.id_instancia_poliza=this.parametrosRuteo.parametro_ruteo.id_instancia_poliza;
    }   
  }

  cargarPolizas() {
    this.polizaService.listaPolizas().subscribe(res => {

      let response = res as { status: string, message: string, data: Poliza[] };
      this.polizas = response.data;

      this.ListaPolizas.push({ label: "Seleccione Poliza", value: null });
      this.polizas.forEach(element => {
        if(element.plan_pago){
          this.ListaPolizas.push({ label: element.descripcion, value: element.id });
        }
      });
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  listaAll(dt: any) {
    dt.reset();
    this.milimizado=false;
    this.solicitudService.isLoadingAgain = true;
    this.planPagoService.GetPlanPagoByNroCertAndIdPoliza(this.objeto).then(res => {
      let response = res as { status: string, message: string, data: any };
      this.plan_pagos = response.data;
      if (this.plan_pagos !== undefined) {
        this.solicitudService.isLoadingAgain = false;
        if (this.plan_pagos.length === 0) {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
        }else{
          this.milimizado=true;
          this.plan_pago.total_prima=this.plan_pagos[0].total_prima;
          this.plan_pago.interes=this.plan_pagos[0].interes;
          this.plan_pago.moneda=this.plan_pagos[0].moneda;
          this.plan_pago.periodicidad_anual=this.plan_pagos[0].periodicidad_anual;
          this.plan_pago.plazo_anos=this.plan_pagos[0].plazo_anos;
          this.plan_pago.prepagable_postpagable=this.plan_pagos[0].prepagable_postpagable;
          this.plan_pago.fecha_inicio=this.plan_pagos[0].fecha_inicio;
          this.plan_pago.poliza=this.plan_pagos[0].poliza;
          this.plan_pago.asegurado=this.plan_pagos[0].asegurado;
          this.plan_pago.fecha_inicio_vigencia=this.plan_pagos[0].fecha_inicio_vigencia;
          this.plan_pago.fecha_fin_vigencia=this.plan_pagos[0].fecha_fin_vigencia;
          this.plan_pago.nro_certificado=this.plan_pagos[0].nro_certificado;
          this.plan_pago.estado_plan=this.plan_pagos[0].estado_plan;
          this.plan_pago.fecha_emision=this.plan_pagos[0].fecha_emision;
          this.plan_pago.estado_instancia=this.plan_pagos[0].estado_instancia;
          let nro_pagados=0;
          let total_cancelado=0;
          this.plan_pagos.forEach(elemento=>{
            if(elemento.estado==='PAGADO'){
              nro_pagados++;
              total_cancelado=total_cancelado+elemento.pago_cuota_prog;
            }
          });
          this.plan_pago.total_calcelado=total_cancelado;
          this.plan_pago.nro_cuotas_canceladas=nro_pagados;
          this.plan_pago.total_pendiente=this.plan_pago.total_prima-this.plan_pago.total_calcelado;
          this.plan_pago.nro_cuotas_pendientes=this.plan_pago.periodicidad_anual-this.plan_pago.nro_cuotas_canceladas;
        }
      } else {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
      }
      this.solicitudService.isLoadingAgain = false;
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  selecionar(nro_certificado:any,id_instancia_poliza:any){
    this.nro_certificado=nro_certificado;
    this.id_instancia_poliza=id_instancia_poliza;
    this.generarPlanPagosByCertAndIdinstancia({nro_certificado,id_instancia_poliza});
  }
  
  listaCertificadosPorNroDocYIdPoliza(dt: any){
    this.milimizado=false;
    this.solicitudService.isLoadingAgain = true;
    this.planPagoService.listaCertificadosPorNroDocYIdPoliza(this.objeto.nro_certificado,this.objeto.id_poliza).then(res => {
      let response = res as { status: string, message: string, data: any };
      this.certificados = response.data;
      if (this.certificados !== undefined) {
        this.solicitudService.isLoadingAgain = false;
        if (this.certificados.length === 0) {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
        }else{
          if(this.certificados.length===1){
            this.listaAll(dt);
            this.nro_certificado=this.certificados[0].nro_documento;
            this.id_instancia_poliza=this.certificados[0].id;
          }
        }
      } else {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
      }
      this.solicitudService.isLoadingAgain = false;
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });    
  }

  generarPlanPagosByCertAndIdinstancia(objeto:any){
    this.milimizado=false;
    this.solicitudService.isLoadingAgain = true;
    this.planPagoService.GetPlanPagoByNroCertAndIdPolizaAndInstanciaPoliza(objeto).then(res => {
      let response = res as { status: string, message: string, data: any };
      this.plan_pagos = response.data;
      if (this.plan_pagos !== undefined) {
        this.solicitudService.isLoadingAgain = false;
        if (this.plan_pagos.length === 0) {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
        }else{
          this.milimizado=true;
          this.plan_pago.total_prima=this.plan_pagos[0].total_prima;
          this.plan_pago.interes=this.plan_pagos[0].interes;
          this.plan_pago.moneda=this.plan_pagos[0].moneda;
          this.plan_pago.periodicidad_anual=this.plan_pagos[0].periodicidad_anual;
          this.plan_pago.plazo_anos=this.plan_pagos[0].plazo_anos;
          this.plan_pago.prepagable_postpagable=this.plan_pagos[0].prepagable_postpagable;
          this.plan_pago.fecha_inicio=this.plan_pagos[0].fecha_inicio;
          this.plan_pago.poliza=this.plan_pagos[0].poliza;
          this.plan_pago.asegurado=this.plan_pagos[0].asegurado;
          this.plan_pago.fecha_inicio_vigencia=this.plan_pagos[0].fecha_inicio_vigencia;
          this.plan_pago.fecha_fin_vigencia=this.plan_pagos[0].fecha_fin_vigencia;
          this.plan_pago.nro_certificado=this.plan_pagos[0].nro_certificado;
          this.plan_pago.estado_plan=this.plan_pagos[0].estado_plan;
          this.plan_pago.fecha_emision=this.plan_pagos[0].fecha_emision;
          this.plan_pago.estado_instancia=this.plan_pagos[0].estado_instancia;
          let nro_pagados=0;
          let total_cancelado=0;
          this.plan_pagos.forEach(elemento=>{
            if(elemento.estado==='PAGADO'){
              nro_pagados++;
              total_cancelado=total_cancelado+elemento.pago_cuota_prog;
            }
          });
          this.plan_pago.total_calcelado=total_cancelado;
          this.plan_pago.nro_cuotas_canceladas=nro_pagados;
          this.plan_pago.total_pendiente=this.plan_pago.total_prima-this.plan_pago.total_calcelado;
          this.plan_pago.nro_cuotas_pendientes=this.plan_pago.periodicidad_anual-this.plan_pago.nro_cuotas_canceladas;
        }
      } else {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
      }
      this.solicitudService.isLoadingAgain = false;
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });    
  }

  beforeToggle(){
      if(this.parametrosRuteo.parametro_ruteo!==null){
        this.menuService.activaRuteoMenu(this.parametrosRuteo.parametro_ruteo.ruta_anterior,null,null);
      }    
  }

  imprimirPlanPagos(){
    let nombre_archivo = 'PlaPago' + this.id_instancia_poliza;
    this.reporteService.ReportePlanPago(this.nro_certificado,this.id_instancia_poliza, nombre_archivo).subscribe(res => {
      let response = res as { status: string, message: string, data: Poliza[] };
      if (response.status == 'ERROR') {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No pudo imprimir el documento, verifique los datos de la solicitud' });
      } else {
        this.reporteService.cargarPagina(nombre_archivo);
      }
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

}
