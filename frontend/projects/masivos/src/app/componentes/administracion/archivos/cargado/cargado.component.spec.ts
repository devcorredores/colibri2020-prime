import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargadoComponent } from './cargado.component';

describe('CargadoComponent', () => {
  let component: CargadoComponent;
  let fixture: ComponentFixture<CargadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
