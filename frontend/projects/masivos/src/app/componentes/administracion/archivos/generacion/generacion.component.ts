import { Poliza } from '../../../../../../../../src/core/modelos/poliza';
import { PolizaService } from '../../../../../../../../src/core/servicios/poliza.service';
import { SelectItem, MessageService } from 'primeng/api';
import { PlanPagoService } from '../../../../../../../../src/core/servicios/plan-pago.service';
import { Util } from '../../../../../../../../src/helpers/util';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-generacion',
  templateUrl: './generacion.component.html',
  styleUrls: ['./generacion.component.css']
})
export class GeneracionComponent implements OnInit {

  util = new Util();
  fecha: Date = new Date();
  id_poliza: number = 0;
  polizas: Poliza[] = [];
  currentPoliza: Poliza;
  ListaPolizas: SelectItem[] = [];
  cols: any[];
  interval:any;
  isLoadingAgain:boolean;
  timeLeft:number=0;
  displayConfirmacion: boolean=false;
  pagos: { num_carga: string, cod_agenda: string, cuenta: string, moneda: string, codigo_producto: string, nro_certificado: string, correlativo: string, glosa: string, fecha_envio: string, id_pago: number, fecha_couta_prog: string}[] = [];

  constructor(private planPagoService: PlanPagoService,
    private router: Router,
    private polizaService: PolizaService,
    private service: MessageService) { }


  ngOnInit() {
    this.cols = [
      { field: 'nro', header: 'Nro.' },
      { field: 'cliente', header: 'CLIENTE' },
      { field: 'cod_agenda', header: 'Cod Agenda' },
      { field: 'nro_certificado', header: 'Nro. Certificado' },
      { field: 'nro_cuota', header: 'Nro. Cuota' },
      { field: 'fecha', header: 'fecha_cuota' },
      { field: 'estado_cuota', header: 'ESTADO' },
      { field: 'monto', header: 'MONTO' },
    ];
    this.cargarPolizas();
  }

  cargarPolizas() {
    this.polizaService.listaPolizas().subscribe(res => {

      let response = res as { status: string, message: string, data: Poliza[] };
      this.polizas = response.data;

      this.ListaPolizas.push({ label: "Seleccione Poliza", value: null });
      this.polizas.forEach(element => {
        this.ListaPolizas.push({ label: element.descripcion, value: element.id });
      });
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }
  
  onSelectPoliza(event:any) {
      this.currentPoliza = this.polizas.find(param => param.id == event.value);
  }
  generarArchivo(dt:any) {
    dt.reset();
    this.planPagoService.GenerarPagosProcedimiento(this.fecha, this.id_poliza, this.currentPoliza.dias_habil_siguiente, this.currentPoliza.id_calendario).then(res => {
      let response = res as { status: string, message: string, data: any[] };
      this.pagos = response.data;
      this.pagos.map(param => param.fecha_couta_prog = moment(new Date(param.fecha_couta_prog)).format('DD/MM/YYYY'));
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  GenerarAndSendArchivo() {
    if (this.pagos.length > 0) {
      this.displayConfirmacion=true;
      /*this.confirmationService.confirm({
            message: 'Esta seguro de enviar esta información al banco ?',
            accept: () => {}
      });*/
    } else {
      this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'Nose tiene ningun resultado para enviar al banco' });
    }
  }

  GenerarAndSendArchivoConfirmado(){
    let ids = Array.from(this.pagos, pago => pago.id_pago);
    this.planPagoService.GenerarArchivo(this.pagos,this.id_poliza).then(res => {
      let response = res as { status: string, message: string, data: any[] };
      this.pagos = [];
      this.displayConfirmacion=false;
        this.isLoadingAgain = false;
      this.service.add({ key: 'tst', severity: 'success', summary: 'Correcto', detail: response.message });
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  Aceptar(){
      this.isLoadingAgain = true;
    this.GenerarAndSendArchivoConfirmado();
  }

  Cancelar(){
    this.displayConfirmacion=false;
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 60;
      }
    },1000)
  }
}
