import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlCobroComponent } from './control-cobro.component';

describe('ControlCobroComponent', () => {
  let component: ControlCobroComponent;
  let fixture: ComponentFixture<ControlCobroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlCobroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlCobroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
