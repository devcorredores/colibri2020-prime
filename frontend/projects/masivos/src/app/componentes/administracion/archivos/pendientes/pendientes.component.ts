import { Util } from '../../../../../../../../src/helpers/util';
import { Poliza } from '../../../../../../../../src/core/modelos/poliza';
import { SolicitudService } from '../../../../../../../../src/core/servicios/solicitud.service';
import { MessageService, SelectItem } from 'primeng';
import { PlanPagoService } from '../../../../../../../../src/core/servicios/plan-pago.service';
import { PolizaService } from '../../../../../../../../src/core/servicios/poliza.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as xlsx from 'xlsx';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-pendientes',
  templateUrl: './pendientes.component.html',
  styleUrls: ['./pendientes.component.css']
})
export class PendientesComponent implements OnInit {

  ListaPolizas: SelectItem[] = [];
  polizas: Poliza[] = [];
  milimizado: boolean = false;
  cols: any[];
  objeto: { dias_gracia: string, id_poliza: number, fecha: Date } = { dias_gracia: '', id_poliza: null, fecha: new Date };
  pendientes: { nro_documento: number, asegurado: string, cuenta: string, persona_celular: string, email: string, nro_cuota_prog: number, fecha_couta_prog: Date, motivo: string, dias_transcuridos: number, fecha_vencimiento: Date, sucursal: string, agencia: string, usuario_nombre_completo: string, usuario_login: string, fecha_emision: string }[] = [];
  util = new Util();

  constructor(
    private polizaService: PolizaService,
    private router: Router,
    private planPagoService: PlanPagoService,
    private service: MessageService,
    public solicitudService: SolicitudService
  ) { }

  ngOnInit() {
    this.cols = [
      { field: 'nro', header: 'Nro.', width: '5%' },
      { field: 'nro_documento', header: 'Certificado' },
      { field: 'fecha_emision', header: 'Fecha Emisión' },
      { field: 'asegurado', header: 'Asegurado' },
      /*{ field: 'cuenta', header: 'Cuenta' },
      { field: 'persona_celular', header: 'Nro. Celular' },
      { field: 'email', header: 'Correo electronico' },*/
      { field: 'nro_cuota_prog', header: 'Cuota en mora', width: '7%' },
      { field: 'fecha_couta_prog', header: 'Fecha Pago Cuota' },
      { field: 'motivo', header: 'Motivo de no cobro' },
      { field: 'dias_transcuridos', header: 'Dias mora' },
      { field: 'fecha_vencimiento', header: 'Fecha Caducidad' },
/*            { field: 'sucursal', header: 'Sucursal' },
            { field: 'agencia', header: 'Agencia' },*/
      { field: 'usuario_nombre_completo', header: 'Vendedor' },
      /*{ field: '', header: '',width:'5%' }*/
    ];
    this.cargarPolizas();

  }

  cargarPolizas() {
    this.polizaService.listaPolizas().subscribe(res => {

      let response = res as { status: string, message: string, data: Poliza[] };
      this.polizas = response.data;

      this.ListaPolizas.push({ label: "Seleccione Poliza", value: null });
      this.polizas.forEach(element => {
        if (element.plan_pago) {
          this.ListaPolizas.push({ label: element.descripcion, value: element.id });
        }
      });
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  listaAll(dt: any) {
    dt.reset();
    this.milimizado = false;
    if (this.objeto.dias_gracia !== null && this.objeto.dias_gracia !== undefined) {
      if ((this.objeto.dias_gracia + '').trim() === '') {
        this.objeto.dias_gracia = null;
      }
    }
    this.solicitudService.isLoadingAgain = true;
    this.planPagoService.GetPendientesByIdPolizaAndFechaAndDiasGracia(this.objeto).then(res => {
      let response = res as { status: string, message: string, data: any };
      this.pendientes = response.data;
      if (this.pendientes !== undefined) {
        this.solicitudService.isLoadingAgain = false;
        if (this.pendientes.length === 0) {
          this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
        } else {
          this.milimizado = true;
        }
      } else {
        this.service.add({ key: 'tst', severity: 'warn', summary: 'Advertencia', detail: 'No se encontro ningun registro' });
      }
      this.solicitudService.isLoadingAgain = false;
    }, err => {
      if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
        this.router.navigate(['login']);
      } else {
        console.log(err);
      }
    });
  }

  exportExcel(dt: any) {
    let instan = null
    if (dt.filteredValue !== null && dt.filteredValue !== undefined) {
      instan = dt.filteredValue;
    } else {
      instan = this.pendientes;
    }
    let instancia_poliza_excel = this.getCars(instan);
    const worksheet = xlsx.utils.json_to_sheet(instancia_poliza_excel);
    const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, "primengTable");
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  getCars(ins_poli: any) {
    let instancias = [];
    for (let instancia of ins_poli) {
      instancias.push(instancia);
    }
    return instancias;
  }

}
