import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoProteccionComponent } from './alta-eco-proteccion.component';

describe('AltaEcoProteccionComponent', () => {
  let component: AltaEcoProteccionComponent;
  let fixture: ComponentFixture<AltaEcoProteccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoProteccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoProteccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
