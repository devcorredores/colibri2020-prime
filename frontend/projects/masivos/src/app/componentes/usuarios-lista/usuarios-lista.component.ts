import { SelectItem } from 'primeng/api';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms'
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../../../../src/core/modelos/usuario';
import { Parametro } from '../../../../../../src/core/modelos/parametro';
import { Rol } from '../../../../../../src/core/modelos/rol';
import { MessageService } from 'primeng/api';
import { LoginService } from '../../../../../../src/core/servicios/login.service';
import { Usuariorol } from '../../../../../../src/core/modelos/usuariorol';
import { ActivatedRoute } from '@angular/router';
import { Componente } from '../../../../../../src/core/modelos/componente';
import { BreadcrumbService } from '../../../../../../src/core/servicios/breadcrumb.service';
import { ParametrosService } from '../../../../../../src/core/servicios/parametro.service';
import { RolesService } from '../../../../../../src/core/servicios/rol.service';
import { CorreoService } from '../../../../../../src/core/servicios/correo.service';
import { AutenticacionService } from '../../../../../../src/core/servicios/autenticacion.service';
import { UsuariosService } from '../../../../../../src/core/servicios/usuarios.service';
import { Router } from '@angular/router';
import {SessionStorageService} from "../../../../../../src/core/servicios/sessionStorage.service";

@Component({
  selector: 'app-usuarios-lista',
  templateUrl: './usuarios-lista.component.html',
  styleUrls: ['./usuarios-lista.component.css'],
  providers: [MessageService]
})

export class UsuariosListaComponent implements OnInit {
  userform: FormGroup;
  userformPwd: FormGroup;
  display: boolean = false;
  displayPwd: boolean = false;
  usuarios: Usuario[] = [];
  usuariorol: Usuariorol[] = [];
  roles: Rol[] = [];
  usuariosXContexto: Usuario[] = [];
  usuariosOri: Usuario[] = [];
  usuario = new Usuario();
  usuarioLogin = new Usuario();
  nuevo: boolean = true;
  msg: string;
  visto: boolean = false;
  parametrosEstado: Parametro[];
  parametrosContexto: Parametro[];
  parametrosLocal: Parametro[];
  parametrosRoles: Rol[];
  usuarioSel: Usuario;
  estadoPar: SelectItem[] = [];
  estadoParUsuario: any[] = [];
  contextoParUsuario: any[] = [];
  localParUsuario: any[] = [];
  contextoPar: SelectItem[] = [];
  localPar: SelectItem[] = [];
  rolPar: SelectItem[] = [];
  idRolPar: number;
  cols: any[];
  componentesInvisibles: Componente[];
  ruta: string;

  ValidarComponentes(id: any) {
    if (this.componentesInvisibles.find(params => params.codigo = id)) {
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private params: ActivatedRoute, private breadcrumbService: BreadcrumbService,
    private parametroService: ParametrosService,
    private rolesService: RolesService,
    private loginService: LoginService,
    private correoService: CorreoService,
    private autenticacionService: AutenticacionService,
    private usuariosService: UsuariosService,
    private sessionStorageService: SessionStorageService,
    private messageService: MessageService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.params.paramMap.subscribe(params => {
      this.componentesInvisibles = JSON.parse(params.get('ComponentesInvisibles'));
      this.ruta = params.get('ruta');
    });

    this.breadcrumbService.setItems([
      { label: this.ruta }
    ]);
  }

  ngOnInit() {

    this.usuarioLogin = this.sessionStorageService.getItemSync('userInfo') as Usuario;
    console.log('usuarioLogin', this.usuarioLogin);


    this.userform = this.fb.group({
      'username': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
      'nombres': new FormControl('', Validators.required),
      'email': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'estado': new FormControl('', Validators.required),
      'contexto': new FormControl('', Validators.required),
      'recoverypwd': new FormControl('')
    });

    this.userformPwd = this.fb.group({
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3),])),
      'password2': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)]))
    });

    this.getParametrosEstado();
    this.getParametrosContexto();
    this.getParametrosLocal();
    this.getUsuarios();

    this.cols = [
      { field: 'id', header: 'ID' },
      { field: 'usuario_login', header: 'Login' },
      { field: 'usuario_nombre_completo', header: 'Nombre completo' },
      { field: 'usuario_email', header: 'Email' },
      { field: 'par_estado_usuario_descripcion', header: 'Estado' },
      { field: 'par_aut_usuario_descripcion', header: 'Autorizacion' },
      { field: 'par_local_descripcion', header: 'Recuperar clave' }
    ];
  }

  getParamEstado(id: number) {
    let response: { status: string, message: string, data: Parametro[] };
    this.parametroService.obtenerParamEstado(id).subscribe(res => {
      response = res as { status: string, message: string, data: Parametro[] };
      return response.data[0].parametro_descripcion;
    },
      err => console.error("ERROR llamando servicio parametro:", err));
  }


  getParametrosRoles() {
    let response: { status: string, message: string, data: Rol[] };
    this.rolesService.obtenerRoles().subscribe(res => {
      response = res as { status: string, message: string, data: Rol[] };
      this.parametrosRoles = response.data;
      this.parametrosRoles.forEach(element =>
        this.rolPar.push({
          label: element.descripcion,
          value: element.id
        })
      );
    },
      err => console.error("ERROR llamando servicio parametro:", err));
  }

  getParametrosEstado() {
    let response: { status: string, message: string, data: Parametro[] };
    this.parametroService.obtenerParametros('5').subscribe(res => {
      response = res as { status: string, message: string, data: Parametro[] };
      this.parametrosEstado = response.data;
      this.parametrosEstado.forEach(element =>
        this.estadoPar.push({
          label: element.parametro_descripcion,
          value: element.id
        }),
      );
      this.parametrosEstado.forEach(element =>
        this.estadoParUsuario.push({
          id: element.id,
          descripcion: element.parametro_descripcion
        })
      );
    },
      err => console.error("ERROR llamando servicio parametro:", err));
  }

  getParametrosLocal() {
    let response: { status: string, message: string, data: Parametro[] };
    this.parametroService.obtenerParametros('13').subscribe(res => {
      response = res as { status: string, message: string, data: Parametro[] };
      this.parametrosLocal = response.data;
      this.parametrosLocal.forEach(element =>
        this.localPar.push({
          label: element.parametro_descripcion,
          value: element.id
        })
      );
      this.parametrosLocal.forEach(element =>
        this.localParUsuario.push({
          id: element.id,
          descripcion: element.parametro_descripcion
        })
      );
    },
      err => console.error("ERROR llamando servicio parametro:", err));
  }

  getRolByUser(idUsuario: number) {
    let response: { status: string, message: string, data: Parametro[] };
    this.rolesService.obtenerRolUsuario(idUsuario).subscribe(res => {
      response = res as { status: string, message: string, data: Parametro[] };
      this.parametrosEstado = response.data;
    });
  }

  getAutenticacionByUser(idUsuario: number) {
    let response: { status: string, message: string, data: Parametro[] };
    this.autenticacionService.obtenerAutenticacionUsuario(idUsuario).subscribe(res => {
      response = res as { status: string, message: string, data: Parametro[] };
      this.parametrosEstado = response.data;
    });
  }


  getParametrosContexto() {
    let response: { status: string, message: string, data: Parametro[] };
    this.parametroService.obtenerParametros('6').subscribe(res => {
      response = res as { status: string, message: string, data: Parametro[] };
      this.parametrosContexto = response.data;
      this.parametrosContexto.forEach(element =>
        this.contextoPar.push({
          label: element.parametro_descripcion,
          value: element.id
        })
      );
      this.parametrosContexto.forEach(element =>
        this.contextoParUsuario.push({
          id: element.id,
          descripcion: element.parametro_descripcion
        })
      );
    },
      err => console.error("ERROR llamando servicio parametro:", err));
  }

  delete() {
    this.messageService.clear();
    this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Estas seguro de eliminar?', detail: 'confirmar para proceder' });
  }

  onReject() {
    this.messageService.clear('c');
  }
  muestraModal() {
    this.visto = true
  }

  onRowSelect(event) {
    this.usuario = event.data as Usuario;
    this.nuevo = false
    this.display = true;
  }

  showDialog() {
    this.nuevo = true;
    this.usuario = new Usuario();
    this.display = true;
    this.userform.reset();
  }

  showEditarDialog() {
    this.nuevo = false;
    this.display = true;
  }

  getUsuarios() {
    let response: { status: string, message: string, data: Usuario[] };
    this.usuariosService.obtenerUsuarios().subscribe(res => {
      response = res as { status: string, message: string, data: Usuario[] };
      this.usuarios = response.data;
      this.usuarios.forEach(element => {
        if (this.estadoParUsuario.find(estado => estado.id === element.par_estado_usuario_id)) {
          element.par_estado_usuario_descripcion = this.estadoParUsuario.find(estado => estado.id === element.par_estado_usuario_id).descripcion;
        } else {
          element.par_estado_usuario_descripcion = '';
        }
      });

      this.usuarios.forEach(element => {
        if (this.contextoParUsuario.find(contexto => contexto.id === element.par_autenticacion_id)) {
          element.par_aut_usuario_descripcion = this.contextoParUsuario.find(contexto => contexto.id === element.par_autenticacion_id).descripcion;
        } else {
          element.par_aut_usuario_descripcion = '';
        }
      });

      this.usuarios.forEach(element => {
        if (this.localParUsuario.find(contexto => contexto.id === element.par_local_id)) {
          element.par_local_descripcion = this.localParUsuario.find(contexto => contexto.id === element.par_local_id).descripcion;
        } else {
          element.par_local_descripcion = '';
        }
      });

    },
      err => console.error("ERROR llamando servicio:", err));
  }

  getRolUsuario(idUsuario: number) {
    let response: { status: string, message: string, data: Usuariorol[] };
    this.usuariosService.obtenerUsuarios().subscribe(res => {
      response = res as { status: string, message: string, data: Usuariorol[] };
      this.usuariorol = response.data;
    },
      err => console.error("ERROR llamando servicio:", err));
  }

  getUsuariosXContexto() {
    let response: { status: string, message: string, data: Usuario[] };
    this.usuariosService.obtenerUsuarios().subscribe(res => {
      response = res as { status: string, message: string, data: Usuario[] };
      this.usuarios = response.data;
      this.usuariosOri = response.data;
    },
      err => console.error("ERROR llamando servicio:", err));
  }

  onConfirm() {
    this.display = false;
    this.messageService.clear('c');
    this.messageService.add({ severity: 'success', summary: 'Gestion usuarios', detail: 'Proceso exitoso' });
  }

  guardarUsuarioXRol(idRol: number, idUsuario: number) {
    let response = { status: '', message: '', data: '' };
    this.rolesService.nuevoUsuarioRol(idRol, idUsuario).subscribe(res => {
      response = res as { status: 'OK', message: string, data: any };
      if (response.message === 'UsuarioxRol creado') {
        this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Roles de Usuario', detail: 'Registro de rol usuario creado' });
        this.getUsuarios();
      } else {
        this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Roles de Usuario', detail: 'Error al crear rol usuario' });
      }
    });
  }

  guardarUsuarioXAutenticacion(idAuth: number, idUsuario: number) {
    let response = { status: '', message: '', data: '' };
    this.autenticacionService.nuevoUsuarioAutenticacion(idAuth, idUsuario).subscribe(res => {
      response = res as { status: 'OK', message: string, data: any };
      if (response.message === 'par_autenticacion_id creado') {
        this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Autenticacion de Usuario', detail: 'Registro de autenticacion usuario creado' });
        this.getUsuarios();
      } else {
        this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Autenticacion de Usuario', detail: 'Error al crear autenticacion usuario' });
      }
    });
  }

  // guardarCambio() {
  async onSubmit() {
    let response: { status: string, message: string, data: Usuario };
    const emailUsuario = this.usuario.usuario_email;
    if (this.usuario.usuario_login && this.usuario.usuario_nombre_completo) {
      if (this.nuevo) {
        let usuarioAux: any = {};
        usuarioAux = this.usuario;
        usuarioAux.pwd_change = true;
        usuarioAux.adicionado_por = this.usuarioLogin.usuario_login;
        this.display = false;
        this.usuariosService.nuevoUsuario(usuarioAux).subscribe(res => {
          response = res as { status: 'OK', message: string, data: any };
          switch (response.message) {
            case 'Registro y autenticacion creado':
              const mailOptions = {
                "para": emailUsuario,
                "asunto": "Notificacion Colibri-2020",
                "mensaje": `Señor: <br>Usuario Colibri2020 <br><br>Bienvenido a Corrredores de Seguro Ecofuturo, su usuario es <b> ${usuarioAux.usuario_login} </b> su clave actual es <b> ${usuarioAux.usuario_password} </b> la cual debe cambiar despues de ingresar la primera vez al sitio.`
              }
              this.correoService.enviarEmail(mailOptions).subscribe(resp => {
                response = resp as any;
              });
              this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Usuarios', detail: 'Registro de usuario creado' });
              this.getUsuarios();
              break;
            case 'Usuario duplicado':
              this.display = false;
              this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Usuarios', detail: 'Usuario ya se encuentra registrado en la base de datos' });
              break;
            case 'Email duplicado':
              this.display = false;
              this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Usuarios', detail: 'Email ya se encuentra registrado en la base de datos' });
              break;
            default:
              this.display = false;
              this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Usuarios', detail: 'No se creo el usuario, hubo algun error' });
              break;
          }
        });
      } else {
        this.display = false;
        let usuarioEditAux: any = {};
        usuarioEditAux = this.usuario;
        usuarioEditAux.modificado_por = this.usuarioLogin.usuario_login;
        if (this.usuario.par_autenticacion_id == 11) {
          usuarioEditAux.par_local_id = null;
        }
        this.usuariosService.modificarUsuario(usuarioEditAux).subscribe(res => {
          response = res as { status: string, message: string, data: any };
          if (response.status === 'OK') {
            this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Usuarios', detail: 'Registro de usuario editado' });
            this.getUsuarios();
          } else {
            this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Usuarios', detail: 'Error al editar usuario' });
          }
        })
      }
      this.usuario = new Usuario();
      this.nuevo = true;
    }
  }

  cerrarSesion() {
    localStorage.removeItem('userInfo');
    this.loginService.cerrarSesion().then(async (response) => {
      this.router.navigate(['']);
    });
  }

  editarUsuario() {
    let usuarioEditAux: any = {};
    usuarioEditAux = this.usuario;
    if (this.usuario.par_autenticacion_id == 11) {
      usuarioEditAux.par_local_id = null;
    }
    this.usuariosService.modificarUsuario(usuarioEditAux).subscribe(res => {
      let response = res as { status: string, message: string, data: any };
      if (response.status === 'OK') {
        this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Registro de usuario' });
        this.getUsuarios();
      } else {
        this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Error' });
      }
    })
  }

  // eliminarUsuario(id: number) {
  //   if (confirm(`Eliminar el usuario ${id}`)) {
  //     let response: { status: string, message: string, data: any };
  //     this.usuariosService.eliminarUsuario(id).subscribe(res => {
  //       response = res as { status: string, message: string, data: any };
  //       if (response.status === 'OK') {
  //         this.getUsuarios();
  //         this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Registro eliminado' });
  //         // this.toastr.success('Registro eliminado', 'Registro de usuario', { timeOut: 2000 });
  //       } else {
  //         this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Error' });
  //         // this.toastr.error(response.message, 'ERROR', { timeOut: 2000 });
  //       }
  //     });
  //   }
  // }

  cancelarCambio() {
    this.usuario = new Usuario();
    this.nuevo = true;
  }

  editReset(usuario: Usuario) {
    this.usuario = usuario;
  }

  resetPassword() {
    this.displayPwd = true;
  }

  cambiarPassword(pwd1: string, pwd2: string) {
    if (pwd1 === pwd2) {
      let response: { status: string, message: string, data: any };
      this.loginService.resetPassword(this.usuario.usuario_login, pwd1, pwd2, 1).subscribe(res => {
        response = res as { status: string, message: string, data: any };
        if (response.message === 'Contraseña actualizada.') {
          this.messageService.clear();
          this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Gestion de Contraseña', detail: 'Contraseña cambiada exitosamente' });
          var mailOptionsPwd = {
            "para": this.usuario.usuario_email,
            "asunto": "Cambio de Contraseña Colibri-2020",
            "mensaje": `Señor <br>${this.usuario.usuario_login}: <br><br> Mediante la presente informarle que se ha cambiado exitosamente la contraseña a <u><b>${pwd1}</b></u> segun requerimiento de ${this.usuario.usuario_email}.<br><br> La cual debe ser cambiada por usted la siguiente vez que ingrese a <a href='https://www.corredoresecofuturo.com:7000'> Colibri2020 </a>`
          }
          this.correoService.enviarEmail(mailOptionsPwd).subscribe(resp => {
            response = resp as any;
          });
          this.displayPwd = false;
        } else {
          this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Error' });
        }
      });
    } else {
      this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '', detail: 'Los passwords deben ser iguales' });
    }
  }
}
