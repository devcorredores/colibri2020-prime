import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoTarjetaComponent } from './alta-eco-tarjeta.component';

describe('AltaEcoTarjetaComponent', () => {
  let component: AltaEcoTarjetaComponent;
  let fixture: ComponentFixture<AltaEcoTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
