import {ChangeDetectorRef, Component, OnInit, ViewChild} from "@angular/core";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {DomSanitizer} from "@angular/platform-browser";
import {isArray, isObject, isString} from "util";
import {Location} from "@angular/common";
import "../../../../../../../src/helpers/prototypes";
import {Beneficiario} from "../../../../../../../src/core/modelos/beneficiario";
import {Parametro} from "../../../../../../../src/core/modelos/parametro";
import {Instancia_poliza_transicion} from "../../../../../../../src/core/modelos/instancia_poliza_transicion";
import {ParametrosService} from "../../../../../../../src/core/servicios/parametro.service";
import {MenuService} from "../../../../../../../src/core/servicios/menu.service";
import {PersonaService} from "../../../../../../../src/core/servicios/persona.service";
import {BeneficiarioService} from "../../../../../../../src/core/servicios/beneficiario.service";
import {SoapuiService} from "../../../../../../../src/core/servicios/soapui.service";
import {AtributoService} from "../../../../../../../src/core/servicios/atributo.service";
import {ObjetoAtributoService} from "../../../../../../../src/core/servicios/objetoAtributo.service";
import {DocumentoService} from "../../../../../../../src/core/servicios/documento.service";
import {PolizaService} from "../../../../../../../src/core/servicios/poliza.service";
import {AnexoService} from "../../../../../../../src/core/servicios/anexo.service";
import {InstanciaPolizaService} from "../../../../../../../src/core/servicios/instancia-poliza.service";
import {ReporteService} from "../../../../../../../src/core/servicios/reporte.service";
import {RolesService} from "../../../../../../../src/core/servicios/rol.service";
import {PlanPagoService} from "../../../../../../../src/core/servicios/plan-pago.service";
import {SessionStorageService} from "../../../../../../../src/core/servicios/sessionStorage.service";
import {SolicitudService} from "../../../../../../../src/core/servicios/solicitud.service";
import {AdministracionDePermisosService} from "../../../../../../../src/core/servicios/administracion-de-permisos.service";
import {BreadcrumbService} from "../../../../../../../src/core/servicios/breadcrumb.service";
import {persona_banco_datos} from "../../../../../../../src/core/modelos/persona_banco_datos";
import {persona_banco} from "../../../../../../../src/core/modelos/persona_banco";
import {persona_banco_account} from "../../../../../../../src/core/modelos/persona_banco_account";
import {Asegurado} from "../../../../../../../src/core/modelos/asegurado";
import {Persona} from "../../../../../../../src/core/modelos/persona";
import {Atributo} from "../../../../../../../src/core/modelos/atributo";
import {Poliza} from "../../../../../../../src/core/modelos/poliza";
import {
    persona_banco_tarjeta_debito_cuentas_vinculadas,
    TarjetaDebito
} from "../../../../../../../src/core/modelos/persona_banco_tarjeta_debito_cuentas_vinculadas";
import {Instancia_documento} from "../../../../../../../src/core/modelos/instancia_documento";
import {Atributo_instancia_poliza} from "../../../../../../../src/core/modelos/atributo_instancia_poliza";
import {Perfil_x_Componente} from "../../../../../../../src/core/modelos/componente";
import {Anexo_poliza} from "../../../../../../../src/core/modelos/anexo_poliza";
import {persona_banco_tarjeta_debito} from "../../../../../../../src/core/modelos/persona_banco_tarjeta_debito";
import {Plan_pago} from "../../../../../../../src/core/modelos/plan_pago";
import {MessageService, SelectItem} from "primeng";
import {Instancia_poliza} from "../../../../../../../src/core/modelos/instancia_poliza";
import {Usuario_x_rol} from "../../../../../../../src/core/modelos/usuario_x_rol";
import {persona_banco_tarjeta_credito} from "../../../../../../../src/core/modelos/persona_banco_tarjeta_credito";
import {TransicionesComponent} from "../../../../../../../src/core/componentes/transiciones/transiciones.component";
import {BeneficiarioComponent} from "../../../../../../../src/core/componentes/beneficiario/beneficiario.component";
import {Util} from "../../../../../../../src/helpers/util";
const util = new Util();

@Component({
    selector: "app-alta-eco-tarjeta",
    templateUrl: "./alta-eco-tarjeta.component.html",
    styleUrls: ["./alta-eco-tarjeta.component.css"],
    providers: [MessageService],
})
export class AltaEcoTarjetaComponent implements OnInit {
    @ViewChild("componentTransiciones", {static: false}) private transicionesComponent: TransicionesComponent;
    @ViewChild("componenteBeneficiario", {static: false}) private componenteBeneficiario: BeneficiarioComponent;
    color = "primary";
    mode = "indeterminate";
    value = 1;
    show = false;
    yesterday: Date = new Date();

    displayTarjetaInvalidaContinuar: boolean = false;
    displayBeneficiariosExcedidos: boolean = false;
    displayBeneficiariosTitular: boolean = false;
    displayBeneficiariosNotFound: boolean = false;
    displayNuevoClienteSinNroCuenta: boolean = false;
    displaySinTarjetaDebito: boolean = false;
    displayBeneficiarioFound: boolean = false;
    collapsedFormTitular: boolean = false;
    collapsedDatosTitular: boolean = false;
    displayErrorPlanPago: boolean = false;
    displayClienteNoExiste: boolean = false;
    displaySolicitudDuplicado: boolean = false;
    displayActualizacionExitoso: boolean = false;
    displayIntroduscaNroTarjeta: boolean = false;
    displayNroTarjetaSinCuentas: boolean = false;
    displayNroCuentasActualizados: boolean = false;
    displayModalFormBusqueda: boolean = false;
    beneficiario: Persona = new Persona();
    Beneficiarios: Persona[] = [];
    BeneficiariosAux: Beneficiario[] = [];
    benAux: Beneficiario;
    id_beneficiarioAux;
    parentescoAux: Parametro;
    numeroBeneficiarios = 2;
    docId: string = null;
    minLengthMsg: string = "";
    minlength: number = 0;
    docIdExt: number = null;
    display: boolean = false;
    displayBeneficiario: boolean = false;
    displayBeneficiarioDouble: boolean = false;
    displayBeneficiariosCambio: boolean = false;
    displayNuevoCliente: boolean = false;
    displayEliminarBeneficiario: boolean = false;
    displayEliminarBeneficiarioOK: boolean = false;
    cuentas: any[] = [];

    parametroMoneda: Parametro;
    MonedasParametroDescripcion: any[] = [];
    userform: FormGroup;
    fechaEmisionForm: FormGroup;
    formBusqueda: FormGroup;
    userform2: FormGroup;
    userform3: FormGroup;
    responsiveOptions: any;
    atributo: Atributo[];
    atributo_instancia_poliza_selected: any[] = [];
    cols: any[];
    doc_id: number = 0;
    ext: number = 0;
    events: Subscription[] = [];
    messages: string[] = [];
    maxLengthTarjeta: number = 1;
    guardandoTitular: boolean = false;
    editandoBeneficiario: boolean = false;
    fromSearch: boolean = false;
    id_instancia_poliza: number;
    id_estado: string;
    id_asegurado: string;
    totalPorcentaje = 0;
    persona: Persona;
    numero_solicitudes: number;
    showErrors: boolean = false;
    displayValidacionAlInicio: boolean = false;
    messaje: string = "";
    withErrorsOrWarnings: boolean = false;
    oldInstanciaPolizaTransiciones: Instancia_poliza_transicion[] = [];
    estadoCaja: Parametro;
    parametroTipoTarPrincipal: Parametro;
    parametroTipoTarAdicional: Parametro;
    polizaEcoTarjeta: Poliza;
    btnTarjetaDebitoCuentasEnabled = true;
    showValidarYContinuar = true;
    showEmitirCertificado = true;
    validarTarjeta: Boolean = false;
    reloadBeneficiarioComponent = true;
    titularOPersona: string = "La persona ";
    idObjectCurrent: number = 0;
    apellidos: string[] = [];
    stopSaving: boolean = false;
    diasGracia: number;
    stopSending: boolean = false;
    msgAlertRenew: string = "";
    msgRenew: string = "";
    lblButtonShowRequests: string = "";
    emitido: boolean = true;
    displayErrorDiasGracia: boolean = false;
    displayErrorRenovacion: boolean = false;
    displayErrorSinCertificado: boolean = false;
    filtros: any;
    idsPerfiles: number[];
    aseguradoWithDiferentDocIdExt: Asegurado;


    constructor(
        private params: ActivatedRoute,
        private breadcrumbService: BreadcrumbService,
        private changeDetection: ChangeDetectorRef,
        private parametroService: ParametrosService,
        private fb: FormBuilder,
        private menuService: MenuService,
        private personaService: PersonaService,
        private beneficiarioService: BeneficiarioService,
        private soapuiService: SoapuiService,
        private parametrosService: ParametrosService,
        private atributoService: AtributoService,
        private objetoAtributoService: ObjetoAtributoService,
        private documentoService: DocumentoService,
        private polizaService: PolizaService,
        private anexoService: AnexoService,
        private router: Router,
        private instanciaPolizaService: InstanciaPolizaService,
        private messageService: MessageService,
        private sanitizer: DomSanitizer,
        private reporteService: ReporteService,
        private rolesService: RolesService,
        private planPagoService: PlanPagoService,
        private _location: Location,
        private sessionStorageService: SessionStorageService,
        public solicitudService: SolicitudService,
        private adminPermisosService: AdministracionDePermisosService
    ) {
    }

    ngOnInit() {
        this.solicitudService.product = this.solicitudService.ruta;
        this.solicitudService.persona_banco_datos = new persona_banco_datos();
        this.solicitudService.persona_banco = new persona_banco();
        this.solicitudService.persona_banco_account = new persona_banco_account();
        this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada = new persona_banco_tarjeta_debito_cuentas_vinculadas();
        this.solicitudService.persona_banco_tarjeta_debito_cuentas_vinculadas = [];
        this.solicitudService.persona_banco_tarjetas_debito = [];
        this.solicitudService.tarjetasDebitos = [];
        this.solicitudService.persona_banco_datos = new persona_banco_datos();
        this.solicitudService.persona_banco_cuentas_vinculadas = [];
        this.solicitudService.isInAltaSolicitud = true;
        this.solicitudService.paramsLoaded = false;
        this.solicitudService.isLoadingAgain = true;
        this.solicitudService.displayBusquedaCI = true;
        this.solicitudService.paramsLoaded = false;
        this.solicitudService.displayModalDatosTitular = false;
        this.solicitudService.constructComponent(async () => {
            this.getPoliza(async () => {
                this.setUserForm();
                if (
                    !this.solicitudService.paramsDeleted &&
                    this.solicitudService.parametrosRuteo.parametro_ruteo !== null &&
                    this.solicitudService.parametrosRuteo.parametro_ruteo !== 'undefined' &&
                    this.solicitudService.parametrosRuteo.parametro_ruteo !== '{}' &&
                    Object.keys(this.solicitudService.parametrosRuteo.parametro_ruteo).length
                ) {
                    //this.solicitudService.isLoadingAgain = true;
                    this.solicitudService.editandoTitular = false;
                    this.solicitudService.displayBusquedaCI = false;
                    this.solicitudService.displayModalFormTitular = false;
                    this.id_instancia_poliza = this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza ? this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza : null;
                    // this.solicitudService.iniciarSolicitud();
                    this.solicitudService.product = this.solicitudService.poliza.descripcion;
                    this.personaService.findPersonaSolicitudByIdInstanciaPoliza(this.solicitudService.idObjetoCurrent, this.id_instancia_poliza).subscribe(async res => {
                        let resp = res as { status: string, message: string, data: Asegurado };
                        if (resp.data && Object.keys(resp.data).length) {
                            this.solicitudService.asegurado = resp.data;
                            this.solicitudService.isLoadingAgain = false;
                            await this.solicitudService.setDatesOfAsegurado();
                            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
                            this.solicitudService.setAtributosToPersonaBanco(this.userform, async () => {
                                this.solicitudService.setAtributosToAsegurado();
                                if (this.solicitudService.id_poliza == 4) {
                                    if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                                        await this.initInstanciaPoliza(async () => {
                                            await this.getDatosFromCuentasVinculadas();
                                        });
                                    } else {
                                        await this.initInstanciaPoliza()
                                    }
                                } else {
                                    await this.initInstanciaPoliza();
                                }
                            });
                        } else {
                            this.solicitudService.isLoadingAgain = false;
                            this.displayClienteNoExiste = true;
                            this.stateButtonsFlow();
                        }
                    }, err => {
                        if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                            this.router.navigate(['login'])
                        } else {
                            console.log(err);
                        }
                    });
                } else {
                    if (this.solicitudService.parametrosRuteo.openForm) {
                    } else {
                        this.solicitudService.isLoadingAgain = false;
                        this.solicitudService.displayModalFormTitular = false;
                        this.solicitudService.displayBusquedaCI = true;
                        this.cancelarSolicitud()
                    }
                }
            });
        });
    }

    async setUserForm() {
        if (this.solicitudService.id_poliza == 3) {
            this.idsPerfiles = [5, 9];
        } else if (this.solicitudService.id_poliza == 4) {
            this.idsPerfiles = [6, 10];
        } else if (this.solicitudService.id_poliza == 13) {
            this.idsPerfiles = [12, 38];
        }
        this.solicitudService.setCurrentPerfil(this.idsPerfiles);
        this.adminPermisosService.getComponentes(this.solicitudService.parametrosRuteo.id_vista, this.solicitudService.currentPerfil.id).subscribe(async (res) => {
            let response = res as { status: string; message: string; data: Perfil_x_Componente[]; };
            this.solicitudService.isLoadingAgain = false;
            this.solicitudService.componentesInvisibles = response.data;
            await this.solicitudService.componentsBehavior(res => {
                if ([3, 13].includes(parseInt(this.solicitudService.id_poliza + ''))) {
                    this.minlength = 16;
                } else if (this.solicitudService.id_poliza == 4) {
                    this.minlength = 9;
                }
                this.solicitudService.minlengthCelular = 8;
                this.minLengthMsg = "Debe contener " + this.minlength + " digitos";
                this.solicitudService.minLengthMsgCelular = "Debe contener " + this.solicitudService.minlengthCelular + " digitos";
                this.formBusqueda = this.fb.group({
                    'persona_doc_id': new FormControl('', [Validators.required]),
                    'persona_doc_id_ext': new FormControl('', Validators.required),
                });
                this.fechaEmisionForm = this.fb.group({
                    'fecha_registro': new FormControl('', Validators.required),
                    'nro_transaccion': new FormControl('', Validators.required)
                });
                this.userform = this.fb.group({
                    'persona_doc_id': new FormControl('', [Validators.required]),
                    'persona_doc_id_ext': new FormControl('', Validators.required),
                    'persona_doc_id_comp': new FormControl(''),
                    'persona_primer_apellido': new FormControl('', this.solicitudService.showPrimerApellido ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    'persona_segundo_apellido': new FormControl('', this.solicitudService.showSegundoApellido ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    'persona_primer_nombre': new FormControl('', this.solicitudService.showPrimerNombre ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    'persona_segundo_nombre': new FormControl(''),
                    'persona_apellido_casada': new FormControl('', this.solicitudService.showApCasada ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    'persona_direccion_domicilio': new FormControl(''),
                    'persona_direcion_trabajo': new FormControl(''),
                    'persona_fecha_nacimiento': new FormControl('', this.solicitudService.showFechaNac ? [Validators.required] : []),
                    // 'persona_celular': new FormControl(''),
                    'persona_telefono_domicilio': new FormControl(''),
                    'persona_telefono_trabajo': new FormControl(''),
                    'persona_telefono_celular': new FormControl('', this.solicitudService.showTelefonoCelular ? [Validators.minLength(8), Validators.maxLength(8), Validators.pattern("^[0-9]*$")] : []),
                    'persona_profesion': new FormControl(''),
                    'par_tipo_documento_id': new FormControl('', this.solicitudService.showTipoDocumento ? [Validators.required] : []),
                    'par_nacionalidad_id': new FormControl(4),
                    'par_pais_nacimiento_id': new FormControl('', this.solicitudService.showPaisNacimiento ? [Validators.required] : []),
                    'par_ciudad_nacimiento_id': new FormControl('', this.solicitudService.showCiudadNacimiento ? [] : []),
                    'par_sexo_id': new FormControl('', this.solicitudService.showSexoId ? [Validators.required] : []),
                    'persona_origen': new FormControl(''),
                    'par_numero_cuenta': new FormControl('', this.solicitudService.showNroCuenta || this.solicitudService.poliza.id == 4 ? [Validators.required] : []),
                    'par_cuenta_expiracion': new FormControl('', this.solicitudService.showCuentaFechaExpiracion ? [] : []),
                    'par_tar_fecha_activacion': new FormControl('', this.solicitudService.showFechaActivacion ? [] : []),
                    'par_ocupacion': new FormControl('', this.solicitudService.showOcupacion ? [] : []),
                    // 'par_plan': new FormControl('', this.solicitudService.showPlan ? [Validators.required] : []),
                    // 'par_plazo': new FormControl('', this.solicitudService.showPlazo ? [Validators.required, Validators.min(1)] : []),
                    'par_codigo_agenda_id': new FormControl(''),
                    'par_estado_civil_id': new FormControl(''),
                    'par_mail_id': new FormControl('', this.solicitudService.showEmail ? [] : []),
                    'par_caedec_id': new FormControl(''),
                    'par_localidad_id': new FormControl(''),
                    'par_departamento_id': new FormControl(''),
                    'par_sucursal': new FormControl('', this.solicitudService.showSucursal ? [] : []),
                    'par_agencia': new FormControl('', this.solicitudService.showAgencia ? [] : []),
                    'par_prima': new FormControl('', this.solicitudService.showPrima ? [Validators.required] : []),
                    'par_nro_sci': new FormControl(''),
                    'par_modalidad_pago': new FormControl(this.solicitudService.showModalidadPago ? [Validators.required] : []),
                    'par_zona': new FormControl(''),
                    'par_nro_direccion': new FormControl(''),
                    'par_razon_social': new FormControl(''),
                    'par_nit_carnet': new FormControl(''),
                    'par_tar_fin_vigencia': new FormControl(''),
                    'par_tarjeta_nro': new FormControl(this.solicitudService.showTarjetaNro ? [Validators.required] : []),
                    'par_tipo_tarjeta': new FormControl(this.solicitudService.showTipoTarjeta ? [Validators.required] : []),
                    'par_estado_tarjeta': new FormControl(this.solicitudService.showEstadoTarjeta ? [Validators.required] : []),
                    'par_tarjeta_nombre': new FormControl(this.solicitudService.showTarjetaNombre ? [Validators.required] : []),
                    'par_moneda': new FormControl('', this.solicitudService.showMoneda && this.solicitudService.id_poliza == 4 ? [Validators.required] : []),
                    'par_debito_automatico_id': new FormControl('', this.solicitudService.showPagoEfectivo && this.solicitudService.showMoneda && this.solicitudService.id_poliza == 4 ? [Validators.required] : []),
                    'par_forma_pago': new FormControl('', this.solicitudService.showFormaPago && this.solicitudService.showMoneda && this.solicitudService.id_poliza == 4 ? [Validators.required] : []),
                    'par_nro_cuotas': new FormControl('', this.solicitudService.showNroCuotas ? [] : []),
                });
                this.solicitudService.getFormValidationErrors(this.userform);
                this.solicitudService.showFormValidation(this.userform);
                this.solicitudService.setFeatureValidacionAlInicioPersonaBanco();
            });
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login'])
            } else {
                console.log(err);
            }
        });
    }

    async initInstanciaPoliza(callback: any = null) {
        this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
        await this.solicitudService.setAtributosToPersonaBanco(this.userform, async res => {
            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach((instancia_documento: Instancia_documento) => {
                if (instancia_documento.id_documento == this.solicitudService.documentoSolicitud.id) {
                    this.solicitudService.instanciaDocumentoSolicitud = instancia_documento;
                } else if (instancia_documento.id_documento == this.solicitudService.documentoComprobante.id) {
                    this.solicitudService.instanciaDocumentoComprobante = instancia_documento;
                } else if (instancia_documento.id_documento == this.solicitudService.documentoCertificado.id) {
                    this.solicitudService.instanciaDocumentoCertificado = instancia_documento;
                }
            });

            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach((instancia_documento: Instancia_documento) => {
                instancia_documento.atributo_instancia_documentos_inter = [];
                // TODO: Iterar instancia_documento.atributo_instancia_documentos
            });

            this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter = [];
            this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.forEach((atributo_instancia_poliza: Atributo_instancia_poliza) => {
                if (atributo_instancia_poliza.objeto_x_atributo.par_comportamiento_interfaz_id == this.solicitudService.parametroVisible.id) {
                    this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter.push(atributo_instancia_poliza);
                }
            });
            await this.solicitudService.setAtributosToPersonaBanco(this.userform).then(async res => {
                await this.stateButtonsFlow();
            });
            await this.solicitudService.setBeneficiarios();
            this.solicitudService.displayModalDatosTitular = true;
            this.solicitudService.displayDatosTitular = true;
            //this.solicitudService.isLoadingAgain = false;
            if (this.componenteBeneficiario != undefined) {
                this.componenteBeneficiario.ngOnInit();
            }
            if (this.transicionesComponent != undefined) {
                this.transicionesComponent.ngOnInit();
            }
            this.stateButtonsFlow();
            if(typeof callback == 'function') {
                await callback();
            }
        });
    }

    async componentsBehaviorOnInit(callback: Function = null) {
        this.solicitudService.showTarjetaNombre = false;
        this.solicitudService.showTarjetaValida = false;
        this.solicitudService.showModalidadPago = true;
        this.solicitudService.showPagoEfectivo = false;
        this.solicitudService.showRazonSocial = false;
        this.solicitudService.showTarjetaNro = false;
        this.solicitudService.showTipoTarjeta = false;
        this.solicitudService.showAgencia = false;
        this.solicitudService.showUsuarioCargo = false;
        this.solicitudService.showSucursal = false;
        this.solicitudService.showNitCarnet = false;
        this.solicitudService.showTarjetaUltimosCuatroDigitos = false;
        this.solicitudService.showNroCuenta = false;
        this.solicitudService.showCuentaFechaExpiracion = false;
        this.solicitudService.showMoneda = false;
        this.solicitudService.showCaedec = true;
        this.solicitudService.showLocalidad = true;
        this.solicitudService.showDepartamento = true;
        this.solicitudService.showCodSucursal = true;
        this.solicitudService.showTipoDocumento = true;
        this.solicitudService.showEmail = true;
        this.solicitudService.showEstadoCivil = true;
        this.solicitudService.showManejo = true;
        this.solicitudService.showCodAgenda = true;
        this.solicitudService.showDescCaedec = true;
        this.solicitudService.showZona = false;
        this.solicitudService.showNroDireccion = false;
        if (typeof callback == "function") {
            callback();
        }
    }

    validateForm() {
        console.log(this.solicitudService.getFormValidationErrors(this.userform));
    }

    async setAnexoPoliza(callback: any = null) {
        await this.anexoService.findAnexoPoliza(this.solicitudService.poliza.id).subscribe(async (res) => {
                let response = res as { status: string; message: string; data: Anexo_poliza[]; };
                this.solicitudService.anexosPoliza = response.data;
                this.solicitudService.anexoPoliza = this.solicitudService.anexosPoliza.find(param => param.id_tipo == 288);
                this.solicitudService.anexosPoliza.forEach((anexoPoliza: Anexo_poliza) => {
                        this.solicitudService.Planes.push({label: anexoPoliza.descripcion, value: anexoPoliza.id,});
                        this.solicitudService.planes.push(anexoPoliza);
                        this.solicitudService.PlanesParametroCod[anexoPoliza.id + ''] = anexoPoliza.descripcion;
                        this.solicitudService.anexoAsegurado = anexoPoliza.anexo_asegurados.find((param) => param.id_tipo == this.solicitudService.parametroAsegurado.id);
                        if (this.solicitudService.anexoAsegurado && this.solicitudService.asegurado) {
                            this.solicitudService.isRenovated = this.solicitudService.asegurado.instancia_poliza.id == this.solicitudService.asegurado.instancia_poliza.id_instancia_renovada ? false : true;
                            switch (this.solicitudService.anexoAsegurado.id_edad_unidad) {
                                case this.solicitudService.parametroYear.id:
                                    this.solicitudService.edadMinimaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                                case this.solicitudService.parametroMonth.id:
                                    this.solicitudService.edadMinimaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                                case this.solicitudService.parametroDay.id:
                                    this.solicitudService.edadMinimaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                            }
                        }
                    }
                );
                if (typeof callback == 'function') {
                    await callback();
                }
            },
            (err) => {
                if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                    this.router.navigate([""]);
                } else {
                    console.log(err);
                }
            }
        );
    }

    cambiarSolicitudEstado(callback: any = null) {
        this.solicitudService.displayValidacionSinObservaciones = false;
        if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
            this.solicitudService.instanciaDocumentoComprobante = new Instancia_documento();
            this.solicitudService.instanciaDocumentoComprobante.documento = this.solicitudService.documentoComprobante;
            if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                this.solicitudService.instanciaDocumentoComprobante.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
            }
            if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(param => param.id_documento == this.solicitudService.instanciaDocumentoComprobante.id_documento)) {
                this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoComprobante);
            }
            this.solicitudService.emitirInstanciaPoliza = false;
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
            if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionNo.id + '') {
                this.solicitudService.instanciaDocumentoCertificado = new Instancia_documento();
                if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                    this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
                }
                this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
                this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
                this.solicitudService.emitirInstanciaPoliza = true;
            } else {
                this.solicitudService.emitirInstanciaPoliza = false;
            }
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
            let transicionSolicitado = new Instancia_poliza_transicion();
            transicionSolicitado.observacion = this.solicitudService.msgText;
            transicionSolicitado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            transicionSolicitado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            transicionSolicitado.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionSolicitado);
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
            let instanciaDocumentoCertificado;
            if (this.solicitudService.asegurado.instancia_poliza.instancia_documentos && this.solicitudService.asegurado.instancia_poliza.instancia_documentos.length) {
                instanciaDocumentoCertificado = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento + "" == this.solicitudService.documentoCertificado.id + "");
            }
            if (instanciaDocumentoCertificado) {
                this.solicitudService.instanciaDocumentoCertificado = instanciaDocumentoCertificado;
            } else {
                this.solicitudService.instanciaDocumentoCertificado = new Instancia_documento();
                this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
            }
            this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
            this.solicitudService.emitirInstanciaPoliza = true;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
            let transicionSolicitado = new Instancia_poliza_transicion();
            transicionSolicitado.observacion = this.solicitudService.msgText;
            transicionSolicitado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            transicionSolicitado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            transicionSolicitado.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionSolicitado);
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id ) {
            this.solicitudService.emitirInstanciaPoliza = true;
        }
        if (!this.solicitudService.userFormHasErrors && !this.solicitudService.emitirInstanciaPoliza) {
            this.updateInstanciaPoliza();
        } else if (!this.solicitudService.userFormHasErrors && this.solicitudService.emitirInstanciaPoliza) {
            if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id + '') {
                this.solicitudService.displayEmitirSolicitud = false;
                this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
            } else {
                this.solicitudService.displayEmitirSolicitud = true;
            }
        }
    }

    updateInstanciaPoliza() {
        if (parseInt(this.solicitudService.atributoDebitoAutomatico.valor) == this.solicitudService.condicionNo.id &&
            this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id &&
            !this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(param => param.id_documento == this.solicitudService.instanciaDocumentoCertificado.id_documento)
        ) {
            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoCertificado);
        }
        this.solicitudService.isLoadingAgain = true;
        if(this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
            if (this.solicitudService.hasRolCajero) {
                if(this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorPagar.id,
                        this.solicitudService.estadoEmitido.id
                    ]
                } else {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoEmitido.id
                    ]
                }
            } else {
                this.solicitudService.estadosUpdateSolicitud = [
                    this.solicitudService.estadoIniciado.id,
                    this.solicitudService.estadoSolicitado.id,
                    this.solicitudService.estadoPorPagar.id,
                    this.solicitudService.estadoEmitido.id
                ]
            }
        } else {
            this.solicitudService.estadosUpdateSolicitud = [
                this.solicitudService.estadoIniciado.id,
                this.solicitudService.estadoSolicitado.id,
                this.solicitudService.estadoEmitido.id
            ]
        }
        this.instanciaPolizaService.updateInstanciaToNextStatus(this.solicitudService.estadosUpdateSolicitud, this.solicitudService.asegurado).subscribe(
            async (res) => {
                let response = res as {
                    status: string;
                    message: string;
                    data: Asegurado;
                };
                this.solicitudService.asegurado = response.data;
                this.stateButtonsFlow();
                this.solicitudService.setDatesOfAsegurado();
                this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach(
                    (instancia_documento: Instancia_documento) => {
                        if (instancia_documento.id_documento == this.solicitudService.documentoSolicitud.id) {
                            this.solicitudService.instanciaDocumentoSolicitud = instancia_documento;
                        } else if (instancia_documento.id_documento == this.solicitudService.documentoComprobante.id) {
                            this.solicitudService.instanciaDocumentoComprobante = instancia_documento;
                        } else if (instancia_documento.id_documento == this.solicitudService.documentoCertificado.id) {
                            this.solicitudService.instanciaDocumentoCertificado = instancia_documento;
                        }
                    }
                );
                this.solicitudService.setComponentes(this.solicitudService.anexosPoliza);
                this.transicionesComponent.ngOnInit();
                await this.setPlanPago(() => {
                    this.solicitudService.displayEmitirSolicitud = false;
                    this.solicitudService.displayEmitirSolicitudDesdeCaja = false;
                    this.solicitudService.displayCambioEstadoExitoso = true;
                    this.solicitudService.msgCambioEstadoExitoso = 'La solicitud fue actualizada exitósamente a la siguiente instancia. ' + this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado];
                    this.solicitudService.isLoadingAgain = false;
                });
            }, (err) => {
                if (
                    err.error.statusCode === 400 &&
                    err.error.message === "usuario no autentificado"
                ) {
                    this.router.navigate([""]);
                } else {
                    console.log(err);
                }
            }
        );
    }

    beforeToggleDatosTitular() {
        this.sessionStorageService.setItemSync("paramsDeleted", 'true');
        this.solicitudService.displayModalDatosTitular = false;
        this.solicitudService.changeView(this.userform, this.solicitudService.asegurado, this.filtros);
    }

    beforeToggleFormTitular() {
        this.solicitudService.displayModalFormTitular = false;
        this.solicitudService.changeView(this.userform, this.solicitudService.asegurado, this.filtros);
    }

    async validarYContinuar() {
        this.solicitudService.msgs_warn = [];
        this.solicitudService.msgs_error = [];
        await this.solicitudService.setAtributosToAsegurado(async () => {
            await this.solicitudService.setComponentes(this.solicitudService.anexosPoliza);
            await this.solicitudService.validarBeneficiarios(this.componenteBeneficiario);
            await this.solicitudService.validarWarnsOrErrors();
            await this.stateButtonsFlow();
            if (this.transicionesComponent != undefined) {
                this.transicionesComponent.ngOnInit();
            }
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                this.solicitudService.mostrarObservaciones = true;
                if (this.solicitudService.msgs_warn.length) {
                    this.solicitudService.msgText = "La información de la solicitud " + this.solicitudService.instanciaDocumentoSolicitud.nro_documento +
                        ", tiene observaciones clasificadas como advertencias que no le impedirán continuar la instrumentación del seguro";
                } else {
                    this.solicitudService.msgText = "La solicitud no tiene observaciones ni advertencias, por lo que se pasará a la siguiente instancia.";
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                this.solicitudService.mostrarObservaciones = false;
                if (this.solicitudService.msgs_warn.length) {
                    this.solicitudService.msgText = "La información de la solicitud " + this.solicitudService.instanciaDocumentoSolicitud.nro_documento +
                        ", tiene observaciones clasificadas como advertencias que no le impedirán continuar la instrumentación del seguro";
                } else {
                    this.solicitudService.msgText = "La solicitud no tiene observaciones ni advertencias, por lo que se pasará a la siguiente instancia.";
                }
            } else {
                this.solicitudService.mostrarObservaciones = false;
                this.solicitudService.msgText = "La solicitud no tiene observaciones ni advertencias, por lo que se la pasará a la siguiente instancia.";
            }

            if (this.solicitudService.userFormHasErrors) {
                this.withErrorsOrWarnings = true;
                //this.messageService.add({ key: 'tst', severity: 'error', summary: 'Error Message', detail: 'Existen campos en el formulario de solicitud que deben ser completados' });
            } else if (this.solicitudService.userFormHasWarnings) {
                this.withErrorsOrWarnings = true;
                //this.messageService.add({ key: 'tst', severity: 'warn', summary: 'Warn Message', detail: 'Existen campos en el formulario de solicitud que tienen observaciones para ser consideradas' });
            } else {
                this.solicitudService.displayValidacionConObservaciones = false;
                this.solicitudService.displayValidacionSinObservaciones = true;
            }
            if (this.solicitudService.userFormHasWarnings && this.solicitudService.userFormHasErrors) {
                this.solicitudService.userFormHasWarnings = false;
                this.solicitudService.userFormHasErrors = true;
                this.solicitudService.displayValidacionConObservaciones = true;
                this.solicitudService.displayValidacionSinObservaciones = false;
            } else {
                this.solicitudService.userFormHasWarnings = false;
                this.solicitudService.userFormHasErrors = false;
                this.solicitudService.displayValidacionConObservaciones = false;
                this.solicitudService.displayValidacionSinObservaciones = true;
            }
        });
    }

    stateButtonsFlow() {
        this.solicitudService.showFormValidation(this.userform);
        this.solicitudService.validarBeneficiarios(this.componenteBeneficiario);
        // this.solicitudService.setFeatureValidacionAlInicioPersonaBanco();
        this.solicitudService.btnValidarYContinuarEnabled = true;
        if ([3, 13].includes(parseInt(this.solicitudService.id_poliza+''))) {
            this.solicitudService.showPlanPago = false;
        }
        if (this.solicitudService.asegurado) {
            if (this.solicitudService.ValidarPlanPago(this.solicitudService.asegurado.instancia_poliza)) {
                this.solicitudService.btnPlanPagoEnabled = true;
            } else {
                this.solicitudService.btnPlanPagoEnabled = false;
            }
            if (this.transicionesComponent != undefined) {
                this.transicionesComponent.ngOnInit();
            }
            if (this.solicitudService.hasRolConsultaTarjetas) {
                this.solicitudService.btnValidarYContinuarEnabled = false;
                this.btnTarjetaDebitoCuentasEnabled = false;
            } else {
                this.solicitudService.btnValidarYContinuarEnabled = true;
                this.btnTarjetaDebitoCuentasEnabled = true;
            }
            this.solicitudService.showOrdenPago = false;
            if (
                this.solicitudService.asegurado.instancia_poliza != null &&
                this.solicitudService.atributoDebitoAutomatico != undefined &&
                this.solicitudService.condicionSi != undefined &&
                this.solicitudService.condicionNo != undefined
            ) {
                if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                    if (this.componenteBeneficiario) {
                        this.componenteBeneficiario.enableAdicionarBeneficiario = false;
                    }
                    this.solicitudService.btnEmitirCertificadoEnabled = false;
                    this.solicitudService.btnImprimirSolicitudEnabled = false;
                    this.solicitudService.btnOrdenPagoEnabled = false;
                    if (this.solicitudService.hasRolConsultaTarjetas) {
                        this.solicitudService.btnRefrescarInformacion = false;
                    } else {
                        this.solicitudService.btnRefrescarInformacion = true;
                    }
                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                    if (this.componenteBeneficiario) {
                        this.componenteBeneficiario.enableAdicionarBeneficiario =
                            true;
                    }
                    this.solicitudService.btnEmitirCertificadoEnabled = false;
                    this.solicitudService.btnRefrescarInformacion = false;
                    this.solicitudService.btnImprimirSolicitudEnabled = true;
                } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
                    if (this.componenteBeneficiario) {
                        this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                    }
                    this.solicitudService.btnPlanPagoEnabled = true;
                    this.solicitudService.btnEmitirCertificadoEnabled = true;
                    this.solicitudService.btnImprimirSolicitudEnabled = true;
                    this.solicitudService.btnOrdenPagoEnabled = true;
                    this.solicitudService.btnValidarYContinuarEnabled = false;
                    this.solicitudService.btnRefrescarInformacion = false;
                } else if (
                    this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoAnulado.id ||
                    this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSinVigencia.id ||
                    this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoCaducado.id ||
                    this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoDesistido.id
                ) {
                    if (this.componenteBeneficiario) {
                        this.componenteBeneficiario.enableAdicionarBeneficiario = false;
                    }
                    this.solicitudService.btnEmitirCertificadoEnabled = false;
                    this.solicitudService.btnImprimirSolicitudEnabled = false;
                    this.solicitudService.btnOrdenPagoEnabled = false;
                    this.solicitudService.btnValidarYContinuarEnabled = false;
                    this.solicitudService.btnRefrescarInformacion = false;
                } else {
                    this.solicitudService.btnEmitirCertificadoEnabled = false;
                    this.solicitudService.btnImprimirSolicitudEnabled = false;
                }
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionNo.id + '') {
                    this.solicitudService.btnOrdenPagoEnabled = false;
                } else if (
                    this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id + '') {
                    this.solicitudService.btnOrdenPagoEnabled = true;
                    this.solicitudService.showOrdenPago = false;
                } else {
                    this.solicitudService.showOrdenPago = false;
                }
                if (this.solicitudService.usuarioLogin.usuarioRoles.find((params) => params.id === this.solicitudService.rolOficialTarjeta.id)) {
                    if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id ) {
                        this.solicitudService.btnValidarYContinuarEnabled = false;
                    }
                    this.showEmitirCertificado = false;
                }
            }
        }
    }

    async refrescarInformacion() {
        this.solicitudService.isLoadingAgain = true;
        this.solicitudService.editandoTitular = true;
        this.getDatosFromCustomerService(async (res) => {
            this.solicitudService.setAtributosToAsegurado();
            await this.stateButtonsFlow();
            this.solicitudService.persona_banco.debito_automatico = this.solicitudService.atributoDebitoAutomatico.valor;
            this.solicitudService.persona_banco_account.moneda = this.solicitudService.atributoMoneda.valor;
            this.solicitudService.persona_banco_datos.nro_tarjeta = this.solicitudService.atributoNroTarjeta.valor;
            this.solicitudService.persona_banco_account.nrocuenta = this.solicitudService.atributoNroCuenta.valor;
            this.solicitudService.persona_banco_datos.zona = this.solicitudService.atributoZona.valor;
            this.solicitudService.persona_banco_datos.tipo_doc = this.solicitudService.atributoTipoDoc.valor;
            this.solicitudService.persona_banco_datos.nro_direccion = this.solicitudService.atributoNroDireccion.valor;
            this.solicitudService.persona_banco_tarjeta_debito.tarjeta_ultimos_cuatro_digitos = this.solicitudService.atributoUltimosCuatroDigitos.valor;
            this.solicitudService.persona_banco_datos.nit_carnet = this.solicitudService.atributoNitCarnet.valor;
            this.solicitudService.persona_banco_datos.razon_social = this.solicitudService.atributoRazonSocial.valor;
            this.solicitudService.persona_banco_tarjeta_debito.fecha_expiracion = isString(this.solicitudService.atributoCtaFechaExpiracion.valor) ? new Date(this.solicitudService.atributoCtaFechaExpiracion.valor + '') : new Date(this.solicitudService.atributoCtaFechaExpiracion.valor + '');
            this.solicitudService.persona_banco_datos_tarjeta.tipo_tarjeta = this.solicitudService.atributoTipoTarjeta.valor;
            this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.estado_tarjeta = this.solicitudService.atributoEstadoTarjeta.valor;
            this.solicitudService.usuario_banco.us_sucursal = parseInt(this.solicitudService.atributoSucursal.valor);
            this.solicitudService.usuario_banco.us_oficina = parseInt(this.solicitudService.atributoAgencia.valor);
            this.solicitudService.usuario_banco.us_cargo = this.solicitudService.atributoUsuarioCargo.valor;
            this.solicitudService.aseguradoBeneficiarios = this.solicitudService.asegurado.beneficiarios;
            this.solicitudService.setComponentes(this.solicitudService.anexosPoliza);
            this.validarTarjetaYGuardarSolicitud(() => {
                this.solicitudService.asegurado.beneficiarios = this.solicitudService.aseguradoBeneficiarios;
                this.solicitudService.setComponentes(this.solicitudService.anexosPoliza);
                this.displayActualizacionExitoso = true;
                this.solicitudService.isLoadingAgain = false;
            });
        });
    }

    async getDatosFromCustomerService(callback: Function = null) {
        await this.soapuiService.getCustomer(this.solicitudService.doc_id, this.solicitudService.extension).subscribe(async (res) => {
            let response = res as { status: string; message: string; data: persona_banco; };
            if (isObject(response.data) && Object.keys(response.data).length) {
                this.solicitudService.persona_banco = response.data;
                this.solicitudService.persona_banco.debito_automatico = "";
                this.solicitudService.esClienteBanco = true;
                if ([3, 13].includes(parseInt(this.solicitudService.id_poliza + ''))) {
                    this.solicitudService.persona_banco_datos = new persona_banco_datos();
                    if (this.solicitudService.persona_banco.cod_agenda != "" && this.solicitudService.persona_banco.cod_agenda != undefined && this.solicitudService.persona_banco.cod_agenda != "undefined") {
                        await this.getDatosFromAccountService(callback);
                    } else {
                        this.solicitudService.persona_banco_accounts = [];
                        this.cuentas = [];
                        this.solicitudService.persona_banco_account = new persona_banco_account();
                        if (typeof callback == "function") {
                            await callback();
                        }
                    }
                } else if (this.solicitudService.id_poliza == 4) {
                    //this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada = new persona_banco_tarjeta_debito_cuentas_vinculadas();
                    await this.getDatosFromCuentasVinculadas(callback);
                }
            } else {
                this.solicitudService.msgText = 'El Cliente no existe';
                this.displayClienteNoExiste = true;
                this.solicitudService.isLoadingAgain = false;
            }
        },(err) => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate([""]);
            } else {
                this.solicitudService.nombreServicioBanco = 'getCustomer';
                this.solicitudService.displayErrorRespuestaBanco = true;
                this.solicitudService.isLoadingAgain = false;
            }
        });
    }

    async getDatosFromAccountService(callback: Function = null) {
        await this.soapuiService.getAccount(this.solicitudService.persona_banco.cod_agenda).subscribe(async (res) => {
            this.solicitudService.Cuentas = [{label: "Seleccione Nro de Cuenta", value: null}];
            let response = res as {
                status: string;
                message: string;
                data: any;
            };
            if (isObject(response.data) && Object.keys(response.data).length) {
                this.solicitudService.persona_banco_accounts = [];
                if (response.data.nrocuenta != undefined) {
                    //this.persona_banco_account = response.data;
                    this.solicitudService.persona_banco_accounts.push(response.data);
                    this.cuentas.push(response.data);
                    this.solicitudService.Cuentas.push({
                        label: response.data.nrocuenta + " " + this.solicitudService.MonedasParametroCod[response.data.moneda + ''],
                        value: response.data.nrocuenta,
                    });
                } else {
                    let keys = Object.keys(response.data);
                    let values = Object.values(response.data);
                    await keys.forEach((key) => {
                        this.cuentas.push(values[key]);
                        this.solicitudService.Cuentas.push({
                            label: values[key].nrocuenta + " " + this.solicitudService.MonedasParametroCod[values[key].moneda],
                            value: values[key].nrocuenta,
                        });
                        this.solicitudService.persona_banco_accounts.push(values[key]);
                    });
                }
                if (this.solicitudService.Cuentas.length) {
                    this.solicitudService.tieneCuentaBanco = true;
                } else {
                    this.solicitudService.tieneCuentaBanco = false;
                    this.solicitudService.msgText = 'El Cliente no tiene numeros de cuenta asociados a su carnet';
                }
            } else {
                this.solicitudService.msgText = 'El Cliente no tiene numeros de cuenta asociados a su carnet';
                this.solicitudService.persona_banco_accounts = [];
                this.solicitudService.Cuentas = [];
                this.cuentas = [];
            }
            this.solicitudService.persona_banco_account = new persona_banco_account();
            if (typeof callback == "function") {
                await callback(this.solicitudService.esClienteBanco, this.solicitudService.tieneCuentaBanco);
            }
        }, (err) => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate([""]);
            } else {
                this.solicitudService.nombreServicioBanco = 'getAccount';
                this.solicitudService.displayErrorRespuestaBanco = true;
                this.solicitudService.isLoadingAgain = false;
            }
        });
    }

    async getDatosFromCuentasVinculadas(callback: Function = null) {
        try {
            this.solicitudService.isLoadingAgain = true;
            await this.soapuiService.cuentasVinculadas(this.solicitudService.doc_id, this.solicitudService.extension).subscribe(async (res) => {
                let response = res as {
                    status: string;
                    message: string;
                    data: any;
                };
                if (response.data) {
                    this.solicitudService.tieneTarjeta = true;
                    await this.getDatosFromAccountService(() => {
                        this.solicitudService.isLoadingAgain = false;
                        this.solicitudService.tarjetasDebitos = [];
                        this.solicitudService.NrosTarjetasDebitos = [];
                        this.solicitudService.NrosTarjetasDebitos = [{
                            label: "Seleccione una tarjeta de débito",
                            value: null
                        }];
                        if (response.data && Object.keys(response.data).length && !response.data[0]) {
                            let data = response.data;
                            data.cuentas.Cuentas = this.cuentas;
                            if ([this.solicitudService.parametroTarjetaHabilitada.parametro_descripcion.toLowerCase().trim(), this.solicitudService.parametroTarjetaInnominada.parametro_descripcion.toLowerCase().trim()].includes(data.estado_tarjeta.toLowerCase().trim())) {
                                this.solicitudService.esClienteBanco = true;
                                this.solicitudService.persona_banco_tarjeta_debito_cuentas_vinculadas = [response.data];
                                if (this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada && !this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.nro_tarjeta) {
                                    this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada = response.data;
                                }
                                this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.fin_vigencia = new Date(data.fin_vigencia + '');
                                this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.fecha_activacion = new Date(data.fecha_activacion + '');
                                this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.estado_tarjeta = data.estado_tarjeta.toLowerCase().trim();
                                this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.tipo = this.solicitudService.TiposTarjetaId[data.tipo.toLowerCase().trim() + ''];
                                this.solicitudService.NrosTarjetasDebitos.push({
                                    label: response.data.nro_tarjeta.trim(),
                                    value: response.data.nro_tarjeta.trim()
                                });
                                this.solicitudService.tarjetasDebitos.push({
                                    fecha_activacion: data.fecha_activacion,
                                    fin_vigencia: data.fin_vigencia,
                                    nro_tarjeta: data.nro_tarjeta,
                                    estado_tarjeta: data.estado_tarjeta ? !parseInt(data.estado_tarjeta) ? this.solicitudService.EstadosTarjetaDescripcion[data.estado_tarjeta.toLowerCase().trim()] : data.estado_tarjeta : null,
                                    tipo: data.tipo ? !parseInt(data.tipo) ? this.solicitudService.TiposTarjetaDescripcion[data.tipo.toLowerCase().trim()] : data.tipo : null,
                                    cuentas: this.cuentas
                                });
                            }
                        } else if (Array.isArray(response.data) && response.data.length) {
                            this.solicitudService.esClienteBanco = true;
                            this.solicitudService.persona_banco_tarjeta_debito_cuentas_vinculadas = response.data;
                            for (let i = 0; i < response.data.length; i++) {
                                let data = response.data[i];
                                data.cuentas.Cuentas = this.cuentas;
                                if ([this.solicitudService.parametroTarjetaHabilitada.parametro_descripcion.toLowerCase().trim(), this.solicitudService.parametroTarjetaInnominada.parametro_descripcion.toLowerCase().trim()].includes(data.estado_tarjeta.toLowerCase().trim())) {
                                    if (this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada && !this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.nro_tarjeta) {
                                        this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada = data;
                                    }
                                    this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.fin_vigencia = new Date(data.fin_vigencia + '');
                                    this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.fecha_activacion = new Date(data.fecha_activacion + '');
                                    this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.estado_tarjeta = data.estado_tarjeta.toLowerCase().trim();
                                    this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.tipo = this.solicitudService.TiposTarjetaId[data.tipo.toLowerCase().trim() + ''];
                                    this.solicitudService.NrosTarjetasDebitos.push({
                                        label: data.nro_tarjeta.trim(),
                                        value: data.nro_tarjeta.trim()
                                    });
                                    this.solicitudService.tarjetasDebitos.push({
                                        fecha_activacion: data.fecha_activacion,
                                        fin_vigencia: data.fin_vigencia,
                                        nro_tarjeta: data.nro_tarjeta,
                                        estado_tarjeta: data.estado_tarjeta ? !parseInt(data.estado_tarjeta) ? this.solicitudService.EstadosTarjetaDescripcion[data.estado_tarjeta.toLowerCase().trim()] : data.estado_tarjeta : null,
                                        tipo: data.tipo ? !parseInt(data.tipo) ? this.solicitudService.TiposTarjetaDescripcion[data.tipo.toLowerCase().trim()] : data.tipo : null,
                                        cuentas: this.cuentas
                                    });
                                }
                            }
                            //this.solicitudService.NrosTarjetasDebitos = response.data;
                        } else if (response.data && Object.keys(response.data).length) {
                            this.solicitudService.esClienteBanco = true;
                            this.solicitudService.persona_banco_tarjeta_debito_cuentas_vinculadas = [];
                            for (let property in response.data) {
                                let data = response.data[property];
                                data.cuentas.Cuentas = this.cuentas;
                                if ([this.solicitudService.parametroTarjetaHabilitada.parametro_descripcion.toLowerCase().trim(), this.solicitudService.parametroTarjetaInnominada.parametro_descripcion.toLowerCase().trim()].includes(data.estado_tarjeta.toLowerCase().trim())) {
                                    this.solicitudService.persona_banco_tarjeta_debito_cuentas_vinculadas.push(data);
                                    if (this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada && !this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.nro_tarjeta) {
                                        this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada = data;
                                    }
                                    this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.fin_vigencia = new Date(data.fin_vigencia + '');
                                    this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.fecha_activacion = new Date(data.fecha_activacion + '');
                                    this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.estado_tarjeta = this.solicitudService.EstadosTarjetaDescripcion[data.estado_tarjeta.toLowerCase().trim() + ''];
                                    this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.tipo = this.solicitudService.TiposTarjetaDescripcion[data.tipo.toLowerCase().trim() + ''];
                                    this.solicitudService.NrosTarjetasDebitos.push({
                                        label: data.nro_tarjeta.trim(),
                                        value: data.nro_tarjeta.trim()
                                    });
                                    this.solicitudService.tarjetasDebitos.push({
                                        fecha_activacion: data.fecha_activacion,
                                        fin_vigencia: data.fin_vigencia,
                                        nro_tarjeta: data.nro_tarjeta,
                                        estado_tarjeta: data.estado_tarjeta ? !parseInt(data.estado_tarjeta) ? this.solicitudService.EstadosTarjetaDescripcion[data.estado_tarjeta.toLowerCase().trim()] : data.estado_tarjeta : null,
                                        tipo: data.tipo ? !parseInt(data.tipo) ? this.solicitudService.TiposTarjetaDescripcion[data.tipo.toLowerCase().trim()] : data.tipo : null,
                                        cuentas: this.cuentas
                                    });
                                }
                            }
                            //this.solicitudService.NrosTarjetasDebitos = response.data;
                        }
                        this.solicitudService.persona_banco_cuentas_vinculadas = this.solicitudService.persona_banco_tarjeta_debito_cuentas_vinculadas;
                        this.solicitudService.tarjetasAsegurar = this.solicitudService.NrosTarjetasDebitos;
                        this.selectNroTarjeta(this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.nro_tarjeta, this.solicitudService.persona_banco_tarjeta_debito_cuentas_vinculadas);
                        if (typeof callback == 'function') {
                            callback(this.solicitudService.esClienteBanco, this.solicitudService.tieneCuentaBanco, this.solicitudService.tieneTarjeta);
                        }
                    });
                } else {
                    //this.solicitudService.esClienteBanco = false;
                    this.solicitudService.isLoadingAgain = false;
                    this.solicitudService.tieneCuentaBanco = false;
                    this.solicitudService.tieneTarjeta = false;
                    this.solicitudService.msgText = 'El Cliente no tiene tarjetas de debito asociadas a su carnet';
                    if (typeof callback == 'function') {
                        callback();
                    }
                }
            }, (err) => {
                if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                    this.router.navigate([""]);
                } else {
                    this.solicitudService.nombreServicioBanco = 'cuentasVinculadas';
                    this.solicitudService.displayErrorRespuestaBanco = true;
                    this.solicitudService.isLoadingAgain = false;
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    async afterSearch() {
        await this.solicitudService.afterSearch(async () => {
            this.solicitudService.isLoadingAgain = false;
            await this.newAsegurado(this.solicitudService.doc_id, this.solicitudService.extension);
            this.stateButtonsFlow();
        });
    }

    async cambiarPersonaExtension(idPerson: number, idInstanciaPoliza: number, docId:string, ext:string) {
        this.solicitudService.isLoadingAgain = true;
        this.personaService.actualizarPersonaExt(idPerson, idInstanciaPoliza, docId, ext).subscribe((resp) => {
            let response = resp as {
                status: string;
                message: string;
                data: { persona:Persona, instancia_documentos:Instancia_documento[]};
            };
            if (isObject(response.data) && Object.keys(response.data).length) {
                this.solicitudService.isLoadingAgain = false;
                this.solicitudService.asegurado.entidad.persona = response.data.persona;
                this.solicitudService.displayActualizacionPersona = false;
                this.afterSearch();
            }
        });
    }

    async getDatosFromTarjetaDebitoCuentaService(callback: Function = null) {
        if (this.solicitudService.persona_banco_datos.nro_tarjeta) {
            this.solicitudService.isLoadingAgain = true;
            await this.soapuiService.getTarjetaDebitoCuentas(this.solicitudService.persona_banco_datos.nro_tarjeta + '').subscribe(async (res) => {
                this.solicitudService.isLoadingAgain = false;
                this.solicitudService.persona_banco_tarjetas_debito = [];
                let response = res as { status: string; message: string; data: any };
                if (isObject(response.data) && Object.keys(response.data).length) {
                    if (response.data.Cuentas != undefined) {
                        this.solicitudService.persona_banco_accounts = [];
                        if (!this.guardandoTitular && !this.solicitudService.editandoTitular) {
                            this.displayNroCuentasActualizados = true;
                        }
                        if (response.data.Cuentas.nro_cuenta != undefined) {
                            this.solicitudService.Cuentas = [{label: "Seleccione Nro de Cuenta", value: null},
                            ];
                            this.solicitudService.persona_banco_tarjetas_debito.push(response.data.Cuentas);
                            this.parametroMoneda =
                                this.solicitudService.parametrosMonedas.find((params) => params.parametro_descripcion == response.data.Cuentas.moneda.trim());
                            const personaBancoAccount: persona_banco_account = new persona_banco_account();
                            personaBancoAccount.nrocuenta = response.data.Cuentas.nro_cuenta;
                            personaBancoAccount.moneda = this.parametroMoneda.parametro_cod;
                            personaBancoAccount.fecha_expiracion = new Date(response.data.Cuentas.fecha_expiracion);
                            personaBancoAccount.cod_agenda = this.solicitudService.persona_banco.cod_agenda;
                            this.solicitudService.persona_banco_accounts.push(personaBancoAccount);
                            this.solicitudService.persona_banco_account.fecha_expiracion = new Date(response.data.Cuentas.fecha_expiracion);
                            this.solicitudService.Cuentas.push({
                                label: personaBancoAccount.nrocuenta + " " + this.solicitudService.MonedasParametroCod[personaBancoAccount.moneda + ''],
                                value: personaBancoAccount.nrocuenta,
                            });
                        } else {
                            this.solicitudService.Cuentas = [
                                {label: "Seleccione Nro de Cuenta", value: null},
                            ];
                            let keys = Object.keys(response.data.Cuentas);
                            let values = Object.values(
                                response.data.Cuentas
                            );
                            values.forEach(
                                (value: persona_banco_tarjeta_debito) => {
                                    this.solicitudService.persona_banco_tarjetas_debito.push(value);
                                    this.parametroMoneda =
                                        this.solicitudService.parametrosMonedas.find((params) => params.parametro_descripcion == value.moneda.trim());
                                    const personaBancoAccount: persona_banco_account = new persona_banco_account();
                                    personaBancoAccount.nrocuenta = value.nro_cuenta;
                                    personaBancoAccount.moneda = this.parametroMoneda.parametro_cod;
                                    personaBancoAccount.fecha_expiracion = new Date(value.fecha_expiracion + '');
                                    personaBancoAccount.cod_agenda = this.solicitudService.persona_banco.cod_agenda;
                                    this.solicitudService.persona_banco_accounts.push(personaBancoAccount);
                                    this.solicitudService.persona_banco_account.fecha_expiracion = new Date(value.fecha_expiracion + '');
                                    this.solicitudService.Cuentas.push({
                                        label: personaBancoAccount.nrocuenta + " " + this.solicitudService.MonedasParametroCod[personaBancoAccount.moneda + ''],
                                        value: personaBancoAccount.nrocuenta,
                                    });
                                }
                            );
                        }
                    }
                } else {
                    if (
                        this.solicitudService.persona_banco.cod_agenda != "" &&
                        this.solicitudService.persona_banco.cod_agenda != undefined &&
                        this.solicitudService.persona_banco.cod_agenda != "undefined"
                    ) {
                        await this.getDatosFromAccountService((res) => {
                            this.solicitudService.persona_banco_tarjetas_debito = [];
                            this.displayNroTarjetaSinCuentas = true;
                            this.userform.controls[
                                "par_tarjeta_nro"
                                ].setErrors(Validators.required);
                            if (typeof callback == "function") {
                                callback();
                            }
                        });
                    } else {
                        this.solicitudService.persona_banco_accounts = [];
                        this.solicitudService.Cuentas = [];
                        this.solicitudService.persona_banco_account =
                            new persona_banco_account();
                        if (typeof callback == "function") {
                            await callback();
                        }
                    }
                }
                if (typeof callback == "function") {
                    await callback();
                }
            }, (err) => {
                if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                    this.router.navigate([""]);
                } else {
                    this.solicitudService.nombreServicioBanco = 'getTarjetaDebitoCuentas';
                    this.solicitudService.displayErrorRespuestaBanco = true;
                    this.solicitudService.isLoadingAgain = false;
                }
            });
        } else {
            this.displayIntroduscaNroTarjeta = true;
        }
    }

    getTarjetasAsegurar(asegurados: Asegurado[], tarjetas: TarjetaDebito[]) {
        let tarjetasAsegurar: any[] = [];
        let tarjetasAseguradas: any[] = [];
        tarjetasAsegurar = [{label: "Seleccione una tarjeta de débito", value: null}];
        if (asegurados && asegurados.length) {
            for (let i = 0; i < asegurados.length; i++) {
                let asegurado: Asegurado = asegurados[i];
                let AseguradoNroTarjeta: Atributo_instancia_poliza = asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.atributo.id == 23);
                let tarjetaAsegurada: TarjetaDebito = tarjetas.find(param => param.nro_tarjeta.trim() == AseguradoNroTarjeta.valor.trim());
                if (tarjetaAsegurada) {
                    tarjetasAseguradas.push({
                        label: tarjetaAsegurada.nro_tarjeta.trim(),
                        value: tarjetaAsegurada.nro_tarjeta.trim()
                    });
                }
            }
            for (let i = 0; i < tarjetas.length; i++) {
                let tarjeta: TarjetaDebito = tarjetas[i];
                if (!tarjetasAseguradas.find(param => param.value.trim() == tarjeta.nro_tarjeta.trim())) {
                    tarjetasAsegurar.push({label: tarjeta.nro_tarjeta.trim(), value: tarjeta.nro_tarjeta.trim()})
                }
            }
        } else {
            for (let i = 0; i < tarjetas.length; i++) {
                let tarjeta: TarjetaDebito = tarjetas[i];
                tarjetasAsegurar.push({label: tarjeta.nro_tarjeta.trim(), value: tarjeta.nro_tarjeta.trim()})
            }
        }
        return [tarjetasAsegurar, tarjetasAseguradas]
    }

    async BuscarCliente() {
        this.solicitudService.buscando = true;
        this.solicitudService.iniciarSolicitud();
        this.solicitudService.setIdPoliza(this.solicitudService.parametrosRuteo.parametro_vista);
        this.stateButtonsFlow();
        await this.getPoliza();
        this.solicitudService.editandoTitular = false;
        this.solicitudService.isLoadingAgain = true;
        this.solicitudService.persona_banco_datos = new persona_banco_datos();
        this.solicitudService.persona_banco = new persona_banco();
        this.solicitudService.persona_banco_account = new persona_banco_account();
        this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada = new persona_banco_tarjeta_debito_cuentas_vinculadas();
        this.solicitudService.persona_banco_tarjeta_debito_cuentas_vinculadas = [];
        this.solicitudService.persona_banco_tarjetas_debito = [];
        this.solicitudService.tarjetasDebitos = [];
        this.solicitudService.persona_banco_datos = new persona_banco_datos();
        this.solicitudService.persona_banco_cuentas_vinculadas = [];
        if (this.solicitudService.doc_id && this.solicitudService.extension) {
            await this.getDatosFromCustomerService(async (isBankClient, tieneCuentaBanco, tieneTarjeta) => {
                let persona_doc_id = this.solicitudService.doc_id ? this.solicitudService.doc_id.trim() : this.solicitudService.doc_id;
                let persona_doc_id_ext = this.solicitudService.extension;
                if (this.solicitudService.persona_banco && Object.keys(this.solicitudService.persona_banco).length) {
                    await this.personaService.findPersonasAseguradasConAtributosByDocIdYPoliza(persona_doc_id, this.solicitudService.objetoAseguradoDatosComplementarios.id, this.solicitudService.id_poliza).subscribe(async res => {
                        let response = res as { status: string; message: string; data: Asegurado[] };
                        this.solicitudService.asegurados = response.data;
                        [this.solicitudService.tarjetasAsegurar, this.solicitudService.tarjetasAseguradas] = this.getTarjetasAsegurar(this.solicitudService.asegurados, this.solicitudService.tarjetasDebitos);
                        if (this.solicitudService.asegurados.length) {
                            this.aseguradoWithDiferentDocIdExt = this.solicitudService.asegurados.find(param => param.entidad.persona.persona_doc_id_ext != persona_doc_id_ext);
                            if (this.aseguradoWithDiferentDocIdExt) {
                                // this.solicitudService.displayActualizacionPersona = true;
                                await this.cambiarPersonaExtension(this.aseguradoWithDiferentDocIdExt.entidad.persona.id, this.aseguradoWithDiferentDocIdExt.id_instancia_poliza, this.solicitudService.doc_id, this.solicitudService.extension);
                            } else {
                                await this.solicitudService.afterSearch(async () => {
                                    this.solicitudService.isLoadingAgain = false;
                                    await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                                    this.stateButtonsFlow();
                                });
                            }
                        } else {
                            this.solicitudService.asegurado = null;
                            if (this.solicitudService.id_poliza == 4) {
                                if (!isBankClient) {
                                    if (!tieneCuentaBanco) {
                                        this.displayNuevoClienteSinNroCuenta = true;
                                    } else {
                                        this.displayNuevoCliente = true;
                                    }
                                } else {
                                    if (!tieneTarjeta) {
                                        this.displaySinTarjetaDebito = true;
                                    } else if (!tieneCuentaBanco) {
                                        this.displayNuevoClienteSinNroCuenta = true;
                                    } else {
                                        await this.abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext);
                                    }
                                }
                            } else if ([3, 13].includes(parseInt(this.solicitudService.id_poliza+''))) {
                                await this.abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext);
                            }
                            this.solicitudService.isLoadingAgain = false;
                            // await this.abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext);
                            this.stateButtonsFlow();
                        }
                    }, err => {
                        if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                            this.router.navigate(['/'])
                        } else {
                            console.log(err);
                        }
                    });
                } else {
                    this.cancelarSolicitud();
                    this.displayClienteNoExiste = true;
                    this.solicitudService.isLoadingAgain = false;
                    this.stateButtonsFlow();
                }
            });
        } else {
            this.displayClienteNoExiste = true;
            this.solicitudService.isLoadingAgain = false;
        }
        this.cols = [];
    }

    async newAsegurado(persona_doc_id, persona_doc_id_ext) {
        let oldAsegurado: Asegurado = new Asegurado();
        if (this.solicitudService.asegurado && Object.keys(this.solicitudService.asegurado).length) {
            oldAsegurado = this.solicitudService.asegurado;
            oldAsegurado.instancia_poliza.atributo_instancia_polizas.forEach(async (atributo_instancia_poliza: Atributo_instancia_poliza) => {
                atributo_instancia_poliza.valor = "";
            });
            oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter.forEach(
                async (
                    atributo_instancia_poliza: Atributo_instancia_poliza
                ) => {
                    atributo_instancia_poliza.valor = "";
                }
            );
        }
        oldAsegurado.entidad.persona.persona_doc_id = persona_doc_id;
        oldAsegurado.entidad.persona.persona_doc_id_ext = persona_doc_id_ext;
        this.solicitudService.asegurado = new Asegurado();
        this.BeneficiariosAux = [];
        this.solicitudService.asegurado.instancia_poliza.id_anexo_poliza = this.solicitudService.poliza.anexo_poliza.id;
        this.solicitudService.asegurado.instancia_poliza.estado = this.solicitudService.estadoIniciado;
        this.solicitudService.asegurado.instancia_poliza.poliza = this.solicitudService.poliza;
        this.solicitudService.asegurado.instancia_poliza.id_estado = this.solicitudService.estadoIniciado.id;
        this.solicitudService.asegurado.instancia_poliza.id_poliza = this.solicitudService.poliza.id;
        this.solicitudService.instanciaDocumentoSolicitud = new Instancia_documento();
        this.solicitudService.instanciaDocumentoSolicitud.documento = this.solicitudService.documentoSolicitud;
        this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoSolicitud);
        this.solicitudService.asegurado.entidad = oldAsegurado.entidad;
        this.solicitudService.persona_banco_datos.tipo_doc = 'CI';
        this.solicitudService.persona_banco_datos.desc_ocupacion = '';
        this.solicitudService.persona_banco_datos.pais_nacimiento = '5';
        this.selectPeriodicidad(this.solicitudService.persona_banco_datos.modalidad_pago);
        this.selectFormaPago(this.solicitudService.persona_banco_solicitud.solicitud_forma_pago);
        this.setNroCuotas(this.solicitudService.persona_banco_datos.nro_cuotas);
        this.solicitudService.persona_banco.debito_automatico = this.solicitudService.Condiciones[2].value + '';
        this.solicitudService.persona_banco_datos_tarjeta.tipo_tarjeta = this.solicitudService.TiposTarjeta[1] ? this.solicitudService.TiposTarjeta[1].value + '' : this.solicitudService.TiposTarjeta[2].value + '';
        this.solicitudService.persona_banco_datos.razon_social = this.solicitudService.persona_banco.paterno + " " + this.solicitudService.persona_banco.nombre;
        this.solicitudService.persona_banco_datos.nit_carnet = this.solicitudService.persona_banco.doc_id + '';
        this.solicitudService.atributoNroCuenta = new Atributo_instancia_poliza();
        this.solicitudService.atributoNroDireccion = new Atributo_instancia_poliza();
        this.solicitudService.atributoUltimosCuatroDigitos = new Atributo_instancia_poliza();
        this.solicitudService.atributoRazonSocial = new Atributo_instancia_poliza();
        this.solicitudService.atributoNitCarnet = new Atributo_instancia_poliza();
        this.solicitudService.atributoTipoTarjeta = new Atributo_instancia_poliza();
        this.solicitudService.atributoSucursal = new Atributo_instancia_poliza();
        this.solicitudService.atributoAgencia = new Atributo_instancia_poliza();
        this.solicitudService.atributoTipoDoc = new Atributo_instancia_poliza();
        this.solicitudService.atributoUsuarioCargo = new Atributo_instancia_poliza();
        this.solicitudService.atributoCtaFechaExpiracion = new Atributo_instancia_poliza();
        this.solicitudService.atributoZona = new Atributo_instancia_poliza();
        this.solicitudService.atributoDebitoAutomatico = new Atributo_instancia_poliza();
        this.solicitudService.aseguradoBeneficiarios = [];
        this.solicitudService.atributoMoneda = new Atributo_instancia_poliza();
        this.solicitudService.atributoNroTarjeta = new Atributo_instancia_poliza();
        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas = oldAsegurado.instancia_poliza.atributo_instancia_polizas;
        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter = oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter;
        await this.componentsBehaviorOnInit();
        await this.solicitudService.setAtributosToAsegurado();
        await this.solicitudService.componentsBehaviorByRol();
        await this.solicitudService.setComponentes(this.solicitudService.anexosPoliza);
        if (this.transicionesComponent != undefined) {
            this.transicionesComponent.ngOnInit();
        }
    }

    selectNroCuenta(event = null) {
        if (this.solicitudService.showNroCuenta && this.solicitudService.showMoneda) {
            let value;
            if (event && event.value) {
                value = event.value;
            } else {
                value = event;
            }
            if (value) {
                if (this.solicitudService.persona_banco_accounts.length) {
                    this.solicitudService.persona_banco_account = this.solicitudService.persona_banco_accounts.find((params) => params.nrocuenta === value);
                    if (this.solicitudService.persona_banco_account) {
                        this.userform.controls["par_moneda"].setValue(this.solicitudService.persona_banco_account.moneda);
                        this.userform.controls["par_numero_cuenta"].setValue(this.solicitudService.persona_banco_account.nrocuenta);
                    }
                }
            }
            if (this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected) {
                this.userform.controls["par_moneda"].setValue(this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.moneda);
                this.userform.controls["par_numero_cuenta"].setValue(this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.nro_cuenta);
            }
        }
    }

    selectFormaPago(event) {
        let value;
        if (event && event.value) {
            value = event.value;
        } else {
            value = event;
        }
        this.solicitudService.editPagoEfectivo = false;
        if (value == this.solicitudService.parametroPagoContado.id) {
            this.solicitudService.showModalidadPago = true;
            this.solicitudService.editModalidadPago = false;
            this.solicitudService.editPrima = false;
            this.solicitudService.editNroCuotas = false;
            this.selectPeriodicidad(this.solicitudService.parAnual.id);
        } else if (value == this.solicitudService.parametroPagoCredito.id) {
            this.solicitudService.showModalidadPago = true;
            this.solicitudService.editModalidadPago = false;
            this.solicitudService.editNroCuotas = false;
            this.selectPeriodicidad(this.solicitudService.parMensual.id);
        } else {
            this.solicitudService.persona_banco_datos.modalidad_pago = null
        }
        this.solicitudService.persona_banco.debito_automatico = this.solicitudService.pagoDebitAutomatico.id + '';

    }

    async setNroCuotas(event) {
        let value;
        if (event && event.target && event.target.value) {
            value = event.target.value;
        } else {
            value = event;
        }
        if (value && parseInt(value)) {
            this.solicitudService.persona_banco_datos.prima = parseInt(this.solicitudService.anexoPoliza.monto_prima + '') / parseInt(value+'');
        } else {
            this.solicitudService.persona_banco_datos.prima = 0;
        }
    }

    async selectPeriodicidad(event) {
        let value;
        if (event && event.value) {
            value = event.value;
        } else {
            value = event;
        }
        if (value && parseInt(value) && this.solicitudService.parAnual.id == value) {
            this.solicitudService.persona_banco_datos.nro_cuotas = 1;
            this.solicitudService.editPagoEfectivo = false;
            this.solicitudService.persona_banco_datos.modalidad_pago = this.solicitudService.parAnual.id+'';
            this.selectMetodoPago(this.solicitudService.pagoDebitAutomatico.id);
        } else if (value && parseInt(value) && this.solicitudService.parMensual.id == value) {
            this.solicitudService.persona_banco_datos.nro_cuotas = 12;
            this.solicitudService.editPagoEfectivo = false;
            this.solicitudService.persona_banco_datos.modalidad_pago = this.solicitudService.parMensual.id+'';
            this.selectMetodoPago(this.solicitudService.pagoDebitAutomatico.id);
        } else {
            this.solicitudService.persona_banco_datos.nro_cuotas = 0;
        }
        this.setNroCuotas(this.solicitudService.persona_banco_datos.nro_cuotas)
    }

    async abrirModalRegistroCliente(persona_doc_id, persona_doc_id_ext) {
        await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
        this.stateButtonsFlow();
        this.solicitudService.displayModalFormTitular = true;
        this.solicitudService.displayBusquedaCI = false;
        this.collapsedFormTitular = false;
        // this.persona_banco_datos_tarjeta.tipo_tarjeta = this.TiposTarjeta[1] ? this.TiposTarjeta[1].value+'' : this.TiposTarjeta[2].value+'';
        await this.solicitudService.validarWarnsOrErrors(() => {
            if (this.solicitudService.userFormHasErrors && this.solicitudService.userFormHasWarnings) {
                this.displayValidacionAlInicio = true;
                this.solicitudService.msgText = "La solicitud tiene las siguientes advertencias y observaciones que deben ser resueltas:";
            } else if (this.solicitudService.userFormHasWarnings) {
                this.displayValidacionAlInicio = true;
                this.solicitudService.msgText = "La solicitud tiene las siguientes advertencias: ";
            }
        });
        this.solicitudService.enableUserform(this.userform);
    }

    cancelarSolicitud() {
        this.solicitudService.iniciarSolicitud();
        this.stateButtonsFlow();
        this.solicitudService.displayModalSolicitudExistente = false;
        this.solicitudService.asegurado = new Asegurado();
        this.sessionStorageService.removeItem('parametros');
        this.sessionStorageService.setItemSync('paramsDeleted', true);
    }

    abrirVentanaTitular() {
        this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_apellido_casada = "";
        this.stateButtonsFlow();
        this.solicitudService.displayModalFormTitular = true;
        this.solicitudService.displayFormTitular = true;
    }

    abrirVentanaRegistroNuevoTitular() {
        this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_apellido_casada = "";
        this.solicitudService.displayFormTitular = true;
        this.solicitudService.displayDatosTitular = true;
        this.solicitudService.isLoadingAgain = false;
        this.stateButtonsFlow();
        this.solicitudService.displayModalFormTitular = true;
    }

    onRowSelectDato_complementario(even: any) {
    }

    getFechaMinimaSegunEdad() {
        var date = new Date();
        if (this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento) {
            date.setDate(this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getDate());
            date.setFullYear(this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getFullYear() - 18);
            return date;
        } else {
            return null;
        }
    }

    getFechaMaximaSegunEdad() {
        var date = new Date();
        if (this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento) {
            date.setDate(this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getDate());
            date.setFullYear(this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getFullYear() + 65);
            return date;
        } else {
            return null;
        }
    }

    async guardarSolicitudValidando() {
        await this.setAnexoPoliza();
        await this.solicitudService.setAtributosToAsegurado(async () => {
            await this.solicitudService.validarFechaNacimiento(this.userform, async () => {
                await this.solicitudService.validarApellidos(this.userform, async () => {
                    if (
                        !this.solicitudService.stopSavingByEdad &&
                        !this.solicitudService.stopSavingByLastName
                    ) {
                        await this.validarTarjetaYGuardarSolicitud();
                    }
                });
            });
        });
    }

    async guardarSolicitudPersona() {
        this.solicitudService.isLoadingAgain = true;
        await this.solicitudService.setAtributosToAsegurado(async () => {
            if (this.solicitudService.editandoTitular) {
                this.solicitudService.editandoTitular = false;
                this.personaService.actualizarSolicitud(this.solicitudService.asegurado).subscribe(async (res) => {
                        let response = res as { status: string; message: string; data: Asegurado; };
                        this.solicitudService.buscando = false;
                        this.solicitudService.asegurado = response.data;
                        this.solicitudService.setFeaturesBeneficiarios(this.solicitudService.asegurado.beneficiarios);
                        this.solicitudService.persona_banco_account = new persona_banco_account();
                        this.solicitudService.persona_banco = new persona_banco();
                        this.solicitudService.persona_banco_datos = new persona_banco_datos();
                        this.solicitudService.setDatesOfAsegurado();
                        await this.stateButtonsFlow();
                        await this.solicitudService.setAtributosToPersonaBanco();
                        await this.solicitudService.setComponentes(this.solicitudService.anexosPoliza);
                        this.solicitudService.isLoadingAgain = false;
                    }, (err) => {
                        if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                            this.router.navigate([""]);
                        } else {
                            console.log(err);
                        }
                    }
                );
            } else {
                this.id_asegurado = "";
                await this.personaService.crearNuevaSolicitud(this.solicitudService.asegurado).subscribe(
                    async (res) => {
                        let response = res as { status: string; message: string; data: Asegurado; };
                        this.solicitudService.buscando = false;
                        this.solicitudService.asegurado = response.data;
                        this.solicitudService.setFeaturesBeneficiarios(this.solicitudService.asegurado.beneficiarios);
                        this.solicitudService.persona_banco_account = new persona_banco_account();
                        this.solicitudService.persona_banco = new persona_banco();
                        this.solicitudService.persona_banco_datos = new persona_banco_datos();
                        await this.stateButtonsFlow();
                        this.solicitudService.setDatesOfAsegurado();
                        await this.solicitudService.setAtributosToPersonaBanco();
                        await this.solicitudService.setComponentes(this.solicitudService.anexosPoliza);
                        this.solicitudService.isLoadingAgain = false;
                    }, (err) => {
                        if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                            this.router.navigate([""]);
                        } else {
                            console.log(err);
                        }
                    }
                );
            }
            this.solicitudService.displayDatosTitular = true;
            this.collapsedDatosTitular = false;
            this.solicitudService.displayModalDatosTitular = true;
            this.solicitudService.displayModalFormTitular = false;
        });
    }

    async validarTarjetaYGuardarSolicitud(callback: Function = null) {
        this.solicitudService.isLoadingAgain = true;
        this.solicitudService.usuarioLogin.usuarioRoles.forEach((usuarioRol) => {
            if (usuarioRol.id == this.solicitudService.rolOficialPlataforma.id) {
                this.validarTarjeta = true;
            }
        });
        // if(this.validarTarjeta && this.persona_banco_datos.nro_tarjeta != '') {
        if (this.solicitudService.persona_banco_datos.nro_tarjeta != "") {
            // this.validarTarjeta = false;
            if ([3, 13].includes(parseInt(this.solicitudService.id_poliza+''))) {
                await this.soapuiService.validaTarjeta(this.solicitudService.persona_banco.cod_agenda, this.solicitudService.persona_banco_datos.nro_tarjeta).subscribe(
                    async (res) => {
                        let response = res as {
                            status: string;
                            message: string;
                            data: persona_banco_tarjeta_credito;
                        };
                        if (response.data != undefined) {
                            this.solicitudService.persona_banco_tarjeta_credito.valida =
                                response.data.valida;
                            if (this.solicitudService.persona_banco_tarjeta_credito.valida == "S") {
                                this.guardarSolicitudPersona();
                                if (callback != null) {
                                    callback();
                                }
                            } else {
                                this.displayTarjetaInvalidaContinuar = true;
                                this.solicitudService.isLoadingAgain = false;
                            }
                        }
                    }, (err) => {
                        if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                            this.router.navigate([""]);
                        } else {
                            this.solicitudService.nombreServicioBanco = 'validaTarjeta';
                            this.solicitudService.displayErrorRespuestaBanco = true;
                            this.solicitudService.isLoadingAgain = false;
                        }
                    }
                );
            } else if (this.solicitudService.id_poliza == 4) {
                this.guardandoTitular = true;
                this.guardarSolicitudPersona();
            }
        } else {
            this.guardarSolicitudPersona();
            if (callback != null) {
                callback();
            }
        }
    }

    async setPlanPago(callback: Function = null) {
        if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id ) {
            this.solicitudService.isLoadingAgain = true;

            let documentoCertificado = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find(param => param.id_documento == this.solicitudService.documentoCertificado.id);
            let atributoCuota = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.solicitudService.atributoNroCuotas.objeto_x_atributo.id_atributo);
            let atributoMonto = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.solicitudService.atributoMonto.objeto_x_atributo.id_atributo);
            let atributoModalidad = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.solicitudService.atributoModalidadPago.objeto_x_atributo.id_atributo);
            let atributoMoneda = this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find(param => param.objeto_x_atributo.id_atributo == this.solicitudService.atributoMoneda.objeto_x_atributo.id_atributo);

            let planPago: Plan_pago = new Plan_pago();

            if (documentoCertificado && documentoCertificado.fecha_emision) {

                planPago.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
                planPago.total_prima = atributoCuota && atributoMonto ? parseInt(atributoCuota.valor) * parseInt(atributoMonto.valor) : 0;
                planPago.interes = 0;
                planPago.id_moneda = parseInt(atributoMoneda.valor);
                planPago.plazo_anos = 1;
                planPago.periodicidad_anual = atributoModalidad && atributoModalidad.valor == this.solicitudService.parMensual.id + '' ? 12 : atributoModalidad.valor == this.solicitudService.parAnual.id + '' ? 1 : 0;
                planPago.prepagable_postpagable = 1;
                planPago.fecha_inicio = documentoCertificado.fecha_inicio_vigencia;
                planPago.adicionado_por = this.solicitudService.asegurado.instancia_poliza.adicionada_por;
                planPago.modificado_por = this.solicitudService.asegurado.instancia_poliza.modificada_por;

                if (!planPago.total_prima) {
                    planPago.total_prima = this.solicitudService.poliza.anexo_poliza.monto_prima;
                }

                this.planPagoService.GenerarPlanPagos(planPago).subscribe(res => {
                    let response = res as { status: string, message: string, data: any[] };
                    if (response.status == 'ERROR') {
                        this.displayErrorPlanPago = true;
                    }
                    if (typeof callback == 'function') {
                        callback();
                    }
                    this.solicitudService.isLoadingAgain = false;
                });
            } else {
                this.displayErrorPlanPago = true;
            }
        } else {
            if (typeof callback == 'function') {
                callback();
            }
            this.solicitudService.isLoadingAgain = false;
        }
    }


    async getPoliza(callback:Function = null) {
        await this.solicitudService.setIdPoliza(this.solicitudService.parametrosRuteo.parametro_vista);
        await this.polizaService.getPolizaById(this.solicitudService.id_poliza).subscribe(async res => {
            let response = res as { status: string, message: string, data: Poliza };
            this.solicitudService.poliza = response.data;
            await this.solicitudService.getAllParametrosByIdDiccionario([32,33],
                this.solicitudService.poliza.id, [1, 2, 3, 4, 11, 17, 18, 20, 21, 22, 25, 29, 30, 31, 32, 33, 36, 38, 40, 52, 34, 58, 59, 41, 61],async () => {
                    if (this.solicitudService.poliza.anexo_polizas && this.solicitudService.poliza.anexo_polizas.length) {
                        this.solicitudService.poliza.anexo_poliza = this.solicitudService.poliza.anexo_polizas.find(param => param.id_tipo == 288 );
                        this.solicitudService.anexoPoliza = this.solicitudService.poliza.anexo_polizas.find(param => param.id_tipo == 288 );
                        this.solicitudService.anexoAsegurado = this.solicitudService.anexoPoliza.anexo_asegurados.find(param => param.id_tipo == this.solicitudService.parametroAsegurado.id);
                        if(this.solicitudService.anexoAsegurado) {
                            this.solicitudService.isRenovated = this.solicitudService.asegurado.instancia_poliza.id == this.solicitudService.asegurado.instancia_poliza.id_instancia_renovada ? false : true;
                            switch (this.solicitudService.anexoAsegurado.id_edad_unidad) {
                                case this.solicitudService.parametroYear.id:
                                    this.solicitudService.edadMinimaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                                case this.solicitudService.parametroMonth.id:
                                    this.solicitudService.edadMinimaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                                case this.solicitudService.parametroDay.id:
                                    this.solicitudService.edadMinimaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                            }
                        }
                    }
                    if(typeof callback == 'function') {
                        await callback();
                    }
                });
        },err =>{
            if(err.error.statusCode===400 && err.error.message==="usuario no autentificado"){
                this.router.navigate(['']);
            }else{
                console.log(err);
            }
        });
    }


    editarTitular() {
        if (this.solicitudService.asegurado.instancia_poliza.id_estado != this.solicitudService.estadoIniciado.id &&
            this.solicitudService.asegurado.instancia_poliza.id_estado != this.solicitudService.estadoSolicitado.id &&
            this.solicitudService.asegurado.instancia_poliza.id_estado != null
        ) {
            this.solicitudService.disableForm(this.userform);
        }
        this.solicitudService.componentsBehavior(res => {
            this.solicitudService.setAtributosToPersonaBanco(this.userform, () => {
                this.stateButtonsFlow();
                this.setPago(this.solicitudService.persona_banco.debito_automatico);
                this.selectNroCuenta(this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected.nro_cuenta);
                this.solicitudService.setDatesOfAsegurado();
                this.solicitudService.editandoTitular = true;
                this.solicitudService.displayModalFormTitular = true;
                this.solicitudService.displayModalDatosTitular = false;
                this.collapsedFormTitular = false;
            });
        });
    }

    beforeToggle() {
        if (this.solicitudService.editandoTitular) {
            this.solicitudService.editandoTitular = false;
            this.solicitudService.displayModalDatosTitular = true;
        } else if (this.solicitudService.buscando) {
            this.solicitudService.displayBusquedaCI = true;
            this.solicitudService.asegurado = new Asegurado();
        } else {
            if (this.solicitudService.fromGestionSolicitudes) {
                //this.verGestionSolicitudes();

                if (this.solicitudService.id_poliza == 3) {
                    this.menuService.activaRuteoMenu("16", null, null);
                }
                if (this.solicitudService.id_poliza == 13) {
                    this.menuService.activaRuteoMenu("84", null, null);
                }
                if (this.solicitudService.id_poliza == 4) {
                    this.menuService.activaRuteoMenu("23", null, null);
                }
            } else {
                this.solicitudService.displayBusquedaCI = true;
            }
        }
    }

    async setPago(value) {
        if (this.solicitudService.showNroCuenta && this.solicitudService.showMoneda) {
            if (this.solicitudService.pagoDebitAutomatico.id == value) {
                this.solicitudService.showTipoCuenta = true;
                this.solicitudService.showNroCuenta = true;
                this.solicitudService.showMoneda = true;
                this.userform.controls['par_numero_cuenta'].setValidators(Validators.required);
                this.userform.controls['par_moneda'].setValidators(Validators.required);
            } else if ([10, 12].includes(parseInt(this.solicitudService.poliza.id + ''))) {
                this.userform.controls['par_numero_cuenta'].clearValidators();
                this.userform.controls['par_moneda'].clearValidators();
                this.solicitudService.showMoneda = false;
                this.solicitudService.showNroCuenta = false;
                this.solicitudService.showTipoCuenta = false;
            } else {
                this.solicitudService.showTipoCuenta = false;
                this.solicitudService.showNroCuenta = false;
                this.solicitudService.showMoneda = false;
                this.userform.controls['par_numero_cuenta'].clearValidators();
                this.userform.controls['par_moneda'].clearValidators();
                this.userform.controls['par_numero_cuenta'].setValue('');
                this.userform.controls['par_moneda'].setValue('');
            }
        } else {
           this.userform.controls['par_numero_cuenta'].clearValidators();
            this.userform.controls['par_moneda'].clearValidators();
            this.userform.controls['par_numero_cuenta'].setValue('');
            this.userform.controls['par_moneda'].setValue('');
        }
    }

    async selectMetodoPago(event:any) {
        let value;
        if (event && event.value) {
            value = event.value;
        } else {
            value = event;
        }
        if (value == this.solicitudService.pagoDebitAutomatico.id + '') {
            this.solicitudService.showNroCuenta = true;
            // this.solicitudService.persona_banco_datos.modalidad_pago = this.solicitudService.parMensual.id + '';
            if (this.solicitudService.id_poliza == 4) {
                await this.getDatosFromCuentasVinculadas(async () => {
                    this.setPago(value);
                });
            } else {
                await this.getDatosFromAccountService(async () => {
                    await this.setPago(value);
                });
            }
        } else {
            this.solicitudService.showNroCuenta = false;
            await this.setPago(value);
        }
    }

    selectNroTarjeta(event: any, cuentasVinculadas: any) {
        let value;
        if (event && event.target && event.target.value) {
            value = event.target.value;
        } else if (event && event.value) {
            value = event.value;
        } else if (typeof event == 'string') {
            value = event;
        }
        if (value) {
            let cuentaVinculada = this.solicitudService.tarjetasDebitos.find(param => param.nro_tarjeta.trim() == value.trim());
            //this.userform.controls['par_tipo_tarjeta'].setValue(cuentaVinculada.tipo);
            this.userform.controls['par_tar_fecha_activacion'].setValue(new Date(cuentaVinculada.fecha_activacion + ''));
            this.userform.controls['par_tar_fin_vigencia'].setValue(new Date(cuentaVinculada.fecha_activacion + ''));
            //this.userform.controls['par_estado_tarjeta'].setValue(this.solicitudService.EstadosTarjetaDescripcion[cuentaVinculada.estado_tarjeta.toLowerCase().trim()+'']);
            this.userform.controls['par_tarjeta_nro'].setValue(cuentaVinculada.nro_tarjeta.trim());
            this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.nro_tarjeta = cuentaVinculada.nro_tarjeta.trim();
            this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.tipo = cuentaVinculada.tipo;
            this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.estado_tarjeta = cuentaVinculada.estado_tarjeta;
            this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.fecha_activacion = new Date(cuentaVinculada.fecha_activacion + '');
            this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.fin_vigencia = new Date(cuentaVinculada.fin_vigencia + '');
            if (cuentaVinculada) {
                cuentaVinculada.tipo = cuentaVinculada ? cuentaVinculada.tipo ? parseInt(cuentaVinculada.tipo) ? cuentaVinculada.tipo : this.solicitudService.TiposTarjetaDescripcion[cuentaVinculada.tipo.toLowerCase()] : null : null;
                this.solicitudService.Cuentas = [];
                this.solicitudService.Cuentas.push({value: null, label: 'Seleccione un Nro. de cuenta'});
                let cuentas = [];
                if (cuentaVinculada.cuentas && Object.keys(cuentaVinculada.cuentas).length) {
                    if (cuentaVinculada.cuentas[0]) {
                        for (const property in cuentaVinculada.cuentas) {
                            cuentas.push(cuentaVinculada.cuentas[property]);
                        }
                    } else {
                        cuentas.push(cuentaVinculada.cuentas);
                    }
                }
                if (cuentas) {
                    this.solicitudService.tieneCuentaBanco = true;
                    for (let i = 0; i < cuentas.length; i++) {
                        let cuenta = cuentas[i];
                        cuenta.moneda = parseInt(cuenta.moneda + '') ? cuenta.moneda : this.solicitudService.MonedasAbreviacion[this.solicitudService.MonedasDescripcion[cuenta.moneda.trim()]];
                        if (!this.solicitudService.Cuentas.find(param => param.value == cuenta.nrocuenta)) {
                            this.solicitudService.Cuentas.push({
                                value: cuenta.nrocuenta,
                                label: `${cuenta.nrocuenta} ${this.solicitudService.MonedasParametroCod[parseInt(cuenta.moneda)]}`
                            });
                        }
                        this.solicitudService.persona_banco_tarjeta_debito_cuenta_vinculada.cuentaSelected = cuenta;
                        this.userform.controls['par_numero_cuenta'].setValue(cuenta.nro_cuenta);
                        this.userform.controls['par_moneda'].setValue(this.solicitudService.MonedasParametroCod[parseInt(cuenta.moneda)]);
                    }
                    //this.setPago(this.solicitudService.persona_banco.debito_automatico);
                }
            }
        } else {
            this.userform.controls['par_tipo_tarjeta'].setValue(null);
            this.userform.controls['par_tar_fecha_activacion'].setValue(null);
            this.userform.controls['par_tar_fin_vigencia'].setValue(null);
            this.userform.controls['par_estado_tarjeta'].setValue(null);
            this.userform.controls['par_tarjeta_nro'].setValue(null);
        }
    }
}
