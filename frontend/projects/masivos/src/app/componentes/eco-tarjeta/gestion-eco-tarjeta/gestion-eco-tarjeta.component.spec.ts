import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionEcoTarjetaComponent } from './gestion-eco-tarjeta.component';

describe('GestionEcoTarjetaComponent', () => {
  let component: GestionEcoTarjetaComponent;
  let fixture: ComponentFixture<GestionEcoTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionEcoTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionEcoTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
