import {SolicitudService} from '../../../../../../../src/core/servicios/solicitud.service';
import {MessageService} from 'primeng/api';
import {ReporteService} from '../../../../../../../src/core/servicios/reporte.service';
import {MenuService} from '../../../../../../../src/core/servicios/menu.service';
import {Router} from '@angular/router';
import {InstanciaPolizaService} from '../../../../../../../src/core/servicios/instancia-poliza.service';
import {ParametrosService} from '../../../../../../../src/core/servicios/parametro.service';
import {BreadcrumbService} from '../../../../../../../src/core/servicios/breadcrumb.service';
import {ActivatedRoute} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {Instancia_poliza} from '../../../../../../../src/core/modelos/instancia_poliza';
import {Parametro} from '../../../../../../../src/core/modelos/parametro';
import {Componente, Filtro} from '../../../../../../../src/core/modelos/componente';
import {Component, OnInit} from '@angular/core';
import * as xlsx from 'xlsx';
import * as FileSaver from 'file-saver';
import {Poliza} from "../../../../../../../src/core/modelos/poliza";
import {PersonaService} from "../../../../../../../src/core/servicios/persona.service";
import {SessionStorageService} from "../../../../../../../src/core/servicios/sessionStorage.service";
import {Plan_pago} from "../../../../../../../src/core/modelos/plan_pago";
import {PlanPagoService} from "../../../../../../../src/core/servicios/plan-pago.service";
import {Util} from "../../../../../../../src/helpers/util";
declare var $:any;

@Component({
    selector: 'app-gestion-eco-tarjeta',
    templateUrl: './gestion-eco-tarjeta.component.html',
    styleUrls: ['./gestion-eco-tarjeta.component.css'],
    providers: [MessageService]
})
export class GestionEcoTarjetaComponent implements OnInit {

    color = 'primary';
    mode = 'indeterminate';
    value = 1;
    componentesEditables: Componente[];
    parametros: Parametro[] = [];
    instancias_polizas: Instancia_poliza[] = [];
    instancias: any[] = [];
    poliza = new Poliza();
    cols: any;
    objeto: Filtro = new Filtro();
    paramAgencias: Parametro[] = [];
    paramSucursales: Parametro[] = [];
    id_instancias: any;
    filtros: boolean = true;
    FiltrosFechas: SelectItem[] = [];
    ProcedenciaCI: SelectItem[] = [];
    Estados_instancia_poliza: SelectItem[] = [];
    Tipos_tarjetas: SelectItem[] = [];
    Agencias: SelectItem[] = [];
    Sucursales: SelectItem[] = [];
    estado: any;
    exportColumns: any[];
    id_documento: any;
    displayExedioResultados: boolean = false;
    showMoreResultsMessage: boolean = false;
    PanelFiltrosColapsed: boolean = false;
    btnEmitir: boolean = false;
    util = new Util();
    btnEditar: boolean = false;
    btnVer1: boolean = false;
    btnVer2: boolean = false;
    index_instancia_poliza: number;
    Estados: SelectItem[] = [];
    rowsPerPage: number = 0;
    numRecords: number = 0;
    totalRows: number = 0;
    vista:any;

    constructor(
        private params: ActivatedRoute, private breadcrumbService: BreadcrumbService,
        private parametrosService: ParametrosService,
        private instanciaPolizaService: InstanciaPolizaService,
        private router: Router,
        private personaService: PersonaService,
        private menuService: MenuService,
        private reporteService: ReporteService,
        private service: MessageService,
        private sessionStorageService: SessionStorageService,
        public solicitudService: SolicitudService,
        private planPagoService: PlanPagoService
    ) {
        this.vista = this.menuService.ObtenerParametrosVista(this.params.snapshot.paramMap.get('id'));
        this.cols = [
            {field: 'Nro', header: 'Nro', width: '5%'},
            {field: 'Nro_Sol', header: 'Nro. Sol.', width: '10%'},
            {field: 'Sucursal', header: 'Sucursal', width: '8%'},
            {field: 'Agencia', header: 'Agencia', width: '10%'},
            {field: 'Nro_Certificado', header: 'Nro. Certificado', width: '8%'},
            {field: 'Estado', header: 'Estado', width: '10%'},
            {field: 'Fecha_Registro', header: 'Fecha Registro', width: '10%'},
            {field: 'Fecha_Solicitud', header: 'Fecha Solicitud', width: '10%'},
            {field: 'Fecha_Emision', header: 'Fecha Emisión', width: '10%'},
            {field: 'Asegurado', header: 'Asegurado', width: '19%'},
            {field: 'usuario', header: 'usuario', width: '10%'},
            {field: null, header: 'Acciones', width: '10%'}
        ];
        this.exportColumns = this.cols.map(col => ({title: col.header, dataKey: col.field}));
    }


    ngOnInit() {
        this.solicitudService.constructComponent(() => {
            if (this.solicitudService.parametrosRuteo && ['14','17'].includes(this.solicitudService.parametrosRuteo.id_vista+'')) {
                let param = JSON.parse(this.solicitudService.parametrosRuteo.parametro_ruteo);
                this.id_instancias = this.solicitudService.parametrosRuteo.parametro_ruteo;
                this.estado = param.estado;
                this.id_documento = param.id_documento;
                this.listaAllByEstado();
            }
            this.breadcrumbService.setItems([{label: this.solicitudService.ruta}]);
            this.objeto = this.solicitudService.parametrosRuteo.parametro_ruteo && this.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? this.solicitudService.parametrosRuteo.parametro_ruteo.filtros : this.objeto;
            this.objeto.id_poliza = parseInt(this.solicitudService.parametrosRuteo.parametro_vista+'') ? parseInt(this.solicitudService.parametrosRuteo.parametro_vista+'') : this.solicitudService.id_poliza;
            this.solicitudService.product = this.solicitudService.ruta;
            this.solicitudService.isInAltaSolicitud = false;
            this.solicitudService.isLoadingAgain = false;
            this.GetAllParametrosByIdDiccionarios();
            if (this.vista.parametros2 && ['16','23','84'].includes(this.solicitudService.parametrosRuteo.id_vista+'')) {
                this.solicitudService.id_poliza = this.vista.parametros2;
            }
            if (this.solicitudService.parametrosRuteo.parametro_ruteo && Object.keys(this.solicitudService.parametrosRuteo.parametro_ruteo).length) {
                this.id_instancias = this.solicitudService.parametrosRuteo.parametro_ruteo;
                this.listaAll();
            } else {
                this.PanelFiltrosColapsed = false;
            }
        })
    }

    listaAllByIds() {
        this.instanciaPolizaService.GetAllByIdsTarjetas(this.id_instancias).subscribe(res => {
            let response = res as { status: string, message: string, data: Instancia_poliza[], recordsTotal: number, recordsOffset: number, recordsFiltered: number };
            this.instancias_polizas = response.data;
            this.totalRows = response.recordsTotal ? response.recordsTotal : 0;
            this.objeto.first = response.recordsOffset ? response.recordsOffset : 0;
            this.objeto.rows = response.recordsFiltered ? this.totalRows >= response.recordsFiltered ? response.recordsFiltered : this.totalRows : this.totalRows > 10 ? 10 : this.totalRows;
            this.instancias_polizas = response.data;
            if (this.instancias_polizas.length >= 300) {
                this.showMoreResultsMessage = true;
            } else {
                this.showMoreResultsMessage = false;
            }
            this.PanelFiltrosColapsed = true;
            this.instancias_polizas = this.getCars(this.instancias_polizas);
            let lastEmptyRows = (this.totalRows % this.objeto.rows) - this.objeto.rows;
            lastEmptyRows = lastEmptyRows < 0 ? lastEmptyRows * (-1) : lastEmptyRows;
            let lastFilledRows = this.objeto.rows - lastEmptyRows;
            let rowsAfter = this.totalRows - lastFilledRows;
            rowsAfter = rowsAfter == this.totalRows ? rowsAfter - this.objeto.rows : rowsAfter;
            for (let i = 0; i < rowsAfter; i++) {
                this.instancias_polizas.push(new Instancia_poliza());
                let rowNumber = i + (this.objeto.first + 1);
                this.instancias_polizas[i].Nro = rowNumber > this.totalRows ? null : rowNumber;
            }
            if (this.totalRows - this.rowsPerPage == lastFilledRows) {
                this.rowsPerPage = this.objeto.first + lastFilledRows;
            } else {
                this.rowsPerPage = this.objeto.first + this.objeto.rows;
            }
            if (this.totalRows - this.objeto.first == lastFilledRows) {
                this.numRecords = lastFilledRows
            } else {
                this.numRecords = this.objeto.rows
            }
            if (this.instancias_polizas && this.instancias_polizas.length) {
                if (this.instancias_polizas.length >= 300 && !this.solicitudService.userInfo.usuarioRoles.find(param => param.id == 5)) {
                    this.displayExedioResultados = true;
                }
            } else {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No se encontro ningun registro'
                });
                this.poliza = new Poliza();
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login'])
            } else {
                console.log(err);
            }
        });
    }

    listaAll(dt: any = null, callback: any = null) {
        if (dt) {
            dt.reset();
        }
        setTimeout(() => {
            if ($(window).width() < 720) {
                $('.ui-table').css('overflow','scroll');
                $('.ui-table table').css('width','400%');
            }
        },2000);
        this.instancias = [];
        if(this.objeto && this.objeto.id_instancia_poliza) {
            this.objeto = new Filtro();
            this.objeto.id_instancia_poliza = this.solicitudService.parametrosRuteo.parametro_ruteo.filtros.id_instancia_poliza;
        } else {
            this.objeto = this.solicitudService.parametrosRuteo.parametro_ruteo && this.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? this.solicitudService.parametrosRuteo.parametro_ruteo.filtros : this.objeto;
        }
        if (!this.objeto.agencia) {
            this.objeto.agencia_required = false;
        }
        if (!this.objeto.sucursal_required) {
            this.objeto.sucursal_required = false;
        }
        this.solicitudService.isLoadingAgain = true;
        this.objeto.id_poliza = this.solicitudService.id_poliza;
        this.objeto.tipo_poliza = this.solicitudService.id_poliza;
        if (this.objeto.tipo_poliza == 3) {
            this.objeto.id_documento = 7;
        }
        if (this.objeto.tipo_poliza == 13) {
            this.objeto.id_documento = 34;
        }

        if (this.objeto.tipo_poliza === 4) {
            this.objeto.id_documento = 10;
        }
        this.objeto.id_poliza = this.solicitudService.id_poliza;
        this.instanciaPolizaService.GetAllByParametrosTarjetas(this.objeto).subscribe(res => {
            let response = res as { status: string, message: string, data: Instancia_poliza[], recordsTotal: number, recordsOffset: number, recordsFiltered: number };
            this.totalRows = response.recordsTotal ? response.recordsTotal : 0;
            this.objeto.first = response.recordsOffset ? response.recordsOffset : 0;
            this.objeto.rows = response.recordsFiltered ? this.totalRows >= response.recordsFiltered ? response.recordsFiltered : this.totalRows : this.totalRows > 10 ? 10 : this.totalRows;
            this.instancias_polizas = response.data;
            if (this.instancias_polizas.length >= 300) {
                this.showMoreResultsMessage = true;
            } else {
                this.showMoreResultsMessage = false;
            }
            this.PanelFiltrosColapsed = true;
            let lastEmptyRows = (this.totalRows % this.objeto.rows) - this.objeto.rows;
            lastEmptyRows = lastEmptyRows < 0 ? lastEmptyRows * (-1) : lastEmptyRows;
            let lastFilledRows = this.objeto.rows - lastEmptyRows;
            let rowsAfter = this.totalRows - lastFilledRows;
            rowsAfter = rowsAfter == this.totalRows ? rowsAfter - this.objeto.rows : rowsAfter;
            this.instancias_polizas = this.getCars(this.instancias_polizas);
            for (let i = 0; i < rowsAfter; i++) {
                this.instancias_polizas.push(new Instancia_poliza());
                let rowNumber = i + (this.objeto.first + 1);
                this.instancias_polizas[i].Nro = rowNumber > this.totalRows ? null : rowNumber;
            }
            if (this.totalRows - this.rowsPerPage == lastFilledRows) {
                this.rowsPerPage = this.objeto.first + lastFilledRows;
            } else {
                this.rowsPerPage = this.objeto.first + this.objeto.rows;
            }
            if (this.totalRows - this.objeto.first == lastFilledRows) {
                this.numRecords = lastFilledRows
            } else {
                this.numRecords = this.objeto.rows
            }
            if (this.instancias_polizas && this.instancias_polizas.length) {
                // if (this.instancias_polizas.length >= 300 && !this.usuarioLogin.rol.find(param => param.id == 5)) {
                //     this.displayExedioResultados = true;
                // }
            } else {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No se encontro ningun registro'
                });
            }
            this.solicitudService.isLoadingAgain = false;
            if (typeof callback == 'function') {
                callback();
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login'])
            } else {
                console.log(err);
            }
        });
    }

    devuelveValorAgenciaSucursal(agencia: any) {
        if (agencia) {
            let parametro = this.parametros.find(parametro => parametro.parametro_cod === agencia.valor);
            if (parametro) {
                return parametro.parametro_descripcion;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    devuelveValorParametroByCodAbreviacion(id: any, idDiccionario: number, ) {
        let parametro = this.parametros.find(parametro => parametro.parametro_cod == id && parametro.diccionario_id == idDiccionario);
        if (parametro) {
            return parametro.parametro_abreviacion;
        } else {
            return '';
        }
    }

    obtenerUltimoDocumento(documentos: any[]) {
        let nro_solicitud = "";
        documentos.forEach(documento => {
            nro_solicitud = documento.nro_documento;
        });
        return nro_solicitud;
    }

    filterAgencies(event) {
        if (event.value) {
            let value = event.value;
            let sucursalPadre = this.paramSucursales.find(param => param.parametro_cod == value);
            if (sucursalPadre) {
                let filteredAgencias = this.paramAgencias.filter(param => param.id_padre+'' == sucursalPadre.id+'');
                this.Agencias = filteredAgencias.map(param => {return {value: param.parametro_cod, label: param.parametro_descripcion}})
                this.Agencias.unshift({ label: "Seleccione Agencia", value: null });
            }
        }
    }

    listaAllByEstado() {
        this.instanciaPolizaService.listaAllByEstado({
            estado: this.estado,
            tipo_poliza: this.solicitudService.id_poliza,
            id_documento: this.id_documento
        }).subscribe(res => {
            let response = res as { status: string, message: string, data: Instancia_poliza[] };
            this.instancias_polizas = response.data;
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login'])
            } else {
                console.log(err);
            }
        });
    }

    devuelveValorParametro(id: any) {
        let parametro = this.parametros.find(parametro => parametro.id === id);
        if (parametro) {
            return parametro.parametro_descripcion;
        } else {
            return '';
        }
    }

    GetAllParametrosByIdDiccionarios() {
        //ids ponemos los id de los diccionarios que necesitemos
        let ids = [11, 1, 17, 18, 33, 38, 40, 54];
        // this.solicitudService.isLoadingAgain = true;
        this.parametrosService.GetAllParametrosByIdDiccionarios(ids).subscribe(res => {
            let response = res as { status: string, message: string, data: Parametro[] };
            this.parametros = response.data;
            this.solicitudService.isLoadingAgain = false;
            this.ProcedenciaCI.push({label: "Seleccione Procedencia", value: ''});
            this.parametros.filter(param => param.diccionario_id + '' === "18").forEach(element => {
                this.ProcedenciaCI.push({label: element.parametro_descripcion, value: element.parametro_cod});
            });

            this.FiltrosFechas.push({label: "Seleccione un tipo de fecha", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "54").forEach(element => {
                this.FiltrosFechas.push({label: element.parametro_descripcion, value: element.parametro_cod})
            });

            this.Estados_instancia_poliza.push({label: "Seleccione Estado Sol.", value: ''});
            this.parametros.filter(param => param.diccionario_id + '' === "11").forEach(element => {
                this.Estados_instancia_poliza.push({label: element.parametro_descripcion, value: element.id});
            });
            this.Estados.push({label: "Seleccione Estado", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "11").forEach(element => {
                this.Estados.push({label: element.parametro_descripcion, value: element.id});
            });
            this.Tipos_tarjetas.push({label: "Seleccione Tipo de Tar.", value: ''});
            this.parametros.filter(param => param.diccionario_id + '' === "33").forEach(element => {
                this.Tipos_tarjetas.push({label: element.parametro_descripcion, value: element.id});
            });
            this.Agencias.push({label: "Seleccione Agencia", value: ''});
            this.parametros.filter(param => param.diccionario_id + '' === "40").forEach(element => {
                this.Agencias.push({label: element.parametro_descripcion, value: element.parametro_cod});
                this.paramAgencias.push(element);
            });
            this.Sucursales.push({label: "Seleccione Surcursal", value: ''});
            this.parametros.filter(param => param.diccionario_id + '' === "38").forEach(element => {
                this.Sucursales.push({label: element.parametro_descripcion, value: element.parametro_cod});
                this.paramSucursales.push(element);
            });
            if (this.solicitudService.userInfo.usuario_banco) {
                if (this.solicitudService.userInfo.usuario_banco.us_sucursal) {
                    this.objeto.sucursal = this.solicitudService.userInfo.usuario_banco.us_sucursal + '';
                }
                if (this.solicitudService.userInfo.usuario_banco.us_oficina) {
                    this.objeto.agencia = this.solicitudService.userInfo.usuario_banco.us_oficina + '';
                }
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login'])
            } else {
                console.log(err);
            }
        });
    }
    exportExcel(dt: any, accept: boolean = false) {
        if (!accept && this.totalRows > 300) {
            this.displayExedioResultados = true
        }
        if (accept || this.totalRows <= 300) {
            this.objeto.rows = 300;
            this.objeto.first = 0;
            this.listaAll(dt, () => {
                let instan = null
                if (dt.filteredValue !== null) {
                    instan = dt.filteredValue;
                } else {
                    instan = this.instancias_polizas;
                }
                let instancia_poliza_excel = this.getCarsExcel(instan);
                const worksheet = xlsx.utils.json_to_sheet(instancia_poliza_excel);
                const workbook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
                const excelBuffer: any = xlsx.write(workbook, {bookType: 'xlsx', type: 'array'});
                this.solicitudService.isLoadingAgain = false;
                this.saveAsExcelFile(excelBuffer, "primengTable");
            });
        }
    }
    generarplanpago(id: any) {
        let pp = new Plan_pago();
        pp.id_instancia_poliza = id;
        pp.total_prima = 192;
        pp.interes = 0;
        pp.plazo_anos = 1;
        pp.periodicidad_anual = 12;
        pp.prepagable_postpagable = 1;
        pp.fecha_inicio = new Date();
        pp.adicionado_por = '2';
        pp.modificado_por = '2';
        this.planPagoService.GenerarPlanPagos(pp).subscribe(res => {
            let response = res as { status: string, message: string, data: any[] };
            if (response.status == 'ERROR') {

            } else {

            }
        }, err => console.error("ERROR llamando servicio plan de pagos:", err));
    }

    async GeneraAllPlanPagos(id: any) {
        await this.planPagoService.listaTodosSinPlanPagos().subscribe(async res => {
            let response = res as { status: string, message: string, data: any[] };
            for (let i = 0; i < response.data.length; i++) {
                let pp = new Plan_pago();
                pp.id_instancia_poliza = response.data[i].id;
                pp.total_prima = parseInt(response.data[i].monto) * parseInt(response.data[i].plazo);
                pp.interes = 0;
                pp.plazo_anos = 1;
                pp.periodicidad_anual = parseInt(response.data[i].monto);
                pp.prepagable_postpagable = 1;
                pp.fecha_inicio = response.data[i].fecha_emision;
                pp.adicionado_por = '4';
                pp.modificado_por = '4';
                await this.planPagoService.GenerarPlanPagos2(pp).then(res => {
                    let response = res as { status: string, message: string, data: any[] };
                    if (response.status == 'ERROR') {

                    } else {

                    }
                }, err => console.error("ERROR llamando servicio plan de pagos:", err));
            }
        }, err => console.error("ERROR llamando servicio plan de pagos:", err));
    }

    verPlanPagos(ins_pol: any) {
        this.sessionStorageService.setItemSync("paramsDeleted", false);
        this.menuService.activaRuteoMenu('66', null, {id_instancia_poliza: ins_pol.id}, 'PlanPago');
    }

    getCarsExcel(ins_poli: any) {
        let instancias = [];
        let c = 0;
        for (let instancia of ins_poli) {
            c++;
            let objeto = {};
            let docSolicitud, docCertificado;
            switch (this.solicitudService.id_poliza + '') {
                case '3':
                    docSolicitud = instancia.Fecha_Solicitud;
                    docCertificado = instancia.Fecha_Emision;
                    break;
                case '13':
                    docSolicitud = instancia.Fecha_Solicitud;
                    docCertificado = instancia.Fecha_Emision;
                    break;
                case '4':
                    docSolicitud = instancia.Fecha_Solicitud;
                    docCertificado = instancia.Fecha_Emision;
                    break;
            }

            objeto['NRO'] = instancia['Nro'];
            objeto['ID'] = instancia['id'];
            objeto['ID ESTADO'] = instancia['id_estado'];
            objeto['NRO SOL'] = instancia['Nro_Sol'];
            objeto['SUCURSAL'] = instancia['Sucursal'];
            objeto['AGENCIA'] = instancia['Agencia'];
            objeto['NRO CERTIFICADO'] = instancia['Nro_Certificado'];
            objeto['ESTADO'] = instancia['Estado'];
            objeto['FECHA REGISTRO'] = instancia['Fecha_Registro'];
            objeto['FECHA SOLICITUD'] = instancia['Fecha_Solicitud'];
            objeto['FECHA EMISION'] = instancia['Fecha_Emision'];
            objeto['ASEGURADO'] = instancia['Asegurado'];
            objeto['USUARIO LOGIN'] = instancia['usuario_login'];
            objeto['PLAN PAGOS'] = instancia['plan_pagos'];
            instancias.push(objeto);
        }

        return instancias;
    }


    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }

    // exportPdf() {
    //               const doc = new jsPDF.default(0,0);
    //               doc.autoTable(this.exportColumns, this.instancias_polizas);
    //               doc.save('primengTable.pdf');
    // }


    getCars(ins_poli: any) {
        let instancias = [];
        let c = 0;
        for (let i = 0; i < ins_poli.length; i++) {
            c++;
            let data = ins_poli[i];
            let objeto = data;
            let instancia = data;
            let docSolicitud, docCertificado, docPlanPago;
            switch (this.solicitudService.id_poliza + '') {
                case '3':
                    if (instancia.solicitudes && instancia.solicitudes.length) {
                        docSolicitud = instancia.solicitudes.find(param => param.id_documento == 7);
                    }
                    if (instancia.certificados && instancia.certificados.length) {
                        docCertificado = instancia.certificados.find(param => param.id_documento == 9);
                    }
                    if (instancia.plan_pagos && instancia.plan_pagos.length) {
                        docPlanPago = instancia.plan_pagos.pop();
                    }
                    break;
                case '4':
                    if (instancia.solicitudes && instancia.solicitudes.length) {
                        docSolicitud = instancia.solicitudes.find(param => param.id_documento == 10);
                    }
                    if (instancia.certificados && instancia.certificados.length) {
                        docCertificado = instancia.certificados.find(param => param.id_documento == 12);
                    }
                    if (instancia.plan_pagos && instancia.plan_pagos.length) {
                        docPlanPago = instancia.plan_pagos.pop();
                    }
                    break;
                case '13':
                    if (instancia.solicitudes && instancia.solicitudes.length) {
                        docSolicitud = instancia.solicitudes.find(param => param.id_documento == 34);
                    }
                    if (instancia.certificados && instancia.certificados.length) {
                        docCertificado = instancia.certificados.find(param => param.id_documento == 36);
                    }
                    if (instancia.plan_pagos && instancia.plan_pagos.length) {
                        docPlanPago = instancia.plan_pagos.pop();
                    }
                    break;
                // case '7':
                //     if (instancia.solicitudes && instancia.solicitudes.length) {
                //         docSolicitud = instancia.solicitudes.find(param => param.id_documento == 7);
                //     }
                //     if (instancia.certificados && instancia.certificados.length) {
                //         docCertificado = instancia.certificados.find(param => param.id_documento == 9);
                //     }
                //     break;
            }
            objeto['Nro'] = c;
            objeto['id'] = instancia.id;
            objeto['id_estado'] = instancia.id_estado;
            objeto['Nro_Sol'] = instancia.solicitudes && instancia.solicitudes.length ? instancia.solicitudes[0].nro_documento : '';
            objeto['Sucursal'] = this.devuelveValorAgenciaSucursal(instancia.sucursal);
            objeto['Agencia'] = this.devuelveValorAgenciaSucursal(instancia.agencia);
            objeto['Nro_Certificado'] = instancia.certificados && instancia.certificados.length ? this.obtenerUltimoDocumento(instancia.certificados) : '';
            objeto['Estado'] = this.devuelveValorParametro(instancia.id_estado);
            objeto['Fecha_Registro'] = this.util.formatoFecha(instancia.fecha_registro + '');
            objeto['Fecha_Solicitud'] = docSolicitud ? this.util.formatoFecha(docSolicitud.fecha_emision + '') : null;
            objeto['Fecha_Emision'] = docCertificado ? this.util.formatoFecha(docCertificado.fecha_emision + '') : null;
            objeto['Asegurado'] = instancia.asegurados[0].entidad.persona.persona_primer_nombre + ' ' + instancia.asegurados[0].entidad.persona.persona_primer_apellido + ' ' + instancia.asegurados[0].entidad.persona.persona_segundo_apellido;
            objeto['usuario_login'] = instancia.usuario.usuario_login ? instancia.usuario.usuario_login : '';
            objeto['plan_pagos'] = docPlanPago;
            instancias.push(objeto);
        }

        return instancias;
    }

    onPageAction(event, dt) {
        console.log(event)
        this.objeto.first = event.first;
        this.objeto.rows = event.rows;
        this.listaAll(dt);
    }

    onSearchAction(event, dt) {
        console.log(event)
        this.objeto.first = 0;
        this.objeto.rows = event.rows;
        this.listaAll(dt);
    }


    listaAllById() {

    }

    EditarPoliza(id: any) {
        this.solicitudService.isLoadingAgain = true;
        if (this.solicitudService.id_poliza == 4) {
            this.menuService.activaRuteoMenu('12', 4, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaTarjetaDebito', this.solicitudService.componentesInvisibles);
        }
        if (this.solicitudService.id_poliza == 3) {
            this.menuService.activaRuteoMenu('13', 3, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaTarjetaCredito', this.solicitudService.componentesInvisibles);
        }
        if (this.solicitudService.id_poliza == 13) {
            this.menuService.activaRuteoMenu('83', 13, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaTarjetaCredito', this.solicitudService.componentesInvisibles);
        }
        //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);
    }

    VerPoliza(id: any) {
        if (this.solicitudService.id_poliza == 4) {
            this.menuService.activaRuteoMenu('26', 4, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaTarjetaDebito', this.solicitudService.componentesInvisibles);
        }
        if (this.solicitudService.id_poliza == 3) {
            this.menuService.activaRuteoMenu('27', 3, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaTarjetaCredito', this.solicitudService.componentesInvisibles);
        }
        if (this.solicitudService.id_poliza == 13) {
            this.menuService.activaRuteoMenu('86', 13, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaTarjetaCredito', this.solicitudService.componentesInvisibles);
        }
        //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);
    }

    EliminarPoliza() {

    }

    ImprimirSolicitud(id_intancia_poliza: any) {
        let nombre_archivo = 'SoliTarCre' + id_intancia_poliza;
        this.reporteService.ReporteSolicitudTarjeta(id_intancia_poliza, nombre_archivo).subscribe(res => {
                this.reporteService.cargarPagina(nombre_archivo);
            }
            , err => {
                if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                    this.router.navigate(['login'])
                } else {
                    console.log(err);
                }
            });
    }

    ImprimirCertificado(id_intancia_poliza: any) {
        let nombre_archivo = 'CertTarCre' + id_intancia_poliza;
        this.reporteService.ReporteCertificadoTarjeta(id_intancia_poliza, nombre_archivo).subscribe(res => {
                this.reporteService.cargarPagina(nombre_archivo);
            }
            , err => {
                if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                    this.router.navigate(['login'])
                } else {
                    console.log(err);
                }
            });
    }

    ValidarEstados(id_estado: any, estados: any[]) {
        if (estados.find(param => param == '*')) {
            return true;
        } else {
            if (estados.includes(parseInt(id_estado + ''))) {
                return true;
            } else {
                return false;
            }
        }
    }

    verifyRol(estadosAvailable: any[], rolsAvailable: any[], instanciaPoliza: Instancia_poliza) {
        let includesRol = false;
        for (let i = 0; i < this.solicitudService.userInfo.usuarioRoles.length; i++) {
            let rol = this.solicitudService.userInfo.usuarioRoles[i];
            if (rolsAvailable.find(param => param == '*')) {
                includesRol = true;
                break;
            } else {
                if (rolsAvailable.includes(parseInt(rol.id + ''))) {
                    includesRol = true;
                }
            }
        }
        if (this.ValidarEstados(instanciaPoliza.id_estado, estadosAvailable) && includesRol) {
            return true;
        }
        return false;
    }

    ValidarComponentesInvisible(id: any) {
        if (this.solicitudService.componentesInvisibles != null) {
            if (this.solicitudService.componentesInvisibles.find(params => params.codigo === id && params.estado === 'A')) {
                return true;
            } else {
                if (this.solicitudService.componentesInvisibles.find(params => params.codigo === id && params.estado === 'I')) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    limpiar() {
        this.instancias = [];
        this.instancias_polizas = [];
        this.poliza = new Poliza();
        this.objeto =  new Filtro();
    }

    devuelveValorSucursal(agencia: any) {
        if (agencia) {
            let parametro = this.Sucursales.find(parametro => parametro.value + '' === agencia + '');
            if (parametro) {
                return parametro.label;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    devuelveValorAgencia(agencia: any) {
        if (agencia) {
            let parametro = this.Agencias.find(parametro => parametro.value + '' === agencia + '');
            if (parametro) {
                return parametro.label;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
}
