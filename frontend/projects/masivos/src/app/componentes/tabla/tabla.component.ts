import { Componente } from '../../../../../../src/core/modelos/componente';
import { Car } from '../../../../../../src/core/modelos/car';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {BreadcrumbService} from '../../../../../../src/core/servicios/breadcrumb.service';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})
export class TablaComponent implements OnInit {

  componentesInvisibles: Componente[];
  ruta:string;
  caros: Car[]=[];
  cols: any[];
  selectedCar: Car;
  
  constructor(private params: ActivatedRoute,private breadcrumbService: BreadcrumbService) {
    this.params.paramMap.subscribe(params => {
      this.componentesInvisibles = JSON.parse(params.get('ComponentesInvisibles'));
      this.ruta=params.get('ruta');
    });

    this.breadcrumbService.setItems([
      {label: this.ruta}
    ]);
  }

  ValidarComponentes(id:any){
    if(this.componentesInvisibles.find(params=>params.codigo=id)){
      return false;
    }else{
      return true;
    }
  }

  ngOnInit() {
    this.cols = [
      { field: 'codigo', header: 'codigo' },
      { field: 'marca', header: 'Marca' },
      { field: 'anio', header: 'año' },
      { field: 'color', header: 'Color' }
    ];

    let caro1={anio:2020,codigo:"fafafa",marca:"zeta",color:"azul"}
    let caro2={anio:2010,codigo:"24512",marca:"toshiba",color:"verde"}
    let caro3={anio:2010,codigo:"25",marca:"toshiba",color:"verde"}
    let caro4={anio:2010,codigo:"43",marca:"toshiba",color:"verde"}
    let caro5={anio:2010,codigo:"65",marca:"toshiba",color:"verde"}
    let caro6={anio:2010,codigo:"76",marca:"toshiba",color:"verde"}
    let caro7={anio:2010,codigo:"11",marca:"toshiba",color:"verde"}
    let caro8={anio:2010,codigo:"22",marca:"toshiba",color:"verde"}
    let caro9={anio:2010,codigo:"33",marca:"toshiba",color:"verde"}
    let caro10={anio:2010,codigo:"44",marca:"toshiba",color:"verde"}
    let caro11={anio:2010,codigo:"55",marca:"toshiba",color:"verde"}
    let caro12={anio:2010,codigo:"66",marca:"toshiba",color:"verde"}
    this.caros.push(caro1);
    this.caros.push(caro2);
    this.caros.push(caro3);
    this.caros.push(caro4);
    this.caros.push(caro5);
    this.caros.push(caro6);
    this.caros.push(caro7);
    this.caros.push(caro8);
    this.caros.push(caro9);
    this.caros.push(caro10);
    this.caros.push(caro11);
    this.caros.push(caro12);
  }
}
