import { User } from '../../../../../../src/core/modelos/user';
import { MessageService } from 'primeng/api';
import { LoginService } from '../../../../../../src/core/servicios/login.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Usuario} from "../../../../../../src/core/modelos/usuario";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  User: User;
  formulario: boolean = true;
  formLogin: FormGroup;
  usuario: Usuario;
  msgError: string;

  constructor(private router: Router,
    private loginService: LoginService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.User = new User();
    this.messageService.add({severity:'success', summary: 'Success Message', detail:'Order submitted'});
  }

  async Session() {
    this.loginService.cerrarSesion();
    this.loginService.login(this.User.userName, this.User.password).subscribe(async res => {
          //this.formulario=false;
          //await this.delay(1500);
          this.usuario = res['user'] as Usuario;
          if (this.usuario.usuario_banco && this.usuario.usuario_banco.us_sucursal && this.usuario.usuario_banco.us_oficina) {
            this.loginService.setUserInfo(res['user']);
            this.router.navigate(['/AppMain']);
            this.messageService.add({severity:'success', summary: 'Success Message', detail:'Order submitted'});
          } else {
            this.msgError = `No se pudo identificar la sucursal o agencia de su usuario: ${this.usuario.usuario_login}, por favor conactese con soporte del banco.`;
            this.messageService.clear();
            this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:this.msgError, detail:''});
          }
        }, err => {
          if(err.error.message){
            this.messageService.clear();
            this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:err.error.message.message, detail:''});
          }else{
            this.messageService.clear();
            this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:err.message, detail:''});
          }
        });
  }

  cerrarSesion() {
    localStorage.removeItem('userInfo');
    sessionStorage.removeItem('userInfo');
    this.loginService.usuario = new Usuario();
    this.loginService.cerrarSesion().then(async (response) => {
      this.router.navigate(['']);
    });
  }

  onReject() {
    this.messageService.clear('c');
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  Registrar(){
    this.loginService.prueba().then(async ( response) => {
      console.log(response);
    });
  }

}
