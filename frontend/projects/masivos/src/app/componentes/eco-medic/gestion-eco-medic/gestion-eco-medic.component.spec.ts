import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionEcoMedicComponent } from './gestion-eco-medic.component';

describe('GestionEcoMedicComponent', () => {
  let component: GestionEcoMedicComponent;
  let fixture: ComponentFixture<GestionEcoMedicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionEcoMedicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionEcoMedicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
