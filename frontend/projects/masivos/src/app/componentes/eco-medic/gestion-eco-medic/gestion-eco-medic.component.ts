import {PlanPagoService} from '../../../../../../../src/core/servicios/plan-pago.service';
import {PersonaService} from '../../../../../../../src/core/servicios/persona.service';
import {SolicitudService} from '../../../../../../../src/core/servicios/solicitud.service';
import {MessageService} from 'primeng/api';
import {Anexo_poliza} from '../../../../../../../src/core/modelos/anexo_poliza';
import {AnexoService} from '../../../../../../../src/core/servicios/anexo.service';
import {Poliza} from '../../../../../../../src/core/modelos/poliza';
import {ReporteService} from '../../../../../../../src/core/servicios/reporte.service';
import {MenuService} from '../../../../../../../src/core/servicios/menu.service';
import {InstanciaPolizaService} from '../../../../../../../src/core/servicios/instancia-poliza.service';
import {ParametrosService} from '../../../../../../../src/core/servicios/parametro.service';
import {BreadcrumbService} from '../../../../../../../src/core/servicios/breadcrumb.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Asegurado} from '../../../../../../../src/core/modelos/asegurado';
import {SelectItem} from 'primeng';
import {Componente, Filtro} from '../../../../../../../src/core/modelos/componente';
import {Parametro} from '../../../../../../../src/core/modelos/parametro';
import {Instancia_poliza} from '../../../../../../../src/core/modelos/instancia_poliza';
import {Component, OnInit, ViewChild} from '@angular/core';
import * as xlsx from 'xlsx';
import * as FileSaver from 'file-saver';
import {SessionStorageService} from "../../../../../../../src/core/servicios/sessionStorage.service";
import {Util} from "../../../../../../../src/helpers/util";
import {ActualizarSolicitudComponent} from "../../../../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component";
import {Instancia_documento} from "../../../../../../../src/core/modelos/instancia_documento";
declare var $:any;
@Component({
    selector: 'app-gestion-eco-medic',
    templateUrl: './gestion-eco-medic.component.html',
    styleUrls: ['./gestion-eco-medic.component.css']
})

export class GestionEcoMedicComponent implements OnInit {

    color = 'primary';
    mode = 'indeterminate';
    value = 1;
    componentesInvisibles: Componente[];
    componentesEditables: Componente[];
    ruta: string;
    parametros: Parametro[] = [];
    instancias_polizas: Instancia_poliza[] = [];
    instancias: any[] = [];
    poliza = new Poliza();
    cols: any;
    objeto: Filtro = new Filtro();
    paramAgencias: Parametro[] = [];
    paramSucursales: Parametro[] = [];
    id_instancias: any;
    filtros: boolean = true;
    ProcedenciaCI: SelectItem[] = [];
    FiltrosFechas: SelectItem[] = [];
    Agencias: SelectItem[] = [];
    Estados_instancia_poliza: SelectItem[] = [];
    Tipos_tarjetas: SelectItem[] = [];
    Anexos_polizas: SelectItem[] = [];
    Sucursales: SelectItem[] = [];
    Estados: SelectItem[] = [];
    TiposPago: SelectItem[] = [];
    id_poliza;
    estado: any;
    id_documento: any;
    usuarioLogin: any;
    vista: any;
    parametrosRuteo: any;
    util = new Util();
    displayExedioResultados: boolean = false;
    showMoreResultsMessage: boolean = false;
    PanelFiltrosColapsed: boolean = false;
    anexo_polizas: any[] = [];
    btnEmitir: boolean = false;
    btnEditar: boolean = false;
    btnVer1: boolean = false;
    btnVer2: boolean = false;
    index_instancia_poliza: number;
    exportColumns: any[];
    rowsPerPage: number = 0;
    numRecords: number = 0;
    totalRows: number = 0;

    @ViewChild('componenteActualizarSolicitud', {static: false}) private componenteActualizarSolicitud: ActualizarSolicitudComponent;

    constructor(private params: ActivatedRoute, private breadcrumbService: BreadcrumbService,
                private anexoService: AnexoService,
                private parametrosService: ParametrosService,
                private instanciaPolizaService: InstanciaPolizaService,
                private router: Router,
                private menuService: MenuService,
                private personaService: PersonaService,
                private reporteService: ReporteService,
                private service: MessageService,
                public solicitudService: SolicitudService,
                public sessionStorageService: SessionStorageService,
                private planPagoService: PlanPagoService) {
        this.usuarioLogin = this.sessionStorageService.getItemSync('userInfo');
        this.parametrosRuteo = this.sessionStorageService.getItemSync('parametros');
        this.vista = this.menuService.ObtenerParametrosVista(this.params.snapshot.paramMap.get('id'));
        if (this.vista) {
            this.vista.componentes = [];
            this.componentesInvisibles = this.vista.componentes;
            this.ruta = this.vista.ruta;
            this.breadcrumbService.setItems([
                {label: this.ruta}
            ]);
        }

        this.cols = [
            {field: 'Nro', header: 'Nro', width: '5%'},
            {field: 'Nro_Sol', header: 'Nro. Sol.', width: '10%'},
            {field: 'Sucursal', header: 'Sucursal', width: '8%'},
            {field: 'Agencia', header: 'Agencia', width: '10%'},
            {field: 'Nro_Certificado', header: 'Nro. Certificado', width: '8%'},
            {field: 'Estado', header: 'Estado', width: '10%'},
            {field: 'Fecha_Registro', header: 'Fecha Registro', width: '10%'},
            {field: 'Fecha_Solicitud', header: 'Fecha Solicitud', width: '10%'},
            {field: 'Fecha_Emision', header: 'Fecha Emisión', width: '10%'},
            {field: 'Tipo_Pago', header: 'Tipo de Pago', width: '10%'},
            {field: 'Asegurado', header: 'Asegurado', width: '19%'},
            {field: 'usuario', header: 'usuario', width: '10%'},
            {field: null, header: 'Acciones', width: '10%'}
        ];

        this.exportColumns = this.cols.map(col => ({title: col.header, dataKey: col.field}));
    }

    ngOnInit() {
        this.solicitudService.constructComponent(() => {
            this.breadcrumbService.setItems([
                {label: this.solicitudService.ruta}
            ]);
            this.objeto = this.solicitudService.parametrosRuteo.parametro_ruteo && this.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? this.solicitudService.parametrosRuteo.parametro_ruteo.filtros : this.objeto;
            this.objeto.id_poliza = parseInt(this.solicitudService.parametrosRuteo.parametro_vista+'') ? parseInt(this.solicitudService.parametrosRuteo.parametro_vista+'') : this.solicitudService.id_poliza;
            this.solicitudService.product = this.solicitudService.ruta;
            this.solicitudService.isInAltaSolicitud = false;
            this.solicitudService.isLoadingAgain = false;
            if (this.vista && this.vista.parametros2 !== null && (this.vista.id === '30')) {
                let param = JSON.parse(this.vista.parametros2);
                this.id_instancias = this.vista.parametros;
                this.estado = param.estado;
                this.id_poliza = param.id_poliza;
                this.id_documento = param.id_documento;
                this.listaAllByEstado();
            }
            this.GetAllParametrosByIdDiccionarios();
            if (this.solicitudService.parametrosRuteo.parametro_ruteo && Object.keys(this.solicitudService.parametrosRuteo.parametro_ruteo).length) {
                this.id_instancias = this.solicitudService.parametrosRuteo.parametro_ruteo;
                this.listaAll();
            } else {
                this.PanelFiltrosColapsed = false;
            }
            this.listaAnexoPolizas();
        });
    }

    filterAgencies(event) {
        if (event.value) {
            let value = event.value;
            let sucursalPadre = this.paramSucursales.find(param => param.parametro_cod == value);
            if (sucursalPadre) {
                let filteredAgencias = this.paramAgencias.filter(param => param.id_padre+'' == sucursalPadre.id+'');
                this.Agencias = filteredAgencias.map(param => {return {value: param.parametro_cod, label: param.parametro_descripcion}})
                this.Agencias.unshift({ label: "Seleccione Agencia", value: null });
            }
        }
    }

    listaAllByIds() {
        this.instanciaPolizaService.GetAllByIdsEcoMedic(this.id_instancias).subscribe(async res => {
            let response = res as { status: string, message: string, data: Instancia_poliza[], recordsTotal: number, recordsOffset: number, recordsFiltered: number };
            this.instancias_polizas = response.data;
            this.totalRows = response.recordsTotal ? response.recordsTotal : 0;
            this.objeto.first = response.recordsOffset ? response.recordsOffset : 0;
            this.objeto.rows = response.recordsFiltered ? this.totalRows >= response.recordsFiltered ? response.recordsFiltered : this.totalRows : this.totalRows > 10 ? 10 : this.totalRows;
            this.instancias_polizas = response.data;
            if (this.instancias_polizas.length >= 300) {
                this.showMoreResultsMessage = true;
            } else {
                this.showMoreResultsMessage = false;
            }
            this.instancias_polizas = this.getCars(this.instancias_polizas);
            let lastEmptyRows = (this.totalRows % this.objeto.rows) - this.objeto.rows;
            lastEmptyRows = lastEmptyRows < 0 ? lastEmptyRows * (-1) : lastEmptyRows;
            let lastFilledRows = this.objeto.rows - lastEmptyRows;
            let rowsAfter = this.totalRows - lastFilledRows;
            for (let i = 0; i < rowsAfter; i++) {
                this.instancias_polizas.push(new Instancia_poliza());
                let rowNumber = i + (this.objeto.first + 1);
                this.instancias_polizas[i].Nro = rowNumber > this.totalRows ? null : rowNumber;
            }
            if (this.totalRows - this.rowsPerPage == lastFilledRows) {
                this.rowsPerPage = this.objeto.first + lastFilledRows;
            } else {
                this.rowsPerPage = this.objeto.first + this.objeto.rows;
            }
            if (this.totalRows - this.objeto.first == lastFilledRows) {
                this.numRecords = lastFilledRows
            } else {
                this.numRecords = this.objeto.rows
            }
            if (this.instancias_polizas && this.instancias_polizas.length) {
                if (this.instancias_polizas.length >= 300 && !this.usuarioLogin.rol.find(param => param.id == 5)) {
                    this.displayExedioResultados = true;
                }
            } else {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No se encontro ningun registro'
                });
                this.poliza = new Poliza();
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }


    listaAll(dt: any = null, callback: any = null) {
        if (dt) {
            dt.reset();
        }
        setTimeout(() => {
            if ($(window).width() < 720) {
                $('.ui-table').css('overflow','scroll');
                $('.ui-table table').css('width','400%');
            }
        },2000);
        this.instancias = [];
        if(this.objeto && this.objeto.id_instancia_poliza) {
            this.objeto = new Filtro();
            this.objeto.id_instancia_poliza = this.parametrosRuteo.parametro_ruteo.filtros.id_instancia_poliza;
        } else {
            this.objeto = this.parametrosRuteo.parametro_ruteo && this.parametrosRuteo.parametro_ruteo.filtros ? this.parametrosRuteo.parametro_ruteo.filtros : this.objeto;
        }
        if (!this.objeto.agencia) {
            this.objeto.agencia_required = false;
        }
        if (!this.objeto.sucursal_required) {
            this.objeto.sucursal_required = false;
        }
        this.solicitudService.isLoadingAgain = true;
        this.objeto.id_poliza = this.solicitudService.id_poliza;
        this.instanciaPolizaService.GetAllByParametrosEcoMedic(this.objeto).subscribe(res => {
            let response = res as { status: string, message: string, data: Instancia_poliza[], recordsTotal: number, recordsOffset: number, recordsFiltered: number };
            this.totalRows = response.recordsTotal ? response.recordsTotal : 0;
            this.objeto.first = response.recordsOffset ? response.recordsOffset : 0;
            this.objeto.rows = response.recordsFiltered ? this.totalRows >= response.recordsFiltered ? response.recordsFiltered : this.totalRows : this.totalRows > 10 ? 10 : this.totalRows;
            this.instancias_polizas = response.data;
            this.instancias_polizas = response.data;
            if (this.instancias_polizas.length >= 300) {
                this.showMoreResultsMessage = true;
            } else {
                this.showMoreResultsMessage = false;
            }
            this.instancias_polizas = this.getCars(this.instancias_polizas);
            let lastEmptyRows = (this.totalRows % this.objeto.rows) - this.objeto.rows;
            lastEmptyRows = lastEmptyRows < 0 ? lastEmptyRows * (-1) : lastEmptyRows;
            let lastFilledRows = this.objeto.rows - lastEmptyRows;
            let rowsAfter = this.totalRows - lastFilledRows;
            rowsAfter = rowsAfter == this.totalRows ? rowsAfter - this.objeto.rows : rowsAfter;
            for (let i = 0; i < rowsAfter; i++) {
                this.instancias_polizas.push(new Instancia_poliza());
                let rowNumber = i + (this.objeto.first + 1);
                this.instancias_polizas[i].Nro = rowNumber > this.totalRows ? null : rowNumber;
            }
            if (this.totalRows - this.rowsPerPage == lastFilledRows) {
                this.rowsPerPage = this.objeto.first + lastFilledRows;
            } else {
                this.rowsPerPage = this.objeto.first + this.objeto.rows;
            }
            if (this.totalRows - this.objeto.first == lastFilledRows) {
                this.numRecords = lastFilledRows
            } else {
                this.numRecords = this.objeto.rows
            }
            if (this.instancias_polizas && this.instancias_polizas.length) {
                // if (this.instancias_polizas.length >= 300 && !this.usuarioLogin.rol.find(param => param.id == 5)) {
                //     this.displayExedioResultados = true;
                // }
            } else {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No se encontro ningun registro'
                });
            }
            this.solicitudService.isLoadingAgain = false;
            if (typeof callback == 'function') {
                callback();
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    onPageAction(event, dt) {
        console.log(event)
        this.objeto.first = event.first;
        this.objeto.rows = event.rows;
        this.listaAll(dt);
    }

    onSearchAction(event, dt) {
        this.objeto.first = 0;
        this.objeto.rows = event.rows;
        this.listaAll(dt);
    }

    listaAllByEstado() {
        this.instanciaPolizaService.listaAllByEstado({
            estado: this.estado,
            tipo_poliza: this.solicitudService.id_poliza,
            id_documento: this.id_documento
        }).subscribe(res => {
            let response = res as { status: string, message: string, data: Poliza };
            this.poliza = response.data;
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    devuelveValorParametro(id: any) {
        let parametro = this.parametros.find(parametro => parametro.id === id);
        if (parametro) {
            return parametro.parametro_descripcion;
        } else {
            return '';
        }
    }

    devuelveValorParametroByCodAbreviacion(id: any, idDiccionario: number, ) {
        let parametro = this.parametros.find(parametro => parametro.parametro_cod == id && parametro.diccionario_id == idDiccionario);
        if (parametro) {
            return parametro.parametro_abreviacion;
        } else {
            return '';
        }
    }

    GetAllParametrosByIdDiccionarios() {
        //ids ponemos los id de los diccionarios que necesitemos
        let ids = [11, 1, 17, 18, 33, 38, 40, 38, 54, 21];
        this.solicitudService.isLoadingAgain = true;
        this.parametrosService.GetAllParametrosByIdDiccionarios(ids).subscribe(res => {
            let response = res as { status: string, message: string, data: Parametro[] };
            this.solicitudService.isLoadingAgain = false;
            this.parametros = response.data;
            this.ProcedenciaCI.push({label: "Seleccione Procedencia", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "18").forEach(element => {
                this.ProcedenciaCI.push({label: element.parametro_descripcion, value: element.parametro_cod});
            });
            this.FiltrosFechas.push({label: "Seleccione un tipo de fecha", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "54").forEach(element => {
                this.FiltrosFechas.push({label: element.parametro_descripcion, value: element.parametro_cod});
            });
            this.Agencias.push({label: "Seleccione Agencia", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "40").forEach(element => {
                this.Agencias.push({label: element.parametro_descripcion, value: element.parametro_cod});
                this.paramAgencias.push(element);
            });

            this.Sucursales.push({label: "Seleccione Sucursal", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "38").forEach(element => {
                this.Sucursales.push({label: element.parametro_descripcion, value: element.parametro_cod});
                this.paramSucursales.push(element);
            });
            this.Estados.push({label: "Seleccione Estado", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "11").forEach(element => {
                this.Estados.push({label: element.parametro_descripcion, value: element.id});
            });

            this.TiposPago.push({label: "Seleccione un tipo de Pago", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "21").forEach(element => {
                this.TiposPago.push({label: element.parametro_descripcion, value: element.id});
            });

            if (this.usuarioLogin) {
                if (this.usuarioLogin.usuario_banco) {
                    if (this.usuarioLogin.usuario_banco.us_sucursal) {
                        this.objeto.sucursal = this.usuarioLogin.usuario_banco.us_sucursal + '';
                    }
                    if (this.usuarioLogin.usuario_banco.us_oficina) {
                        this.objeto.agencia = this.usuarioLogin.usuario_banco.us_oficina + '';
                    }
                }
            } else {
                this.router.navigate(['login']);
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }
    exportExcel(dt: any, accept: boolean = false) {
        if (!accept && this.totalRows > 300) {
            this.displayExedioResultados = true
        }
        if (accept || this.totalRows <= 300) {
            this.objeto.rows = 300;
            this.objeto.first = 0;
            this.listaAll(dt, () => {
                let instan = null;
                if (dt.filteredValue !== null) {
                    instan = dt.filteredValue;
                } else {
                    instan = this.instancias_polizas;
                }
                let instancia_poliza_excel = this.getCarsExcel(instan);
                const worksheet = xlsx.utils.json_to_sheet(instancia_poliza_excel);
                const workbook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
                const excelBuffer: any = xlsx.write(workbook, {bookType: 'xlsx', type: 'array'});
                this.solicitudService.isLoadingAgain = false;
                this.saveAsExcelFile(excelBuffer, "primengTable");
            });
        }
    }

    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }

    // exportPdf() {
    //     const doc = new jsPDF.default(0, 0);
    //     doc.autoTable(this.exportColumns, this.poliza.instancia_polizas);
    //     doc.save('primengTable.pdf');
    // }

    devuelveValorAgenciaSucursal(agencia: any) {
        if (agencia) {
            let parametro = this.parametros.find(parametro => parametro.parametro_cod === agencia.valor);
            if (parametro) {
                return parametro.parametro_descripcion;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    obtenerUltimoDocumento(documentos: any[]) {
        let nro_solicitud = "";
        documentos.forEach(documento => {
            nro_solicitud = documento.nro_documento;
        });
        return nro_solicitud;
    }

    getCarsExcel(ins_poli: any) {
        let instancias = [];
        let c = 0;
        for (let instancia of ins_poli) {
            c++;
            let objeto = {};
            let docSolicitud, docCertificado;
            switch (this.solicitudService.id_poliza + '') {
                case '5':
                    docSolicitud = instancia.Fecha_Solicitud;
                    docCertificado = instancia.Fecha_Emision;
                    break;
                case '8':
                    docSolicitud = instancia.Fecha_Solicitud;
                    docCertificado = instancia.Fecha_Emision;
                    break;
                case '14':
                    docSolicitud = instancia.Fecha_Solicitud;
                    docCertificado = instancia.Fecha_Emision;
                    break;
            }

            objeto['NRO']  = instancia['Nro'];
            objeto['ID']  = instancia['id'];
            objeto['ID ESTADO']  = instancia['id_estado'];
            objeto['NRO SOL']  = instancia['Nro_Sol'];
            objeto['SUCURSAL']  = instancia['Sucursal'];
            objeto['AGENCIA']  = instancia['Agencia'];
            objeto['NRO CERTIFICADO'] = instancia['Nro_Certificado'];
            objeto['ESTADO'] = instancia['Estado'];
            objeto['FECHA REGISTRO'] = instancia['Fecha_Registro'];
            objeto['FECHA SOLICITUD'] = instancia['Fecha_Solicitud'];
            objeto['FECHA EMISION'] = instancia['Fecha_Emision'];
            objeto['TIPO PAGO'] = instancia['Tipo_Pago'];
            objeto['ID TIPO PAGO'] = instancia['id_tipo_pago'];
            objeto['ASEGURADO'] = instancia['Asegurado'];
            objeto['USUARIO LOGIN'] = instancia['usuario_login'];

            instancias.push(objeto);
        }
        return instancias;
    }

    getCars(ins_poli: any) {
        let instancias = [];
        let c = 0;
        for (let i = 0; i < ins_poli.length; i++) {
            c++;
            let data = ins_poli[i];
            let objeto = data;
            let instancia = data;
            let docSolicitud, docCertificado;
            switch (this.solicitudService.id_poliza + '') {
                case '5':
                    if (instancia.solicitudes && instancia.solicitudes.length) {
                        docSolicitud = instancia.solicitudes.find(param => param.id_documento == 13);
                    }
                    if (instancia.certificados && instancia.certificados.length) {
                        docCertificado = instancia.certificados.find(param => param.id_documento == 15);
                    }
                    break;
                case '8':
                    if (instancia.solicitudes && instancia.solicitudes.length) {
                        docSolicitud = instancia.solicitudes.find(param => param.id_documento == 22);
                    }
                    if (instancia.certificados && instancia.certificados.length) {
                        docCertificado = instancia.certificados.find(param => param.id_documento == 24);
                    }
                    break;
                case '14':
                    if (instancia.solicitudes && instancia.solicitudes.length) {
                        docSolicitud = instancia.solicitudes.find(param => param.id_documento == 39);
                    }
                    if (instancia.certificados && instancia.certificados.length) {
                        docCertificado = instancia.certificados.find(param => param.id_documento == 41);
                    }
                    break;
            }
            objeto['Nro'] = c;
            objeto['id'] = instancia.id;
            objeto['id_estado'] = instancia.id_estado;
            objeto['Nro_Sol'] = instancia.solicitudes && instancia.solicitudes.length ? instancia.solicitudes[0].nro_documento : '';
            objeto['Sucursal'] = this.devuelveValorAgenciaSucursal(instancia.sucursal);
            objeto['Agencia'] = this.devuelveValorAgenciaSucursal(instancia.agencia);
            objeto['Nro_Certificado'] = instancia.certificados && instancia.certificados.length ? this.obtenerUltimoDocumento(instancia.certificados) : '';
            objeto['Estado'] = this.devuelveValorParametro(instancia.id_estado);
            objeto['Fecha_Registro'] = this.util.formatoFecha(instancia.fecha_registro + '');
            objeto['Fecha_Solicitud'] = docSolicitud ? this.util.formatoFecha(docSolicitud.fecha_emision + '') : null;
            objeto['Fecha_Emision'] = docCertificado ? this.util.formatoFecha(docCertificado.fecha_emision + '') : null;
            objeto['Tipo_Pago'] = instancia.tipo_pago ? this.devuelveValorParametro(instancia.tipo_pago.valor) : '';
            objeto['id_tipo_pago'] = instancia.tipo_pago ? instancia.tipo_pago.valor : '';
            objeto['Asegurado'] = instancia.asegurados[0].entidad.persona.persona_primer_nombre + ' ' + instancia.asegurados[0].entidad.persona.persona_primer_apellido + ' ' + instancia.asegurados[0].entidad.persona.persona_segundo_apellido;
            objeto['usuario_login'] = instancia.usuario.usuario_login ? instancia.usuario.usuario_login : '';
            instancias.push(objeto);
        }
        return instancias;
    }

    ValidarEstados(id_estado: any, estados: any[]) {
        if (estados.find(param => param == '*')) {
            return true;
        } else {
            if (estados.includes(parseInt(id_estado + ''))) {
                return true;
            } else {
                return false;
            }
        }
    }

    verifyRol(estadosAvailable: any[], rolsAvailable: any[], instanciaPoliza: Instancia_poliza) {
        let includesRol = false;
        for (let i = 0; i < this.usuarioLogin.usuarioRoles.length; i++) {
            let rol = this.usuarioLogin.usuarioRoles[i];
            if (rolsAvailable.find(param => param == '*')) {
                includesRol = true;
                break;
            } else {
                if (rolsAvailable.includes(parseInt(rol.id + ''))) {
                    includesRol = true;
                }
            }
        }
        if (this.ValidarEstados(instanciaPoliza.id_estado, estadosAvailable) && includesRol) {
            return true;
        }
        return false;
    }

    ValidarTipoPago(id_tipo_pago: any) {
        if (id_tipo_pago == 58) {
            return true;
        } else {
            return false;
        }
    }

    EditarPoliza(id: any) {
        if (this.solicitudService.id_poliza == 5) {
            this.menuService.activaRuteoMenu('30', 5, {id_instancia_poliza: id, filtros: this.objeto}, 'AltaEcoMedic', this.solicitudService.componentesInvisibles);
        }
        if (this.solicitudService.id_poliza == 8) {
            this.menuService.activaRuteoMenu('54', 8, {id_instancia_poliza: id, filtros: this.objeto}, 'AltaEcoMedicPlus', this.solicitudService.componentesInvisibles);
        }    //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);
        if (this.solicitudService.id_poliza == 14) {
            this.menuService.activaRuteoMenu('90', 14, {id_instancia_poliza: id, filtros: this.objeto}, 'AltaEcoEstudio', this.solicitudService.componentesInvisibles);
        }    //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);
    }

    listaAnexoPolizas() {
        this.anexoService.findAllAnexoPolizaByIdPoliza(this.solicitudService.id_poliza).subscribe(async res => {
            let response = res as { status: string, message: string, data: Anexo_poliza[] };
            this.anexo_polizas = response.data;
            this.Tipos_tarjetas.push({label: "Seleccione Plan", value: ''});
            this.anexo_polizas.forEach(element => {
                this.Tipos_tarjetas.push({label: element.descripcion, value: element.id});
            });
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    obtenerSucursal(instancia_poliza: Instancia_poliza): string {
        if (instancia_poliza.sucursal) {
            return this.devolverDescripcionByCodSucursal(instancia_poliza.sucursal.valor);
        }
        return "";
    }

    obtenerAgencia(instancia_poliza: Instancia_poliza): string {
        if (instancia_poliza.agencia) {
            return this.devolverDescripcionByCodAgencia(instancia_poliza.agencia.valor);
        }
        return "";
    }

    obtenerPlan(instancia_poliza: Instancia_poliza): string {
        if (instancia_poliza.plan) {
            return this.devolverDescripcionPoliza(instancia_poliza.plan.valor);
        }
        return "";
    }

    devolverDescripcion(id: any): string {
        if (id && this.parametros.length > 0) {
            if (this.parametros.find(param => param.id + '' === id + '')) {
                return this.parametros.find(param => param.id + '' === id + '').parametro_descripcion;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    devolverDescripcionByCodAgencia(id: any): string {
        if (id && this.parametros.length > 0) {
            if (this.parametros.find(param => param.parametro_cod + '' === id + '')) {
                return this.parametros.find(param => param.parametro_cod + '' === id + '' && param.diccionario_id + '' === '40').parametro_descripcion;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    devolverDescripcionByCodSucursal(id: any): string {
        if (id && this.parametros.length > 0) {
            if (this.parametros.find(param => param.parametro_cod + '' === id + '')) {
                return this.parametros.find(param => param.parametro_cod + '' === id + '' && param.diccionario_id + '' === '38').parametro_descripcion;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    devolverDescripcionPoliza(id: any): string {
        if (id && this.anexo_polizas.length > 0) {
            if (this.anexo_polizas.find(param => param.id + '' === id + '')) {
                return this.anexo_polizas.find(param => param.id + '' === id + '').descripcion;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    limpiar() {
        this.instancias_polizas = [];
        this.instancias = [];
        this.poliza = new Poliza();
        this.objeto = new Filtro();
    }

    buscarObjetoAsegurado(id_instancia_poliza: any, index: number) {
        this.index_instancia_poliza = index;
        let id_objeto = 0
        if (this.solicitudService.id_poliza == 6) {
            id_objeto = 18;
        }
        if (this.solicitudService.id_poliza == 7) {
            id_objeto = 20;
        }
        if (this.solicitudService.id_poliza == 8) {
            id_objeto = 22;
        }
        if (this.solicitudService.id_poliza == 14) {
            id_objeto = 32;
        }
        this.btnEmitir = true;
        this.solicitudService.isLoadingAgain = true;
        this.personaService.findPersonaSolicitudByIdInstanciaPoliza(id_objeto, id_instancia_poliza).subscribe(async res => {
            let response = res as { status: string, message: string, data: Asegurado };
            this.solicitudService.asegurado = response.data;
            await this.componenteActualizarSolicitud.cambiarSolicitudEstado().then(respo => {
                /*let ins = this.instancias_polizas.find(param => param.id + '' === id_instancia_poliza + '');
                if (ins) {
                    ins.id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
                }*/
                this.solicitudService.isLoadingAgain = false;
            });
            this.poliza.instancia_polizas = [];
            this.btnEmitir = false;
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });

    }

    VerPoliza(id: any) {
        this.btnVer2 = true;
        if (this.solicitudService.id_poliza === 5) {
            this.menuService.activaRuteoMenu('30', 5, {id_instancia_poliza: id, filtros: this.objeto}, 'AltaEcoMedic');
        }
        if (this.solicitudService.id_poliza == 8) {
            this.menuService.activaRuteoMenu('54', 8, {id_instancia_poliza: id, filtros: this.objeto}, 'AltaEcoMedicPlus');
        }
        if (this.solicitudService.id_poliza == 14) {
            this.menuService.activaRuteoMenu('90', 14, {id_instancia_poliza: id, filtros: this.objeto}, 'AltaEcoEstudio');
        }
        //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);
    }

    verPlanPagos(ins_pol: Instancia_poliza) {
        if (['8','14'].includes(this.solicitudService.id_poliza + '')) {
            let instanciaCertificado:Instancia_documento;
            if(ins_pol.instancia_documentos && ins_pol.instancia_documentos.length && ins_pol.instancia_documentos.find(param => param.documento.id_tipo_documento == 281)) {
                instanciaCertificado = ins_pol.instancia_documentos.find(param => param.documento.id_tipo_documento == 281);
            }
            this.menuService.activaRuteoMenu('66', this.solicitudService.id_poliza, {
                nro_certificado: instanciaCertificado.nro_documento,
                id_instancia_poliza: instanciaCertificado.id_instancia_poliza,
                filtros: this.objeto
            }, 'PlanPago');
        }
    }

    ImprimirSolicitudEcoMedic(id: any) {
        let nombre_archivo = 'SoliEcoMedic' + id;
        this.solicitudService.isLoadingAgain = true;
        this.reporteService.ReporteSolicitudEcomedic(id, nombre_archivo).subscribe(res => {
            this.solicitudService.isLoadingAgain = false;
            let response = res as { status: string, message: string, data: Poliza[] };
            if (response.status == 'ERROR') {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
            } else {
                this.reporteService.cargarPagina(nombre_archivo);
            }
        }, err => {
            this.solicitudService.isLoadingAgain = false;
            console.error("ERROR llamando servicio ImprimirSolicitud:", err);
        });
    }

    ImprimirCertificadoEcoMedic(id: any) {
        let nombre_archivo = 'CertEcoMedic' + id;
        this.solicitudService.isLoadingAgain = true;
        this.reporteService.certificado_Ecomedic2(id, nombre_archivo).subscribe(res => {
            let response = res as { status: string, message: string, data: Poliza[] };
            this.solicitudService.isLoadingAgain = false;
            if (response.status == 'ERROR') {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
            } else {
                this.reporteService.cargarPagina(nombre_archivo);
            }
        }, err => {
            this.solicitudService.isLoadingAgain = false;
            console.error("ERROR llamando servicio ImprimirCertificadoEcoVida:", err);
        });
    }
}
