import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoMedicComponent } from './alta-eco-medic.component';

describe('AltaEcoMedicComponent', () => {
  let component: AltaEcoMedicComponent;
  let fixture: ComponentFixture<AltaEcoMedicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoMedicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoMedicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
