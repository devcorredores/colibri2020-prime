import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoVidaComponent } from './alta-eco-vida.component';

describe('AltaEcoVidaComponent', () => {
  let component: AltaEcoVidaComponent;
  let fixture: ComponentFixture<AltaEcoVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
