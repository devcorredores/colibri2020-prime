import {ChangeDetectorRef, Component, OnInit, ViewChild,} from "@angular/core";
import {Asegurado} from "../../../../../../../src/core/modelos/asegurado";
import {Instancia_documento} from "../../../../../../../src/core/modelos/instancia_documento";
import {Atributo_instancia_poliza} from "../../../../../../../src/core/modelos/atributo_instancia_poliza";
import {FormBuilder, FormControl, FormGroup, Validators,} from "@angular/forms";
import {Poliza} from "../../../../../../../src/core/modelos/poliza";
import {Instancia_poliza_transicion} from "../../../../../../../src/core/modelos/instancia_poliza_transicion";
import {Message, MessageService, SelectItem} from "primeng/api";
import {isObject, isString} from "util";
import {persona_banco} from "../../../../../../../src/core/modelos/persona_banco";
import {persona_banco_datos} from "../../../../../../../src/core/modelos/persona_banco_datos";
import {persona_banco_account} from "../../../../../../../src/core/modelos/persona_banco_account";
import {persona_banco_tarjeta_debito} from "../../../../../../../src/core/modelos/persona_banco_tarjeta_debito";
import {Anexo_poliza} from "../../../../../../../src/core/modelos/anexo_poliza";
import {Objeto_x_atributo} from "../../../../../../../src/core/modelos/objeto_x_atributo";
import {ActivatedRoute, Router} from "@angular/router";
import {BreadcrumbService} from "../../../../../../../src/core/servicios/breadcrumb.service";
import {ParametrosService} from "../../../../../../../src/core/servicios/parametro.service";
import {AnexoService} from "../../../../../../../src/core/servicios/anexo.service";
import {PersonaService} from "../../../../../../../src/core/servicios/persona.service";
import {BeneficiarioService} from "../../../../../../../src/core/servicios/beneficiario.service";
import {SoapuiService} from "../../../../../../../src/core/servicios/soapui.service";
import {AtributoService} from "../../../../../../../src/core/servicios/atributo.service";
import {ObjetoAtributoService} from "../../../../../../../src/core/servicios/objetoAtributo.service";
import {DocumentoService} from "../../../../../../../src/core/servicios/documento.service";
import {PolizaService} from "../../../../../../../src/core/servicios/poliza.service";
import {InstanciaPolizaService} from "../../../../../../../src/core/servicios/instancia-poliza.service";
import {DomSanitizer} from "@angular/platform-browser";
import {ReporteService} from "../../../../../../../src/core/servicios/reporte.service";
import {RolesService} from "../../../../../../../src/core/servicios/rol.service";
import {UsuariosService} from "../../../../../../../src/core/servicios/usuarios.service";
import {ContextoService} from "../../../../../../../src/core/servicios/contexto.service";
import {InstanciaPolizaTransService} from "../../../../../../../src/core/servicios/instancia-poliza-trans.service";
import {SolicitudService} from "../../../../../../../src/core/servicios/solicitud.service";
import {Persona} from "../../../../../../../src/core/modelos/persona";
import {Beneficiario} from "../../../../../../../src/core/modelos/beneficiario";
import {Parametro} from "../../../../../../../src/core/modelos/parametro";
import {Atributo} from "../../../../../../../src/core/modelos/atributo";
import {Sucursal} from "../../../../../../../src/core/modelos/sucursal";
import {Agencia} from "../../../../../../../src/core/modelos/agencia";
import {Subscription} from "rxjs";
import {persona_banco_pep} from "../../../../../../../src/core/modelos/persona_banco_pep";
import {persona_banco_transaccion} from "../../../../../../../src/core/modelos/persona_banco_transaccion";
import {VigenciaService} from "../../../../../../../src/core/servicios/vigencia.service";
import '../../../../../../../src/helpers/prototypes';
import * as moment from "moment";
import {SessionStorageService} from "../../../../../../../src/core/servicios/sessionStorage.service";
import {Perfil_x_Componente} from "../../../../../../../src/core/modelos/componente";
import {AdministracionDePermisosService} from "../../../../../../../src/core/servicios/administracion-de-permisos.service";
import {TransicionesComponent} from "../../../../../../../src/core/componentes/transiciones/transiciones.component";
import {BeneficiarioComponent} from "../../../../../../../src/core/componentes/beneficiario/beneficiario.component";
import {ActualizarSolicitudComponent} from "../../../../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component";
import {Util} from "../../../../../../../src/helpers/util";
const util = new Util();
import {Usuario} from "../../../../../../../src/core/modelos/usuario";

declare var $: any;

@Component({
    selector: "app-alta-eco-vida",
    templateUrl: "./alta-eco-vida.component.html",
    styleUrls: ["./alta-eco-vida.component.css"],
})
export class AltaEcoVidaComponent implements OnInit {
    @ViewChild("componentTransiciones", {static: false}) private transicionesComponent: TransicionesComponent;
    @ViewChild("componenteBeneficiario", {static: false}) private componenteBeneficiario: BeneficiarioComponent;
    @ViewChild("componenteActualizarSolicitud", {static: false}) public componenteActualizarSolicitud: ActualizarSolicitudComponent;

    color = "primary";
    mode = "indeterminate";
    value = 1;
    show = false;
    yesterday: Date = new Date();
    displayFormTitular: boolean = true;
    displayDatosTitular: boolean = false;
    displayTarjetaInvalida: boolean = false;
    displayTarjetaInvalidaContinuar: boolean = false;
    displayBeneficiariosExcedidos: boolean = false;
    displayBeneficiariosTitular: boolean = false;
    displayBeneficiariosNotFound: boolean = false;
    displayEdadIncorrecta: boolean = false;
    displayErrorDiasGracia: boolean = false;
    displayErrorRenovacion: boolean = false;
    displayErrorSinCertificado: boolean = false;
    displayForceRenovacion: boolean = false;
    displayBeneficiarioFound: boolean = false;
    collapsedFormTitular: boolean = false;
    collapsedDatosTitular: boolean = false;
    displayClienteNoExiste: boolean = false;
    displayFaltanDatos: boolean = false;
    displaySolicitudDuplicado: boolean = false;
    displayActualizacionExitoso: boolean = false;
    displayIntroduscaNroTarjeta: boolean = false;
    displayNroTarjetaSinCuentas: boolean = false;
    displayNroCuentasActualizados: boolean = false;
    msgErrRenovacion: string = "";
    beneficiario: Persona = new Persona();
    anexosPlanesPoliza: Anexo_poliza[] = [];
    anexoPoliza: Anexo_poliza;
    Beneficiarios: Persona[] = [];
    BeneficiariosAux: Beneficiario[] = [];
    benAux: Beneficiario;
    id_beneficiarioAux;
    parentescoAux: Parametro;
    numeroBeneficiarios = 2;
    docId: string = null;
    minLengthMsg: string = "";
    minLengthMsgCelular: string = "";
    minlength: number = 0;
    minlengthCelular: number = 0;
    docIdExt: number = null;
    display: boolean = false;
    displayBeneficiario: boolean = false;
    displayBeneficiarioDouble: boolean = false;
    displayBeneficiariosCambio: boolean = false;
    displayEliminarBeneficiario: boolean = false;
    displayEliminarBeneficiarioOK: boolean = false;
    parametroMoneda: Parametro;
    SucursalesId: any[] = [];
    AgenciasId: any[] = [];
    MonedasParametroDescripcion: any[] = [];
    parametros: Parametro[] = [];
    userform: FormGroup;
    fechaEmisionForm: FormGroup;
    envioDocumentosForm: FormGroup;
    formBusqueda: FormGroup;
    userform2: FormGroup;
    userform3: FormGroup;
    responsiveOptions: any;
    nuevo: boolean = true;
    atributo: Atributo[];
    atributo_instancia_poliza_selected: any[] = [];
    cols: any[];
    doc_id: number = 0;
    ext: number = 0;
    persona_banco_beneficiario: persona_banco;
    plan: Anexo_poliza = new Anexo_poliza();
    usuario_banco_sucursal: Sucursal;
    usuario_banco_agencia: Agencia;
    events: Subscription[] = [];
    messages: string[] = [];
    maxLengthTarjeta: number = 1;
    guardandoTitular: boolean = false;
    editandoBeneficiario: boolean = false;
    fromSearch: boolean = false;
    id_instancia_poliza: number;
    id_estado: string;
    id_asegurado: string;
    totalPorcentaje = 0;
    persona: Persona;

    showErrors: boolean = false;
    displayValidacionAlInicio: boolean = false;
    messaje: string = "";
    withErrorsOrWarnings: boolean = false;

    btnRefrescarInformacion = false;
    btnEnviarDocumentosEnabled = false;
    btnTarjetaDebitoCuentasEnabled = true;
    showValidarYContinuar = true;
    showEmitirCertificado = true;
    showEnviarDocumentos = true;
    validarTarjeta: Boolean = false;
    reloadBeneficiarioComponent = true;
    util = new Util();
    apellidos: string[] = [];
    stopSaving: boolean = false;
    // edadMinimaYears: number = 18; // 18 Años;
    // edadMaximaYears: number = 65; // 65 años;
    // edadMinimaMonths: number = 18; // 18 Años;
    // edadMaximaMonths: number = 65; // 65 años;
    // edadMinimaDays: number = 18; // 18 Años;
    // edadMaximaDays: number = 65; // 65 años;
    diasGracia: number;

    stopSavingByEdad: boolean = false;
    stopSavingByLastName: boolean = false;
    stopSending: boolean = false;
    validandoContinuando: boolean = false;
    emitido: boolean = true;
    filtros: any;
    messageFind: string;
    messageRenew: string;
    lblButton: string;
    idsPerfiles: number[] = [];
    aseguradoWithDiferentDocIdExt: Asegurado;

    constructor(
        private params: ActivatedRoute,
        private breadcrumbService: BreadcrumbService,
        private changeDetection: ChangeDetectorRef,
        private parametroService: ParametrosService,
        private anexoService: AnexoService,
        private fb: FormBuilder,
        private personaService: PersonaService,
        private beneficiarioService: BeneficiarioService,
        private soapuiService: SoapuiService,
        private atributoService: AtributoService,
        private objetoAtributoService: ObjetoAtributoService,
        private documentoService: DocumentoService,
        private polizaService: PolizaService,
        private router: Router,
        private instanciaPolizaService: InstanciaPolizaService,
        private messageService: MessageService,
        private sanitizer: DomSanitizer,
        private reporteService: ReporteService,
        private rolesService: RolesService,
        private service: MessageService,
        private usuarioService: UsuariosService,
        private contextoService: ContextoService,
        private adminPermisosService: AdministracionDePermisosService,
        private instanciaPolizaTransService: InstanciaPolizaTransService,
        private cdRef: ChangeDetectorRef,
        public solicitudService: SolicitudService,
        public vigenciaService: VigenciaService,
        public sessionStorageService: SessionStorageService,
        public usuariosService: UsuariosService
    ) {
    }

    beforeToggleDatosTitular() {
        this.sessionStorageService.setItemSync("paramsDeleted", true);
        this.solicitudService.displayModalDatosTitular = false;
        this.solicitudService.changeView(this.userform, this.solicitudService.asegurado, this.filtros);
    }

    beforeToggleFormTitular() {
        this.solicitudService.displayModalFormTitular = false;
        this.solicitudService.changeView(this.userform, this.solicitudService.asegurado, this.filtros);
    }

    async setFeaturesBeneficiarios(beneficiarioResp: any) {
        if (beneficiarioResp != undefined) {
            if (beneficiarioResp.length) {
                beneficiarioResp.forEach(async (beneficiario: any) => {
                    beneficiario.persona_doc_id_ext =
                        beneficiario.entidad != undefined
                            ? beneficiario.entidad.persona.persona_doc_id_ext
                            : beneficiario.persona_doc_id_ext != undefined
                            ? beneficiario.persona_doc_id_ext
                            : "";
                    beneficiario.persona_primer_apellido =
                        beneficiario.entidad != undefined
                            ? beneficiario.entidad.persona
                                .persona_primer_apellido
                            : beneficiario.persona_primer_apellido != undefined
                            ? beneficiario.persona_primer_apellido
                            : "";
                    beneficiario.persona_segundo_apellido =
                        beneficiario.entidad != undefined
                            ? beneficiario.entidad.persona
                                .persona_segundo_apellido
                            : beneficiario.persona_segundo_apellido != undefined
                            ? beneficiario.persona_segundo_apellido
                            : "";
                });
            }
        }
    }

    ngOnInit() {
        this.solicitudService.product = this.solicitudService.ruta;
        this.solicitudService.persona_banco_datos = new persona_banco_datos();
        this.solicitudService.persona_banco = new persona_banco();
        this.solicitudService.persona_banco_account = new persona_banco_account();
        this.solicitudService.persona_banco_pep = new persona_banco_pep();
        this.solicitudService.persona_banco_transaccion = new persona_banco_transaccion();
        this.solicitudService.isInAltaSolicitud = true;
        this.solicitudService.paramsLoaded = false;
        this.solicitudService.isLoadingAgain = true;
        this.solicitudService.displayBusquedaCI = true;
        this.solicitudService.destinatariosCorreos = "";
        this.solicitudService.displayModalDatosTitular = false;
        this.solicitudService.displayModalSolicitudExistenteConRenovacion = false;
        this.solicitudService.displayModalSolicitudExistenteAlertRenovacion = false;
        this.solicitudService.displayModalSolicitudExistente = false;
        this.solicitudService.constructComponent(async () => {
            this.getPoliza(async () => {
                this.setUserForm();
                if (
                    !this.solicitudService.paramsDeleted &&
                    this.solicitudService.parametrosRuteo.parametro_ruteo !== null &&
                    this.solicitudService.parametrosRuteo.parametro_ruteo !== "undefined" &&
                    this.solicitudService.parametrosRuteo.parametro_ruteo !== "{}" &&
                    Object.keys(this.solicitudService.parametrosRuteo.parametro_ruteo).length
                ) {
                    this.solicitudService.isLoadingAgain = true;
                    this.solicitudService.editandoTitular = false;
                    this.solicitudService.displayBusquedaCI = false;
                    this.solicitudService.displayModalFormTitular = false;
                    this.id_instancia_poliza = this.solicitudService.parametrosRuteo.parametro_ruteo ? this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza ? this.solicitudService.parametrosRuteo.parametro_ruteo.id_instancia_poliza : null : null;
                    this.iniciarSolicitud();
                    this.solicitudService.product = this.solicitudService.poliza.descripcion;
                    this.personaService.findPersonaSolicitudByIdInstanciaPoliza(this.solicitudService.objetoAseguradoDatosComplementarios.id, this.id_instancia_poliza).subscribe(async (res) => {
                            let resp = res as { status: string; message: string; data: Asegurado };
                            if (resp.data && Object.keys(resp.data).length) {
                                this.solicitudService.asegurado = resp.data;
                                this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
                                await this.solicitudService.setDatesOfAsegurado();
                                this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
                                this.solicitudService.setAtributosToPersonaBanco(this.userform, async () => {
                                    await this.solicitudService.setAtributosToAsegurado(async (res) => {
                                            await this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach(async (instancia_documento: Instancia_documento) => {
                                                if (instancia_documento.id_documento == this.solicitudService.documentoSolicitud.id) {
                                                    this.solicitudService.instanciaDocumentoSolicitud = instancia_documento;
                                                } else if (instancia_documento.id_documento == this.solicitudService.documentoComprobante.id) {
                                                    this.solicitudService.instanciaDocumentoComprobante = instancia_documento;
                                                } else if (instancia_documento.id_documento == this.solicitudService.documentoCertificado.id) {
                                                    this.solicitudService.instanciaDocumentoCertificado = instancia_documento;
                                                }
                                            });

                                            await this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach(async (instancia_documento: Instancia_documento) => {
                                                    instancia_documento.atributo_instancia_documentos_inter = [];
                                                    // TODO: Iterar instancia_documento.atributo_instancia_documentos
                                                }
                                            );

                                            this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter = [];
                                            await this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.forEach(async (atributo_instancia_poliza: Atributo_instancia_poliza) => {
                                                    if (atributo_instancia_poliza.objeto_x_atributo.par_comportamiento_interfaz_id == this.solicitudService.parametroVisible.id) {
                                                        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter.push(atributo_instancia_poliza);
                                                    }
                                                }
                                            );
                                            await this.solicitudService.setAtributosToPersonaBanco(this.userform).then(async (res) => {
                                                await this.stateButtonsFlow();
                                            });
                                            this.setPago(this.solicitudService.atributoDebitoAutomatico.valor);
                                            this.solicitudService.setBeneficiarios();
                                            this.solicitudService.displayModalDatosTitular = true;
                                            this.displayDatosTitular = true;
                                            this.solicitudService.isLoadingAgain = false;
                                            if (this.componenteBeneficiario != undefined) {
                                                this.componenteBeneficiario.ngOnInit();
                                            }
                                            if (
                                                this.transicionesComponent != undefined
                                            ) {
                                                this.transicionesComponent.ngOnInit();
                                            }
                                            if (
                                                this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id
                                            ) {
                                                await this.getDatosFromAccountService();
                                            }
                                            this.stateButtonsFlow();
                                        }
                                    );
                                })

                            } else {
                                this.solicitudService.isLoadingAgain = false;
                                this.displayClienteNoExiste = true;
                                this.stateButtonsFlow();
                            }
                        },
                        (err) => {
                            if (
                                err.error.statusCode === 400 &&
                                err.error.message === "usuario no autentificado"
                            ) {
                                this.router.navigate([""]);
                            } else {
                                console.log(err);
                            }
                        }
                    );
                } else {
                    if (this.solicitudService.parametrosRuteo.openForm) {
                    } else {
                        this.solicitudService.isLoadingAgain = false;
                        this.solicitudService.displayModalFormTitular = false;
                        this.solicitudService.displayBusquedaCI = true;
                        this.cancelarSolicitud()
                    }
                }

            });
        });
    }

    async setUserForm() {
        if (this.solicitudService.id_poliza == 6) {
            this.idsPerfiles = [17];
        }
        this.solicitudService.setCurrentPerfil(this.idsPerfiles);
        this.adminPermisosService.getComponentes(this.solicitudService.parametrosRuteo.id_vista, this.solicitudService.currentPerfil.id).subscribe(async (res) => {
            let response = res as { status: string; message: string; data: Perfil_x_Componente[]; };
            this.solicitudService.isLoadingAgain = false;
            this.solicitudService.componentesInvisibles = response.data;
            await this.solicitudService.componentsBehavior(res => {
                if (this.solicitudService.id_poliza == 3) {
                    this.minlength = 16;
                } else if (this.solicitudService.id_poliza == 4) {
                    this.minlength = 9;
                }
                this.minlengthCelular = 8;
                this.minLengthMsg = "Debe contener " + this.minlength + " digitos";
                this.minLengthMsgCelular = "Debe contener " + this.minlengthCelular + " digitos";
                this.formBusqueda = this.fb.group({
                    persona_doc_id: new FormControl("", [Validators.required]),
                    persona_doc_id_ext: new FormControl("", Validators.required),
                });
                this.fechaEmisionForm = this.fb.group({
                    fecha_registro: new FormControl("", Validators.required),
                    nro_transaccion: new FormControl("", Validators.required),
                });

                this.envioDocumentosForm = this.fb.group({
                    destinatarios_correos: new FormControl("", [
                        Validators.required,
                    ]),
                    envio_documentos: new FormControl("", Validators.required),
                });
                this.userform = this.fb.group({
                    persona_doc_id: new FormControl("", [Validators.required]),
                    persona_doc_id_ext: new FormControl("", Validators.required),
                    //'persona_doc_id_comp': new FormControl(''),
                    persona_primer_apellido: new FormControl("", this.solicitudService.showPrimerApellido ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    persona_segundo_apellido: new FormControl("", this.solicitudService.showSegundoApellido ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$"),] : []),
                    persona_primer_nombre: new FormControl("", this.solicitudService.showPrimerNombre ? [Validators.required, Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    persona_segundo_nombre: new FormControl("", this.solicitudService.showSegundoNombre ? [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")] : []),
                    persona_apellido_casada: new FormControl("", [Validators.pattern("^[ÑñA-Za-z _]*[ÑñA-Za-z][ÑñA-Za-z _]*$")]),
                    persona_direccion_domicilio: new FormControl(""),
                    persona_direcion_trabajo: new FormControl(""),
                    persona_fecha_nacimiento: new FormControl("", [Validators.required, Validators.pattern("^(?:3[01]|[12][0-9]|0?[1-9])([\\-/.])(0?[1-9]|1[0-2])\\1\\d{4}$")]),
                    // 'persona_celular': new FormControl(''),
                    persona_telefono_domicilio: new FormControl(this.solicitudService.showTelefonoDomicilio ? [Validators.minLength(8), Validators.maxLength(8), Validators.pattern("^[0-9]*$")] : []),
                    persona_telefono_trabajo: new FormControl(this.solicitudService.showTelefonoTrabajo ? [Validators.minLength(8), Validators.maxLength(8), Validators.pattern("^[0-9]*$")] : []),
                    persona_telefono_celular: new FormControl("", this.solicitudService.showTelefonoCelular ? [Validators.minLength(8), Validators.maxLength(8), Validators.pattern("^[0-9]*$")] : []),
                    //'persona_profesion': new FormControl(''),
                    //'persona_profesion': new FormControl(''),
                    par_tipo_documento_id: new FormControl("", this.solicitudService.showNroCuenta ? [Validators.required] : []),
                    // 'par_nacionalidad_id': new FormControl(4),
                    par_pais_nacimiento_id: new FormControl("", [Validators.required]),
                    par_ciudad_nacimiento_id: new FormControl("", this.solicitudService.showCiudadNacimiento ? [Validators.required] : []),
                    par_sexo_id: new FormControl("", [Validators.required]),
                    //'persona_origen': new FormControl(''),
                    par_numero_cuenta: new FormControl("", this.solicitudService.showNroCuenta ? [Validators.required] : []),
                    par_cuenta_expiracion: new FormControl("", this.solicitudService.showCuentaFechaExpiracion ? [] : []),
                    par_ocupacion: new FormControl("", this.solicitudService.showOcupacion ? [] : []),
                    // 'par_plan': new FormControl('', this.solicitudService.showPlan ? [Validators.required] : []),
                    // 'par_plazo': new FormControl('', this.solicitudService.showPlazo ? [Validators.required, Validators.min(1)] : []),
                    par_codigo_agenda_id: new FormControl(""),
                    //'par_estado_civil_id': new FormControl(''),
                    par_mail_id: new FormControl("", this.solicitudService.showEmail ? [Validators.email] : []),
                    //'par_caedec_id': new FormControl(''),
                    //'par_localidad_id': new FormControl(''),
                    //'par_departamento_id': new FormControl(''),
                    par_sucursal: new FormControl("", this.solicitudService.showSucursal ? [] : []),
                    par_agencia: new FormControl("", this.solicitudService.showAgencia ? [] : []),
                    par_prima: new FormControl('', this.solicitudService.showPrima ? [Validators.required] : []),
                    //'par_nro_sci': new FormControl(''),
                    par_modalidad_pago: new FormControl(""),
                    par_zona: new FormControl(""),
                    par_nro_direccion: new FormControl(""),
                    par_razon_social: new FormControl(""),
                    par_nit_carnet: new FormControl(""),
                    par_moneda: new FormControl("", this.solicitudService.showMoneda ? [Validators.required] : []),
                    par_debito_automatico_id: new FormControl("", this.solicitudService.showPagoEfectivo ? [Validators.required] : []),
                    par_condicion_pep: new FormControl("", this.solicitudService.showCondicionPep ? [Validators.required] : []),
                    par_cargo_pep: new FormControl("", this.solicitudService.showCargoEntidadPep ? [] : []),
                    par_periodo_pep: new FormControl("", this.solicitudService.showPeriodoCargoPep ? [] : []),
                    par_direccion_laboral: new FormControl("", this.solicitudService.showDireccionLaboral ? [] : []),
                    par_desc_ocupacion: new FormControl("", this.solicitudService.showDescOcupacion ? [] : []),
                });
                this.solicitudService.getFormValidationErrors(this.userform);
                this.solicitudService.showFormValidation(this.userform);
                this.solicitudService.setFeatureValidacionAlInicioPersonaBanco();
            });
        });
    }

    async renovarSolicitud(emitido: boolean = true, forceRenovacion = false) {
        this.emitido = emitido;
        let [idInstanciaFinVigencia, asegurado] = this.getLasSolicitudVigente();
        let usuarioLogin = this.sessionStorageService.getItemSync("userInfo") as Usuario;
        if (asegurado) {
            let instanciaDocumentoCertificado = asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.solicitudService.documentoCertificado.id);
            let instanciaDocumentoSolicitud = asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.solicitudService.documentoSolicitud.id);
            let atributoInstanciaSucursal = asegurado.instancia_poliza.atributo_instancia_polizas.find((param) => param.objeto_x_atributo.id_atributo + "" == this.solicitudService.atributoSucursal.id + "");
            let atributoInstanciaAgencia = asegurado.instancia_poliza.atributo_instancia_polizas.find((param) => param.objeto_x_atributo.id_atributo + "" == this.solicitudService.atributoAgencia.id + "");
            let usuarioSucursal = usuarioLogin && usuarioLogin.usuario_banco && usuarioLogin.usuario_banco.us_sucursal ? usuarioLogin.usuario_banco.us_sucursal : null;
            let usuarioAgencia = usuarioLogin && usuarioLogin.usuario_banco && usuarioLogin.usuario_banco.us_oficina ? usuarioLogin.usuario_banco.us_oficina : null;
            if (instanciaDocumentoCertificado) {
                let momInicioVigencia = moment(instanciaDocumentoCertificado.fecha_inicio_vigencia);
                let momFinVigencia = moment(instanciaDocumentoCertificado.fecha_fin_vigencia);
                let momToday = moment(new Date());
                let diffDays = momFinVigencia.diff(momToday, "days") + 1;
                diffDays = diffDays < 0 ? diffDays * -1 : diffDays;
                this.diasGracia = parseInt(diffDays + "") ? parseInt(diffDays + "") : 0;
                let respRenovacion: any;
                this.solicitudService.isLoadingAgain = true;
                if (forceRenovacion) {
                    this.diasGracia += 360;
                }
                if (this.diasGracia >= 0) {
                    if (emitido) {
                        respRenovacion = await this.vigenciaService.verificaRenovandoSolicitud(this.solicitudService.poliza.id, asegurado.instancia_poliza.id, this.solicitudService.estadoPorPagar.id, this.diasGracia, true, usuarioSucursal, usuarioAgencia).toPromise();
                    } else {
                        respRenovacion = await this.vigenciaService.verificaRenovandoSolicitud(this.solicitudService.poliza.id, asegurado.instancia_poliza.id, this.solicitudService.estadoIniciado.id, this.diasGracia, true, usuarioSucursal, usuarioAgencia).toPromise();
                    }
                } else {
                    this.displayErrorDiasGracia = true;
                }
                this.solicitudService.isLoadingAgain = false;
                if (respRenovacion.data) {
                    let respRenovaciones = respRenovacion.data;
                    if (this.solicitudService.id_poliza == 6) {
                        this.idsPerfiles = [17];
                    }
                    this.solicitudService.setCurrentPerfil(this.idsPerfiles);
                    this.adminPermisosService.getComponentes(this.solicitudService.parametrosRuteo.id_vista, this.solicitudService.currentPerfil.id).subscribe(async (res) => {
                        let response = res as { status: string; message: string; data: Perfil_x_Componente[]; };
                        this.solicitudService.isLoadingAgain = false;
                        this.solicitudService.componentesInvisibles = response.data;
                        await this.solicitudService.componentsBehavior(async res => {
                            if (this.solicitudService.id_poliza == 3) {
                                this.minlength = 16;
                            } else if (this.solicitudService.id_poliza == 4) {
                                this.minlength = 9;
                            }
                            this.minlengthCelular = 8;
                            this.minLengthMsg = "Debe contener " + this.minlength + " digitos";
                            this.minLengthMsgCelular =
                                "Debe contener " + this.minlengthCelular + " digitos";
                            for (let j = 0; j < respRenovaciones.length; j++) {
                                let respRenovacion = respRenovaciones[j];
                                if (respRenovacion && respRenovacion.data && Object.keys(respRenovacion.data).length) {
                                    this.solicitudService.asegurado = respRenovacion.data;
                                    this.solicitudService.instanciaDocumentoCertificado = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento + "" == this.solicitudService.documentoCertificado.id + "");
                                    this.solicitudService.instanciaDocumentoSolicitud = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento + "" == this.solicitudService.documentoSolicitud.id + "");
                                    this.solicitudService.instanciaDocumentoComprobante = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento + "" == this.solicitudService.documentoComprobante.id + "");
                                    this.solicitudService.beneficiarios = this.solicitudService.asegurado.beneficiarios;
                                    this.solicitudService.persona_banco_account = new persona_banco_account();
                                    this.solicitudService.persona_banco = new persona_banco();
                                    this.solicitudService.persona_banco_datos = new persona_banco_datos();
                                    await this.stateButtonsFlow();
                                    await this.solicitudService.setDatesOfAsegurado();
                                    await this.solicitudService.setAtributosToPersonaBanco(this.userform);
                                    await this.solicitudService.setBeneficiarios();
                                    // await this.setSolicitudAsIniciado();
                                    if (this.componenteBeneficiario != undefined) {
                                        this.componenteBeneficiario.ngOnInit();
                                    }
                                    this.solicitudService.isLoadingAgain = false;
                                    this.solicitudService.displayBusquedaCI = false;
                                    this.solicitudService.displayModalDatosTitular = true;
                                    this.solicitudService.displayModalFormTitular = false;

                                    this.displayDatosTitular = true;
                                    this.collapsedDatosTitular = false;
                                } else {
                                    this.displayErrorRenovacion = true;
                                    this.msgErrRenovacion = "No se pudo renovar la solicitud debido a que existe una solicitud vigente";
                                }
                            }
                            this.solicitudService.displayRenovacionEstado = false;
                        });
                    });
                } else {
                    this.displayErrorRenovacion = true;
                }
            } else {
                if (instanciaDocumentoSolicitud) {
                    this.msgErrRenovacion = `La solicitud sin vigencia cuyo Nro de solicitud es: ${instanciaDocumentoSolicitud.nro_documento} no cuenta con un certificado de cobertura`;
                    this.displayErrorSinCertificado = true;
                } else {
                    this.displayErrorRenovacion = true;
                }
            }
        } else {
            this.displayErrorRenovacion = true;
        }
    }

    async featureApPaternoMaternoCasada() {
        if (
            this.solicitudService.persona_banco.paterno == "" &&
            this.solicitudService.persona_banco.materno == "" &&
            this.solicitudService.persona_banco.apcasada == ""
        ) {
            this.userform.controls["persona_primer_apellido"].setValidators([
                Validators.pattern(
                    "^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$"
                ),
            ]);
            return false;
        }
        return true;
    }

    async componentsBehaviorOnInit(callback: Function = null) {
        this.solicitudService.showTarjetaNombre = true;
        this.solicitudService.showTarjetaValida = true;
        this.solicitudService.showModalidadPago = true;
        this.solicitudService.showPagoEfectivo = true;
        this.solicitudService.showRazonSocial = true;
        this.solicitudService.showTarjetaNro = true;
        this.solicitudService.showNitCarnet = true;
        this.solicitudService.showTarjetaUltimosCuatroDigitos = true;
        this.solicitudService.showNroCuenta = true;
        this.solicitudService.showCuentaFechaExpiracion = true;
        this.solicitudService.showOcupacion = true;
        this.solicitudService.showPlan = true;
        this.solicitudService.showPlazo = true;
        this.solicitudService.showAgencia = true;
        this.solicitudService.showUsuarioCargo = true;
        this.solicitudService.showPrima = true;
        this.solicitudService.showSucursal = true;
        this.solicitudService.showMoneda = true;
        this.solicitudService.showCaedec = true;
        this.solicitudService.showLocalidad = true;
        this.solicitudService.showDepartamento = true;
        this.solicitudService.showCodSucursal = true;
        this.solicitudService.showTipoDocumento = true;
        this.solicitudService.showEmail = true;
        this.solicitudService.showEstadoCivil = true;
        this.solicitudService.showManejo = true;
        this.solicitudService.showCodAgenda = true;
        this.solicitudService.showDescCaedec = true;
        this.solicitudService.showZona = true;
        this.solicitudService.showNroDireccion = true;
        if (typeof callback == "function") {
            callback();
        }
    }

    async getPoliza(callback:Function = null) {
        await this.solicitudService.setIdPoliza(this.solicitudService.parametrosRuteo.parametro_vista);
        await this.polizaService.getPolizaById(this.solicitudService.id_poliza).subscribe(async res => {
            let response = res as { status: string, message: string, data: Poliza };
            this.solicitudService.poliza = response.data;
            await this.solicitudService.getAllParametrosByIdDiccionario([32,33],
                this.solicitudService.poliza.id, [1, 2, 3, 4, 11, 17, 18, 20, 21, 22, 25, 29, 30, 31, 32, 36, 38, 40, 52, 34, 58, 41],async () => {
                    if (this.solicitudService.poliza.anexo_polizas && this.solicitudService.poliza.anexo_polizas.length) {
                        this.solicitudService.poliza.anexo_poliza = this.solicitudService.poliza.anexo_polizas.find(param => param.id_tipo == 288 );
                        this.solicitudService.anexoPoliza = this.solicitudService.poliza.anexo_polizas.find(param => param.id_tipo == 288 );
                        this.solicitudService.anexoAsegurado = this.solicitudService.anexoPoliza.anexo_asegurados.find(param => param.id_tipo == this.solicitudService.parametroAsegurado.id);
                        if(this.solicitudService.anexoAsegurado) {
                            this.solicitudService.isRenovated = this.solicitudService.asegurado.instancia_poliza.id == this.solicitudService.asegurado.instancia_poliza.id_instancia_renovada ? false : true;
                            switch (this.solicitudService.anexoAsegurado.id_edad_unidad) {
                                case this.solicitudService.parametroYear.id:
                                    this.solicitudService.edadMinimaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                                case this.solicitudService.parametroMonth.id:
                                    this.solicitudService.edadMinimaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                                case this.solicitudService.parametroDay.id:
                                    this.solicitudService.edadMinimaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                    this.solicitudService.edadMaximaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                    break;
                            }
                        }
                    }
                    if(typeof callback == 'function') {
                        await callback();
                    }
                });
        },err =>{
            if(err.error.statusCode===400 && err.error.message==="usuario no autentificado"){
                this.router.navigate(['']);
            }else{
                console.log(err);
            }
        });
    }

    cambiarSolicitudEstado() {
        this.solicitudService.displayValidacionSinObservaciones = false;
        if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
            this.solicitudService.instanciaDocumentoComprobante = new Instancia_documento();
            this.solicitudService.instanciaDocumentoComprobante.documento = this.solicitudService.documentoComprobante;
            if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                this.solicitudService.instanciaDocumentoComprobante.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
            }
            if (!this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.solicitudService.instanciaDocumentoComprobante.id_documento)) {
                this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoComprobante);
            }
            this.solicitudService.emitirInstanciaPoliza = false;
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
            if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionNo.id+'') {
                let instanciaDocumentoCertificado;
                if (this.solicitudService.asegurado.instancia_poliza.instancia_documentos && this.solicitudService.asegurado.instancia_poliza.instancia_documentos.length) {
                    instanciaDocumentoCertificado = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento + "" == this.solicitudService.documentoCertificado.id + "");
                }
                if (instanciaDocumentoCertificado) {
                    this.solicitudService.instanciaDocumentoCertificado = instanciaDocumentoCertificado;
                } else {
                    this.solicitudService.instanciaDocumentoCertificado = new Instancia_documento();
                }
                if (this.solicitudService.poliza.tipo_numeracion.id == this.solicitudService.parametroNumeracionUnificada.id) {
                    this.solicitudService.instanciaDocumentoCertificado.nro_documento = this.solicitudService.instanciaDocumentoSolicitud.nro_documento;
                }
                this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
                this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
                this.solicitudService.emitirInstanciaPoliza = true;
            } else {
                this.solicitudService.emitirInstanciaPoliza = false;
            }
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
            let transicionSolicitado = new Instancia_poliza_transicion();
            transicionSolicitado.observacion = this.solicitudService.msgText;
            transicionSolicitado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            transicionSolicitado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            transicionSolicitado.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionSolicitado);
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
            let instanciaDocumentoCertificado;
            if (this.solicitudService.asegurado.instancia_poliza.instancia_documentos && this.solicitudService.asegurado.instancia_poliza.instancia_documentos.length) {
                instanciaDocumentoCertificado = this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento + "" == this.solicitudService.documentoCertificado.id + "");
            }
            if (instanciaDocumentoCertificado) {
                this.solicitudService.instanciaDocumentoCertificado = instanciaDocumentoCertificado;
            } else {
                this.solicitudService.instanciaDocumentoCertificado = new Instancia_documento();
                this.solicitudService.instanciaDocumentoCertificado.fecha_emision = new Date();
            }
            this.solicitudService.instanciaDocumentoCertificado.documento = this.solicitudService.documentoCertificado;
            this.solicitudService.emitirInstanciaPoliza = true;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
            let transicionSolicitado = new Instancia_poliza_transicion();
            transicionSolicitado.observacion = this.solicitudService.msgText;
            transicionSolicitado.id_instancia_poliza = this.solicitudService.asegurado.instancia_poliza.id;
            transicionSolicitado.par_estado_id = this.solicitudService.asegurado.instancia_poliza.id_estado;
            transicionSolicitado.par_observacion_id = this.solicitudService.parametroSuccess.id;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(transicionSolicitado);
        } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
            this.solicitudService.emitirInstanciaPoliza = true;
        }
        if (!this.solicitudService.userFormHasErrors && !this.solicitudService.emitirInstanciaPoliza) {
            this.updateInstanciaPoliza();
        } else if (!this.solicitudService.userFormHasErrors && this.solicitudService.emitirInstanciaPoliza) {
            if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                this.solicitudService.displayEmitirSolicitud = false;
                this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
            } else {
                this.solicitudService.displayEmitirSolicitud = true;
            }
        }
    }

    updateInstanciaPoliza() {
        if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionNo.id + '' &&
            this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id &&
            !this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.solicitudService.instanciaDocumentoCertificado.id_documento)
        ) {
            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoCertificado);
        }
        this.solicitudService.isLoadingAgain = true;
        if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
            if (this.solicitudService.hasRolCajero) {
                if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoPorPagar.id,
                        this.solicitudService.estadoEmitido.id,
                    ];
                } else {
                    this.solicitudService.estadosUpdateSolicitud = [
                        this.solicitudService.estadoIniciado.id,
                        this.solicitudService.estadoSolicitado.id,
                        this.solicitudService.estadoEmitido.id,
                    ];
                }
            } else {
                this.solicitudService.estadosUpdateSolicitud = [
                    this.solicitudService.estadoIniciado.id,
                    this.solicitudService.estadoSolicitado.id,
                    this.solicitudService.estadoPorPagar.id,
                    this.solicitudService.estadoEmitido.id,
                ];
            }
        } else {
            this.solicitudService.estadosUpdateSolicitud = [
                this.solicitudService.estadoIniciado.id,
                this.solicitudService.estadoSolicitado.id,
                this.solicitudService.estadoEmitido.id,
            ];
        }
        this.instanciaPolizaService
            .updateInstanciaToNextStatus(
                this.solicitudService.estadosUpdateSolicitud,
                this.solicitudService.asegurado
            )
            .subscribe(
                (res) => {
                    let response = res as {status: string;message: string;data: Asegurado;};
                    this.solicitudService.asegurado = response.data;
                    this.stateButtonsFlow();
                    this.solicitudService.setDatesOfAsegurado();
                    this.solicitudService.asegurado.instancia_poliza.instancia_documentos.forEach(
                        (instancia_documento: Instancia_documento) => {
                            if (instancia_documento.id_documento == this.solicitudService.documentoSolicitud.id) {
                                this.solicitudService.instanciaDocumentoSolicitud = instancia_documento;
                            } else if (instancia_documento.id_documento == this.solicitudService.documentoComprobante.id) {
                                this.solicitudService.instanciaDocumentoComprobante = instancia_documento;
                            } else if (instancia_documento.id_documento == this.solicitudService.documentoCertificado.id) {
                                this.solicitudService.instanciaDocumentoCertificado = instancia_documento;
                            }
                        }
                    );
                    this.solicitudService.setBeneficiarios();
                    this.transicionesComponent.ngOnInit();
                    this.componenteBeneficiario.ngOnInit();
                    if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                        if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                            this.solicitudService.msgCambioExitoso = "Se habilitó la posibilidad de imprimir la solicitud Nro: " +
                                this.solicitudService.instanciaDocumentoSolicitud.nro_documento+'' + ",  y el comprobante de pago Nro:" +
                                this.solicitudService.instanciaDocumentoComprobante.nro_documento+'';
                        } else {
                            this.solicitudService.msgCambioExitoso =
                                "Se habilitó la posibilidad de imprimir la solicitud Nro: " +
                                this.solicitudService.instanciaDocumentoSolicitud.nro_documento+'';
                        }
                    } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                        this.solicitudService.msgCambioExitoso =
                            "El cliente debe aproximarse a caja para hacer el pago de la prima y obtener su certificado de cobertura.";
                    } else if (this.solicitudService.asegurado.instancia_poliza.id_estado+'' == this.solicitudService.estadoEmitido.id+'') {
                        this.solicitudService.msgCambioExitoso =
                            "¿Desea imprimir el certificado de cobertura Nro:" +
                            this.solicitudService.instanciaDocumentoCertificado.nro_documento+'' + ".";
                    }
                    this.solicitudService.displayEmitirSolicitud = false;
                    this.solicitudService.displayEmitirSolicitudDesdeCaja = false;

                    if (
                        this.solicitudService.isInAltaSolicitud &&
                        this.solicitudService.hasRolCajero &&
                        this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id
                    ) {
                        this.solicitudService.displayCambioEstadoExitosoSinOpciones = false;
                        this.solicitudService.displayEmitirSolicitudDesdeCaja = true;
                        this.solicitudService.setDocumentoCertificado();
                    } else {
                        this.solicitudService.displayCambioEstadoExitosoSinOpciones = true;
                    }

                    this.solicitudService.msgCambioEstadoExitoso = "La solicitud fue actualizada exitósamente a la siguiente instancia. " +
                        this.solicitudService.EstadosInstaciaPolizaId[this.solicitudService.asegurado.instancia_poliza.id_estado];
                    this.solicitudService.isLoadingAgain = false;
                    this.validandoContinuando = false;
                },
                (err) => {
                    if (
                        err.error.statusCode === 400 &&
                        err.error.message === "usuario no autentificado"
                    ) {
                        this.router.navigate([""]);
                    } else {
                        console.log(err);
                    }
                }
            );
    }

    async cambiarPersonaExtension(idPerson: number, idInstanciaPoliza: number, docId:string, ext:string) {
        this.solicitudService.isLoadingAgain = true;
        this.personaService.actualizarPersonaExt(idPerson, idInstanciaPoliza, docId, ext).subscribe((resp) => {
            let response = resp as {
                status: string;
                message: string;
                data: { persona:Persona, instancia_documentos:Instancia_documento[]};
            };
            if (isObject(response.data) && Object.keys(response.data).length) {
                this.solicitudService.isLoadingAgain = false;
                this.solicitudService.asegurado.entidad.persona = response.data.persona;
                this.solicitudService.displayActualizacionPersona = false;
                this.afterSearch();
            }
        });
    }

    async validarYContinuar() {
        this.solicitudService.msgs_warn = [];
        this.solicitudService.msgs_error = [];
        this.solicitudService.userFormHasErrors = false;
        this.solicitudService.userFormHasWarnings = false;
        this.validandoContinuando = true;
        await this.setAtributosToAsegurado(async () => {
            await this.solicitudService.setBeneficiarios();
            await this.solicitudService.validarBeneficiarios(this.componenteBeneficiario);
            await this.solicitudService.setUsuarioRoles();
            await this.solicitudService.validarWarnsOrErrors();
            await this.stateButtonsFlow();
            if (this.transicionesComponent != undefined) {
                this.transicionesComponent.ngOnInit();
            }
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                this.solicitudService.mostrarObservaciones = true;
                if (this.solicitudService.msgs_warn.length) {
                    this.solicitudService.msgText = "La información de la solicitud " + this.solicitudService.instanciaDocumentoSolicitud.nro_documento + ", tiene observaciones clasificadas como advertencias que no le impedirán continuar la instrumentación del seguro";
                } else {
                    this.solicitudService.msgText = "La solicitud no tiene observaciones ni advertencias, por lo que se pasará a la siguiente instanciá.";
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                this.solicitudService.mostrarObservaciones = false;
                if (this.solicitudService.msgs_warn.length) {
                    this.solicitudService.msgText = "La información de la solicitud " + this.solicitudService.instanciaDocumentoSolicitud.nro_documento + ", tiene observaciones clasificadas como advertencias que no le impedirán continuar la instrumentación del seguro";
                } else {
                    this.solicitudService.msgText = "La solicitud no tiene observaciones ni advertencias, por lo que se pasará a la siguiente instanciá.";
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                this.solicitudService.mostrarObservaciones = false;
                if (this.solicitudService.msgs_warn.length) {
                    this.solicitudService.msgText = "La información de la solicitud " + this.solicitudService.instanciaDocumentoSolicitud.nro_documento + ", tiene observaciones clasificadas como advertencias que no le impedirán continuar la instrumentación del seguro";
                } else {
                    this.solicitudService.msgText = "La solicitud no tiene observaciones ni advertencias, por lo que se pasará a la siguiente instanciá.";
                }
            } else {
                this.solicitudService.mostrarObservaciones = false;
                this.solicitudService.msgText = "La solicitud no tiene observaciones ni advertencias,  por lo que se la pasará a la siguiente instanciá.";
            }

            if (this.solicitudService.userFormHasErrors) {
                this.withErrorsOrWarnings = true;
                // this.messageService.add({ key: 'tst', severity: 'error', summary: 'Error Message', detail: 'Existen campos en el formulario de solicitud que deben ser completados' });
            } else if (this.solicitudService.userFormHasWarnings) {
                this.withErrorsOrWarnings = true;
                // this.messageService.add({ key: 'tst', severity: 'warn', summary: 'Warn Message', detail: 'Existen campos en el formulario de solicitud que tienen observaciones para ser consideradas' });
            } else {
                this.solicitudService.displayValidacionConObservaciones = false;
                this.solicitudService.displayValidacionSinObservaciones = true;
            }

            if (this.solicitudService.asegurado.instancia_poliza.id == this.solicitudService.asegurado.instancia_poliza.id_instancia_renovada) {
                if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id && this.solicitudService.hasRolCajero) {
                    this.solicitudService.userFormHasWarnings = false;
                    this.solicitudService.userFormHasErrors = false;
                    this.solicitudService.displayValidacionConObservaciones = false;
                    this.solicitudService.displayValidacionSinObservaciones = true;
                } else {
                    if (this.solicitudService.userFormHasWarnings && this.solicitudService.userFormHasErrors) {
                        this.solicitudService.userFormHasWarnings = false;
                        this.solicitudService.userFormHasErrors = false;
                        this.solicitudService.displayValidacionConObservaciones = true;
                        this.solicitudService.displayValidacionSinObservaciones = false;
                    } else if (this.solicitudService.userFormHasErrors) {
                        this.solicitudService.userFormHasWarnings = false;
                        this.solicitudService.userFormHasErrors = true;
                        this.solicitudService.displayValidacionConObservaciones = true;
                        this.solicitudService.displayValidacionSinObservaciones = false;
                    } else {
                        this.solicitudService.userFormHasWarnings = false;
                        this.solicitudService.userFormHasErrors = false;
                        this.solicitudService.displayValidacionConObservaciones = false;
                        this.solicitudService.displayValidacionSinObservaciones = true;
                    }
                }
            } else {
                this.solicitudService.userFormHasWarnings = false;
                this.solicitudService.userFormHasErrors = false;
                this.solicitudService.displayValidacionConObservaciones = false;
                this.solicitudService.displayValidacionSinObservaciones = true;
            }
        });
    }

    validateForm() {
        // console.log(this.userform.valid);
        // console.log(this.userform.controls);
        // console.log(this.solicitudService.asegurado);
        // Object.values(this.userform.controls).forEach((value, index) => {
        //     console.log(Object.keys(this.userform.controls)[index], value.validator);
        // });
        // console.log(this.userform.getRawValue());
        // console.log(this.util.ValidarComponentesInvisible(this.componentesInvisibles,'EA_TitParSexoId'));
        // console.log(this.util.ValidarComponentesInvisible(this.componentesInvisibles,'EA_TitParNumeroCuenta'));
        // console.log(this.util.ValidarComponentesInvisible(this.componentesInvisibles,'EA_TitParMoneda'));
        // console.log(this.util.ValidarComponentesInvisible(this.componentesInvisibles,'EA_TitDebitoAutomatico'));
        // console.log(this.util.ValidarComponentesInvisible(this.componentesInvisibles,'EA_TitTarjetaNro'));
        // console.log(this.nuevo);
        console.log(
            this.solicitudService.getFormValidationErrors(this.userform)
        );
        console.log(
            "atributoCondicionPep",
            this.solicitudService.atributoCondicionPep.requerido
        );
    }

    disableForm(form: FormGroup) {
        for (var control in form.controls) {
            if (form.controls[control]) {
                form.controls[control].disable();
            }
        }
        setTimeout(() => {
            $(".ui-message").css("display", "none");
        }, 200);
    }

    enableUserform() {
        for (var control in this.userform.controls) {
            if (
                control == "par_tipo_documento_id" ||
                control == "persona_direccion_domicilio" ||
                control == "persona_doc_id_comp" ||
                control == "par_pais_nacimiento_id" ||
                control == "persona_telefono_trabajo" ||
                control == "persona_telefono_domicilio" ||
                control == "par_debito_automatico_id" ||
                control == "submit" ||
                control == "par_moneda" ||
                control == "par_numero_cuenta"
            ) {
                if (this.userform.controls[control]) {
                    this.userform.controls[control].enable();
                }
            }
        }
    }

    // setSolicitudAsIniciado() {
    //     if (
    //         this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id
    //     ) {
    //         this.solicitudService.btnEmitirCertificadoEnabled = false;
    //         this.btnEnviarDocumentosEnabled = false;
    //         this.btnImprimirSolicitudEnabled = false;
    //         if (
    //             this.solicitudService.atributoDebitoAutomatico.valor ==
    //             this.solicitudService.condicionSi.id+''
    //         ) {
    //             this.solicitudService.showOrdenPago = true;
    //         } else {
    //             this.solicitudService.showOrdenPago = false;
    //         }
    //         this.solicitudService.btnOrdenPago(false);
    //         if (
    //             this.solicitudService.hasRolConsultaTarjetas ||
    //             this.solicitudService.hasRolConsultaCajero
    //         ) {
    //             this.btnRefrescarInformacion = false;
    //         } else {
    //             this.btnRefrescarInformacion = false;
    //         }
    //     }
    // }

    stateButtonsFlow() {
        this.solicitudService.showFormValidation(this.userform);
        this.solicitudService.validarBeneficiarios(this.componenteBeneficiario);
        // this.solicitudService.setFeatureValidacionAlInicioPersonaBanco();
        this.solicitudService.btnValidarYContinuarEnabled = true;
        this.btnTarjetaDebitoCuentasEnabled = true;
        this.solicitudService.showOrdenPago = true;
        this.solicitudService.btnOrdenPago(true);
        if (
            this.solicitudService.asegurado.instancia_poliza != null &&
            this.solicitudService.atributoDebitoAutomatico != undefined &&
            this.solicitudService.condicionSi != undefined &&
            this.solicitudService.condicionNo != undefined
        ) {
            if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoIniciado.id) {
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = false;
                }
                this.btnEnviarDocumentosEnabled = false;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                    if (this.solicitudService.hasRolCajero) {
                        this.solicitudService.btnOrdenPago(false);
                        this.solicitudService.showOrdenPago = false;
                    } else {
                        this.solicitudService.btnOrdenPago(true);
                        this.solicitudService.showOrdenPago = true;
                    }
                } else {
                    this.solicitudService.showOrdenPago = false;
                }
                this.solicitudService.btnOrdenPago(false);
                if (
                    this.solicitudService.hasRolConsultaTarjetas ||
                    this.solicitudService.hasRolConsultaCajero
                ) {
                    this.btnRefrescarInformacion = false;
                } else {
                    this.btnRefrescarInformacion = false;
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSolicitado.id) {
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.btnRefrescarInformacion = false;
                this.btnEnviarDocumentosEnabled = true;
                this.solicitudService.btnImprimirSolicitudEnabled = true;
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                    if (this.solicitudService.hasRolCajero) {
                        this.solicitudService.btnOrdenPago(false);
                        this.solicitudService.showOrdenPago = false;
                    } else {
                        this.solicitudService.btnOrdenPago(true);
                        this.solicitudService.showOrdenPago = true;
                    }
                } else {
                    this.solicitudService.btnOrdenPago(true);
                    this.solicitudService.showOrdenPago = false;
                }
            } else if ( this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoPorPagar.id) {
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.btnRefrescarInformacion = false;
                this.btnEnviarDocumentosEnabled = true;
                this.solicitudService.btnImprimirSolicitudEnabled = true;
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                    if (this.solicitudService.hasRolCajero) {
                        this.solicitudService.btnOrdenPago(false);
                        this.solicitudService.btnValidarYContinuarEnabled = true;
                        this.solicitudService.showOrdenPago = false;
                    } else {
                        this.solicitudService.btnOrdenPago(true);
                        this.solicitudService.btnValidarYContinuarEnabled = false;
                        this.solicitudService.showOrdenPago = true;
                    }
                }
            } else if (this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoEmitido.id) {
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = true;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = true;
                this.solicitudService.btnImprimirSolicitudEnabled = true;
                this.btnEnviarDocumentosEnabled = true;
                this.solicitudService.btnValidarYContinuarEnabled = false;
                this.btnRefrescarInformacion = false;
                if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
                    if (this.solicitudService.hasRolCajero) {
                        this.solicitudService.btnOrdenPago(false);
                        this.solicitudService.showOrdenPago = false;
                    } else {
                        this.solicitudService.btnOrdenPago(true);
                        this.solicitudService.showOrdenPago = true;
                    }
                }
            } else if (
                this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoAnulado.id ||
                this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoSinVigencia.id||
                this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoCaducado.id||
                this.solicitudService.asegurado.instancia_poliza.id_estado == this.solicitudService.estadoDesistido.id
            ) {
                if (this.componenteBeneficiario) {
                    this.componenteBeneficiario.enableAdicionarBeneficiario = false;
                }
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
                this.btnEnviarDocumentosEnabled = false;
                this.solicitudService.btnOrdenPago(false);
                this.solicitudService.btnValidarYContinuarEnabled = false;
                this.btnRefrescarInformacion = false;
            } else {
                this.solicitudService.btnOrdenPago(false);
                this.solicitudService.btnEmitirCertificadoEnabled = false;
                this.solicitudService.btnImprimirSolicitudEnabled = false;
            }
        }
    }

    // async btnOrdenPago(isEnabled:boolean) {
    //     if(isEnabled) {
    //         if (this.solicitudService.condicionNo) {
    //             if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionNo.id+'') {
    //                 this.solicitudService.btnOrdenPagoEnabled = false;
    //                 this.solicitudService.showOrdenPago = false;
    //             } else if (this.solicitudService.atributoDebitoAutomatico.valor == this.solicitudService.condicionSi.id+'') {
    //                 this.solicitudService.btnOrdenPagoEnabled = true;
    //             } else {
    //                 //this.solicitudService.showOrdenPago = false;
    //             }
    //         } else {
    //             this.solicitudService.btnOrdenPagoEnabled = false;
    //         }
    //     } else {
    //         this.solicitudService.btnOrdenPagoEnabled = false;
    //     }
    // }
    //
    async refrescarInformacion() {
        this.solicitudService.isLoadingAgain = true;
        this.solicitudService.editandoTitular = true;
        this.getDatosFromCustomerService(async (res) => {
            this.setAtributosToAsegurado();
            await this.stateButtonsFlow();
            this.solicitudService.persona_banco.debito_automatico = this.solicitudService.atributoDebitoAutomatico.valor;
            this.solicitudService.persona_banco_account.moneda = this.solicitudService.atributoMoneda.valor;
            this.solicitudService.persona_banco_datos.nro_tarjeta = this.solicitudService.atributoNroTarjeta.valor;
            this.solicitudService.persona_banco_account.nrocuenta = this.solicitudService.atributoNroCuenta.valor;
            this.solicitudService.persona_banco_datos.zona = this.solicitudService.atributoZona.valor;
            this.solicitudService.persona_banco_datos.nro_direccion = this.solicitudService.atributoNroDireccion.valor;
            this.solicitudService.persona_banco.e_mail = this.solicitudService.atributoEmail.valor;
            this.solicitudService.persona_banco_tarjeta_debito.tarjeta_ultimos_cuatro_digitos = this.solicitudService.atributoUltimosCuatroDigitos.valor;
            this.solicitudService.persona_banco_datos.nit_carnet = this.solicitudService.atributoNitCarnet.valor;
            this.solicitudService.persona_banco_datos.razon_social = this.solicitudService.atributoRazonSocial.valor;
            this.solicitudService.persona_banco_datos.ocupacion = this.solicitudService.atributoOcupacion.valor;
            this.solicitudService.persona_banco_datos.tipo_doc = this.solicitudService.atributoTipoDoc.valor;
            this.solicitudService.persona_banco_datos.amp_con_amb_med_general = this.solicitudService.atributoAmpConAmbMedGeneral.valor;
            this.solicitudService.persona_banco_datos.amp_con_amb_med_especializada = this.solicitudService.atributoAmpConAmbMedEspecializada.valor;
            this.solicitudService.persona_banco_datos.amp_sum_amb_medicamentos = this.solicitudService.atributoAmpSumAmbMedicamentos.valor;
            this.solicitudService.persona_banco_datos.amp_sum_exa_laboratorio = this.solicitudService.atributoAmpSumExaLaboratorio.valor;
            this.solicitudService.persona_banco_datos.plan = parseInt(this.solicitudService.atributoPlan.valor);
            this.solicitudService.persona_banco_transaccion.moneda = this.solicitudService.atributoTransaccionMoneda.valor;
            this.solicitudService.persona_banco_transaccion.detalle = this.solicitudService.atributoTransaccionDetalle.valor;
            this.solicitudService.persona_banco_transaccion.importe = this.solicitudService.atributoTransaccionImporte.valor;
            this.solicitudService.persona_banco_transaccion.fechatran = this.solicitudService.atributoFechaTransaccion.valor;
            this.solicitudService.persona_banco_transaccion.nrotran = this.solicitudService.atributoNroTransaccion.valor;
            this.solicitudService.persona_banco_datos.plazo = parseInt(this.solicitudService.atributoPlazo.valor);
            if (this.solicitudService.usuario_banco) {
                this.solicitudService.usuario_banco.us_sucursal = parseInt(this.solicitudService.atributoSucursal.valor);
                this.solicitudService.usuario_banco.us_oficina = parseInt(this.solicitudService.atributoAgencia.valor);
                this.solicitudService.usuario_banco.us_cargo = this.solicitudService.atributoUsuarioCargo.valor;
            }
            this.solicitudService.persona_banco_datos.modalidad_pago = this.solicitudService.atributoModalidadPago.valor;
            this.solicitudService.persona_banco_datos.prima = parseInt(this.solicitudService.atributoPrima.valor);
            this.solicitudService.persona_banco.direccion = this.solicitudService.atributoDireccionDomicilio.valor;
            this.solicitudService.persona_banco_datos.direccion_laboral = this.solicitudService.atributoDireccionLaboral.valor;
            this.solicitudService.persona_banco_datos.zona = this.solicitudService.atributoZona.valor;
            this.solicitudService.persona_banco.fono_domicilio = this.solicitudService.atributoTelefonoDomicilio.valor;
            this.solicitudService.persona_banco.e_mail = this.solicitudService.atributoEmail.valor;
            this.solicitudService.persona_banco.nro_celular = this.solicitudService.atributoTelefonoCelular.valor;
            this.solicitudService.persona_banco_datos.ocupacion = this.solicitudService.atributoOcupacion.valor;
            this.solicitudService.persona_banco_datos.desc_ocupacion = this.solicitudService.atributoDescOcupacion.valor;
            this.solicitudService.persona_banco_datos.telefono_celular = this.solicitudService.atributoTelefonoCelular.valor;
            this.solicitudService.persona_banco_datos.ciudad_nacimiento = this.solicitudService.atributoCiudadNacimiento.valor;
            this.solicitudService.persona_banco_datos.direccion_laboral = this.solicitudService.atributoDireccionLaboral.valor;
            this.solicitudService.persona_banco_pep.condicion = this.solicitudService.atributoCondicionPep.valor;
            this.solicitudService.persona_banco_pep.cargo_entidad = this.solicitudService.atributoCargoEntidadPep.valor;
            this.solicitudService.persona_banco_pep.periodo_cargo_publico = this.solicitudService.atributoPeriodoCargoPublico.valor;
            this.solicitudService.persona_banco_tarjeta_debito.fecha_expiracion = isString(this.solicitudService.atributoCtaFechaExpiracion.valor) ? new Date(this.solicitudService.atributoCtaFechaExpiracion.valor) : new Date(this.solicitudService.atributoCtaFechaExpiracion.valor + '');
            this.solicitudService.aseguradoBeneficiarios = this.solicitudService.asegurado.beneficiarios;
            // this.solicitudService.setBeneficiarios();
            // this.validarTarjetaYGuardarSolicitud(() => {
            this.solicitudService.asegurado.beneficiarios = this.solicitudService.aseguradoBeneficiarios;
            this.solicitudService.setBeneficiarios();
            this.displayActualizacionExitoso = true;
            this.solicitudService.isLoadingAgain = false;
            // });
        });
    }

    async getDatosFromCustomerService(callback: Function = null) {
        await this.soapuiService.getCustomer(
            this.solicitudService.doc_id,
            this.solicitudService.extension
        ).subscribe(
            async (res) => {
                let response = res as {
                    status: string;
                    message: string;
                    data: persona_banco;
                };
                if (isObject(response.data) && Object.keys(response.data).length) {
                    this.solicitudService.persona_banco = response.data;
                    this.solicitudService.persona_banco.debito_automatico = "";
                    if (this.solicitudService.id_poliza == 6) {
                        this.solicitudService.persona_banco_datos = new persona_banco_datos();
                        if (this.solicitudService.persona_banco.cod_agenda != "" &&
                            this.solicitudService.persona_banco.cod_agenda != undefined &&
                            this.solicitudService.persona_banco.cod_agenda != "undefined"
                        ) {
                            this.solicitudService.esClienteBanco = true;
                            await this.getDatosFromAccountService(callback);
                        } else {
                            this.solicitudService.persona_banco_accounts = [];
                            this.solicitudService.esClienteBanco = false;
                            this.solicitudService.Cuentas = [];
                            this.solicitudService.persona_banco_account = new persona_banco_account();
                            if (typeof callback == "function") {
                                await callback(this.solicitudService.esClienteBanco);
                            }
                        }
                    }
                } else {
                    this.solicitudService.esClienteBanco = false;
                    if (typeof callback == "function") {
                        await callback(
                            this.solicitudService.esClienteBanco
                        );
                    }
                    // this.displayClienteExistente = true;
                    this.solicitudService.isLoadingAgain = false;
                }
            },
            (err) => {
                if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                    this.router.navigate([""]);
                } else {
                    this.solicitudService.nombreServicioBanco = 'getCustomer';
                    this.solicitudService.displayErrorRespuestaBanco = false;
                }
            }
        );
    }

    async getDatosFromAccountService(callback: Function = null) {
        if (this.solicitudService.asegurado.instancia_poliza.id_estado == 24) {

            await this.soapuiService.getAccount(this.solicitudService.persona_banco.cod_agenda).subscribe(
                async (res) => {
                    this.solicitudService.Cuentas = [{label: "Seleccione Nro de Cuenta", value: null},];
                    let response = res as { status: string; message: string; data: any; };
                    if (isObject(response.data) && Object.keys(response.data).length) {
                        this.solicitudService.persona_banco_accounts = [];
                        this.solicitudService.esClienteBanco = true;
                        this.solicitudService.tieneCuentaBanco = true;
                        if (response.data.nrocuenta != undefined) {
                            //this.solicitudService.persona_banco_account = response.data;
                            this.solicitudService.persona_banco_accounts.push(response.data);
                            this.solicitudService.Cuentas.push({
                                label: response.data.nrocuenta + " " + this.solicitudService.MonedasParametroCod[response.data.moneda+''],
                                value: response.data.nrocuenta,
                            });
                        } else {
                            let keys = Object.keys(response.data);
                            let values = Object.values(response.data);
                            await keys.forEach((key) => {
                                this.solicitudService.Cuentas.push({
                                    label: values[key].nrocuenta + " " + this.solicitudService.MonedasParametroCod[values[key].moneda],
                                    value: values[key].nrocuenta,
                                });
                                this.solicitudService.persona_banco_accounts.push(values[key]);
                            });
                        }
                    } else {
                        this.solicitudService.tieneCuentaBanco = false;
                        this.solicitudService.persona_banco_accounts = [];
                        this.solicitudService.Cuentas = [];
                    }
                    this.solicitudService.persona_banco_account = new persona_banco_account();
                    if (typeof callback == "function") {
                        await callback(this.solicitudService.esClienteBanco);
                    }
                },
                (err) => {
                    if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        this.router.navigate([""]);
                    } else {
                        this.solicitudService.nombreServicioBanco = 'getAccount';
                        this.solicitudService.displayErrorRespuestaBanco = false;
                    }
                }
            );
        } else {
            if (typeof callback == 'function') {
                await callback();
            }
        }
    }

    async getDatosFromTarjetaDebitoCuentaService(callback: Function = null) {
        if (this.solicitudService.persona_banco_datos.nro_tarjeta) {
            this.solicitudService.isLoadingAgain = true;
            await this.soapuiService.getTarjetaDebitoCuentas(this.solicitudService.persona_banco_datos.nro_tarjeta+'').subscribe(
                async (res) => {
                    this.solicitudService.isLoadingAgain = false;
                    this.solicitudService.persona_banco_tarjetas_debito = [];
                    let response = res as {
                        status: string;
                        message: string;
                        data: any;
                    };
                    if (isObject(response.data) && Object.keys(response.data).length) {
                        if (response.data.Cuentas != undefined) {
                            this.solicitudService.persona_banco_accounts = [];
                            if (!this.guardandoTitular && !this.solicitudService.editandoTitular) {
                                this.displayNroCuentasActualizados = true;
                            }
                            if (response.data.Cuentas.nro_cuenta != undefined) {
                                this.solicitudService.Cuentas = [{label: "Seleccione Nro de Cuenta", value: null}];
                                this.solicitudService.persona_banco_tarjetas_debito.push(response.data.Cuentas);
                                this.parametroMoneda = this.solicitudService.parametrosMonedas.find((params) => params.parametro_descripcion == response.data.Cuentas.moneda.trim());
                                const personaBancoAccount: persona_banco_account = new persona_banco_account();
                                personaBancoAccount.nrocuenta = response.data.Cuentas.nro_cuenta;
                                personaBancoAccount.moneda = this.parametroMoneda.parametro_cod;
                                personaBancoAccount.fecha_expiracion = new Date(response.data.Cuentas.fecha_expiracion);
                                personaBancoAccount.cod_agenda =
                                    this.solicitudService.persona_banco.cod_agenda;
                                this.solicitudService.persona_banco_accounts.push(
                                    personaBancoAccount
                                );
                                this.solicitudService.persona_banco_account.fecha_expiracion = new Date(response.data.Cuentas.fecha_expiracion);
                                this.solicitudService.Cuentas.push({
                                    label: personaBancoAccount.nrocuenta + " " + this.solicitudService.MonedasParametroCod[personaBancoAccount.moneda+''],
                                    value: personaBancoAccount.nrocuenta,
                                });
                            } else {
                                this.solicitudService.Cuentas = [{label: "Seleccione Nro de Cuenta", value: null}];
                                let keys = Object.keys(response.data.Cuentas);
                                let values = Object.values(response.data.Cuentas);
                                values.forEach((value: persona_banco_tarjeta_debito) => {
                                    this.solicitudService.persona_banco_tarjetas_debito.push(value);
                                    this.parametroMoneda = this.solicitudService.parametrosMonedas.find((params) => params.parametro_descripcion == value.moneda.trim());
                                    const personaBancoAccount: persona_banco_account = new persona_banco_account();
                                    personaBancoAccount.nrocuenta = value.nro_cuenta;
                                    personaBancoAccount.moneda = this.parametroMoneda.parametro_cod;
                                    personaBancoAccount.fecha_expiracion = new Date(value.fecha_expiracion + '');
                                    personaBancoAccount.cod_agenda = this.solicitudService.persona_banco.cod_agenda;
                                    this.solicitudService.persona_banco_accounts.push(personaBancoAccount);
                                    this.solicitudService.persona_banco_account.fecha_expiracion = new Date(value.fecha_expiracion + '');
                                    this.solicitudService.Cuentas.push({
                                        label: personaBancoAccount.nrocuenta + " " + this.solicitudService.MonedasParametroCod[personaBancoAccount.moneda+''],
                                        value: personaBancoAccount.nrocuenta
                                    });
                                });
                            }
                        }
                    } else {
                        if (this.solicitudService.persona_banco.cod_agenda != "" && this.solicitudService.persona_banco.cod_agenda != undefined && this.solicitudService.persona_banco.cod_agenda != "undefined") {
                            await this.getDatosFromAccountService((res) => {
                                this.solicitudService.persona_banco_tarjetas_debito = [];
                                this.displayNroTarjetaSinCuentas = true;
                                if (typeof callback == "function") {
                                    callback();
                                }
                            });
                        } else {
                            this.solicitudService.persona_banco_accounts = [];
                            this.solicitudService.Cuentas = [];
                            this.solicitudService.persona_banco_account = new persona_banco_account();
                            if (typeof callback == "function") {
                                await callback();
                            }
                        }
                    }
                    if (typeof callback == "function") {
                        await callback();
                    }
                },
                (err) => {
                    if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                        this.router.navigate([""]);
                    } else {
                        this.solicitudService.nombreServicioBanco = 'getTarjetaDebitoCuentas';
                        this.solicitudService.displayErrorRespuestaBanco = true;
                    }
                }
            );
        } else {
            this.displayIntroduscaNroTarjeta = true;
        }
    }

    async setAnexoPoliza() {
        await this.anexoService.findAnexoPoliza(this.solicitudService.poliza.id).subscribe(async (res) => {
                let response = res as { status: string; message: string; data: Anexo_poliza[]; };
                this.solicitudService.anexosPoliza = response.data;
                this.solicitudService.anexosPoliza.forEach((anexoPoliza: Anexo_poliza) => {
                    this.solicitudService.Planes.push({label: anexoPoliza.descripcion, value: anexoPoliza.id,});
                    this.solicitudService.planes.push(anexoPoliza);
                    this.solicitudService.PlanesParametroCod[anexoPoliza.id+''] = anexoPoliza.descripcion;
                    this.solicitudService.anexoAsegurado = anexoPoliza.anexo_asegurados.find((param) => param.id_tipo == this.solicitudService.parametroAsegurado.id);
                    if (this.solicitudService.anexoAsegurado) {
                        this.solicitudService.isRenovated = this.solicitudService.asegurado.instancia_poliza.id == this.solicitudService.asegurado.instancia_poliza.id_instancia_renovada ? false : true;
                        switch (this.solicitudService.anexoAsegurado.id_edad_unidad) {
                            case this.solicitudService.parametroYear.id:
                                this.solicitudService.edadMinimaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                this.solicitudService.edadMaximaYears = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                break;
                            case this.solicitudService.parametroMonth.id:
                                this.solicitudService.edadMinimaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                this.solicitudService.edadMaximaMonths = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                break;
                            case this.solicitudService.parametroDay.id:
                                this.solicitudService.edadMinimaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_min_permanencia : this.solicitudService.anexoAsegurado.edad_minima;
                                this.solicitudService.edadMaximaDays = this.solicitudService.isRenovated ? this.solicitudService.anexoAsegurado.edad_max_permanencia : this.solicitudService.anexoAsegurado.edad_maxima;
                                break;
                        }
                    }
                });
            },
            (err) => {
                if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                    this.router.navigate([""]);
                } else {
                    console.log(err);
                }
            });
    }

    async setAnexoPolizaPlanes() {
        await this.anexoService.findPlanesAnexoPoliza(this.solicitudService.poliza.id).subscribe(async (res) => {
            let response = res as {
                status: string;
                message: string;
                data: Anexo_poliza[];
            };
            this.solicitudService.anexosPoliza = response.data;
            this.anexosPlanesPoliza = this.solicitudService.anexosPoliza.filter((params) => params.id_tipo == 110);
            this.anexosPlanesPoliza.forEach((anexoPoliza: Anexo_poliza) => {
                this.solicitudService.Planes.push({
                    label: anexoPoliza.descripcion,
                    value: anexoPoliza.id,
                });
                this.solicitudService.planes.push(anexoPoliza);
                this.solicitudService.PlanesParametroCod[anexoPoliza.id+''] = anexoPoliza.descripcion;
            });
        }, (err) => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate([""]);
            } else {
                console.log(err);
            }
        });
    }

    async setAnexoPolizaWithAsegurado() {
        await this.anexoService.findAnexoPolizaWithAsegurado(this.solicitudService.poliza.id, this.solicitudService.asegurado.id).subscribe(
            async (res) => {
                let response = res as {
                    status: string;
                    message: string;
                    data: Anexo_poliza[];
                };
                this.solicitudService.anexosPoliza = response.data;
                if (this.solicitudService.anexosPoliza.length) {
                    this.anexosPlanesPoliza = this.solicitudService.anexosPoliza.filter((params) => params.id_tipo == 110);
                    this.anexosPlanesPoliza.forEach((anexoPoliza: Anexo_poliza) => {
                        this.solicitudService.Planes.push({
                            label: anexoPoliza.descripcion,
                            value: anexoPoliza.id,
                        });
                        this.solicitudService.planes.push(anexoPoliza);
                        this.solicitudService.PlanesParametroCod[
                            anexoPoliza.id+''
                            ] = anexoPoliza.descripcion;
                    });
                }
            }, (err) => {
                if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                    this.router.navigate([""]);
                } else {
                    console.log(err);
                }
            });
    }

    async setAnexoPolizaWithAseguradoAnexo() {
        if (this.solicitudService.persona_banco_datos.plan != 0) {
            await this.anexoService.listaAtributosByIdPoliza(this.solicitudService.poliza.id, this.solicitudService.asegurado.id, this.solicitudService.persona_banco_datos.plan).then(async (res) => {
                let response = res as {
                    status: string;
                    message: string;
                    data: Anexo_poliza[];
                };
                this.solicitudService.anexosPoliza = response.data;
                if (this.solicitudService.anexosPoliza.length) {
                    this.anexosPlanesPoliza = this.solicitudService.anexosPoliza.filter((params) => params.id_tipo === 110);
                    this.anexosPlanesPoliza.forEach(
                        async (anexoPoliza: Anexo_poliza) => {
                            await this.solicitudService.Planes.push({
                                label: anexoPoliza.descripcion,
                                value: anexoPoliza.id,
                            });
                        });
                }
            });
        }
    }

    getLasSolicitudVigente() {
        let finVigenciaDates = [];
        let finVigenciaInstancias = [];
        for (let i = 0; i < this.solicitudService.asegurados.length; i++) {
            let asegurado = this.solicitudService.asegurados[i];
            for (let j = 0; j < asegurado.instancia_poliza.instancia_documentos.length; j++) {
                let instanciaDocumento = asegurado.instancia_poliza.instancia_documentos[j];
                finVigenciaDates.push(new Date(instanciaDocumento.fecha_fin_vigencia + ''));
                finVigenciaInstancias.push(instanciaDocumento.id_instancia_poliza);
            }
        }
        let maxFinVigenciaDate = new Date(Math.max.apply(null, finVigenciaDates));
        let indexFinVigenciaDate = finVigenciaDates.findIndex((param) => param+'' == maxFinVigenciaDate+'');
        let idInstanciaFinVigencia = finVigenciaInstancias[indexFinVigenciaDate];
        let lastFinVigenciaSolicitud = this.solicitudService.asegurados.find((param) => param.instancia_poliza.id == idInstanciaFinVigencia);
        return [idInstanciaFinVigencia, lastFinVigenciaSolicitud];
    }

    async BuscarCliente() {
        this.solicitudService.buscando = true;
        this.iniciarSolicitud();
        this.stateButtonsFlow();
        // this.getPoliza();
        this.solicitudService.setUserLogin();
        this.solicitudService.editandoTitular = false;
        this.solicitudService.isLoadingAgain = true;
        this.solicitudService.isRenovated = false;
        this.solicitudService.msgAlertRenew = "";
        this.solicitudService.msgRenew = "";
        if (this.solicitudService.doc_id && this.solicitudService.extension) {
            await this.getDatosFromCustomerService(async (isBankClient) => {
                let persona_doc_id = this.solicitudService.doc_id;
                let persona_doc_id_ext = this.solicitudService.extension;
                if (this.solicitudService.persona_banco && Object.keys(this.solicitudService.persona_banco).length) {
                    if (persona_doc_id && persona_doc_id_ext) {
                        await this.personaService.findPersonasAseguradasConAtributosByDocIdYPoliza(persona_doc_id, this.solicitudService.objetoAseguradoDatosComplementarios.id, this.solicitudService.id_poliza).subscribe(async (res) => {
                            this.solicitudService.componentsBehavior(async () => {
                                // await this.setAnexoPolizaPlanes();
                                let response = res as { status: string; message: string; data: Asegurado[]; };
                                this.solicitudService.asegurados = response.data;
                                if (this.solicitudService.asegurados.length) {
                                    this.aseguradoWithDiferentDocIdExt = this.solicitudService.asegurados.find(param => param.entidad.persona.persona_doc_id_ext != persona_doc_id_ext);
                                    if (this.aseguradoWithDiferentDocIdExt) {
                                        // this.solicitudService.displayActualizacionPersona = true;
                                        await this.cambiarPersonaExtension(this.aseguradoWithDiferentDocIdExt.entidad.persona.id, this.aseguradoWithDiferentDocIdExt.id_instancia_poliza, this.solicitudService.doc_id, this.solicitudService.extension);
                                        this.solicitudService.isLoadingAgain = false;
                                    } else {
                                        await this.solicitudService.afterSearchWithRenovation(this.solicitudService.asegurado.entidad.persona.persona_doc_id, this.solicitudService.asegurado.entidad.persona.persona_doc_id_ext, isBankClient, async () => {
                                            this.solicitudService.isLoadingAgain = false;
                                            await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                                            this.stateButtonsFlow();
                                        });
                                    }
                                } else {
                                    // await this.abrirModalRegistroCliente();
                                    await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                                    // this.solicitudService.asegurado.instancia_poliza = new Instancia_poliza();
                                    // this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
                                    if (!isBankClient) {
                                        this.solicitudService.editPaterno = true;
                                        this.solicitudService.editMaterno = true;
                                        this.solicitudService.editPrimerApellido = true;
                                        this.solicitudService.editSegundoApellido = true;
                                        this.solicitudService.editPrimerNombre = true;
                                        this.solicitudService.editSegundoNombre = true;
                                        this.solicitudService.editApCasada = true;
                                        this.solicitudService.editFechaNac = true;
                                        this.solicitudService.editCiudadNacimiento = true;
                                        this.solicitudService.editPaisNacimiento = true;
                                        this.solicitudService.editDireccionDomicilio = true;
                                        this.solicitudService.editDireccionLaboral = true;
                                        this.solicitudService.editTelefonoCelular = true;
                                        this.solicitudService.editTelefonoTrabajo = true;
                                        this.solicitudService.editTelefonoDomicilio = true;
                                        this.solicitudService.editEmail = true;
                                        this.abrirModalRegistroCliente();
                                    } else {
                                        this.solicitudService.displayClienteExistente = true;
                                    }
                                    this.solicitudService.isLoadingAgain = false;
                                    this.stateButtonsFlow();
                                }
                                this.setAtributosToAsegurado();
                                this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter = [];
                                await this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.forEach(async (atributo_instancia_poliza: Atributo_instancia_poliza) => {
                                    if (atributo_instancia_poliza.objeto_x_atributo.par_comportamiento_interfaz) {
                                        if (atributo_instancia_poliza.objeto_x_atributo.par_comportamiento_interfaz.id == this.solicitudService.parametroVisible.id) {
                                            this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter.push(atributo_instancia_poliza);
                                        }
                                    }
                                });
                            });
                        }, (err) => {
                            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                                this.router.navigate(["login"]);
                            } else {
                                console.log(err);
                            }
                        });
                    } else {
                        this.displayFaltanDatos = true;
                    }
                } else {
                    this.solicitudService.persona_banco_account = new persona_banco_account();
                    this.solicitudService.persona_banco_datos = new persona_banco_datos();
                    this.solicitudService.persona_banco = new persona_banco();
                    await this.newAsegurado(persona_doc_id, persona_doc_id_ext);
                    this.solicitudService.displayClienteExistente = true;
                    this.solicitudService.isLoadingAgain = false;
                    this.stateButtonsFlow();
                }
            });
        } else {
            this.displayFaltanDatos = true;
            this.solicitudService.isLoadingAgain = false;
        }
        this.cols = [];
    }

    async newAsegurado(persona_doc_id, persona_doc_id_ext) {
        let oldAsegurado: Asegurado = new Asegurado();
        if (isObject(this.solicitudService.asegurado)) {
            oldAsegurado = this.solicitudService.asegurado;
            if (oldAsegurado.instancia_poliza.atributo_instancia_polizas) {
                oldAsegurado.instancia_poliza.atributo_instancia_polizas.forEach(async (atributo_instancia_poliza: Atributo_instancia_poliza) => {
                    atributo_instancia_poliza.valor = "";
                });
                if (oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter) {
                    oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter.forEach(async (atributo_instancia_poliza: Atributo_instancia_poliza) => {
                        atributo_instancia_poliza.valor = "";
                    });
                }
            } else {
                oldAsegurado.instancia_poliza.atributo_instancia_polizas = [];
                oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter = [];
            }
        }
        oldAsegurado.entidad.persona.persona_doc_id = persona_doc_id;
        oldAsegurado.entidad.persona.persona_doc_id_ext = persona_doc_id_ext;
        this.solicitudService.persona_banco.doc_id = persona_doc_id.trim();
        this.solicitudService.persona_banco.extension = persona_doc_id_ext+'';
        this.solicitudService.asegurado = new Asegurado();
        this.BeneficiariosAux = [];
        //this.solicitudService.usuario_banco = this.solicitudService.usuarioLogin.usuario_banco;
        this.solicitudService.asegurado.instancia_poliza.id_anexo_poliza = this.solicitudService.poliza.anexo_poliza.id;
        this.solicitudService.asegurado.instancia_poliza.estado = this.solicitudService.estadoIniciado;
        this.solicitudService.asegurado.instancia_poliza.poliza = this.solicitudService.poliza;
        this.solicitudService.asegurado.instancia_poliza.id_estado = this.solicitudService.estadoIniciado.id;
        this.solicitudService.asegurado.instancia_poliza.id_poliza = this.solicitudService.poliza.id;
        this.solicitudService.instanciaDocumentoSolicitud = new Instancia_documento();
        this.solicitudService.instanciaDocumentoSolicitud.documento = this.solicitudService.documentoSolicitud;
        if (
            !this.solicitudService.asegurado.instancia_poliza.instancia_documentos.find((param) => param.id_documento == this.solicitudService.instanciaDocumentoSolicitud.id_documento)
        ) {
            this.solicitudService.asegurado.instancia_poliza.instancia_documentos.push(this.solicitudService.instanciaDocumentoSolicitud);
        }
        this.solicitudService.asegurado.entidad = oldAsegurado.entidad;
        this.solicitudService.persona_banco_datos.ocupacion = this.solicitudService.persona_banco.caedec && this.solicitudService.persona_banco.desc_caedec ? this.solicitudService.persona_banco.caedec + " - " + this.solicitudService.persona_banco.desc_caedec : "";
        this.solicitudService.persona_banco_datos.modalidad_pago = this.solicitudService.Periodicidad[2].value+'';
        this.solicitudService.persona_banco_datos.plazo = 12;
        this.solicitudService.persona_banco_datos.tipo_doc = "CI";
        this.solicitudService.persona_banco_datos.desc_ocupacion = '';
        this.solicitudService.persona_banco_datos.ocupacion = '';
        this.solicitudService.persona_banco.debito_automatico = this.solicitudService.Condiciones[1].value;
        this.solicitudService.persona_banco_datos.razon_social = this.solicitudService.persona_banco.paterno + " " + this.solicitudService.persona_banco.nombre;
        this.solicitudService.persona_banco_datos.nit_carnet = this.solicitudService.persona_banco.doc_id+'';
        this.solicitudService.atributoNroCuenta = new Atributo_instancia_poliza();
        this.solicitudService.atributoNroDireccion = new Atributo_instancia_poliza();
        this.solicitudService.atributoEmail = new Atributo_instancia_poliza();
        this.solicitudService.atributoUltimosCuatroDigitos = new Atributo_instancia_poliza();
        this.solicitudService.atributoRazonSocial = new Atributo_instancia_poliza();
        this.solicitudService.atributoOcupacion = new Atributo_instancia_poliza();
        this.solicitudService.atributoAmpConAmbMedGeneral = new Atributo_instancia_poliza();
        this.solicitudService.atributoAmpConAmbMedEspecializada = new Atributo_instancia_poliza();
        this.solicitudService.atributoAmpSumAmbMedicamentos = new Atributo_instancia_poliza();
        this.solicitudService.atributoAmpSumExaLaboratorio = new Atributo_instancia_poliza();
        this.solicitudService.atributoNitCarnet = new Atributo_instancia_poliza();
        this.solicitudService.atributoCtaFechaExpiracion = new Atributo_instancia_poliza();
        this.solicitudService.atributoPlan = new Atributo_instancia_poliza();
        this.solicitudService.atributoNroTransaccion = new Atributo_instancia_poliza();
        this.solicitudService.atributoTransaccionImporte = new Atributo_instancia_poliza();
        this.solicitudService.atributoTransaccionDetalle = new Atributo_instancia_poliza();
        this.solicitudService.atributoFechaTransaccion = new Atributo_instancia_poliza();
        this.solicitudService.atributoTransaccionMoneda = new Atributo_instancia_poliza();
        this.solicitudService.atributoPlazo = new Atributo_instancia_poliza();
        this.solicitudService.atributoAgencia = new Atributo_instancia_poliza();
        this.solicitudService.atributoPrima = new Atributo_instancia_poliza();
        this.solicitudService.atributoTelefonoCelular = new Atributo_instancia_poliza();
        this.solicitudService.atributoCiudadNacimiento = new Atributo_instancia_poliza();
        this.solicitudService.atributoSucursal = new Atributo_instancia_poliza();
        this.solicitudService.atributoUsuarioCargo = new Atributo_instancia_poliza();
        this.solicitudService.atributoZona = new Atributo_instancia_poliza();
        this.solicitudService.atributoDebitoAutomatico = new Atributo_instancia_poliza();
        this.solicitudService.aseguradoBeneficiarios = [];
        this.solicitudService.atributoMoneda = new Atributo_instancia_poliza();
        this.solicitudService.atributoNroTarjeta = new Atributo_instancia_poliza();
        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas = oldAsegurado.instancia_poliza.atributo_instancia_polizas;
        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter = oldAsegurado.instancia_poliza.atributo_instancia_polizas_inter;
        await this.componentsBehaviorOnInit();
        await this.setAtributosToAsegurado();
        //await this.solicitudService.componentsBehavior();
        await this.solicitudService.setBeneficiarios();
        this.setPago(this.solicitudService.persona_banco.debito_automatico);
        if (this.transicionesComponent != undefined) {
            this.transicionesComponent.ngOnInit();
        }
    }

    selectNroCuenta(event) {
        if (event.value) {
            let value = event.value;
            if (this.solicitudService.persona_banco_accounts.length) {
                this.solicitudService.persona_banco_account = this.solicitudService.persona_banco_accounts.find((params) => params.nrocuenta === value);
                if (this.solicitudService.persona_banco_account) {
                    this.userform.controls["par_moneda"].setValue(this.solicitudService.persona_banco_account.moneda);
                }
            }
            // this.setAtributosToAsegurado();
        }
    }

    selectPagoEfectivo(event) {
        if (event.value) {
            let value = event.value;
            this.setPago(value);
            // this.setAtributosToAsegurado();
        }
    }

    setPago(value) {
        if (this.solicitudService.condicionSi.id == value) {
            this.solicitudService.showNroCuenta = false;
            this.solicitudService.showMoneda = false;
            this.userform.controls["par_numero_cuenta"].clearValidators();
            this.userform.controls["par_moneda"].clearValidators();
            this.userform.controls["par_numero_cuenta"].setValue("");
            this.userform.controls["par_moneda"].setValue("");
        } else {
            this.userform.controls["par_numero_cuenta"].setValidators(Validators.required);
            this.userform.controls["par_moneda"].setValidators(Validators.required);
            this.solicitudService.showMoneda = true;
            this.solicitudService.showNroCuenta = true;
        }
    }

    selectPlan(event) {
        if (event.value) {
            let value = event.value;
            if (this.solicitudService.Planes.length) {
                this.plan = this.solicitudService.planes.find((params) => params.id === value);
                if (this.plan) {
                    this.solicitudService.persona_banco_datos.plan = this.plan.id;
                    this.userform.controls["par_prima"].setValue(this.plan.monto_prima);
                }
            }
            // this.setAtributosToAsegurado();
        }
    }

    async abrirModalRegistroCliente() {
        //this.solicitudService.displayModalSolicitudExistente = false;
        this.solicitudService.setUserLogin();
        this.stateButtonsFlow();
        this.setPago(this.solicitudService.persona_banco.debito_automatico);
        await this.setAtributosToAsegurado();
        this.solicitudService.showFormValidation(this.userform);
        this.solicitudService.displayModalFormTitular = true;
        this.solicitudService.displayModalSolicitudExistenteAlertRenovacion = false;
        this.solicitudService.displayBusquedaCI = false;
        this.collapsedFormTitular = false;
        await this.solicitudService.validarWarnsOrErrors(() => {
            if (this.solicitudService.userFormHasErrors && this.solicitudService.userFormHasWarnings) {
                if (this.solicitudService.esClienteBanco) {
                    this.displayValidacionAlInicio = true;
                } else {
                    this.displayValidacionAlInicio = false;
                }
                this.solicitudService.msgText = "La solicitud tiene las siguientes advertencias y observaciones que deben ser resueltas:";
            } else if (this.solicitudService.userFormHasWarnings) {
                if (this.solicitudService.esClienteBanco) {
                    this.displayValidacionAlInicio = true;
                } else {
                    this.displayValidacionAlInicio = false;
                }
                this.solicitudService.msgText = "La solicitud tiene las siguientes advertencias: ";
            }
        });
        this.enableUserform();
    }

    iniciarSolicitud() {
        this.solicitudService.msgs_warn = [];
        this.solicitudService.msgs_error = [];
        this.stateButtonsFlow();
    }

    cancelarSolicitud() {
        this.iniciarSolicitud();
        this.solicitudService.displayModalSolicitudExistente = false;
        this.solicitudService.displayModalSolicitudExistenteConRenovacion = false;
        this.solicitudService.asegurado = new Asegurado();
        this.solicitudService.persona_banco = new persona_banco();
        this.solicitudService.persona_banco_datos = new persona_banco_datos();
        this.solicitudService.persona_banco_account = new persona_banco_account();
        this.solicitudService.persona_banco_accounts = [];
        this.solicitudService.persona_banco_pep = new persona_banco_pep();
        this.sessionStorageService.removeItem('parametros');
        this.sessionStorageService.setItemSync('paramsDeleted', true);
    }

    abrirVentanaTitular() {
        this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_apellido_casada = "";
        this.stateButtonsFlow();
        this.solicitudService.displayModalFormTitular = true;
        this.displayFormTitular = true;
    }

    abrirVentanaRegistroNuevoTitular() {
        this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_nombre = "";
        this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = "";
        this.solicitudService.asegurado.entidad.persona.persona_apellido_casada = "";
        this.displayFormTitular = true;
        this.displayDatosTitular = true;
        this.solicitudService.isLoadingAgain = false;
        this.stateButtonsFlow();
        this.solicitudService.displayModalFormTitular = true;
    }

    onRowSelectDato_complementario(even: any) {
    }

    async EnviarDocumentosADestinatariosEcoVida() {
        await this.validarCorreos();
        if (!this.stopSending) {
            //this.solicitudService.displayEnviarDocumento=false;
            this.solicitudService.envioDestinatariosExitoso = true;
        }
    }

    async onApellidoInput() {
        this.userform.controls["persona_primer_apellido"].clearAsyncValidators();
        this.userform.controls["persona_primer_apellido"].setErrors(null);
        this.userform.controls["persona_segundo_apellido"].clearAsyncValidators();
        this.userform.controls["persona_segundo_apellido"].setErrors(null);
    }

    async validarApellidos() {
        if (
            this.solicitudService.persona_banco.paterno == "" &&
            this.solicitudService.persona_banco.materno == "" &&
            this.solicitudService.persona_banco.apcasada == ""
        ) {
            this.userform.controls["persona_primer_apellido"].setErrors([Validators.pattern("^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$"),]);
            this.userform.controls["persona_segundo_apellido"].setErrors([Validators.pattern("^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$"),]);
            this.solicitudService.atributoApellidoPaterno.requerido = true;
            this.solicitudService.isLoadingAgain = false;
            this.stopSavingByLastName = true;
        } else {
            //this.onApellidoInput();
            this.stopSavingByLastName = false;
        }
    }

    async validarCorreos() {
        if (this.solicitudService.destinatariosCorreos != "") {
            let correos = this.solicitudService.destinatariosCorreos.split(",");
            let correosValidos = true;
            correos.forEach((correo) => {
                if (!this.solicitudService.validarEmail(correo)) {
                    correosValidos = false;
                }
            });
            if (!correosValidos) {
                this.envioDocumentosForm.controls["destinatarios_correos"].setErrors([Validators.email]);
                this.solicitudService.atributoDestinatarios.requerido = true;
                this.solicitudService.atributoFechaNacimiento.tipo_error = "Uno de los correos no es valido, por favor revisalos";
                this.solicitudService.isLoadingAgain = false;
                this.stopSending = true;
            } else {
                this.stopSending = false;
            }
        } else {
            this.envioDocumentosForm.controls["destinatarios_correos"].clearAsyncValidators();
            this.envioDocumentosForm.controls["destinatarios_correos"].setErrors(null);
            this.stopSending = false;
        }
    }


    async afterSearch() {
        await this.solicitudService.afterSearchWithRenovation(this.solicitudService.doc_id, this.solicitudService.extension, this.solicitudService.esClienteBanco, async () => {
            this.solicitudService.isLoadingAgain = false;
            await this.newAsegurado(this.solicitudService.doc_id, this.solicitudService.extension);
            this.stateButtonsFlow();
        });
    }

    async afterAlertEdad() {
        this.userform.controls["persona_fecha_nacimiento"].setErrors(null);
        this.stopSavingByEdad = false;
    }

    async validarFechaNacimiento() {
        let fechaMin = this.solicitudService.getFechaMinimaSegunEdad();
        let fechaMax = this.solicitudService.getFechaMaximaSegunEdad();
        let edadYears, edadMonths, edadDays, edadMiliseconds;
        if (this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento) {
            edadMiliseconds = new Date().getTime() - this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
            edadYears = moment().diff(this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento, "years");
            edadMonths = moment().diff(this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento, "months");
            edadDays = moment().diff(this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento, "days");
            if (fechaMin && fechaMax) {
                //let minTime = fechaMin.getTime();
                // 567648000000 = 18 años
                let edadMinimaMiliseconds, edadMaximaMiliseconds;
                switch (this.solicitudService.anexoAsegurado.id_edad_unidad) {
                    case this.solicitudService.parametroYear.id:
                        edadMinimaMiliseconds = this.solicitudService.edadMinimaYears * 31556900000;
                        edadMaximaMiliseconds = this.solicitudService.edadMaximaYears * 31556900000;
                        this.solicitudService.edadAsegurado = edadYears;
                        break;
                    case this.solicitudService.parametroMonth.id:
                        edadMinimaMiliseconds = this.solicitudService.edadMinimaYears * 2629750000;
                        edadMaximaMiliseconds = this.solicitudService.edadMaximaYears * 2629750000;
                        this.solicitudService.edadAsegurado = edadYears;
                        break;
                    case this.solicitudService.parametroDay.id:
                        edadMinimaMiliseconds = this.solicitudService.edadMinimaYears * 86400000;
                        edadMaximaMiliseconds = this.solicitudService.edadMaximaYears * 86400000;
                        this.solicitudService.edadAsegurado = edadYears;
                        break;
                }
                switch (this.solicitudService.anexoAsegurado.id_edad_unidad) {
                    case this.solicitudService.parametroYear.id:
                        this.solicitudService.edadAsegurado = edadYears;
                        if (edadYears < this.solicitudService.edadMinimaYears) {
                            this.solicitudService.atributoFechaNacimiento.tipo_error = `La persona es menor de ${this.solicitudService.edadMinimaYears} ${this.solicitudService.anexoAsegurado.edad_unidad.parametro_descripcion}`;
                            this.solicitudService.isLoadingAgain = false;
                            this.userform.controls["persona_fecha_nacimiento"].setErrors([Validators.min(fechaMin.getTime())]);
                            this.stopSavingByEdad = true;
                            if (!this.validandoContinuando) {
                                this.displayEdadIncorrecta = true;
                            }
                            return "";
                        } else if (edadYears >= this.solicitudService.edadMaximaYears) {
                            this.solicitudService.atributoFechaNacimiento.tipo_error = `La edad de la persona es mayor o igual a ${this.solicitudService.edadMaximaYears} ${this.solicitudService.anexoAsegurado.edad_unidad.parametro_descripcion}`;
                            this.solicitudService.isLoadingAgain = false;
                            this.userform.controls["persona_fecha_nacimiento"].setErrors([Validators.max(fechaMax.getTime())]);
                            this.stopSavingByEdad = true;
                            if (!this.validandoContinuando) {
                                this.displayEdadIncorrecta = true;
                            }
                            return "";
                        } else {
                            this.userform.controls["persona_fecha_nacimiento"].clearAsyncValidators();
                            this.userform.controls["persona_fecha_nacimiento"].setErrors(null);
                            this.stopSavingByEdad = false;
                        }
                        break;
                    case this.solicitudService.parametroMonth.id:
                        this.solicitudService.edadAsegurado = edadMonths;
                        if (edadMonths < this.solicitudService.edadMinimaMonths) {
                            this.solicitudService.atributoFechaNacimiento.tipo_error = `La persona es menor de ${this.solicitudService.edadMinimaMonths} ${this.solicitudService.anexoAsegurado.edad_unidad.parametro_descripcion}`;
                            this.solicitudService.isLoadingAgain = false;
                            this.userform.controls["persona_fecha_nacimiento"].setErrors([Validators.min(fechaMin.getTime())]);
                            this.stopSavingByEdad = true;
                            if (!this.validandoContinuando) {
                                this.displayEdadIncorrecta = true;
                            }
                            return "";
                        } else if (edadMonths >= this.solicitudService.edadMaximaMonths) {
                            this.solicitudService.atributoFechaNacimiento.tipo_error = `La edad de la persona es mayor o igual a ${this.solicitudService.edadMaximaMonths} ${this.solicitudService.anexoAsegurado.edad_unidad.parametro_descripcion}`;
                            this.solicitudService.isLoadingAgain = false;
                            this.userform.controls["persona_fecha_nacimiento"].setErrors([Validators.max(fechaMax.getTime())]);
                            this.stopSavingByEdad = true;
                            if (!this.validandoContinuando) {
                                this.displayEdadIncorrecta = true;
                            }
                            return "";
                        } else {
                            this.userform.controls["persona_fecha_nacimiento"].clearAsyncValidators();
                            this.userform.controls["persona_fecha_nacimiento"].setErrors(null);
                            this.stopSavingByEdad = false;
                        }
                        break;
                    case this.solicitudService.parametroDay.id:
                        this.solicitudService.edadAsegurado = edadDays;
                        if (edadDays < this.solicitudService.edadMinimaDays) {
                            this.solicitudService.atributoFechaNacimiento.tipo_error = `La persona es menor de ${this.solicitudService.edadMinimaDays} ${this.solicitudService.anexoAsegurado.edad_unidad.parametro_descripcion}`;
                            this.solicitudService.isLoadingAgain = false;
                            this.userform.controls["persona_fecha_nacimiento"].setErrors([Validators.min(fechaMin.getTime())]);
                            this.stopSavingByEdad = true;
                            if (!this.validandoContinuando) {
                                this.displayEdadIncorrecta = true;
                            }
                            return "";
                        } else if (edadDays >= this.solicitudService.edadMaximaDays) {
                            this.solicitudService.atributoFechaNacimiento.tipo_error = `La edad de la persona es mayor o igual a ${this.solicitudService.edadMaximaDays} ${this.solicitudService.anexoAsegurado.edad_unidad.parametro_descripcion}`;
                            this.solicitudService.isLoadingAgain = false;
                            this.userform.controls["persona_fecha_nacimiento"].setErrors([Validators.max(fechaMax.getTime())]);
                            this.stopSavingByEdad = true;
                            if (!this.validandoContinuando) {
                                this.displayEdadIncorrecta = true;
                            }
                            return "";
                        } else {
                            this.userform.controls["persona_fecha_nacimiento"].clearAsyncValidators();
                            this.userform.controls["persona_fecha_nacimiento"].setErrors(null);
                            this.stopSavingByEdad = false;
                        }
                        break;
                    default:
                        this.solicitudService.atributoFechaNacimiento.tipo_error = `La póliza ${this.solicitudService.poliza.descripcion} no tiene resgitrado la unidad del rango de edad admitido, por favor contáctese con soporte colibri`;
                        this.solicitudService.isLoadingAgain = false;
                        this.userform.controls["persona_fecha_nacimiento"].setErrors([
                            Validators.max(fechaMax.getTime()),
                            Validators.min(fechaMin.getTime()),
                        ]);
                        this.stopSavingByEdad = true;
                        if (!this.validandoContinuando) {
                            this.displayEdadIncorrecta = true;
                        }
                        return "";
                }
            }
            return false;
        }
        return false;
    }

    async validarFechaMinima() {
        let fechaMin = this.solicitudService.getFechaMinimaSegunEdad();
        let diff = new Date().getTime() - this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
        let yearsMiliseconds = 5.676e11; // 18 años;
        if (diff < fechaMin.getTime()) {
            this.userform.controls["persona_fecha_nacimiento"].setErrors([Validators.min(fechaMin.getTime())]);
            this.solicitudService.atributoFechaNacimiento.tipo_error = "La persona no es mayor de 18 años";
            this.solicitudService.isLoadingAgain = false;
            this.stopSaving = true;
        } else {
            this.userform.controls["persona_fecha_nacimiento"].clearAsyncValidators();
            this.userform.controls["persona_fecha_nacimiento"].setErrors(null);
            this.stopSaving = false;
        }
    }

    async validarFechaMaxima() {
        let fechaMax = this.solicitudService.getFechaMaximaSegunEdad();
        let diff = new Date().getTime() - this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento.getTime();
        let yearsMiliseconds = 2.05e12; // 65 años;
        if (diff > fechaMax.getTime()) {
            this.userform.controls["persona_fecha_nacimiento"].setErrors([Validators.max(fechaMax.getTime())]);
            this.solicitudService.atributoFechaNacimiento.tipo_error = "La persona es mayor de 65 años";
            this.solicitudService.isLoadingAgain = false;
            this.stopSaving = true;
        } else {
            this.userform.controls["persona_fecha_nacimiento"].clearAsyncValidators();
            this.userform.controls["persona_fecha_nacimiento"].setErrors(null);
            this.stopSaving = false;
        }
    }

    async guardarSolicitudValidando() {
        await this.setAnexoPoliza();
        await this.setAtributosToAsegurado(async () => {
            await this.validarApellidos();
            await this.validarFechaNacimiento();
        });
        this.guardarSolicitudPersona();
    }

    async guardarSolicitudPersona() {
        if (
            !this.stopSaving &&
            !this.stopSavingByEdad &&
            !this.stopSavingByLastName
        ) {
            this.solicitudService.isLoadingAgain = true;
            this.stateButtonsFlow();
            if (
                this.solicitudService.editandoTitular &&
                this.solicitudService.asegurado.instancia_poliza.id
            ) {
                this.solicitudService.editandoTitular = false;
                this.personaService.actualizarSolicitud(this.solicitudService.asegurado).subscribe(
                    async (res) => {
                        let response = res as {
                            status: string;
                            message: string;
                            data: Asegurado;
                        };
                        this.solicitudService.buscando = false;
                        this.solicitudService.asegurado = response.data;
                        // this.setFeaturesBeneficiarios(this.solicitudService.asegurado.beneficiarios);
                        this.solicitudService.persona_banco_account = new persona_banco_account();
                        this.solicitudService.persona_banco = new persona_banco();
                        this.solicitudService.persona_banco_datos = new persona_banco_datos();
                        await this.stateButtonsFlow();
                        await this.solicitudService.setDatesOfAsegurado();
                        await this.solicitudService.setAtributosToPersonaBanco(this.userform);
                        await this.solicitudService.setBeneficiarios();
                        this.componenteBeneficiario.ngOnInit();
                        this.solicitudService.isLoadingAgain = false;
                    },
                    (err) => {
                        if (
                            err.error.statusCode === 400 &&
                            err.error.message === "usuario no autentificado"
                        ) {
                            this.router.navigate([""]);
                        } else {
                            console.log(err);
                        }
                    }
                );
            } else {
                this.personaService.crearNuevaSolicitud(this.solicitudService.asegurado).subscribe(
                    async (res) => {
                        let response = res as {
                            status: string;
                            message: string;
                            data: Asegurado;
                        };
                        this.solicitudService.buscando = false;
                        this.solicitudService.asegurado = response.data;
                        // this.setFeaturesBeneficiarios(this.solicitudService.asegurado.beneficiarios);
                        this.solicitudService.persona_banco_account = new persona_banco_account();
                        this.solicitudService.persona_banco = new persona_banco();
                        this.solicitudService.persona_banco_datos = new persona_banco_datos();
                        await this.stateButtonsFlow();
                        await this.solicitudService.setDatesOfAsegurado();
                        await this.solicitudService.setAtributosToPersonaBanco(this.userform);
                        await this.solicitudService.setBeneficiarios();
                        // await this.setSolicitudAsIniciado();
                        this.componenteBeneficiario.ngOnInit();
                        this.solicitudService.isLoadingAgain = false;
                    },
                    (err) => {
                        if (
                            err.error.statusCode === 400 &&
                            err.error.message === "usuario no autentificado"
                        ) {
                            this.router.navigate([""]);
                        } else {
                            console.log(err);
                        }
                    }
                );
            }
            this.displayDatosTitular = true;
            this.collapsedDatosTitular = false;
            this.solicitudService.displayModalDatosTitular = true;
            this.solicitudService.displayModalFormTitular = false;
        }
    }

    async onAcceptValidationInit() {
        this.displayValidacionAlInicio = false;
        if (
            this.userform.controls["par_debito_automatico_id"].value == null ||
            this.userform.controls["par_debito_automatico_id"].value == ""
        ) {
            this.solicitudService.showNroCuenta = false;
            this.solicitudService.showMoneda = false;
        }
        if (this.solicitudService.persona_banco_accounts.length == 0) {
            //this.cancelarSolicitud();
            this.stateButtonsFlow();
            this.solicitudService.displayModalFormTitular = true;
            //this.solicitudService.displayBusquedaCI = true;
        }
    }

    async setAtributosToAsegurado(callback: Function = null) {
        if (this.solicitudService.asegurado) {
            this.solicitudService.msgs_warn = [];
            this.solicitudService.msgs_error = [];
            this.instanciaPolizaTransService.oldInstanciaPolizaTransiciones = this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions;
            this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions = [];
            this.solicitudService.asegurado.entidad.persona.persona_doc_id = this.solicitudService.persona_banco.doc_id;
            this.solicitudService.asegurado.entidad.persona.persona_doc_id_ext = util.isNumber(this.solicitudService.persona_banco.extension) ? this.solicitudService.persona_banco.extension : this.solicitudService.ProcedenciaCIAbreviacion[this.solicitudService.persona_banco.extension];
            // if(this.displayModalSolicitudExistente) {
            //     this.setMsgsWarnsOrErrors(this.parametroWarning, this.msgSolicitudExistente);
            // }
            //this.solicitudService.persona_banco_datos.modalidad_pago = this.solicitudService.Periodicidad[2].value+'';

            if (this.solicitudService.persona_banco.complemento != null) {
                this.solicitudService.asegurado.entidad.persona.persona_doc_id_comp = this.solicitudService.persona_banco.complemento;
            }
            if (this.solicitudService.persona_banco.nombre != null) {
                this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = this.solicitudService.persona_banco.nombre;
            }
            if (this.solicitudService.persona_banco.paterno != null) {
                this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = this.solicitudService.persona_banco.paterno;
            }
            if (this.solicitudService.persona_banco.materno != null) {
                this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = this.solicitudService.persona_banco.materno;
            }
            if (this.solicitudService.persona_banco.apcasada != null) {
                this.solicitudService.asegurado.entidad.persona.persona_apellido_casada = this.solicitudService.persona_banco.apcasada;
            }
            if (this.solicitudService.persona_banco.e_mail != null) {
                this.solicitudService.asegurado.entidad.persona.persona_email_personal = this.solicitudService.persona_banco.e_mail;
            }
            if (this.solicitudService.persona_banco.direccion != null) {
                this.solicitudService.asegurado.entidad.persona.persona_direccion_domicilio = this.solicitudService.persona_banco.direccion;
            }
            if (this.solicitudService.persona_banco.fono_domicilio != null) {
                this.solicitudService.asegurado.entidad.persona.persona_telefono_domicilio = this.solicitudService.persona_banco.fono_domicilio;
            }
            if (this.solicitudService.persona_banco.nro_celular != null) {
                this.solicitudService.asegurado.entidad.persona.persona_celular = this.solicitudService.persona_banco.nro_celular;
            }
            if (this.solicitudService.persona_banco.fono_oficina != null) {
                this.solicitudService.asegurado.entidad.persona.persona_telefono_trabajo = this.solicitudService.persona_banco.fono_oficina;
            }
            if (this.solicitudService.persona_banco.sexo != null) {
                this.solicitudService.asegurado.entidad.persona.par_sexo_id = this.solicitudService.SexosAbreviacion[this.solicitudService.persona_banco.sexo];
            }
            // if (this.solicitudService.persona_banco.fecha_nacimiento) {
            //     let nums = [], dateFormat = this.solicitudService.persona_banco.fecha_nacimiento + "";
            //     if (typeof this.solicitudService.persona_banco.fecha_nacimiento == "string") {
            //         nums = dateFormat.indexOf("/") ? dateFormat.split("/") : nums;
            //         nums = dateFormat.indexOf("-") ? dateFormat.split("/") : nums;
            //         nums = dateFormat.indexOf(".") ? dateFormat.split("/") : nums;
            //         this.solicitudService.persona_banco.fecha_nacimiento = new Date(nums[2], nums[1] - 1, nums[0]);
            //     }
            //     this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento =
            //         this.solicitudService.persona_banco.fecha_nacimiento;
            // }
            // if (this.solicitudService.persona_banco.fecha_nacimiento_str) {
            //     let nums = [],
            //         dateFormat = this.solicitudService.persona_banco.fecha_nacimiento_str + "";
            //     if (typeof this.solicitudService.persona_banco.fecha_nacimiento_str == "string") {
            //         if (dateFormat.indexOf("/")) {
            //             nums = dateFormat.split("/");
            //             this.solicitudService.persona_banco.fecha_nacimiento_str = `${nums[0]}/${nums[1]}/${nums[2]}`;
            //         } else if (dateFormat.indexOf("-")) {
            //             nums = dateFormat.split("-");
            //             this.solicitudService.persona_banco.fecha_nacimiento_str = `${nums[0]}-${nums[1]}-${nums[2]}`;
            //         } else if (dateFormat.indexOf(".")) {
            //             nums = dateFormat.split(".");
            //             this.solicitudService.persona_banco.fecha_nacimiento_str = `${nums[0]}.${nums[1]}.${nums[2]}`;
            //         }
            //         this.solicitudService.persona_banco.fecha_nacimiento = new Date(nums[2], nums[1] - 1, nums[0]);
            //     }
            //     this.solicitudService.asegurado.entidad.persona.persona_fecha_nacimiento = this.solicitudService.persona_banco.fecha_nacimiento;
            // }

            if (this.solicitudService.asegurado.entidad.persona.persona_primer_apellido == null) {
                this.solicitudService.asegurado.entidad.persona.persona_primer_apellido = "";
            }
            if (this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido == null) {
                this.solicitudService.asegurado.entidad.persona.persona_segundo_apellido = "";
            }
            if (this.solicitudService.asegurado.entidad.persona.persona_primer_nombre == null) {
                this.solicitudService.asegurado.entidad.persona.persona_primer_nombre = "";
            }

            if (this.solicitudService.asegurado.instancia_poliza.id) {
                this.solicitudService.personaTitular = "El titular";
            } else {
                this.solicitudService.personaTitular = "La Persona";
            }

            if (this.solicitudService.persona_banco.doc_id != '' && this.solicitudService.persona_banco.extension != "") {
                this.solicitudService.TiposDocumentosId.forEach(
                    (label, index) => {
                        if (label == this.solicitudService.persona_banco.doc_id) {
                            this.solicitudService.asegurado.entidad.persona.par_tipo_documento_id = index+'';
                        }
                    }
                );
                if (this.solicitudService.asegurado.entidad.persona.par_tipo_documento_id == "" || this.solicitudService.asegurado.entidad.persona.par_tipo_documento_id == undefined) {
                    this.solicitudService.asegurado.entidad.persona.par_tipo_documento_id = "1";
                }
            }
            this.solicitudService.asegurado.entidad.persona.par_pais_nacimiento_id = this.solicitudService.persona_banco_datos.pais_nacimiento = "5";
            this.solicitudService.asegurado.entidad.persona.par_nacionalidad_id = this.solicitudService.NacionalidadesParametroCod["BO"];

            this.solicitudService.setDatesOfAsegurado();
            // await this.solicitudService.setAtributoObservacion( this.solicitudService.asegurado,this.solicitudService.atributoFechaNacimiento.tipo_error = this.solicitudService.personaTitular + ' no tiene la edad entre '+this.edadMinimaYears+' y '+this.edadMaximaYears , this.validarFechaNacimiento(), this.solicitudService.parametroWarning,true,'fecha_nacimiento_str');
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoComplementoDocId.tipo_error = this.solicitudService.personaTitular + " no tiene registrado un complemento del documento de identidad"),
                this.solicitudService.persona_banco.complemento,
                this.solicitudService.parametroWarning,
                true,
                "complemento"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoDocId.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado un número de documento"),
                this.solicitudService.persona_banco.doc_id,
                this.solicitudService.parametroError,
                true,
                "doc_id"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoDocIdExt.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado una extencion del número de documento"),
                this.solicitudService.persona_banco.extension,
                this.solicitudService.parametroError,
                true,
                "extension"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoPrimerNombre.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado un nombre"),
                this.solicitudService.persona_banco.nombre,
                this.solicitudService.parametroWarning,
                true,
                "nombre"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoApellidoPaterno.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado un apellido paterno"),
                this.solicitudService.persona_banco.paterno,
                this.solicitudService.parametroWarning,
                true,
                "paterno"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoApellidoMaterno.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado un apellido materno"),
                this.solicitudService.persona_banco.materno,
                this.solicitudService.parametroWarning,
                true,
                "materno"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoPaisNacimiento.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado un pais de nacimiento"),
                this.solicitudService.persona_banco_datos.pais_nacimiento,
                this.solicitudService.parametroWarning,
                true,
                "pais_nacimiento"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoFechaNacimiento.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado una fecha de nacimiento"),
                this.solicitudService.persona_banco.fecha_nacimiento,
                this.solicitudService.parametroError,
                true,
                "fecha_nacimiento"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoApellidoCasada.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado un apellido de casada"),
                this.solicitudService.persona_banco.apcasada,
                this.solicitudService.parametroWarning,
                true,
                "apcasada"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoDireccionDomicilio.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado una dirección"),
                this.solicitudService.persona_banco.direccion,
                this.solicitudService.parametroWarning,
                true,
                "direccion"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoTelefonoDomicilio.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado un teléfono de domicilio"),
                this.solicitudService.persona_banco.fono_domicilio,
                this.solicitudService.parametroWarning,
                true,
                "fono_domicilio"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoTelefonoTrabajo.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado un teléfono de trabajo"),
                this.solicitudService.persona_banco.fono_oficina,
                this.solicitudService.parametroWarning,
                true,
                "fono_oficina"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoTelefonoCelular.tipo_error =
                    this.solicitudService.personaTitular +
                    " no tiene registrado un número de celular"),
                this.solicitudService.persona_banco.nro_celular,
                this.solicitudService.parametroWarning,
                true,
                "nro_celular"
            );
            await this.solicitudService.setAtributoObservacion(
                this.solicitudService.asegurado,
                (this.solicitudService.atributoSexo.tipo_error = this.solicitudService.personaTitular + " no tiene registrado su sexo"),
                this.solicitudService.persona_banco.sexo,
                this.solicitudService.parametroWarning,
                true,
                "sexo"
            );
            if (this.solicitudService.persona_banco_accounts.length == 0) {
                await this.solicitudService.setAtributoObservacion(
                    this.solicitudService.asegurado,
                    (this.solicitudService.atributoNroCuenta.tipo_error = this.solicitudService.personaTitular + " no tiene registrado ningun número de cuenta"),
                    this.solicitudService.persona_banco_accounts.length,
                    this.solicitudService.parametroWarning
                );
            }
            await this.solicitudService.setFromObjetoAtributoToAtributoInstanciaPoliza();
            if (!this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter) {
                this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter = [];
            }
            await this.solicitudService.ObjetoAseguradoAtributosFiltered.forEach(
                async (objetoAtributo: Objeto_x_atributo, index) => {
                    let atributoInstanciaPoliza: Atributo_instancia_poliza =
                        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas.find((params) => params.objeto_x_atributo.id_atributo === objetoAtributo.id_atributo);
                    if (atributoInstanciaPoliza) {
                        this.solicitudService.asegurado.instancia_poliza.atributo_instancia_polizas_inter[index] = atributoInstanciaPoliza;
                    }
                }
            );

            let trancisiones: Instancia_poliza_transicion[] =
                this.solicitudService.asegurado.instancia_poliza
                    .instancia_poliza_transicions;
            await this.instanciaPolizaTransService.oldInstanciaPolizaTransiciones.forEach(
                async (
                    oldInstanciaPolizaTransicion: Instancia_poliza_transicion
                ) => {
                    if (oldInstanciaPolizaTransicion.par_estado_id == this.solicitudService.asegurado.instancia_poliza.id_estado) {
                        trancisiones.forEach(
                            async (
                                newInstanciaPolizaTransaccion: Instancia_poliza_transicion
                            ) => {
                                if (oldInstanciaPolizaTransicion.observacion == newInstanciaPolizaTransaccion.observacion) {
                                    newInstanciaPolizaTransaccion.id = oldInstanciaPolizaTransicion.id;
                                    newInstanciaPolizaTransaccion.createdAt = oldInstanciaPolizaTransicion.createdAt;
                                    newInstanciaPolizaTransaccion.adicionado_por = oldInstanciaPolizaTransicion.adicionado_por;
                                    newInstanciaPolizaTransaccion.modificado_por = oldInstanciaPolizaTransicion.modificado_por;
                                    newInstanciaPolizaTransaccion.updatedAt = oldInstanciaPolizaTransicion.updatedAt;
                                } else if (
                                    oldInstanciaPolizaTransicion.par_estado_id != this.solicitudService.asegurado.instancia_poliza.id_estado &&
                                    oldInstanciaPolizaTransicion.observacion ==
                                    newInstanciaPolizaTransaccion.observacion
                                ) {
                                    this.solicitudService.asegurado.instancia_poliza.instancia_poliza_transicions.push(oldInstanciaPolizaTransicion);
                                }
                            }
                        );
                    }
                }
            );
            if (typeof callback == "function") {
                await callback();
            }
        }
        await this.stateButtonsFlow();
    }

    async editarTitular() {
        if (
            this.solicitudService.asegurado.instancia_poliza.id_estado != this.solicitudService.estadoIniciado.id &&
            // this.solicitudService.asegurado.instancia_poliza.id_estado != this.solicitudService.estadoSolicitado.id &&
            this.solicitudService.asegurado.instancia_poliza.id_estado != null
        ) {
            this.solicitudService.disableForm(this.userform);
            this.stateButtonsFlow();
        }
        if (this.solicitudService.id_poliza == 4) {
            this.stateButtonsFlow();
            this.solicitudService.editandoTitular = true;
            this.getDatosFromTarjetaDebitoCuentaService((res) => {
                this.solicitudService.setDatesOfAsegurado();
                if (this.solicitudService.persona_banco_accounts.length) {
                    this.solicitudService.persona_banco_account =
                        this.solicitudService.persona_banco_accounts.find((params) => params.nrocuenta === this.solicitudService.persona_banco_account.nrocuenta);
                    if (this.solicitudService.persona_banco_account) {
                        this.userform.controls["par_moneda"].setValue(
                            this.solicitudService.persona_banco_account.moneda
                        );
                    }
                }
                this.stateButtonsFlow();
                this.solicitudService.displayModalFormTitular = true;
                this.solicitudService.displayModalDatosTitular = false;
                this.collapsedFormTitular = false;
            });
        } else {
            await this.solicitudService.setAtributosToPersonaBanco(
                this.userform
            );
            this.stateButtonsFlow();
            this.solicitudService.setDatesOfAsegurado();
            this.solicitudService.editandoTitular = true;
            this.solicitudService.displayModalFormTitular = true;
            this.solicitudService.displayModalDatosTitular = false;
            this.collapsedFormTitular = false;
        }
    }
}
