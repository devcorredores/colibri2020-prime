import {PlanPagoService} from '../../../../../../../src/core/servicios/plan-pago.service';
import {Plan_pago} from '../../../../../../../src/core/modelos/plan_pago';
import {Poliza} from '../../../../../../../src/core/modelos/poliza';
import {SolicitudService} from '../../../../../../../src/core/servicios/solicitud.service';
import {ReporteService} from '../../../../../../../src/core/servicios/reporte.service';
import {Asegurado} from '../../../../../../../src/core/modelos/asegurado';
import {Instancia_poliza} from '../../../../../../../src/core/modelos/instancia_poliza';
import {InstanciaPolizaService} from '../../../../../../../src/core/servicios/instancia-poliza.service';
import {Component, OnInit, ViewChild, AfterViewChecked} from '@angular/core';
import {Parametro} from "../../../../../../../src/core/modelos/parametro";
import {ActivatedRoute, Router} from "@angular/router";
import {BreadcrumbService} from "../../../../../../../src/core/servicios/breadcrumb.service";
import {ParametrosService} from "../../../../../../../src/core/servicios/parametro.service";
import {MenuService} from "../../../../../../../src/core/servicios/menu.service";
import {PersonaService} from "../../../../../../../src/core/servicios/persona.service";
import {Componente, Filtro} from "../../../../../../../src/core/modelos/componente";
import {MessageService, SelectItem, SortEvent} from "primeng/api";
import * as xlsx from 'xlsx';
import * as FileSaver from 'file-saver';
import {SessionStorageService} from "../../../../../../../src/core/servicios/sessionStorage.service";
import {FilterUtils} from "primeng/utils";
import {ActualizarSolicitudComponent} from "../../../../../../../src/core/componentes/actualizar-solicitud/actualizar-solicitud.component";
import {Util} from "../../../../../../../src/helpers/util";
declare var $:any;

@Component({
    selector: 'app-gestion-eco-vida',
    templateUrl: './gestion-eco-vida.component.html',
    styleUrls: ['./gestion-eco-vida.component.css'],
    providers: [MessageService]
})

export class GestionEcoVidaComponent implements OnInit, AfterViewChecked {

    color = 'primary';
    mode = 'indeterminate';
    value = 1;
    componentesEditables: Componente[];
    parametros: Parametro[] = [];
    instancias_polizas: Instancia_poliza[] = [];
    instancias: any[] = [];
    poliza = new Poliza();
    cols: any;
    objeto: Filtro = new Filtro();
    paramAgencias: Parametro[] = [];
    paramSucursales: Parametro[] = [];
    id_instancias: any;
    filtros: boolean = true;
    FiltrosFechas: SelectItem[] = [];
    ProcedenciaCI: SelectItem[] = [];
    Agencias: SelectItem[] = [];
    Sucursales: SelectItem[] = [];
    Estados: SelectItem[] = [];
    es: any;
    util = new Util();
    displayExedioResultados: boolean = false;
    showMoreResultsMessage: boolean = false;
    PanelFiltrosColapsed: boolean = false;
    btnEmitir: boolean = false;
    btnEditar: boolean = false;
    btnVer1: boolean = false;
    btnVer2: boolean = false;
    index_instancia_poliza: number;
    exportColumns: any[];
    rowsPerPage: number = 0;
    numRecords: number = 0;
    totalRows: number = 0;

    @ViewChild('componenteActualizarSolicitud', {static: false}) private componenteActualizarSolicitud: ActualizarSolicitudComponent;

    constructor(
        private params: ActivatedRoute, private breadcrumbService: BreadcrumbService,
        private parametrosService: ParametrosService,
        private instanciaPolizaService: InstanciaPolizaService,
        private router: Router,
        private menuService: MenuService,
        private reporteService: ReporteService,
        private service: MessageService,
        private personaService: PersonaService,
        public solicitudService: SolicitudService,
        public sessionStorageService: SessionStorageService,
        private planPagoService: PlanPagoService
    ) {
        this.cols = [
            {field: 'Nro', header: 'Nro', width: '5%'},
            {field: 'Nro_Sol', header: 'Nro. Sol.', width: '10%'},
            {field: 'Sucursal', header: 'Sucursal', width: '8%'},
            {field: 'Agencia', header: 'Agencia', width: '10%'},
            {field: 'Nro_Certificado', header: 'Nro. Certificado', width: '8%'},
            {field: 'Estado', header: 'Estado', width: '10%'},
            {field: 'Fecha_Registro', header: 'Fecha Registro', width: '10%'},
            {field: 'Fecha_Solicitud', header: 'Fecha Solicitud', width: '10%'},
            {field: 'Fecha_Emision', header: 'Fecha Emisión', width: '10%'},
            {field: 'Asegurado', header: 'Asegurado', width: '19%'},
            {field: 'usuario', header: 'usuario', width: '10%'},
            {field: null, header: 'Acciones', width: '10%'}
        ];

        FilterUtils['custom'] = (value, filter): boolean => {
            if (filter === undefined || filter === null || filter.trim() === '') {
                return true;
            }

            if (value === undefined || value === null) {
                return false;
            }

            return parseInt(filter) > value;
        }
    }

    ValidarComponentesInvisible(id: any) {
        if (this.solicitudService.componentesInvisibles.find(params => params.codigo === id && params.estado === 'I')) {
            return false;
        } else {
            return true;
        }
    }

    ngAfterViewChecked() {
        /*if (this.solicitudService.asegurado.instancia_poliza.id_estado + '' === '59') {
          if (this.poliza.instancia_polizas.length > 0 && (this.index_instancia_poliza || this.index_instancia_poliza===0)) {
            this.poliza.instancia_polizas[this.index_instancia_poliza].id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
            this.instancias[this.index_instancia_poliza].id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
            this.instancias[this.index_instancia_poliza]['Estado'] = this.devuelveValorParametro(this.instancias[this.index_instancia_poliza].id_estado);
          }
        }*/
    }

    ValidarComponentesEditable(id: any) {
        if (this.componentesEditables.find(params => params.codigo === id && params.estado === 'NE')) {
            return true;
        } else {
            return false;
        }
    }

    ngOnInit() {
        this.solicitudService.constructComponent(() => {
            this.breadcrumbService.setItems([
                {label: this.solicitudService.ruta}
            ]);
            this.objeto = this.solicitudService.parametrosRuteo.parametro_ruteo && this.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? this.solicitudService.parametrosRuteo.parametro_ruteo.filtros : this.objeto;
            this.objeto.id_poliza = parseInt(this.solicitudService.parametrosRuteo.parametro_vista+'') ? parseInt(this.solicitudService.parametrosRuteo.parametro_vista+'') : this.solicitudService.id_poliza;
            this.solicitudService.product = this.solicitudService.ruta;
            this.solicitudService.isInAltaSolicitud = false;
            this.solicitudService.isLoadingAgain = false;
            this.GetAllParametrosByIdDiccionarios();
            if (this.solicitudService.parametrosRuteo.parametro_ruteo && Object.keys(this.solicitudService.parametrosRuteo.parametro_ruteo).length) {
                this.id_instancias = this.solicitudService.parametrosRuteo.parametro_ruteo;
                this.listaAll(null, () => {
                    //this.limpiar();
                });
            } else {
                this.PanelFiltrosColapsed = false;
            }
        });
    }

    listaAll(dt: any = null, callback: any = null) {
        if (dt) {
            dt.reset();
        }
        setTimeout(() => {
            if ($(window).width() < 720) {
                $('.ui-table').css('overflow','scroll');
                $('.ui-table table').css('width','400%');
            }
        },2000);
        this.instancias = [];
        if(this.objeto && this.objeto.id_instancia_poliza) {
            this.objeto = new Filtro();
            this.objeto.id_instancia_poliza = this.solicitudService.parametrosRuteo.parametro_ruteo.filtros.id_instancia_poliza;
        } else {
            this.objeto = this.solicitudService.parametrosRuteo.parametro_ruteo && this.solicitudService.parametrosRuteo.parametro_ruteo.filtros ? this.solicitudService.parametrosRuteo.parametro_ruteo.filtros : this.objeto;
        }
        if (!this.objeto.agencia) {
            this.objeto.agencia_required = false;
        }
        if (!this.objeto.sucursal_required) {
            this.objeto.sucursal_required = false;
        }
        this.solicitudService.isLoadingAgain = true;
        this.objeto.id_poliza = this.solicitudService.id_poliza;
        this.instanciaPolizaService.GetAllByParametrosEcoVida(this.objeto).subscribe(res => {
            let response = res as { status: string, message: string, data: Instancia_poliza[], recordsTotal: number, recordsOffset: number, recordsFiltered: number };
            this.totalRows = response.recordsTotal ? response.recordsTotal : 0;
            this.objeto.first = response.recordsOffset ? response.recordsOffset : 0;
            this.objeto.rows = response.recordsFiltered ? this.totalRows >= response.recordsFiltered ? response.recordsFiltered : this.totalRows : this.totalRows > 10 ? 10 : this.totalRows;
            this.instancias_polizas = response.data;
            if (this.instancias_polizas.length >= 300) {
                this.showMoreResultsMessage = true;
            } else {
                this.showMoreResultsMessage = false;
            }
            this.PanelFiltrosColapsed = true;
            let lastEmptyRows = (this.totalRows % this.objeto.rows) - this.objeto.rows;
            lastEmptyRows = lastEmptyRows < 0 ? lastEmptyRows * (-1) : lastEmptyRows;
            let lastFilledRows = this.objeto.rows - lastEmptyRows;
            let rowsAfter = this.totalRows - lastFilledRows;
            rowsAfter = rowsAfter == this.totalRows ? rowsAfter - this.objeto.rows : rowsAfter;
            this.instancias_polizas = this.getCars(this.instancias_polizas);
            for (let i = 0; i < rowsAfter; i++) {
                this.instancias_polizas.push(new Instancia_poliza());
                let rowNumber = i + (this.objeto.first + 1);
                this.instancias_polizas[i].Nro = rowNumber > this.totalRows ? null : rowNumber;
            }
            if (this.totalRows - this.rowsPerPage == lastFilledRows) {
                this.rowsPerPage = this.objeto.first + lastFilledRows;
            } else {
                this.rowsPerPage = this.objeto.first + this.objeto.rows;
            }
            if (this.totalRows - this.objeto.first == lastFilledRows) {
                this.numRecords = lastFilledRows
            } else {
                this.numRecords = this.objeto.rows
            }
            if (this.instancias_polizas && this.instancias_polizas.length) {
                // if (this.instancias_polizas.length >= 300 && !this.usuarioLogin.rol.find(param => param.id == 5)) {
                //     this.displayExedioResultados = true;
                // }
            } else {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No se encontro ningun registro'
                });
            }
            this.solicitudService.isLoadingAgain = false;
            if (typeof callback == 'function') {
                callback();
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    onPageAction(event, dt) {
        console.log(event)
        this.objeto.first = event.first;
        this.objeto.rows = event.rows;
        this.listaAll(dt);
    }
    onSearchAction(event, dt) {
        this.objeto.first = 0;
        this.objeto.rows = event.rows;
        this.listaAll(dt);
    }

    filterAgencies(event) {
        if (event.value) {
            let value = event.value;
            let sucursalPadre = this.paramSucursales.find(param => param.parametro_cod == value);
            if (sucursalPadre) {
                let filteredAgencias = this.paramAgencias.filter(param => param.id_padre+'' == sucursalPadre.id+'');
                this.Agencias = filteredAgencias.map(param => {return {value: param.parametro_cod, label: param.parametro_descripcion}})
                this.Agencias.unshift({ label: "Seleccione Agencia", value: null });
            }
        }
    }

    async listaAllByIds() {
        await this.instanciaPolizaService.GetAllByIdsEcoVida(this.id_instancias).subscribe(async res => {
            let response = res as { status: string, message: string, data: Instancia_poliza[], recordsTotal: number, recordsOffset: number, recordsFiltered: number };
            this.PanelFiltrosColapsed = true;
            this.instancias_polizas = response.data;
            this.totalRows = response.recordsTotal ? response.recordsTotal : 0;
            this.objeto.first = response.recordsOffset ? response.recordsOffset : 0;
            this.objeto.rows = response.recordsFiltered ? this.totalRows >= response.recordsFiltered ? response.recordsFiltered : this.totalRows : this.totalRows > 10 ? 10 : this.totalRows;
            this.instancias_polizas = response.data;
            if (this.instancias_polizas.length >= 300) {
                this.showMoreResultsMessage = true;
            } else {
                this.showMoreResultsMessage = false;
            }
            this.instancias_polizas = this.getCars(this.instancias_polizas);
            let lastEmptyRows = (this.totalRows % this.objeto.rows) - this.objeto.rows;
            lastEmptyRows = lastEmptyRows < 0 ? lastEmptyRows * (-1) : lastEmptyRows;
            let lastFilledRows = this.objeto.rows - lastEmptyRows;
            let rowsAfter = this.totalRows - lastFilledRows;
            rowsAfter = rowsAfter == this.totalRows ? rowsAfter - this.objeto.rows : rowsAfter;
            for (let i = 0; i < rowsAfter; i++) {
                this.instancias_polizas.push(new Instancia_poliza());
                let rowNumber = i + (this.objeto.first + 1);
                this.instancias_polizas[i].Nro = rowNumber > this.totalRows ? null : rowNumber;
            }
            if (this.totalRows - this.rowsPerPage == lastFilledRows) {
                this.rowsPerPage = this.objeto.first + lastFilledRows;
            } else {
                this.rowsPerPage = this.objeto.first + this.objeto.rows;
            }
            if (this.totalRows - this.objeto.first == lastFilledRows) {
                this.numRecords = lastFilledRows
            } else {
                this.numRecords = this.objeto.rows
            }
            if (this.instancias_polizas && this.instancias_polizas.length) {
                if (this.instancias_polizas.length >= 300 && !this.solicitudService.userInfo.usuarioRoles.find(param => param.id == 5)) {
                    this.displayExedioResultados = true;
                }
            } else {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No se encontro ningun registro'
                });
                this.poliza = new Poliza();
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    devuelveValorParametro(id: any) {
        let parametro = this.parametros.find(parametro => parametro.id === id);
        if (parametro) {
            return parametro.parametro_descripcion;
        } else {
            return '';
        }
    }

    devuelveValorParametroByCod(id: any) {
        let parametro = this.parametros.find(parametro => parametro.parametro_cod === id);
        if (parametro) {
            return parametro.parametro_descripcion;
        } else {
            return '';
        }
    }

    devuelveValorParametroByCodAbreviacion(id: any, idDiccionario: number, ) {
        let parametro = this.parametros.find(parametro => parametro.parametro_cod == id && parametro.diccionario_id == idDiccionario);
        if (parametro) {
            return parametro.parametro_abreviacion;
        } else {
            return '';
        }
    }

    devuelveValorAgenciaSucursal(agencia: any) {
        if (agencia) {
            let parametro = this.parametros.find(parametro => parametro.parametro_cod === agencia.valor);
            if (parametro) {
                return parametro.parametro_descripcion;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    async GetAllParametrosByIdDiccionarios() {
        //ids ponemos los id de los diccionarios que necesitemos
        let ids = [11, 1, 17, 18, 38, 40, 54];
        this.solicitudService.isLoadingAgain = true;
        await this.parametrosService.GetAllParametrosByIdDiccionarios(ids).subscribe(res => {
            let response = res as { status: string, message: string, data: Parametro[] };
            this.solicitudService.isLoadingAgain = false;
            this.parametros = response.data;
            this.ProcedenciaCI.push({label: "Seleccione Procedencia", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "18").forEach(element => {
                this.ProcedenciaCI.push({label: element.parametro_descripcion, value: element.parametro_cod});
            });

            this.FiltrosFechas.push({label: "Seleccione un tipo de fecha", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "54").forEach(element => {
                this.FiltrosFechas.push({label: element.parametro_descripcion, value: element.parametro_cod})
            });

            this.Agencias.push({label: "Seleccione Agencia", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "40").forEach(element => {
                this.Agencias.push({label: element.parametro_descripcion, value: element.parametro_cod});
                this.paramAgencias.push(element);
            });

            this.Sucursales.push({label: "Seleccione Sucursal", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "38").forEach(element => {
                this.Sucursales.push({label: element.parametro_descripcion, value: element.parametro_cod});
                this.paramSucursales.push(element);
            });
            this.Estados.push({label: "Seleccione Estado", value: null});
            this.parametros.filter(param => param.diccionario_id + '' === "11").forEach(element => {
                this.Estados.push({label: element.parametro_descripcion, value: element.id});
            });
            if (this.solicitudService.userInfo) {
                if (this.solicitudService.userInfo.usuario_banco) {
                    if (this.solicitudService.userInfo.usuario_banco.us_sucursal) {
                        this.objeto.sucursal = this.solicitudService.userInfo.usuario_banco.us_sucursal + '';
                    }
                    if (this.solicitudService.userInfo.usuario_banco.us_oficina) {
                        this.objeto.agencia = this.solicitudService.userInfo.usuario_banco.us_oficina + '';
                    }
                }
            } else {
                this.router.navigate(['login']);
            }
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    listaAllById() {

    }

    EditarPoliza(id: any) {
        this.btnEditar = true;
        this.btnVer1 = true;
        if (this.solicitudService.id_poliza == 6) {
            this.menuService.activaRuteoMenu('46', 6, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaEcoVida', this.solicitudService.componentesInvisibles);
        }
        if (this.solicitudService.id_poliza == 7) {
            this.menuService.activaRuteoMenu('49', 7, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaEcoProteccion', this.solicitudService.componentesInvisibles);
        }
        //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);
    }

    VerPoliza(id: any) {
        this.btnVer2 = true;
        if (this.solicitudService.id_poliza == 6) {
            this.menuService.activaRuteoMenu('52', 6, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaEcoVida', this.solicitudService.componentesInvisibles);
        }
        if (this.solicitudService.id_poliza == 7) {
            this.menuService.activaRuteoMenu('53', 7, {
                id_instancia_poliza: id,
                filtros: this.objeto
            }, 'AltaEcoProteccion', this.solicitudService.componentesInvisibles);
        }
        //this.router.navigate(['/AppMain/AltaEcoAguinaldo',{id:id}]);
    }

    ValidarEstados(id_estado: any, estados: any[]) {
        if (estados.find(param => param == '*')) {
            return true;
        } else {
            if (estados.includes(parseInt(id_estado + ''))) {
                return true;
            } else {
                return false;
            }
        }
    }

    verifyRol(estadosAvailable: any[], rolsAvailable: any[], instanciaPoliza: Instancia_poliza) {
        let includesRol = false;
        if (this.solicitudService.userInfo) {
            for (let i = 0; i < this.solicitudService.userInfo.usuarioRoles.length; i++) {
                let rol = this.solicitudService.userInfo.usuarioRoles[i];
                if (rolsAvailable.find(param => param == '*')) {
                    includesRol = true;
                    break;
                } else {
                    if (rolsAvailable.includes(parseInt(rol.id + ''))) {
                        includesRol = true;
                    }
                }
            }
            if (this.ValidarEstados(instanciaPoliza.id_estado, estadosAvailable) && includesRol) {
                return true;
            }
            return false;
        } else {
            this.router.navigate(['login']);
        }
    }

    buscarObjetoAsegurado(id_instancia_poliza: any, index: number) {
        this.index_instancia_poliza = index;
        let id_objeto = 0
        if (this.solicitudService.id_poliza + '' === '6') {
            id_objeto = 18;
        }
        if (this.solicitudService.id_poliza + '' === '7') {
            id_objeto = 20;
        }
        this.btnEmitir = true;
        this.solicitudService.isLoadingAgain = true;
        this.personaService.findPersonaSolicitudByIdInstanciaPoliza(id_objeto, id_instancia_poliza).subscribe(async res => {
            let response = res as { status: string, message: string, data: Asegurado };
            this.solicitudService.asegurado = response.data;
            await this.componenteActualizarSolicitud.cambiarSolicitudEstado().then(respo => {
                /*let ins = this.instancias_polizas.find(param => param.id + '' === id_instancia_poliza + '');
                if (ins) {
                    ins.id_estado = this.solicitudService.asegurado.instancia_poliza.id_estado;
                }*/
                this.solicitudService.isLoadingAgain = false;
            });
            this.instancias_polizas = [];
            this.btnEmitir = false;
            this.instancias = [];
        }, err => {
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });

    }

    limpiar() {
        this.instancias_polizas = [];
        this.instancias = [];
        this.poliza = new Poliza();
        this.objeto = {
            agencia_required: true,
            id_instancia_poliza: null,
            sucursal_required: true,
            estado: null,
            agencia: null,
            sucursal: null,
            usuario_login: '',
            persona_primer_apellido: '',
            persona_segundo_apellido: '',
            persona_primer_nombre: '',
            persona_doc_id: '',
            persona_doc_id_ext: '',
            id_poliza: null,
            campo_fecha: null,
            fecha_min: null,
            fecha_max: null,
            fecha_registro_min: null,
            fecha_registro_max: null,
            fecha_solicitud_min: null,
            fecha_solicitud_max: null,
            fecha_emision_min: null,
            fecha_emision_max: null,
            id_certificado: '',
            nro_documento: '',
            usuarioLogin: null,
            first: 0,
            rows: 10
        };
    }

    obtenerUltimoDocumento(documentos: any[]) {
        let nro_solicitud = "";
        documentos.forEach(documento => {
            nro_solicitud = documento.nro_documento;
        });
        return nro_solicitud;
    }

    ImprimirSolicitud(id: any) {
        let nombre_archivo = 'SoliEcoAgui' + id;
        this.solicitudService.isLoadingAgain = true;
        this.reporteService.ReporteSolicitud(id, nombre_archivo).subscribe(res => {
            let response = res as { status: string, message: string, data: Poliza[] };
            this.solicitudService.isLoadingAgain = false;
            if (response.status == 'ERROR') {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
            } else {
                this.reporteService.cargarPagina(nombre_archivo);
            }
        }, err => {
            this.solicitudService.isLoadingAgain = false;
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    ImprimirCertificado(id: any) {
        let nombre_archivo = 'CertEcoAgui' + id;
        this.solicitudService.isLoadingAgain = true;
        this.reporteService.certificado_ecoaguinaldo(id, nombre_archivo).subscribe(res => {
            let response = res as { status: string, message: string, data: Poliza[] };
            this.solicitudService.isLoadingAgain = false;
            if (response.status == 'ERROR') {
                this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Advertencia',
                    detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                });
            } else {
                this.reporteService.cargarPagina(nombre_archivo);
            }
        }, err => {
            this.solicitudService.isLoadingAgain = false;
            if (err.error.statusCode === 400 && err.error.message === "usuario no autentificado") {
                this.router.navigate(['login']);
            } else {
                console.log(err);
            }
        });
    }

    exportExcel(dt: any, accept: boolean = false) {
        if (!accept && this.totalRows > 300) {
            this.displayExedioResultados = true
        }
        if (accept || this.totalRows <= 300) {
            this.objeto.rows = 300;
            this.objeto.first = 0;
            this.listaAll(dt, () => {
                let instan = null
                if (dt.filteredValue !== null) {
                    instan = dt.filteredValue;
                } else {
                    instan = this.instancias_polizas;
                }
                let instancia_poliza_excel = this.getCarsExcel(instan);
                const worksheet = xlsx.utils.json_to_sheet(instancia_poliza_excel);
                const workbook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
                const excelBuffer: any = xlsx.write(workbook, {bookType: 'xlsx', type: 'array'});
                this.solicitudService.isLoadingAgain = false;
                this.saveAsExcelFile(excelBuffer, "primengTable");
            });
        }
    }

    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }

    getCars(ins_poli: any) {
        let instancias = [];
        let c = 0;
        for (let i = 0; i < ins_poli.length; i++) {
            c++;
            let data = ins_poli[i];
            let objeto = data;
            let instancia = data;
            let docSolicitud, docCertificado;
            switch (this.solicitudService.id_poliza + '') {
                case '6':
                    if (instancia.solicitudes && instancia.solicitudes.length) {
                        docSolicitud = instancia.solicitudes.find(param => param.id_documento == 16);
                    }
                    if (instancia.certificados && instancia.certificados.length) {
                        docCertificado = instancia.certificados.find(param => param.id_documento == 18);
                    }
                    break;
                case '7':
                    if (instancia.solicitudes && instancia.solicitudes.length) {
                        docSolicitud = instancia.solicitudes.find(param => param.id_documento == 19);
                    }
                    if (instancia.certificados && instancia.certificados.length) {
                        docCertificado = instancia.certificados.find(param => param.id_documento == 21);
                    }
                    break;
            }
            objeto['Nro'] = c;
            objeto['id'] = instancia.id;
            objeto['id_estado'] = instancia.id_estado;
            objeto['Nro_Sol'] = instancia.solicitudes && instancia.solicitudes.length ? instancia.solicitudes[0].nro_documento : '';
            objeto['Sucursal'] = this.devuelveValorAgenciaSucursal(instancia.sucursal);
            objeto['Agencia'] = this.devuelveValorAgenciaSucursal(instancia.agencia);
            objeto['Nro_Certificado'] = instancia.certificados && instancia.certificados.length ? this.obtenerUltimoDocumento(instancia.certificados) : '';
            objeto['Estado'] = this.devuelveValorParametro(instancia.id_estado);
            objeto['Fecha_Registro'] = this.util.formatoFecha(instancia.fecha_registro + '');
            objeto['Fecha_Solicitud'] = docSolicitud ? this.util.formatoFecha(docSolicitud.fecha_emision + '') : null;
            objeto['Fecha_Emision'] = docCertificado ? this.util.formatoFecha(docCertificado.fecha_emision + '') : null;
            objeto['Asegurado'] = instancia.asegurados[0].entidad.persona.persona_primer_nombre + ' ' + instancia.asegurados[0].entidad.persona.persona_primer_apellido + ' ' + instancia.asegurados[0].entidad.persona.persona_segundo_apellido;
            objeto['usuario_login'] = instancia.usuario ? instancia.usuario.usuario_login : '';
            instancias.push(objeto);
        }

        return instancias;
    }

    getCarsExcel(ins_poli: any) {
        let instancias = [];
        let c = 0;
        for (let instancia of ins_poli) {
            c++;
            let objeto = {};
            let docSolicitud, docCertificado;
            switch (this.solicitudService.id_poliza + '') {
                case '6':
                    docSolicitud = instancia.Fecha_Solicitud;
                    docCertificado = instancia.Fecha_Emision;
                    break;
                case '7':
                    docSolicitud = instancia.Fecha_Solicitud;
                    docCertificado = instancia.Fecha_Emision;
                    break;
            }

            objeto['NRO'] = instancia['Nro'];
            objeto['ID'] = instancia['id'];
            objeto['ID ESTADO'] = instancia['id_estado'];
            objeto['NRO SOL'] = instancia['Nro_Sol'];
            objeto['SUCURSAL'] = instancia['Sucursal'];
            objeto['AGENCIA'] = instancia['Agencia'];
            objeto['NRO CERTIFICADO'] = instancia['Nro_Certificado'];
            objeto['ESTADO'] = instancia['Estado'];
            objeto['FECHA REGISTRO'] = instancia['Fecha_Registro'];
            objeto['FECHA SOLICITUD'] = instancia['Fecha_Solicitud'];
            objeto['FECHA EMISION'] = instancia['Fecha_Emision'];
            objeto['ASEGURADO'] = instancia['Asegurado'];
            objeto['USUARIO LOGIN'] = instancia['usuario_login'];

            instancias.push(objeto);
        }

        return instancias;
    }

    customSort(event: SortEvent) {
        event.data.sort((data1, data2) => {
            let value1 = data1[event.field];
            let value2 = data2[event.field];
            let result = null;

            if (value1 == null && value2 != null)
                result = -1;
            else if (value1 != null && value2 == null)
                result = 1;
            else if (value1 == null && value2 == null)
                result = 0;
            else if (typeof value1 === 'string' && typeof value2 === 'string')
                result = value1.localeCompare(value2);
            else
                result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

            return (event.order * result);
        });
    }

    // exportPdf() {
    //       const doc = new jsPDF.default(0, 0);
    //       doc.autoTable(this.exportColumns, this.poliza.instancia_polizas);
    //       doc.save('primengTable.pdf');
    // }

    ImprimirSolicitudEcovida(id: any) {
        let nombre_archivo = 'SoliEcoVida' + id;
        this.solicitudService.isLoadingAgain = true;
        if (this.solicitudService.id_poliza + '' === '6') {
            this.reporteService.ReporteSolicitudEcoVida(id, nombre_archivo).subscribe(res => {
                let response = res as { status: string, message: string, data: Poliza[] };
                this.solicitudService.isLoadingAgain = false;
                if (response.status == 'ERROR') {
                    this.service.add({
                        key: 'tst',
                        severity: 'warn',
                        summary: 'Advertencia',
                        detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                    });
                } else {
                    this.reporteService.cargarPagina(nombre_archivo);
                }
            }, err => {
                this.solicitudService.isLoadingAgain = false;
                console.error("ERROR llamando servicio ImprimirSolicitud:", err);
            });
        }
        if (this.solicitudService.id_poliza + '' === '7') {
            this.reporteService.ReporteSolicitudEcoProteccion(id, nombre_archivo).subscribe(res => {
                let response = res as { status: string, message: string, data: Poliza[] };
                this.solicitudService.isLoadingAgain = false;
                if (response.status == 'ERROR') {
                    this.service.add({
                        key: 'tst',
                        severity: 'warn',
                        summary: 'Advertencia',
                        detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                    });
                } else {
                    this.reporteService.cargarPagina(nombre_archivo);
                }
            }, err => {
                this.solicitudService.isLoadingAgain = false;
                console.error("ERROR llamando servicio ImprimirSolicitud:", err);
            });
        }
    }

    ImprimirCertificadoEcoVida(id: any) {
        let nombre_archivo = 'CertEcoVida' + id;
        this.solicitudService.isLoadingAgain = true;
        if (this.solicitudService.id_poliza + '' === '6') {
            this.reporteService.ReporteCertificadoEcoVida(id, nombre_archivo).subscribe(res => {
                let response = res as { status: string, message: string, data: Poliza[] };
                this.solicitudService.isLoadingAgain = false;
                if (response.status == 'ERROR') {
                    this.service.add({
                        key: 'tst',
                        severity: 'warn',
                        summary: 'Advertencia',
                        detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                    });
                } else {
                    this.reporteService.cargarPagina(nombre_archivo);
                }
            }, err => {
                this.solicitudService.isLoadingAgain = false;
                console.error("ERROR llamando servicio ImprimirCertificadoEcoVida:", err);
            });
        }
        if (this.solicitudService.id_poliza + '' === '7') {
            this.reporteService.ReporteCertificadoEcoProteccion(id, nombre_archivo).subscribe(res => {
                let response = res as { status: string, message: string, data: Poliza[] };
                this.solicitudService.isLoadingAgain = false;
                if (response.status == 'ERROR') {
                    this.service.add({
                        key: 'tst',
                        severity: 'warn',
                        summary: 'Advertencia',
                        detail: 'No pudo imprimir el documento, verifique los datos de la solicitud'
                    });
                } else {
                    this.reporteService.cargarPagina(nombre_archivo);
                }
            }, err => {
                this.solicitudService.isLoadingAgain = false;
                console.error("ERROR llamando servicio ImprimirCertificadoEcoVida:", err);
            });
        }
    }

    generarplanpago(id: any) {
        let pp = new Plan_pago();
        pp.id_instancia_poliza = id;
        pp.total_prima = 192;
        pp.interes = 0;
        pp.plazo_anos = 1;
        pp.periodicidad_anual = 12;
        pp.prepagable_postpagable = 1;
        pp.fecha_inicio = new Date();
        pp.adicionado_por = '2';
        pp.modificado_por = '2';
        this.planPagoService.GenerarPlanPagos(pp).subscribe(res => {
            let response = res as { status: string, message: string, data: any[] };
            if (response.status == 'ERROR') {

            } else {

            }
        }, err => console.error("ERROR llamando servicio plan de pagos:", err));
    }

    async GeneraAllPlanPagos(id: any) {
        await this.planPagoService.listaTodosSinPlanPagos().subscribe(async res => {
            let response = res as { status: string, message: string, data: any[] };
            for (let i = 0; i < response.data.length; i++) {
                let pp = new Plan_pago();
                pp.id_instancia_poliza = response.data[i].id;
                pp.total_prima = parseInt(response.data[i].monto) * parseInt(response.data[i].plazo);
                pp.interes = 0;
                pp.plazo_anos = 1;
                pp.periodicidad_anual = parseInt(response.data[i].monto);
                pp.prepagable_postpagable = 1;
                pp.fecha_inicio = response.data[i].fecha_emision;
                pp.adicionado_por = '4';
                pp.modificado_por = '4';
                await this.planPagoService.GenerarPlanPagos2(pp).then(res => {
                    let response = res as { status: string, message: string, data: any[] };
                    if (response.status == 'ERROR') {

                    } else {

                    }
                }, err => console.error("ERROR llamando servicio plan de pagos:", err));
            }
        }, err => console.error("ERROR llamando servicio plan de pagos:", err));
    }

    verPlanPagos(ins_pol: any) {
        this.menuService.activaRuteoMenu('66', null, {id_instancia_poliza: ins_pol.id}, 'PlanPago');
    }
}

