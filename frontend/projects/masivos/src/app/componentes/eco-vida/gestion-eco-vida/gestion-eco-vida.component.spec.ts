import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionEcoVidaComponent } from './gestion-eco-vida.component';

describe('GestionEcoVidaComponent', () => {
  let component: GestionEcoVidaComponent;
  let fixture: ComponentFixture<GestionEcoVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionEcoVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionEcoVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
