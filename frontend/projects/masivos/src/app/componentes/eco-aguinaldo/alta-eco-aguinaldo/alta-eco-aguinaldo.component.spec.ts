import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEcoAguinaldoComponent } from './alta-eco-aguinaldo.component';

describe('AltaEcoAguinaldoComponent', () => {
  let component: AltaEcoAguinaldoComponent;
  let fixture: ComponentFixture<AltaEcoAguinaldoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEcoAguinaldoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEcoAguinaldoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


