import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionEcoAguinaldoComponent } from './gestion-eco-aguinaldo.component';

describe('GestionEcoAguinaldoComponent', () => {
  let component: GestionEcoAguinaldoComponent;
  let fixture: ComponentFixture<GestionEcoAguinaldoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionEcoAguinaldoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionEcoAguinaldoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
