export const settings = {
    production: false,
    version: "1.0.0",
    URL: 'http://localhost:7001',
    URL_API_MASIVOS: 'http://localhost:7001/api-corredores-ecofuturo',
    URL_PUBLIC_PDF_MASIVOS: 'http://localhost:7001/pdf',
    URL_PUBLIC_VIEWER_MASIVOS: 'http://localhost:7001/viewer',
};
