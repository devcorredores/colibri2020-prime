const { Router } = require('express');
const router = new Router();
const ds = require('../models');
const util = require("../module/util");


router.get('/',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await ds.Seg_Observaciones.findAll()
        .then(tomadors => {
            response.data = tomadors;
        }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;        
        });
    res.json(response);
});

router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    await ds.Seg_Observaciones.findOne({ where : {Obs_Id : id}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/IdSol/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    await ds.Seg_Observaciones.findAll({ where : {Obs_IdSol : id}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "NOK";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const segObservaciones = req.body;

    segObservaciones.Obs_FechaReg = util.getDateYYYYmmDD();
    segObservaciones.Obs_HoraReg = util.getHoraActual();

    await ds.Seg_Observaciones.create(segObservaciones)
    .then(resp => {
        response.data = resp;
        response.message = 'Registro creado';
    })
    .catch((err) => {
        response.status = 'ERROR';
        response.message = err;
    });

    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Observaciones.findOne({ where : {Obs_Id:id}})
        .then(resp => {
            if(resp){
                resp.update(req.body);
                response.message = 'Registro actualizado';
            }else{
                response.status = 'ERROR';
                response.message = 'Registro no existe';
            }
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Observaciones.findOne({ where : {Obs_Id:id}})
    .then(resp => {
        if(resp){
            resp.destroy();
            response.message = "Registro eliminado"
        }else{
            response.status = 'ERROR';
            response.message = "Registro no exite"
        }
    })
    .catch(err=>{
        response.status = "ERROR",
        response.message = err
    })
    res.json(response);
});

module.exports = router;
