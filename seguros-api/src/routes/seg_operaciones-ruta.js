const { Router } = require('express');
const router = new Router();
const ds = require('../models');


router.get('/',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await ds.Seg_Operaciones.findAll({limit:100})
        .then(resul => {
            response.data = resul;
        }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;        
        });
    res.json(response);
});

router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    await ds.Seg_Operaciones.findOne({ where : {Operacion_Id:id}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/idsol/:nro', async(req, res) => {
    let response = { status: 'OK', message: 'solicitud', data: {} };
    const nro = req.params.nro;
    await ds.Seg_Operaciones.findAll({ where: {Operacion_IdSol:nro}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "NOK";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'NOK';
            response.message = err;
        });
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { Operacion_Id } = req.body;
    const param = await ds.Seg_Operaciones.findOne({where:{Operacion_Id}});
    if(param){
        response.status = 'ERROR',
        response.message = 'Registro existe'
    }else{
        await ds.Seg_Operaciones.create(req.body)
        .then(resp => {
            response.data = resp;
            response.message = 'Registro creado';
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    }
    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Operaciones.findOne({ where : {Operacion_Id:id}})
        .then(resp => {
            if(resp){
                resp.update(req.body);
                response.message = 'Registro actualizado';
            }else{
                response.status = 'ERROR';
                response.message = 'Registro no existe';
            }
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Operaciones.findOne({ where : {Operacion_Id:id}})
    .then(resp => {
        if(resp){
            resp.destroy();
            response.message = "Registro eliminado"
        }else{
            response.status = 'ERROR';
            response.message = "Registro no exite"
        }
    })
    .catch(err=>{
        response.status = "ERROR",
        response.message = err
    })
    res.json(response);
});

module.exports = router;
