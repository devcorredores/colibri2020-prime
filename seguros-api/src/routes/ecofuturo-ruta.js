const { Router } = require('express');
const router = new Router();
const ecoservice = require('../module/ecofuturo-servicios');
const util = require('../module/util');

router.get('/',async (req, res) => {
    let response = { status: 'OK', message: 'idepro', data: '' };
    res.json(response);
});

// http://localhost:3003/api-seguros/ecofuturo/getsolicitud/270737
router.get('/getsolicitud/:nro', async(req, res) => {
    let response = { status: 'OK', messages: [], data: {} };
    const nro = req.params.nro;
    await ecoservice.getSolicitud(nro)
    .then(parti=>{
        if ( parti.status == 'OK'){
            response.data = parti.data;
        }else{
            response.status = 'NOK';
            response.messages.push('Error con el servicio');
        } 
    }).catch(err=>{response.status='NOK';response.messages.push('Error de comunicacion con el servicio')});
    res.json(response);
});

// http://localhost:3003/api-seguros/ecofuturo/getsolicitudprimera/270737
router.get('/getsolicitudprimera/:nro', async(req, res) => {
    let response = { status: 'OK', messages: [], data: {} };
    const nro = req.params.nro;
    await ecoservice.getSolicitudPrimeraEtapa(nro)
    .then(parti=>{
        if ( parti.status == 'OK'){
            response.data = parti.data;
        }else{
            response.status = 'NOK';
            response.messages.push('Error con el servicio');
        } 
    }).catch(err=>{response.status='NOK';response.messages.push('Error de comunicacion con el servicio')});
    res.json(response);
});

// 277106
// http://localhost:3003/api-seguros/ecofuturo/getsolicitudsegunda/270737
router.get('/getsolicitudsegunda/:nro', async(req, res) => {
    let response = { status: 'OK', messages: [], data: {} };
    const nro = req.params.nro;
    await ecoservice.getSolicitudSegundaEtapa(nro)
    .then(parti=>{
        if ( parti.status == 'OK'){
            response.data = parti.data;
        }else{
            response.status = 'NOK';
            response.messages.push('Error con el servicio');
        } 
    }).catch(err=>{response.status='NOK';response.messages.push('Error de comunicacion con el servicio')});
    res.json(response);
});

// http://localhost:3003/api-seguros/ecofuturo/getsolicitudtercera/270737
router.get('/getsolicitudtercera/:nro', async(req, res) => {
    let response = { status: 'OK', messages: [], data: {} };
    const nro = req.params.nro;

    await ecoservice.getSolicitudTerceraEtapa(nro)
    .then(parti=>{
        if ( parti.status == 'OK'){
            response.data = parti.data;
        }else{
            response.status = 'NOK';
            response.messages.push('Error con el servicio');
        } 
    }).catch(err=>{response.status='NOK';response.messages.push('Error de comunicacion con el servicio')});
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    res.json(response);
});

// http://localhost:3003/api-seguros/ecofuturo/getsolicitudAll/270737
router.get('/getsolicitudall/:nro', async(req, res) => {
    let response = { status: 'OK', messages: [], data: {} };
    const nro = req.params.nro;
    // getSolicitude
    await ecoservice.getSolicitud(nro)
    .then(sol=>{
        if ( sol.status == 'OK'){
            response.data.solicitud = sol.data;
        }else{
            response.status = 'NOK';
            response.messages.push('Error con el servicio getSolicitud');
        } 
    }).catch(err=>{response.status='NOK';response.messages.push('Error de comunicacion con el servicio getSolicitudes')});
    // Primera Etapa
    await ecoservice.getSolicitudPrimeraEtapa(nro)
    .then(prime=>{
        if ( prime.status == 'OK'){
            response.data.primera = prime.data;
        }else{
            response.status = 'NOK';
            response.messages.push('Error con el servicio getSolicitudPrimeraEtapa');
        } 
    }).catch(err=>{response.status='NOK';response.messages.push('Error de comunicacion con el servicio getSolicitudPrimeraEtapa')});
    // Segunda Etapa
    await ecoservice.getSolicitudSegundaEtapa(nro)
    .then(seg=>{
        if ( seg.status == 'OK'){
            response.data.segunda = seg.data;
        }else{
            response.status = 'NOK';
            response.messages.push('Error con el servicio getSolicitudSegundaEtapa');
        } 
    }).catch(err=>{response.status='NOK';response.messages.push('Error de comunicacion con el servicio getSolicitudSegundaEtapa')});
    // tercera Etapa
    await ecoservice.getSolicitudTerceraEtapa(nro)
    .then(seg=>{
        if ( seg.status == 'OK'){
            response.data.tercera = seg.data;
        }else{
            response.messages.push('Error con el servicio getSolicitudTerceraEtapa');
        } 
    }).catch(err=>{response.status='NOK';response.messages.push('Error de comunicacion con el servicio getSolicitudSegundaEtapa')});

    res.json(response);
});

module.exports = router;
