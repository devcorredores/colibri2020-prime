const { Router } = require('express');
const router = new Router();
const ds = require('../models');

// http://localhost:3003/api-seguros/seg-beneficiarios
router.get('/',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await ds.Seg_Beneficiarios.findAll({limit:10})
        .then(tomadors => {
            response.data = tomadors;
        }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;        
        });
    res.json(response);
});

router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    await ds.Seg_Beneficiarios.findOne({ where : {Ben_Id:id}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };

    await ds.Seg_Beneficiarios.create(req.body)
        .then(resp => {
            response.data = resp;
            response.message = 'Registro creado';
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
    });
    
    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Beneficiarios.findOne({ where : {Ben_Id:id}})
        .then(resp => {
            if(resp){
                resp.update(req.body);
                response.message = 'Registro actualizado';
            }else{
                response.status = 'ERROR';
                response.message = 'Registro no existe';
            }
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Beneficiarios.findOne({ where : {Ben_Id:id}})
    .then(resp => {
        if(resp){
            resp.destroy();
            response.message = "Registro eliminado"
        }else{
            response.status = 'ERROR';
            response.message = "Registro no exite"
        }
    })
    .catch(err=>{
        response.status = "ERROR",
        response.message = err
    })
    res.json(response);
});

router.get('/IdSol/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    await ds.Seg_Beneficiarios.findAll({ where : {Ben_IdSol:id}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

// this.segBeneficiariosService.updateAll
router.put('/all/:idSol', async(req, res) => {
    let response = { status: 'OK', messages: [], data: [] };
    const idSol = req.params.idSol;
    const beneficiarios = req.body;

    try {

        // beneficiarios
        for (let i = 0; i < beneficiarios.length ; i++){
            if (beneficiarios[i].Ben_Id){
                await ds.Seg_Beneficiarios.update(beneficiarios[i], { where: { Ben_Id: beneficiarios[i].Ben_Id } })
                .then((resp) => {
                    response.messages.push(` update beneficiario`);
                })
                .catch((err) => {
                    response.status = "NOK";
                    response.messages.push(` error beneficiario: ${err}`);
                });
            }else{
                await ds.Seg_Beneficiarios.create(beneficiarios[i])
                .then(resp => {
                    response.messages.push(` insert beneficiario`);
                })
                .catch((err) => {
                    response.status = 'ERROR';
                    response.messages.push(`${i}. error beneficiario ${err}`);
                });
            }
        };
        // recuperamos los beneficiarios de la solicitud
        await ds.Seg_Beneficiarios.findAll({ where : {Ben_IdSol:idSol}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
        
    } catch (error) {
        response.status = 'ERROR';
        response.messages = 'Error al actualizar BENEFICIARIOS';
    }

    res.json(response);
});

module.exports = router;
