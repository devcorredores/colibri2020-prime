const { Router } = require('express');
const router = new Router();
const ds = require('../models');
const fs = require('fs');
let path = require("path");
const path_config = path.join(__dirname,`../config/config-param.json`);
const configJs = require('../config/config-param');

const saveConfig = (data) =>{
    //const stringJson = JSON.stringify(data);
    fs.writeFileSync(path_config,data);
}

const getconfig = ()=>{
    const jsonData = fs.readFileSync(path_config);
    return JSON.parse(jsonData);
}

router.get('/',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    response.data = getconfig();
    res.json(response);
});

router.get('/:key', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const key = req.params.key;
    const configParam = getconfig();
    response.message = key;
    response.data = configParam[key];
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { contenido } = req.body;
    saveConfig(contenido);
    res.json(response);
});

// http://localhost:4005/api-seguros/config-param/keys
router.post('/keys', async(req, res) => {
    let response = { status: 'OK', message: 'keys', data: '' };
    const { keys } = req.body;
    const configParam = getconfig();
    let param = {};
    keys.forEach(key=>{
        param[key] = configParam[key];
    })
    response.data = param;
    res.json(response);
});

router.put('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    saveConfig(configJs)
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    res.json(response);
});

module.exports = router;
