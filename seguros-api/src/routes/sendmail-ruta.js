const { Router } = require('express');
const router = new Router();
const nodemailer = require('nodemailer');
const config_param = require('../config/config-param.json');

const transporter = nodemailer.createTransport({
    host: "mail.privateemail.com",
    port: 587,
    secure: false, // upgrade later with STARTTLS
    auth: {
      user: "desgravamen@seguroscolibri.com",
      pass: "Desgravamen123"
    }
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { to, cc ,subject, html } = req.body;

    try {

        const  mailOption = {
            from: 'desgravamen@seguroscolibri.com',
            to: to,
            cc: cc,
            subject: subject,
            html: html
        };
    
        transporter.sendMail(mailOption, function (error, info) {
            if (error) {
                console.log('ERROR_SEND_MAIL:',error);
                resp = `ERROR,${error}`;
            } else {
                console.log(`Email send:`,info.response);
                resp = `OK,${JSON.stringify(info.response)}`;
            }
        });

    } catch (error) {
        response.status = 'ERROR';
        response.message = `Error al intentar enviar correos ${error} `;
        console.log('ERROR, el enviar correos',error);
    }

    res.json(response);
});

module.exports = router;