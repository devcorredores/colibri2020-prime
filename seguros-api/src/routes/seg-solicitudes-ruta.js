const { Router } = require('express');
const router = new Router();
const ds = require('../models');
const ecofuturo = require('../module/ecofuturo-servicios');
const { QueryTypes } = require('sequelize');
const config_param = require('../config/config-param.json');
const parametros_campo = require('../config/parametros-campos.json');
const util = require("../module/util");
const nodemailer = require('nodemailer');

const config_param_ = async()=>{
    const consulta = "SELECT JSONData FROM Seg_Parametros WHERE Pmt_Tipo = 'DH' ";
    const resultSql = await ds.sequelize.query(consulta, {type: QueryTypes.SELECT});

    console.log('===============>');
    console.log(resultSql[0].JSONData);

    //return JSON.parse(resultSql);
};



// servicios : seg-solicitudes
router.get('/',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await ds.Seg_Solicitudes.findAll({limit:100,order: [["Sol_IdSol","DESC"]]})
        .then(resul => {
            response.data = resul;
        }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;        
        });
    res.json(response);
});

router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    await ds.Seg_Solicitudes.findOne({ where : {Sol_idSol:id}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.get('/usuario/:nombre_usuario', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const Sol_CodOficial = req.params.nombre_usuario;
    
    await ds.Seg_Solicitudes.findAll({ where : {Sol_CodOficial }})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });

    res.json(response);
});

// consulta busqueda 
router.get('/solicitud/:nro', async(req, res) => {
    let response = { status: 'OK', message: 'solicitud :>', data: {} };
    const nro = req.params.nro;
    // buscar en la tabla solicitudes
    response.data.solicitud = [];
    response.data.operacion={};
    response.data.deudor={};

    try {

        await ds.Seg_Solicitudes.findAll({ 
            where: {Sol_NumSol:nro},
            include : [{model:ds.Seg_Deudores}]
            })
            .then(resp => {
                if(resp){
                    response.data.solicitud = resp;
                }else{
                    response.status = "NOK";
                    response.message = "Registro no encontrado :>";
                }
            }
            ).catch(err => {
                response.status = 'NOK';
                response.message = err;
            });
        if(response.data.solicitud.length==0){
            //obtener datos operacion
            const datosPrimeraEtapa = await ecofuturo.getSolicitudPrimeraEtapa(nro);
            if(datosPrimeraEtapa){
                if(datosPrimeraEtapa.data.getSolicitudPrimeraEtapaResult.Operacion_Solicitud){
                    const data = datosPrimeraEtapa.data.getSolicitudPrimeraEtapaResult;
                    response.data.operacion = data;
                }
            };
            //obtener datos del deudor
            const datosSolicitud = await ecofuturo.getSolicitud(nro);
            if(datosSolicitud.data.getSolicitudResult){
                const cliente = datosSolicitud.data.getSolicitudResult.cliente;
                response.data.deudor = cliente;
            }
        };
        
    } catch (error) {
        response.status = 'NOK',
        response.message = 'Solicitud no pudo recuperarse, intente nuevamente :>';
    }

    res.json(response);
});

// para verificar la creacion de djs por fecha
router.get('/fecha-sol-djs/:fecha',async(req,res)=>{

    let response = { status: 'OK', message: 'fecha-sol-djs', data: {}, messages : [] };
    const fecha = req.params.fecha; // yyyy-mm-dd

    let data = {seg_solicitudes:[]};
    response.messages.push(`Fecha : ${fecha} `);

    await ds.Seg_Solicitudes.findAll({ where : {Sol_FechaSol:fecha},include : [{model:ds.Seg_Deudores}] })
    .then(solicitud => {
            if(solicitud){
                data.seg_solicitudes = solicitud;
                response.messages.push(`Recuparacion de seg_solicitudes`);
                response.data = data.seg_solicitudes;
            }
    });

    const consulta = `SELECT dbo.Seg_Solicitudes.Sol_FechaSol,Seg_Solicitudes.Sol_IdSol,
    Seg_Solicitudes.Sol_NumSol,
    Seg_Deudores.Deu_Id,Seg_Deudores.Deu_IdSol,Seg_Deudores.Deu_Nombre,
      ( select count(Adic_Id) from Seg_Adicionales
   where Adic_IdDeu = Seg_Deudores.Deu_Id and Adic_IdSol = Seg_Deudores.Deu_IdSol  ) as adicionales
  FROM dbo.Seg_Solicitudes
   INNER JOIN dbo.Seg_Deudores
   ON Seg_Solicitudes.Sol_IdSol = Seg_Deudores.Deu_IdSol
    WHERE Seg_Solicitudes.Sol_FechaSol = '${fecha}' `;

    const resultSql = await ds.sequelize.query(consulta, {type: QueryTypes.SELECT});

    if(resultSql){
        response.data = resultSql;
    }

    res.json(response);

});

router.post('/buscar',async(req,res)=>{
    let response = { status: 'OK', message: 'buscar', data: {} };

    const { take,skip,solicitud,deudor} = req.body;

    console.log('[busca]===>', req.body );

    let param_sol = {};
    if (solicitud.campo){
        param_sol[solicitud.campo] = `${solicitud.valor}`;
    };
    let param_deu = {};
    if(deudor.campo){
        param_deu[deudor.campo] = `${deudor.valor}`;
    };

    console.log('[busca param_sol]===>', param_sol );
    console.log('[busca param_deu]===>', param_deu );

    await ds.Seg_Solicitudes.findAll({ 
            where : param_sol ,
            offset:skip,
            limit:take,
            order: [["Sol_IdSol","DESC"]],
            include : [{
                model:ds.Seg_Deudores,
                where: param_deu
            }]})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "NOK";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });

    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { Sol_NumSol } = req.body;
    const param = await ds.Seg_Solicitudes.findOne({where:{Sol_NumSol}});
    if(param){
        response.status = 'ERROR',
        response.message = 'Registro existe'
    }else{
        await ds.Seg_Solicitudes.create(req.body)
        .then(resp => {
            response.data = resp;
            response.message = 'Registro creado';
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    }
    res.json(response);
});

router.post('/pag',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { take,skip,user } = req.body;
    Sol_CodOficial = (user? user : null);

    if(Sol_CodOficial) {

        await ds.Seg_Solicitudes.findAll({ 
            where : {Sol_CodOficial },
            offset:skip,
            limit:take,
            order: [["Sol_IdSol","DESC"]],
            include : [{model:ds.Seg_Deudores}]  })
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "NOK";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });

    } else {

        await ds.Seg_Solicitudes.findAll({ 
            offset:skip,
            limit:take,
            order: [["Sol_IdSol","DESC"]],
            include : [{model:ds.Seg_Deudores}]
         })
        .then(resul => {
            response.data = resul;
        }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;        
        });

    }

    res.json(response);
});

// UPDATE un registro de la solicitud
router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' ,messages : []};
    const id = req.params.id;

    await ds.Seg_Solicitudes.findOne({ where : {Sol_idSol:id}})
        .then(resp => {
            if(resp){
                resp.update(req.body);
                response.message = 'Registro actualizado';
            }else{
                response.status = 'ERROR';
                response.message = 'Registro no existe';
            }
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        }); 
    res.json(response);
});

//UPDATE (botton Actualizar) desde servicio SOAP a seg_solicitudes,seg_deudores,seg_operaciones

router.put('/update-servicios-data/:id',async(req,res)=>{
    let response = { status: 'OK', message: 'update-servicios-data', data: '', messages:[] };
    const id = req.params.id; // idsol

    const segSolicitudes = await ds.Seg_Solicitudes.findOne({ where : {Sol_idSol:id}});
    const nro_solicitud = segSolicitudes.Sol_NumSol;

    response.messages.push(`ID Solicitud : ${id}`);
    response.messages.push(`Num. Solicitud : ${nro_solicitud}`);

    let datosSolicitud;
    let datosPrimeraEtapa;
    let datosSegundaEtapa;
    let data = {
        segSolicitudes : {},
        segDeudores : [],
        segOperaciones : {},
        cliente : {},
        codeudores : []
    };
    
    try {

        datosSolicitud = await ecofuturo.getSolicitud(nro_solicitud); // deudor - codeudor
        datosPrimeraEtapa = await ecofuturo.getSolicitudPrimeraEtapa(nro_solicitud);  // getSolicitudPrimeraEtapaResult
        datosSegundaEtapa = await ecofuturo.getSolicitudSegundaEtapa(nro_solicitud); // getSolicitudSegundaEtapaResult

        data.segSolicitudes = datosPrimeraEtapa.data.getSolicitudPrimeraEtapaResult;
        data.segOperaciones = datosSegundaEtapa.data.getSolicitudSegundaEtapaResult;
        data.cliente =  datosSolicitud.data.getSolicitudResult.cliente; 

        if(datosSolicitud.data.getSolicitudResult.codeudores){
            data.codeudores = datosSolicitud.data.getSolicitudResult.codeudores.Codeudor;
        };
        
        // sacar los campos que son editables de la definiciion de campos
        let segSolicitudesUpdate = {};
        const solicitud_def = await parametros_campo.filter((row) => row.grupo === 'solicitud_credito');
        solicitud_def.forEach(elm =>{
            const valor = data.segSolicitudes[elm.campo_servicio];
            if(valor){
                segSolicitudesUpdate[elm.campo] = valor;
            }
        });
        // UPDATE seg_solicitudes
        await ds.Seg_Solicitudes.findOne({ where : {Sol_idSol:id}})
        .then(resp => {
            if(resp){
                resp.update(segSolicitudesUpdate);
                response.messages.push('Registro Seg_Solicitudes actualizado');
            }
        }).catch((err) => {response.status = 'ERROR';response.message = err;}); 

        // UPDATE seg_deudores - cliente
        let segDeudoresUpdate = {};
        const deudor_def = await parametros_campo.filter((row) => row.grupo === 'deudor');
        deudor_def.forEach(elm=>{
            const valor = data.cliente[elm.campo_servicio];
            //if(valor){
                segDeudoresUpdate[elm.campo] = valor;
            //}
        });
        segDeudoresUpdate.Deu_Actividad = `${data.cliente.Deudor_Actividad} - ${data.cliente.Deudor_Detalle_Actividad}`;
        
        // actualiza al deudor
        await ds.Seg_Deudores.findOne({ where : {Deu_IdSol:id,Deu_NIvel:0}})
        .then(resp => {
            if(resp){
                resp.update(segDeudoresUpdate);
                response.messages.push(`Seg_Deudores titular id : ${resp.Deu_Id}, se actualizo con exito`);
            }
        }
        ).catch(err => {response.status = 'ERROR';response.message = err;});

        // UPDATE Seg_deudores - Codeudores
        const codeudor_def = await parametros_campo.filter((row) => row.grupo === 'codeudor');

        for(const codeu of  data.codeudores ){

            const codeu_def = JSON.parse(JSON.stringify(codeudor_def));
            let segCoDeudoresUpdate = {};
            codeu_def.forEach(el=>{
                const valor = codeu[el.campo_servicio];
                //if(valor){
                    segCoDeudoresUpdate[el.campo] = valor;
                //}
            });
            segCoDeudoresUpdate.Deu_Actividad = `${codeu.Codeudor_Actividad} - ${codeu.Codeudor_Detalle_Actividad}`;
            
            // buscar codeudor para actualizar de lo contrario crea una nuevo por el catnet de identidad
            await ds.Seg_Deudores.findOne({ where : {Deu_IdSol:id,Deu_NumDoc:`${codeu.Codeudor_Numero_Documento}`}})
            .then(resp => {
                if(resp){
                    resp.update(segCoDeudoresUpdate);
                    response.messages.push(`Seg_Deudores actualizado codeudor documento: ${codeu.Codeudor_Numero_Documento}`);
                }else{
                    response.messages.push(`NO encontrado Seg_Deudores codeudor documento: [${codeu.Codeudor_Numero_Documento}] `);
                    segCoDeudoresUpdate.Deu_NIvel = 1;
                    segCoDeudoresUpdate.Deu_IdSol = id;
                    segCoDeudoresUpdate.Deu_Incluido = 'S';
                    segCoDeudoresUpdate.Deu_MontoActAcumVerif = 0;
                    ds.Seg_Deudores.create(segCoDeudoresUpdate);
                }
            }
            ).catch(err => {response.status = 'ERROR';response.message = err;});

        }

        // UPDATE seg_operaciones
        await ds.Seg_Operaciones.findOne({ where : {Operacion_IdSol:id}})
        .then(resp => {
            if(resp){
                resp.update(data.segOperaciones);
                response.messages.push('Registro Seg_Operaciones actualizado');
            }
        }).catch((err) => {response.status = 'ERROR';response.message = err;});
            
    } catch (ex) {
        response.status = 'ERROR';
        response.message = ex;  
    }

    res.json(response);
});

// UPDATE ALL Seg_Solicitudes Seg_Deudores SOPORTE
router.put('/soporte-solicitud/:id', async(req, res) => {
    let response = { status: 'OK', message: 'Solicitudes y Deudores', data: {} ,messages : []};
    const id = req.params.id;

    const {data,observaciones,usuario} = req.body;

    // guardar en observaciones
    //    const cmdsql = `INSERT INTO Seg_Observaciones VALUES(${id},'${observaciones.motivo}',
    //'${data.seg_solicitudes.Sol_CodOficial}' ,'1','',CONVERT(date, GETDATE()) , CONVERT(CHAR(8), DATEADD(HOUR,-4,GETDATE()), 108) ,'C')`;
    //    ds.sequelize.query(cmdsql, {type: QueryTypes.INSERT});

    try {

        if(observaciones.motivo){
            const segObservaciones = {
                Obs_IdSol: id,
                Obs_Observacion : observaciones.motivo,
                Obs_Usuario: usuario,
                Obs_Eeff:'1',
                Obs_Aseg:'',
                Obs_FechaReg : util.getDateYYYYmmDD(),
                Obs_HoraReg : util.getHoraActual(),
                Obs_Cerrado:'C'
            };

            await ds.Seg_Observaciones.create(segObservaciones)
            .then(resp => {
                response.data = segObservaciones;
                response.message = 'Registro creado';

            })
            .catch((err) => {
                response.status = 'ERROR';
                response.message = err;
            });
        }

        // update seg_solicitudes
        await ds.Seg_Solicitudes.findOne({ where : {Sol_idSol:id}})
            .then(resp => {
                if(resp){
                    resp.update(data.seg_solicitudes);
                    response.message = 'Registro actualizado';
                }else{
                    response.status = 'ERROR';
                    response.message = 'Registro no existe';
                }
            })
            .catch((err) => {
                response.status = 'ERROR';
                response.message = err;
            });

        // update seg_deudores
        for(const deudor of data.seg_deudores ){
            const deuObj = await ds.Seg_Deudores.findOne({ where : {Deu_Id: deudor.Deu_Id} });
            if(deuObj){
                await deuObj.update(deudor);
            }
        }

    } catch (error) {

        response.status = "ERROR";
        response.message = error
    }
    
    res.json(response);
});



// DELETE Seg_Solicitudes | Seg_Deudores | Seg_Observaciones | Seg_Beneficiarios
//        Seg_Adicionales
router.delete("/:id", async (req, res) => {
    let response = { status: "OK", messages: [],message:'' };
    const id = req.params.id;
    let messages = [];
  
    messages.push(`Sol_idSol: ${id}`);
    // solicitud credito
    await ds.Seg_Solicitudes
      .destroy({ where: { Sol_idSol: id } })
      .then((resp) => {
        messages.push("Seg_Solicitudes destroy");
      })
      .catch((err) => {
        err;
        response.status = "NOK";
      });
    // deudor
    await ds.Seg_Deudores
      .destroy({ where: { Deu_IdSol: id } })
      .then((resp) => {
        messages.push("Seg_Deudores destroy");
      })
      .catch((err) => {
        messages.push(err);
        response.status = "NOK";
      });
    // beneficiarios
    await ds.Seg_Beneficiarios
      .destroy({ where: { Ben_IdSol: id } })
      .then((resp) => {
        messages.push("Seg_Beneficiarios destroy");
      })
      .catch((err) => {
        messages.push(err);
        response.status = "NOK";
      });
    // declaracion de salud
    await ds.Seg_Adicionales
      .destroy({ where: { Adic_IdSol: id } })
      .then((resp) => {
        messages.push("Seg_Adicionales destroy");
      })
      .catch((err) => {
        messages.push(err);
        response.status = "NOK";
      });
    // observaciones
    await ds.Seg_Observaciones
      .destroy({ where: { Obs_IdSol: id } })
      .then((resp) => {
        messages.push("Seg_Observaciones destroy");
      })
      .catch((err) => {
        messages.push(err);
        response.status = "NOK";
    });
    // aprobaciones
    await ds.Seg_Aprobaciones
      .destroy({ where: { Apr_IdSol: id } })
      .then((resp) => {
        messages.push("Seg_Aprobaciones destroy");
      })
      .catch((err) => {
        messages.push(err);
        response.status = "NOK";
    });
    // operaciones
    await ds.Seg_Operaciones
    .destroy({ where: { Operacion_IdSol: id } })
    .then((resp) => {
      messages.push("Seg_Aprobaciones destroy");
    })
    .catch((err) => {
      messages.push(err);
      response.status = "NOK";
  });
  
    response.messages = messages;
    res.json(response);
  });

router.post("/add-desgravamen", async (req, res) => {

    let response = { status: "OK", messages: [],message:'',data: {} };
    let messages = [];

    const { nro_solicitud,user } = req.body;

    const Sol_NumSol = nro_solicitud;
    let id_folder = 0;
    let id_deudor = 0;
    let id_codeudores = [];
    let id_tipo_seguro = 0;
    let tipo_solicitud;

    const tiposSolicitudes = [
        '2 L - SOLIC. BAJO LINEA SFI',
        '7 L - SOLIC. BAJO LINEA NUEVA',
        '8 L - SOLIC. BAJO AMPLIACION DE LINEA',
        '10 L - SOLIC BAJO LINEA NUEVA CON REFINANCIAMIENTO'];

    let message_log = {
        num_solicitud:nro_solicitud,
        action:'add-desgravamen',
        message:'',
        messages:[],
        data:{},user,status:'OK'};

    // verificamos si existe la solicitud
    const folder = await ds.Seg_Solicitudes.findOne({where:{Sol_NumSol:nro_solicitud.toString()}});
    if (folder) {
        response.status = 'NOK'; 
        messages.push(`Solicitud ${nro_solicitud} ya existe`);
    } else {
        messages.push(`Inicio nueva solicitud : ${nro_solicitud}`);
        // invocamos servicios soap getSolicitud, getSolicitudPrimeraEtapa, getSolicitudSegundaEtapa
        let datosSolicitud;
        let datosPrimeraEtapa;
        let datosSegundaEtapa;
        let segSolicitudes = {};
        let segOperaciones = {};
        let segDeudores = [];
        let segDeudor = {}
        
        try{
            datosSolicitud = await ecofuturo.getSolicitud(nro_solicitud); // cliente | codeudores 
            datosPrimeraEtapa = await ecofuturo.getSolicitudPrimeraEtapa(nro_solicitud);
            datosSegundaEtapa = await ecofuturo.getSolicitudSegundaEtapa(nro_solicitud);
        }catch(ex){
            response.status = 'ERROR';
            response.message = 'No estan disponibles los servicios del Banco';
            messages.push('Error en comunicación en los servicios del banco');
        };

        
        // Insert into Seg_Solicitudes 
        if(datosPrimeraEtapa && datosSegundaEtapa){

            // EL SISTEMA CENTRAL DEL BANCO NO PROPORCIONO LA INFORMACION MINIMA NECESARIA.
            // POR FAVOR, CONTACTESE CON SOPORTE DEL BANCO
            let tipoCredito; // Tipo de Crédito = Operacion_Tipo_Credito
            let tipoAsfi; // Tipo de Crédito sg/ASFI = Operacion_Tipo_Asfi
            let codigoTipoGarantia; //Tipo Garantia Codigo = Operacion_Tipo_Grantia_Codigo
            let codigoDestino; //Destino Codigo = Operacion_Destino_Codigo

            let message_required = 'EL SISTEMA CENTRAL DEL BANCO NO PROPORCIONO LA INFORMACION MINIMA NECESARIA.';
            let esRequerido = false;

            try {
                //validamos datos requeridos
                if(!datosPrimeraEtapa.data.getSolicitudPrimeraEtapaResult.Operacion_Tipo_Credito){message_required += ', Tipo de Crédito es requerido'; esRequerido=true };
                if(!datosPrimeraEtapa.data.getSolicitudPrimeraEtapaResult.Operacion_Tipo_Asfi){message_required += ', Tipo de Crédito ASFI es requerido '; esRequerido=true};
                if(!datosPrimeraEtapa.data.getSolicitudPrimeraEtapaResult.Operacion_Tipo_Solicitud){message_required += ', Tipo de Solicitud es requerido '; esRequerido=true};

                if(!datosSegundaEtapa.data.getSolicitudSegundaEtapaResult.Operacion_Tipo_Grantia_Codigo){message_required += ', Tipo Garantia Codigo es requerido'; esRequerido=true};
                if(!datosSegundaEtapa.data.getSolicitudSegundaEtapaResult.Operacion_Destino_Codigo){message_required += ', Destino Codigo es requerido'; esRequerido=true};

                tipo_solicitud = datosPrimeraEtapa.data.getSolicitudPrimeraEtapaResult.Operacion_Tipo_Solicitud;
                
            } catch (error) {
                message_required += 'Tipo de Crédito, Tipo de Crédito ASFI, Tipo Garantia Codigo y Destino Codigo';
                esRequerido = true;
            }

            
            if(esRequerido){
                if(tipo_solicitud){
                    if(tiposSolicitudes.includes(tipo_solicitud)){
                        esRequerido = false;
                    }
                }
            };

            if(!esRequerido){

                if(datosPrimeraEtapa.data.getSolicitudPrimeraEtapaResult.Operacion_Solicitud){
                    // obtenemos los campos de definicion relacion campo tabla == campo servicios
                    const solicitud_def = await parametros_campo.filter((row) => row.grupo === 'solicitud_credito');
                    const primeraEtapa = datosPrimeraEtapa.data.getSolicitudPrimeraEtapaResult;
                    // crear campos de la seg_solicitud segun definicio parametros campos

                    solicitud_def.forEach(elm =>{
                        const valor = primeraEtapa[elm.campo_servicio];
                        if(valor){
                            segSolicitudes[elm.campo] = valor;
                        }
                    });
                    // nuevo numero de solicitud
                    const sql = 'select (max(Sol_IdSol)+1) as Sol_IdSol from Seg_Solicitudes';
                    const resultSql = await ds.sequelize.query(sql, {type: QueryTypes.SELECT});
                    let sol_idsol_new = 1
                    if(resultSql[0].Sol_IdSol){
                        sol_idsol_new = resultSql[0].Sol_IdSol;
                    };
                    // Sol_IdSol
                    const Sol_IdSol = sol_idsol_new;
                    segSolicitudes.Sol_IdSol = Sol_IdSol;
                    segSolicitudes.Sol_NumSol = nro_solicitud;
                    // entidad de seguros 1 y estado P = EN PROCESO
                    //segSolicitudes.Sol_Edff = '1';
                    segSolicitudes.Sol_EstadoSol = 'P';
                    // 2018-07-25 00:00:00  2021-10-26
                    if(segSolicitudes.Sol_FechaSol){
                        segSolicitudes.Sol_FechaSol = segSolicitudes.Sol_FechaSol.substring(0,10);
                    };

                    segSolicitudes.Sol_TipoOpeUni = primeraEtapa.Operacion_Tipo_Solicitud;
                    segSolicitudes.Sol_SolTipoSol = primeraEtapa.Operacion_Tipo_Solicitud;
    
                    // INSERT Seg_Solicitudes
                    await ds.Seg_Solicitudes.create(segSolicitudes)
                    .then((resp) => {
                        messages.push("Seg_Solicitudes, creado");
                    })
                    .catch((err) => {
                        response.status = "ERROR";
                        messages.push('No se pudo crear Seg_Solicitudes');
                    });
    
                    response.data = {Sol_IdSol : segSolicitudes.Sol_IdSol };
    
                    // DATOS DEUDOR  
                    if(datosSolicitud.data.getSolicitudResult){
                        const deudor_def = await parametros_campo.filter((row) => row.grupo === 'deudor');
                        const cliente = datosSolicitud.data.getSolicitudResult.cliente;
                        // definicion datos deudores 

                        for (const elm of deudor_def){
                            const valor = cliente[elm.campo_servicio];
                            if(valor){
                                segDeudor[elm.campo] = `${valor}`;
                            }
                        };

                        segDeudor.Deu_NIvel = 0;
                        segDeudor.Deu_IdSol = sol_idsol_new;
                        segDeudor.Deu_Incluido = 'S';
                        segDeudor.Deu_MontoActAcumVerif = 0;
                        segDeudor.Deu_Actividad = `${cliente.Deudor_Actividad} - ${cliente.Deudor_Detalle_Actividad}`;

                        // INSERT Seg_Deudores titular
                        await ds.Seg_Deudores.create(segDeudor)
                        .then((deu) => { 
                            messages.push("Seg_Deudores se creo con existo");
                        })
                        .catch((err) => {
                            response.status = "ERROR";
                            messages.push('No se pudo crear el registro Seg_deudores');
                        });
    
                        // DATOS CODEUDORES
                        const codeudores = datosSolicitud.data.getSolicitudResult.codeudores;
                        if(codeudores){

                            const codeudor_def = await parametros_campo.filter((row) => row.grupo === 'codeudor');
                            let codeu_def = {};

                            for(const codeu of codeudores.Codeudor){

                                codeu_def = JSON.parse(JSON.stringify(codeudor_def));
                                let codeudorObj = {};

                                for(const el of codeu_def){
                                    const valor = codeu[el.campo_servicio];
                                    if(valor){
                                      codeudorObj[el.campo] = `${valor}`;
                                    }
                                }
                                
                                codeudorObj.Deu_NIvel = 1;
                                codeudorObj.Deu_IdSol = sol_idsol_new;
                                codeudorObj.Deu_Incluido = 'S';
                                codeudorObj.Deu_MontoActAcumVerif = 0;
                                codeudorObj.Deu_Actividad = `${codeu.Codeudor_Actividad} - ${codeu.Codeudor_Detalle_Actividad}`;
                                segDeudores.push(codeudorObj);
    
                                await ds.Seg_Deudores.create(codeudorObj)
                                .then((codeu) => {
                                    messages.push("Se creo registro en Seg_Deudores de los codeudores");
                                })
                                .catch((err) => {
                                    response.status = "ERROR";
                                    messages.push("No se pudo crear registro de los codeudores en Seg_Deudores");
                                });
                            }
    
                        };
    
                        // INSERT OPERACION CREDITICIA
                        if (datosSegundaEtapa.data.getSolicitudSegundaEtapaResult){
                            const segundaEtapa = datosSegundaEtapa.data.getSolicitudSegundaEtapaResult;
                            segOperaciones = {... segundaEtapa};
                            segOperaciones.Operacion_IdSol = Sol_IdSol;
                            await ds.Seg_Operaciones.create(segOperaciones)
                            .then((resp) => {
                                messages.push("Se ha creado con exito en Seg_Operaciones");
                            })
                            .catch((err) => {
                                response.status = "ERROR";
                                messages.push("No se pudo crear registro en Seg_Operaciones");
                            });
                        }
    
                    }
                }


            }else{
                response.status = 'ERROR';
                response.message = message_required;
            }


        }else{
            response.status = 'NOK';
            messages.push('Datos del servicio : datosPrimeraEtapa estan nulos');
        }
    };
    response.messages = [...messages];
    res.json(response);

    //message_log.status = response.status;
    //message_log.message = response.message;
    //message_log.messages = response.messages;
    //AddLog(message_log);
    
});

router.get('/update-poliza/:idsol', async(req, res) => {

    let response = { status: 'OK', message: '',messages:[], data: {} };
    let messages = [];
    const idsol = req.params.idsol;

    let segSolicitudes = {};
    let segOperaciones = {};
    
    // Seg_Solicitudes
    await ds.Seg_Solicitudes.findOne({ where : {Sol_idSol:idsol}})
    .then(resp => { segSolicitudes = resp; }
    ).catch(err => { response.status = 'ERROR'; messages.push(`Seg_Solicitudes,${err}`); });
    // Seg_Operaciones
    await ds.Seg_Operaciones.findOne({ where: { Operacion_IdSol: idsol } })
    .then((resp) => { segOperaciones = resp; })
    .catch((err) => { response.status = "ERROR";messages.push(`Seg_Operaciones,${err}`);});
    // operaciones vacias invocamos al servicios
    let isEmpty = Object.keys(segOperaciones).length === 0;
    if(isEmpty){
        messages.push('Seg_Operaciones esta vacio');
        try{
            const nro_solicitud = segSolicitudes.Sol_NumSol;
            let datosSegundaEtapa = await ecofuturo.getSolicitudSegundaEtapa(nro_solicitud);
            if (datosSegundaEtapa.data.getSolicitudSegundaEtapaResult){
                const segundaEtapa = datosSegundaEtapa.data.getSolicitudSegundaEtapaResult;
                segOperaciones = {... segundaEtapa};
                segOperaciones.Operacion_IdSol = idsol;
                await ds.Seg_Operaciones.create(segOperaciones)
                .then((resp) => {
                    messages.push("segOperaciones, creado");
                })
                .catch((err) => {
                    response.status = "ERROR";
                    messages.push(err);
                });
            }
          }catch(ex){
            response.status = 'ERROR';
            response.message = ex;
            messages.push('Datos del Servicio Solicitud Segunda Etapa, error en comunicacion');
          }
    }
    // * parametros para identificar TIPO SEGURO | TIPO COBERTURA.
    const tipoCredito = segSolicitudes.Sol_TipoCred;
    const tipoAsfi = segSolicitudes.Sol_TipoAsfi;
    const montoSolicituado = segSolicitudes.Sol_MontoSol;
    const monedaSolicitado = segSolicitudes.Sol_MonedaSol;// BOLIVIANOS|DOLARES AMERICANOS
    messages.push(`Datos Solicitud, Monto Solicitudo : ${montoSolicituado} ${monedaSolicitado}`);
    messages.push(`Datos Solicitud, Tipo Credito y tipo Asfi  : ${tipoCredito}, ${tipoAsfi}`);
    // 1. Determinar tipo seguro : LICITADA | NO LICITADA
    let tipoSeguro = 'NO LICITADA';
    let tipoCobertura = '';
    let montoCredito = 1;
    let nro_poliza = '';
    let tasa_anual_ref = 0.00;
    isEmpty = Object.keys(segOperaciones).length === 0;
    if(!isEmpty){
        // * parametros para identificar CODIGO DESTINO = 60221 | GARANTIA CODIGO = OT9b1
        const codigoTipoGarantia = segOperaciones.Operacion_Tipo_Grantia_Codigo;
        const codigoDestino = segOperaciones.Operacion_Destino_Codigo;
        const montoAprobado = segOperaciones.Operacion_Monto_Aprobado;
        const monedaAprobado = segOperaciones.Operacion_Moneda_Aprobada;
        // 1.1 caedec destino (getSolicitudSegundaEtapaResult.Operacion_Destino_Codigo)
        const tipoSeguroDestino = config_param.tipo_seguro_licitada.caedec_destino.find(
            row=>row.codigo_detino == codigoDestino );
        tipoSeguro = (tipoSeguroDestino ? tipoSeguroDestino.tipo_seguro : 'NO LICITADA');
        if (tipoSeguroDestino ){messages.push(`Determinacion de cartera, Destino Codigo : ${codigoDestino} es LICITADA `);} 
        // 1.2 tipo garantia
        if(tipoSeguro == 'NO LICITADA'){
            const tipoSeguroGarantia = config_param.tipo_seguro_licitada.tipo_garantia.find(
            row=>row.codigo_garantia == codigoTipoGarantia );
            tipoSeguro = (tipoSeguroGarantia ? tipoSeguroGarantia.tipo_seguro : 'NO LICITADA');
            if(tipoSeguroGarantia){messages.push(`Determinacion de cartera, Codigo Tipo Garantia : ${codigoTipoGarantia} es LICITADA `);}
        };
        // 1.3 Tipo garantia y tipo asfi
        if(tipoSeguro == 'NO LICITADA'){
            const tipoSeguroGarantiaAsfi = config_param.tipo_seguro_licitada.garantia_tipo_asfi.find(
            row=>row.codigo_garantia == codigoTipoGarantia && row.tipo_asfi ==  tipoAsfi);
            tipoSeguro = (tipoSeguroGarantiaAsfi ? tipoSeguroGarantiaAsfi.tipo_seguro : 'NO LICITADA');
            if(tipoSeguroGarantiaAsfi){messages.push(`Determinacion de cartera, Tipo garantia y tipo asfi : ${codigoTipoGarantia} y ${tipoAsfi} es LICITADA `);}
        };
        // 1.4 Tipo credito y Garantia
        if(tipoSeguro == 'NO LICITADA'){
            const tipoSeguroCreditoGarantia = config_param.tipo_seguro_licitada.tipo_credito_garantia.find(
                row=>row.codigo_tipo_credito == tipoCredito.substr(0,4)
                && row.codigo_garantia_corto ==  codigoTipoGarantia.substr(0,1));
            tipoSeguro = (tipoSeguroCreditoGarantia ? tipoSeguroCreditoGarantia.tipo_seguro : 'NO LICITADA');
            if(tipoSeguroCreditoGarantia){messages.push(`Determinacion de cartera, Tipo credito y Garantia : ${codigoTipoGarantia} , ${tipoCredito} es LICITADA `);};
        };
        // 1.5 Tipo credito y tipo asfi y garantia
        if(tipoSeguro == 'NO LICITADA'){
            const tipoSeguroCreditoAsfiGerantia = config_param.tipo_seguro_licitada.tipo_credito_asfi_garantia.find(
                row=>row.codigo_tipo_credito == tipoCredito.substr(0,4)
                && row.codigo_garantia_corto ==  codigoTipoGarantia.substr(0,1) && row.tipo_asfi == tipoAsfi);
            tipoSeguro = (tipoSeguroCreditoAsfiGerantia ? tipoSeguroCreditoAsfiGerantia.tipo_seguro : 'NO LICITADA');
            if(tipoSeguroCreditoAsfiGerantia){messages.push(`Tipo credito y tipo asfi y garantia: ${tipoCredito} , ${codigoTipoGarantia} y ${tipoAsfi} es LICITADA `);}
        };
    }else{
        messages.push('Seg_Operaciones el objecto sigue vacio');
    }

    // 3. Determinamos la POLIZA
    const datosPoliza = config_param.datos_poliza.find(row=>
        row.tipo_seguro == tipoSeguro && row.moneda == monedaSolicitado && row.vigente == true
    );
    nro_poliza = (datosPoliza? datosPoliza.nro_poliza:'');
    tasa_anual_ref = (datosPoliza? datosPoliza.tasa_anual_ref:0.00);

    // 4.1 Determinar tipo de cobertura LICITADA
    if(tipoSeguro == 'LICITADA'){
        tipoCobertura = 'DH';
    }

    // 4.2 Determinar cobertura no [NO LICITADA]
    if(tipoSeguro == 'NO LICITADA'){
        montoCredito = parseFloat(montoSolicituado);
        const tiposCoberturaNoLicitada = config_param.tipo_cobertura_no_licitada.find(row=> 
          montoCredito => row.monto_desde && montoCredito <= row.monto_hasta && row.nro_poliza == nro_poliza
           && row.moneda == monedaSolicitado);
    
        if(tiposCoberturaNoLicitada){
            tipoCobertura = tiposCoberturaNoLicitada.tipo_cobertura;
        }else{
            messages.push('Tipo de covertura para la NO LICITADA no esta parametrizada');
        }
    }

    // Sol_TipoParam | Sol_TipoSeg | Sol_Poliza
    segSolicitudes.Sol_TipoSeg = tipoSeguro;
    segSolicitudes.Sol_TipoParam = tipoCobertura;
    segSolicitudes.Sol_Poliza = nro_poliza;
    //actualizar Seg_solicitudes
    await ds.Seg_Solicitudes.findOne({ where : {Sol_idSol:idsol}})
    .then(resp => {
            if(resp){
                const solicitud = {
                    Sol_TipoSeg : tipoSeguro,
                    Sol_TipoParam : tipoCobertura,
                    Sol_Poliza : nro_poliza,
                    Sol_TasaSeg : tasa_anual_ref
                };
                resp.update(solicitud);
                messages.push('SegSolicitudes se actualizo con exito');
            }
    })
    .catch((err) => {
        response.status = 'ERROR';
        response.message = err;
        messages.push('SegSolicitudes no se pudo actualizar');
    });
    // crear DJS segun la poliza
    if(response.status=='OK'){
        const djsFrm = await config_param.djs_salud.filter(row=>
            row.nro_poliza == nro_poliza);
        let segDeudores = [];
        
        // extraemos los deudores
        await ds.Seg_Deudores.findAll({ where: {Deu_IdSol:idsol}})
            .then(resp => {
                if(resp){
                    segDeudores = resp;
                }else{
                    response.status = "NOK";
                    response.message = "Registro no encontrado"
                }
            }
            ).catch(err => {
                response.status = 'NOK';
                response.message = err;
        });
        // extraemos DJS para verificar si existen
        const sql = `delete from Seg_Adicionales where Adic_IdSol = ${idsol}`;
        const resultSql = await ds.sequelize.query(sql, {type: QueryTypes.DELETE});
        messages.push(`Seg_Adicionales se borro para los ${idsol}`)
        // creamos DJS por cada deudor
        segDeudores.forEach((deudor)=>{
            djsFrm.forEach(async(el)=>{
                const pregunta = {
                    Adic_IdSol : idsol,
                    Adic_IdDeu : deudor.Deu_Id,
                    Adic_Pregunta : el.nro,
                    Adic_Texto : el.pregunta,Adic_Respuesta : '',
                    Adic_Comment : ''
                };
                await ds.Seg_Adicionales.create(pregunta)
                .then(resp => {messages.push(`Seg_Adicionales djs creado para ${deudor.Deu_Id}`)})
                .catch(err => {response.status='NOK';messages.push(err)});
            });
        });

    }

    response.data = {tipoSeguro , tipoCobertura , nro_poliza};

    response.messages = messages;
    res.json(response);

});

router.get('/getcodeudores/:numsol', async(req, res) => {
    
    const nro_solicitud = req.params.numsol;

    let response = { status: "OK", messages: [],message:'',data: {} };
    let messages = [];

    let datosSolicitud;
    let datosPrimeraEtapa;
    let datosSegundaEtapa;
    let segSolicitudes = {};
    let segOperaciones = {};
    let segDeudores = [];
    let segDeudor = {}

    try{
        datosSolicitud = await ecofuturo.getSolicitud(nro_solicitud);
        datosPrimeraEtapa = await ecofuturo.getSolicitudPrimeraEtapa(nro_solicitud);
        datosSegundaEtapa = await ecofuturo.getSolicitudSegundaEtapa(nro_solicitud);
    }catch(ex){
        response.status = 'ERROR';
        response.message = 'No estan disponemos con el servicios de datos del Banco';
    }

    // DATOS CODEUDORES
    const codeudores = datosSolicitud.data.getSolicitudResult.codeudores;
    if(codeudores){
        const codeudor_def = await parametros_campo.filter((row) => row.grupo === 'codeudor');
        let codeu_def = {};
        codeudores.Codeudor.forEach(async (codeu)=>{
        codeu_def = JSON.parse(JSON.stringify(codeudor_def));
        let codeudorObj = {};
        codeu_def.forEach(el=>{
            const valor = codeu[el.campo_servicio];
                if(valor){
                    codeudorObj[el.campo] = `${valor} `;
                }
            });
            codeudorObj.Deu_NIvel = 1;
            codeudorObj.Deu_IdSol = 123;
            codeudorObj.Deu_Incluido = 'S';
            // Deu_Actividad = Deudor_Actividad + Deudor_Detalle_Actividad
            codeudorObj.Deu_Actividad = `${codeu.Codeudor_Actividad} - ${codeu.Codeudor_Detalle_Actividad}`;
            segDeudores.push(codeudorObj);
        });
    };

    response.data = segDeudores;
    
    res.json(response);
});

router.get('/tipo-cartera/:numsol', async(req, res) => {

    const nro_solicitud = req.params.numsol;

    let response = { status: "OK", messages: [],message:'',data: {} };
    let messages = [];

    let segSolicitudes = {};

    await ds.Seg_Solicitudes.findAll({ 
        where: {Sol_NumSol:nro_solicitud}
        })
        .then(resp => {
            segSolicitudes = resp;
        }
        ).catch(err => {
            response.status = 'NOK';
            response.message = err;
        });

    if(response.status=='OK'){

        // * parametros para identificar TIPO SEGURO | TIPO COBERTURA.
        const tipoCredito = segSolicitudes.Sol_TipoCred;
        const tipoAsfi = segSolicitudes.Sol_TipoAsfi;
        const montoSolicituado = segSolicitudes.Sol_MontoSol;
        const monedaSolicitado = segSolicitudes.Sol_MonedaSol;// BOLIVIANOS|DOLARES AMERICANOS

        // *** TIPO SEGURO (Sol_TipoSeg) | TIPO COBERTURA (Sol_TipoParam)
        let tipoSeguro;
        let tipoCobertura;
        let montoCredito = 1;

    }

    response.data = segSolicitudes;
    res.json(response);

});

router.post('/desistimiento-solicitud',async(req,res)=>{
    let response = { status: 'OK', message: 'historial estados', data: {} };

    const {causal,autorizado_por,idsol,usuario} = req.body;

    const fecha = util.getDateYYYYmmDD();
    const hora = util.getHoraActual();
    const estado = 'D';

    let segObservaciones = {};
    let segHistEstados = {};
    let segSolicitudes = {}; 

    // Insertamos en Seg_Observaciones
    segObservaciones.Obs_IdSol = idsol;
    segObservaciones.Obs_Observacion = causal;
    segObservaciones.Obs_FechaReg = fecha;
    segObservaciones.Obs_HoraReg = hora;
    segObservaciones.Obs_Usuario = usuario;

    await ds.Seg_Observaciones.create(segObservaciones)
    .then(resp => {
        response.data = resp;
        response.message = 'Seg_Observaciones Registro creado';
    })
    .catch((err) => {response.status = 'ERROR';response.message = err;});
    // Insertamos en Seg_HistEstados
    segHistEstados.Est_IdSol = idsol;
    segHistEstados.Est_Usuario = usuario;
    segHistEstados.Est_FechaReg = fecha;
    segHistEstados.Est_HoraReg = hora;
    segHistEstados.Est_Estado = estado;
    await ds.Seg_HistEstados.create(segHistEstados)
    .then(resp => {
        response.data = resp;
        response.message = 'segHistEstados Registro creado';
    })
    .catch((err) => {
        response.status = 'ERROR';
        response.message = err;
    });
    // Actualizamos Seg_Solicitudes
    segSolicitudes.Sol_EstadoSol = estado;
    await ds.Seg_Solicitudes.findOne({ where : {Sol_idSol:idsol}})
    .then(resp => {
        if(resp){
            resp.update(req.body);
            response.message = 'Registro actualizado';
        }else{
            response.status = 'ERROR';
            response.message = 'Registro no existe';
        }
    })
    .catch((err) => {
        response.status = 'ERROR';
        response.message = err;
    });

    res.json(response);
});

router.get('/historial-estados/:idsol', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.idsol;

    await ds.Seg_HistEstados.findAll({ where : {Est_IdSol:id}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);

});

router.post('/historial-estados',async(req,res)=>{
    let response = { status: 'OK', message: 'historial estados', data: {} };

    const segHistEstados = req.body;

    await ds.Seg_HistEstados.create(segHistEstados)
    .then(resp => {
        response.data = resp;
        response.message = 'segHistEstados Registro creado';
    })
    .catch((err) => {
        response.status = 'ERROR';
        response.message = err;
    });

    res.json(response);
});

//CONSULTA PRINCIPAL
router.get("/idsol-all/:id", async(req,res)=>{
    let response = { status: "OK",message: "",messages: [], data: {} };
    const id = req.params.id;
    let messages = [`Proceso consulta ID SOLICITUD : ${id}`];

    // EL SISTEMA CENTRAL DEL BANCO NO PROPORCIONO LA INFORMACION MINIMA NECESARIA.
    // POR FAVOR, CONTACTESE CON SOPORTE DEL BANCO
    let tipoCredito; // Tipo de Crédito
    let tipoAsfi; // Tipo de Crédito sg/ASFI
    let codigoTipoGarantia; //Tipo Garantia Codigo
    let codigoDestino; //Destino Codigo
    let message_required = 'EL SISTEMA CENTRAL DEL BANCO NO PROPORCIONO LA INFORMACION MINIMA NECESARIA.';
    let esRequerido = false;


    data = {
      seg_solicitudes: {},
      seg_deudores: [],
      seg_beneficiarios:[],
      seg_adicionales:[],
      seg_observaciones:[],
      seg_operaciones:{},
      seg_aprobaciones:[],
      getSolicitudPrimeraEtapa:{},
      getSolicitudSegundaEtapa:{},
      getSolicitudTerceraEtapa:{},
      estado_general:'',
      config_param:{}
    };

    //Obtener parametros config
    const consulta = "SELECT JSONData FROM Seg_Parametros WHERE Pmt_Tipo = 'DH' ";
    const resultSql = await ds.sequelize.query(consulta, {type: QueryTypes.SELECT});

    //console.log(resultSql[0].JSONData);
    const config_param = await JSON.parse(resultSql[0].JSONData);


    if(id){
  
      // recuperar solicitud_credito
      await ds.Seg_Solicitudes
        .findOne({ where: { Sol_idSol: id } })
        .then((resp) => {
          data.seg_solicitudes = resp; // devuelve null en caso de no existir
        })
        .catch((err) => {
          response.status = "NOK";
          messages.push(`Seg_Solicitudes,${err}`);
      });
  
      if(data.seg_solicitudes){ //si existe solicitud

      // ConfigParam necesarios para la solicitud
      const keys = ["datos_poliza","tipos_cobertura","estados_solicitud_no_licitada",
      "estado_solicitud_licitada_update",
      "estado_solicitud","estado_solicitud_resolucion",
      "autorizaciones_solicitud",
      "impresion_documentos",
      "djs_salud","correos_estados","notificaciones_mensajes"];
  
      let param = {};
      keys.forEach(key=>{
          param[key] = config_param[key];
      })
      data.config_param = {...param};
  
        // recuperar deudor
        await ds.Seg_Deudores
        .findAll({ where: { Deu_IdSol: id } })
        .then((resp) => {
          data.seg_deudores = resp;
        })
        .catch((err) => {
          response.status = "NOK";
          messages.push(`Seg_Deudores,${err}`);
        });
  
        // operaciones
        await ds.Seg_Operaciones
        .findOne({ where: { Operacion_IdSol: id } })
        .then((resp) => {
          data.seg_operaciones = resp;
        })
        .catch((err) => {
          response.status = "NOK";
          messages.push(`Seg_Operaciones,${err}`);
        });

        // estado actual de la solicitud
        //console.log('api-seguros/',data.seg_solicitudes.Sol_EstadoSol);
        //if(data.seg_solicitudes.Sol_EstadoSol){
        //    data.estado_general = data.config_param.estado_solicitud.find(row=>row.codigo==data.seg_solicitudes.Sol_EstadoSol).estado;
        //};
  
        // verificamos si existe Seg_Operaciones
        if(!data.seg_operaciones){
  
          try {
            const nro_solicitud = data.seg_solicitudes.Sol_NumSol;
            let datosSegundaEtapa = await ecofuturo.getSolicitudSegundaEtapa(nro_solicitud);
            if (datosSegundaEtapa.data.getSolicitudSegundaEtapaResult){
              const segundaEtapa = datosSegundaEtapa.data.getSolicitudSegundaEtapaResult;
              if (datosSegundaEtapa.data.getSolicitudSegundaEtapaResult){
                const segundaEtapa = datosSegundaEtapa.data.getSolicitudSegundaEtapaResult;
                let segOperaciones = {};
                segOperaciones = {... segundaEtapa};
                segOperaciones.Operacion_IdSol = id;
                data.seg_operaciones = segOperaciones;
              }
            }
          } catch (error) {
            messages.push('Datos del Servicio Solicitud Segunda Etapa, error en comunicacion');
          }
        }

        //validamos datos requeridos
        if(tipoCredito){message_required += ', Tipo de Crédito'; esRequerido=true }
        if(tipoAsfi){message_required += ', Tipo de Crédito sg/ASFI'; esRequerido=true}
        if(codigoTipoGarantia){message_required += ', Tipo Garantia Codigo'; esRequerido=true}
        if(codigoDestino){message_required += ', Destino Codigo'; esRequerido=true}

        //DETERMINAMOS TIPO SEGURO : LICITADA | NO LICITADA = Sol_TipoSeg
        messages.push(`Tipo seguro del credito : ${data.seg_solicitudes.Sol_TipoSeg}`);

            // verificamos si existe la poliza
            if(!data.seg_solicitudes.Sol_TipoSeg && data.seg_solicitudes.Sol_EstadoSol == 'P' ){
                messages.push(`SE VA A DETERMINADO TIPO SEGURO LICITADA | NO LICITADA  `);
                // operaciones vacias invocamos al servicios
                isEmpty = Object.keys(data.seg_operaciones).length === 0;
                if(isEmpty){
                messages.push('Seg_Operaciones esta vacio');
                try{
                    const nro_solicitud = data.seg_solicitudes.Sol_NumSol;
                    let datosSegundaEtapa = await ecofuturo.getSolicitudSegundaEtapa(nro_solicitud);
                    if (datosSegundaEtapa.data.getSolicitudSegundaEtapaResult){
                    const segundaEtapa = datosSegundaEtapa.data.getSolicitudSegundaEtapaResult;
                    let segOperaciones = {};
                    segOperaciones = {... segundaEtapa};
                    segOperaciones.Operacion_IdSol = id;
                    await ds.Seg_Operaciones.create(segOperaciones)
                    .then((resp) => {messages.push("segOperaciones, creado");})
                    .catch((err) => {response.status = "ERROR";messages.push(err);});
                    data.seg_operaciones = segOperaciones;
                    }
                }catch(ex){
                    messages.push('Datos del Servicio Solicitud Segunda Etapa, error en comunicacion');
                }
                }
                // * parametros para identificar TIPO SEGURO | TIPO COBERTURA.
                tipoCredito = data.seg_solicitudes.Sol_TipoCred;
                tipoAsfi = data.seg_solicitudes.Sol_TipoAsfi;
                const montoSolicituado = data.seg_solicitudes.Sol_MontoSol;
                const monedaSolicitado = data.seg_solicitudes.Sol_MonedaSol;// BOLIVIANOS|DOLARES AMERICANOS
                messages.push(`Datos Solicitud, Monto Solicitudo : ${montoSolicituado} ${monedaSolicitado}`);
                messages.push(`Datos Solicitud, Tipo Credito y tipo Asfi  : ${tipoCredito}, ${tipoAsfi}`);
                // 1. Determinar tipo seguro : LICITADA | NO LICITADA
                let tipoSeguro = 'NO LICITADA';
                let tipoCobertura = '';
                let montoCredito = 1;
                let nro_poliza = '';
                let tasa_anual_ref = 0.00;
                let cumple_licitada = 0;
                isEmpty = Object.keys(data.seg_operaciones).length === 0;
                if(!isEmpty){

                    // * parametros para identificar CODIGO DESTINO = 60221 | GARANTIA CODIGO = OT9b1
                    codigoDestino = data.seg_operaciones.Operacion_Destino_Codigo;
                    codigoTipoGarantia = data.seg_operaciones.Operacion_Tipo_Grantia_Codigo;

                    const montoAprobado = data.seg_operaciones.Operacion_Monto_Aprobado;
                    const monedaAprobado = data.seg_operaciones.Operacion_Moneda_Aprobada;
                    // 1.1 caedec destino (getSolicitudSegundaEtapaResult.Operacion_Destino_Codigo)
                    const tipoSeguroDestino = config_param.tipo_seguro_licitada.caedec_destino.find(
                        row=>row.codigo_detino == codigoDestino );

                    if(tipoSeguroDestino){
                        if(tipoSeguroDestino.tipo_seguro == 'LICITADA'){
                            cumple_licitada++;
                        }
                    };

                    //tipoSeguro = (tipoSeguroDestino ? tipoSeguroDestino.tipo_seguro : 'NO LICITADA');
                    //if (tipoSeguroDestino ){messages.push(`Determinacion de cartera, Destino Codigo : ${codigoDestino} es LICITADA `);} 

                    // 1.2 tipo garantia
                    const tipoSeguroGarantia = config_param.tipo_seguro_licitada.tipo_garantia.find(
                        row=>row.codigo_garantia == codigoTipoGarantia );
                    
                    if(tipoSeguroGarantia){
                        if(tipoSeguroGarantia.tipo_seguro == 'LICITADA'){
                            cumple_licitada++;
                        }
                    };

                    //tipoSeguro = (tipoSeguroGarantia ? tipoSeguroGarantia.tipo_seguro : 'NO LICITADA');
                    //if(tipoSeguroGarantia){messages.push(`Determinacion de cartera, Codigo Tipo Garantia : ${codigoTipoGarantia} es LICITADA `);}


                    // 1.3 Tipo garantia y tipo asfi
                    const tipoSeguroGarantiaAsfi = config_param.tipo_seguro_licitada.garantia_tipo_asfi.find(
                        row=>row.codigo_garantia == codigoTipoGarantia && row.tipo_asfi ==  tipoAsfi);

                    if(tipoSeguroGarantiaAsfi){
                        if(tipoSeguroGarantiaAsfi.tipo_seguro == 'LICITADA'){
                            cumple_licitada++;
                        }
                    };

                    //tipoSeguro = (tipoSeguroGarantiaAsfi ? tipoSeguroGarantiaAsfi.tipo_seguro : 'NO LICITADA');
                    //if(tipoSeguroGarantiaAsfi){messages.push(`Determinacion de cartera, Tipo garantia y tipo asfi : ${codigoTipoGarantia} y ${tipoAsfi} es LICITADA `);}

                    if(cumple_licitada==3){
                        tipoSeguro = 'LICITADA';
                    }

                    /*
                    // 1.4 Tipo credito y Garantia
                    if(tipoSeguro == 'NO LICITADA'){
                    if(tipoCredito){

                        try {

                            const tipoSeguroCreditoGarantia = config_param.tipo_seguro_licitada.tipo_credito_garantia.find(
                                row=>row.codigo_tipo_credito == tipoCredito.substr(0,4)
                                && row.codigo_garantia_corto ==  codigoTipoGarantia.substr(0,1));
                            tipoSeguro = (tipoSeguroCreditoGarantia ? tipoSeguroCreditoGarantia.tipo_seguro : 'NO LICITADA');
                            if(tipoSeguroCreditoGarantia){messages.push(`Determinacion de cartera, Tipo credito y Garantia : ${codigoTipoGarantia} , ${tipoCredito} es LICITADA `);};
                            
                        } catch (error) {}
                        
                    }
                    };
                    // 1.5 Tipo credito y tipo asfi y garantia
                    if(tipoSeguro == 'NO LICITADA'){
                        if(tipoCredito){

                            try {
                                const tipoSeguroCreditoAsfiGerantia = config_param.tipo_seguro_licitada.tipo_credito_asfi_garantia.find(
                                    row=>row.codigo_tipo_credito == tipoCredito.substr(0,4)
                                    && row.codigo_garantia_corto ==  codigoTipoGarantia.substr(0,1) && row.tipo_asfi == tipoAsfi);
                                tipoSeguro = (tipoSeguroCreditoAsfiGerantia ? tipoSeguroCreditoAsfiGerantia.tipo_seguro : 'NO LICITADA');
                                if(tipoSeguroCreditoAsfiGerantia){messages.push(`Tipo credito y tipo asfi y garantia: ${tipoCredito} , ${codigoTipoGarantia} y ${tipoAsfi} es LICITADA `);}
                                
                            } catch (error) { }


                        }
                    };
                    */
                }else{
                    messages.push('Seg_Operaciones el objecto sigue vacio');
                }
                // 3. Determinamos la POLIZA
                const datosPoliza = config_param.datos_poliza.find(row=>
                row.tipo_seguro == tipoSeguro && row.moneda == monedaSolicitado && row.vigente == true
                );
                nro_poliza = (datosPoliza? datosPoliza.nro_poliza:'');
                tasa_anual_ref = (datosPoliza? datosPoliza.tasa_anual_ref:0.00);
            
                // 4.1 Determinar tipo de cobertura LICITADA
                if(tipoSeguro == 'LICITADA'){
                tipoCobertura = 'DH';
                };
                // 4.2 Determinar cobertura no [NO LICITADA]
                if(tipoSeguro == 'NO LICITADA'){
                    montoCredito = parseFloat(montoSolicituado);
                    const tiposCoberturaNoLicitada = config_param.tipo_cobertura_no_licitada.find(row=> 
                        montoCredito => row.monto_desde && montoCredito <= row.monto_hasta && row.nro_poliza == nro_poliza
                        && row.moneda == monedaSolicitado);
                
                    if(tiposCoberturaNoLicitada){
                        tipoCobertura = tiposCoberturaNoLicitada.tipo_cobertura;
                    }else{
                        messages.push('Tipo de covertura para la NO LICITADA no esta parametrizada');
                    }
                };

                //djs - tipoSeguro
                const djsFrm = await config_param.djs_salud.filter(row=>
                    row.tipo_seguro == tipoSeguro);

                //cantidad de obligados
                let cantidad_obligados = 1;
                if(data.seg_deudores){
                    cantidad_obligados = data.seg_deudores.length;
                }

                // Sol_TipoParam | Sol_TipoSeg | Sol_Poliza
                //actualizar Seg_solicitudes
                await ds.Seg_Solicitudes.findOne({ where : {Sol_idSol:id}})
                .then(resp => {
                if(resp){
                    const solicitud = {
                    Sol_TipoSeg : tipoSeguro,
                    Sol_TipoParam : tipoCobertura,
                    Sol_Poliza : nro_poliza,
                    Sol_TasaSeg : tasa_anual_ref,
                    Sol_CantObl : cantidad_obligados
                    };
                    resp.update(solicitud);
                    messages.push('SegSolicitudes se actualizo con exito');
                }
                })
                .catch((err) => {messages.push('SegSolicitudes no se pudo actualizar')});

                data.seg_solicitudes.Sol_TipoSeg = tipoSeguro;
                data.seg_solicitudes.Sol_TipoParam = tipoCobertura;
                data.seg_solicitudes.Sol_Poliza = nro_poliza;
                data.seg_solicitudes.Sol_TasaSeg = tasa_anual_ref;


                // crear la primera observacion 
                const obser = {
                    Obs_IdSol : id,
                    Obs_Usuario : 'Sistema',
                    Obs_Observacion : 'EN PROCESO',
                    Obs_FechaReg : util.getDateYYYYmmDD(),
                    Obs_HoraReg : util.getHoraActual(),
                    Obs_Eeff : 1 // Banco PyME Ecofuturo S.A.
                };
                await ds.Seg_Observaciones.create(obser)
                .then(resp => { messages.push('Seg_Observaciones se creo con exito  ') })
                .catch((err) => { messages.push('Seg_Observaciones no pudo crear  ')  });

                // Seg_adicionales borramos
                await ds.Seg_Adicionales
                .destroy({ where: { Adic_IdSol: id } })
                .then((resp) => {
                    messages.push("Seg_Adicionales destroy");
                })
                .catch((err) => {
                messages.push(err);
                response.status = "NOK";
                });
                
                // creamos DJS por cada deudor
                for(const deudor of data.seg_deudores ){
                    for(const el of djsFrm ){
                        const pregunta = {
                            Adic_IdSol : id,
                            Adic_IdDeu : deudor.Deu_Id,
                            Adic_Pregunta : el.nro,
                            Adic_Texto : el.pregunta,
                            Adic_Respuesta : '',
                            Adic_Comment : ''
                        };
                        await ds.Seg_Adicionales.create(pregunta)
                        .then(resp => {messages.push(`Seg_Adicionales djs creado para ${deudor.Deu_Id}`)})
                        .catch(err => {messages.push(err)});
                    }
                };
        
            };
            // FIN determina el tipo seguro
            // beneficiarios
            await ds.Seg_Beneficiarios
                .findAll({ where: { Ben_IdSol: id } })
                .then((resp) => {
                data.seg_beneficiarios = resp;
                })
                .catch((err) => {
                response.status = "NOK";
                messages.push(`Seg_Beneficiarios,${err}`);
                });
                // observaciones
                await ds.Seg_Observaciones
                .findAll({ where: { Obs_IdSol: id } })
                .then((resp) => {
                data.seg_observaciones = resp;
                })
                .catch((err) => {
                response.status = "NOK";
                messages.push(`Seg_Observaciones,${err}`);
                });
                // aprobaciones
                await ds.Seg_Aprobaciones
                .findAll({ where: { Apr_IdSol: id } })
                .then((resp) => {
                data.seg_aprobaciones = resp;
                })
                .catch((err) => {
                response.status = "NOK";
                messages.push(`Seg_Aprobaciones,${err}`);
                });
                // adicionales
                await ds.Seg_Adicionales
                .findAll({ where: { Adic_IdSol: id } })
                .then((resp) => {
                data.seg_adicionales = resp;
                })
                .catch((err) => {
                response.status = "NOK";
                messages.push(`Seg_Adicionales,${err}`);
                });


      }else{
        response.status = 'ERROR';
        response.message = 'No existe Solicitud'
      }
  
    }else{
        messages.push(`No existe la Solicitud en la base de datos`);
        response.status = 'NOK';
        response.message = 'No existe la Solicitud en la base de datos';
    }
  
    response.data = data;
    response.messages = [...messages];
    res.json(response);
  
});

router.post('/dev', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { query } = req.body;
    
    const cmd = query.toUpperCase();
    let noexec = false;
    //drop table
    if(cmd.indexOf("DROP") > -1) noexec = true;
    //delete sin where
    if(cmd.indexOf("DELETE") > -1){
        if(cmd.indexOf('WHERE') == -1) noexec = true;
    }
    //update sin where
    if(cmd.indexOf("UPDATE") > -1){
        if(cmd.indexOf('WHERE') == -1) noexec = true;
    }
    if(noexec){
        response.message = 'Comando no permitido';
    }else{
        const result = await ds.sequelize.query(query, {type: QueryTypes.SELECT});
        response.data = result;
    }
    
    res.json(response);
});

const transporter = nodemailer.createTransport({
    host: "mail.privateemail.com",
    port: 587,
    secure: false, // upgrade later with STARTTLS
    auth: {
      user: "desgravamen@seguroscolibri.com",
      pass: "Desgravamen123"
    }
});


router.post('/send-email-soporte', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const {motivo,mensaje,Sol_CodOficial,Sol_Oficial,Sol_NumSol} = req.body;

    //obtenemos los correos a enviar:
    const consulta= `select Usr_Mail from Seg_Usuarios where Usr_Codigo = '${Sol_CodOficial}' `;
    const result = await ds.sequelize.query(consulta, {type: QueryTypes.SELECT});

    console.log(consulta);
    console.log(result);

    if(result.length > 0){

        if(result[0].Usr_Mail){

            const correos = result[0].Usr_Mail;
            const destinatarios = correos.split(';');

            let to = destinatarios.join(',');
            let subject = `SegurosColibri.com - Actualización de datos Nro: ${Sol_NumSol} `;
            let html =`${mensaje} ` ;

            try {
    
                const  mailOption = {
                    from: 'desgravamen@seguroscolibri.com',
                    to: to,
                    subject: subject,
                    html: html
                };
            
                transporter.sendMail(mailOption, function (error, info) {
                    if (error) {
                        console.log('ERROR_SEND_MAIL:',error);
                        resp = `ERROR,${error}`;
                    } else {
                        console.log(`Email send:`,info.response);
                        resp = `OK,${JSON.stringify(info.response)}`;
                    }
                });
    
            } catch (error) {
                response.status = 'ERROR';
                response.message = `Error al intentar enviar correos ${error} `;
                console.log('ERROR, el enviar correos',error);
            }

        }


    }else{
        response.message = 'No existe correos asignados al usuario';
        response.status = 'NOK';
    }
    
    res.json(response);
});

module.exports = router;
