const { Router } = require('express');
const router = new Router();
const ds = require('../models');

router.get('/',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await ds.Seg_Aprobaciones.findAll({limit:10})
        .then(tomadors => {
            response.data = tomadors;
        }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;        
        });
    res.json(response);
});

router.get('/:idsol', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const Apr_IdSol = req.params.idsol;
    await ds.Seg_Aprobaciones.findAll({ where : {Apr_IdSol}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "NOK";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { Apr_IdSol,Apr_IdDeu } = req.body;
    const param = await ds.Seg_Aprobaciones.findOne({where:{Apr_IdSol,Apr_IdDeu}});
    if(param){
        response.status = 'ERROR',
        response.message = 'Registro existe'
    }else{
        await ds.Seg_Aprobaciones.create(req.body)
        .then(resp => {
            response.data = resp;
            response.message = 'Registro creado';
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    }
    res.json(response);
});

router.put('/:idsol/:iddeu', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const Apr_IdSol = req.params.idsol;
    const Apr_IdDeu = req.params.iddeu;
    await ds.Seg_Aprobaciones.findOne({ where : {Apr_IdSol,Apr_IdDeu}})
        .then(resp => {
            if(resp){
                resp.update(req.body);
                response.message = 'Registro actualizado';
            }else{
                response.status = 'ERROR';
                response.message = 'Registro no existe';
            }
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.delete('/:idsol/:iddeu', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const Apr_IdSol = req.params.idsol;
    const Apr_IdDeu = req.params.iddeu;
    await ds.Seg_Aprobaciones.findOne({ where : {Apr_IdSol,Apr_IdDeu}})
    .then(resp => {
        if(resp){
            resp.destroy();
            response.message = "Registro eliminado"
        }else{
            response.status = 'ERROR';
            response.message = "Registro no exite"
        }
    })
    .catch(err=>{
        response.status = "ERROR",
        response.message = err
    })
    res.json(response);
});

module.exports = router;
