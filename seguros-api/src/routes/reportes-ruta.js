const { Router } = require('express');
const router = new Router();
const ds = require('../models');

router.get('/',async (req, res) => {
    let response = { status: 'OK', message: 'reportes', data: '' };
    res.json(response);
});


router.post('/', async(req, res) => {
    let response = { status: 'OK', message: 'Reporte generado', data: {} };
    const { id_solicitud, plantilla , id_deudor } = req.body;

    res.json(response);
});

module.exports = router;
