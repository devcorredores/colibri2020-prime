const { Router } = require('express');
const router = new Router();
const parametros_campo = require("../config/parametros-campos.json");
const config_param = require('../config/config-param.json');
const fs = require('fs');
let path = require("path");
const dir_config = path.join(__dirname,`../config/`);

//devuelve un objeto json
getParametrosCampos = () =>{
    try{
        const param = fs.readFileSync(`${dir_config}parametros-campos.json`,'utf8');
        return JSON.parse(param);
    }catch(err){
        console.error(err);
        return [];
    }
}

updateParametrosCampos = (objJson) =>{
    try{
        fs.writeFileSync(`${dir_config}parametros-campos.json`,JSON.stringify(objJson));
    }catch(err){
        response.status = 'ERROR';
        response.message = err;
    }
}

router.get('/',async (req, res) => {
    let response = { status: 'OK', message: 'parametros-campos', data: '' };
    response.data = getParametrosCampos();
    res.json(response);
});

router.get('/campos-config-param',async (req, res) => {
    let response = { status: 'OK', message: 'config-param', data: '' };
    let param = {};
    param.parametros_campo = getParametrosCampos();
    param.config_param = await config_param;
    response.data = param;
    res.json(response);
});


router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    try{

    }catch(err){
        response.status = 'ERROR';
        response.message = err;
    }
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { grupo,campo } = req.body;
    let parametros = getParametrosCampos();
    const result = parametros.find(itm =>itm.grupo === grupo && itm.campo === campo);
    if(!result){
        parametros.push(req.body);
        updateParametrosCampos(parametros);
    }
    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    let parametros = getParametrosCampos();
    const index = parametros.findIndex(itm => itm.id == id);
    parametros[index] = req.body;
    updateParametrosCampos(parametros);
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    let parametros = getParametrosCampos();
    await parametros.splice(index,1);
    res.json(response);
});

module.exports = router;
