const { Router } = require('express');
const router = new Router();
const ds = require('../models');
const config_param = require('../config/config-param.json');

// seg-adicionales/
router.get('/',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    await ds.Seg_Adicionales.findAll({limit:10})
        .then(tomadors => {
            response.data = tomadors;
        }
        ).catch((err) => {
            response.status = 'ERROR';
            response.message = err;        
        });
    res.json(response);
});

router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    
    await ds.Seg_Adicionales.findOne({ where : {id}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });

    res.json(response);
});

// Adicionar DJS a la Solicitud por el numero de poliza
router.get('/add-djs-idsol/:id', async(req, res) => {
    let response = { status: 'OK', message: 'add-djs-idsol', data: {},messages:[] };

    const id = req.params.id;
    let messages = [];

    messages.push(`Adicionar id solicitud : ${id}`);

    let data = {seg_deudores:[]};

    try {

        // recuperar deudor
        await ds.Seg_Deudores
        .findAll({ where: { Deu_IdSol: id } })
        .then((resp) => {
            data.seg_deudores = resp;
            messages.push("Seg_Deudores Recuparado");
        })
        .catch((err) => {
            response.status = "ERROR";
            response.message = err;
            messages.push(`Error en seg_deudores recuperando`);
        });

        //numero de poliza
        const nro_poliza = data.seg_deudores[0].Deu_Poliza;

        messages.push(`numero de poliza  : ${nro_poliza} `);

        //config-paran-djs
        const djsFrm = await config_param.djs_salud.filter(row=>
            row.nro_poliza.split(',').includes(nro_poliza)  );

        messages.push(`config-param, djs : ${djsFrm.length } `);

        // Seg_adicionales borramos
        await ds.Seg_Adicionales
        .destroy({ where: { Adic_IdSol: id } })
        .then((resp) => {
            messages.push("Seg_Adicionales destroy");
        })
        .catch((err) => {
            response.status = "NOK";
            response.message = err;
            messages.push("error Seg_Adicionales destroy");
        });

        // creamos DJS por cada deudor
        for (const deudor of data.seg_deudores){
            for (const el of djsFrm){
                const pregunta = {
                    Adic_IdSol : id,
                    Adic_IdDeu : deudor.Deu_Id,
                    Adic_Pregunta : el.nro,
                    Adic_Texto : el.pregunta,
                    Adic_Respuesta : '',
                    Adic_Comment : ''
                };
                await ds.Seg_Adicionales.create(pregunta)
                    .then(resp => {messages.push(`Seg_Adicionales djs creado id deudor: ${deudor.Deu_Id}`)})
                    .catch(err => {messages.push(err)});
            }
        };
        
    } catch (error) {
        response.status = 'ERROR';
        response.message = error;
    }

    response.messages = messages;

    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { razon_social } = req.body;
    const param = await ds.Seg_Adicionales.findOne({where:{razon_social}});
    if(param){
        response.status = 'ERROR',
        response.message = 'Registro existe'
    }else{
        await ds.tomador.create(req.body)
        .then(resp => {
            response.data = resp;
            response.message = 'Registro creado';
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    }
    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Adicionales.findOne({ where : {id}})
        .then(resp => {
            if(resp){
                resp.update(req.body);
                response.message = 'Registro actualizado';
            }else{
                response.status = 'ERROR';
                response.message = 'Registro no existe';
            }
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Adicionales.findOne({ where : {id}})
    .then(resp => {
        if(resp){
            resp.destroy();
            response.message = "Registro eliminado"
        }else{
            response.status = 'ERROR';
            response.message = "Registro no exite"
        }
    })
    .catch(err=>{
        response.status = "ERROR",
        response.message = err
    })
    res.json(response);
});

//metodo update de los servicios para modificar seg-adicionales
router.put("/seg-adicionales/:id", async (req, res) => {
    let response = { status: "OK", messages: [], data: {},message:'' };
    let messages = [];
  
    const { seg_adicionales } = req.body;

    for(const row of seg_adicionales){

        await ds.Seg_Adicionales
        .update(row, { where: { Adic_Id: row.Adic_Id  } })
        .then((resp) => {
          messages.push("Seg_Adicionales update");
        })
        .catch((err) => {
          messages.push("error Seg_Adiconales");
          response.status = "NOK";
        });

    };
  
    response.messages = messages;
    res.json(response);
  });




module.exports = router;
