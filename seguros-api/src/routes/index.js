const { Router } = require('express');
const router = new Router();

router.get('/',async (req, res) => {
    let response = { status: 'OK', message: 'api-seguros', data: '' };
    res.json(response);
});

router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: 'api-seguros', data: {} };
    const id = req.params.id;
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: 'api-seguros', data: '' };
    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: 'api-seguros', data: '' };
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: 'api-seguros', data: '' };
    res.json(response);
});


module.exports = router;
