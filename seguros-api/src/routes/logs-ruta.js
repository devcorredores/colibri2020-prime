const { Router } = require('express');
const router = new Router();
const util = require('../module/util');
const { join } = require('path');
//const lowdb = require("lowdb");
//const {  } = require('../config/config-db-log');

router.get('/',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    //const logs = await getLog().get('logs').value();
    //response.data = logs;
    res.json(response);
});

router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    //const { process_name,date,message } = req.body;
    //getLog().get('logs').push(req.body).write();
    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    res.json(response);
});


module.exports = router;
