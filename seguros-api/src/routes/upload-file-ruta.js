const { Router } = require('express');
const router = new Router();
const ds = require('../models');
const formidable = require('formidable');
const nodemailer = require('nodemailer');
const multer = require('multer')
const fs = require('fs');
const path = require('path');

router.get('/',async (req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    res.json(response);
});

router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    res.json(response);
});

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const transporter = nodemailer.createTransport({
    host: "mail.privateemail.com",
    port: 587,
    secure: false, // upgrade later with STARTTLS
    auth: {
      user: "desgravamen@seguroscolibri.com",
      pass: "Desgravamen123"
    }
});

router.post('/upload-send-email/:param',upload.single('file'),(req, res) => {
    let response = { status: 'OK', message: 'upload-send-email', data: '' };
    const param = req.params.param;

    console.log('===> upload-send-email,request : ',req.body.subject);
    const { subject, message , to , cc } = req.body;

    try {

        const  mailOption = {
            from: 'desgravamen@seguroscolibri.com',
            to: to,
            cc: cc,
            subject: subject,
            html: message,
            attachments: [
                {
                    filename: req.file.originalname,
                    content: req.file.buffer,
                    cid: req.file.originalname
                }
            ]
        };
    
        transporter.sendMail(mailOption, function (error, info) {
            if (error) {
                console.log('===> upload-send-email,transporter.sendMail :'.error);
                response.status = 'ERROR';
                response.message = error    
            } else {
                console.log(`===> upload-send-email: ${info.response}`);
                response.status = 'OK';
                response.message = info.response;
            }
        });

        
    } catch (error) {
        console.log('===> upload-send-email:'.error);
        response.status = 'ERROR';
        response.message = `Error al intentar enviar correos adjuntos ${error} `;
    }

    res.json(response);

});


router.post('/',(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { contenido } = req.body;

    try{
        const form = formidable.IncomingForm();
        const uploadFolder = path.join(__dirname,'../public','files');
    
        form.multiples = true;
        form.maxFileSize = 1*1024*1024; //1MB
        form.uploadDir = uploadFolder;
    
        form.parse(req,(error, fields, file) =>{
            //console.log('UPLOAD req =>',req);
            if(error){
                response.status = "ERROR";
                response.message = err;
            }
        });
    
        form.on('uncaughtException', (err) => {
            if(err){
                response.status = "ERROR";
                response.message = err;
            }
        })

        form.on('fileBegin',(name,file)=>{
            file.path = `${uploadFolder}/${file.name}`;
        });
    
        form.on('file',(name,file)=>{
            console.log(`Uploaded ${file.name}`);
        });


    }catch(ex){
        console.log('Error subiendo archivo');
    }
    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    res.json(response);
});

module.exports = router;
