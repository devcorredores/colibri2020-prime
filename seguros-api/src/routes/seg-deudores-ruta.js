const { Router } = require('express');
const router = new Router();
const ds = require('../models');

// router.get('/',async (req, res) => {
//     let response = { status: 'OK', message: '', data: '' };
//     await ds.Seg_Deudores.findAll({limit:100})
//         .then(resul => {
//             response.data = resul;
//         }
//         ).catch((err) => {
//             response.status = 'ERROR';
//             response.message = err;
//         });
//     res.json(response);
// });

router.get('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: {} };
    const id = req.params.id;
    await ds.Seg_Deudores.findOne({ where : {Deu_Id:id}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

// http://localhost:3003/api-seguros/seg-deudores/idsol/89946
router.get('/idsol/:nro', async(req, res) => {
    let response = { status: 'OK', message: 'solicitud', data: {} };
    const nro = req.params.nro;
    await ds.Seg_Deudores.findAll({ where: {Deu_IdSol:nro}})
        .then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "NOK";
                response.message = "Registro no encontrado"
            }
        }
        ).catch(err => {
            response.status = 'NOK';
            response.message = err;
        });
    res.json(response);
});

router.post('/', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const { Deu_IdSol } = req.body;
    const param = await ds.Seg_Deudores.findOne({where:{Deu_IdSol}});
    if(param){
        response.status = 'ERROR',
        response.message = 'Registro existe'
    }else{
        await ds.Seg_Deudores.create(req.body)
        .then(resp => {
            response.data = resp;
            response.message = 'Registro creado';
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    }
    res.json(response);
});

router.put('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Deudores.findOne({ where : {Deu_Id:id}})
        .then(resp => {
            if(resp){
                resp.update(req.body);
                response.message = 'Registro actualizado';
            }else{
                response.status = 'ERROR';
                response.message = 'Registro no existe';
            }
        })
        .catch((err) => {
            response.status = 'ERROR';
            response.message = err;
        });
    res.json(response);
});

router.put('/update-deudores/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '',messages : [] };
    let messages = [];
    const segDeudores = req.body;

    segDeudores.forEach( async(deudor) =>{
        await ds.Seg_Deudores.findOne({where: { Deu_Id : deudor.Deu_Id } })
        .then(resp=>{
            response.data = resp;
            resp.update(deudor);
        })
        .catch(ex=>{response.status='ERROR';messages.push(ex)});
    });
    response.messages = messages;

    res.json(response);
});

router.delete('/:id', async(req, res) => {
    let response = { status: 'OK', message: '', data: '' };
    const id = req.params.id;
    await ds.Seg_Deudores.findOne({ where : {Deu_Id:id}})
    .then(resp => {
        if(resp){
            resp.destroy();
            response.message = "Registro eliminado"
        }else{
            response.status = 'ERROR';
            response.message = "Registro no exite"
        }
    })
    .catch(err=>{
        response.status = "ERROR",
        response.message = err
    })
    res.json(response);
});


router.post('/update-deudor/:id', async(req, res) => {
    try {
        let { id } = req.params;
        let { Seg_Deudor } = req.body;
        let { Seg_Observacion } = req.body;
        let response = { status: 'OK', message: '', data: {},messages : [] };
        let messages = [];
        await ds.Seg_Deudores.update(Seg_Deudor, {where:{ Deu_Id : id }});
        await ds.Seg_Observaciones.create(Seg_Observacion);
        response.data = await ds.Seg_Deudores.findOne({
            where : {Deu_Id:id},
            include: {
                model: ds.Seg_Solicitudes,
                as: 'deu_solicitud',
                include: {
                    model: ds.Seg_Observaciones,
                    as: 'sol_observaciones',
                }
            }
        });
        response.message = 'Registros Actualizados';
        response.status = 'ok';
        res.json(response);
    } catch (e) {
        console.log(e);
    }
});

router.get('/find-deudor/:id', async(req, res) => {
    try {
        let response = { status: 'OK', message: '', data: {} };
        const id = req.params.id;
        await ds.Seg_Deudores.findOne({
            where : {Deu_Id:id},
            include: {
                model: ds.Seg_Solicitudes,
                as: 'deu_solicitud',
                include: {
                    model: ds.Seg_Observaciones,
                    as: 'sol_observaciones',
                }
            }
        }).then(resp => {
            if(resp){
                response.data = resp;
            }else{
                response.status = "ERROR";
                response.message = "Registro no encontrado"
            }
        }).catch(err => {
            response.status = 'ERROR';
            response.message = err;
        });
        res.json(response);
    } catch (e) {
        console.log(e)
    }
});

module.exports = router;
