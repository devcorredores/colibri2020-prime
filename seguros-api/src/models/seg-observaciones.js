'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seg_Observaciones extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Seg_Observaciones.init({
    Obs_Id:{
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
      },
    Obs_IdSol: DataTypes.INTEGER,
    Obs_Observacion: DataTypes.STRING,
    Obs_Usuario: DataTypes.STRING,
    Obs_Eeff: DataTypes.STRING,
    Obs_Aseg: DataTypes.STRING,
    Obs_FechaReg: DataTypes.DATEONLY,
    Obs_HoraReg: DataTypes.STRING,
    Obs_Cerrado: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Seg_Observaciones',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Seg_Observaciones;
};