'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seg_Solicitudes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      
      Seg_Solicitudes.hasMany(models.Seg_Deudores,{
        foreignKey: 'Deu_IdSol',
        sourceKey: 'Sol_IdSol'
      })
      
    }
  };
  Seg_Solicitudes.init({
    Sol_IdSol: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    Sol_Eeff: DataTypes.STRING,
    Sol_NumSol: DataTypes.STRING,
    Sol_FechaSol: DataTypes.DATEONLY,
    Sol_TipoCred: DataTypes.STRING,
    Sol_TipoAsfi: DataTypes.STRING,
    Sol_TipoCli: DataTypes.STRING,
    Sol_Sucursal: DataTypes.STRING,
    Sol_Agencia: DataTypes.STRING,
    Sol_Ciudad: DataTypes.STRING,
    Sol_Oficial: DataTypes.STRING,
    Sol_Digitador: DataTypes.STRING,
    Sol_MontoSol: DataTypes.DECIMAL(18,4),
    Sol_MonedaSol: DataTypes.STRING,
    Sol_PlazoSol: DataTypes.STRING,
    Sol_FrecPagoSol: DataTypes.STRING,
    Sol_CantObl: DataTypes.STRING,
    Sol_TipoOpeUni: DataTypes.STRING,
    Sol_TipoOpeAdic: DataTypes.STRING,
    Sol_TipoOpeLC: DataTypes.STRING,
    Sol_NumLC: DataTypes.STRING,
    Sol_MonedaLC: DataTypes.STRING,
    Sol_FechaLC: DataTypes.STRING,
    Sol_MontoActAcum: DataTypes.STRING,
    Sol_TipRelASFI: DataTypes.STRING,
    Sol_TipoSeg: DataTypes.STRING,
    Sol_Aseg: DataTypes.STRING,
    Sol_PrimaSeg: DataTypes.STRING,
    Sol_SDH: DataTypes.STRING,
    Sol_TasaSeg: DataTypes.STRING,
    Sol_EstadoSol: DataTypes.STRING,
    Sol_Poliza: DataTypes.STRING,
    Sol_ExtraPrima: DataTypes.DECIMAL(18,4),
    Sol_MontoSolSus: DataTypes.STRING,
    Ope_TipoGtia: DataTypes.STRING,
    Ope_Destino: DataTypes.STRING,
    Ope_TasaSeg: DataTypes.STRING,
    Ope_ImporteSeg: DataTypes.STRING,
    Sol_CodOficial: DataTypes.STRING,
    Sol_LC: DataTypes.STRING,
    Sol_TipoParam: DataTypes.STRING,
    operacion_estado: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Seg_Solicitudes',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Seg_Solicitudes;
};