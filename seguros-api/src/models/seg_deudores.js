'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seg_Deudores extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      
      Seg_Deudores.belongsTo(models.Seg_Solicitudes,{
        foreignKey:'Deu_IdSol',
        targetKey:'Sol_IdSol'
      });
      
    }
  };
  Seg_Deudores.init({
    Deu_Id:{
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true
    },
    Deu_IdSol: DataTypes.INTEGER,
    Deu_NIvel: DataTypes.INTEGER,
    Deu_Nombre: DataTypes.STRING,
    Deu_Paterno: DataTypes.STRING,
    Deu_Materno: DataTypes.STRING,
    Deu_Casada: DataTypes.STRING,
    Deu_DirDom: DataTypes.STRING,
    Deu_DirOfi: DataTypes.STRING,
    Deu_TelDom: DataTypes.STRING,
    Deu_TelOfi: DataTypes.STRING,
    Deu_TelCel: DataTypes.STRING,
    Deu_EstCiv: DataTypes.STRING,
    Deu_Sexo: DataTypes.STRING,
    Deu_Actividad: DataTypes.STRING,
    Deu_DetACtiv: DataTypes.STRING,
    Deu_FecNac: DataTypes.STRING,
    Deu_PaisNac: DataTypes.STRING,
    Deu_CiudadNac: DataTypes.STRING,
    Deu_TipoDoc: DataTypes.STRING,
    Deu_NumDoc: DataTypes.STRING,
    Deu_ExtDoc: DataTypes.STRING,
    Deu_CompDoc: DataTypes.STRING,
    Deu_CodCli: DataTypes.STRING,
    Deu_FecRegCli: DataTypes.STRING,
    Deu_Mano: DataTypes.STRING,
    Deu_Peso: DataTypes.STRING,
    Deu_Talla: DataTypes.STRING,
    Deu_MontoActAcum: DataTypes.STRING,
    Deu_Incluido: DataTypes.STRING,
    Deu_MontoActAcumVerif: DataTypes.STRING,
    Deu_Verificado: DataTypes.STRING,
    Deu_Edad: DataTypes.STRING,
    Deu_FechaResolucion: DataTypes.DATEONLY,
    Hist_TipoSeg: DataTypes.STRING,
    Deu_cobertura: DataTypes.STRING,
    Deu_Poliza:DataTypes.STRING,
    Deu_TasaSeg:DataTypes.DECIMAL,
    Deu_PrimaSeg:DataTypes.DECIMAL,
    id_documento_version:DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Seg_Deudores',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Seg_Deudores;
};