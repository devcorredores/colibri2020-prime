'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seg_Aprobaciones extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // define association here
    }
  };
  Seg_Aprobaciones.init({
    Apr_IdSol:{
        primaryKey: true,
        type: DataTypes.INTEGER
      },
    Apr_IdDeu:{
        primaryKey: true,
        type: DataTypes.INTEGER
      },
    Apr_Aprobado:DataTypes.STRING,
    Apr_ExtraPrima:DataTypes.FLOAT,
    Apr_Condiciones: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Seg_Aprobaciones',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Seg_Aprobaciones;
};