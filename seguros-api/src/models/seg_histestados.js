'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seg_HistEstados extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Seg_HistEstados.init({
    Est_Id:{
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true
    },
    Est_IdSol: DataTypes.INTEGER,
    Est_Usuario: DataTypes.DATEONLY,
    Est_FechaReg: DataTypes.STRING,
    Est_HoraReg: DataTypes.STRING,
    Est_Estado: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Seg_HistEstados',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Seg_HistEstados;
};