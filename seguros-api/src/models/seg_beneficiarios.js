'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seg_Beneficiarios extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Seg_Beneficiarios.init({
    Ben_Id:{
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true
    },
    Ben_IdSol: DataTypes.INTEGER,
    Ben_IdDeu: DataTypes.INTEGER,
    Ben_Nombre: DataTypes.STRING,
    Ben_Paterno: DataTypes.STRING,
    Ben_Materno: DataTypes.STRING,
    Ben_Casada: DataTypes.STRING,
    Ben_Telefono: DataTypes.STRING,
    Ben_TipoDoc: DataTypes.STRING,
    Ben_NumDoc: DataTypes.STRING,
    Ben_ExtDoc: DataTypes.STRING,
    Ben_CompDoc: DataTypes.STRING,
    Ben_Relacion: DataTypes.STRING,
    Ben_Porcentaje: DataTypes.STRING,
    Ben_Sexo: DataTypes.STRING,
    Ben_OtraRel: DataTypes.STRING,
    Ben_Tipo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Seg_Beneficiarios',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Seg_Beneficiarios;
};