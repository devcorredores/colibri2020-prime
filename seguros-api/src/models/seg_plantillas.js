'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seg_Plantillas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Seg_Plantillas.init({
    id:{
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true,
    },
    nombre: DataTypes.STRING,
    fecha: DataTypes.DATEONLY,
    poliza: DataTypes.STRING,
    plantilla: DataTypes.TEXT,
    version: DataTypes.STRING,
    estado: DataTypes.STRING,
    grupo: DataTypes.STRING,
    tipo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Seg_Plantillas',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Seg_Plantillas;
};