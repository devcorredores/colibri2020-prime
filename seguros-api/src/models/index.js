'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];

const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});


// Relations

db.Seg_Deudores.belongsTo(db.Seg_Solicitudes, {foreignKey: "Deu_IdSol", targetKey: "Sol_IdSol", as: "sol_deudor"});
db.Seg_Solicitudes.hasMany(db.Seg_Deudores, {foreignKey: "Deu_IdSol", sourceKey: "Sol_IdSol", as: "sol_deudor"});

db.Seg_Deudores.belongsTo(db.Seg_Solicitudes, {foreignKey: "Deu_IdSol", targetKey: "Sol_IdSol", as: "deu_solicitud"});
db.Seg_Solicitudes.hasMany(db.Seg_Deudores, {foreignKey: "Deu_IdSol", sourceKey: "Sol_IdSol", as: "deu_solicitud"});

db.Seg_Observaciones.belongsTo(db.Seg_Solicitudes, {foreignKey: "Obs_IdSol", targetKey: "Sol_IdSol", as: "sol_observaciones"});
db.Seg_Solicitudes.hasMany(db.Seg_Observaciones, {foreignKey: "Obs_IdSol", targetKey: "Sol_IdSol", as: "sol_observaciones"});

db.Seg_Adicionales.belongsTo(db.Seg_Solicitudes, {foreignKey: "Adic_IdSol", targetKey: "Sol_IdSol", as: "sol_adicional"});
db.Seg_Solicitudes.hasMany(db.Seg_Adicionales, {foreignKey: "Adic_IdSol", sourceKey: "Sol_IdSol", as: "sol_adicional"});

db.Seg_Adicionales.belongsTo(db.Seg_Deudores, {foreignKey: "Adic_IdDeu", targetKey: "Deu_Id", as: "deu_adicional"});
db.Seg_Deudores.hasMany(db.Seg_Adicionales, {foreignKey: "Adic_IdDeu", sourceKey: "Deu_Id", as: "deu_adicional"});

db.Seg_Beneficiarios.belongsTo(db.Seg_Deudores, {foreignKey: "Ben_IdDeu", targetKey: "Deu_Id", as: "deu_beneficiarios"});
db.Seg_Deudores.hasMany(db.Seg_Beneficiarios, {foreignKey: "Ben_IdDeu", sourceKey: "Deu_Id", as: "deu_beneficiarios"});

db.Seg_Aprobaciones.belongsTo(db.Seg_Solicitudes, {foreignKey: "Apr_IdSol", targetKey: "Sol_IdSol", as: "sol_aprobacion"});
db.Seg_Solicitudes.hasMany(db.Seg_Aprobaciones, {foreignKey: "Apr_IdSol", sourceKey: "Sol_IdSol", as: "sol_aprobacion"});

db.Seg_Aprobaciones.belongsTo(db.Seg_Deudores, {foreignKey: "Apr_IdDeu", targetKey: "Deu_Id", as: "deu_aprobacion"});
db.Seg_Deudores.hasMany(db.Seg_Aprobaciones, {foreignKey: "Apr_IdDeu", sourceKey: "Deu_Id", as: "deu_aprobacion"});


db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
