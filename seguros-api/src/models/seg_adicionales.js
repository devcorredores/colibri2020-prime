'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seg_Adicionales extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  //Adic_Id	Adic_IdSol	Adic_IdDeu	Adic_Pregunta	Adic_Texto	Adic_Opciones	Adic_Respuesta	Adic_Comment
  Seg_Adicionales.init({
    Adic_Id:{
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true,
    },
    Adic_IdSol: DataTypes.INTEGER,
    Adic_IdDeu: DataTypes.INTEGER,
    Adic_Pregunta: DataTypes.INTEGER,
    Adic_Texto: DataTypes.STRING,
    Adic_Opciones: DataTypes.STRING,
    Adic_Respuesta: DataTypes.STRING,
    Adic_Comment: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Seg_Adicionales',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Seg_Adicionales;
};