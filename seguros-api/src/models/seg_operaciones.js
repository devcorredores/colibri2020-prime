'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seg_Operaciones extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Seg_Operaciones.init({
    Operacion_Id:{
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true
    },
    Operacion_IdSol: DataTypes.INTEGER,
    Operacion_Numero_Aprobado: DataTypes.STRING,
    Operacion_Fecha_Aprobacion: DataTypes.STRING,
    Operacion_Monto_Aprobado: DataTypes.STRING,
    Operacion_Moneda_Aprobada: DataTypes.STRING,
    Operacion_Plazo_Aprobado: DataTypes.STRING,
    Operacion_Frecuencia_Aprobada: DataTypes.STRING,
    Operacion_Plazo_Aprobado: DataTypes.INTEGER,
    Operacion_Frecuencia_Aprobada: DataTypes.STRING,
    Operacion_Actual_Acumulado: DataTypes.STRING,
    Operacion_Tipo_Garantia: DataTypes.STRING,
    Operacion_Tipo_Grantia_Codigo:DataTypes.STRING,
    Operacion_Valor_Garantia: DataTypes.STRING,
    Operacion_Destino_Prestamo: DataTypes.STRING,
    Operacion_Destino_Codigo: DataTypes.STRING,
    Operacion_Fecha_Reprogramacion: DataTypes.STRING,
    Operacion_Fecha_Vencimiento: DataTypes.STRING,
    Operacion_Fecha_Desembolso: DataTypes.STRING,
    Operacion_Monto_Desembolsado: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Seg_Operaciones',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  });
  return Seg_Operaciones;
};