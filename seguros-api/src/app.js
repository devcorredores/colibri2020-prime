const express = require('express');
const app = require("express")();
const http = require("http").Server(app);
const https = require('https'); 
const path = require('path');
const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');
const compression = require('compression');
const util = require('./module/util');
const env = require("./config/env");

const fs = require('fs');

const https_options = {
	key: fs.readFileSync(path.join(__dirname, 'sslcert/seguroscolibri.key'), 'utf8'),
	cert: fs.readFileSync(path.join(__dirname, 'sslcert/seguroscolibri.crt'), 'utf8'),
	// ca: [
	//   fs.readFileSync(path.join(__dirname, 'sslcert/ce_publickey.crt'), 'utf8'),
	//   fs.readFileSync(path.join(__dirname, 'sslcert/ce_bunble.crt'), 'utf8')
	// ]
};

app.set('port', env.PORT || 4005);
app.set('ports', env.PORTSSL || 4004);

// middlewares
app.use(express.urlencoded({ extended: false }));
app.use(morgan('combined'));
app.use(cors());
app.use(helmet());
//app.use(helmet.frameguard({ action: 'deny' }));
app.use(compression());
app.use(express.json());
//createLog();

app.use('/api-seguros/reportes', require('./routes/reportes-ruta'));
app.use('/api-seguros/config-param',require('./routes/config-param-ruta'));
app.use('/api-seguros/upload-file',require('./routes/upload-file-ruta'));
app.use('/api-seguros/ecofuturo', require('./routes/ecofuturo-ruta'));
app.use('/api-seguros/send-mail',require('./routes/sendmail-ruta'));
app.use('/api-seguros/parametros-campo', require('./routes/parametros_campo-ruta'));

app.use('/api-seguros/seg-solicitudes',require('./routes/seg-solicitudes-ruta'));
app.use('/api-seguros/seg-deudores',require('./routes/seg-deudores-ruta'));
app.use('/api-seguros/seg-beneficiarios', require('./routes/seg_beneficiarios-ruta'));
app.use('/api-seguros/seg-adicionales', require('./routes/seg_adicionales-ruta'));
app.use('/api-seguros/seg-aprobaciones',require('./routes/seg_aprobaciones-ruta'));
app.use('/api-seguros/seg-observaciones',require('./routes/seg_observaciones-ruta'));
app.use('/api-seguros/seg-operaciones', require('./routes/seg_operaciones-ruta'));

app.use('/api-seguros/seg-plantillas',require('./routes/seg_plantillas-ruta'));

// static files
app.use(express.static(path.join(__dirname, 'public')));

// starting server http
http.listen(app.get('port'),()=>{
   console.log(` ${new Date()} : Server http escuchando en el puerto ${app.get('port')}`);
});

// starting server https
const httpsServer = https.createServer(https_options,app);
httpsServer.listen(app.get('ports'), () => {
  console.log(` ${new Date()} :  Server https escuchando en el puerto ${app.get('ports')}`);
});


