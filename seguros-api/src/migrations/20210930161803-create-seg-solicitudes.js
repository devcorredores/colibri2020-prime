'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Seg_Solicitudes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Sol_IdSol: {
        type: Sequelize.INTEGER
      },
      Sol_Eeff: {
        type: Sequelize.STRING
      },
      Sol_NumSol: {
        type: Sequelize.STRING
      },
      Sol_FechaSol: {
        type: Sequelize.DATE
      },
      Sol_TipoCred: {
        type: Sequelize.STRING
      },
      Sol_TipoAsfi: {
        type: Sequelize.STRING
      },
      Sol_TipoCli: {
        type: Sequelize.STRING
      },
      Sol_Sucursal: {
        type: Sequelize.STRING
      },
      Sol_Agencia: {
        type: Sequelize.STRING
      },
      Sol_Ciudad: {
        type: Sequelize.STRING
      },
      Sol_Oficial: {
        type: Sequelize.STRING
      },
      Sol_Digitador: {
        type: Sequelize.STRING
      },
      Sol_MontoSol: {
        type: Sequelize.FLOAT
      },
      Sol_MonedaSol: {
        type: Sequelize.STRING
      },
      Sol_PlazoSol: {
        type: Sequelize.INTEGER
      },
      Sol_FrecPagoSol: {
        type: Sequelize.STRING
      },
      Sol_CantObl: {
        type: Sequelize.INTEGER
      },
      Sol_TipoOpeUni: {
        type: Sequelize.STRING
      },
      Sol_TipoOpeAdic: {
        type: Sequelize.STRING
      },
      Sol_TipoOpeLC: {
        type: Sequelize.STRING
      },
      Sol_NumLC: {
        type: Sequelize.INTEGER
      },
      Sol_MonedaLC: {
        type: Sequelize.STRING
      },
      Sol_FechaLC: {
        type: Sequelize.DATE
      },
      Sol_MontoActAcum: {
        type: Sequelize.FLOAT
      },
      Sol_TipRelASFI: {
        type: Sequelize.STRING
      },
      Sol_TipoSeg: {
        type: Sequelize.STRING
      },
      Sol_Aseg: {
        type: Sequelize.INTEGER
      },
      Sol_PrimaSeg: {
        type: Sequelize.FLOAT
      },
      Sol_SDH: {
        type: Sequelize.STRING
      },
      Sol_TasaSeg: {
        type: Sequelize.FLOAT
      },
      Sol_EstadoSol: {
        type: Sequelize.STRING
      },
      Sol_Poliza: {
        type: Sequelize.STRING
      },
      Sol_ExtraPrima: {
        type: Sequelize.FLOAT
      },
      Sol_MontoSolSus: {
        type: Sequelize.FLOAT
      },
      Ope_TipoGtia: {
        type: Sequelize.STRING
      },
      Ope_Destino: {
        type: Sequelize.STRING
      },
      Ope_TasaSeg: {
        type: Sequelize.FLOAT
      },
      Ope_ImporteSeg: {
        type: Sequelize.FLOAT
      },
      Sol_CodOficial: {
        type: Sequelize.STRING
      },
      Sol_LC: {
        type: Sequelize.STRING
      },
      Sol_TipoParam: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Seg_Solicitudes');
  }
};